# Lecture 1 --- Introduction

Barbieri: oral exam and hope to scatter more so that more people can do it before so to have advantage for other exams. Scatter along 10 days according to our needs. Two days around the exams are the rule. 

Monday 15:30 - 18:00 
Tuesday 10:30 - 13:00 
Wednesday 10:30 - 12:00 

Insights on Information Theory on 20th-21st April. 

**Outline** 
- Data Mining and AI / Biomedical Signals 
- Multivariate and Franger Causality, Connectivity 
- Higher order spectra in Physiological and neural signals 
- Neural Signaling 
- Statistical modeling: PDF, PPM, SSM (applications to cardio and neural signals 
- ICA, applications on EEG and MEG 
- Statistical Learning (unsupervised Learning) 
- Programming statistical learning for cardiovascular data 
- Combining Brain Imaging techniques with physiological variables

# OVERVIEW 

Data comes in many forms (even health data). There are four categories: 
- Simple points in time that represent an observation (date of birth, BP at the netrance of the hospital, comorbidities or other examinations). Not sequential in time and most of the times they are digital (processing is possible)
- Time Series signals: signals that can be recorded in a discrete frequency (fs). ECG, EEG, EMG, BP, Respiration, Pupillometry etc. Variables that vary in time. 
- Event Series signals: linked to physiological system that activates only at a specific time. Heart Beat, Neural Spiking Activity. Series in time but relates to events along time. 
- Spatial Series signals: Independent variable is a function of space. Ultrasound, CT scan, FMRI, MRI, they can be either 2D or 3D. 

This taxonomy is an approximation of the reality with some items that belong to none of them. 

The first thing is to understand the scale at which the system works. It is always useful to understand the scale. If we see too close with very high degree of specificity. Some information always is in higher scale. 

*Example* . EEG has very low spatial resolution but very high time resolution is required. Spatial and temporal resolution should be at the same level of the changes in the system under study. 

A lot of attention will be put to neural recordings. BCIs with several steps that require to understand how Neurons work. Encoder-Decoder structure. 

**Digitalization** of any process allows to store the information and gives the power to generate models following the encoding-decoding paradigm. 

Neural Recordings can create code to grab information on neurons and how they interact. EEG, MEG store the same information in two different ways. 

**fMRI** signal gives the activity of the entire brain and records spin of the atoms and thanks to hemoglobin can see which neurons are more active. Map of the activity of the brain. Connect activity with a specific activity. Anything dynamic can be correlated with the brain activity (statistical correlation). By using correlation we can unserstand how our areas are reacting to tasks or stimuli (images of the brain). 

*Example*. We can see activity of the heart and relate to the activity in the brain. We can also decode the information from HRV and understand where the ANS activity that drive the change comes from. 

**Modelling** is based on the transformation of the input variables in another domain. It can be done through differential equations, Transform theory or by increasing the dimension of the output space. It is important to understand that we can augment our space so to divide and classify properly information. 
- Model handles all possible interactions without having explicitly to anticipate them all
- Fit params to physiological model
- Combining associational and model based models

**Artificial Intelligence** is not intelligent and there is overparametrization. The machine is capable of doing what is allowed to do. 

# Analyze Data

The problems arise from different sources: 
- Physician shortage
- Expanding body of knowledge 
- Scientific Drive

Main problems of doctors are: 
1. Diagnosis (understand what does the patient have)
2. Prognosis (understa how it will unfold) 
3. Therapy (know what to do, prescriptions)

Doctors have always pursued diagnosis thorugh methods. Collect information and put them together so to have the bigger picture. (Flow charts)
Questions and answers are considered information bits that can be used to build trees. (rules)

Some representations of disease can be used with diagnostic algorithms that receive as input some data (sympthoms). Here the main issue is **causality** that aims at understanding the relation between two events. (i.e. cloths after vaccine) 

Statistics are necessary to understand the relationship between symptoms and diseases. **Bayes' Rule** updates disease probabilities based on observing symptoms. Practical tools that can give answers to complicated questions like relationships between events. 
- Retrieve conditional probabilities given probabilities that are related to other variables. By observing we can know also its opposite. 

## Probability rules

Confusion Matrix offers a simple representation of the relationship between a diagnostic conclusion and a diagnostic test. (TP, FP, TN, FN) The higher the number on the diagonal the better the model at discriminating.

We need a system that allow to perform optimization of the TP and TN. It can be as simple as disease, no disease. 
- Sensitivity: TPR = TP/(TP+FN)
- Specificity: TNR = TN/(FP+TN)

We should also test the threshold at which there is the best discrimination. Highest TP and TN and depends on the distribution of the target values. 


![Screenshot from 2021-05-10 11-10-17.png](./Pictures/8d89628cad6345df9536d89d2bd4d5fb.png)

Suppose that we are working with ABP. We want to know if the subject is normal or hypertensive. Pressure is very variable and everyone has a different outline and reference value. It is easy to say that **ABP values will distribute to different levels** in different subject and statistically some people with hypertension might have lower value than others that are normal. A threshold should be set up statistically. Below the threshold there is also the tale of the TP distirbution representing the FN. 

We want to **separate the classes as much as possible** and we have to hope that the distribution of the measured values is gaussian and well separated. If not so we have to look at a different set of variables. 

All **new data should fall in the estimated distribution** if the model works as expected. Estimated distribution must be tested and if performance is low new models should be defined. (validation)

The system might change in time and we do not know the ground truth. We have to deal with data. 

### Moving the Threshold
NOTE: Threshold down for screening and up when only the most severe cases should be cured. By moving the threshold we can create the ROC curve. 

# Data Mining 

Precursor of AI and precursor of Knowledge discovery in databases. Same methods and intuitions but different names. (ML, Statistical Learning, business intelligence...)

AI deals with automation. Before with data the qualitative approach was the standard. AI is the process of applying the methods with the intention of uncovering hidden patterns in large datasets. 

## Knowledge Discovery in DBs

Paradigm on how to deal with data from selection to Evaluation (piepeline). 
1. Choose what to record (selection)
2. Pre-processing (remove noise and not default)
3. Transformation 
4. Data Mining 
5. Evaluation / Interpretation (understand why features are important and critical interpretation of the functioning of the system under study)

All these steps can be applied also backwards after evaluation. (**overfitting**)
- Anomaly detection and outliers which might be critical. Need to find similarities in features. 
- Complexity is not always the answer

Ethical commission and privacy issues
