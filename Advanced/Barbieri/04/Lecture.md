L4. Information Theory 


## BASICS OF INFORMATION THEORY 

Information theory studies the transmission, processing, and extraction of information.
Abstractly, information can be thought of as the resolution of uncertainty.

IT was developed with communication theory: information can be transmitted from point A to point B. Transmission of information, information is thought of a measurable quantity reflecting receiver ability to distinguish one sequence from any other: 
H = logS^n = nlogS

S= number of possible symbols
N = number of symbols in a transmission 
Measure of information is the hartley [H]. 

Critical concepts associated with Transmission of Information are:
- The information entropy and redundancy of a source, source coding theorem; (scale is fundamental, thermodynamics laws). 
- Time irreversibility is connected to entropy and its maximization (disorder). System tends to distribute and reach most probable order, disorder is more probable than order. Greater in entropy means later in time.
	- Uncertainty is the key word linked to entropy. More uncertain more entropy, higher states of disorder or entropy follow in time. 
- The mutual information, and the channel capacity of a noisy channel, including the promise of perfect loss-free communication given by the noisy-channel coding theorem;
- The practical result of the **Shannon-Hartley law** for the channel capacity of a Gaussian channel (assumptions on nature and properties of signals);
- The bit: a new way of seeing the most fundamental unit of information.

**ENTROPY**: lack of order or predictability, gradual decline into disorder. In IT a log measure of the rate of transfer of information in a particular message or language. 

*Example*. Dice in a normal env tends to 6 elements of source space and each of the faces will occur 1/6th of the times since they have the same probability of occurrence. Sample of trials might be too small to estimate probability that drives events. System might reach local minima of entropy. 

## Shannon entropy H
`$H_x = -\sum_i p_i log_2(p_i)$` [in units of bits]
- `$p_i$` is the probability of occurrence of the I-th value of the source symbol of a random variable X 
- `$H_x$` is a measure of the amount of uncertainty associated with the value of X when only its distribution is known. 

Sum of all probabilities should be 1 and if probabilities are all equal we have max uncertainty, while a system that has an higher probability value for one case we have a more predictable source. Probability of occurrence of a specific value. Higher predictability lower the entropy and lower uncertainty. 

I can define how much information is acquired due to the observation of event I. It is always a on negative quantity.  
- Zero information when we already know that it would have happened 100%. 

## Properties

- Information due to independent events is additive. If two sources are independent and have no relation there is no deal in studying them together. They do not relate and by observing one we cannot infer nothing about other variable. Information is additive and not complementary. 
- If they are related we have overlapping information and it decreases uncertainty. Gaining information and thus reducing entropy.

**Change in entropy can tell about variables, if no correlation entropy does not reduce. Entropy is the aggregate of the total amount of information that we get.**

## Information

I(p) = log(1/p) where p<1. Variable information is positive. 

The base of the logarithm can be any fixed real number greater than The different units of information (bits for log2, trits for logs, nats for the natural logarithm ln and so on) are just constant multiples of each other. —> I need to be aware of the choice and maintain convention. 

Point wise information with a specific event. If we have a distribution with event i with probability of occurence p_i with outpcome n_i. 

`$\sum_i n_i I(p_i) = \sum N p_i log(1/p_i)$`
The more N represent a real fraction of the system the more we are able to compute the real entropy of the system. Formula simple but we have to assume that p_i reflect real underlying probability of the system we are observing. The better the estimate the better the results in representing the real case even if it is a stochastic process. It is not easy to compute probability. Some assumptions can be made and probabilities in a biological system are not as intuitive as the dice. 

**Modelling**: finding exact probabilities of occurrence. 
I = `$\sum_i p_i log(1/p_i)$`
I = Average amount of information that we receive with every event

## Entropy

The entropy of a source that emits a sequence of N symbols that are **Independent** and **Identically distributed** is N*H bits, while if non independent the entropy will be less than N*H. 

**Entropy is a measure of uncertainty.**

### Binary Entropy
Example. Binary case. Max information is when the following bit cannot be predicted by receiver (50% chance that is either 0 or 1). We have to compute real probabilities. 

Gaussian assumption can give ceiling value. In general entropy is computed for a finite number of sources and if we have a set S (even continuous) we need the distribution of occurrence of those values. p(x) describes the values that are more probable. 
It is not a point by point information, but we have to know the entire system through observation. Entropy is the expected values of information given all the values that X can assume. (Information is additive)

The special case of information entropy for a random variable with two outcomes is the binary entropy function, usually taken to the logarithmic base 2, thus having the shannon (Sh) as unit: 
H_b(p) = -p log_2p - (1-p)log_2(1-p) 
it is a parabola with maximum at 0.5, the porbability at which we have maximum uncertainty. This can be extended to any dimensionality. 

## JOINT ENTROPY 

With log transformation we go from a system with multiplications to a system with the **property of additivity**. 

The joint entropy of two discrete random variables X and Y is merely the entropy of their pairing: (X, Y). This implies that if X and Y are independent, then their joint entropy is the sum of their individual entropies.

RULE: **PROBABILITIES MULTIPLY > ENTROPIES ADD**

For example, if (X, Y) represents the position of a chess piece X the row and Y the column, then the joint entropy of the row of the piece and the column of the piece will be the entropy of the position of the piece.

`$H(X,Y) = -\sum_{x,y} p(x,y)log p(x,y)$`

## CONDITIONAL ENTROPY 

When we **loose independence** the problem is more difficult to frame and we can compute Conditional Entropy. We would like some dependence since in there it relies information.
We have to introduce the concept of conditional entropy or conditional uncertainty. Look at x, then y, but also at the two vars together. We can compute the equivocation of x about y and viceversa. Pick one value of y and see what x does. We need to increase number of observations to analyze both. We need more observations to analyze the hypothesis done. Lack of independence means that one variable can affect the likelihood of observing a given realization of another variable (decrease uncertainty or entropy)

Because entropy can be conditioned on a random variable or on that random variable being a certain value, care should be taken not to confuse these two definitions of conditional entropy, the former of which is in more common use. A basic property of this form of conditional entropy is that:
H(x|Y) = H(X,Y) - H(Y)

Conditional probability (equivocation of x about y) is equal to joint entropy of the two variables minus entropy of y. There are also other point of view to compute that and can compute H(Y|X) in a similar way.

## MUTUAL INFORMATION 
Measures the amount of information that can be obtained about one random variable by observing another. It is important in communication where it can be used to maximize the amount of information shared between sent and received signals. The mutual information of X relative to Y is given by:

`$I(X;Y) = \sum_{x,y} log(p(x,y)/p(x)p(y))$`

`$I(X;Y) = H(x)-H(X|Y)$`

`$H(X|Y) < H(X)$` (log of joint probability at lowest is 0 when independent x,y)

The general concept can be espressed as: knowing Y, we can save an average of I(X; Y) bits in encoding X compared to not knowing Y. Mutual information is symmetric. 
`$I(X;Y) = I(Y;X) = H(X) + H(Y) -H(X,Y)$`

**when X,Y independent mutual information is 0**

KULLBACK-LEIBER divergence provides information as expected value of differences of conditioned probabilities and prior probability. The higher the distance the more information is carried. **Mutual information is expected value of KL divergence.**

![Screenshot from 2021-05-13 12-51-59.png](./Pictures/b18e153ab1cb4943bbc1f8535a6c2543.png)

Everything comes to different frameworks and joins in a common framework. ESSENTIAL TO REMEMBER 

![Screenshot from 2021-05-13 12-52-28.png](./Pictures/e9077da87d2642dfb5f2c692209a5b21.png)

X, Y belong to different sets. Intersection is the information they have in common (mutual information). The circles represent entropy. 

**Joint entropy is lower than the single entropies by the amout of shared information.**

- If information is 0 then the two are independent (H(X,Y)= H(X)+Y(Y)) 
- If two signals share information we have decrease in entropy
- Entropy is maximal when there is independence

### Bayes Theorem 
`$p(x,y) = p(x|y)p(y) = p(y|x)p(x)$`

also holds in entropy and can retrieve the **formula of mutual information**

`$H(X,Y) = H(X|Y) + H(Y) = H(Y|X) + H(X)$`

![Screenshot from 2021-05-13 12-56-48.png](./Pictures/1d31c159e8d14b71a69e96b19a358d03.png)


## CONTINUOUS DIFFERENTIAL ENTROPY

We can have continuous entropy (we do not have them in real life but we can use this representation that can help us define important variables). 

P(x) is the **probability density function.** Entropy will be computed using the integral in the course. Integral adds up to 1 by definition. S is the domain of X random variable (support region) 

![Screenshot from 2021-05-13 12-59-02.png](./Pictures/897cad6655214583b0826aeef95a3fd9.png)

### Differential entropy of a Gaussian

Only first two moment are non zero. Mean and variance are a priori non zero. Pdf is defined by continuous time but with only two params: mean and variance. (Bell shape)

![Screenshot from 2021-05-13 13-01-37.png](./Pictures/e49155b90be94f1eaa3cf06194c67c4a.png)

Then entropy (h(x)) is equal to:  

![Screenshot from 2021-05-13 13-09-18.png](./Pictures/dd3e02c7e5b340e7a35f04894d16494f.png)

and in this case we have an expression for entropy. We have the entropy proportional to `$\sigma$` square. This tell us that if we have a variable that is more predictable we have lower entropy. Less predictable higher entropy. Phi is the probability function. Bell smaller around the mean means that we have higher probability to obtain the mean value and viceversa. 

### Non Gaussian Distribution
Now given a random var X with a pdf f(x) with zero mean and variance sigma^2. 
we can define `$h_{\Phi} (x)$` which is not entropy:

![Screenshot from 2021-05-13 13-04-06.png](./Pictures/6300576744d343b285a10f4b4f7778c5.png)

so to obtain proportionality to sigma square as if the distribution was a gaussian (it may only at first approximation and non zero higher order moments). 

This value is the same as the entropy of a gaussian. The true entropy of the function is h(X). 

## MAXIMAL DIFFERENTIAL ENTROPY 

If we sum this value to entropy we have an integral that is less than the log of the integral. 

![Screenshot from 2021-05-13 13-13-25.png](./Pictures/8bee4e65177a4162af54cb5d65d1c793.png)

**Gaussian of the second order of any function has always maximum entropy.** There is no function that can have higher entropy. 

Entropy of the process cannot be higher than its gaussian counterpart (gaussian with variance of the system under study). The error we make is proportional to how much the distribution is far from normal. We have a way to measure the entropy of a system. This can be done with neurons. 

## ENTROPY MULTIVARIATE NORMAL DISTIRBUTION 
To go from an univariate to a multivariate distribution we just consider a series of n variables in the multivariate normal distribution. 

We use the covariance matrix K and can have the maximum value possible. Covariance matrix is related to entropy. 

![Screenshot from 2021-05-13 13-17-07.png](./Pictures/76d291d2509547d8b30948a06485e171.png)

Similarly to the one dimensional case we can use the matricial form to define the differential entropy and the conditional differential entropy. 

## BIDIMENSIONAL REPRESENTATION 

Bivariate model and we assume that noise is gaussian white noise. Here we use same assumption and we can compute entropy of multivariate system. Entropy depends on cross term. 

![Screenshot from 2021-05-13 13-21-35.png](./Pictures/40f12ce892904f57bcdecf81d86ba0ec.png)

Practical demonstration that by increasing dependence of two variables it directly affects value of entropy. Proportional to uncertainty to single processes and how much they are related. If they are correlated it tends to a low value. For mutual information we have positive value of information. 

NOTE: we have related variability, entropy and information through matricial relationships. AR modelling relates to entropy and conditional information relates to the gains of the signals. Gaussian assumption simplifies the computation of the variables. (useful insights)

### Computational Neuroscience example

Estimate max information capacity of neuron and compare information to measure how much a linear model can approximate the complexity of a neural code. Temporal precisione of neural code and minimal timescale in which info is contained. 
Information can be measured but the stage has to be set up and it is bound to resolution.Shades of entropy assuming different time resolution. Hypothesis on specific resolution to analyze neural code. Distance between spikes and try to find minimum definition in time. Models 

Stimulus response information model. Neuron as a system that is listening to input stimuli and responses are provided. This is ideal for applying IT studied today. Paper that describes the details. 


**Within the proposed probabilistic framework, the key problem is to find the best way to estimate the stimulus from the response.**
