L5. Neural Coding

# NEURAL CODE 

Examples of how to apply IT to Computational Neuroscience. Neural Coding tries to quantify efficiency of a neuron and the information it carries. It goes hand in hand with technology that allows us to record and study signals and the system. 

The spiking activity stores information about the stimuli given to the system (response is correlated to the stimuli). Not only the model is important, but also its interpretation and the way it is applied is essential. We need to look at correct stimuli and responses so to avoid undesired biases. 

We want to estimate the maximum information capacity of a neuron and the actual information transmitted. We are using models that represent the neuron as ideal. Stimulus and response are defined in a specific environment composed of pathways and physical interactions. There are synapses and receptors. We must be clear about the assumptions we make. (Modeling)

Information is also useful to test optimal models and non linearities. When facing a problem start with the easiest and simples model. It can be done fast without computation. Even if it is not the way to go a lot of information can be retrived. We can tell something about the system. 

We can go from simple models to models that decode the temporal precision of the spiking activity. 

## Measures of neural code

Models that embed probability structure so to compute information. We can use simpler models to measure the real amount information. Approximation on how the neuron is working. (bases the theory on the gaussian hypothesis since it simplifies the formulation of the model)

- Calculating information transfer 
- Direct (estimate probabilities and compute I(x)) 
- Upper bound (extract response and noise from averaging and compute SNR) 
- Lower bound (information is never created in the end) 
- Upper -lower = nonlinearity

The simplest model is the black box which correlates stimulus and response of the single neuron (very complex). Experiments must be set up so to provide specific sequencies of stimuli and require a lot of knowledge of the system under study. 
![Schermata 2021-05-14 alle 12.19.25.png](./Pictures/a47ff79202eb49b580ac68e79ed2ea7f.png)

Given a response which is the probability of having a stimulus ? Experiments are needed to establish what are the variables. We start looking at the models and compare the stimulus provided by the model and the one effectively given. We can write relationships between two variables (conditional probability of having a response given a stimulus or viceversa). 

## Stimulus Response Information 

It is important to compute entropy and information. Suppose we observe a specific response: two very deterministic strings of fires and 50% we have response 1 and 50% response 2. Our prior is 1/2. 

We can observe the conditional probabilities (example with only two stimuli). We observe how many times we see a response given our stimuli (of the stimulus we decide the probability of occurence). 

We can compute joint entropy and joint probability. By only looking at the response we can compute conditionals. 

![Schermata 2021-05-14 alle 12.28.10.png](./Pictures/ab28554522204346a9acf41984241b6e.png)

Entropy is lower: we know that if we observe r2 we are sure the stimulus was s2. Absolute certainty, while s1 triggers both r1 and r2. Overall we have a decrease in uncertainty. 

This is what we want from BCI (guess neurons that encode an action or thought). Estimate a stimulus from the response. We want models that decrease entropy. We want to build relationships between input and output (increase information). Model to be able to transfer max amount of information. 

## Neural Spike Trains 

Spike trainsa are represented by a temporal series of impulse events. Given a temporal window the event count within the window is the simples estimate of neuron firing rate (model: binary code). 

![Schermata 2021-05-14 alle 12.31.22.png](./Pictures/c8df758edd894328871440b0ac257824.png)

We care about the time at which the spike occurs. They can also be represented with dots so to consider more experiments at a time. Scientists showed that only the spike does not tell anything about the light (stimuli) since it is different experiment by experiment. What counts is the frequency of spiking not just the spike itself. 

Several experiments must be put in place so to be able to identify this pattern (series of experiments). 

### Representation of Neural Data 

**Raster Plot**: allow *firing rate estimation*. Multiple trials of the same experiments. There are lighter areas and darker areas. Light area in the first ms, darker area right after and gives us information about the response of the neuron. (count spikes)
- More observations allows us to compute firing rate

![Schermata 2021-05-14 alle 12.34.21.png](./Pictures/805343d8303846dea99f62b7cc1d1742.png)

**Post Stimulus Time Histogram**: spikes are summed up within each temporal bin and including all trials, and the values are normalized by the total number of spikes. 
- Time binning is needed to partition the temporal axis in small windows.

The neuron overall gives a dalayed response linked to the refractaroy period of the cell. 

![Schermata 2021-05-14 alle 12.37.38.png](./Pictures/c32cf9e80a72443c9ddaf820d89c6f86.png)

Estimating the firing rate is not an absolute procedure and depends on the definition of a time window. (count of event in a specific time window)

The more we decrease the resolution (time bin) the less points we have inside the bin. Too wide bin we have enough points, but decrease resolution across time. While, too small bins would provide a bad estimate of the firing rate. 

We need to enhance the properties we are looking for at our best. We always normalize. 

**Example**. Neuron Adapting to stimulus

Rare spikes during first trials (Neuron always spikes randomly with a resting firing rate) and with higher number of trials (trial 25/26) we have an increase in response after 1s approx. 

As the number of trials increases the response is more intense and with smaller delay. (The Neuron is learning). Reinforcement. 

PSTH looses the moment of leanring, but gaining amount of response of the neuron. If we integrate along the trial number we obtain the Spike counts. Quantification of the response of these neurons. 

![Schermata 2021-05-14 alle 13.28.00.png](./Pictures/12cd925234f54e968f74c96f059f476f.png)

Data gives information when properly represented.

## Model of a Noisy Neuron 

The neuron has a response that can be defined as: 
`$R(s) = T(s) + \sqrt V(s) \xi$`

A neuron that is responding to light is establishing a law of proportionality between intensity of stimulus and firing rate. We cannot represent only the response with a deterministic part, but we need noise (uncertainty) since the neuron is a statistical entity. 
- Gaussianity allows to represent with two parameters (suits our observations)

Some stimuli do not affect at all the response of the neuron so they are discarded since do not bring any information. We play the prior of the stimulus and we observe the distribution of the responses. 

x-axis: Stimulus intensity (scaled to the maximum intensity)
y-axis: response firing rate

Not deterministic curve. At saturation the firing rate does not increase more. There is a refractatory period. At the intensity of saturation we need to provide higher intensity stimuli (most of the times it is useless and damaging). Responses are not always the same, but distribute around the mean value with a given specific range.  

![Schermata 2021-05-17 alle 14.30.20.png](./Pictures/cd5532d268ec400fb4e264fc525d1ead.png)

We need to choose stimulus prior so to have optimal distribution for the information we want to identify. We are creating probability of the stimulus (i.e. intensity). The bin (stimulus resolution) is also defined by user. 
Finally the response (occurrence of firing rate) can be plotted. Distribution of all responses scattered from 0 firing rate to the highest firing rate. 

Afterwards, the conditional probabilities can be defined by looking at specific values. (i.e. color). s1, s2, s3 all have a bell shape and approximately a gaussian. 
- prior chosen on purpose so to provide more stimuli in the range s2, s3. There is more detail in that area, while little experiments over s1. This has been done to enhance information knowing how the neuron respond. 
- we want more stimuli where the response is more sensible, while less points where differences are minor. 
- The area that represent stimulus-response is the **tuning curve**

### Stimulus-Response mutual information

Given the tuning curve we can compute the information:

![Schermata 2021-05-17 alle 14.39.57.png](./Pictures/e40bdbff3d8543d1bac8e0feb5553872.png)

Information = Entropy(r) - Conditional Entropy for all stimuli 
- prior of stimuli s_i 
- distribution of response p(r|s_i) for each stimulus s_i
- conditional entropies: conditional probabilities (noise entropy)
- The more p(r|s_i) is closer to delta function the lower the value for H(r|s_i)

higher entropy, higher noise entropy. Wider the curve around the tuning curve the more uncertain the model is and the less accurate the model is. (**Less noise uncertainty=higher information**)

When tuning curve is thinner then the uncertainty restricts. More deterministic relationship. 
I(S,R) = H(R) - H(R|S)

Set of formulas to use and try to represent model with priors and conditional probabilities by using the **gaussian approximation**. (in the paper there is a review of all the formulas). 
- Gaussian approximation: entropy is proportional to variance 
- For additive Gaussian Noise, Information is proportional to SNR

[Audio lost 1:20 27-04-2021 - Formulas not asked during exam]

Information rate is computed in bits/sec or bits/spike.
Transfer function is like having a tuning curve (in-out) and relates frequency response of the system to the stimulus input. 
- In AR model noise is proportional to uncertainty of the noise. 
- Variability of singal, noise and response can be estimated. 
- **SNR** (linear domain): coherence^2/(1- coherence^2). Coherence to 1 means SNR to infinity. More coherent more SNR ratio, noise tends to 0. 

No linear relationship between stimulus and response due to saturation. Highest resolution possible, but with enough data in each bin. We start with a gaussian distribution of the input. 

## Methods for estimating information
We estimated a probability for our neuron. In order to estimate a model we need to gain observations of the neuron's response knowing the stimulus we provide. 

Each point is placed in the plane (cloud of points) so to estimate distribution of occurence (histogram). We can bin and project on x or y to have observed probabilities. **We need a lot of data points for the direct estimation**

There are shortcuts to estimate information: 
- Upper Bound: gaussian approximation SNR (information of the gaussian is an upper bound of the variable of interest) 
- Lower Bound: computed to reverse engineer the problem so to go back to stimulus given the response (conditional)

Given a response we can use the rule and estimate stimulus. Model might not be accurate and the more different it is the less it is good. Model always contains less information than the true one, if the model is very good it can at maximum come close to it. 

The upper-bound calculation is a variant of the direct method that is used for dynamic stimuli. It assumes that the neuronal response and neuronal noise have Gaussian probability distributions in the frequency domain and that neuronal noise is additive.
- Gaussian has maximum entropy (SNR method gives upper bound)
### Channel Capacity of a Neuron (Upper Bound)
1. The same stimulus is presented n times while the responses R, are measured.
2. These responses are averaged to obtain the average response Ravg. The difference between each R_i and R_avg become the noise traces Ni. 
3. These are Fourier-transformed to the noise power spectra N_i(f), which can be averaged as well. 

![Schermata 2021-05-17 alle 15.12.34.png](./Pictures/33e2c3485b454e03b7833e1712d8e86a.png)

- Bottom left, power spectra of the mean response (red) together with the mean power spectra of the noise (yellow).
- Bottom right, ratio of these two functions, the so-called signal-to-noise ratio or SNR, together with the cumulative information rate.
- Response and noise data were created in a pseudorandom way from Gaussian distributions. 

### Lower Bound 

To estimate the lower bound we need a model with a rule (tuning curve). It gives less information than the true one I can ideally derive. 

Rule that relates response to go back to stimulus. Impulse response and we have real stimulus and response estimated of spiking activity. The reverse will give a decent result when based on previous works. When we reverse a correct estimated stimulus it will follow what the original stimulus is doing. 

The more the estimated stimulus reversed and the true stimulus coincide the better the model is (parametrization). Information content is bounded and thus estimated. With the model we decode the response to obtain estimated stimulus and will return SNR ratio (lower bound)

The information content tell us about linearity or non linearity and goodness of the chosen models for encoding dynamical stimuli. (precision of neural code). Finally we can estimate stimulus given the response (decoding neural activity) 

## References

- Read: *Compendium Neural Code: vol 2 no 11 Borst Theunissen* 
- Information Theory Matlab Toolbox
- [**Information Theory Toolbox**](http://it.mathworks.com/matlabcentral/fileexchange/35625information-theory-toolbox)
- [**This package**](http://www.mathworks.com/matlabcentral/fileexchange/5582 6-pattern-recognition-and-machine-learning-toolbox) (now a part of the PRML toolbox)
