L6. Point Process Theory & BCI

# Neural Modeling

Statistical methods are important for specific applications for research and medicine. In particular neural modeling is related to applied medicine.

- Develops algorithms that are embedded in devices that are used in applied medicine. One of the Historical evidences is that Neuroscientists played a key role in medicine. i.e.
- **Place cells highlighted** the importance of the neuron in characterizing the response that the neuron was providing. Stimuli were entailing the position of the subject. There are several experiments that highlighted the stimulus response entity. (core of modeling)
- **BCI (brain computer interfaces)** starting from 80s. Phillip Kennedy was one of the pioneers in this field. There are still a lot of issues in allowing this to be a paramount application. There have been important advancements that pushed this field forward. (**Neuralink**)

## Nicolelis 2000

(monkey connected to joystick. task to bring own dot to cover target point. Reward paradigm).

![Schermata 2021-05-18 alle 17.28.23.png](./Pictures/b07dc9244c6b4a13856d0f46fbd40424.png)

With **raster plots** we can see spike activities when a specific movement is performed (encode directional space in a finite number of directions and understand stimulus-response algorithm).

- From the response (movement) we want to know which stimuli triggered it.
- we could attach computer and artificial attuator instead of the arm (encoding-decoding models). Preserve right guess of the movement

By looking at the motor area we can sense a signature and distribution that let us see the specific direction that the monkey has taken. We determine a rule that bring us back to the right direction that the spike activity has taken. We can create an actuator that performs the same motion task. Encoding and decoding model allow to transfer information in a way that the right guess or movement is preserved.

When the monkey is performing a specific movement the neurons will spike and the model guesses the direction. The movement can be performed by the computer. The joystick can be disconnected.

Then, after a set of time starts understanding that just by thinking the cursor moves. Without moving the joystick the monkey is optimizing the performance. Bypass motor link by decoding what neurons are saying. Principle that takes to a new artificial arm that can be moved (prosthetic device controllable by own will and neural activity)

### Going further with the research

In the computer we can develop maths and stats that can develop a BCI. Examples: Robotic arm, Brain to Brain interface. Technology is the limit today. This can be done by being able to **encode and decode information**.

- Much easier to control an artificial arm than own hand.
- health is the topic of interest. We are concerned with growth of health after wealth (commerical products)

## BCI Scheme

![Schermata 2021-05-18 alle 17.41.29.png](./Pictures/606e075584b74133bd4ed22cf1576283.png)

We want to understand and maximise transfer of information from stimulus to response (encoding) and viceversa (decoding).

Once we know the models (by performing experiments and recordings) we can go back and start decoding the information carried by stimuli in responses. (encode information in new ways)

- when we own encoding it depends only on physical transfer of information (EEG, ECoG, LGP, SU)
- If we can record single neurons we can get to exact processes that our mind processes but nowadays technology is still recording surface potential or small groups of neurons via inplanted electrodes.

By understainding how information is transferred from image to occipital cortex level we could create an artificial stimulator that can be applied to occipital cortex. If our pathway is damaged we could restore vision capabilities. There are thousands of other examples that can be found in literature. Robotic arms

Modeling hypothesis on the rules that make the system observed to behave in a given way. Statistical rules that try to understand the signals to check what is going on in the brain. (relationships between inputs and outputs) The most general rule as related to information (probability of observing a specific response given a stimulus ) and to go back to the stimulus we have to go back to check the probability of the stimulus given the output.

## The Neuron

![Schermata 2021-05-18 alle 17.47.18.png](./Pictures/1b34b8dcae7f417c82db0cd2ead7f025.png)

Central Body with an axon (myelinated) that can transmit information in a fast way. Communicate through synapses through chemical interactions.

**NOTE**
Without physiology the models cannot be built. We have to look at how the system works and the variables that are relevant to the system. Some models might not contain relevant information when disconnected from the reality they are trying to model.

### Action Potential

Cell being able to open and close channels for ions (change ion potentials). Dynamic changes and big spiking in potential (hyperpolarization)

Observe how the **H&H model** predicts that the generation of the action potential is described by a very specific cascade of events.

- The fastest change is related to the increase of m.
- The conductance at Na + increases indicating the opening of the relative ion channels with consequent entry of Na + ions and rapid depolarization of the membrane.
- Approximately when m reaches its peak, two other phenomena occur: the decrease of h and the increase of h n. The first predicts a reduction of gNa (channels are closing), the second an increase in gr and therefore the opening of the lonic potassium channels.
- The collapse of g a reduces the inflow of Na +, while the increase of gk Increases the K + efflow. Both phenomena contribute to the membrane repolarization, the potential of which returns to rest.

Cell is coding input into spiking activity thanks to the conductive properties of the membrane (physiological level allows to select a specific model that relates a specific input type to the output). Encoding is done through changes in potential.

- Interested in sequences of AP and not the potential itself (not H&H model)
- More inputs the more action potentials. The cell gives us a specific action potential. We want to see how stimulus changes sequence of AP.

## Integrate and Fire Model (IF)

![Schermata 2021-05-18 alle 22.01.14.png](./Pictures/1a77e1c52f2f452fa85841583cc6ba0f.png)

Explains the onset of each AP. It is at the core of what we want to understand. We still do not know how it is connected to the delta potential. Shape of the AP does not matter and we can just use the diract delta function.

- series of dirac functions

We use the Neuron Synaptic Model. Addition up to threshold like in the Integrate and fire model which is related to the H&H model but explains onset of each AP and not the specific morphology of the AP. We still do not know how stimulus is related to AP. Since the shape of AP does not matter we can use any marker that tells us when AP happens (Dirac delta function impulse).

![Schermata 2021-05-18 alle 22.02.34.png](./Pictures/ad51b1a5d76644778f55923d715e836a.png)

The neuron is transmitting a series of Dirac functions happening at specific moments in time. At time t_j the impulse occurs. The IF model can be implemented using differential equations which are a very powerful mathematical tools to connect inputs and outputs.

![Schermata 2021-05-18 alle 22.14.35.png](./Pictures/47c6906604994c808083c3ca16b5117c.png)

As we increase dimensionality we make models that are more and more complex with articulated rules. IF model is a simple way of charactering how the rising potential gets up to the theta threshold. If slope increases then it will be able to reach the threshold in less time and spiking rate is higher. Take a window and count spikes and gives the rate of spiking activities. More often spiking means the communicating neuron has a steeper curve.

**Neural Spike trains**: represented by a temporal series of impulse events. Given a temporal window the event count within the window is the estimate of the neuron’s FIRING RATE.

![Schermata 2021-05-18 alle 22.15.07.png](./Pictures/ac20294dabb2448599d97e3a34eba54f.png)

## Representing Neural Data

Before modeling we had to represent data. The easiest way is to realize the neuron is a stochastic entity and in order to grasp how the neuron is responding we need to reproduce an experiment over and over. We need trials.

The **neuron is always answering differently**, but as we observe that there are patterns in the occurrence of spikes in the space time-trials and we can make some hypothesis on how the neuron is responding to the stimulus. (i.e. light stimulus)

Spikes with a bin of time (without caring at the trial) we have a quantifier: the **histogram**. Here we find a number that is the number of spikes per second after a specific time after the stimulus. Here we see that the response happens after a fixed time. **Post stimulus time histogram**. (PSTH)

Brain is able to cancel out or focus stimuli (issue of learning). Neurons learn as they perform tasks. Reinforce of weaken connections when useful (efficiency of the task)

### Multiple Neural Recording

Trial at every level is recorded and raster plot returns several experiments and check for how long the neuron is firing. We can provide an intensity or timing related stimulus. Example of Phase-Locking stimulus-response relationship. Phase is fundamental for auditory neurons.

Modeling hypothesis on the rules that make the system observed to behave in a given way. Statistical rules that try to understand the signals to check what is going on in the brain. There is a black box that wants to understand relationships between inputs and outputs. The most general rule as related to information (probability of observing a specific response given a stimulus ). To go back to the stimulus we have to go back to check the probability of the stimulus given the output, after having estimated the behavior of the brain.

There is a saturation level, no increasing the frequency but the time and it tells more than a time specific limited response. Neuron encodes the stimulus intensity. Not enough the response time, but also worth to record for how long the neuron keeps a given firing frequency.

### Stimulus-Response Structure

We can give the response a specific intensity but also changing the time of intensity. Neurons have found to be sensitive also to the phase (moment in time) at which we provide a stimulus. Neurons attach to waves (EEG alpha wave)

We have a stimulus phase (potential waves) and each neuron modulates response not only from intensity but also at which point of this phase the response is given.

For some neurons the response is highest when stimulus is given in phase with these waves. We need to define a cycle used to relate the phase. Phase is important for auditory neurons.

![Screenshot from 2021-05-20 10-37-16.png](./Pictures/4ca2cc8da468465885c58132ec3d01ab.png)

Neurons have to be tuned to a specific rhythm. Neurons can answer to very complex stimuli also in a very non linear way.

*Example* Sensors that change potential as our hair move. Brain is able to perceive that sensation, in some animals this is crucial for their survival. Neurons can encode very fast changes and by encoding different velocities we know which neurons are responsible for a given sensation.

*Example 2* Place cells are sensible to specific stimuli and each cell has its own task.

### Behavioral Experiment: Learning

Hippocampal Neural Activity (information about places and scenes in an image) analysis. It is essential to recognise familiar faces or mates. It let us know where we are and in which position we are together with each of the subjects. Maps are created by neuron collaboration. Each neuron is responsible for a specific region, the more it fiers the closest I am to that place.

Ensambles of neurons are used to establish and encoding the environment and assigning a region for the neuron (same as whiskers).

Fixation > Scene (repetition/learning) > Delay > Eye Movement response

Rat movement in a small cage can also be sucessfully encoded and decoded. Videotape motion of the rat in space. Black line is the path of the rat back and forth and activity is recorded (invasive hippocampal recording). Now we have a task and we know where in space the cell is firing. The cell is firing always in same direction and position. Cell is encoding specific location and direction of the animal.

There are enough cells to cover our pathway.

Another example is a rat that can move around and each cell is firing when the rat move to a specific region. The paradigm becomes natural, cells are responding to position. Each cell is responding to position thanks to a rule that organizes very specific synaptic networks. But how it happens ?

![Screenshot from 2021-05-20 11-14-00.png](./Pictures/1fedbb2cba4c446f9cbc0b748a19ce0a.png)

We can start estabilishing rules and understand if we can retrieve the position by just recoding the activity of the neurons. Use the reverse of the model.

If we are able to guess a stimulus it means that our model is good. Once we found the model we also perform experiment and look at the difference between predicted and measured position.

## From Representation to Model

Representation is not absolute. Integration for example the raster plot to collaps on time or trials to create an histogram requires a convention.

We need to count spikes and thus we have to define a time bin. Different deltas will lead to different representations even for the same experiment. There is a wide range of deltas that lead to the same conclusion, but it can hide the scale at which the system is working.

There is a delta to understand response, but a different binning is required to encode the information (binning window). Smallest bin would be the best since we want to know the presence of a spike at each time. (for each window we ask whether we find impulse or not)

![Screenshot from 2021-05-20 11-31-48.png](./Pictures/95a820fd54bb449087e4d0057155cc79.png)

Spike train encoding depending on the computer we are working with. Probabilities are computed in discrete times.

We want to move in the continuous time since in the probabilistic framework we prefer continuously defined probabilitity functions. Rules define with continuous time functions for each moment in time. We still want best model that carries maximum amount of information possible for the system under study.

### Inter Spike Interval (ISI)

The dimensionality can be increase by considering also Inter Spike Interval.
ISI\_k = n\_k - n_K-1

which is simply the difference between each spike time occurence and the previous spike time.

ISI provides the first estimate of the probability of having a spike by looking at the differences of all the occurences and generating their histogram.

Given a spike before it gives a probability function of having a new spike (cannot be lower than refractatory period).

The Histogram now can count the ISIs to characterise the stochastic structure in the spiking activity. A different representation allows us to find a probability region in which the spikes are more probable than in other intervals.

Model in which we can guess spiking activity of the neuron. This is part of the process of estimating the right activity. Bin must be defined.

Building the model means to build a Probability Density Function in continuous time which respects what we observe.

# Stimulus Response Model

![Screenshot from 2021-05-20 13-06-39.png](./Pictures/6a1bf50b18f3413b940ffb1e994a1ff9.png)

Neuron as a stimulus response model. Response consists in spikes. Rules that relates stimuli and response in terms of firing rate, but also synchronization.

Continuous process (stimulus) and a response. A **point process** is a binary stochastic process that occurs in continuous time or space. It has only two kind of outcomes.
0: no spike
1: spike

We bin our time (small enough to have one pspike per bin) using t lower than the refractatory period.

Binary process can be treated as any other statistichal process. The point process can be characteristed by making some **assumptions**:

## POISSON PROCESS

Each spike is independent from the others (poisson process with **rate function lambda = # spikes / second**, probability of observing a spike in that delta time)

![Screenshot from 2021-05-20 15-10-44.png](./Pictures/2214095257b74edab2c4aa68af5013f3.png)

Spikes occurring in non overlapping intervals are independent. Interspike Interval probability density is the exponential:

![Screenshot from 2021-05-20 15-18-05.png](./Pictures/8a934a94a7d247849d1ed28586714e93.png)

This model has never been applied since the neuron changes in time and if we want to understand how a neuron changes the lambda is proportional to the intensity of light time variant. For any lambda we have a different probability (**inhomogeneous**)

Poisson is limited in that it is memory-less. Does not account for any spiking history when calculating current probability of firing.

- There is history dependence due to refactory period (non zero at the origin)
- after a spike there is a period in which there is no activity
- still represents basic relationship

## Beyond Poisson

We want a model that represents the physiology and we can work in two directions:

1.  Generalize rate function:
    1.  Inhomogeneous Poisson Process
    2.  Conditional Intensity Function: bound to the proba of finding a spike in specific delta time. Depends on having a spike before or not. (generalization of lambda function)
2.  Generalize Interspike interval distribution (comes from physiology with probability function to estimate it, modelling AR):
    1.  Inhomogeneous Poisson Process
    2.  Renewal Process

Information is univocally defined by probability which may come from observed data (create plausible model).

## Conditional Intensity Function

Conditional Intensity Function Expresses the instantaneous firing probability and implicitly defines a complete Probability model for the point process (probability per unit time)

## Renewal Process

Generalize ISI Distirbution. Model that have been useful and this would be our first guess.

Any Proba Density f(t)>0 for t>0 can be a renewal Proba Density. (Exponential, inverse Gaussian, log normal)

### Exponential Distribution

By changing lambda we allow the curve to be flexible and the histogram can guide us in defining lambda. (memoryless)

![Screenshot from 2021-05-20 16-15-34.png](./Pictures/f509ab84440e45fcba75eeded4ae29e8.png)

![Screenshot from 2021-05-20 16-16-06.png](./Pictures/91315acb6f1d42478f9f4d08a9fc7c7e.png)

Specific skewness and kurtosis. Entropy is log(e/lambda). Decrease lambda means flattened and more uncertainty. More difficult to estimate presence of a spike.

### Gamma distribution

Extention of the factorial function

![Screenshot from 2021-05-20 16-19-06.png](./Pictures/b690aa2ba3a04055af73341a6d28dff0.png)

Two params: shape parameter K and scale parameter theta

![Screenshot from 2021-05-20 16-20-25.png](./Pictures/8a7f202fbb5b4766921cbba359f4c08e.png)

CDF domain and is clearer to see matches (cumulative histogram, interspikes lower than a given Threshold). CDF closer to own histogram which depends on binning (might not have same histogram so different functions can be defined, histogram should not be used to define the mathematical form of the model)

hgihest flexibility the better the fitting and the more difficult is to interpret the model. Sometimes is better to have a less perfect model but very clear interpretation.

### Log Normal Distribution

Log of random variable is normally distributed (Y = ln(X) is normally distributed, only positive real values)

![Screenshot from 2021-05-20 16-27-27.png](./Pictures/9f507c82322044ba9a6557936b26a369.png)

![Screenshot from 2021-05-20 16-30-05.png](./Pictures/3fcd6b0acc2844c3b4c9f364a8dffe51.png)

Defining a gaussian distribution in the log domain so we do the inverse log. Transforming domain of time, but shifting in log scale. This is used to change scale perception (sounds, filters..) and amplifies low values. This is important when spiking activity is very high. We would like to expand domain to the lower part (dilating that domain will give more space to establish important variations to encode stimulus)

- Scale time allows to build models that consider different aspects of our neuron

Transform domains in order to better interpret and increase performance of our model. Gaussian Approximation in log space (very easy).

Closed form of the CDF without needing to integrate and several parameters are already defined when mu and sigma are chosen.

### Inverse Gaussian

Schrondinger used it to describe the first passage of a brownian motion. These definitions apply to different mechanism.

![Screenshot from 2021-05-20 16-35-30.png](./Pictures/ddf0c559623c4867816fd9bacb734c0c.png)

Two parameters mu (mean of the function), theta (shape factor, skewness around mean)

- Differential equations that lead to Inverse gaussian when fed to the brownian noise, the inverse gaussian is returned. (Beta is the slope of the IF model, speed of activity of the neuron)

Models to shape ISI through a PDF (remember the equations).

## CONDITIAL INTENSITY FUNCTION

It is allowed to do anything, depends on t and history. We assume a specific lambda to be variable in time and function of certain spikes (features).

We want to restrict to causality when characterizing a point process.

![Screenshot from 2021-05-20 16-43-44.png](./Pictures/3abf80b0e5044c2b863b213d0b9b2fdf.png)

![Screenshot from 2021-05-20 16-43-37.png](./Pictures/7453f55acd454b14bd4237fdff500de8.png)

There is one to one correspondence between PDF and CIF

![Screenshot from 2021-05-20 16-44-36.png](./Pictures/d685ae8bb53d4b4d84e7e58211a7803e.png)

04-05-2021

# Point Process Model

Highest resolution in the brain is the Neuron. They are structured in networks and the Brain has optimized the neural shape to optimize transfer of information which is specific to given specific stimuli (light, sound, somatosensory ...).

Black box model that takes inputs and returns spikes which are represented by dirac functions (discretized).

Build up a model of the neuron that explains as much as possible the response to a given stimuli (of a specific kind). The process is back and forth and needs a deep basis on physiology, but also mathematical foundations.

After several representations we need to focus on the **Point Process Model** which is a binary stochastic process that represents the occurence of spikes. Bin after bin provides either 0 or 1.

Our goal is to predict the response so we want to predict when a new spike occurs. We need a new representation: the histogram of the Inter Spike Intervals (ISI).

For a series of spikes we need to separate the histograms in bins and define a continuous probability function that better explains what we can grasp prom the histogram.

![Screenshot from 2021-05-21 11-35-06.png](./Pictures/02c5ffc030b848b1b3f84db04af61c30.png)

Parametric models that can be represented also in the frequency domain are very useful but require hypothesis. More parsimonious way of modeling (family of curves is bounding the model only to that shape, still looking for flexibility)

**We make some assumptions on the curves used to estimate the probability function.** Need to find the curves that better fits the data.

We can find rules of proportionality that link firing rate to stimulus intensity. This hypothesis is good and we can count, but it is not enough. Poisson Process (lambda constant) is not representing the neuron since there is relationship with previous spikes (refractatory period is one evidence of this).

A non poisson process uses lambda that is not the rate function anymore, but representing the probability function that we establish (conditional intensity function).

- depends on time (inhomogeneous process)
- depends on history (past activity)

![Screenshot from 2021-05-21 11-48-18.png](./Pictures/f82ba814a3234003857d93530b212360.png)

After the probability function is defined we can derive the Conditional Intensity function. **Probabilistic Point Process** that represents the probability of occurence of a spike given that another happended.

## Properties

### Basic Probability Elements

Given n events (i.e. same neuron at given delta time) we get some information at every bin (one bit of information when there is complete uncertainty).

- If events are independent the probability of the behaviour will be the product
- If they are non independent we use the conditional probabilities (requires a lot of memory)

![Screenshot from 2021-05-21 13-01-23.png](./Pictures/2ee6906f1ef54a599a7a58ab365e73d9.png)

**Markov chains** have very short memory and condition probability only to the previous event. We care only about the information brought by the last event. If we stop at the first element:

![Screenshot from 2021-05-21 13-02-47.png](./Pictures/b101cf84caf84c3abc9151cc41f348da.png)

Which recalls the bayes theorem and allows to swap the variables and compute very useful probabilistic relationships.

## Binary assumption

We can represent the joint probability density as a product of Conditional Bernoulli Probabilities.

Lambda (Conditional Intensity Function) was defined as the probability of having a spike in an interval. lambda Delta is the probability of having a spike in an interval.

Joint probability density must be defined also for the opposite case of not having an event (1-lambda Delta). We then exponentiate by n.

![Screenshot from 2021-05-21 13-05-12.png](./Pictures/23d7c19859064f01bc5b8c05728bf736.png)

We can represent the joint probability as function of lambda, as we observe an event we compute joint probability density and this provides the amount of information that we add by observing our process.

If lambda is High we will have a small term thanks to the exponential. We want to find lambda such that the joint probability is maximal. Lambda higher when we have a spike and viceversa when we don't record one. Parametric formula, we are estimating.

Given the spike times, the joint distribution of the spikes is the product of lambda exponentiated to the integral of lambda, formula that relates probability to lambda (bernoulli equation and lambda as extension of rate function).

## Time Rescaling theorem

Any set of observations from a point process that has a conditional intensity function can be transformed into a sequence of independent exponential random variables with a rate of 1.

we have k-1 **zetas and when lambda was correct they were all distributed as an exponential independent random variable with rate 1**.

![Screenshot from 2021-05-21 13-19-55.png](./Pictures/07b9e9c230e8450fa5d434f9eebf2596.png)

# Kolmogorov Smirnov Plots

Zetas are quantiles and we can compare them with plots. Very useful to confront models and we cannot compare models in the feature space since they are different.

KS plots are a graphical measure of goodness of fit based on time rescaling, comparing empirical and model cumulative distribution function. (45 deg line)

![Screenshot from 2021-05-21 13-27-51.png](./Pictures/15eb26c868d442b4b47760f11064bad0.png)

We can transform the zetas in a new variable in the 0,1 space. We can thus compare the quantiles to the ideal ones (normal distribution). (qq plot)

![Screenshot from 2021-05-21 13-39-15.png](./Pictures/8dc9413324dd4f1abcf9142ad4029686.png)

In practical examples we will have a certain degree of deviation that will twist diagonal either up or down. Close to diagonal closer to ideal model quantiles.

- New space in which I can compare models

![Screenshot from 2021-05-21 13-40-09.png](./Pictures/76159e1dbc804e54804238e71c7d4d95.png)

The **KS distance** is the maximum distance from diagonal. This is a graphical modeling tool that exploits probability theory. We use powerful statistical tools to compare probabilities and to relate them to the ideal condition.

- statistical norm defined mathematically
- Time scaling theorem applied to Point process model to compare models.

z is nothing but the integral of lambda in time between spikes. When there are more points than the normal distribution the line will be higher than diagonal and when less viceversa. Deviation from uniform will give distance measure in a new domain that allows to compare models.

- CI can be defined (p value < 0.05 threshold of confidence)

By doing the opposite process we can define a neuron with a specific probability of firing. Inverse engineer our model and create artificial neurons.

History encompasses the history of stimuli or influences from other env. vars.