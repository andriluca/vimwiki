L7. Continuous Point Process and State Space Models

# CT Point Process

Links between continuous time and discrete time. Times at which the spikes are given we can define a delta function. dN is the sum of all the delta, the counting process is the integral of the dN.

Note. A point process is a sequence of discrete events occurring in continuous time:
![Screenshot from 2021-05-21 16-21-09.png](./Pictures/cafcc61843254acbbe935407a1fd391c.png)

The counting process becomes:

![Screenshot from 2021-05-21 16-22-41.png](./Pictures/c3ecd947fa134a43bd28c4324a55cdab.png)

The Conditional Intensity Function is the probability in a Delta of observing a spike. Istantaneous when delta tends to zero.

![Screenshot from 2021-05-21 16-23-10.png](./Pictures/0a08b8bbea50446fa958efbeaa3adced.png)

Any value in discrete time can be defined in continuous time by using the limit. We can define a formulation that can represent what we are doing. Lambda moving Delta by Delta. Probability of having a spike is lambda Delta plus an error term.

In general lambda can be anything and correpond to the rate function when it is a poisson model.

## Continuous vs Discrete

![Screenshot from 2021-05-21 16-26-44.png](./Pictures/572a7a6b32684a5a9b54233323b143b1.png)

In continuous time we define spike times while in discrete time a poisson process. We can go back and forth between the two with the right transformations.

Optimal parameters lambda to maximize a weight function. We have to decide which type of model and what function to use to optimize.

We want to maximize lambda when there is spike and to be low when there isn't (maximum joint probability density)

![Screenshot from 2021-05-21 16-38-30.png](./Pictures/74bca978a47544f9b7b63dfd4a4974aa.png)

NOTE. From joint probability density we can go to formulas that link probability function to lambda function. e^x = 1+ x for x small (used in step 2 of the definition)

NOTE. log(xy) = log x + log y (multiplication becomes a sum)
log y^x = x log y

![Screenshot from 2021-05-21 16-39-25.png](./Pictures/3cc0130ab1234d448e876eec2679eebc.png)

The beronoulli equation has become (from discrete time to continuous) in the continuous time the **log likelihood** (the term we want to maximize) so to have a model that fits the data best. High probability when there are a lot of spikes.

Likelihood of having a spike or not (Point Process Likelihood). Likelihood that the model is right. Exponential so we need to compute the log: 

![Screenshot from 2021-05-21 16-39-38.png](./Pictures/966810009ca94906b11a75ceb2176c38.png)

Final relationship between probability and intensity function: the joint probability of a spike train is a product of lambda e^-lambda . 

![Screenshot from 2021-05-27 11-53-23.png](./Pictures/7ca271f7048144e8953d6b9f6f6e965d.png)

Given spike times the joint distribution is: 

![Screenshot from 2021-05-27 11-53-44.png](./Pictures/5f7bf88aa94c4bc78ce89929873bfca5.png)

We are going back and forth and used concepts linked to the bernoulli approximation to define something coherent to the intuitive concept of likelihood and by maximising that we obtain the best model joint distribution. 


ISI Probability density and conditional intensity function 

By integrating lambda we can compute quantiles and order them and transform in zetas. If they are normally distributed lambda is good. 

Find those params that maximise the log likelihood. This does not mean that we have the best model, assumptions might go against the underlying system, but the best lambda is found. See if lambda matches the spikes we are observing. 

## GENERALIZED LINEAR MODEL 

When we really do not know in terms of probability we start with a general case that defines our lambda. Similar to what we have done with RR intervals by assuming a relationship between the current and the previous RR interval. We define a linear model and assume the ANS acts on the heart. 

We can create a simple linear case by assuming the variables we are investigating (stimulus, spikes to the past or other neurons) are linnked to the model and we want to integrate them in the model and thus define a lambda so that those values are included in our parametric function of lambda. 

![Screenshot from 2021-05-27 12-07-57.png](./Pictures/861af5bbe8314526a33336f404215c82.png)

Linear combination of variables relevant to our problem. Lambda is associated to a probability of having a spike. 
1. Mu gives the basic firing frequency. Resting firing activity of a neuron, mu will give basic firing spikes (rest fire rate) 
2. contribution that comes from past and present stimuli (felt sometimes with a delay) 
3. history of past firing activity that can influence the neuron (i.e. past RR intervals that influence the actual) to have dynamics we must account for history and with poisson it is not possible. 

We have a linear function that represents the log lambda (matricial linear form). Problem easy to solve. Assume there is a linear relationship with a set of variables (more than one variable and more than one value in the past). With poisson we have a log lambda fixed.

Maximise the likelihood we can go back to the Least Square model. Maximinsing likelihood using a simple poisson GLM it is like maximising using Least Squares. 
GLM has two components: 
- type of obesrvation (time structure of the observations) with specific probability structure. 
- Equation expressing the mean as linear combination of covariates. 

![Screenshot from 2021-05-27 12-17-37.png](./Pictures/2ac6d5a12e704947a9b87ebb65768a50.png)

When trasformed we can use the matricial form


## FITTING GLM 

Maximise Likelihood (log of matricial form) using the newton's method (the same of LS equations). Find initial theta (initialization) and construct the matricial transformation. Gradient in the direction of the new theta depends on spike and lambda, this is at the core of the likelihood. 

high lambda, theta is at the right place. Theta does not change much, correction term depends on relationship and how much observation is different from expectation.Lambda is low we do not want a spike ! If this is always true we are happy with the theta we have. Theta has optimized our likelihood. 

When there is spike but lambda is low we need to change theta so to maximise the quadratic term 

![Screenshot from 2021-05-27 12-21-30.png](./Pictures/3bdbc2cf07e841f5ac91a5199194ed22.png)

In the gaussian case the iteration converges in one step. Each newton step is a weighted LS. (NOte LS algorithm is not required for the exam)

# State Space Model 

Understand what neurons are saying and comunicate that to the actuators (BCI). A state space representation is a mathematical model of a physiological system as a set of input, output and dynamic state variables. 

System is complex and time varying so we must use models that keep track the relationship between stimulus and response. We must consider the environment in which the variables reside. We must consider evolution along time  and we want the relationship to take into account what is going on in the system, that we might not see but still influences the output. I/O state space model)

Defined by state variables with own dynamics and represent what is happening inside the system. We would like to find only the necessary vars to define our model (finite dimension). Internal state must be smallest possible. 
- Define specific synamics of the same variable (i.e. physics motion law, position, velocity, accelearation, liked to each other) 

Observation model, given a statr and input with their own dimensionality we define univocally the output. 

Filtering problem: we do not observe all state variables we create a model that estimates them. States we are evolving with. State at t-1 and we estimate its change so we know how it evolves at time t. Estimates are defined by functions and the state is not observed. We see only the output. 

To check that the state is ok we can confront output. Predicted with estimate and actual output. The difference has a certain gain (relationship hypothetized). 

![Screenshot from 2021-05-27 12-32-23.png](./Pictures/d30e4ac38acf450caec01aff9e394aaa.png)

- With LS and likelihood we were using a general observation paradigm (state is the parameters we are assuming for the model)

With state space model we can model interactions non previously explainable. Inside our brain we have the state transition paradigm which relates to the increased expected probability of having an event. Brain has transition state paradigm (transition is expected) 

The model is defined with rules and at the simples they can be first order differential state equations. The simples is the monovariate first order differential equations. The variation of x proportional to given parameter represents the evolution. Simple model but used for several applications (even the neuron for changes in potential and current)
- we can increase the complexity by using multivariate signals (represent system in a matricial form). Coefficient a becomes a matrix. 
- Haromic oscillator is defined using the second order (acceleration)

input to the model is the exogenous variables (state has its own dynamics) and we can add terms that relate the influence of the input to the system. Input can influence separatedly (additive) or we can assume linear interaction with the state (stimuli have their own dimensionality). Higher order, more parameters.

$$
y(t) = C \cdot x(t) + D \cdot u(t)
$$ 

usually n > m (inputs more than outputs). State evolves on its own, C bridges output and state.

![Screenshot from 2021-05-27 12-54-37.png](./Pictures/7179b9d12cc445d083b4de5c936e2e2b.png)

We can go from continuous to discrete (difference equations) and use observations at each delta. **System theory and state space estimation**. 
We might start form models for the state and check evolution can explain inputs and outputs we have. 

with I/O we can estimate the model. look for univocal solutions (observations allow to have more univocal solutions). 

Now we can extend this approach to the most general case by defining probabilities. State space equation can be defined univocally by defining a conditional probability of a new state given that you were in previous state and the input. 

Observation equations: probability of observing output given previous states and the input. From there we can simplify and use **markov processes** (conditional proabilities to the step before). 

**Additive gaussian uncertainty** (noisy neuron). Neuron estabishes a deterministic rule (mena probability) and uncertainity is additive gaussian noise. Uncertainty in the estimate is gaussian white noise (measurement error with vairance and bias)

![Screenshot from 2021-05-27 13-08-41.png](./Pictures/aa4337a5db4b42ee9286788824041b18.png)


## Neuron State Space Model 
Framework is always the same. Lets relate the state space model to our neuron. 
Next state given uncertainty, previous state and stimulus. 

We define the state as the previous values of a point process (last M previous observations of our spiking activity as represented by a binary model). We can create a string and gives the state at time k. (markov process or order M). 

The next state will be what we observe at k+1 and what we observe up to k-M. 2^M number of combinations that can represent the possible states. Now we can define probability of having either 1 or 0. We choose lambda and it depends on state and on speciif stimulus. 

MLE operation returns a lambda which gives the prediciton of what the spike train will be with a specific state and stimulus (H_k history) abd the probability of a spike is lambda Delta


### Recursive Solution

Chapman-Kolmogorov Equation establishes a relationship between state and observation at stage k and the successive estimation given all the previous observation. 

![Screenshot from 2021-05-28 08-37-32.png](./Pictures/f0a8aa3751ea49f594acfb4e9521fe5e.png)

Estimate Delta State given that you have not an observation yet. One step evolution of our state given we do not observe anything. 

The state evolution equations establish the prior for the state and we have a probability of having a state a k+1 given the state x_k. 

The probability of x_k+1 given a x_k is the moltiplication of the probability. But we do not know that it is the right one. The previous x_k might be anything so its domain is all the possible states it can assume. 

Probability of having a k+1 is a combination of all the steps to an x_ks that it can assume. Bring you a specific state and specific probability. 

Prior will establish what is going to be your future state, average all possible probability given that you have the possibility to go in any state that you have. Integral (sum of all weighting functions or transition) will give the probability of being in another state. Prediction step: evolution of the state and probability given that we do not have a new observation. 

#### Observation yk+1
Recursively we want to estabilsh a new state: Probability of having k+1 given we add a new observation. From x_k+1 given y1:k to x_k+1 conditioned to y1:k+1. 
- bayes theorem to manipulate probabilities to give recursive estimation of state and relationship i/o given we observe a specific output y. 

Recursive formula that can go from x_k+1 to the new probability of having a state given that we have a new observation (additional information to update probability of having a state)
- Guess what is the most probable state

RULES TO KNOW
$$
p(x|y) = p(x,y) /p(y)
$$ 

$$
p(x,y) = p(y|x) p(x)
$$

Observation equation depends on the state and so we want to relate to our model. in CK we use the integral to represent all possible transitions given that we can go to any state. Average of the value gives us the prior defined by our model: it can ge a random variable or we can use our rule to be more specific. 

If the model is correct we have a more certain probability. We go from the probability we were computing from the CK to the new probability. 

With CK we go up to y_k and noew we go up to y_k+1 and again we can find p(x_k+n| y_1:k+n). We update our state and check if our state is right to the observation we have and if not we update our probability. 

1. Prediction step : state equation
2. Update step : observation equation, output given a state. I/O model establishes a rule that returns output given an input. 

If we do gaussian additive noise we make the probability easy, they have a precisione probability function and we do not need to establish all probability function, just mean and variance (expected value and distribution around the mean)


#### Application a Gaussian- Markov process 

Linear model and gaussian we have that product of the two is still a gaussian. Defined by just two numbers so computation is easier. 

![Screenshot from 2021-05-28 09-07-25.png](./Pictures/7860af2215e848febb0eb947f519e00c.png)


When we do prediction step we add uncertainty to the noise. We are creating a matrix where the new mean is multiplied by A and the new variance (uncertainty) is the previous + uncertainty added by model. 

CK provides the step to predict your state. With next step we add an observation but now we have k+1 (update state according to observation that we have)
- becomes always a new ggaussian: estimate of the output in the first state but variance is weighted by C + uncertainty of the observation.

We can compute with rules the mean and variance as function of A, C, R. 
If we compute K_k+1 we get a simple formula and the new mean at the end of the update state (new expected value of the state) will be the previous expected value from prediction step (prev. pred step) plus a term that is proportional to K (Gain) and term that is higher as the distance from current state measured y_k+1 (new observation that we have) and the estimate obtained by updating the state in the prediction step. 

It is like the newton method: update is previous state varied along the direction which minimize the difference between observation and the estimate from the state equations. 

Variance gives uncertainty around estimate. Increase uncertainty since we inject noise, but that uncertainty decreases by a term proportional to your gain (new observation). This comparison leads to a state that is closer to the estimate established by observation rule. 

Original Gaussian we estimate state with certain uncertainty of sigma. Apply CK and have new evolution of the state which gives you new meand and uncertainty. 

The new bell moves by the mean and elarges by the noise uncertainty. Observation helps in keeping you estimate unbay in terms of uncertainty. 

### Kalman Filter 
NOTE. We have two equations that evolve estimate and uncertainty (prediction step from model and update state from observation and model). This is nothing but the kalman filter which is a prediction update step with a probability that is a gaussian (Kalman gain for a linear model). Sigma is the residual, uncertainty of the noise. 

States are your values at the k past values. 


