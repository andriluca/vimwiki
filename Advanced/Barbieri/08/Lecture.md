L8. Modeling experiments (State space) 

# Modeling Structure

Thanks to bayes rule we can go from probability of having event given stimulus (our model) we can compute probability of the stimulus given the event. 

When we define probability it helps us to predict the state, the less random is the probability the more we are able to predict the event. (predict stimulus given what we observe)

Decoding
Extract information from neural code to see what were the specific commands that they were encoding. How the neuron is responding to a specific stimulus: thanks to the model and the bayes theorem we can reach the definition of the stimulus given what we observe by the noise. 

Guess stimulus from neural code that is what we want to do in a BCI. Computer able to understand neural activity to derive driving comand to sento to a prosthetic device. 

## Hippocampal Neurons (Place Cells) 

objective: 
To characterize a single neuron spiking activity with a Bayesian statistical stimulus-response model (Encoding) 

To characterize how ensembles of neurons maintain dynamic representation of the Animal' s position (Decoding)

With information from more than one cell we can build more precise models. Point process and bayesian framework. 

Experimet: 
25 min experiment: 15 mins for encoding and 10 mins for prediction. 

Camera on top that tracks posiiton of the rat and recoding 30 cells. 
- Stimulus and response find model (parametric that represent stimulus and response)
- Decoding: compute probability of having a stimulus given the recorded cell activity (assume probability to be gaussian)

Observation eqaution was based on bayes (spike given position). Formulation followed a point process model: compute events 0 and 1 and have a history. c place cells (for each of the cells we find a specific lambda or specific probability)

We can start with simple exponential probability (define lambda or the probability). we start with probability and namely those probability functions presented before. Any function depends on history and to link our problem to this temporal function we define s(u_k) as part of the model. 

s(u) is an extended lambda (for the poisson was the rate function and allowed us to use a lot of finding from scientific literature)
- higher firing rate wehne the animal was in a specific location.
- firing rate in function of position (place fields that researchers hypothesized)
- 3d histogram (coordinates of the space) 

Gaussian model that is parametrizing the histogram of the spiking activity as function of space. Relate to space since that is the stimulus. 

ISI: histogram along variable time (build function upon that)

Parametric function that approximates the rate we have established: gaussian was the most succesful since it is the easiest (simplicity is the key). Bidimentional gaussian with a specific sigma (width) 

Paradigm in terms of probability of what we observe in the experiment. With just time we wouldn't have reached any knowledge on the behaviour. 

Able to follow the path and count the spikes (time is not relevant, but the coordinate in space is). Modeling histogram with function. We can make the gaussian more complicate 

We can establish a relationship and see how it looks compared to the spikes and decide if that is good or not. We use likelihood: linked s(u) to H_k 

![Screenshot from 2021-05-28 10-20-45.png](./Pictures/c4eab2614c364de6b44f7e6c380d0d6e.png)

time structure: intensity structure and temporal probability. Jump that the point process does. 

More complex temporal function (beyond poisson) allows to have better likelihood. Find z by integrate between spikes. THey distribute better than when we use only s(t). Improve likelihood of our model and is more able to predict position of the rat. (**Place field**)

Now in the mathematical domain we can use any function (Zernike Polinomials with more complex polynomials which exploit gradients)

Zernike allows to be inside the space and it is more specific. We still cannot see what is happening neuron by neuron. 
- The Zernike model is able to shape both symmetric place fields, as well as very asymmetric fields closer to the walls

With correct predictions we can verify that the model is working correctly and even though we do not know the underlying behaviour we can still hypothesize that some information is transmitted in such a way. 

NOTE: each neuron has a lambda and as we integrate lambda and compute zetas and transform them in the uniform range and compare quantiles from the uniform distribution (KS plots)

Each curve deviates from the ideal line at 45 degs. (when close it means that is close to the uniform distirbution)

Note: some of the recorded cells might not be place cells !. Some other cells are more scattered. Tighter the fields the more information they provide (specific position). The more the cell is firing randomly the less informative it is. 

DECODING: 
Bayes to go from spikes to position. We have to know prior 

![Screenshot from 2021-05-28 10-28-01.png](./Pictures/d8752aea74114b839a1c2670bb7e623d.png)

References: 

*Barbieri R, Frank LM, Nguyen DP, Quirk MC, Solo V, Wilson MA, Brown EN. Dynamic analyses of
information encoding in neural ensembles. Neural Comput. Feb;16(2):277-307, 2004.*