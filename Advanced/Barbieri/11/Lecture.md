L11.  Statistical Learning

17-05-2021
[*Some topic will not be required strictly. Questions will be posed on theoretical questions (some lectures are just application of statistical modelling)* ]

Statistical Learning is a framework aimed at solving some specific problems using data (set of variables linked to outcomes). i.e. risk factors for prostate cancer.

System that tends to classify or understand and outcome and it is done by observing variables that are interrelated with outcome. Interrelations bring information on the outcome. By observing we can go back and predict specific variables. It is up to us to define the set and for each set different models may be needed and might be very different. 
y = A * x 

## Supervised learning
Modeling framework that has features (variables that can be measured) and outputs. It can be either regression in the continuous space or classes. 
- outcome measurement Y (dependent variable)
- p is the predictor measurements X (inputs or features)
- **regression problem** has Y quantitative 
- **classification problem** (Y can take only finite values)

We have **training data** (X1)1),...,(XN-YN). These are observations (examples, instances) of these measurements. (Supervised learning)

On the basis of a set of training data we would like to:
1. Accurately predict unseen test cases.
2. Understand which inputs affect the outcome, and how.
3. Assess the quality of our predictions and inferences.

We define outcome(its dimensionality) and variables. Maximum freedom that puts us in a complex scenario. We must be methodological since we do not have an optimal approach a priori. 

The tool might become useless when the hypothesis change. We need to understand from physicians how the system works and we need to understand the best way to formulate the problem. We need to approximate the problem and reach to a mathematical solution. We go back and see if the solution is good if conditions change and in different settings. Models learn by exploiting the machine learning statistical framework. 

## Unsupervised learning

- **No outcome variable**, just a set of predictors (features) measured on a set of samples.
- The objective is more challenging find groups of samples that behave similarly, find features that behave similarly, find linear combinations of features with the most variation.
- Difficult to know how well you are doing.
- Different from supervised learning, but it can be useful as a pre-processing step for supervised learning.

### Terminology 
Now communication is so complex due to abundance of content. Impossible to keep track of everyone else. Parallel studies with very different terms that refer to the same thing (science is more approachable today). 

- **Machine learning** has a greater emphasis on large scale applications and prediction accuracy.
- **Statistical learning** emphasizes models and their interpretability, and precision and uncertainty.

*Example* : understand where to put ads. Regression might be a choice to confront distribution w.r.t. a variable. (from a simple questionnaire)

![Schermata 2021-05-17 alle 16.10.39.png](./Pictures/fc90e2957f8146abae508b4cda5d5603.png)

However there is an inner uncertainty that make the output depend on many other factors. The model can be defined using: 

![Schermata 2021-05-17 alle 16.16.37.png](./Pictures/49e546ea594148c8ac2cff4f14d534e7.png)

where epsilon captures measurement errors and other discrepancies. We might get wrong interpretations from wrong models. We need to find a way to procede in a world of almost infinite possibilities. (need to define a flowchart)

1. We can just **look at values and make relationships** (correspondence of values through tables). The problem is that there might be roundings and approximations that can lead to errors. 
	1. We could average all values obtained in correspondence of a given input. X at very high resolution (i.e. regression problem) requires a lot of observations and memory space 
	2. We would prefere to have a function (regression function) or to release constraint on X and define bins. More points in the interval but less precise. The bigger the intervals the lower the computation (loss in resolution)
	3. **f(x) = E(Y|X=x)** where x might be in a range 
2. The function can be very complex and with several parameters, but we can parametrize it. Optimal predictor minimized the expected value observed minus the one we computed with our function. 
	1. Even if we find ideal f(x) we still have measurement error and there is still an error (**irreducible error**). 
	2. Some models might be close to the real function that is generating our data. (more information means better models)

![Schermata 2021-05-17 alle 16.31.46.png](./Pictures/27428681870041529b107eb26814387b.png)

3. The regression function is defined through a bin and for each bin we estimate a value Y. The performance is bound to the scale of the bin (efficient with low dimensional problems p<4 and N large) 
	1. Neighbour in 1D is just th range. In 2D it is a surface (2 ranges) more and more data is required and range is exponentially bigger to maintain the same complexity. 
	2. A model overcomes the issue of increased dimensionality since it does not depend on local nighborhoods (beyond averaging observation)
	3. Nearest Neighborhood might be feasable as a very first approach (outliers might worsen performance)
4. Parametric and Structured Models: linear regression model is the simples and most common example of a parametric model. p+1 params. 
	1. simple rule between variable and outcome (simple to predict the outcome and easy to understand importance)
	2. We can increase complexity of the model (quadratic model) and study bispectrum (establish relationships between variables not possible with simple linear model)
	3. Regression in a 2D domain is a plane or a surface and the more parameters the more tunable the function is. (wobbly)
	4. Any model can be used (wavelets or sinus functions)
	5. The model can have zero error when the function passes through all the data points, however it does not grasp the rule that generated the points (we need to take into account the non deterministic noise)
	
![Schermata 2021-05-17 alle 16.42.58.png](./Pictures/4b6d62dbacc243b38eebe59ffa18a83d.png)

## Trade-offs 

- **Prediction accuracy versus interpretability**. Linear models are easy to interpret; thin-plate splines are not.
- **Good fit versus over-fit or under-fit**. How do we know when the fit is just right?
	- measure of goodness is not only accuracy alone
- **Parsimony versus black-box**. We often prefer a simpler model involving fewer variables over a black-box predictor involving them all. 

5. **Assess Model Accuracy** by computing Average Squared Prediction Error (MSE) on test data (not training since it is biased towards more complex and overfitting models)
	1. Very flexible model will mean a low MSE in training but not necessarely reflected on test set. 
	2. More flexible means lower MSE but higher risk of overfitting
	3. More linear model might have higher MSE but better performing in testing

![Schermata 2021-05-17 alle 16.57.26.png](./Pictures/50f7fca40ecd41c4a83594bcbb859f20.png)

![Schermata 2021-05-17 alle 17.00.02.png](./Pictures/f6f6e6adbe2a4576955a039e919cf6b4.png)

*Black line*: model used to generate data by adding noise 
*Blue line*: model estimated using spline 
*Yellow line*: regression model 

Need to look at test data. Number gives us clues, but we need to make choices to interpret and get closer to the underlying variability of data. 

6. We need to find a **bias-variance trade off**. Flexibility and complexity of the model. Increase flexibility means describing variability of points, but bias increases because we follow our point. Lower bias means less flexible model that less reflect current data points. 
	1. compare training and testing. Training is by definition biased. 
	2. Too high complexity must be avoided (too many params with low data means to overfit)
	3. Need to find a sweet spot where the two are at their lowest possible value 

![Schermata 2021-05-17 alle 17.09.40.png](./Pictures/5704b9c70b0b48ebabcf52edcc4f66bf.png)

7. **Estimate parameters by Least Squares**. Select parameters to minimize Residual Sumo of squares for a regression problem. 


![Schermata 2021-05-17 alle 17.13.40.png](./Pictures/e126371628ff43248c96a6e07bde1b6e.png)


![Schermata 2021-05-17 alle 17.13.46.png](./Pictures/39ac9f2be4cf4fdc8cc530a346a78f04.png)


![Schermata 2021-05-17 alle 17.13.00.png](./Pictures/e662c937d16849068968562519933c4f.png)

Minimize sum of all the distances. Obviously the underlying model is never linear. 
![Schermata 2021-05-17 alle 17.14.02.png](./Pictures/476cfb2d0b574dd086f70e181125e039.png)

The standard error of an estimator reflects how it varies under repeated sampling. We have: 

![Schermata 2021-05-17 alle 17.16.46.png](./Pictures/87499ae53dc24ee980c2ea5832b3c551.png)

8. **Hypothesis testing**.  beta_1 is estimated and depends on the variablity of data. I can test the hypothesis of it being different from 0 or = 0 in order to test the relationship with outcome. (confidence interval of beta in the range of the estimate)
	1. t-statistic with n-2 degrees of freedom. (**p-value**)
	2. Confidence interval (95%): range of values such that with 95% probability the tange will contain the true unknown value of the parameter

![t-statistic](./Pictures/02afac0c6a274284a296e6984dcdbe03.png)

![Confidence interval](./Pictures/328b31e54af748a69ce2daa5438af5d5.png)

9. Model Accuracy Can be also assessed by looking at the Residual Standard Erorr, the R-squared, Total sum of squares 
	1. It can be shown that in this simple linear regression setting we have R^2= r^2, where r is r the correlation between X and Y. (p.43 L.11)

![Schermata 2021-05-17 alle 17.22.56.png](./Pictures/1705b5cad7b844ef917d3053bd6cdb69.png)


![Schermata 2021-05-17 alle 17.25.08.png](./Pictures/10c89bdda2fe4f46a2e5c9f4993ce230.png)


![Schermata 2021-05-17 alle 17.24.56.png](./Pictures/f6a215fe5aaa4d2babb78b09e7cdf2a0.png)


![Schermata 2021-05-17 alle 17.25.24.png](./Pictures/e03dcc16f0c84177aa26a307e5dc2c48.png)


F statistics can give an importance value for each of the variables when different subsets are asssessed. 
![Schermata 2021-05-17 alle 17.27.55.png](./Pictures/b2494dbabaef4d47b1c02e8fd2e50698.png)


10. Multiple Linear regression increases parameters and extends regression to all the variables. The framework is the same. By increasing the dimensionality the less the model is representable and understandable. 

11. **Forward selection** of variables that allows to choose one variable at a time (hierarchy). The **Backward selection** model is the same but starts with all variables and studies how the model changes as the variables are removed. One is about the performance and the other is about [17:32] 
	1. Begin with the null model, a model that contains an intercept but no predictors.
	2. Fit p simple linear regressions and add to the null model the variable that results in the lowest RSS.
	3. Add to that model the variable that results in the lowest RSS amongst all two-variable models.
	4. Continue until some stopping rule is satisfied, for example when all remaining variables have a p-value above some threshold.

## Model Selection 
Systematic criteria help choosing an "optimal" member in the path of models produced by forward or backward stepwise selection. These include
- Mallow's Cp 
- Akaike information criterion (AIC) 
- Bayesian information criterion (BIC)
- adjusted R2
- Cross-validation (CV).

But what about qualitative approaches? Dummy variables that exclude one variables since one is the baseline model (it is not important the choice of the baseline for the dummy variables)

## Extend the Linear Model 
Expand scope of linear models: 

**Classification problems**: logistic regression, support vector machines
**Non-linearity**: kernel smoothing, splines and generalized additive models; nearest neighbor methods.
**Interactions**: Tree-based methods, bagging, random forests and boosting (these also capture non-linearities) 
**Regularized fitting**: Ridge regression and lasso


The **hierarchy principle**:
If we include an interaction in a model, we should also include the main effects, even if the p-values associated with their coefficients are not significant. interactions are hard to interpret in a model without main effects, i.e. their meaning is changed.

## Classification Problem 

1. **Bayes Optimal classifier**: assign to the point the class with highest probability (probability of a specific class in a neighboorhood). Maximum of probability gives the class. 

![Schermata 2021-05-17 alle 17.44.16.png](./Pictures/305386c7cc6142e4aadf4847f87dc57e.png)

Bayes problem similar to the Point Process Model (If probability is correct it will give the highest resolution possible). I also know entropy and information of the process. I know how to generate points with specific class. 
- We always have the constraint of time (estimate probability not true one)

2. **NEAREST-NEIGHBOR AVERAGING**: compute probabilities by looking at neighbours and the complexity is reduced with respect to the regression problem. 

## Performance Analysis

Misclassification Error Rate. 
With bayes classifier using true probability function is the best classifier (smallest error)

*example* 
New space separation boundary (**Bayes decision boundary**). The contour where the probabilities of belonging to each of the two classes are equal. (true distirbution). (segmented line)

Using a KNN K = 10 we can define our rule which will be close to the true one, but with some error lying on the boundary. 
The border can be constructed by picking any point in the feature space and draw radius that includes 10 points belonging to one of the two classes. The boundary is where there are 10 points of each class (randomly assigned to either one class or the other). **Majority voting**

K = 10 is powerful even if simple. Border is very close to the optimal classifier. This bounder is called **Piecewise Linear Decision Boundary**. 

Smallest K means that the boder will be variable and fragmented. At worst K = 1 and each point is assigned to the proper class in the training (not test). If K is very high we tend to a straight line since the boundary values will impact a little on the boundary. 

![Schermata 2021-05-17 alle 17.51.52.png](./Pictures/663850af98d049a9a5dca7348194adda.png)

![Schermata 2021-05-17 alle 17.58.07.png](./Pictures/4f8cac1822394340b0353f25946805f6.png)

We can increase or decrease flexibilty as needed. Rule that assigns probability according to majority of points. Bigger neighbour less flexible but more robust. K higher is lower complexity. 

**Variance bias plot**

![Schermata 2021-05-17 alle 18.01.52.png](./Pictures/23614d8032b84d5c8753c10fe9e678fd.png)

18-05-2021

More complex models can describe specific variance in the output as function of the input. More complex model (parametric like in regression) the error rate is going down since it can better describe the training data. However it is not necessarily the best model. We need to test is on new values (X, y) pairs. 

With one variable the representation is simple, when we use two variables the space is threedimensional. We can define surfaces that model the boundary. 
When the task is classification we define class spaces and we have to guess the right class, which is different from all the others. 

The goal is always to understand relationship between variables and output (classes or continuous variable). 

**NOTE**. With the bayes theorem we can find the bayesian border which is the optimal classifier by definition. Every other classifier does less in terms of classification power. All other models are just for the training data but do not precisely represent the underlying probability function which is unknown. 
- Bayes theorem allows us to get the best statistical model (theoretically)
- If we have the underlying probability we can compute the error bayes error. In some cases we can get lower than that error on training but not on test. We must accept some error which is intrinsic. (cannot go further)

Simple model increases variance and decrease bias. We need to start simple but it does not mean that will do well. 

## Separable classes 

Classes are separable when the points we have do cluster in specific region (separable by an hyperplane). Once we represent our data we see that we can represent them in the space (when dimension is low). 

Our visual and perception capability is bound to the 3D, but we have statistics and maths that can expand analysis to higher dimensionality of the output space. 

Stistical Learning is a different point of view of tools that have already been considered a long time ago. We can still go beyond the simple KNN algorithm that is binning the size of the neighboorhood, but does not make any assumption on the relationship between inputs and outputs. Underneath the counting of points we were estimating the probability of having blue or orange. 

## Logistic regression 

The regression problem can be off scale somehow (line non linear from 0 to 1 which is parallel t the probability of having either 0 or 1)
- Saturation at 0 or 1. A trasformation from a line to a sigmoid 
- regression might be good for a binary classification problem however might produce probabilities less than 0 or bigger than 1. 

Note that for multiclass problems the linear regression might not be appropriate since introduces differences between classes and ranks them. 

Example. 

![Schermata 2021-05-18 alle 11.12.07.png](./Pictures/de6c1329503e4748b3f4007bf9354daa.png)

This coding suggests an ordering, and in fact implies that the difference between stroke and drug overdose is the same as between drug overdose and epileptic seizure. Multiclass Logistic Regression or Discriminant Analysis are more appropriate.

Logistic regression 

![Schermata 2021-05-18 alle 11.13.30.png](./Pictures/32aec663103d445a938cfdbf7622cf05.png)

Transformation in a new space of the linear regression line. 

![Schermata 2021-05-18 alle 11.13.55.png](./Pictures/ad6e9f755ca949309a91050081988fd0.png)

The monotone transformation is called the log odds or logit transformation of p(X). The inverse will give the linear regression (reminds of probabilistic framework we can define a model by just creating a regression of the lambda function. Transformation that gives the probability function)

We are going into a space where we go from linear regression to probabilitites. But we can reason also about the opposite by a log transformation so that it si proportional to linear regression. 

Generalized linear model: model probability transformed by a regression. 

Now variables defining lambda and thus define probability. Logistic is like applying a GLM model to our feature space. (Statistical Modelling)

With a statistical model we can measure distances and maximise Likelihood of having a specific class (MLE)

![Schermata 2021-05-18 alle 11.17.40.png](./Pictures/e3948f5d56e747e895c9fdd4d1a58647.png)

Note. Be careful on how we can go back to meaningful representations that allows to understand what is encoded in the model. We need to provide an interpretation of the information that we can measure and record. (sometimes we can rely on machines)

**Logistic regression** can be define from the point of view of probabilities or from the point of view of regression (transformation of probabilities)

![Schermata 2021-05-18 alle 11.22.45.png](./Pictures/f01345490c584eaba2c3c3b0204e5c6f.png)

Note. We want all possible classes to be confronted. (healthy / non healty). Risk factors can be used (variables / features) to raise or lower the probability of having a disease or not. (Database)

The first part is to know what leads to the disease and how to measure those cause (measurements in order to anticipate the rise of the pathology). Gathering data and representing it is essential. We can also guess and try which might be the variables involved and see if we get any relationship. Lower the dimensionality the easier is to control the problem. 
- **Scatterplot**: not portray 7 spaces, but portray two by two and encode in the plot the classes through colors. 
- We can incluse also for classification binary variables that indicate a specific class 
- representing data gives us a lot of information on the relationship between classes and the output (establish rules)
- Objective is to find predicting variables (therapy can include avoiding risk factors)

Logistic regression allows us to go into probability space and gives us betas that gives us insight on importance of variables. We can do anything we saw for linear regression but we are in the probability domain. 

So far we have discussed **logistic regression** with two classes. It is easily generalized to more than two classes. One version (used in the R package glmnet) has the symmetric form. 

## Discriminant Analysis

It starts in the probability space and models the distribution of the classes separately. Observing our set of data and statistically analysis of the prior we already define a possible strategy. 
- Probabilitites that approximate what we see in the data (histograms, probability that match shape)
- flip probability to the data with bayes (p(X|Y) to P(Y|X))
- The prior is know by looking at data. Probability can be plot in the feature space (spikes given the rat was in a specific position). 
- Make the hypothesis on the distribution (Gaussianity) and on independence of classes

When we represent points gathered in the space we can define a gaussian function of the two vars we are portraying. Bayes problem with the class k given a specific observation of predictor (choose points class = k and create probability of having a given x-axis value) 
- We have the prior and what kind of x we measure and we estimate probabilities of having class k given that we are in a specific state. 
- THe highest probability will be the one assigned to the point 

The problem can be simplified by defining a function that links the input and class. We want to have the prior probability of observing each class the same or very similar (more complex when not so). 

When the **priors are different**, we take them into account as well, and compare `$\pi_k f_k(x)$`. On the right, we favor the pink class and the decision boundary shifts to the left.

![Schermata 2021-05-18 alle 11.47.05.png](./Pictures/0f539fe0f9ad4e0d90893242f671b5cc.png)

### LINEAR DISCRIMINANT ANALYSIS

Gaussian density: 

![Schermata 2021-05-18 alle 11.49.45.png](./Pictures/2c4a7139d53f434391d596038b2251a1.png)

and with the bayes formula we get: 

![Schermata 2021-05-18 alle 11.50.03.png](./Pictures/40ab4f4963e643e48df95080da800d0c.png)

probability is a fraction of all the possible classes. That creates a boder line. On one side the probability is higher with respect to the other. 

We can define a discriminant which is the value for which the probabilitites are equal. This discriminant is a linear function of x if we **assume that the variance of each variable is the same**. 

![Schermata 2021-05-18 alle 11.53.02.png](./Pictures/0d7088c50e714cc19895ae15184329fd.png)

delta function = log numerator of the probability we defined is linear with the aforementioned assumptions. this makes the problem easy. For each class we look at how much each class is an output for our data. Prior for y and we compute the mid point of all the outcomes in x. mu_k is the center of the probability in the feature space and sigma is the variablity around the mean. Sigma and mu for each of the classes. 

the border becomes our decision boundary. By definition the Bayes decision boundary is the one that gives the lowest misclassification error among all possible classifier (we can get close with simulations but we do not really know)

[lost 7 slides 91-97]

### Confusion matrix and threshold 
