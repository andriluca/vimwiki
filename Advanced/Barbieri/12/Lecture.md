L12. Cross Validation - Bootstrapping - Trees

# Resampling Methods

Add insights into the model we are using: 

- cross-validation 
- bootstrap

These methods refit a model of interest to samples formed from the training set, in order to obtain additional information about the fitted model.

They provide estimates of test-set prediction error, and the standard deviation and bias of our parameter estimates. 

The **test error** is the average error that results from using a statistical learning method to predict the response on a new observation, one that was not used in training the method. In contrast, the training error can be easily calculated by applying the statistical learning method to the observations used in its training.

- training error rate often is quite different from the test error rate, and in particular the former can dramatically underestimate the latter.

![Schermata 2021-05-18 alle 12.37.46.png](./Pictures/687a278cc97e4f08b54240a4aefa4b98.png)

Here we consider a class of methods that estimate the test error by holding out a subset of the training observations from the fitting process, and then applying the statistical learning method to those held out observations. 
- We want to have reproducible resuts

19-05-2021

# Tree Based methods 

The term Classification And Regression Tree (CART) analysis is an umbrella term used to refer to both of the above procedures, first introduced by Breiman et al in 1984.

Tree-based methods are used for regression and classification. These involve stratifying or segmenting the predictor space into a number of simple regions.

Since the set of **splitting rules** used to segment the predictor space can be summarized in a tree, these types of approaches are known as decision-tree methods. (similar to what Doctors do in their clinical practice)

In a classification problem Y is not representable in a scale. Classes are mutually exclusive. Simple methods (linear regression) can be extendend with non linear terms and the function can be of any type and we have to verify assumption. 

Optimal goal is to predict Y and understand prediction distance to the real value of the outcome. This requires a lot of data (training- testing) 

Pros: 
1. Simple and intepretable (choices that involve only one predictor at a time)
2. Can be combined and multiple trees can be grown to yeald a single output (bagging, random forest)

Cons: 
1. However they typically are not competitive with the best supervised learning approaches in terms of prediction accuracy. (non optimal solution)

Decision Nodes where we compare a threhsold with the input feature and define different branches. At the end of the branch a new decisional node can be defined. 
- Divide feature space with planes (horizontal or vertical splits). As the dimensions increase we go in an hyperplane
- the choices are the results of all possible splits available and the result must be optimized according to the Residual Sum of Squares. 

## Tree Building Process

In theory, the regions could have any shape. However, we choose to divide the predictor space into high dimensional rectangles, or boxes, for simplicity and for ease of interpretation of the resulting predictive model.

The goal is to find boxes R1, R2 .... R that minimize the residual sum of squares (RSS), given by

![Schermata 2021-05-19 alle 11.06.47.png](./Pictures/4551074cf11b416d812ed23460210bd6.png)

This is done in a greedy way. It is greedy because at each step of the tree-building process, the best split is made at that particular step, rather than looking ahead and picking a split that will lead to a better tree in some future step. 
- successive splits are conditioned to the previous ones to reduce computational cost. 
- The tree structure is sequential and there is an hierarcy of choices. (reduce exploration of the feature space but reduces the trainig cost and time) **TOP DOWN APPROACH** 

We predict the response for a given test
observation using the mean of the training observations in the region to which that test observation belongs.

RSS is reduced more and more as we go down the tree. Some splits might reduce more than other the RSS. A new point falls in one and only one region and the outcome is the average estimated in that region. 

## Dealing with complexity (Pruning)
Complexity and variance, bias tradeoff is non avoidable. Overfitting is the risk we need to avoid. 

- A smaller tree with fewer splits (that is, fewer regions R1, R2, Rj) might lead to lower variance and better interpretation at the cost of a little bias.

Early stopping will result in smaller trees but is short sighted. 
A better strategy is to grow a very large tree To, and then prune it back in order to obtain a subtree

Cost complexity pruning, also known as weakest link pruning, is used to do this we consider a sequence of trees indexed by a nonnegative tuning parameter a. For each value of alpha there corresponds a subtree T included in Tc such that

![Schermata 2021-05-19 alle 11.20.58.png](./Pictures/64c0b0085ed34ea98f55d6d16770850e.png)

is as small as possible. 

The tuning parameter determines a trade-off between the subtree's complexity and its fit to the training data. We select an optimal value `$\hat{alpha}$` using cross-validation.
We then return to the full data set and obtain the subtree corresponding to â. (optimizing our choice)

### Algorithm

1. Use recursive binary splitting to grow a large tree on the training data, stopping only when each terminal node has fewer than some minimum number of observations.
2. Apply cost complexity pruning to the large tree in order to obtain a sequence of best subtrees, as a function of a.
3. Use K-fold cross-validation to choose a. :
	1. For each k = 1,...., K:
		1. Apply Steps 1 and 2 on the K-4th K fraction of the training data, excluding the kth fold.
		2. Evaluate the mean squared prediction error on the data in the left-out kth fold, as a function of a.
	2. Average the results, and pick a to minimize the average error. 
4. Return the subtree from Step 2 that corresponds to the chosen value of alpha

As we go down we prune the tree when it does not meet the cost criteria (elbow structure). 

## CLassification tree

A classification tree is very similar to a regression tree, except that it is used to predict a qualitative response rather than a quantitative one. 

For a classification tree, we predict that each observation belongs to the most commonly occurring class of training observations in the region to which it belongs.

In the classification setting, RSS cannot be used as a criterion for making the binary splits

A natural alternative to RSS is the classification error rate. this is simply the fraction of the training observations in that region that do not belong to the most common class:

![Schermata 2021-05-19 alle 11.34.50.png](./Pictures/ad30dd98a87b4e3d8eb7e259a01fec9d.png)

We want the regions to be the purest (separe classes into different branches the most). Maximum with equal probabilities. 
- pmk represents the proportion of training observations in the mth region that are from the kth class.
- classification error is not sufficiently sensitive for tree-growing

## Gini Index 

Measure of total variance accross K classes

![Schermata 2021-05-19 alle 11.36.01.png](./Pictures/b8c36f6faa8e4a468d361553a23348e0.png)

For this reason the Gini index is referred to as a measure of node purity. This means that a small value indicates that a node contains predominantly observations from a single class.

Maximum uncertainty is the maximum Gini Index.

## Cross Entropy

![Schermata 2021-05-19 alle 11.39.08.png](./Pictures/0158ae030afa43eb8537443e2428dd07.png)

Very similar to Gini Index. Measure entropy is essential for classification problems based on probabilities. Hard to estimate probability. 


There is never a method that can be blindly applied to data without knowing anything about the data. 

# Bagging 

Bootstrap aggregation, or Bagging, is a general-purpose procedure for reducing the variance of a statistical learning method; we introduce it here because it is particularly useful and frequently used in the context of decision trees.

Recall that given a set of n independent observations Z1, Z2, Zm, each with variance sigma^2 the variance of the mean Z of the observations is given by sigma^2/n. In other words, averaging a set of observations reduces variance. Of course, this is not practical because we generally do not have access to multiple training sets.

For classification trees: for each test observation, we record the class predicted by each of the B trees, and take a majority vote: the overall prediction is the most commonly occurring class among the B predictions.
- these sets are overlapping and closed related to each other. Datasets are not independent. 
- To increase independence we can apply specific trees (out of bag cross validation)

[not clear this part of bagging]

# Random Forest 

Random forests provide an improvement over bagged trees by way of a small tweak that decorrelates the trees. This reduces the variance when we average the trees.

As in bagging, we build a number of decision trees on bootstrapped training samples. When building these decision trees, each time a split in a tree is considered, a random selection of m predictors is chosen as split candidates from the full set of p predictors. 

The split is allowed to use only one of those m predictors. 

A fresh selection of m predictors is taken at each split, and typically we choose `$m~\sqrt{P}$`, i.e. the number of predictors considered at each split is approximately equal to the square root of the total number of predictors (e.g. 4 out of the 13 for the Heart data).

- Different trees will lead to different choices for each decision node. 
- Refresh subset of predictors for each point (all combinations have been considered) 
- few predictors will have very different trees but will affect information loss. 

Bagging: increase number of trees (specific error with more independent tree). 

Out of Bag (OOB)
Boostrapping 

Best results obtained with OOB and RandomForest
We can get results with varible importance thanks to the purity gini index. The more the regions are pure (lower Gini Index) with given splitting the more those variables are important in the problem. 
