L13. SVM

Find plane that separates classes in the feature space. Go beyond any dimensionality of the input. 

Hyperplanes that are linear function of the predictor space (any dimension). 2D case can be extended mathematically to higher dim spaces. It gives a powerful tool that naturally extends to higher dimensional spaces. 

We do not just compute error separation but also entails how we want the separation (conditions). 
Once we have defined in the prediction space separation we can transform in a new space and go back and fine the separation surface in the orginal space in the predictor dimension. 

When we cannot separe data we can find the trasformation that identifies the space in which data is linearly separable. 


Find a plane in the feature space that is able to separate the classes (binary). In SVM classes are defined -1, + 1. 

In p dimentional space is a linear equation of the features and the betas are the params of the linear regression with an intercept (intercept the origin). Geometric representation is like defining a classification problem as a classification problem. 

Vector beta is the normal vector (perpendicular to the separation plane). The 2D case can be extended to multiple dimensions (hyperplane in 2d is a line in the feature space)

![Screenshot 2021-05-24 at 15.35.23.png](./Pictures/05e238da964e4a8e8890e81845578331.png)

Separating hyperplanes are those which divide the field in two and separate the two classes such that they all fall in one or the other half. This can be defined by a unique formula if we define the classes as +1 and -1. 

y * f(x) > 0 one class and the y * f(x) = 0 is the boundary. 
There is a region in the hyperplane in which the points do not fall (Hard Margin), the points belonging to each class are at a specific distance from the line is the target condition. 

Multiple lines can be drawn to separate the classes. We need to put condition on the beta params. I want the plane that has the biggest gap between the two classes. 

constrain the vector of beta coordinates to 1 is like normalizing. Most of the cases in reality are non simple and there might not be separability. We have to work on that and the principle is to maximize the soft margin. 

Data is never steady and it can be added. Each data that adds to the problem changes its definition. We want to give to our problem  margin of error. We want some noise in the classification so to make the algorithm more robust to noise (allow margin of error in the classifier so to mimic the unpredictable noise)

Maximise margin M by constraining the solution to be unique. We insert an uncertainty epsilon. The more uncertainty you allow the more we can increase its margin. When C is large there is high tolerance for observations being on the wrong side. As C decreases the tolerance decreases and the margin narrows. 

One equation: 

- vector of beta normalized to 1 
- allow certain amount of uncertainty to the problem

![Screenshot 2021-05-24 at 15.47.35.png](./Pictures/698ab68f3bc34ba6babe61c7e8dcb08e.png)

NOTE: sometimes a linear boundary can simply fail. We can consider a new regression in a transformed space and it can be of higher dimension. (transformations)

we go from a p-dimensional to a M > p dimensional space (i.e. polinomial exponential)

Fit a support-vector classifier in the elarged space. This results in non linear decision boundaries in the original space. In the new space operating a separation. We start with simple transformations and complicate as needed. 

In other classification problems we can have separation lines and we have non linear discriminant analysis with curves that are more complex (higher dimensional plane that consider non linearities). 

By transforming we can find region spaces that are more complex. Quadratic region better models. Here we focus on the feature space and see if we can find surfaces that can separate in a better way the data. It depends on the data we have. Shperical transformation requires X1^2 + X2^2 transformation for instance. 

NOTE. We can use kernels and can define inner products and not the single features count, but the kernel (inner product is the projection of each point onto the perpendicular space)


![Screenshot 2021-05-24 at 15.57.30.png](./Pictures/0779ee63e0a0448dbdbe1b5436f15af9.png)

Perpendicular is the normal vector (betas that define the plane) + intercept(distance from the origin to separation hyperplane). Once we have hyperplane we can define distance by projecting each point onto the normal vector w. Distance from plane is distance from vector minus intercept. 

Hyperplane is points with zero distance from normal vector. Inner product is used to grant that all the points with the same class fall in one side of the plane. 

![Screenshot 2021-05-24 at 16.03.02.png](./Pictures/4bb998ff59c845c594c52e64f2fac257.png)

What if we cannot find a separation ? some zetas will be less than 0 and we want to minimize those cases. Find a separation so that we have minimum points that fall on the wrong side. Define loss function: 
- 1 for each point in the wrong side. Loss function will be 0 if points satisfy condition (1 when z<0). Equal weight to any point that do not satisfy condition. 

![Screenshot 2021-05-24 at 16.04.49.png](./Pictures/0f633562e35f468e92b10a250b79e385.png)

To find optimal solution we can minimize the **empirical risk**. Simple problem but there can be a lot of solutions that find the same minimum (not unique). For any two points in very different regions in the other class are weighted in the same way even if they are close to the border (with more noise there is more probability to be on the wrong side). I need to weight the risk by the distance from the boundary. 

HARD MARGIN: normalize by abs value of the distance vector so to shrink our ploblem to the minimum value for our betas. Norm will weight the margin that we have. Shrink or enlarge by operating a change on the norm of w. It is the condition that moves the margin. 

Maximise the margin is equivalent to minimizing 1/2*||w||. 

Different weighting strategy for the points that do not satisfy our condition. We introduce a soft margin when the problem is non linearly separable. Each point that fall on the other side of the margin will have a specific distance. loss function proportional to the distance from soft margin (support plane). 

hinge loss is zero when there is sufficient distance from boundary. 

![Screenshot 2021-05-24 at 16.14.54.png](./Pictures/b67ed308d22a406c9866ee5690a34e83.png)

where l is the hinge loss (see formula on slides). Equation that allows to implement a soft margin linear SVM classifier. 

Support points are key in defining your problem and some other points do not count since they are for sure in the right region (do not play a role in the separation boundary). 

The points that count the most are the onest that fall on the margin. Wrong points are the ones that conunt the most in defining the problem (paradox in a sense) but also the points correctly classified with insufficient margin. (SUPPORT VECTORS)

![Screenshot 2021-05-24 at 16.18.06.png](./Pictures/b7ddfa04245d401aa67ccb0fe4c4d80c.png)

optimal plane is a linear combination of support vectors with ts are a set of indices of support vectors with loss function more than 0. 

![Screenshot 2021-05-24 at 16.21.24.png](./Pictures/6471a046f84f436484172309f563bddf.png)

sum of all inner products but only those that are defined as support vector count as h. We can define the location of the points by setting up the problem as a SVM problem (representer theorem)
- inner product is used as a similarity measure

dange is that we have a powerful tool but at expense of interpretability in most cases (in some they are). 

## Kernels

Non linear SVM consider a non linear mapping. Kernel is an inner product of the transfrom we choose and the optimization problem is a linear combination of kernels (easy to optimize). 

Relationships are within the kernels. The decision function is simply: 

![Screenshot 2021-05-24 at 16.34.19.png](./Pictures/968935a3048e4c65b378873e0c161014.png)

We can use polinomials to define a different kind of space that is linear combination of something very complex (polinomials that might take into account infinite memory). 

Some structures that integrate infromation in a complex way and linearly relate those quantities to solve the problem. Decision function is a combination of kernels. 

- Linear is the one to one transformation 
- increase dim space by the power of the exponential 
- inhomogeneous polinomial 
- gaussian radial basis function (gaussian fields in the SVM space)


![Screenshot 2021-05-24 at 16.35.36.png](./Pictures/62739f8537b24d4ba6daacaf4c12a9a5.png)


Can parametrize the radial kernel with gamma. Still have C parameter and can even use D as the parameter and go to the cube, to the fourth to add complexity. The more we make the problem complex the more there is the risk of overfitting. Complex surfaces but they are at the expense of bias. 

![Screenshot 2021-05-24 at 16.36.38.png](./Pictures/394af0c54f7f4d919fa151d87be05ff6.png)

Multiclassification problem 

One versus all : one class against all the others (I can do it for each of the classes) k classifiers for k classes
One versus One: Increase complexity and have binomial relationship with K classes. 