L14. UNSUPERVISED LEARNING

we do not have a reference label Y (not have an associated response). 

Try and make the most out of the infromation we have at disposal. Problems that deal with information extraction are unsupervised. Look at representations of data that tell us something about our data. Correlate points and make the points to a possible class (assigned by us not by a reference). 

Representing data so that relationships are more evident (space reduction, projection techniques). Allows to see the data that can give quantitative relationships among features. 



**Principal component analysis**: tool used for data visualization or data pre-processing before supervised techniques are applied

**Clustering**: discovering subgroups in data

unsupervised learning is more subjective than supervised learning (need to define a precise goal for the algorithm)
- i.e. group breast cancer patients by means of gene expression measurements 

NOTE. easier to gather unsupervised (unlabelled) data (just monitor choices)

PCA: uses an orthogonal transformation to convert a set of observations of possibly correlated variables into a set of values of linearly uncorrelated variables called principal components (**uncorrelated does not mean indepedent**)

First principal component has the largest possible vairance (accounts for as much of the variablity in data as possible). The others follow the same hypothesis and also must be orthogonal. 

Principal components are the eigenvectors of the covariance matrix (symmetric). 

In the space they spread more in the direction we chose to follow. Maintain max separation that the points have. Separation allows to identify clusters of data better. We want to do that in the dimension in the direction that spreads points as much as possible. 

PCA is related to factor analysis. 

Mathematically we have a linear combination of the feature space and finidn the phis is like finidng the largest spread of the points we have. Normalize the phis like SVMs. 

phis (loadings) > principal component loading vector. Constrain the loadings so that their sum of squares is equal to one

![Screenshot 2021-05-24 at 17.18.25.png](./Pictures/b60fdfb5d18140dc8c26dc046063dc24.png)

transformation vectors (diagonalization) with matricial formulas that link transformation to variance. 

If first components hold the highest variance we can afford to discard lowest components (less information is carried) and we can represent our problem by looking only at the main principal components (loss of information but less dimensionality of the problem)

loading vectors: phis related to the original variables. Loading vector is bidimensional since we held the first two PC. Old coordinates are transfromed into the first and second PC. 

Each state is 1 data point. Normalized. We know the directionality of the old components. Say which are the states with higher murders (projection onto the murder direction)

Now in this new space we can have a portrayal of the data points and original variables. We can group states in the new 2D domain and label them. 

Loading factors can tell us something about data and the features. PCA spreads those vectors as much as possible. If we have two PCAs. One detection the maximum and the other the minimum, with two dims we can simply compute the distance between the points and the plane we use. Distance is whatever the other component is representing (plane and distance from other points)

Modeling effort that minimize distances and use clustering methods to define points with similar characteristics. 

scaling the variables if they are in different units


K means clustering 

random initializations 

hierarchical clustering 
build distances for every couple of points 

dendogram 

![Screenshot 2021-05-24 at 18.00.58.png](./Pictures/424ba374c4344bd69990e20a10ffe0b6.png)
## HIERARCHICAL CLUSTERING 

k means clustering requires to pre-specify the number of clusters K. This can be a disadvantage 

hierarchical clustering is an alternative approach which does not require that we commit to a particular choice of K. 

Bottom up or agglomerate clustering is the most common type and refers to the fact that a dendrogram is built starting from the leaves and combining clusters up to the trunk

Kaplan Meier Survival Curves


