# Lecture 2 --- Adaptive filtering

### Slide 3

Adaptive filtering is different from traditional approaches since they adapt to
the ongoing signal.  It is different from Kalman and wiener, which are
statistical filters. 

Adaptive filters require a period of adaptation since information is detected
from signal (no a priori information required). Faster adaptations might not be
precise enough, but it depends on algorithm used and signal under study. 

### Slide 4

It is useful to reduce noise, enhance SNR when a priori information is not
available. It performs well when signal is non stationary. (frequent in
practice)

Example. In a stroke we have a change from the situation before and after it
and the adaptive filter is able to understand when this change happens.

### Slide 5

The structure of the filter can be: 

- transversal (trasversali) 
- lattice (a traliccio)

Adaptive algorithm can be non recursive or recursive, while the correction
criteria is based on the minimisation of the squared of the output error. It is
the difference between signal and signal without noise. Correction criterion:
output error (output signal - output estimation).

### Slide 6

- Signal to be corrected (any biomed signal)
- $`\eta`$ is the noise
- $`\underbar{w}`$ vector: series of parameters that weight input signal in such a way that
  output is the corrected signal. Adaptation happens by adjusting these
  parameters so to minimise M.S.E. 

### Slide 7

Sequence of signals. j is the discrete time expression.

### Slide 9

To find minimum M.S.E. we have to compute value of the weighting vector
$`\underbar{W}`$ such that gradient is 0.  $`p_j`$ is the cross correlation
between $`d_j`$ input signal and $`x_{kj}`$ reference inputs derived from the
signal itself. 

### Slide 10

Two cases can be considered:

1. *stationary case*: adjustment descending along surface until minimum is
   reached
2. *non stationary case*:  minimum is drifting and the algorithm has to adapt the
   weights such that they track minimum. 

with:

- $`W_{\text{opt}}`$ = inverse of autocorrelation matrix - cross correlation
  signal and noise 
- the $`\underbar{w}`$ vector is the same of h in the Wiener Hopf equation

### Slide 11

$`\mu`$ controls stability and rate of convergence of the gradient. In reality
we do not have $`\nabla_j`$ but we perform its estimation using LMS. 

### Slide 12

In this slide there are the equations to obtain the estimated nabla with
respect to W of the error evaluation used to update the weights. 

### Slide 13

Convergence can be reached when $`\mu`$ is smaller than the largest eigenvalue
of the correlation matrix R and greater than 0. 

To be able to capture all information in the signal we just initialize it. We
just need to perform test after optimisation is completed. We use a priori
information to understand the quality of the approach, but still there is no
need to use precise model and equations (only an estimate is required). 

### Slide 14

## Applications of Adaptive Filtering (Cohen Paper) 

### Slide 10

A. Elimination of power line interference (50-60 Hz sinusoidal noise) $`d = s +
A\cdot \cos{(\omega _0 t + \Phi)}`$   where A, $`\Phi`$ are unknown and s is ECG
without interference.

Through adaptive filtering and tracking of $`\omega_0`$ close to its nominal value
two reference inputs are taken and shifted of 90 degs. Widrow demonstrated that
adaptive filter has notch effect on $`\omega_0`$. 

Note that a band stop filter is computationally comparable to the adaptive
filtering. Here adaptive filtering might be more convenient for real time
applications. 

B. Heart-transplanted patient.  He might require to separate activity of the new
heart from the old heart via catheter in atrium (close to SA node of old
heart). Traditional ECG can be used instead, but it is fundamental to recognise
and enhance recording of new heart (depolarizations). 

C. Decreasing presence of high frequency given by electro-surgical equipment.
When using high frequency generators in electrical knifes to avoid muscular
contraction it is essential to pre-process ECG and EEG signals to maintain
their information content and filtering is not performed when not needed. When
using a reference electrode we could obtain an activation of the filtering of
the knife when it is on. Many systems use adaptive filtering. 

NOTE. When choosing right approach, consider the ease of use and avoid
overcomplicated algorithms when possible. 

## References

Slides: "02 Adaptive filtering", "03 Adaptive filtering Cohen Paper"
