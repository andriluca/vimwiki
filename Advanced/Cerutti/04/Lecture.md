# Lecture 4 --- Quadratic time-frequency


Short time Fourier Transform: It uses time windos with constant duration and
rhis allows obtaining a good frequency resolution with long time windows (bad
time resolution) and viceversa. This is inherently of this method of STFT.

WT (wavelet trasform): time frequency plane is subdivided with STFT in
rectangles of same areas spanning all over time-frequency plane... that allow
saying that along frequency axes we have .... It's a possibility that we have
to use different time and frequency resolution.

Wavelet Vigner Ville decomposition: good time and frequency resolution but
introduces interferences (cross-terms) that make the distribution hardly
interpretable. A good way to preprocess the data.

Time-variant stochastic (or statistical) models: this approach start from the
consideration that we should have in mind the model of signal/noise
interaction. The wide noise introduced in the model can model the signal well
in some occasion. Dependent on the morphology of forgetting factors. It could
work properly following the true signal and not following the noise. The
forgetting factor is the key parameter to provide a good performance of the
model itself. By not having a forgetting factor... This is a parametric
approach in which we have to develop a model of the signal. The simplest model
we can consider is AR or MA or ARMA.

## Beyond Cohen's class

Cohen's class is just a viener ville distribution a bit more complex. There are
similar transfomration that are more used in automation engineering rather than
biomedical. It's much suitable to the considered topic. This approach is not a
blind one (black box) but we need to study the problem at first. We have to
understand what we want to obtain at first. I can compare the different results
of the methods and see what it best embodies the problem with a critical
capacity.

We have a lot of parametric and non parametric approach that we have
encountered.

## References

Slides: "05 Quadratic time frequency"
Paper: "flandin84"
Cerutti's Book chapter: 10
