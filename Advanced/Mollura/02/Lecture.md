# Lecture 2 --- Multivariate causality

## Slide 2

Today's lecture is about multimodal causality but first a recap on spectral
analysis concepts and autoregressive modeling that were already studied in
another courses.

## Slide 4

Let's start from the basis. When we are processing a signal we always have to
remember that this signal comes from a system and this signal is always
modified by the properties of this system. We can imagine that there's an
original signal at the very beginning that is fed into the system we are
studying and that provides us an output which is the input signal changed by
the characteristics of this system. 

The system can be analyzed in different ways, describing properties and
functions. We can imagine to have a model function and that the system is
basically a function that, given an input, produces an output adapted by the
characteristics of this function. 

We can study the properties of a system:
- in the **time domain**: the output as the convolution between the input
  signal and the so-called impulse response (the response of the system to an
  inpulse elicitation in time).  
- in the **frequency domain**: the output frequency representation is the
  product in frequency of the frequency representation of the input multiplied
  by the frequency response of our system (i.e. how a system modulates the
  various frequencies of an input signal).

## Slide 5

Properties. We can use the frequency domain to study the properties of the
Heart Rate signal (i.e. RR signal to obtain the tachogram). 

We can start doing non-parametric approaches: Fourier Transform of our signal
in time: taking some windows in which we assume stationarity (we assume that
the properties of this signal will not change over time in this window), then
we compute the FT and we can get the spectra or frequency representation of our
signal that we all know that in case of HR in the rest-tilt protocol will be
characterized by different power in different frequency ranges

## Slide 6

If we consider the RR intervals' modelization with an autoregressive approach
we can see that, for example, the next RR event can be predicted by linear sum
of the previous events of the same signal. We don't have to forget that we have
always a noise that is present because the reported relationship is not capable
to fully explain and predict the next outcome but we know that there are other
processes and anything else will be considered in this kind of model as noise. 

The autoregressive approach depends on the order of the model (p): how many
previous time instants we want to use in order to predict the next one. We are
basically considering a linear summation of the product of a specific RR
interval at a specific time point multiplied by a coefficient that needs to be
estimated. We can include as many terms as we want ideally. In a more practical
way we have a vector of RR interval. The easiest thing we can do to estimate
such coefficient is to build this system as seen on the bottom of the slides. 

We have the regressors of our linear relationship in the X matrix. As we can
see we have the regressors from 1 to p in the row of the matrix. We multiply
the row by the column of coefficient and it needs to be also summed up with the
noise term. Now we are able to predict the next event that will be p+1 in our
case.

We can build vectors and matrices with the observations that we have. in this
way we can build a column of the next events which will be the outputs of each
of the rows of the matrix X multiplied by the column of coefficient and added
up with the noise. Considering that we want to minimize the noise term we don't
know how much that is but we know that in this very easy linear relationship:
$`y = X\beta + \varepsilon`$ and we can call $`\bar{y} = X\beta`$, an estimation of
the output. So if we would like to minimize this error we can make the
difference between the real output and the estimation. We can then substitute
the estimation in the expression.

The goal is then to find those beta that are able to minimize the noise term
and to put this difference as close as possible to zero. We use absolute
squared error because it's easier to be minimized and allows to have minima in
a way that is easier to handle. What we can end up is the LSE (least squares
estimation). Some assumptions are here made:

1. Gaussianity of the data
2. All the weights of the rows have the same weight

There are many other technique to obtain these coefficients. There's also the
Yule-Walker approach through the ACF and more advanced technique that we'll see
in the next classes.

### Notes

No errors on this slide

## Slide 7

Once estimated the coefficient we can also estimate the frequency
representation of our system which is a function of our coefficients and,
specifically of the FT of these coefficients. 

This is an expression used to estimate the frequency representation of our
signal starting from the coefficients obtained with an autoregressive modeling
approach.

The noise is assumed to be Gaussian with a specific average value and variance.

In this representation we have in the end p roots that will coincide with the
poles of the frequency representation and these poles will give the peaks in
the spectra in correspondance of the estimated frequency where most of the
activity is concentrated.

## Slide 8

Always by assuming stationarity within our window, we can select specific
window and then perform this modeling just by sliding this window and,
therefore, we are able to estimate in a sort of time-variant fashion the
spectral activity in each subwindow. So, with classical autoregressive approach
we will have constraints on the data we are including in the window, in the
size of the window to get correct information in the frequency domain. This
allows up to handle just a bit the non-stationarity problem. We will see next
more advanced probabilistic framework that allows us to refine and to have
better and better approximation of  the processes under study.

## Slide 9

All the information we are using to predict the next RR event are only those
that come from the past RR interval events. We know that physiologically there
are different reflexes that act on the heart beat activity and, for example we
know that the respiration modulates the heart activity through the so-called
Respiratory Sinus Arrhytmia (RSA), a reflex that increases HR during
inspiration and decreases it during expiration due to changes in pressures
during this mechanical effect.

The pressure influences the HR. Indeed there are some mechanical receptors in
the aortic arch that measures the systolic pressure and that then adapt the RR
interval: if the HR is faster the pressure rises and if the pressure reaches
some too high level then there's such a reflex that goes into the autonomic
nervous system and acts on the sinus-atrial node in order to slow down the HR
and to avoid too high pressure.

Probably the model with only RR regressor is not sufficient to describe the
full ANS ans we need to find some other features to improve our prediction
which means then to improve our modeling approach and effort. We need to
decompose the noise in some other primitives.

## Slide 10

What researcher have done in the last years is to have a bigger view on this
system and all the different signals that can be useful to provide an more
accurate output. As we can see in this graph: 

- the ABP and HRV can be modeled as a close loop where one influences the other
  and vice versa.
- Respiratory effect is added that mechanically influences the pressure but,
  through the nervous pathways influences the Heart Rate.
- Central Venous Pressure (CVP) which also modulates ABP and HRV.

This suggests us that we can include more signals to have a bigger picture of
the whole system.

### Note

HRV can be considered the same as RR.

## Slide 11

If we add for example the ABP to our model of the RR intervals. With respect to
the standard AR univariate modeling approach (only one time series of
regressors), we have a sum of two contributions. if taken singularly the first
term is exactly the same as the one seen in the previous example with $`X_1`$
put equal to RR signal. We can add a second time series which can be pressure,
in this example, but anything else we think it might be involved in this
process and we can fit so that $`X_1 (t)`$ can be obtain from p past events of
itself, properly weighted, that contribute to the output and the effect that
the second time series might have in predicting the next event of the first
one. So in this case this coefficients $`A_{12,j}`$ describe this influence.
They are the weights to consider in order to have, when multipling with the
time series, a more accurate prediction. Then we always need to add in our
model the error term. The value at this time point will be the sum of all of
these regressors plus the noise that will be present at that specific time
point. As we observed before we have a close loop and this two formulas are
representing it: this means that we can also model the other branch. We are
modeling the effect that the pressure has on the HR so the central part of the
diagram of the previous slide.

We can also measure the opposite: try to predict the next pressure with its
past values $`X_2`$ properly weighted plus the past event of the previous RR
intervals. We are trying to estimate the effect of one variable on the other
one.

We are looking at what is happening at the same time what is happening on both
time series. We always have to add the noise term of the second time series as
well.

We are assuming that $`X_1`$ and $`X2`$ are linearly dependent and able, in a
linear way to predict the next event and also that $`E1`$ and $`E2`$ are
independent each other.

## Slide 12

Questions directly asked to the students:

- How can we quantify the interaction of a second time series with respect to
  the first one?  What would you do, just with the actual knowledge? What will
  you look at? A: Someone is saying correlation between $`X_1`$ and $`X_2`$.
  It's something that we can look at but it does not quantify the interaction
  but just how much they are related but not exactly how much they interact.
  Someone else said cross-spectrum. Yes this is something that can measure
  interaction. But what could be the actors that allow us to compute the
  cross-spectrum? $`A_{12}`$ and $`A_{21}`$ measure the interaction and as
  someone has told the whole matrix of the coefficient will allow us to compute
  the cross-spectrum. 
- Then if I basically ask you how can we assess whether one variable is causing
  the other one? What is the first characteristic that we can identify also in
  this slide? Someone says to look at the phase of the cross-spectrum. The
  phase helps understanding the shift, at a specific frequency, between the two
  time series. So it can help us but we can cosider something easier. Basically
  the causing variable happens before the effect variable. And we can see in
  the reported formulation that we have something of this concept because we
  are only using the past terms of the second variable to predict the next
  event of the other one. If causation effect exist this effect should be
  present when we try to use past event to predict to the next ones


## Slide 13

How can we estimate a cause-effect when looking at multivariate signals
(signals with different inputs, not only one).

## Slide 14

Causality is important. **Correlation does not imply causation**. We can't
imply that if the two variable are correlates, then one is causing the other.
It is the biggest error we can commit when dealing with data. 

There's a study: Tsunamic effect of data, how much the data are getting into
today's society and all the good and bad analysis done during time. It is
reported an example of two time series: per capita consumption of mozzarella
cheese and the number of doctorates award in civil engineering. The correlation
between these two is very high as shown in the slide. Do you think that there's
a causation effect between these two variable? It's absurd. This is an example
just to show that we can't infer any cause-effect from the correlation.

We need to setup a framework that allows us to say that one variable is causing
the other one.

## Slide 15

Granger is one of those scientists that, starting from the definitions of
Wiener, tried to transform in a practical form all the methods to derive some
indices that allow to quantify the causation. This is important because
investigators want to find the cause of an event, not only to establish links
between events.

## Slide 16

Granger HP: if X Granger-causes Y, the patterns in X need to be present also in
Y but with the presence of a time shift. The two time series need to be shifted
in time and the causing variable should happen before the effect (or caused)
one. 

If such an effect exists, the presence of a time dependency, then we might be
able to predict the next event just by looking at the previous events of X, for
example.

## Slide 17

We already analyzed part of this slide (basically it is summarizing what was
said before). 

The basic definition of Granger causality ... Read.

Granger also speculated this: suppose we have two time series: X and W. We want
to predict the value of X at the next time instant ($`X_{t+1}`$). Granger said
that if we can assume cause-effect we should find something that is able to
predict this next event with only the past events of X and W. We can try with
our estimate, like we did with the autoregressive univariate modeling: we only
used previous RR to predict the next one and then we had the error that can be
assimilated to our W. If I have another variable ($`Y_t`$). Is this variable
Granger-causing X in some other ways? If we setup the problem in Granger contex
we can say that if this relationship exist we should be able to use only the
past values of Y to infer something on the next value of X. We now try to
predict $`X_{t+1}`$ with X, W and Y. Now if we compare the two predictions (the
one with W and the one with both W and Y) what we expect is that if Y contains
some relevant information, because Y is also causing X, if Y contains also
cause-effect on the next event then the second predicion need to be definetly
more accurate than the first one, otherwise it doesn't make sense to consider Y
and we can say that Y is not linked and causing X. By thinking at the first
attempt, when we just had X and W, we are always considering that X will have
some cofficient to predict the next event and W will contain all the other
non-modeled information. What would we expect if we include Y? We are modeling
the next event with something that is due to past events of X and Y. Every
other non modeled effect in W. Should W be bigger or smaller in the second
attempt with respect to the first one? W will be *smaller*: if Y contains
information this should necessarily be drawn from what we were assuming before
were noise. So W, which is the noise, is becoming smaller when we find
variables containing information.  W is a key variable because it allows us to
understand whether something went better or not.

So the concept in this slide is that Granger uses a third variable to assess
causality.

## Slide 18

If $`W_t`$ is larger at the beginning this means that we have many other things
that can take part in this modeling process, so $`Y_t`$ is passing more
stringent criterion. At the end, eventually, $`Y_t`$ might seem to contain
information about the next prediction if we are able to reduce this W. We can
start thinking that a causation effect is present between Y and X also and not
only between the past of X and ???.

## Slide 19

By going back to the bivariate linear AR modeling approach we can say that
$`X_1`$ (RR time series) is basically X, $`X_2`$ (ABP time series) is Y and W
is E, the error term. So if the pressure contains information to predict the
next RR event then the error should be lower with respect to the original
formulation in which we don't include the blood pressure.

## Slide 20

If we now try to better formulate this: If the variance of $`E_1`$ is reduced
including the second time series in the equation then X_2 is said to
Granger-causes $`X_1`$. All the same consideration can be done for $`E_2`$.

This condition is not unique: the joint coefficient $`A_{12}`$ and $`A_{21}`$
must be different from zero, otherwise there's no effect on the second
variable. What this framework allows us to do is also to, then, test with an
F-test the null hypothesis that the joint coefficients are equal to zero. And
we would like those coefficients not to be equal to zero to be really sure that
there's some causality effect. The magnitude of this causality interaction can
be estimated by the logarithm of the corresponding F-statistic. If we are
approaching this framework with AR model we will always need with the choice of
the best model order and so we can then use all the tools (Bayesian Information
Criterion or AIC) to understand whether this order is appropriate or not.

## Slide 21

We can extend this setup with more than two variables by using N variables. We
will look at lagged observations of one and try to see whether these lagged
observations contain information for the next one.

## Slide 22

This framework allows to solve a problem that cannot be solved otherwise.
Let's consider these two different conditions starting from three variables
that are somehow linked by causality.  We have an output variable y and we have
two other variables x and z. The image is straigh forward: we try to measure
for example correlation between y and x and y an z. Correlation does not allow
to understand which of the two links is the object of the analysis: we will see
that y is correlated with both x and z but we are not able to understand, with
such a standard approach, whether x is having an effect on y directly or
whether x is having an effect on z which will have an effect on y (no direct
effect of x on y).  In the case in which there's no link (the one on the
right), this framework allows to understand that, for example, coefficients are
equal to zero (between x and y) so there's no action on y from x and they are
different from zero (in all the other connections). Then we can also
disentangle the influence of x on z and we are able to only catch the inference
of x on y and z on y but, more importantly we are able to discriminate these
two conditions. So this analysis allows us to understand how the network of the
interaction is built in the system under study.

So in these cases we need a multivariate analysis because we have more than two
actors under study.

## Slide 23

Everything that was told before.

## Slide 24

The framework is relying on these assumptions:

1. *Stationarity of the covariance between the various time series*: we can
   build a covariance matrix of the time series if we assume that the
   covariance does not change over time or, at least, in the time window that
   we are considering.
2. This link between variables is linear in our AR modeling approach.

## Slide 25

An extension of this framework was also proved in the frequency domain. Thanks
to Fourier methods we can also look at the G-causality in the spectral
domain and analyse this causation effect also frequency by frequency where we
can then estimate also how much of a fraction of the total power (the
information we have in the spectrum: the variance decomposed frequency by
frequency) in $`x_1`$ is due to an activity at that frequency for examply by
$`x_2`$. This is a powerful tool in both physiology and neuroscience.

## Slide 26

In this lecture we will look at other indices that are particularly useful in
this framework in the frequency context. Here are reported many references and
studies that explore and allow the progression of this formulation.

## Slide 27

These are the main topics:

1. GCI: Granger Causality Index.
2. DTF: Directed Transfer Function.
3. PDC: Partial Directed Coherence.

## Slide 28

Let's extend the AR modeling approach and extend it to the multivariate case.
We don't have only one or two series but we can assume that x are matrices of
many different signals. A is a matrix. We can build our Multivariate AR model.
This is a more condensed and elegant form to build up the model with many
channels. 

What we can do is to compute the FT and, due to the properties of FT, end up to
a more condensed formulation where now the independent variable is the
frequency.

How to obtain the frequency domain formulation: move the X dependent term on
the left and group by X. Fourier transform both the membrers. Then by
performing the inversion of the coefficient matrix A we are able to describe
the transfer function of the system when the input is a matrix of noises and how
this noises are modeled by the transfer function of the system to give us the
different signal that we record.

## Slide 29

This is a bivariate example in which we can see the matricial form for x,
coefficient a and the error w. In this case we are using RR intervals and the
sistolic blood pressure. p is again the model order.

### Notes

**Typo**: instead of Y[n] we should consider X[n].

## Slide 30

if we compute the FT we end up with this system here reported and we can put
this on the left and then this is like multiplied by the identity matrix and we
can do the difference between the identity and A matrix, then we compute the
inversion and we obtain the transfer funtion. Here we can see the model that
shows how this relationship is described. We can see that $`A_{12}`$
coefficient allows to define the coupling between the second series with the
first one, then this rows is normalized by the actual effect of the first
signal itself, plus the contribution of the noise. The same for the feedback
brench. In this contex all these coefficients are in the frequency domain so
what we did is the FT of the coefficients.  To obtain the result we just
compute the product between the matrix A and the vector x. Bring all the terms
to isolate to the left, gather and then obtain the formulas shown below.

Basically the cross-terms are identifying the influence of a different variable
on the one in analysis.
$`A_{ij}`$ with:

- i: the index referring to the influenced variable; 
- j: the index of the influencing variable

## Slide 31

As we were saying we compute the inversion of the A matrix and we are able to
model the two output singlas as a modulation of two uncorrelated noises as
input. These noises are modeled by the system transfer function H. Indeed as we
can see we have basically the values of each of these terms.

## Notes

Demonstration (as an exercise):

$`\begin{bmatrix}
	\text{RR}(f)\\
	\text{SBP}(f)
\end{bmatrix} =
\begin{bmatrix}
	A_{11}(f) & A_{12}(f)\\
	A_{21}(f) & A_{22}(f)
\end{bmatrix}
\begin{bmatrix}
	\text{RR}(f)\\
	\text{SBP}(f)
\end{bmatrix} +
\begin{bmatrix}
	w_{\text{RR}}(f)\\
	w_{\text{SBP}}(f)
\end{bmatrix} `$

$`\begin{bmatrix}
	1 & 0\\
	0 & 1
\end{bmatrix}
\begin{bmatrix}
	\text{RR}(f)\\
	\text{SBP}(f)
\end{bmatrix} =
\begin{bmatrix}
	A_{11}(f) & A_{12}(f)\\
	A_{21}(f) & A_{22}(f)
\end{bmatrix}
\begin{bmatrix}
	\text{RR}(f)\\
	\text{SBP}(f)
\end{bmatrix} +
\begin{bmatrix}
	w_{\text{RR}}(f)\\
	w_{\text{SBP}}(f)
\end{bmatrix}`$

$`\begin{bmatrix}
	1-A_{11}(f) & -A_{12}(f)\\
	-A_{21}(f) & 1-A_{22}(f)
\end{bmatrix}
\begin{bmatrix}
	\text{RR}(f)\\
	\text{SBP}(f)
\end{bmatrix} =
\begin{bmatrix}
	w_{\text{RR}}(f)\\
	w_{\text{SBP}}(f)
\end{bmatrix}`$

$`\begin{bmatrix}
	\text{RR}(f)\\
	\text{SBP}(f)
\end{bmatrix} =
\begin{bmatrix}
	1-A_{11}(f) & -A_{12}(f)\\
	-A_{21}(f) & 1-A_{22}(f)
\end{bmatrix}^{-1}
\begin{bmatrix}
	w_{\text{RR}}(f)\\
	w_{\text{SBP}}(f)
\end{bmatrix}`$

$`\begin{bmatrix}
	\text{RR}(f)\\
	\text{SBP}(f)
\end{bmatrix} =
B^{-1}
\begin{bmatrix}
	w_{\text{RR}}(f)\\
	w_{\text{SBP}}(f)
\end{bmatrix}`$

$`H(f) = 
\begin{bmatrix}
	1-A_{11}(f) & -A_{12}(f)\\
	-A_{21}(f) & 1-A_{22}(f)
\end{bmatrix}^{-1} = \boxed{\dfrac{1}{\text{det}B} \cdot
\begin{bmatrix}
	1-A_{22}(f) & A_{12}(f)\\
	A_{21}(f) & 1-A_{11}(f)
\end{bmatrix}}`$

## Slide 32

What changes is that we switched from one model representation to another one.
Obviously the model is the same but the representation is different. 

- Model representation as function of the A coefficients. Here we have a
  bivariate close loop model where the contribution of each of this is as a
  function of the A coefficient so a more direct approach. We have the cross
  terms that are at the numerator that are linearly linking the effect of one
  variable to the other and there's also the modulation of the different noise
  terms.
- Model representation as function of h coefficients. Considering noise
  generated model: we assume that our model is stimulated by two uncorrelated
  noises that will interact with the system, generating their own responses.
  One of the brenches is due to the action of the system on the noise itself
  which gives one part of the variability of the two signals but also,
  considering the interaction between these two noises is taken in
  consideration with the cross-terms h coefficients that then will contribute
  to the two variances. So basically, considering the first branch linked to
  $`W_{\text{SBP}}`$, this is how the noise at the origin of the blood pressure
  series add variance in the noise that generates the RR series.

## Slide 33

H(f) matrix allows us to compute the cross-spectrum. Let's now look at the
matrix $`\Sigma`$: we are assuming that the two noises are uncorrelated at the
beginning (no cross interaction at the beginning) but there are only the two
variances that then in some ways interact each other due to the system. 

With the first formulation we are able to compute the matrix below where we can
see the autospectra of RR and of SBP on the main diagonal and and also the
cross-spectrum and its conjugate on the other. As we can see the autospectrum
of the RR is basically due to the activity of RR signal itself and a
contribution due to the other signal. (as we also observed before). The
opposite for SBP is also true. instead the cross-spectrum is basically due to
the interaction of these coefficients each with a respective noise at the base.

### Notes

- There's a typo in the matrix $`\Sigma`$: we are considering variances inside
  this matrix so instead of $`\sigma`$ we have to substitute $`\sigma^2`$
- A second consideration can be done: we can consider $`\sigma_{\text{RR}} =
  \sigma_{1}`$ $`\sigma_{\text{SBP}} = \sigma_{2}`$. In this way the matrix
  below is better showing the influences of noises with respect to the
  reference position.

## Slide 34

In the past, through these measures, different indexes that indicates the
relationships between these variables were hypothesized. 

- One of this is simply the ratio of the auto-spectra under the square root
  because the spectrum is a squared measure: spectrum represents the variance
  and if we want this quantity in some sort of normalized unit of measures we
  need to compute the square root. This first index is balancing the effect of
  the auto-spectra of the RR with respect to the systolic blood pressure
  frequency by frequency, so we are looking at an open loop gain of these two
  indexes. 
- But here we are not taking into account the fact that there are also some
  cross-terms that are more related to the interactions so another index that
  was developed is the second one we can see in here which is basically the
  ratio between the cross-spectrum with respect to the SBP: how much of the
  cross interaction remains when we normalize it by the effect of the
  SBP.

## Slide 35

We can also compute the coherence as the the squared cross-spectrum normalized
by the two auto-spectra. This number will always be less than one because the
product of the auto-spectra will always be bigger then the squared of the cross
spectrum because the cross-spectrum includes part of the auto-spectrum of RR
and part of the auto-spectrum of SBP to describe the interaction but the whole
activity of the two series will be always greater than this. This let us
understand that the second index we have seen before will always be less than
the one seen before, smaller than the other one.

In order to compare these two indices we have to square them.

High coherence tells that at that specific frequency the interaction is strong.
the assumption is that we are able to look at linear interactions with this
framework and the coherence gives us an estimation of how strong this link is,
how much this connection is (linearly) present at that specific frequency.

Low coherence (=.1) means that either no linear connection is present between
the two at that frequency or the connection is not linear but we don't know
whether there's some non linear interaction at that point.

## Slide 36

This is exactly the same slide as before. Looking at the same model with
different perspectives that give us different insights.

## Slide 37

What we can also compute, in terms of indices to describe our system, are the
so-called directional gains. Thanks to our close loop approach we can compute
the amount of gain in one direction and in the other one. A directional gain is
embedding the information on how much interaction is moving from variable 1 to
2 or from 2 to 1. There are two formulations, both of them are based on the use
of cross-terms: 

1. Strength of the activity that grows from 1 to 2 is basically the cross-term
   $`h_{21}`$ normalized by the activity of the first signal itself.  In this
   way we are able to understand only the added contribution due to the second
   signal. 
2. here we come back to the formulations in the bivariate close loop
   representation. if we then substitute we can draw this interaction also with
   this representation.

Something interesting to observe is the symmetry of the notation.

## Slide 38

We can use both parametric and non-parametric approaches to compute the
auto-spectra and cross-spectrum. Each auto-spectrum is the sum of all the
contributions that come from both the input noises. That's why the product of
the two auto-spectra is always less than the squared value of the
cross-spectrum (??? it seems contraddictory).

### Notes

The last sentence is not so clear. Even in the previous slide Mollura said the
same but I don't understand how the ratio could be less than one with these
conditions.

## Slide 39

The previous directional gain can be more correctly adapted if we do some
consideration. We can use the causal Gain identification: we know that some
variable is causing some other and we can correct this terms by computing the
ratio for the 2-to-1 directional gain (it should be interpreted as the amount
of gain that from SBP goes to RR): 

- *numerator*: We start from the whole RR auto-spectrum and we remove its own
  effect (aka self gain, all the activity due to itself). We are left with the
  activity (variance) due to the interaction (i.e. only the contribution of SBP
  to the RR)
- *denominator*: total activity of SBP to which we subtract the activity (the
  variance in the SBP due to the RR) due to the RR.
  
We now can use a more intuitive formulation the same indices we had before.
Similarly we can analyse the opposite equation.

- $`G_{2\to 1}`$: SBP to RR so this is a baroreflex
- $`G_{1\to 2}`$: RR to SBP so this is feedforward gain.

If $`h_{11} \geq h_{21}`$ then $`\alpha_c < \alpha_{o}^I`$: The close loop
index will be smaller than the open one. That's because the open one does not
take into account the cross-interactions but it just computes the ratio between
the two but it does not subtract the other terms. Indeed if we don't consider
the subtractions at numerator and at denominator we would obtain exactly
$`\alpha_{o}^I`$ we have seen in the slide 34. Our directional gain also comes
from the consideration and written to be compared with the open loop gain where
only the auto-spectra were considered and then we remove what we know it's not
what we are really interested in, it's not really the interaction in this
direction.

## Slide 40

This is the general formulation for our directional gain. Read as is.

## Slide 41

Granger causality can be quantified by the logarithm of the residual variance
without the new series with respect to the variance when we add the new series.
Here we have the same formulation.

- $`\sigma_{X|X}^{2}`$: residual variance when we consider only X.
- $`\sigma_{X|X+Y}^{2}`$: residual variance when we consider X and Y, so when
  we are adding information to the model.

### Why logarithm?

What happens to this index with a logarithmic transform applied to this ratio?

If the ratio is 1 then logarithm is zero. This condition is happening when
$`\sigma_{X|X}^{2} = \sigma_{X|X+Y}^{2}`$, so when the residual explained
variance is eual between a model that does not consider another explanatory
variable. If the explained variance is not changing no further information is
introduced with another variable.

What will happen to the index if the residual variance at the denominator is smaller?

The index is greater than zero. If the variance at the denominator is
increasing, something is not going on correctly in the model. If this increases
the ratio becomes smaller and less than 1, leading to an index that is
negative. Basically the model has something that is not correct in this case. A
negative causal index means that probably the causality is reversed. What we
are sure is that if the variance decreases when we add the variable, this ratio
is greater than 1 and the index is positive. We are quantifying this causality
and we can say if the causality is wrong or right with these values.

We can also apply a minus [sono arrivato qui]

## Slide 42

We can also apply a minus sign in front of the logarithm exchanging numerator
and denominator but the two different notations are the same.

If we consider a bivariate gaussian case we can decompose the residual
variances with the terms that we already know:

- Auto-spectrum
- variance of j
- variance of i
- covariance of ij
- cross-interaction terms from j to i.

If we multiply we can see that at the denominator we have the auto-spectrum and
at the numerator we have auto-spectrum minus the cross term contribution.


## Slide 43

if the noises are independent we don't have the covariance (because it is zero)
and the formula simplifies. We have the logarithm of the ratio of the
autospectrum (residual variance with only x) divided by the auto-spectrum minus
the contribution that we are now able to quantify from the second signal. This
gives us a measure of Granger causality.

## Slide 44

We extend the bivariate analysis incorporating a new signal. If we then include
other signals what we were looking at untill now becomes difficult to
understand the power of one signal, in terms of causation effect, with respect
to the others. That's why we subtract anytime the different contributions.


## Slide 45

What we can include is the presence of a third signal when we always look at
the interaction between all of these. Moreover we want to understand what is
the residual variance of two signals when we are considering the presence of a
third one. We can see how we can compute the cross-spectrum between x and y
when we include the information of a third signal z that is defined by the
subtraction of the whole cross-spectrum and the contribution of the
cross-spectra with the first-third multiplied by the cross-spectrum of the
second-third normalized by the auto-spectrum of the third signal. This is a way
to correct the cross-indices when we include a third variable in our analysis. 

- Partial cross-spectrum: The cross-spectrum of x and y given that we know the
  influence of a third signal z: The cross-spectrum has to be reduced by an
  amount that includes the contribution of this third signal, which is
  basically the product of the cross-spectra normalized by the auto-spectrum of
  the z signal. "Partial" because we are looking only at a partial cross
  relationship giving that we removed contribution of a third one.
- Squared partial coherence: partial cross spectrum squared at the numerator
  and we normalize it by the auto-spectra given the information that we have on
  z. So also this auto-spectra are not the full autospectra but without the
  ocntribution of z to x and the contribution of z to y. Partial coherence
  because it is the coherence of x and y given z.


## Slide 46

A pretty explicative slide that explains what said.

1. read as is.
2. If three signals are fully coherent with each other, partialization between
   any two signal and the remaining will result to a zero coherence because
   they are all fully coherent and, when you subtract the new term, what
   remains basically it's nothing because the system is a whole (it is a fully
   coherent system). If the remaining coherence is different than zero this
   means that part of the coherence is due to the third signal and another part
   is present and not shared also by this new signal.
3. Read as is. If Z influences the interdependence between X and Y by actually
   studying the partial coherence, in particular it is verified if: $`k_{xy|z}
   (f) < k_{xy}(f)`$.
4. We are always looking at a linear interdepencency. Obviously non-linearity
   can have different contributions that we are not able to look at whith this
   framework because the linearity assumption will always need to be satisfied.

## Slide 47

1. Read as is. Granger definition can be extended to multichannel system and we
   can include as many channel we need.
2. We need to quantify the influence of signal j to signal i for n channels.
   We can approach multivariate autoregressive models to do this.
3. What can be done is to compute the granger causality index by fitting the
   entire n-channel system and explore how the variance changes when one is
   removed. We fit the whole system and we have the residual variance, then we
   feed the system without one channel and we should have an higher variance.
   We can then compare these two and understand whether a variable is adding
   information to the system or not. The residual variance is the variance of
   the error considering n-1 channels.

## Slide 48

We look at the Granger causality index from j to 1 by computing the ratio of
the variances when the model contains all the n channels with respect to the
model which is not presenting one of this (in this case the j-th channel). We
compute the log of the ration with the minus sign or not depending on how the
ratio is built.

Issue: we can measure the causality for index j to index i and this is straight
forward for a bivariate system because that Granger causality index will be
directly compared to the other one because we have only two actors in the
system but how can we compare the strength to this coupling with all the other
indices? now we remove only one at a time how can we have a reliable measure?

## Slide 49

What we can observe to solve the problem is that the approach of the DTF
(directed transfer function) which basically allows to describe the causal
influence of channel j on channel i at a specific frequency by normalizing by
the overall activity on the same channel. This means that here we are computing
the ratio of the influence of channel j to i normalized by the sum of all the
influences that all the n channels from 1 to k have to i. We obtain a fraction
of the j to i effect. In this case this ratio will have values that ranges from
0 to 1. $`\text{DFT}_{j\to i}^2 (f) = 1`$ when the other channels does not have
any contribution and so the j to i contribution is the only one from which we
will divide to obtain a ratio equal to 1. Differently, this index will range
from zero to one. This value can be thought as the flow from channel j to
channel i. 

### Notes

$`H_{ij}`$ is referring to the coefficient of the matrix we saw before.

## Slide 50

This allows to better understand: if we look at a system with three channels
and we have 3 noises acting on $`X_1`$. If we are looking at channel 1 (H_{ij}
with i=1). We want to know how much of the signals are acting on X_1. Basically
we compare $`H_{11}, H_{12}`$ and $`H_{13}`$ terms. If we want to know the
strength of the flow that goes from channel 2 to channel one we basically have
to consider the ratio between $`H_{12}`$ normalized by the sum of the three H
belonging to the level i=1.

This index makes use of the noise generated model (H coefficients we saw
before).

### Notes

Not necessary

## Slide 51

This slide summarizes really well the operations to be performed to quantify
the interaction.

## Slide 52

We can analyze the inverse flow if we use the A coefficients. This is called
partial directed coherence (PDC) from signal j to signal i. In the same way we
can estimate this index by the ratio of the A coefficient from j to i
normalized by the squared root of the squared sum of the whole actions that j
has on all the other signals. Partial directed coherence represent the relative
coupling strenght of the interaction of a given source (j) with respect to
another signal (i) when we compare this to all the connections that j has with
all the other signals. Also in this case this, the index ranges from 0 to 1.

## Slide 53

Not necessary

## Slide 54

Let's go back to the multivariate closed loop model. Now we look at channels 2
and we want to quantify how much this channel is coupled with the others. In
this case we can compute PDC as the coupling between channel 2 and channel 1
wrt all the other couplings that channel 2 has (with channel 2 itself and with
channel 3).

### Notes

I don't think the slide is correct: the variable x should point to the A
coefficients but it makes more sense to have the other signals nearby, not the
noises.

## Slide 55

All of these indices is dependent on the model that we are developing and on
the reliability of the model (e.g. order, on the way in which parameters are
estimated, on different durations of the epoque lengths we consider to fit the
model). If we have a model order that's too low we are not able to describe all
the relationships and so we are not able to capture the full dynamics of the
system and all the oscillations that are part of the system. So we are
suffering of the underfitting problem. If we use a too high order the system is
very good in fitting the observed data but has a lot of problems in
generalizing the data and so we are always striking with a problem that is
called overfitting and also in stability: if we have too high an order we are
not able to correctly estimate the model parameters which may go to zero and
cause problem of instability as well.

## Slide 56

Here is a link where there's toolbox for the multivariate granger causality.

## References

Slides: "02_ASDPM_2021_Multivariate_Causality"
