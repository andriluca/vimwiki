# Lecture 3 --- Higher order spectra

## Slide 3

So far we studied all the properties of the power spectrum and the abilities of
this approach in many different biomedical applications, starting from EEG to
the HRV, where all these processes are made out of oscillations that are
characteristics of the system that is under study. These oscillations are
generated by the system and these resonances are those things that allow us to
live We know that EEG with the main bandwidth that describes different mental
activities while instead the HRV has a selection that describes the autonomic
activity and control of our heart.

The power spectrum became a very popular tool in biomedical signal processing.
We always have to deal with the problem of non-linearities: we are able only to
understand linear correlation and linear links within the signal and between
different signals when we approach multivariate systems. One way to deal with
non-linearity is the one with the Taylor expansion: we can imagine that, for
example, we look at quadratic forms of our signal and try to analyse in a
linear context some non linear events. What we will look at is how to try to
broaden the view to include in a linear context some non linear interactions
that may happen in our system.

## Slide 4


Let's consider this example.  We can have some non linearities in our system
like the square value. When we have an input signal which is a sinusoidal one
this very easy non stationarity will produce different behaviors (will give
origin to different frequencies with respect to the one we are dealing). For
example we have an input activity at frequency $`\omega`$ and then with a
square effect, due to goniometric functions, we can say that we have some
shifts and when we have two sinusoidal effects we have also a linear
interaction between the these two. In this case a non linear effect will
produce new linear interactions between these frequencies, so new frequencies
in our output will be generated with a very simple non linearity of our system.

## Slide 5

We will see how we are trying to deal with the limits of the classical spectral
analysis which is not able to discern between these two conditions in which we
have three sinusoids where, in both the cases $`\omega_a`$, $`\omega_b`$ and
the sum of the two:

- Made up by the interactions between the two. In the case on the left we have
  only two phases, then the phase of the final signal is the sum of the two.
- Not made up by the interactions between the two. In this case we have another
  phase ($`\phi_c`$)

There's something in the undergoing models that makes them differ but
with the classical power spectrum approach we are not able to distinguish these
two conditions. We will see that especially if $`\phi_a + \phi_b = \phi_c`$ we
will have two models with two different mechanisms that apparently behave in
the same way. So we try to overcome this issue with different paradigm of
spectral analysis.

## Slide 6
## Slide 7
## Slide 8
## Slide 9
## Slide 10
## Slide 11
## Slide 12
## Slide 13
## Slide 14
## Slide 15
## Slide 16
## Slide 17
## Slide 18
## Slide 19
## Slide 20
## Slide 21
## Slide 22
## Slide 23
## Slide 24
## Slide 25
## Slide 26
## Slide 27
## Slide 28
## Slide 29
## Slide 30
## Slide 31
## Slide 32
## Slide 33
## Slide 34
## Slide 35
## Slide 36
## Slide 37
## Slide 38
## Slide 39
## Slide 40
## Slide 41
## Slide 42
## Slide 43
## Slide 44
## Slide 45
## Slide 46
## Slide 47
## Slide 48
## Slide 49
## Slide 50
## Slide 51
## Slide 52
## Slide 53
## Slide 54
## Slide 55
## Slide 56
## Slide 57
## Slide 58
## Slide 59
## Slide 60
## Slide 61

## References

Slides: "03_ASDPM_2021_Higher_Order_Spectra"
