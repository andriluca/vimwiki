L9. Cardiovascular Modeling

10/05/2021

# HRV ANALYSIS 

Framework of the Point Process Modeling approach to model heartbeat dynamics. 

HR: number of r-wave events per unit time 
HRV: variation in RR intervals or HR with time. Variability is the way our system tries to compensate changes in order to maintain BP and other variables constant. 

Autonomic Control of the Heart (SA node where all the activity starts). Through the process of IF of cardiac cells starts the depolarization of artia and then ventircles. ANy contraction will pump blood in our arteries and an increase in pressure is measured as a control system that can be schematized. 
- **Sympathetic**: slower response (5 sec)
- **Parasympathetic**: faster response (1/2 beats)
- **Blood Pressure**: baroreceptors in the aortic arch and carotid which send a message up to the brain (baroreflex branch). Too high pressure will lead to a reduction in the activity of the heart (BP to be constant and ensure oxygenation of the periphery). Baroreceptors send a message up to the brain through the baroreflex branch that quantify how stretched are our vessels. Too high pressure will lead to a reduction in the HR activity in order to maintain pressure and ensure oxigenation. 
- **Humural factors** (termoregulation) 

Brain will respond in order to ensure best cardiac output for any specific condition (oxygenation level of our tissues). Keep constant oxygenation level. There are also humoral factors and thermoregulation. The brain will respond in order to provide best cardiac output depending on the condition. Oxigenation level will change the heart activity in order to maintain a constant oxygenation level. 

Activity is extracted through QRS complex that can be best spotted, P wave should be the reference since is contraction of atrium. The QRS is used to detect R peaks and Heart Beats. It should be the p wave the reference but it is more difficult to detect. There are a lot of studies that demonstrate HRV as a clear signal to discriminate pathological condition in feedback control (ANS or other involved systems)

There are a lot of studies that focus on issues in the feedbacks of these systems at the expense of proprer regulation. Important to spot them at rest for better identification and treatment. (non invasive measurement)
- A lot of effort in modeling approach (add more signals to have broader prespective and better represent the system)
- Assess state of the subject

under different conditions we can identify a different activity in the frequency domain of the HRV. We can apply the point processing modeling to a cardiovascular control system. Characterize the response we can identify: HR, respiration and ABP. 

![Schermata 2021-05-29 alle 11.17.01.png](./Pictures/8ed4a037a14349589f66b90dd0c32ac6.png)

CVP has an effect on the modulation of the respiratory activity on the SA node. There is a lot of work on the acquisition of the signal (non invasive) and in modeling the causality and non stationarity of the HRV signal. 
In clinical settings we try to characterize the state of the patient by looking at the HRV spectrum. 

## Blood pressure control
Point processing modelling to a cardiovascular system. 
- HR 
- ABP 
- Respiration 
- Blood Oxygenation

Three actors in the model and have two branches that represent: 
1. Sympathetic
2. Vagus

There are also hidden dynamics on venticles and vasculature that show to play a role in several clinical scenarios. Simplification is required when looking at the broader picture. 

Gains that quantify ABP role in modulation HR and respiration that acts through different changes in pressure and modulates activity of the heart. Identify interval between RRs and we can retrieve HR = 1/RR* specific factor. 

RR interval is possible only when we have next event. In order to process it it is usually done **Resample RR series**. (change the structure of HRV since we modify specific time strucutre defined by an integrate and fire model of the pacemaker cells) RR series when resempled will modify this very specific time strucutre which is representing the activity of the pacemaker cells. 

**Sample sinusoidal respiration** in the correspondence of each R beat so we have the same number of events and respiratory points 
- 0.3 Hz - 0.4 Hz is the frequency affected by HR. We are correctly sampling respiratory signal and AR(8) model is estimating in a good way most of the activity. 
To include respiration we record the level (lung expansion or volume) at each heart Beat (same number of respiratory points). 

To include **pressure signal** a fiducail point is identified. For each beat a systolic and diastolic events are identified. Baroreflex uses Systolic while we use diastolic to study HR effect on BP. From the fiducial points we can extract the systogram (series of systolic events)

 With all these signals we have a **better picture of the physiology** involved in the definition of the HR. We need to tackle changes with a time variant approach. AR modelling on a small window and move it along time. 
![Schermata 2021-05-29 alle 11.25.48.png](./Pictures/0aa7b6e4961a48e2a25a8f1d52492c67.png)

![Schermata 2021-05-29 alle 11.28.42.png](./Pictures/dc327477d819432f9e91205d0f42f1f9.png)

However we can do something more accurate. **We can hypothesise that there are probability distributions behind HR** and we can estimate it with an higher temporal resolution that would allow us to estimate the spectral parameters. 
- time variant approach 

## Probabilistic Modeling Approach

Autonomic activity can be estimated in order to provide a continuous estimation in time of the Heart Rate. Barbieri focused on modelling the SA node in continuous time. 

**Point Process Approach** might be the correct one since we can imagine that we cannot have overlapping events in the same window. Physiologically impossible to have two consecutive beats too close to one another. 

History of past events could be used to estimate next event and the value in continuous time of HR (probability of having a beat for every t_k + n). 
We would like to define a probability region where we can say that there is high probability that the next beat will occur. The estimate can be updated with new time points and it can be adjusted. 

## Encoding Step
Define the probability of having and Heart Beat given the state (for us is the Autonomic Nervous System). Probabilities for each event (interbeat activity) and once we have defined the probability as a function of the identity we can choose to estimate probability and compute Conditional Intensity Function. 

We now are **able to estimate in a continuous way our signal** and it was not so easy before. We can setup any other estimation problem with a solid hypothesis and we can also **quantify statistically the goodness of our model (goodness of fit)**. Unitl now the only way to assess the validity of a AR model that works at a beat level was to look at AIC and BIC and look at ACF. 

**Interbeat Interval Probability model (IBI)**. Inverse gaussian probability function depend on the average distirbution RR interval (value around which there is uncertainty) and Skewness factor. 

Estimate time - previous event u_k - average distribution is used to catch the dynamics that define this physiological signal.

After several apptempts they found that the average can be modeled as an AR. More parameters are added to better estimate distribution. A lot of literature demonstrated that AR model could be well estimate Autonomic activity and Spectrum. History is represented by this AR model. 

Estimate probability through the use of the conditional intensity function. We can quantify wether the model is good or not (order and other params). 

## RR intervals into Heart rate variables
Inverse of the distance from the last event and scale by a factor c that corrects for different unit of measures. Expression of the two params in terms of HR and not RR intervals. 

Until now we fit our model using an inverse gaussian. We know that our params rely on a solid statistical distirbution representative of the underlying physiology. We can estimate the frequency domain params by using the AR coeffs. 
These models can be extended in order to catch structures that are more complex and we can then add to classical AR terms. 

With such a multivariate model we can estimate all cardiovascular indices: 
1. Baroreflex 
2. RSA
3. ....
4. ...

(....complete)

## Tetravariate Model 
Estimate closed loop action of pressure and RR and viceversa. Other variables can be also added like the pulse wave velocity which is the time required by a pressure wave to travel from one to another district and it is an estimation of the pulse wave that propagates in artheries innervated by sympathetic nerves. We could be able to quantify the activity of the nervous system on the vessels from the activity of the Heart. Modulation of the heart might be healthy but once the system does not work the recorded signals will not be normal anymore. 

Pulse arrival time is the distance between R event and the diastolic or systolic event. Pulse transit time can be approximated by the distance between beat and pressure measure. As we can image there is an interval in the Impulse arrival time that includes the time during which the heart pumps and the blood goes out of the heart (pre ejection period). 

Finally, self history of all signals can be used to predict and build the model. 120 mmHg at previous beat it cannot be dramatically different. 

Probability density functions: 
- RRI, PTT: inverse gaussian 
- RSP, SAP: Gaussian 

they can be approximated with a IF model. Respiratory activity and systolic activity require a gaussian distirbution and no IF modeling can be used. 

Thanks to the TF (hp of gaussianity) we multiply the covariance matrix (hp diagonal) to understand cross terms distributed. 
Coherence can be estimated from one signal to the other (lesson 2). 

# NON-LINEAR AR MODEL 
New space that allow us to catch more complex dynamics. If we use the AR **Volterra Series** we can approach non linearity. 

The expression is mainly the product of n observations in addition to a standard AR model. Each product is weighted a specific coefficient (h coeffients to be estimated) that maps output measure to its new space. The space has higher order. 

Go beyond classical linear model and we have a second order term. 

There is also the **Laguerre Expansion** which has quadratic expantion (laguerre Filter). The advantage is that with this model we estimate the response of a corresponding model with the whole story of past events. With proper kernels we can avoid the problem of limited history. 

We can use these coefficients to estimate the spectrum. With higher order statistics we can compute different indexes. 

## Estimate Entropy Signal 

RR series entropy can be computed with the point process approach (statistical approach). We have a probability distribution that describes the characteristics of our process. 

Distance measure was before the distance between observations with the KS distance (distance between probability distributions) and then by adapting the concept of distance we can follow the rules to extract ApEn. We have defined a probability for each time stamp with a continuous resolution of our probability. 

We assume the activity of the ANS and we can use the cardiovascular control to estimate such an activity and this is done by looking at the RR series. We can assume that the distribution can be contextualized in a point process network. We define a inverse gaussian model and we can decide to tune the parameters with an AR model or NARM. We can track non linearity because the model on which the system is based in not purely reliable and we can extract indexes. 

### Applications 
**Tilt-table** (Lesson 3). Data Available on physionet. Each segment correspond to a complete protocol. In this data we can see different tilts (15s each). The subject moves to supine to upright or viceversa. The response is different.
- muscolar vasoconstriction and not all the blood accumulates in the legs
- During tilt there is not so much muscolar contraction and the blood accumulates in the lower part of the body which leads to reduction in BP and oxygenation of the brain. 
- In healthy condition in order to maintain homeostasis HR increases. ANS increase Sympathetic activity. 

We can use the point process approach. Bigger time window the longer the time is required for convergence. **Goodness of fit analysis** should be performed and we look at correlation function in order to be sure that the order is the most appropriate one. 


**Autonomic Blockade**: requires the use of a drug that blocks the ANS activity. THey study the effects of respiration on RR intervals. They discovered that this gain (information from Respiration to RR) in reduced when the drug is administered and this study shows how it acts on the vagal activity. The interaction between respiration and RR is the high frequency domain of the vagal activity. 

With Respiration we have important information about the functioning of the heart. 

**Effect of meditation**: during meditation the respiration gain [ms]/[nru] are higher and they can increase relationship. 

**Depth of anesthesia**: here we could observe that the drug induces hypotension and they observed that baroreflex gain drops after anesthesia. Baroreceptors are not working properly and there is inibition of this communication channel. this might be one of the reasons why hypotension happens (no feedback from the state of the vessels)

Additional statistical models could try and find out correct relationship between **unplesent or pleasent stimuli**. (images usually classified by psychologists). Stress induced by hearing loss. 

**Dynamic characterisation of Parkinson Disease by instantaneous Entropy Measures**: reduction of entropy in RR signals also in continuous time.  

**Temporal Lobe Epilepsy**: cognitive functioning may decrease and there are protocols to study ANS activity in epileptic subjects under specific clinical trials. 

Use of ML approaches that takes cardiovascular indexes in order to train other models able to map this cardiovascular state into a specific outcome. Which can be binary or continuous. Link cardiovascular response to predict mortality of subjects in ICU or specific events. i.e. SVMs


