PROGRAMMING 1

# Tilt data analysis 

We plot pressure signal, ECG signal and different phases of the protocol. There is a lot of noise in the data but the QRS complex can be detected. 

The last signal represents the tilt of the bed in degrees. 

The first approach is to perform AR modelling with classical methods. For a classical analysis we need stationary signals and thus we need to window signal (100 s). We need to subtract the mean of the RR so to appreciate other variations in the signal. 

The AR model is computed using the yule walker approach: 
`mod = ar(RRdep, Pmono(i), 'yw');`

and evaluate the goodness of the model by using the AIC/ADTest/lillie Test. The errors are the errors we are dealing with. We analyze distribution of the residuals to see if they are a gaussian. If they are white it means that we are not loosing information: 
`Ak(i) = aic(mod, 'aic');`
`res= resid(RRdep, mod);`

residual's ACF can be computed and we expect that if data is normal distirbuted we have only one value at zero lag. 

For higher lags we can see there are not a lot of peaks in non zero lags. The tradeoff of model complexity and information loss is probably around 8. By changing the window and test the model order found might be different, be aware of that. 

## Monovariate AR Modelling
Monovariate **AR Modeling**: 
`theta = aryule(RRdep, Pmono);`
which returns the coefficients of the model. 

The **PSD** can be computed using:
`[tot,f]= pyulear(RRdep, Pmono, 512, 1/mean(diff(R)))`

And we can spot an high frequency component during Rest, while during tilt we spot a reduction of HF amplitude and introduction of a LF component. 

## Batch Monovariate PP Modelling 
### Function regr_likel.m
We try to model our data with a function created by Barbieri that takes as input the times of the peaks and a character vector representing a parameter. 1 should be passed as input when there is a theta0 constraint. 

1. Depolarize data (first peaks as it happens at 0 time)
2. uk are the observed events (P order)
3. Extract RR intervals from observed events
4. Compute prediction at P+1 by using toepliz matrix. To build it we pass first column and first row: 
`xn = toepliz(rr(P:end-1), rr(P:-1:1));`
`wn= rr(P+1:end);`
6. Add a col of ones to count for the continuous power. 
7. Fit distribution of the data and to maximise the likelihood: `[]= maximize_loglikel(xn,wn);` which takes the predictions and observations. 
8. Output parameters of the computation Thetap, Kappa, opt 

### Function spectral.m
Then we can compute the power and estimate basically the spectrum starting from the parameters used to compute the R peaks. 
- Compute plots of the data

Spectrum is estimated by finding the roos of the expression:

![Schermata 2021-05-12 alle 11.22.37.png](./Pictures/a321f0068b624861907b7e0abbc708f8.png)

`poles= roots([1;-A]);`

For each of these poles the contirbution to the spectrum is computed multiplied by the distances that all the other poles have with respect to the frequency of interest. 

The frequency of the different poles can be extracted and complex conjugates must be addressed properly. 