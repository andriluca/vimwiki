# Barbieri

| Lecture                      | Topic                                  | Revised     | References             | Days     |
| :---                         | :---                                   | :---        | :---                   | :---     |
| [01](Barbieri/01/Lecture.md) | Introduction                           | No          |                        |          |
| [02](Mollura/02/Lecture.md)  | Multivariate Causality                 | Yes         | Multivariate_Causality | 20.04    |
| [03](Mollura/03/Lecture.md)  | Higher order spectra                   | In progress | Higher_order_spectra   | 21.04    |
| [04](Barbieri/04/Lecture.md) | Information Theory                     |             |                        | 26.04    |
| [05](Barbieri/05/Lecture.md) | Neural Code                            |             |                        | 27.04    |
| [06](Barbieri/06/Lecture.md) | SM(I): BCI & Point Process Theory      |             |                        | 03.05    |
| [07](Barbieri/07/Lecture.md) | SM(II)                                 |             |                        | 04.05    |
| [08](Barbieri/08/Lecture.md) | SM(III)                                |             |                        | 05.05    |
| [09](Mollura/09/Lecture.md)  | SM(IV)                                 |             |                        | 10.05    |
| [10](Mollura/10/Lecture.md)  | Blind Source Separation & ICA          |             |                        | 11.05    |
| [P1](Mollura/P1/Lecture.md)  | Programming 01                         |             |                        | 12.05    |
| [11](Barbieri/11/Lecture.md) | Statistical Learning I                 |             |                        | 17-18.05 |
| [12](Barbieri/12/Lecture.md) | Cross validation, Bootstrapping, Trees |             |                        | 18.05    |
| [13](Barbieri/13/Lecture.md) | SVM                                    |             |                        |          |
| [14](Barbieri/14/Lecture.md) | Unsupervised Learning                  |             |                        |          |
| [P2](Mollura/P2/Lecture.md)  | Programming 02                         |             |                        | 26.05    |
| [P3](Mollura/P3/Lecture.md)  | Programming 03                         |             |                        | 31.05    |
| [P4](Mollura/P4/Lecture.md)  | Programming 04                         |             |                        | 01.06    |


| Day   | Link                                                                                                                                                             |
| :---  | :---                                                                                                                                                             |
| 19.04 |                                                                                                                                                                  |
| 20.04 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=9d4fd9e77d65422cb10f34881c3ca695                                                              |
| 20.04 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=e3d0aa60dd444d039f1aeed40fad076a                                                              |
| 21.04 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=7866be1985194d60be40369e7039e08f                                                              |
| 21.04 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=ed4ef36fd4e54241800eefdead466f7d                                                              |
| 26.04 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=dcfe138dedb14519a1716fc818fa8af7                                                              |
| 26.04 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=e550024553584cb1839be865652b7037                                                              |
| 27.04 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=a591fa93353e4c01bb04f495593947a0                                                              |
| 27.04 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=339d6fa6f4f249aa8b76bca826c17efc                                                              |
| 03.05 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=db1065b396ac4028a1384f6d8baa8358                                                              |
| 03.05 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=0f0173dc46484a2d843a49259e043d71                                                              |
| 04.05 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=058495d698d7491ba1d3279aa885a4fe                                                              |
| 05.05 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=d9f1f6056b1f4a9b998ada69c79e14e2                                                              |
| 10.05 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=fb75891a41e84c62a6eeb090ef4b3a5d                                                              |
| 10.05 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=f00616845e33413ca49a60199cd24af9                                                              |
| 11.05 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=7d493211c0034180a029875623e4bf87                                                              |
| 11.05 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=7cfee05ae19f4df0bb71cd00fcfd4bfd                                                              |
| 12.05 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=8df28b953e2846dabcba4ac2c553d408                                                              |
| 17.05 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=f3c5e2de31104a58ba22e8206531674e                                                              |
| 18.05 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=e19596acc5394ee1acff8b3cc8a05651                                                              |
| 18.05 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=7806a76d992d4ccc881a6406d71150f3                                                              |
| 19.05 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=71fdf5f3bc1c4672ba5c3d0dd296fe0d                                                              |
| 24.05 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=d90b9d00bc734b00bd6a0126bd41a188                                                              |
| 24.05 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=81ba4037843247508c7254dda811515f                                                              |
| 25.05 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=d90b9d00bc734b00bd6a0126bd41a188                                                              |
| 26.05 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=1842e2f56e354136b435c621b77214ce                                                              |
| 26.05 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=e02bb1ddce063150efb9089c4e6e1067                                                              |
| 31.05 | https://polimi365-my.sharepoint.com/:v:/r/personal/10487818_polimi_it/Documents/Advanced%20signal%20proc/Barbieri/2021-05-31%2015-30-31.mkv?csf=1&web=1&e=NFgjQt |
| 01.06 |                                                                                                                                                                  |

## Examination procedure

Multiple choices probably, as mentioned by Prof. Cerutti.

# Cerutti

| Lecture                     | Topic                                  | Slides   | Revised     | Difficulty | Comprehensible | Days                      |
| :---                        | :---                                   | :---     | :---        | :---       | :---           | :---                      |
| [01](Cerutti/01/Lecture.md) | Wiener & Kalman (1)                    | 1 - 18   | Yes         | Normal     | yes            | 22.02                     |
|                             | Wiener & Kalman (2)                    | 15 - 29  | No          | Normal     | yes            | 24.02                     |
|                             | Wiener & Kalman (3)                    | 29 - 36  | No          | Normal     | yes            | 01.03 (start - 01:00:25)  |
| [02](Cerutti/02/Lecture.md) | Adaptive filtering                     | Complete | No          | hard       | ?              | 01.03 (01:00:25 - end)    |
| [03](Cerutti/03/Lecture.md) | Time-frequency & Wavelet (1)           | 1 - 37   | No          |            |                | 02.03                     |
|                             | Time-frequency & Wavelet (2)           | 1 - 41   | No          | Easy       |                | 03.03                     |
|                             | Time-frequency & Wavelet (3)           | 29 - 70  | No          |            |                | 08.03                     |
|                             | Time-frequency & Wavelet (4)           | 71 - 77  | No          |            |                | 09.03 (missing last part) |
| [04](Cerutti/04/Lecture.md) | Quadratic Time-frequency               |          | No          |            |                | 09.03                     |
| [05](Cerutti/05/Lecture.md) | Non-linearity and Chaos (1)            | 1 - 15   | Yes         | hard       |                | 10.03                     |
|                             | Non-linearity and Chaos (2)            | 6 - 24   | Yes         | hard       |                | 15.03                     |
|                             | Non-linearity and Chaos (3)            | 10 - 40  | No          | hard       |                | 16.03                     |
|                             | Non-linearity and Chaos (4)            | 38 - end | No          | Normal     | yes            | 17.03                     |
| [06](Cerutti/06/Lecture.md) | Complexity and fractals                |          |             |            |                | 22.03 (start - 01:58:46)  |
| [07](Cerutti/07/Lecture.md) | Fractal and non linearities (1)        |          |             |            |                | 22.03 (01:58:46 - end)    |
|                             | Fractal and non linearities (2)        |          |             |            |                | 23.03                     |
| [08](Cerutti/08/Lecture.md) | Biomedical Signal I Non linear (1)     |          | No          |            | yes            | 24.03                     |
|                             | Biomedical Signal I Non linear (2)     | 17 - end | No          | Very hard  | No             | 29.03 (start - 01:14:13)  |
| [09](Cerutti/09/Lecture.md) | Biomedical Signal II Non linear (1)    |          | No          | Very hard  | No             | 29.03 (01:14:13 - end)    |
|                             | Biomedical Signal II Non linear (2)    |          | No          | Very hard  | No             | 30.03                     |
| [10](Cerutti/10/Lecture.md) | Multiscale                             |          | No          | Very hard  | No             | 30.03                     |
| [11](Cerutti/11/Lecture.md) | Brain - Multimodal                     |          | No          | Hard       | No             | 31.03 (start - 01:10:00)  |
| [12](Cerutti/12/Lecture.md) | Technologies for future healthcare (1) |          |             |            | Yes            | 31.03 (01:10:00 - end)    |
|                             | Technologies for future healthcare (2) |          |             |            | Yes            | 07.04                     |
| [13](Cerutti/13/Lecture.md) | Etichs                                 | Complete | Yes         | Easy       | Yes            | 12.04                     |
| [14](Cerutti/14/Lecture.md) | Multimodal magnetic resonance          | Complete | In-progress | Easy       | Yes            | 13.04                     |


| Lecture | Link                                                                                                                             |
| :---    | :---                                                                                                                             |
| 22.02   | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=a295540e965d44d79c53acfdc630c4f5                              |
| 24.02   | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=6a4c23dfd76a4797b4ea31fbf12c4d50                              |
| 24.02   | https://polimi365-my.sharepoint.com/:v:/g/personal/10746314_polimi_it/EUcFRoCJI7pBn0EAZHQN0_oB9o05QikYTxn8GXvOlTjTsA?e=H16rrb    |
| 01.03   | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=7ac3b57d2ee8439ab345fa28afea09c1                              |
| 02.03   | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=ea522b62f64d47968fa1808fc980ca33                              |
| 03.03   | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=8af53383883b489e90986af56fbaaeab                              |
| 08.03   | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=1a166e295c8b4afe9953db17b5cff6ce                              |
| 09.03   | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=14b5000ca4cb40599d4ba4014ff66d91                              |
| 10.03   | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=60527866e4f84c388c1fae66fe8412f7                              |
| 15.03   | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=4c34e1ae799f409896170e34516ee20a                              |
| 16.03   | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=b593512988a148a38866da6563186848                              |
| 17.03   | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=55e7124dfce840f4bf8a6579d82f6857                              |
| 22.03   | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=6aa7b08073e445e6ac9ec09fde68d277                              |
| 23.03   | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=575545000d5247369cc706ffc0ec5dbe                              |
| 24.03   | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=a93d1681828d4f1f8972f8335c176134                              |
| 29.03   | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=c21aa897225741cab147481311315729                              |
| 30.03   | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=fc5497a5dd874361b3a92f60e8f85156                              |
| 31.03   | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=3619a5fee98d4857a37c62351b75f263                              |
| 07.04   | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=c73582edc43242af8a90b18891359104                              |
| 07.04   | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=5f7a1dbf0df84e41b5e6a72f552d513b                              |
| 12.04   | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=b471abe8c4a641f6bf2ca25ec31595c4                              |
| 13.04   | https://politecnicomilano.webex.com/recordingservice/sites/politecnicomilano/recording/7c7350402b634ec3a847995282ddd0fc/playback |

## Examination procedure

Oral exam. The first term is probably going to be written. Verifica in itinere:
written instead of oral exam. Multiple choice. The next exams are being held to
a modality that still needs to be defined.
