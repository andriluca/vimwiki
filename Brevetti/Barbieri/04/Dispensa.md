# Lezione 4 --- Procedure brevettuali

## Gli step della procedura

1. *Ricerca di prior art* per controllare che non esistano dei documenti che
   potrebbero impedire di ottenere la concessione del titolo.
2. *Valutazione dei requisiti* di novità e di attività inventiva.
3. *Deposito* della domanda di brevetto. il primo deposito viene chiamato
   "domanda di priorità". È un deposito nazionale ma anche europeo o
   internazionale.
4. Nei 12 mesi di priorità, quindi nel periodo che intercorre tra il deposito
   della prima domanda a quella successiva, bisogna fare *un'estensione
   regionale e/o internazionale* (a seconda delle strategie di business
   dell'azienda). Potrei farlo anche entro il 18esimo mese (prima della
   pubblicazione della domanda nazionale) ma non potrei rivendicare nella
   successiva estensione PCT, EU o US la domanda di priorità perchè non ci sono
   più i termini.

## I costi legati alla procedura

In Europa più o meno i costi dal deposito, alla concessione, alle convalide
nazionali, tenendo conto di convalidare il brevetto europeo in non più di 5
stati sono all'incirca 30k euro. In Cina sono 20k euro con procedura molto
simile a quella dell'EPO. In US si aggirano attorno ai 18k euro. questi costi
non sono tasse ma dovuti alle varie azioni ufficiali e sono onorari per il
consulente brevettuale.

## I trattati internazionali

- Convenzione dell'Unione di Parigi
- Patent Law Treaty
- Patent Cooperation Treaty (cooperazione in materia di brevetti)
- Trattato di Budapest (come depositare i microorganismi)
- Accordo di Strasburgo (introduce classificazione internazionale dei brevetti)

## I principi fondamentali del diritto industriale

1. *Priorità*: possibilità del titolare di una domanda di brevetto di avere
   questi 12 mesi per poi estendere all'estero la domanda nazionale. Per 6 mesi
   per quanto riguarda i marchi, disegni, modelli industriali.
2. *Indipendenza dei brevetti*: se depositassi una domanda di brevetto in
   Italia e poi nei 12 mesi di priorità lo estendo in Germania o Francia,
   ciascuna di queste domande di brevetto seguirà la procedura nazionale e
   potrà essere concessa oppure respinta indipendentemente dal fatto che in un
   altro ufficio magari viene concessa. Posso ottenere ad esempio la
   concessione in Italia ma in Germania, dove l'esame è più complicato,
   potrebbe accadere di essere respinta. Sono territoriali, quindi vengono
   fatti valere solo sul territorio dello stato in cui vengono concessi.
3. *Trattamento nazionale*: ogni stato che ha aderito alla Convenizone di
   Unione di Parigi accorda i cittadini di tutti gli altri Stati membri lo
   stesso trattamento riservato ai nazionali. Io italiano posso depositare una
   domanda di brevetto in Francia, un'altra in Belgio. Questi due stati non
   possono impedirmi di depositare una domanda di brevetto perchè non sono
   cittadino di quei particolari stati.

## Le tipologie di procedure di brevettazione

Esistono due tipologie delle procedure di brevettazione:

- *Ad esame sostanziale*: maggioranza delle procedure adottate negli uffici
  brevettuali
- *A semplice registrazione*: con procedura amministrativa formale (in Italia
  avviene per i modelli di utilità). Non vengono esaminati e dopo la
  registrazione il modello di utilità viene pubblicato e concesso dall'ufficio.

La validità di queste due tipologie viene determinata in tribunale, in una
causa (di non contraffazione, di nullità, etc.).  Un brevetto quindi si presume
concesso finchè non c'è opposizione e quindi una causa.

Argomenti a favore dell'esame:

- Riduce il numero di cause.
- Maggiore certezza per gli investitori (business angels, venture capitals etc).
- Maggiore qualità dell'intero iter di brevettazione.

## Aspetti generali delle procedure

È possibile depositare in Italia una domanda di brevetto e poi all'estero entro
12 mesi dalla data di priorità.

![](Figure/Aspetti1.png)

Primo caso: viene depositata una domanda italiana, entro i 12 mesi deposito una
domanda di brevetto internazionale PCT e dopo 18 mesi dalla data di deposito di
quest'ultima data posso fare una qualsiasi fase nazionale estera e/o regionale.
L'ingresso in queste fasi si ha dopo 30 mesi dalla data di priorità.  

![](Figure/Aspetti2.png)

Secondo caso: Se ho una domanda di brevetto europea ho 12 mesi di tempo
(periodo di priorità) in cui posso fare una domanda PCT o fasi nazionali
estere.

La data di priorità fornisce il vantaggio di poter presentare la domanda in
giorni diversi in uffici diversi, andando quindi a dilazionare meglio, a
posticipare i costi già elevati.

### Scadenze temporali

![](Figure/Aspetti3.png)

- T0: Primo deposito. La domanda di priorità.
- T1: Estensione internazionale in regime di priorità.
- T2: Pubblicazione.
- T3: Scadenza della domanda PCT.

Nel precedente grafico: a T0 avviene il deposito della domanda di
brevetto. A partire da quella data, si calcolano 12 mesi per poter fare a T1
un'estensione PCT in regime di priorità (se non lo faccio perdo la priorità).
Dopo 6 mesi, a T2, avremo la pubblicazione sia della domanda di priorità
che della successiva estensione PCT oppure regionale, europea etc. Poi dopo 12
mesi da T2 avremo la scadenza della domanda PCT, quindi dopo T3 devo
decidere se fare oppure no le fasi nazionali.

La domanda di brevetto internazionale non è una vera e propria domanda di
brevetto (non conferisce un brevetto PCT): posso ottenere una tutela
provvisoria nei 153 stati aderenti al Trattato di Cooperazione in materia di
Brevetti ma non ottengo un brevetto concesso a livello internazionale. Alla
scadenza del PCT devo fare necessariamente delle fasi nazionali per avere la
tutela in altri paesi.

![](Figure/Aspetti4.png)

Nel precedente grafico: Si supponga di avere una domanda di brevetto italiana,
si aspettano 12 mesi e si esegue un'estensione PCT poi entro 30-31 mesi dalla
data di priorità della domanda italiana T0 si devono fare le varie fasi
nazionali. **Come si calcolano i 20 anni della scadenza di un brevetto**: per
quanto riguarda la domanda di priorità (brevetto italiano) li calcolo a partire
da T0. Per quanto riguarda la fase europea bisogna fare riferimento alla
prima estensione internazionale PCT, quindi la durata della domanda europea non
va calcolata a partire dalla domanda di priorità ma dalla prima estensione
internazionale, quindi da T1. Per la stessa invenzione ho 20 anni di
validità per il brevetto italiano e sempre 20 anni per quella europea ma viene
calcolata dall'anno dopo.

![](Figure/Aspetti5.png)


Nel precedente grafico: Si supponga di avere una domanda italiana e nei 12 mesi
viene fatta un'estensione presso l'ufficio europeo brevetti. Quindi 20 anni
vengono calcolati da T0 per la domanda italiana, quelli per la domanda
europea vengono calcolati da T1. Ho una tutela complessiva di 20+1 anni.

## Deposito a livello internazionale

Nelle procedure abbiamo la possibilità di deposito a livello internazionale:

- *Deposito nazionale (all'estero)*:
	- In alcuni casi c'è la possibilità di effettuare *l'esame di novità
	  differito*: per esempio in Germania a 7 anni dal deposito. Viene
	  esaminata la domanda su richiesta del titolare che paga le tasse
	  d'esame oppure un terzo che ha interessi commerciali e paga le tasse
	  di esame e la domanda viene esaminata.
	- Si può avere una *ricerca separata dalla fase d'esame* (i.e.
	  all'ufficio brevetti si paga una tassa di ricerca al deposito della
	  domanda. Questa viene pubblicata dopo 18 mesi dalla data di deposito
	  e si hanno 6 mesi di tempo per richiedere l'esame. Se non viene
	  richiesto, la domanda viene considerata come ritirata).
	- In alcuni paesi (i.e. Francia) viene eseguito *un esame di novità*.
	  Viene preso in considerazione solo il requisito di novità e non di
	  inventività. ma viene messo in rapporto di ricerca e il richiedente
	  deve modificare le rivendicazioni oppure non le modifica ma deve
	  presentare una relazione con un commento sui documenti dicendo che
	  non sono rilevanti. In ogni caso questa domanda viene accolta
	  d'ufficio: è un esame di novità che non ha implicazioni forti sul
	  testo. viene concesso un titolo la cui validità dovrà essere
	  determinata durante un contenzioso in tribunale.
- Brevetto europeo (fascio di brevetti nazionali).
- Procedura PCT.

## La procedura italiana

Prima di descrivere la procedura, è bene ricordare che in luglio 2008, UIBM e
EPO hanno sottoscritto un accordo per cui tutte le domande di brevetto
nazionali per invenzione (quindi a esclusione dei modelli d'utilità) vengono
esaminati dall'EPO.

### Gli step necessari

![](Figure/Procedura_ita.png)

Supponiamo di avere il deposito della domanda di brevetto al tempo iniziale di
riferimento (0). È necessario compilare la modulistica semplice. Il fascicolo
comprende anche la traduzione delle rivendicazioni. La documentazione
dev'essere completa all'inizio e si ha 1 mese di tempo per ovviare a degli
errori nella stesura del testo. L'UIBM fa una valutazione dei requisiti formali
(controlla che ci siano i documenti, siano state pagate le tasse di deposito
...) e a quel punto emette un verbale di deposito, assegnando alla domanda un
numero ed una data di deposito. Entro 5 mesi dal deposito, l'UIBM invia il
fascicolo all'EPO, altrimenti non sono rispettati i termini: potrei ricevere il
rapporto di ricerca a ridosso dell'anno di priorità. *Entro 9 mesi* dal
deposito della domanda, se sono stati rispettati i requisiti formali che
richiede UIBM, viene emesso il rapporto di ricerca e l'opinione scritta da
parte dell'Ufficio Brevetti Europeo. Il rapporto di ricerca è uno strumento
utile per poter decidere se fare un'estensione internazionale: se il rapporto è
completamente negativo potrebbe non convenire e quindi si abbandona la domanda.
Il rapporto di ricerca costituisce un fondamentale strumento decisionale.  

Dopo di che il secondo step è la pubblicazione della domanda di brevetto che
avviene dopo 18 mesi. 

Successivamente avviene la fase d'esame: quando ricevo da UIBM il rapporto di
ricerca e l'opinione scritta vi è in allegato la cosiddetta *lettera
ministeriale* in cui si dice che la fase d'esame avrà inizio a partire dalla
data di pubblicazione della domanda di brevetto e il titolare ha tempo 3 mesi
dalla pubblicazione per fare delle modifiche e per rispondere alla
ministeriale. Se non vi è risposta e il rapporto di ricerca è negativo la
domanda di brevetto viene respinta. Deve esserci una risposta all'esaminatore.
Non essendoci un contraddittorio con l'esaminatore nella procedura italiana è
verosimile ottenere la concessione del titolo. Se ottengo il rifiuto per un
disguido procedurale posso fare ricorso.  Il ricorso può essere fatto solo
entro certi termini ed entro 60 giorni dalla notifica di rifiuto della domanda
nazionale.

### Il deposito

Per poterlo effettuare sono necessari dei documenti che devono essere compresi
nel fascicolo brevettuale. è necessario allegare la traduzione in inglese delle
rivendicazioni altrimenti si deve pagare una tassa di 200€ e la traduzione
viene fatta automaticamente dal sistema. Ciò è altamente sconsigliato perchè la
traduzione è un'operazione difficile e molto delicata.

Ci sono due modalità per depositare un brevetto:

1. *Deposito telematico*: bisogna essere registrati, quindi solo i consulenti in
   proprietà intellettuale sono soliti utilizzarla.
2. *Deposito cartaceo*: raccoglie la modulistica da dover compilare. Si può
   consegnare la documentazione presso una Camera di Commercio. il modulo del
   richiedente raccoglie i dati bibliografici, titolo, chi sono gli inventori,
   se propongo una classificazione internazionale etc. Il testo della domanda
   di brevetto deve essere allegato con le rivendicazioni tradotte anche in
   inglese, in quanto l'Ufficio Europeo Brevetti va ad esaminarle.

Se tutta la procedura viene eseguita online, la tassa di deposito è di soli 50
€. Bisogna poi considerare gli onorari del consulente che si prende carico
della scrittura della domanda di brevetto (2-3k€). Se poi la domanda non viene
depositata con modalità telematica le tasse sono più alte. Il costo è crescente
in base al numero di rivendicazioni inserite nella domanda e a seconda del
numero di pagine.

### Il rapporto di ricerca

L'EPO è l'autorità competente a dare questo tipo di valutazione. Non tutte le
domande vengono inviate ma vengono effettuate delle preselezioni. La
ricerca di prior art riguarda le domande di brevetto per le quali non è stata
rivendicata la priorità. Non viene fatta alcuna valutazione per quelle domande
nazionali che rivendicano una priorità interna, viene fatta solo per i primi
depositi.

In questo rapporto vengono citati i documenti rilevanti per l'oggetto della
domanda di brevetto con i dati bibliografici. Ciascun documento viene
contrassegnato con una lettera che lo classifica all'interno del rapporto di
ricerca. Quelle più utilizzate sono le seguenti:

- **X**: Rilevanti per requisito di novità o attività inventiva e questo è
  specificato nell'opinione scritta.
- **Y**: Molto rilevanti per attività inventiva se combinati ad altri.
  Nell'opinione scritta si troverà sempre due documenti con la Y. è la
  combinazione di questi due documenti che rende la domanda carente di attività
  inventiva.
- **A**: Descrivono lo stato della tecnica ma non sono considerati
  pregiudizievoli per i requisiti di brevettabilità.
- **P**: Articoli e domande di brevetto pubbicate dopo la data di priorità ma
  prima della data di deposito. Sono pubblicati tra la data di deposito della
  domanda in esame e la sua priorità. Questi documenti sono rilevanti per i
  requisiti solo se la priorità non viene accettata dall'esaminatore (i.e. sono
  state modificate le rivendicazioni in fase di deposito della domanda). Se la
  data di priorità viene annullata allora questo tipo di documenti diventa
  rilevante. Si fa riferimento alla pubblicazione. Possono essere articoli
  scientifici, documenti tecnici e domande di brevetto.
- **E**: Domande di brevetto europee pubblicate dopo la data di deposito della
  domanda in esame ma hanno una data di priorità o di deposito che è precedente
  a quella del documento in esame, quindi sono pregiudizievoli per i requisiti
  di brevettabilità.
- **T**: Articoli scientifici che spiegano le basi teoriche
- **O**: Divulgazioni orali.
- **L**: Documenti generici (i.e. un catalogo tecnico che descrive le
  caratteristiche di questi prodotti e queste sono accessibili al pubblico
  prima della data di deposito della domanda, quindi risulta pregiudizievole
  per il requisito della novità).

Vengono inoltre indicati tutti i membri della famiglia dei documenti citati,
così da rendere più semplice la loro consultazione da parte del titolare o del
consulente in una lingua più comprensibile. In conclusione il rapporto di
ricerca consiste in uno strumento decisionale e oltretutto permette di
effettuare ricerche di prior art perchè viene indicata sia la classificazione
della domanda (IPC) che i campi tecnici ricercati.

### La lettera ministeriale

è una lettera in formato standard che sancisce delle scadenze ben precise. Per
ottenere la concessione è necessario rispondere a quanto evidenziato
nell'opinione scritta.

### L'esame

L'esame avviene entro i 24-30 mesi dal deposito della domanda (e comunque dopo
la pubblicazione della domanda).  Il richiedente può presentare le proprie
osservazioni, emendamenti, ecc. entro i successivi 3 mesi dalla pubblicazione
della domanda.  Nei casi in cui dal rapporto di ricerca o dall'opinione scritta
si evince che la domanda risponde ai requisiti di brevettabilità, il brevetto
verrà concesso dall'Ufficio senza necessità di una risposta.  In caso di
rifiuto è possibile presentare un ricorso oppure convertire la domanda di
brevetto per invenzione in un modello d'utilità (se ci sono i requisiti).

### Fare ricorso

Il ricorso può essere fatto solo entro certi termini ed entro 60 giorni dalla
notifica di rifiuto della domanda nazionale.

- Si può presentare ricorso *se si riesce a dimostrare che l'UIBM non ha inviato
  nei termini corretti la lettera ministeriale*.
- *Se il rapporto di ricerca ha esito negativo e non si risponde alla lettera
  ministeriale* **non** è possibile fare ricorso in quanto verrebbe respinto.
- La conversione del brevetto nullo in brevetto per modello d'utilità
  dev'essere fatta in certi termini. Esiste una domanda di conversione da
  eseguire e viene proposta dall'UIBM.

### Le tasse annuali di mantenimento

Si pagano a partire dal quinto anno di vita del brevetto e sono crescenti al
trascorrere degli anni di vita.

## La procedura PCT

![](https://www.epo.org/applying/international/guide-for-applicants/html/e/guiapp2_cleaned.image2_en.png){ width=500px }

Si basa sulla Convenzione di Washington del 1970 (trattato multilaterale  in
vigore dal '78 in US e dall' '85 in italia).  Si tratta di una procedura
unificata con un unico deposito si ottiene una tutela provvisoria in tutti i
153 stati membri del Trattato di Cooperazione in Materia di Brevetti (Patent
Cooperation Treaty). La domanda internazionale produce in tutti gli stati
designati gli stessi effetti legali di una domanda nazionale. La concessione
del brevetto però è di competenza dell'Ufficio brevetti nazionale che viene
designato, in cui si procede a convalidare la domanda di brevetto
internazionale PCT. Non esiste un brevetto mondiale. Il vantaggio della
procedura PCT è che consente di posticipare l'ingresso nelle fasi nazionali (da
30 a 32 mesi: 31 per la fase regionale europea, 32 per la Cina).

Stabilisce una procedura centralizzata per il deposito e l'esame di una domanda
di brevetto.  Si tratta di una procedura semplice e più economica per ottenere
la tutela in diversi Stati.  La via nazionale al PCT per alcuni Paesi è chiusa:
si deve seguire la procedura EURO-PCT.

Per poter depositare una domanda PCT almeno un richedente deve avere la
residenza o la nazionalità in uno degli stati membri del PCT. La domanda può
essere presentata in una delle 10 lingue della procedura (in particolare
Inglese, Francese e Tedesco sono lingue della procedura EPO). Il fascicolo PCT
deve includere l'indicazione che si richiede una domanda PCT, la
designazione degli Stati (dal 2004 in automatico vengono designati tutti gli
stati del trattato), il nome del richiedente, la descrizione e almeno una
rivendicazione (se non ve n'è neanche una la domanda non viene accettata).

Ci sono delle novità procedurali perchè dal 1 luglio 2020 è possibile
effettuare, dalla scadenza dei 30 mesi, l'ingresso diretto nella fase nazionale
italiana mentre prima bisognava procedere con la fase regionale europea.  

### Gli step necessari

- Primo deposito (domanda di priorità). 
- Dopo 12 mesi ho un deposito PCT. 
- Prima della pubblicazione della domanda PCT il richiedente ottiene il
  rapporto di ricerca e l'opinione scritta dell'esaminatore (ovviamente pagando
  le tasse di deposito e di ricerca). 
- Dopo 18 mesi viene pubblicata la domanda PCT.  Se il rapporto di ricerca è
  negativo esiste una possibilità per il titolare di richiedere un esame
  opzionale (un esame preliminare) e ottenere una decisione prima della
  scadenza al ventottesimo mese ossia prima della scadenza dell'ingresso nelle
  fasi nazionali.  Questo potrebbe essere utile nei casi in cui ad esempio è
  facile modificare la rivendicazione, non ci sono tanti documenti
  contrassegnati con X e Y. 

Negli US l'esaminatore dell'USPTO riprende tutta la
procedura di ricerca e di esame.

### PCT direct

È possibile fare una procedura particolare, la *PCT direct*: una comunicazione
che viene allegata in fase di deposito della domanda PCT. Ci sono dei commenti,
degli argomenti sulle obiezioni formulate dall'esaminatore nell'opinione
scritta della domanda di priorità.

Se la domanda italiana ha ricevuto un rapporto di ricerca negativo posso fare
un ingresso in fase PCT cambiando le rivendicazioni, oppure se si ritiene che i
documenti citati dall'esaminatore non siano rilevanti si allega una
comunicazione di questo tipo in cui si spiega all'esaminatore perchè questi
documenti non sono rilevanti. Essendo una procedura opzionale potrebbe non
essere tenuta conto dall'esaminatore. Di solito viene presa in considerazione
se esiste solo un documento contrassegnato con X che non è rilevante. serve per
ottenere poi un rapporto di ricerca positivo. è da redigere in inglese.

## La procedura EPO

La procedura EPO si applica per tutti i 38 stati membri della convenzione e le
varie estensioni. Contempla anche Stati extra UE.

Non si ottiene con la procedura EPO un brevetto valido in tutti gli stati
designati ma un fascio di brevetti nazionali, per cui per ogni stato designato
si devono pagare le tasse di convalida (di ingresso in quello stato e di
mantenimento). Potrà poi essere abbandonato, revocato etc indipendentemente
dagli altri stati sulla base di decisioni nazionali perchè poi essendo un
fascio di brevetti nazionali tutte le varie procedure da quelle cautelari, al
contenzioso etc vengono gestite dai tribunali nazionali perchè non esiste un
brevetto unitario. è una procedura che dopo la concession e è dunque
frammentata.

Il vantaggio della procedura EPO è quello di semplificare alcune delle fasi
(deposito, di esame e di concessione) e non ci sono gli iter burocratici dei
singoli depositi nazionali.

Quando conviene l'ingresso diretto negli stati esteri? quando per esempio
l'azienda ha un business che è solo per un paio di stati. La rule of thumb è
quella che se ho **almeno quattro stati** da coprire utilizzo un'estensione
internazionale (sia essa PCT o EP).

Qual è a differenza di fare un ingresso diretto in uno stato e non di fare la
procedura EPO: Nella procedura dell'EPO il titolo che ottengo ha lo stesso
ambito di tutela in tutti i paesi designati perchè faccio semplicemente una
traduzione delle rivendicazioni concesse ma il paese designato non continua
l'iter di esame perchè già ultimato con la procedura EPO. Se faccio una
procedura nazionale potrei ottenere un ambito di tutela in Italia e in Francia
(ad esempio, in cui gli esami sono più semplici) e restringere l'ambito di
tutela per altri stati come la Germania. Si tratta dunque di un bilanciamento
tra costi e benefici futuri.

Le rivendicazioni non sono necessarie per ottenere una data di deposito: potrei
depositare una domanda di brevetto all'EPO fornendo semplicemente un testo, una
descrizione. Ovviamente perchè venga poi riconosciuta come domanda e quindi
possa farla valere in modo effettivo devo necessariamente completare la
procedura inviando le rivendicazioni entro 2 mesi dal deposito.

### Gli step necessari

Descrizione delle varie fasi della procedura:

1. Deposito della domanda di brevetto
2. Trasmissione del rapporto di ricerca (6-9 mesi dal deposito se si tratta di
   un primo deposito all'EPO) solo al richiedente perchè la domanda non è
   ancora pubblica.
3. è necessario richedere l'esame entro 6 mesi da quando si riceve il rapporto
   di ricerca altrimenti la domanda è considerata ritirata.
4. pubblicazione di domanda e rapporto di ricerca dopo 18 mesi dal deposito.
5. la concessione avviene dopo 3-4 anni dal deposito.
6. rispetto alla procedura nazionale si ha l'opposizione: quando si ottiene la
   pubblicazione della concessione sul bollettino dei brevetti europei
   qualsiasi terzo a partire da quella data può fare opposizione alla
   concessione (si ha tempo 9 mesi).
7. Nazionalizzazione: il brevetto europeo è un fascio di brevetti e deve essere
   convalidato negli stati di interessi. Senza la procedura di convalida anche
   se ho ottenuto la concessione del brevetto EPO questa non vale nulla. Se non
   avviene la nazionalizzazione quindi si incombe nell ritiro del brevetto.
8. Limitazione e revoca: questi due ultimi procedimenti li può richiedere il
   titolare.

### Ottenere la tutela in Europa:

- È possibile *depositare tante domande di brevetto presso i vari uffici
  nazionali*.
- È possibile *depositare un'unica domanda europea* e, dopo la concessione del
  brevetto, *fare la convalida* negli stati d'interesse (dove ci sono i
  competitor dell'azienda, dove l'azienda ha un mercato potenziale).
- Si può utilizzare la national-PCT phase: deposito una domanda PCT e dopo 30
  mesi dalla data di priorità seleziono gli stati in cui richiedere la tutela
  (con alcune eccezioni riportate qui e viste prima).
- *Depositare una domanda PCT e dopo 31 mesi dalla data di priorità effettuare
  l'ingresso in fase regionale europea*.

## La procedura US

1. Deposito: è possibile fare una domanda di brevetto provvisoria (Provisional
   Application): semplicemente un testo (i.e. articolo scientifico). Nell'arco
   di 12 mesi, avendo depositato una domanda provvisoria oppure un'altra
   domanda provvisoria o una non provisional (detta anche utility) application
   (cioè la domanda di brevetto vera e propria) posso fare un'estensione
   internazionale con procedura PCT. Le domande provvisorie hanno una durata
   limitata (i.e. un anno di validità). Però queste domande provvisorie non
   sono pubblicate nei database brevettuali e se non vengono estese a livello
   internaizonale o come domanda non provvisoria (come utility) decadono, è
   come non aver depositato nulla. Se depositassi una domanda regolare (regular
   application, che corrisponde alla non provisional application) devo anche
   depositare una Declaration of inventorship (dichiarazione d'inventore) e un
   assignment. in questo caso gli inventori (persone fisiche) cedono
   all'azienda i diritti patrimoniali.
2. L'esame: parte circa dopo un anno dal deposito della domanda US. Ci sono
   delle formalità. è un esame formale e bisogna aver sottoscritto una lettera
   di incarico (power of attoney) in cui ho domandato ad un patent agent US di
   seguire la procedura. Ci sono poi delle formalità: il ricevimento del
   deposito ufficiale e la pubblicazione. Successivamente bisogna depositare
   l'IDS (information disclosure statement): questo è un documento in cui il
   titolare deve dichiarare tutti i documenti di prior art a sua conoscenza.
   Deve farlo con diligenza perchè questo non completo può essere una causa di
   nullità da parte dell'USPTO.  Successivamente iniziano le office action che
   nella procedura US sono solamente due:
	1. un'office action non finale
	2. office action finale. questa potrebbe essere una comunicazione di
	   rifiuto.

Nel caso in cui la final office action sia una comunicazione di rifiiuto quello
che il titolare può fare è replicare depositando anche quella che viene
chiamata "continuation" o RCE (Request Continuing Examining). Si richiede a
USPTO di continuare effettivamente l'esame pagando un ulteriore deposito.
mentre le azioni ufficiali sono sempre due all'USPTO (all'EPO non ci sono
limiti alla numerosità di azioni legali) posso ottenere la notifica di
concessione oppure il rifiuto. se voglio continuare deposito le continuation
(che possono anche essere continuation "in parte" se aggiungo elementi tecnici
che non erano presenti nella domanda di priorità. questo è concesso negli US ma
non in EU). La replica nel caso di una final office action prevede una risposta
all'esaminatore e un deposito della RCE, oppure posso rispondere e fare
l'appello. è meglio depositare la continuation perchè se perdessi l'appello la
domanda sarebbe respinta completamente e definitivamente. Utilizzando il
sistema delle continuation posso andare avanti indefinitamente a seconda delle
finanze di cui dispongo. Le domande divisionali e le varie continuation devono
essere depositate prima della concessione del brevetto. Quindi in una
continuation in un certo senso cambio le rivendicazioni (non aggiungo nuova
materia ma cambio il wording delle rivendicazioni). nella continuation in parte
aggiungo invece nuova informazione tecnica.

## Procedura eurasiatica

Una procedura molto simile a quella dell'ufficio europeo brevetti con la sola
differenza che la concessione del brevetto euroasiatico varrà in tutti questi
stati. non viene considerato come un fascio di brevetti ma è effettivamente
valido per quelli. Se però ottengo un rifiuto posso fare un ingresso dei vari
paesi, ovviamente con una procedura di deposito e di esame. L'unica difficoltà
è che si tratta di un brevetto difficilmente azionabile in questi stati. nel
2019 sono state depositate 3k domande che rispetto alle 10k italiane sono
poche.

## Brevetti di interesse per la Difesa

L'articolo 198 del codice delle proprietà intellettuali (C.P.I) asserisce che
non è possibile brevettare, per i residenti nel territorio dello Stato,
esclusivamente presso uffici di Stati esteri qualora le domande riguardino
oggetti potenzialmente utili per la difesa del Paese. La procedura è
schematizzata nel seguente modo:

![Procedura Art.128 C.P.I.](Figure/art_128.png)

## Glossario

- Continuation in part: dà la possibilità di aggiungere degli elementi tecnici
  che non erano presenti nella domanda di priorità.
- Divisionale: non c'è unità di invenzione, allora l'esaminatore invia una
  restriction requirement, dunque devo suddividere la domanda di priorità in
  più domande divisionali.
- *Famiglia brevettuale*: tutte le famiglie brevettuali dei documenti citati
  nel rapporto di ricerca.
- *PCT*: Patent Cooperation Treaty, trattato internazionale di cooperazione in
  materia di brevetti.
- *Pregiudizievole*: che può recare danno, pregiudizio.
- Provvisoria: 1 anno di tutela e può essere presentata senza rivendicazioni
- Continuation: da la possibilità di aggiungere, di modificare il set di
  rivendicazioni.

## Riferimenti bibliografici

Lezione: "procedure di brevettazione", "strategie brevettuali"
