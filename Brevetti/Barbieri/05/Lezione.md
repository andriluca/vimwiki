# Lezione 5 --- Esercitazione, ricerche di prior art e qualche esempio

# Esercitazione

## Esercizio 1

Una domanda nazionale viene depositata il 23 marzo 2021, viene fatta
un'estensione internazionale (PCT) il 15 marzo 2022 e si entra poi in fase
regionale europea il 15 Febbraio 2023. Quando scadono i brevetti di convalida
procedura europea? il 15 marzo 2042 in quanto si deve considerare la data
dell'estensione internazionale.

## Esercizio 2

Un'azienda italiana intende tutelare il prodotto in fase di sviluppo. Il
mercato di questo prodotto è costituito da Italia, Francia, Germania.
Argentina, Australia e Brasile, con maggiori competitor in Spagna e Belgio. La
strategia di tutela più conveniente è quella di effettuare una deposito in
Europa, il che mi permette di risparmiare 10k€ di deposito nazionale. Alla
scadenza dell'anno di priorità si effettuano le fasi nazionali estere in
Argentina Australia e Brasile. La domanda europea seguirà la procedura d'esame
al termine della quale posso fare le convalide in questi cinque paesi.

## Note

La procedura europea (e mi verrebbe da dire anche quella internazionale)
conviene se devo coinvolgere più di quattro stati.

## Esercizio 3

Startup italiana ha sviluppato il concept di un nuovo dispositivo. Il time to
market ossia il tempo di messa sul mercato del dispositivo è di circa 3 anni.
B2B: Business to business. Il prodotto è ventuto in Germania, Israele, Cina,
USA, Canada. Qual è la strategia di tutela?

Si effettua un deposito PCT e alla scadenza dei 30 mesi posso fare una fase
nazionale in Germania, Israele, US e Canada ed entro 32 mesi in Cina.

## Esercizio 4

Un'azienda italiana ha esposto a una fiera di settore una nuova apparecchiatura
non tutelata. Il mercato di riferimento è EU, Cina, USA e Canada. Può ancora
brevettare? Se sì dove?

Sfruttando il grace period può brevettare negli Stati Uniti e in Canada. Non
conviene designare EU e Cina perché essendo stato divulgato non verrebbe
concesso il brevetto.

# Ricerche di prior art

## Slide 1

Perchè si effettuano ricerche di prior art?

1. Per evitare di "reinventare" qualcosa di già noto. Brevettare significa
   impegnare risorse monetarie e soprattutto il tempo da dedicare alla ricerca,
   quindi è necessario rivolgere l'attenzione verso la novità.
2. Per evitare di essere considerati dei contraffattori di prodotti di altre
   aziende.
3. Esiste una sezione apposita nella domanda di brevetto denominata "prior art"
   nella quale viene indicata l'arte nota in quell'ambito e come la domanda
   proposta si differenzi e superi quanto rivendicato dall'anteriorità
4. Per effettuare delle ricerche più ampie su un particolare settore
   tecnologico. è possibile, inoltre osservare quali sono gli sviluppi di tale
   tecnologia.

## Slide 2

Quando si effettua una ricerca di prior art?

Prima di effettuare una tesi sperimentale, verificando sia le anteriorità che
lo stato dell'arte nella letteratura scientifica, (master o Ph.D.) e prima di
depositare una domanda di brevetto.  Per esempio a una richiesta fatta da un
ricercatore all'ufficio brevetti di proposta di brevettazione si effettua
quantomeno una ricerca del requisito della novità.  In caso la ricerca abbia
esito positivo per il ricercatore si può procedere ed effettuare il deposito
della domanda nazionale e, dopo 6-9 mesi si otterrà il search report e
l'opinione scritta dell'Ufficio Europeo Brevetti.  Trascorsi 18 mesi la domanda
è pubblicata (testo e classificazione dell'invenzione). Maggiore è il numero di
codici di classificazioni in una domanda di brevetto e maggiore è la
complessità di questa.  I codici di classificazione sono utilizzati per
monitorare le pubblicazioni simili e comprendere le strategie brevettuali dei
potenziali licenziatari.

## Slide 3

Le tipologie di ricerche di prior art:

1. Informative (aka Quick or state of the art) search: Sono ricerche sullo
   stato della tecnica. Servono per pianificare attività di ricerca e sviluppo,
   individuare trend tecnologici e ricercare eventuali concorrenti. Vengono
   utilizzate sia parole chiave (aka keywords o KW) e codici di classificazione
   (aka intellectual property code o IPC). è possibile utilizzare solo i codici
   di classificazione quando l'ambito tecnologico appartiene a pochi
   sottogruppi. Lo scopo di tale ricerca è quello di avere un'alta precisione
   mentre non è importante la completezza del rapporto (bastano anche solo
   pochi documenti).
2. Ricerche di brevettabilità: Si tratta delle ricerche fatte ordinariamente
   dagli esaminatori degli Uffici Brevetti. L'obiettivo è quello di valutare se
   un'invenzione è nuova e creativa pertanto non è necessaria un'elevata
   completezza sullo stato della tecnica.
3. Ricerca di validità od opposizione: Sono ricerche effettuate posteriormente
   in relazione alla pubblicazione della domanda di brevetto e hanno lo scopo
   di rassicurarne il titolare sullo stato di validità. Ha senso effettuare
   queste ricerche nel momento in cui ci si affida ad uffici che valutano il
   solo requisito di novità. Nel caso delle ricerche di opposizioni chiunque,
   in ambito di brevettazione europea entro 9 mesi dalla concessione, è libero
   di opporsi basandosi necessariamente su una di queste affermazioni:
	1. quanto dichiarato non è nuobo o inventivo. Di solito questa è la
	   causa maggiormente incidente sulla revoca delle domande di brevetto.
	2. non è sufficientemente descritto.
	3. durante la prosecuzione della domanda di brevetto l'ambito di tutela
	   è stato ampliato.
4. Ricerca sullo stato legale (legal status): La si effettua per comprendere lo
   stato di attività di un brevetto. La domanda è ancora attiva? è stato
   nazionalizzato il brevetto? sono state pagate le tasse di mantenimento? etc.
5. FTO (Freedom To Operate): Si valuta la libertà di attuazione di un brevetto
   ossia determinano se ciò che è stato brevettato è in contraffazione o se il
   prodotto ha caratteristiche simili a quelle di un altro per identificare
   eventuali dipendenze. Questo tipo di ricerca dev'essere molto completo, a
   scapito della precisione perchè bisogna avere la visione più ampia
   possibile. L'FTO è effettuata su brevetti concessi e attivi, domande di
   brevetto ancora in fase d'esame e domande PCT (in quanto potrebbe essere
   nazionalizzato in quel paese).  è una ricerca che richiede un accurato
   monitoraggio della letteratura scientifica ma che deve essere eseguita in un
   certo periodo di tempo perchè bisognerebbe comprendere tutte le domande di
   brevetto che al tempo della ricerca di libertà di attuazione non sono ancora
   disponibili perchè segrete. Se alcuni documenti non vengono considerati
   questo costituisce un errore grave. Ecco perchè nei contratti di licenza dei
   brevetti in genere non inserisco delle clausole concernenti la libertà di
   attuazione.
   
## Slide 9

Come effettuare una ricerca di prior art?

Qual è lo scopo? trovare documenti che rivendicano caratteristiche tecniche
simili e non solo una concordanza di termini. Di solito uso delle parole chiavi
ma non è detto che un documento sia effettivamente rilevante ma bisogna
valutare le rivendicazioni di questo. 

- per parole chiave (KW): è la ricerca più intuitiva e soggettiva perchè dipende
  dalle parole che vengono inserite.
- per simboli di classificazione: meno soggettiva perchè si tratta di strumenti
  di ricerca indipendenti dal linguaggio utilizzato. Hanno una certa
  definizione.
- per citazioni: meno soggettiva perchè si tratta di strumenti di ricerca
  indipendenti dal linguaggio utilizzato. Posso vedere quali documenti sono
  citati e quindi considerare sia backward e forward citation.

Per avere una ricerca mirata posso utilizzare diversi tipi di operatori:

- logici (booleani): and, or, not, andnot. 
- di prossimità: i.e. due termini sono vicine nella frase. Il linguaggio
  utilizzato di solito è l'inglese.

Esistono dei limiti all'approccio con parole chiave (KW):

- I documenti molto datati non sono indicizzati ed è molto difficile reperirli
  utilizzando delle parole chiave. Non posso fare delle ricerche di parole ma
  solo una scansione in pratica e quindi posso solo affidarmi solo ai codici di
  identificazione.
- Esistono dei documenti in cui il titolo del riassunto non è tradotto in
  inglese ma è in cirillico o in cinese. Se i documenti brevettuali sono
  pubblicati in una lingua che non utilizza caratteri latini non è possibile
  ricercare nulla. Posso ovviare a tutto ciò utilizzando dei simboli di
  classificazione.
- Tra le due classificazioni (quella internazionale IPC e quella coperativa
  CPC) è quella internazionale quella che viene assegnata da un gran numero di
  uffici brevetti nazionali ed internazionale. Siccome non tutti i documenti
  hanno un codice di classificazione cpc o non è stato assegnato è bene non
  utilizzarlo come simbolo prevalente ma bisogna utilizzare unacombinazione di
  keyword e codici di classificazione internazionale.

Quindi ritornando al come si effettuano queste ricerche:

1. Identificare le caratteristiche tecniche essenziali dell'invenzione
2. Identificare qual è la miglior banca dati per cercare quella specifica prior art
3. Pianificare una strategia di ricerca: quali parole chiave, quali combinazioni di KW e IPC
4. Valutare l'output della ricerca

Consigli utili:

1. Fare ricerca nel titolo, abstract e rivendicazioni ma non nel testo completo
   all'inizio per non avere troppi documenti. Poi espando la ricerca.
2. Non conviene utilizzare operatori di prossimità in titolo o abstract.
3. Non scrivere stringhe di ricerca molto lunghe e complicate. Voglio avere un
   set limitato di risultati (max 50), esaminare i risultati ed eventualmente
   espandere la ricerca.
4. Se trovo un documento rilevante si verificano le citazioni di quel
   documento, oltre ai simboli di classificazione. In certi casi questa
   strategia è risolutiva perchè trovo il documento più rilevante (closest
   prior art).
5. Per fare una ricerca di novità o di stato della tecnica devo riferimi ai
   depositi più recenti e quindi effettuo una limitazione della ricerca in un
   range di date.

## Slide 12

Una possibile strategia è quella di considerare l'invenzione scomponendola
nelle caratteristiche tecniche essenziali (akaEF o essential features) che la
compongono. In esempio possiamo notarne tre. Ciascuna feature può essere poi
descritta da un elenco di KW e IPC. Ciascuna EF è descritta dalla somma (quindi
dall'or) di tutto ciò che la descrive.  Combinando ora le varie EF è possibile
costruire le stringhe di ricerca da utilizzare. Questa strategia però parte da
tutti i documenti presenti e quindi è più complessa e scomoda perché dovrò
continuare a limitare fino a raggiungere il risultato desiderato.

Svantaggi di questa strategia:

1. Il numero di risultati su cui è possibile effettuare una revisione è troppo
   elevato
2. Si suppone che l'elenco di KW e IPC che definisce una certa EF sia completo.

## Slide 13

Perchè è meglio iniziare con pochi risultati ma precisi e poi espandere
successivamente?

1. Per evitare di avere molti risultati che non sono importanti.
2. per determinare l'utilità di ciascuna stringa di ricerca.
3. per ridurre il rischio di scartare dei documenti importanti.
4. Questa è una metodologia che può aiutare a decidere quando fermare la
   ricerca. Quando poi con le successive stringhe di ricerca ottengo dei
   risultati che non sono assolutamente rilevanti e quindi non è utile
   continuare.

## Slide 14

Inoltre non bisogna dimenticare che alcune parole presentano un significato
ambiguo. Basti pensare alla parola bridge che ha sia significato in ambito
architettonico ma anche biomedico e di elemento circuitale. Se utilizzo una
parola chiave generica rischio di trovare molti documenti non pertinenti.

Bisogna controllare lo spelling perchè esistono regionalismi. Bisogna anche
conoscere i diversi sinonimi.

Bisogna conoscere il linguaggio dei brevetti: un compromesso tra il linguaggio
legale e il tecnico. Le rivendicazioni vengono rese generiche con determinate
espressioni.

## Slide 17

Classificazione dei brevetti

La suddivisione in campi tecnici che coprono tutti i campi della tecnica. è il
classificatore e non l'applicant ad attribuire questi simboli. Gli uffici
brevettuali hanno elaborato sistemi di classificazione all'inizio del XIX
secolo per compensare il volume crescente di letteratura brevettuale e
scientifica. Esistono due sistemi, entrambi di tipo gerarchico (si parte da un
livello di suddivisione superiore degli ambiti di applicazione andando via via
a suddividere in ambiti più specifici):

1. IPC (international patent classification): possiede un livello superiore
   costituito da 8 sezioni generiche in cui si suddividono i campi tencici.
   Questi poi vanno a suddividersi ulteriormente in classi, sottoclassi, gruppi
   e sottogruppi fino ad arrivare ad una numerosità di 76k suddivisioni. L'IPC
   viene aggiornato ogni anno dal 1968 e con questo anche la classificazione di
   tutti i documenti che la utilizzano. Esistono molteplici sistemi IPC in
   vigore.
2. CPC (cooperative patent classification): è una classificazione potenziata
   che si basa su IPC ma con molte più suddivisioni specifiche. Elaborato da
   USPTO e EPO, sostituisce la ECLA (European Classification) e la USPC (U.S.
   Patent Classification). Incorpora i simboli di classificazione di ECLA e
   USPTC e alcuni simboli di indicizzazione (ICO) è entrato in vigore nel 2013
   ed è unico per chi lo utilizza. L'assegnazione dei simboli avviene 8-9 mesi
   dopo la pubblicazione della domanda di brevetto. Per questo motivo non
   dovrebbe essere utilizzato in esclusiva quando si fa ricerca brevettuale ma
   insieme ai codici di identificazione internazionali, in quanto potrebbero
   esserci nuovi brevetti che non hanno ancora ricevuto un'assegnazione e che
   sono di interesse.  Esiste una sola versione della CPC. Rispetto alla IPC
   possiede una sezione in più (la Y). La slide 25 riporta la copertura della
   CPC per vari uffici nazionali. è possibile notare la differente
   distribuzione in percentuale per i vari paesi. Un altro dato interessante è
   il trend delle pubblicazioni per ufficio brevettuale che evidenzia
   sbilanciamenti. Da queste considerazioni è possibile confermare che non si
   può utilizzare la classificazione cooperativa in esclusiva ma assieme a
   quella internazionale. Le otto sezioni A-H possiedono:
	- simboli principali: utilizzati per classificare sia la parte
	  inventiva che quella aggiuntiva.
	- codici di indicizzazione: utilizzati esclusivamente per informazioni
	  aggiuntive.
	- c set: utilizzato sia per informazioni inventive che aggiuntive. Sono
	  limitati a solo 37 sottoclassi, molte delle quali appartenenti al
	  settore chimico (i.e. polimeri). Servono per trarre informazioni che
	  non sarebbero raggiungibili diversamente.
   
   La sezione Y è invece dedicata ad alcune tecnologie particolarmente
   emergenti.

### Note

Ricercando il brevetto riportato a fine slide 26 si può notare nella sezione
CPC e IPC dei codici di classificazione in grassetto. Quelli sono relativi
all'informazione inventiva. Nella sezione IPC in carattere normale sono
riportati quelli relativi alle infomazioni aggiuntive. I corrispettivi di
quest'ultimi nella sezione CPC sono invece simboli di indicizzazione.

## Slide 21

Mediante questo database è possibile ricercare qualsiasi simbolo facendo uso
della barra di ricerca laterale. è possibile sia espandere le tab utilizzando
un approccio denominato "top-down" oppure effettuare una ricerca diretta del
simbolo.

Se avessimo invece un riassunto (abstract) di un brevetto come facciamo a
classificarlo? Quello che si può fare è copiare tale testo e utilizzo il
sistema di ricerca integrato (search) incollando il testo. Verranno indicizzati
alcuni codici di classificazione e cliccando posso vedere le definizione di
ciascuno.

La cosa migliore da fare quando si ha a che fare con un testo è cliccare su
IPCCAT che fornisce una predizione su una base di un testo anche abbastanza
ampio (articolo). Impostando anche un determinato numero di predizioni e
ottengo i risultati.

## Slide 28

Regole di classificazione:

- Viene effettuata dagli esaminatori che classificano le rivendicazioni
  attribuendo un codice di classificazione considerando sempre il livello
  gerarchico più basso possibile. Questa regola è nota con il nome di **Last
  Place Rule**.
- un simbolo di classificazione non è cumulativo: l'insieme di brevetti
  classificati ad un livello gerarchico più elevato non include i brevetti
  appartenenti ad un livello gerarchico più basso.

## Slide 29

Gerarchia, un esempio

consideriamo l'esempio dell'ossido di grafene (graphene). Andando a
identificare il codice di classificazione per questo materiale ritroviamo C01B
32/198. Notiamo che accanto al nome si trovano tre asterischi che indicano il
livello di gerarchia (maggiore il numero più basso è il livello gerarchico). Il
codice di classificazione dipende da quello situato nella parte superiore
avente un numero di asterischi inferiore a quello considerato, in questo caso
dal grafene (C01B 32/182). Quindi la lettura ha da intendersi in questo modo:
materiali nanostrutturati > grafene > ossido di grafene.

Se l'esaminatore classifica una domanda di brevetto con il codice associato
all'ossido di grafene significa che la parte inventiva delle rivendicazioni è
riferita all'ossido di grafene. Se poi volessi fare una ricerca più completa
dovrei considerare anche i codici con livello gerarchico più alto.

## Slide 30

Si consideri l'esempio del grafene (C01B32/182). Per la Last Place Rule
l'esaminatore dovrebbe classificare la domanda di brevetto associata con il
codice identificativo qui riportato. Ma se in una domanda di brevetto non ci
fosse solo il grafene ma anche i nanotubi di carbonio (livello gerarchico più
alto, meno asterischi) allora dovrebbe utilizzare entrambe le classificazioni.
è però più probabile che l'esaminatore, utilizzi, al posto della combinazione
del codice di entrambi, quello associato al livello gerarchico immediatamente
precedente a quello dei nanotubi (nano-sized carbon materials C01B32/15). 

Dal punto di vista della ricerca brevettuale si può proseguire considerando la
combinazione di questi codici (nanotubi and grafene). è anche possibile fare
un'ulteriore ricerca sul livello gerarchico "nano-sized carbon materials",
utilizzando delle parole chiave ("carbon nanotubes" and "graphene").

## Slide 31

Esempio 2: Preparazione del grafene mediante CVD. Esistono quindi due diversi
concetti: il grafene come sostanza chimica e il processo di sintesi mediante
CVD.

Vado a vedere come è classificato il grafene. La slide mostra molto bene le
porzioni del codice associate a sezione, classe, sottoclasse, gruppo e
sottogruppo. Utilizzando la combinazione del codice del grafene e del codice
relativo alla preparazione del grafene mediante CVD posso effettuare la query.
Questo per quanto riguarda la classificazione cooperativa. Se volessi
effettuare una ricerca completa è necessario includere la classificazione
internazionale dei brevetti (IPC): è necessario effettuare un'analisi nel
database IPC per ritrovare il codice di classificazione del grafene e della
CVD. Il database potrebbe presentare un avviso che indica la riclassificazione
della documentazione minima PCT, quindi è necessario verificare com'erano
classificati prima. Cliccando su RCL infatti si rimanda ad una tabella che
mostra come è stata effettuata la riclassificazione. Per avere dei risultati
che comprendono i documenti non riclassificati basti aggiungere alla query
anche i codici presenti nella versione non aggiornata.

## Slide 35

Come trovare i codici di classificazione cooperativa CPC? utilizzando la banca
dati Espacenet nella parte classification search. Inserendo delle keywords
posso ritrovare i vari codici di classificazione che poi dovrò andare a
verificare prima di farne uso.

Come trovare i codici di classificazione internazionale IPC? Utilizzando il
sito della WIPO.

## Slide 38

Esistono banche dati gratuite fornite dagli uffici brevetti nazionali (EPO,
USPTO, JPO etc) e produttori indipendenti (Google). Sono utilizzate ad uso
esplorativo.

Esistono anche risorse professionali che danno la possibilità di fare ricerca
più facilmente per quanto riguarda l'analisi dei dati. Utilizzate per ricerche
complete per lavoro.

## Slide 41

### Note

NPL: non patent literature.

## Slide 42

Documentazione minima PCT: definita dalla WIPO come il numero minimo degli
documenti da utilizzare per fare ricerca di prior art con lo scopo di valutare
novità e invenzione (ricerca di brevettabilità).

## Slide 45

Nella parte superiore di Espacenet è riportata la smart search, strumento che
tramite opportuna sintassi è in grado di ricercare all'interno del database.

## Slide 51

Su Espacenet è possibile utilizzare i vari operatori booleani. L'AND è
l'operatore di default se viene omesso.

## Slide 52

Questa è la guida alla sintassi di Espacenet. Sono da notare gli operatori di
prossimità e quelli di confronto sulla destra.

## Slide 53

Operatori di troncamento (wildcard):

- asterisco: stringa di caratteri di lunghezza qualsiasi
- '?': 0 o 1 carattere
- '#': esattamente 1 carattere.

## Slide 55

Una gran limitazione è quella che non si possono ricercare documenti di
letteratura scientifica (quelli inseriti poi dall'esaminatore con acronimo XP).

## Slide 56

Un esempio: analisi di prior art per identificare i trend globali del caso di
batterie, idrogeno e bioenergia.

## Riferimenti bibliografici

Slides: 
- Esercitazione: "Strategie brevettuali" 
- Lezione: "ricerche prior art"
- Esempi: "Esempi di ricerche"
