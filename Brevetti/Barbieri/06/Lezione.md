# Lezione 6 --- Ricerche brevettuali ed esercitazioni

## Slide 2

In che documenti si ritrova il rapporto di ricerca europeo?

1. domande di brevetto contrassegnate col codice A1, A3, A4. Non si trovano nei
   brevetti europei concessi. Il codice A1: testo della domanda di brevetto
   pubblicato con rapporto di ricerca. A3: frontespizio + rapporto di ricerca.
   A4: rapporti di ricerca supplementari, effettuati in particolari condizioni.
   Una di queste potrebbe essere quella di un applicant statunitense che fa una
   domanda internazionale PCT. questa domanda è analizzata dall'USPTO e l'EPO
   fa un'ulteriore ricerca emettendo un rapporto con codice A4.
2. domande di brevetto depositate in europa e quelle internazionali PCT dove il
   richiedente fa parte degli stati membri della convenzione sul brevetto
   europeo.
3. alcune delle domande di brevetto nazionali (i.e. Italia).

## Slide 3

Ulteriori tipologie di rapporti di ricerca sono:

1. supplementari: domande PCT non esaminate dall'epo
2. aggiuntivi: quando un rapporto di ricerca viene emesso in forma non completa
   o se le rivendicazioni sono state modificate in estensione internazionale o
   in fase regionale europea. Si fa un rapporto di ricerca aggiuntivo per
   esaminare le nuove rivendicazioni oppure prechè l'esaminatore ha giudicato
   la domanda di brevetto manchevole di inventività.

## Slide 4

Categorie di documenti citati (i primi tre sono i più importanti):

- X: rilevanti per requisito di novità o attività inventiva e questo è
  specificato nell'opinione scritta
- Y: molto rilevanti per attività inventiva. Nell'opinione scritta si troverà
  sempre due documenti con la Y. è la combinazione di questi due documenti che
  rende la domanda carente di attività inventiva.
- A: descrivono lo stato della tecnica ma non sono considerati pregiudizievoli
  per i requisiti di brevettabilità
- P: articoli e domande di brevetto pubbicate dopo la data di priorità ma prima
  della data di deposito. Sono pubblicati tra la data di deposito della domanda
  in esame e la sua priorità. Questi documenti sono rilevanti per i requisiti
  solo se la priorità non viene accettata dall'esaminatore (i.e. sono state
  modificate le rivendicazioni in fase di deposito della domanda). Se la data
  di priorità viene annullata allora questo tipo di documenti diventa
  rilevante. Si fa riferimento alla pubblicazione. Possono essere articoli
  scientifici, documenti tecnici e domande di brevetto.
- E: domande di brevetto europee pubblicate dopo la data di deposito della
  domanda in esame ma hanno una data di priorità o di deposito che è precedente
  a quella del documento in esame, quindi sono pregiudizievoli per i requisiti
  di brevettabilità.
- T: articoli scientifici che spiegano le basi teoriche
- O: divulgazioni orali.
- L: documenti generici (i.e. un catalogo tecnico che descrive le
  caratteristiche di questi prodotti e queste sono accessibili al pubblico
  prima della data di deposito della domanda, quindi risulta pregiudizievole
  per il requisito della novità).

## Slide 8

Le ricerche parziali:

A volte queste sono parziali 
1. perchè l'esaminatore vede che sono state scritte troppe rivendicazioni
   indipendenti nella domanda (apparato, metodo, processo, prodotto)
2. quando non è possibile effettuare una ricerca completa perchè le
   rivendicazioni non sono chiare (gli elementi tecnici essenziali
   dell'invenzione) nemmeno leggendo la descrizione della domanda di brevetto.
3. quando la domanda manca di unità inventiva. in questo caso l'esaminatore
   valuta il primo gruppo di rivendicazioni e gli altri gruppi li lascia come
   domande divisionali.

## Slide 9

È possibile per il richiedente, pagando per ogni gruppo di invenzioni una tassa
di ricerca, avere una ricerca completa. 

## Slide 12

In alcuni rapporti di ricerca si possono trovare delle informazioni sulla
strategia che l'esaminatore ha utilizzato. Questo è utile nel caso di
opposizioni.

## Slide 14

Vediamo ora i codici di classificazione. Come ricercarli nei vari database.
Esistono due tipi di classificazioni: 

IPC: stabilita dall'accordo di Strasburgo del 1971 da 62 paesi. Sebbene
limitato l'insieme di paesi questo è stato adottato dalla maggior parte degli
uffici nazionali e dai quattro uffici regionali. Viene aggiornata ogni anno e
la pubblicazione avviene il primo di ogni anno.

I metodi per poter ricercare i codici IPC sono tre:

1. approccio top down: poco utilizzato. Si parte dallo schema di
   classificazione scendendo sempre di più nel dettaglio coi livelli
   gerarchici.
2. con un indice chiamato "catchwords" in cui sono indicati i termini in un
   eleenco non esaustivo, quindi non è utilizzato.
3. attraverso un sistema di ricerca che offre quattro opzioni:
	1. uso di alcuni termini
	2. cross-references (aka riferimenti incrociati)
	3. STATS: questo e il successivo ricercano le parole chiave nelle
	   domande di brevetto internazionali. STATS si riferisce ai codici di
	   classificazione più utilizzati nel database di riferimento
	   (patentscope).
	4. IPCCAT: effettua una ricerca sulle domande di brevetto. Questo è il
	   più utilizzato perchè se ho un riassunto (abstract) di parole chiave
	   ottengo una serie di codici di classificazione. 
	5. utilizzo il sito del JPO che non solo consente la ricerca dei codici
	   IPC ma anche CPC e FI.
  

## Slide 18

CPC è un sistema di classificazione sviluppato da due uffici (EPO e USPTO) che
hanno voluto armonizzare le pratiche utilizzate da entrambi gli enti. è una
classifica attiva da poco ma consente una maggiore efficienza nel recupero
dell'informazione perchè contiene più di 250000 simboli, viene classificata
tutta l'informazione tecnica e non solo le rivendicazioni. La frequenza di
revisione del codice è di 4 volte l'anno. Esistono delle tabelle di concordanza
che associano codici di classificazione IPC a quelli CPC.

Come ricercare i codici CPC:

- su espacenet
- pagin ufficiale della classificazione cooperativa.
- JPO
- USPTO (non molto user-friendly)

## Slide 24

Citazioni brevettuali, quali sono e come utilizzarle

Ce ne sono di due tipi:

- Backward: quelle contenute nel rapporto di ricerca del documento stesso
- Forward: quelle successive, nei rapporti di ricerca o nella descrizione di
  domande di brevetto che sono state pubblicate successivamente e richiamano
  invenzioni precedenti

Vengono utilizzate per calcolare dal punto di vista qualitativo il valore di un
brevetto. Se ho un elevato numero di citazioni successive allora è probabile
che quella tecnologia sia importante. può essere utile nella fase di licensing
ossia di trasferimento tecnologico ma in questo caso è necessaria una banca
dati che permette di discriminare la tipologia di citazioni (A, X, Y cambia di
molto).

Per trovare le informazioni su espacenet possiamo utilizzare l'operatore "ct"
associato al numero della domanda di pubblicazione o di brevetto per ottenere
le citazioni.

# Esercitazione 1

Setup di Espacenet. In MyEspacenet > MySettings abilitare "Enable query
history" selezionando dal menù a tendina 100 elementi. Nella home, abilitare
"Advanced search": Andando a compilare la smart search i campi vengono
automaticamente riempiti anche nell'advanced a lato.

## Esercizio 1

Trovare i brevetti depositati da Novartis pubblicate nel 2013 dove compare la
parola "diabetes" nel titolo.

### Soluzione

Nella smart search: pa=Novartis AND pd=2013 AND ti="diabetes"

## Esercizio 2

Trovare brevetti pubblicati nel 2012 negli stati uniti o in canada in cui
l'applicant è Samsung e le parole "dual sim" si ritrovano nel titolo o
nell'abstract (utilizzando Advanced search).

### Soluzione

pa=Samsung AND pd=2012 AND ta="dual sim" AND (pn=us OR pn=CA)

### Note

Il trucco per poter localizzare il brevetto è sfruttare il fatto che il patent
number inizi con il simbolo del paese di interesse.

## Esercizio 3

Trovare il brevetto con application number (numero di domanda)
"10-2012-7021522".

### Soluzione

Nella smart search: ap=10-2012-7021522

### Note

Non è necessaria alcuna sigla inizialmente ma solo un copy-paste del numero di
domanda.

## Esercizio 4

Trovare tutte le domande pubblicate dall'Ufficio Brevetti svizzero tra il mese di gennaio e febbraio 2013.

### Soluzione

pn = "ch" AND pd within "201301 201302"

### Note

pd within "20130101 20130228" è equivalente a scrivere (pd=201301 OR pd=201302)

---

trovare tutte le demande di brevetto depositate da Apple pubblicate tra il 1 gennaio 2012 e il 15 Marzo 2012

### Soluzione

pa = "Apple" AND pd within "20120101 20120315"

## Esercizio 5

Trovare tutte le domande di brevetto depositate da Siemens Aktiengesellshaft
tra gennaio 2010 e maggio 2010 con una domanda di priorità negli USA.

### Soluzione

pa = "Siemens Aktiengesellshaft" AND pd within "201001 201005" AND pr = "US"

### Note

Il risultato non fornisce alcun documento, errore del professore nella
formulazione.

---

Trovare tutte le domande di brevetto depositate da Siemens Aktiengesellshaft o
da Siemens Medical pubblicate nel 2010 o con un una priority application in USA
o CA.

### Soluzione

(pa = "Siemens Aktiengesellshaft" OR pa = "Siemens Medical") AND (pd = "2010" OR (pr = "US" OR pr = "CA"))

## Esercizio 6

Trovare il search report di EP3527675, la corrispondente opinione scritta e la
prosecuzione della domanda.

### Soluzione

1. Trovare il brevetto con quel numero digitando num=EP3527675
1. Navigare su "original document" > "Search report" per ottenere l'opinione
   scritta.
1. Navigare su "Bibliographic data" > "Register" > "All documents" per
   visualizzare i documenti di prosecuzione della domanda.


## Esercizio 7

Trovare il seguente brevetto: The present invention proposes novel materials
and methods, for preparation of coatings based on titanium dioxide for
osteointegrated biomedical prostheses. The coatings are realized with
nanomaterials having antibacterial properties, and have the purpose of :
promoting osteointegration of the implants, and, at the same time, reducing
rejection attributable to inflammatory processes which derive from infections
which may develop in the neighborhood of the implants.  In particular, the
present invention relates to an endo- osseous implant comprised of
biocompatible metallic materials; characterized in that said implant comprises
a coating comprised of nanocrystalline material comprising nanoparticles of
formula `$(\text{I}) \text{AO}_x- (\text{L}- \text{Me}^{n+} )\text{i},
(\text{I})$` where `$\text{AO}_x$` represents `$\text{TiO}_2$` or
`$\text{ZrO}_2$`; `$\text{Me}^{\text{n+}}$` is a metallic ion having
antibacterial activity, with n = 1 or 2; L is a bi- functional organic molecule
which can simultaneously bind to the metal oxide and to the metallic ion
`$\text{Me}^{\text{n+}}$`; and i is the number of L-`$\text{Me}^{\text{n+}}$`
groups bound to one nanoparticle of `$\text{AO}_x$` .

### Soluzione

1. Effettuare un copy-paste sul motore di ricerca WIPO per trovare dei codici
   di classificazione. Utilizzo lo strumento IPCCAT. Scelgo il codice di
   classificazione che è stato utilizzato maggiormente ma non lo considero in
   termini di sottogruppo ma a livello di sottoclasse o di gruppo principale.
2. Noto che è ricorrente il A61L27 quindi comincio a scrivere ipc="a61l27"
   nella ricerca espacenet.
3. Analizzando il testo ritrovo delle keyword in grado di limitare
   ulteriormente l'ambito di ricerca quali "osteointegrated prosthesis"
   (possono anche trovarsi distanziate quindi utilizzo any), "ZrO2" e "titanium
   dioxide"

la stringa di ricerca complessiva è la seguente: ipc = "A61L27" AND ctxt =
"titanium dioxide" AND ctxt any "osteointegrated prosthesis" AND ctxt = "ZrO2"

il risultato è il seguente: NANOMATERIAL COATINGS FOR OSTEOINTEGRATED
BIOMEDICAL PROSTHESES.

# Esercitazione 2

## Esercizio 1

Trovare le forward citation dato il titolo di una famiglia brevettuale.

### Soluzione

1. ti="MODE MULTIPLEXING OPTICAL COMMUNICATION SYSTEM"
2. Seleziono il primo risultato
3. navigo su patent family per visualizzare la famiglia brevettuale semplice e
   allargata (INPADOC)
4. navigo su Citations > "Citing documents" per visualizzare le forward citing.
   Essendo molte è necessario un ulteriore step
5. ct=US2014199066A1 per visualizzare tutti i documenti che citano quello in
   esame
6. non ho finito perchè devo effettuare la stessa ricerca per ogni membro della
   famiglia brevettuale! Quindi ritorno in "patent family" e reitero questi
   punti per ogni documento.

### Note

Bisogna tenere a mente che per come è strutturato il database Espacenet non
vengono indicizzati i brevetti raggruppati per famiglia quindi è necessario
fare questa ricerca in modo manuale. Esistono altri database in grado di
elaborare query estesa sulla famiglia brevettuale.

## Esercizio 2

Trovare tutti i membri della famiglia brevettuale di quest'invenzione.

### Soluzione

ti="Adducts between carbon allotropes and serinol derivatives"

### Note

è possibile vedere lo stato legale di un brevetto accedendo al registro dalla
bibliografia. Infatti facendolo a partire dal brevetto europeo che abbiamo
ricercato possiamo visualizzare le convalide nei vari stati europei e gli stati
legali di questo brevetto.

---

Trovare tutti i membri della famiglia brevettuale di quest'invenzione (+ alcune
informazioni extra ma non necessarie).

### Soluzione

1. ti ="Osteointegrative interface" AND pa="Politecnico di Milano" AND in="Roberto Chiesa"
2. navigo su "patent family"

## Esercizio 3

Albero delle rivendicazioni. Data una domanda di brevetto trovare le
rivendicazioni indipendenti e quelle dipendenti. Esiste anche una forma
grafica.

### Soluzione

1. utilizzare la versione classica di Espacenet
2. num=EP3146537
3. navigo su claims > "claim tree"

### Note

Il diagramma ad albero ha al livello più alto la/le rivendicazione/i
indipendenti e al di sotto quelle dipendenti. La visuale aggiornata di
Espacenet non è del tutto corretta quindi è necessario, per poter avere una
visualizzazione corretta del claim tree, utilizzare la versione classica di
espacenet

## Esercizio 4 --- Legal status

Trovare lo stato legale del brevetto numero EP2004312. Usare INPADOC e il
Patent Register.

### Soluzione

1. pn=EP2004312
2. navigo su "Bibliographic data" > Register > "Legal status".
3. Scopro che il brevetto è stato dismesso per mancato pagamento delle tasse di
   rinnovo.

---

Trovare il brevetto numero US2015051280. è stato concesso? è ancora attivo?

### Soluzione

1. pn=US2015051280
2. scopro che è concesso dalla sigla B2 nella barra laterale del documento
   (oppure controllando in "Legal Events")
3. ricerco il documento originale e copio l'application number (14/391504)
   all'interno del [database USPTO](https://portal.uspto.gov/pair/PublicPair).
4. Andando sulla sezione "fees" ho delle informazioni dettagliate sui pagamenti
   si scopre che le tasse sono state pagate correttamente e che si dovrà pagare
   a fine anno la fee.

## Esercizio 5

Trovare una domanda di brevetto partendo dal testo della prima rivendicazione.

1. A working fluid for a thermodynamic cycle which comprises CO2 as main
   component and one or more of the compounds selected from the group which
   comprises: TiCl4, TiBr4, SnCl4 , SnBr4, VCl4 , VBr4, GeCl4, metal carbonyls,
   by way of example Ni(CO)4 .

### Soluzione

1. ctxt all "working fluid thermodynamic cycle co2" 
2. CO2-BASED MIXTURES AS WORKING FLUID IN THERMODYNAMIC CYCLES è il documento
   che stiamo cercando

### Note

è possibile limitare ulteriormente la ricerca ricercando con IPCCAT i codici di
classificazione utilizzati dal brevetto.  Il documento lo si trova subito
utilizzando questa stringa di ricerca: ctxt all "working fluid thermodynamic
cycle co2 sncl4" AND ipc = "F01K25".

## Esercizio 6

L'Amuchina è un marchio italiano. Quali sono i brevetti con questo marchio?
sono ancora in vigore? utilizzare internet per ricercare informazioni riguardo
la composizione, espacenet e UIBM per ricercare i brevetti.

### Soluzione

1. con un normale motore di ricerca trovo il sito di Amuchina. Scopro che si
   tratta di ipoclorito di sodio e che l'azienda produttrice è l'Acraf (Angelini).
2. pa = "Acraf" AND ctxt all "sodium hypochlorite"
3. trovo due brevetti di cui uno è un disinfettante a base di ipoclorito di
   sodio e l'altro è l'idrogelo utilizzato comunemente.

## Esercizio 6

Trovare tutti i codici di classificazione CPC e IPC associati alla stampa 3D

### Soluzione

Utilizziamo classification search all'interno di Espacenet con le parole chiave
"3D printing"

## Esercizio 7

Trovare il brevetto numero EP0174696. Ha subito un'opposizione? se affermativo
scoprire la devisione della Board of Appeal.

### Soluzione

1. num=EP0174696
2. Nella sezione "about this file" è visibile che questo brevetto è stato
   revocato.
3. Nella sezione "all documents" si va a vedere le informazioni relative
   all'opposizione (con tag "opposition").

## Ultimi esercizi

Son da fare per casa. Le soluzioni sono riportate poi su BeeP.

## Riferimenti Bibliografici

- Lezione: "Lezione 3_prior art"
- Esercitazione 1: "Esercizi-281-29"
- Esercitazione 2: "Esercizi-282-29"
