# Lezione 8 --- Tutela dei marchi e design

## Slide 2

- Brevetto: Si tratta di documenti legali che tutelano un'invenzione tecnica
  (soluzione ad un problema tecnico). I brevetti sono territoriali (validi
  nello stato in cui è effettuato il deposito ed è stato concesso il titolo) e
  limitati temporalmente (20 anni).
- Modello d'utilità: tutelano un'innovazione più che un'invenzione perchè si
  riferisce ad una forma o struttura di un prodotto conferendone una
  particolare utilità.
- disegno industriale: docmento legale che copre le caratteristiche estetiche
  di un oggetto con utilità pratica.
- Marchi: parole, frase, disegni che identificano l'origine di un prodotto.
  Servono a differenziare i prodotti di un'azienda da quelli di un'altra.

## Slide 3

Un riassunto delle informazioni importanti delle proprietà intellettuali:

| Tipologia         | Requisiti                       | Durata (anni)                    |
| :---              | :---                            | :---                             |
| Brevetto          | Novità, attività inventiva      | 20                               |
| Modello d'utilità | Novità, praticità d'innovazione | 10                               |
| Disegno           | Novità, carattere individuale   | 25 (rinnovo ogni 5 anni)         |
| Marchio           | Novità, carattere distintivo    | 10 (rinnovabile indefinitamente) |

## Slide 4

Design

Possiede alcune caratteristiche che i brevetti per invenzioni e modelli
d'utilità non hanno:

- Periodo di grazia di 12 mesi in EU. è possibile esporre il prodotto in una
  fiera, divulgare le caratteristiche ma il titolare può depositare la domanda
  di brevetto di disegno industriale entro questi 12 mesi.
- Differimento della pubblicazione di 30 mesi dalla data di registrazione.
- Design non registrato: è possibile esporre in fiera prodotti e divulgare le
  caratteristiche e ottenere una tutela limitata (3 anni dalla data di
  divulgazione) dalla copia pedissequa (senza originalità).
- Il design può essere registrato presso un ufficio nazionale o comunitario.
- I design o modelli comunitari non sono esaminati ma esiste solo una verifica
  formale.
- La verifica dei requisiti (novità e carattere individuale) è effettuata da
  EUIPO solo dopo un processo di nullità che può essere effettuato da chiunque
  o dai tribunali nazionali dopo la registrazione.

## Slide 5

Cosa si valuta nel design?

- Forma esteriore del prodotto.
- Novità: in europa non è assoluta ma relativa a quanto è stato divulgato nei
  paesi dell'EU.
- Carattere individuale: giudizio d'impressione condotto da un utilizzatore
  informato (non è nè designer, nè un tecnico del settore). è conferito grazie
  alla particolare forma del modello o combinazione di linee colori o altri
  elementi grafici
- è possibile fare un deposito multiplo: ottenere la tutela su più modelli
  (fino a 100 modelli), l'importante è che questi siano nella stessa classe
  merceologica.

## Slide 7

Disegni e modelli non registrati:

- Il diritto nasce per effetto della pubblicazione e sussiste per 3 anni.
- Si tratta di tutela contro la copia pedissequa.
- Tutela prodotti di vita commerciale breve (moda, tessile, giocattoli).

## Slide 8

è possibile ricercare disegni e modelli dal sito UIBM con molte limitazioni.

## Slide 9

Esistono altri database ben forniti tra cui "eSearch plus" e "Designview".

## Slide 15

Marchi

Segno distintivo che consente di distinguere i prodotti di un'inpresa da quelli
di un'altra.  Possono essere registrati segni, parole, disegni, lettere, suoni
forme. Il segno è l'elemento distintivo del marchio in quanto trasmette un
significato al consumatore. Ne esistono di varie tipologie.

## Slide 17

I marchi notori hanno una tutela più ampia rispetto alle classi merceologiche
in cui viene effettuata perchè essendo marchi famosi chi non è titolare del
marchio potrebbe ottenere un vantaggio competitivo che non è dovuto. Quindi
Messi, in questo caso, ha ottenuto un vantaggio rispetto all'azienda che già
aveva registrato quel marchio per via della sua notorietà.

# Seminario Saguatti --- Marchio

## Slide 2

è un avvocato dal '93. dal '98 è entrata nello studio Torta in campo di PI nel campo della prosecution (depositi di PI e la consulenza che sta attorno alle PI). Da allora si è sempre occupata di marchi, nomi a dominio, contraffazione.

Studio Torta: nasce nel 1879 dai fratelli Torta e questo era costituito da 3 soci e poi è divenuta una SPA (chi entra può diventare partner dello studio). Vari settori sono coperti da questo studio con problematiche molto diversificate.

## Slide 3

Argomenti trattati:

- Intro sul marchio
- fasi dalla creazione alla registrazione
- requisiti di legge
- sorveglianza

## Slide 4

Funzione del marchio: indicatore d'origine. distinguere un prodotto/servizio
proveniente da un'impresa rispetto a quello proveniente da un'altra.

Qual è la differenza tra marchio e brand? Vedendo il marchio Ferrari a tutti
viene in mente l'indicatore d'origine ma suscita magari delle emozioni, il
senso di appartenenza ad una squadra che corre in F1. Questo marchio non è solo
un'indicatore d'origine ma con la comunicazione, il marketing e l'esperienza
del consumatore questo marhcio si arricchisce di significati e diventa veicolo
di trasmissione di diversi messaggi. Questo segno ricco di informazioni,
messaggi e in grado di suscitare delle emozioni nel consumatore è denominato
"Brand" (o "Marca") dal punto di vista commerciale. Dal punto di vista legale
invece si parla di "Marchio" e di "indicatore d'origine".

Ad un marchio viene richiesto legalmente di distinguere un prodotto da altri
soggetti.

## Slide 10

Quando si crea un nuovo prodotto la prima cosa a cui si pensa è il nome di
questo e al nome dell'azienda/attivita.

### Step 1 --- creazione del marchio

Quali sono i principi base di un marchio?
Partiamo definendo quali sono i segni non registrabili come marchio per la legislazione:

- Segno generico (nome comune del prodotto). i.e. Super, Plus, Iper, lettere e
  forme non complesse.
- Segno descrittivo (descrive caratteristiche, qualità, provenienza geografica
  etc). i.e. quick-gripp.
- Segni ingannevoli. i.e. "caffè veloce" era stato utilizzato per tè e cacao
  quindi traeva in inganno.
- Contrari all'ordine pubblico e al buon costume. i.e. "La Mafia" offende i
  valori fondamentali della società civile e quindi non registrabile.

Possiamo creare marchi forti o deboli a seconda dei requisiti che vogliamo
rispettare.

- Marchi forti: ottengono una tutela ampia. Quindi anche marchi simili ma non
  troppo a quel marchio possono essere considerati in contraffazione del
  marchio forte.
	- Marchi di fantasia o inventati, che non hanno nessun significato e
	  nessun'attinenza con servizio che vanno a contraddistinguere. Hanno
	  una forte attività distintiva, per cui il legislatore premia questo
	  sforzo tutelandolo più ampiamente
	- Marchi arbitrari: hanno un significato che però non ha attinenza
	  concettuale con i prodotti/servizi che vanno a contraddistinguere.
	  Amazon si ispira al rio delle Amazzoni, quindi ha un significato in
	  se ma non ha nulla a che fare con i servizi che Amazon vende. Amazon
	  ha voluto questo nome pensando al Rio delle Amazzoni, questo grande
	  fiume che attraversa il Sud America, che trasporta di tutto e di più.
	  La freccia del marchio sembra un sorriso ma in realtà indica dalla a
	  alla z. Si premia l'originalità dell'azienda che, pur prendendo una
	  parola con un significato, l'ha associata a dei servizi che non hanno
	  nulla a che fare con esso.
- Marchi deboli: ottengono una tutela ristretta. Sono sufficienti anche piccole
  variazioni rispetto al marchio anteriore per uscire dalla contraffazione.
  Bisogna comunque fare attenzione alla confondibilità.
	- Marchi suggestivi: Non sono completamente distintivi altrimenti non
	  registrabili ma che suggeriscono alcuni attributi e vantaggi del
	  prodotto. i.e. Benagol (fa passare il mal di gola). Patasnella
	  (patatina con meno grassi quindi più dietetica). Viene riconosciuta
	  la tutela per il minimo sforzo inventivo (in forma più ridotta).
	- Marchi descrittivi: descrivono il prodotto. i.e. Facebook che ricorda
	  un libro con le facce, piattaforma in cui erano presenti tutte le
	  foto e le informazioni degli studenti dell'università. Questo marchio
	  è diventato forte ma è nato come marchio debole.

## Slide 19

Esistono marchi descrittivi che con l'uso, man mano che acquistano notorietà, possono diventare da deboli a forti. è ovvio che per aziende che partono con un marhcio debole è necessario un grosso investimento in termini di comunicazione e pubblicità per renderlo più noto possibile e per acquisire quella forza che non aveva in partenza quel marchio. Coca cola inizialmente era un marchio descrittivo che riguardava una bevanda contenente coca ed estratti della noce di cola, due sostanze presenti in essa. Questo marchio tramite l'uso e la notorietà ha assunto un cosiddetto "secondary meaning" e da descrittivo è diventato un segno con funzione di marchio e consente al pubblico di riconoscere come proveniente da quell'azienda.

Esistono marchi forti che possono diventare deboli per via dell'uso. Questo accade quando diventa il nome comune utilizzato da tutti relativo a quel prodotto. Se uno inventa un prodotto nuovo che non ha un nome comune e viene lanciato un marchio si rischia che il pubblico utilizzi poi il marchio per fare riferimento al nome comune per quella tipologia di prodotto. i.e. Post-it, Cellophane, Scottex. La volgarizzazione comporta questo e quindi la registrazione potrebbe essere annullata e il titolare potrebbe perdere i diritti di esclusiva su quel marchio perchè ormai tutti lo utilizzano. Il titolare può evitare la volgarizzazione nonostante le persone continuino ad utilizzarlo come uso comune e dimostrare di aver fatto di tutto per evitare la volgarizzazione. La R di copyright serve alle persone per riconoscere che quella parola è un marchio registrato e non di uso comune.

## Slide 23

### Step 2 --- Ricerche d'anteriorità

Esistono delle forme che non sono registrabili. Essi sono i segni costituiti
esclusivamente:

- Dalla forma o altra caratteristica imposta dalla natura stessa del prodotto.
  (i.e. non posso registrare la forma di una banana per una banana).
- Dalla forma o altra caratteristica del prodotto necessaria per ottenere un
  risultato tecnico. (i.e. un braccialetto utilizzato come repellente per le
  zanzare. Questo braccialetto è stato inizialmente registrato come marchio e
  un'altra azienda ha iniziato un procedimento di nullità sostenendo che il
  marchio non era registrabile perchè aveva una funzione ben specifica. La
  forma del prodotto aveva quindi una funzione e alla fine la registrazione del
  marchio è stata annullata. La funzione è rendere elastico il prodotto
  rendendo possibile l'adattamento su braccio e caviglia pertanto questo segno
  tridimensionale costituito dalla forma del prodotto necessaria per avere quel
  risultato tecnico non può essere utilizzata come marchio).
- Dalla forma o altra caratteristica dei prodotto che dà un valore sostanziale
  al prodotto

## Slide 27

La novità. Il concetto è diverso dai vari concetti di novità delle altre
proprietà intellettuali. Significa che il marchio che ho creato non deve essere
uguale e confondibile con i diritti anteriori di terzi. Questo significa che
devo verificare che non ci siano dei marchi anteriori uguali o simili al mio.
Queste verifiche devono essere fatte tramite ricerche di anteriorità nelle
banche dati esistenti. La ricerca ha da farsi perchè molti uffici marchi e
brevetti quando si presenta una domanda di registrazione di marchio fanno un
esame su quei requisiti detti prima, un esame formale sulla domanda ma non lo
fanno sulla novità del marchio. Quindi è il titolare del marchio anteriore che
dovrà attivarsi per garantire che non vi siano contraffattori e non l'ufficio
brevetti e marchi con procedimenti di opposizione. Questo avviene nell'ambito
dell EUIPO. USPTO, JPO fanno delle ricerche sulla novità.

## Slide 33

### Step 3 --- Classi merciologiche

Dopo aver fatto le ricerche sui database e prima di registrarlo è necessario
fare una riflessione su quali sono i prodotti/servizi per i quali lo voglio
utilizzare. Sono un'informazione richiesta al momento del deposito della
domanda di registrazione del marchio. I prodotti e servizi sono stati suddivisi
in classi secondo la classificazione di Nizza e a seconda del numero delle
classi si pagano diverse tasse.

## Slide 35

### Step 4 --- Registrazione

Creato il marchio devo creare il diritto di esclusiva, un recinto intorno al
marchio di modo da escludere gli altri dall'utilizzo di questo marchio. è il
primo passo per ottenere una tutela e potersi difendere. Essendo una tutela
territoriale devo pensare quali sono gli stati su cui voglio tutelare il
marchio. Bisogna tenere in considerazione i paesi che commercialmente sono
d'interesse e dove sto già vendendo, poi anche la Cina perchè è molto frequente
il deposito di marchi cinesi in Cina di marchi occidentali.

Strumenti per il deposito:

- Depositi nazionali (Italia, Francia)
- Marchio dell'Unione Europea (27 paesi): tutela tutto il territorio relativo
  alla UE. Questi marchi sono però unitari: se lo registro ed esiste un
  impedimento alla registrazione (perchè un terzo ha un'anteriorità in un paese
  UE) si può annullare tutto il marchio della UE.
- Marchi internazionale
- Altre convenzioni regionali

## Slide 36

La procedura di registrazione (EUIPO):

- Deposito
- Esame (no novità)
- Pubblicazione (per 3 mesi). Suscettibile ad eventuali opposizioni. Se non ce
  ne sono vi è la registrazione.

## Slide 42

Come funziona il marchio internazionale?

Si effettua un deposito di base (della nazione in cui si trova il titolare):
esempio un'azienda italiana vuole depositare un marchio internazionale --> i
requisiti sono un marchio italiano oppure un marchio UE. Il deposito viene
effettuato presso WIPO a Ginevra stabilendo in che stati si vuole registrare il
marchio. Pago in base al numero di Stati designati. L'esame è formale e subito
dopo avviene la pubblicazione del marchio e la registrazione. Dopo la
registrazione la WIPO manda una notifica agli Stati designati avvisandoli della
presenza del marchio. Dalla notifica si ha 12 mesi di tempo per esaminare il
marchio secondo la legislazione locale. Ogni stato quindi effettua la propria
istanza di esame alla fine della quale il marchio viene registrato oppure no.
In questa procedura non si parla più di marchio unitario e ogni stato registra
indipendentemente il marchio.

### Note

Tramite la procedura internazionale si può includere il marchio della UE.

### Domande

- Marchi olfattivi? In UE l'unico è il profumo dell'erba tagliata per delle
  palline da tennis. Marchio che ha sollevato molte discussioni. Il problema di
  questo tipo di marchio è che chi vede quella registrazione deve sapere
  esattamente qual è l'ambito di protezione del marchio. è molto difficile da
  proteggere perchè non è facilmente rappresentabile.
- Svantaggi del marchio internazionale: Quando si deposita deve basarsi sul
  marchio nazionale o UE precedentemente presentato. La vita del marchio
  internazionale dipende dalla vita del marchio italiano per i primi 5 anni. Se
  per qualche motivo il marchio italiano venisse annullato anche il marchio
  internazionale cadrebbe. Si parla di attacco centrale quando, entro questi 5
  anni, vado a chiedere la nullità del marchio italiano, se ho l'anteriorità.
  In questo modo annullo quello internazionale risparmiandomi l'attacco ad ogni
  singolo Stato.

## Slide 43

La registrazione in genere dura 10 anni e possono essere rinnovati
all'infinito. Facendo un rinnovo della registrazione otterrei una registrazione
illimitata.

## Slide 44

è necessario utilizzarlo un marchio per farlo sopravvivere. Se in 5 anni questo
non è mai stato utilizzato e qualcun altro deposita un marchio simile al mio e
volessi azionare il diritto e non ho le prove d'uso questo diritto potrebbe non
valere nulla.

# Seminario Andreotti --- Design

## Slide 2

Design

Mescola tanti aspetti che vanno dall'ingegneria all'analisi di mercato
all'analisi di esigenze funzionali, socio-culturali, stile e componente
artistica.

A cosa si pensa di solito quando si sente parlare di design: prodotti iconici
del settore di arredamento od oggetti di particolare forma.

Se però andassimo a vedere il Design Award categoria industry del 2018
dell'EUIPO è un sistema di angiografia di Siemens. Questo significa che il
concetto di design non viene applicato solo ad elementi caratterizzanti ma
anche oggetti comuni.

## Slide 7

Cosa possiamo proteggere col design? Non solo opere di arredamento e di design
puro di designer famosi ma anche oggetti di uso comune o di tecnologia.
L'articolo 3 è parte del regolamento di disegni e modelli comunitari.

Quest'articolo sancisce che il disegno o modello è l'aspetto di un prodotto o
di una sua parte. Per prodotto si intende un qualsiasi oggetto industriale
comprese le componenti destinate ad essere assemblate per formare un prodotto
complesso (esclusi i programmi per elaboratori). Un prodotto complesso è un
prodotto costituito da più componenti che possono essere sostituite consentendo
lo smontaggio e il montaggio del prodotto.

è da notare che nonostante i programmi non siano coperti i risultati dei
programmi lo sono. L'interfaccia utente può rientrare nel concetto di prodotto.

## Slide 17

Cosa possiamo tutelare i nuovi modelli?

Requisiti:

- Nuovo: non è identico a nessun disegno o modello divulgato prima della data
  di deposito o di priorità.
- Carattere individuale: quando l'impressione generale che suscita
  nell'utilizzatore informato differisce in modo significativo (trascurando le
  differenze che non hanno influenza su quest'impressione generale: si mettono
  a confronto il modello depositato e quello anteriore e si valuta
  l'impressione generale suscitata in chi conosce bene il mercato) da ciò che è
  stato divulgato prima della data di deposito o di priorità.

### Note

La priorità nei disegni e modelli è 6 mesi dalla data di deposito.

## Slide 18

Divulgazione

L'approccio della novità nell'ambito del design non è assoluto come nei
brevetti (in cui tutto ciò che viene divulgato è un'anteriorità rilevante ai
fini della valutazione della novità). Nel design c'è novità con approccio
assoluto ma con limitazioni:

1. Il disegno non è ragionevolmente conosciuto nel corso della normale attività
   commerciale negli ambienti specializzati del settore interessato. Esempio:
   moda: alcuni disegni tribali africani che indicano delle divinità se
   utilizzate in ambito diverso quindi con richiami in ambito di moda si
   intende che quei disegni non fossero ragionevolmente conosciuti nel settore
   specializzato perchè limitati alla divulgazione in quella zona in un ambito
   diverso da quello della moda. La novità è parzialmente relativa.
2. Periodo di grazia: il titolare del disegno può divulgare il design anche
   prima di depositarlo avendo 12 mesi in cui può divulgarlo e poi procedere al
   deposito. Quest'eccezione alla divulgazione esiste solo in alcuni paesi (UE,
   USA) e non in altri (Cina).

## Slide 19

Carattere individuale: non è semplice definire un procedimento per la sua valutazione.

## Slide 20

Col design non possiamo proteggere:

- le componenti di un prodotto complesso non visibili durante l'uso, 
- le caratteristiche determinate unicamente dalla sua funzione tecnica (non
  devo ottenere una tutela estetica su qualcosa che ha una funzione
  esplicitamente tecnica per ottenere un certo tipo di risultato). 
- Le caratteristiche che devono essere necessariamente riprodotte per
  consentire al prodotto di essere connesso meccanicamente ad un altro
  (filettature ecc).

## Slide 21

La componente di un prodotto complesso, per essere tutelata, deve rimanere
visubile durante l'utilizzazione di quest'ultimo. Inoltre le caratteristiche
visibili devono soddisfare i requisiti di novità e indivudualità.

Le parti di prodotto: sono proteggibili anche se non visibili durante la
normale utilizzazione del prodotto.

Normale utilizzazione: impiego da parte dell'utilizzatore finale (esclusa
manutenzione).

## Slide 24

In tutti gli esempi riportati di seguito si parla di diversi oggetti: reti per
cemento armato, blocchi isolanti e dispositivi impiantabili. Tutti questi sono
proteggibili perchè non vanno a costituire un prodotto complesso.

## Slide 29

La tutela attuabile col design è territoriale come per il brevetto, quindi
attuata solo nei paesi che ho designato.

## Slide 30

L'eccezione già citata è quella del design non registrato valida in Europa e in
UK.

## Slide 41

Sono ammesse domande con più domande: posso depositare il design di
un'automobile e di sue parti (sedili ecc) nella stessa domanda se tutto ciò
appartiene alla stessa classificazione di Locarno. La classificazione ha
valenza solo amministratia (ossia serve solo per capire se gli oggetti
rientrano tutti nella stessa domanda) ma differisce da quella del marchio in
quanto se depositassi il design di un prodotto questo non può essere utilizzato
anche in un diverso ambito merciologico.

## Slide 42

Esiste un design internazionale, similmente al marchio internazionale. Ha un
unico ufficio di deposito, un unica lingua. La fase centralizzata è solo quella
di deposito mentre quella di esame è diversa per i paesi aderenti.
 
## Riferimenti bibliografici

Prima parte: "Introduzione alla tutela di marchi e design"
Seconda parte: "Tutela dei marchi_ver2"
