# Lezione 3 --- Testimonianza Crippa

## Slide 2

### Il senso delle proprietà intellettuali

Che senso ha avere un sistema di tutela della proprietà intellettuale? Una di
queste cose è l'esclusiva e vietare agli altri di copiare. è lecito oppure no
copiare? Se si copia durante un esame si viene buttati fuori. perchè?
esattamente per lo stesso motivo per cui è vietato copiare scaricando la musica
da internet, perchè esistono dei diritti non registrati che nascono con la
creazione dell'opera. L'opera è il compito in classe, il brano musicale, il
video su youtube. Con la sola creazione di quest'opera si ha il diritto di
copyright. Esistono dei diritti non registrati che nascono con la creazione
dell'opera e non si può piratarli.

## Slide 4

La tecnologia lavora in una maniera diversa. Se la brevetti hai un'esclusiva,
se ha i requisiti, altrimenti è di tutti. Non brevettarla significa regalare la
tecnologia anzi è stupido non copiare la tecnologia che è liberamente
disponibile. è inutile andare a reinventare l'acqua calda. se qualcuno ha
inventato la ruota anni fa è meglio usarla.

## Slide 5

Cosa significa innovare? Non significa solo avere un'idea ma anche saperla
usare.

## Slide 6

### I Tipi di innovazione

Esistono le innovazioni:

- Incrementali: elaborate a partire da competenze interne all'azienda.
  L'azienda è composta dall'imprenditore, gli immobili, le persone, i
  macchinari, i brevetti, dalle conoscenze e idee delle presone che stanno
  all'interno. Se le persone parlano solo all'interno dell'azienda si
  ottengono, tranne in qualche raro caso, delle migliorie incrementali delle
  soluzioni.
- Radicali: si è capito, facendo innovazione, che è molto interessante avere un
  colloquio continuo con soggetti esterni all'azienda (i.e. Università,
  consulenti, fornitori, clienti, concorrenti). In questo modo delle competenze
  esterne entrano nell'azienda andando a creare un'innovazione più radicale.

Se continuassi a migliorare le candele farò sempre candele per illuminare le
case. Edison si è inventato un modo diverso di illuminare un luogo,
introducendo un'innovazione di tipo radicale. Qui è riportato il brevetto di
Edison. L'inventore della lampadina non fu Edison ma Sir Wilson Swan, Cruto ha
migliorato il dispositivo. Edison ha solo preso licenza del brevetto di Swan e
l'ha sviluppato, facendo altri 5 brevetti, sviluppando il mercato.

## Slide 7

### Innovazione vs. invenzione

Innovare non significa inventare e queste due cose sono diverse dall'invenzione
brevettata.

- *Innovazione*: Soluzione applicata e sfruttata.
- *Invenzione*: Soluzione originale ad un problema tecnico.
- *Invenzione brevettata*: un'invenzione con i requisiti definiti dalla legge
  per diventare un monopolio temporaneo.

## Slide 10

### Cos'è brevettabile

Cosa si può brevettare? Tutto ciò che è nuovo worldwide.  Anche se, in un posto
sperduto, avessero sviluppato una certa soluzione dapprima sconosciuta al resto
del mondo e riuscissero a fare una divulgazione che fosse sufficientemente
appresa dalla collettività e qualcuno negli Stati Uniti andasse a depositare la
stessa idea all'ufficio brevettuale, il brevetto negli Stati Uniti risulterebbe
nullo per mancanza di novità.

Non deve essere nuova una soluzione ma anche inventiva ossia non una
conseguenza ovvia della combinazione di soluzioni già note. L'Ufficio Brevetti
Europeo si è costruito una logica per ragionare su quello che significa
combinare documenti noti per arrivare a stabilire se esiste o meno il requisito
dell'inventività in una domanda di brevetto. Ovviamente devono essere tutte
soluzioni che hanno un'applicazione industriale.

Non sono invenzioni:

- Le mere idee (senza realizzazione pratica): Non tutte le idee possono
  diventare brevetti ma soltanto le applicazioni industriali e quelle che hanno
  i requisiti di novità e inventività. Le invenzioni che hanno un carattere
  tecnico. Non sono invenzioni le mere idee, le formule matematiche, le cose
  che non hanno una ricaduta "pratica".
- Il software: Il software di per sè non è brevettabile ma, in un certo senso,
  non è vero perché questo software fa qualcosa (e.g. muove una macchina,
  acquisisce dei dati e li elabora ecc). Se ha un effetto tecnico il software è
  brevettabile. Di per sè le righe del codice non sono brevettabili. 
- I metodi per fare business: se mi invento un modo per giocare in borsa,
  lavorare sugli edge fund e faccio milioni di euro quel metodo non è
  brevettabile
- Terapie mediche, operazioni chirurgiche e ciò che non è giusto sia bloccato.

### Note

EPC: European Patent Convention: normativa a livello comunitario che regola il
brevetto europeo.

## Slide 11

### Motivi di nullità

#### La sufficiente descrizione

L'invenzione dev'essere descritta in modo sufficientemente chiaro e completo
perché ogni persona esperta del ramo possa attuarla. Questo non è un requisito
di legge per avere un brevetto ma un motivo di nullità. La collettività trova
assurdo dare un monopolio ad un'impresa ma lo fa perché, altrimenti, nessuno
investirebbe centinaia di milioni di euro per sviluppare un nuovo vaccino se
poi il tizio di turno lo copia tale e quale e ci fa sopra i soldi perchè magari
è più bravo a produrlo e distribuirlo. è giusto che se investo in ricerca e
sviluppo ne abbia un ritorno per lo meno temporaneo. L'inventore in cambio deve
raccontare l'invenzione (è lecito quindi copiare) in modo tale che, al termine
dell'esclusiva temporanea del proprietario del brevetto, io possa copiare
l'invenzione così da poterlo sfruttare liberamente. 

Esiste l'equilibrio virtuoso del sistema della collettività tra la divulgazione
dell'invenzione (avendo una tutela temporanea nel tempo) e la possibilità di
copiare quello che non ha più una tutela temporanea nel tempo. Se vedessi
inoltre che AstraZeneca fa il brevetto di un certo vaccino, potrei studiarlo e
inventarne uno nuovo per aggirare il brevetto di AstraZeneca che risolve altri
problemi e posso percorrere varie strade parallele della tecnologia.  Questi
sono stati giudicati dal legislatore degli aspetti positivi.

Se non rispetto questo patto, racconto bugie nella descrizione brevettuale così
da impedire la copia, il brevetto è nullo. Questo accada molto di frequente in
chimica mentre non spesso in meccanica per ovvi motivi.

## Slide 12

#### Estensione oltre il contenuto della domanda iniziale

Altro motivo di nullità è se l'oggetto del brevetto si estende oltre il
contenuto della domanda iniziale.

Facciamo un esempio: Crippa è il primo ad arrivare nel territorio degli indiani
d'america a inizio '800. Pensava che questo territorio non fosse di nessuno.
Prende 4 paletti al catasto e corro nella prateria, pianto i quattro paletti e
quel territorio è mio, torno al catasto e lo registro pagando le tasse ecc.
Arrivano altre persone come secondi e come prima cosa osserviamo il territorio
che abbiamo davanti e ci accorgiamo che il territorio di Crippa non ha la fonte
dell'acqua e la miniera d'oro che si vede in lontananza. Queste persone vanno a
prendere quel territorio. Crippa si accorge di ciò, prende i paletti e li
sposta per includere entrambe le risorse. Questo non si può fare in ambito
brevettuale. Se ho deciso che il contenuto di un certo brevetto è di un certo
tipo non lo posso più modificare nel tempo. Questo è un requisito pesantissimo
perchè se combinato al requisito della novità assoluta limita tantissimo:
scegliere il momento giusto in cui fare la domanda di brevetto in un processo
di ricerca e sviluppo è difficile. Prima devo stare zitto o regolare i rapporti
con consulenti, fornitori, clienti per sviluppare bene la ricerca così che
rimanga tutto a me e non perda il requisito della novità ma dall'altra parte
devo aver capito fino in fondo l'invenzione affinchè sia completa la sua
divulgazione e non debba poi cambiarne i confini.

Ci sono due strategie per fare un brevetto:

- una buona ricerca e regolamento in maniera adeguata i rapporti coi soggetti
  che partecipano alla ricerca e sviluppo e poi brevetto quando l'invenzione è
  matura.
- brevetto a raffica. la prima, la seconda, la terza idea che mi vengono. Devo
  stare attento perchè i primi brevetti uccidono quelli che vengono dopo perchè
  diventano loro stessi una divulgazione.

Un brevetto per funzionare dev'essere pubblicato. è ovvio perchè se devo
raccontare a tutti l'invenzione significa che il sistema deve aver previsto che
la domanda di brevetto così come è stata scritta venga pubblicata affinchè
tutti vedano dove sia il monopolio e come copiare l'invenzione quando questa
cade in pubblico dominio.

Quindi quello che non posso fare è quello che succede visivamente con la
macchina sotto che prende le barriere delle carreggiate del Golden Gate e le
sposta perchè la mattina fanno 6 corsie che entrano in città mentre la sera
fanno 6 corsie che escono dalla città e molte meno che entrano, in funzione
della rush hour, di fatto cambiando i confini della carreggiata. Nei brevetti
non si può cambiare i confini della tutela durante la vita del brevetto ma solo
prima. Ci sono dei trucchi per evitare questi problemi ma hanno effetto
limitato.

## Slide 13

### Cosa non fare prima di depositare il brevetto

Cosa non si deve fare quando si intende depositare una domanda di brevetto:

1. Non posso pubblicare prima di aver fatto il deposito della domanda di
   brevetto.
2. Non posso vendere il prodotto prima del deposito della domanda di brevetto.
3. Non posso fare lezioni o presentazioni della tecnologia. A meno che lo
   faccia ad un numero ristretto di persone legate contrattualmente al
   silenzio. Questi legami contrattuali si chiamano patti di riservatezza o
   Non-Disclosure Agreements (NDA).
4. Richiedere appena possibile un brevetto e ciò è complicato quindi bisogna
   andare da qualcuno che lo sappia scrivere altrimenti si sta buttando i
   soldi.

## Slide 14

L'invenzione è una soluzione originale ad un problema tecnico. La soluzione è
un insegnamento che può essere un prodotto, un processo, un metodo, un uso (ad
esempio di una macchina utensile).

La pubblicazione brevettuale è un documento tecnico-giuridico che ha una
struttura che si è andata codificandosi nel tempo in funzione degli esami,
della legge e dell'uso della legge che è stata fatta sia a livello
amministrativo dall'ufficio brevetti europeo che dai giudici che sono stati
chiamati a valutare se i brevetti azionati posseggano o meno i requisiti di
brevettabilità.

Com'è fatto questo brevetto?  Deve dire a chi lo legge come copiare. Devo
indicare che il brevetto si riferisce ad una data tecnologia già nota che però
veniva risolta ed applicata in un determinato modo che aveva dei problemi che
non permettevano di raggiungere un determinato risultato. Io col brevetto che
propongo voglio raggiungere quel risultato e descrivo la soluzione nel
dettaglio. Chiudo il brevetto con le rivendicazioni che sono un massimo comun
divisore di tutte le forme realizzative che ho in mente perchè quello che si
protegge con un brevetto di invenzione non è una forma realizzativa ma un
principio di funzionamento di qualcosa che può essere poi concretizzato in
diversi modi. Le rivendicazioni:

- delimitano sintetizzando l'essenza dell'insegnamento, 
- definiscono un'astrazione teorica della soluzione che comprende molte diverse
  forme realizzative altrimenti il brevetto non interesserebbe a nessuno.
- includono tutte le forme possibili di realizzazioni.
- escludono le soluzioni note perchè devo essere nuovo. 

Devo mettere insieme una formulazione a a parole che sia capace di dire queste
quattro cose.

### Nota

Litigare qualcosa si dice "azionare un brevetto".

## Slide 15

Se ho una bella idea e quest'idea è una soluzione tecnica ad un problema e la
divulgo la regalo al mercato e non la posso più tutelare a meno che non tutelo
gli investimenti fatti attivando al tempo giusto gli strumenti di protezione
della proprietà intellettuale che non sono solo il brevetto ma i contratti di
accordi per fare la ricerca e sviluppo e per i primi utilizzi perchè poi nel
primo utilizzo scopro che posso migliorare la mia invenzione e voglio
depositare le varie migliorie. Il brevetto dura 20 anni dalla data di deposito
e questa durata è cambiata nella storia dei brevetti ma è consolidata in tutto
il mondo. Alcuni partivano a calcolare la durata del brevetto dal deposito e
altri dalla concessione e ciò è un problema delicato. Se sto brevettando una
formula chimica (i.e. vaccino Covid-19) e l'agenzia di autorizzazione in
emissione in commercio del farmaco (FDA) ci mette 3 anni a darmi
l'autorizzazione io ho perso 3 anni di vita del brevetto e non posso vendere in
quei 3 anni ma devo aspettare l'autorizzazione. Per questo motivo si è
uniformato a 20 anni dalla data di deposito.

## Slide 17

Edison è l'inventore del brevetto della lampadina (1880). È stato il primo
imprenditore che associò i principi della produzione di massa al processo
inventivo: aveva una fabbrica di candele e l'ha trasformata in una fabbrica di
lampadine. è stato il primo ad aver iniettato nell'azienda il principio di
innovare.

## Slide 20

Esempio che non aveva la sufficienza di descrizione. Antonio Meucci, il vero
inventore del telefono. Questo ha inventato il telefono, è andato negli stati
uniti nel 1871 depositando una provisional (domanda di brevetto temporaneo) ma
non ha mai descritto in maniera sufficiente e completa il telefono in quella
domanda di brevetto. Non ha descritto come si trasforma il movimento di una
membrana e di una bobina elettromagnetica in una corrente elettrica che può
essere poi trasmessa in un cavo, quindi non esisteva la sufficienza di
descrizione.

## Slide 21

Infatti qualche anno dopo nel 1875 è arrivato Bell che ha depositato nell'arco
di due anni ben cinque domande di brevetto.

## Slide 22

Guglielmo Marconi nel 1899 ha depositato il telegrafo senza fili. la
comunicazione radio. 

## Slide 23

Brevettare è un modo per certificare la presenza della tecnologia all'interno
di un'azienda.

- Se volessi negoziare. Se volessi comprare un'idea di qualcuno non posso
  chiedere a quel qualcuno di rivelarmi l'idea liberamente, altrimenti potrei
  andare in giro a diffonderla a tutti, non mantenendola segreta. Se però tu
  prendi l'idea e la incapsuli in un brevetto (monopolio temporaneo) e poi me
  lo vendi a quel punto l'idea diventa mia e nè tu nè qualcun altro la può
  fare. Oppure si può anche decidere di licenziarla a determinate persone per
  poterla utilizzare in determinati Stati. Quindi queste persone possono
  utilizzarla perchè possiedono il diritto monopolio temporaneo che se non
  viene rispettato possiamo andare dal giudice di un tribunale dello Stato
  chiedendo l'enforcement. Con il monopolio temporaneo nel tempo posso rendere
  negoziabile una proprietà immateriale.
- Il brevetto rende visibile perchè, siccome dev'essere una pubblicazione,
  tutti devono sapere dov'è il mio monopolio non solo per poter rispettarlo
  quando è ancora un monopolio ma perchè quando cade in pubblico dominio devono
  poterlo copiare. Ciò lo rende visibile.
- Il brevetto patrimonializza: quanto vale l'azienda senza la tecnologica
  brevettata? e con? Se il Politecnico fa una start-up e poi la vende e la
  tecnologica che si trova dentro non è brevettata e i dipendenti nella
  start-up non sono legati da un contratto di non-concorrenza che garanzia ne
  ho che sto mettendo lì dei soldi e poi la gente se ne va e ricostruisce la
  start-up da un'altra parte? zero e allora non ce li metto i soldi. Voglio
  vedere un brevetto, dopodichè mi compro la tecnologia innanzitutto e poi chi
  la sa utilizzare. La tecnologia non è mai solo il brevetto ma bisogna anche
  costruirla. Avere soltanto la licenza non basta a volte ma è necessario anche
  il know-how.

### Note

Negoziare: rendere un certo bene trasferibile a qualcuno. 

## Slide 24

Un imprenditore di Brescia ha fondato l'azienda nel '96 che poi ha venduto alla
Metronik per 500 milioni. Era un'azienda per estrudere tubi in plastica con i
quali si realizzavano componenti per cateteri e ha continuato a fare ricerca e
sviluppo prendendosi delle licenze da professori della Bayer e ha ideato un
farmaco da mettere su un catetere per portare un farmaco in posizione ben
precisa. Il brevetto più importante è quello del modo per prendere un farmaco,
metterlo su un palloncino per angioplastica, di modo che quando si gonfia il
palloncino per allargare la stenosi (l'occlusione) andando però a creare una
ferita. Questa ferita non deve cicatrizzare perchè altrimenti si crea un'altra
occlusione, quindi è necessario che un farmaco agisca riparando il tessuto
senza cicatrizzare. Sto tizio ha inventato un modo per mettere il farmaco nel
palloncino e quando attraversa tutto il percorso per arrivare dove serve non
venga "lavato via". La Metronik ha visto questa domanda di brevetto e ha
comprato l'azienda per 500 milioni di euro. Tutto questo per dire che i
brevetti conferiscono valore all'azienda.

## Slide 25

### Domande

- È possibile vendere senza l'autorizzazione di emissione in commercio? No, non
  si può vendere un prodotto farmaceutico che non ha l'autorizzazione in
  emissione in commercio dall'ente preposto (negli Stati Uniti l'FDA). La
  domanda però è giusta perchè io che ho fatto il brevetto su un vaccino dovrei
  aspettare degli anni per poterlo sfruttare mentre un altro tizio che ha fatto
  il brevetto su un qualcosa di meccanico può iniziare a sfruttarlo dal giorno
  dopo quello del deposito. La legge prevede dei correttivi, dei certificati
  complementari che vengono richiesti all'ufficio brevetti e se ci ho messo un
  numero di anni (fino a un max di 5) ad ottenere l'emissione in commercio del
  prodotto, loro estendono la durata del brevetto. Durerà quindi 20 anni + il
  certificato complementare ottenuto. Ciò non significa però che un'azienda non
  possa iniziare a preparare la produzione del vaccino stesso. Lo sputnik è già
  in produzione anche se non è stato approvato dall'EMA. L'EMA non va a
  guardare se tu hai i requisiti o meno e si inventa delle prove sue ma esiste
  una procedura ben precisa per chiedere l'autorizzazione e l'emissione in
  commercio di un farmaco con dei test fatti in un certo modo etc e sei tu
  imprenditore che devi dimostrare di avere tutti i requisiti. se riesci a
  farlo in 5 anni hai tutti i requisiti altrimenti non se ne fa nulla. Il virus
  però continuerà a circolare e nel normale vaccino per l'influenza ci
  metteranno anche quello del Covid-19.
- Non posso vendere perchè sennò non ho il requisito fondamentale della novità,
  se faccio accordi con l'acquirente per mantenere la segretezza della
  tecnologia, a quel punto posso brevettare? Dipende dal tipo di prodotto: se è
  una macchina utensile piazzata in una fabbrica e faccio un accordo di
  riservatezza col cliente affinchè quella macchina utensile sia circoscritta
  ad un area dove suoi clienti e fornitori non possono accedere alla tecnologia
  certo che sì. La stessa cosa non è valida se il prodotto va in un
  supermercato perchè non si riesce a controllare la divulgazione
  dell'informazione. Dipende tutto da come si controlla la divulgazione
  dell'informazione.

## Slide 28

Esempio di invenzione "disruptive". Benedetto vigna, un dirigente della ST
microelectronics nel 2006 ha trovato un modo per miniaturizzare un
accelerometro capacitivo. Ricordando che la carica sulla seconda armatura
dipende dalla distanza tra le due armature. Una delle due armature è fissa e
l'altra è mobile. Vigna ha trasformato una tecnologia macroscopica in una
microscopica che può essere messa all'interno di un microprocessore: gli strati
del wafer della topografia a semiconduttori e ha creato un foro nel circuito
costruendo un sensore capacitivo. Questo oggetto è dentro ogni manipolo della
Wii, accelerometri nei dispositivi elettrici etc. è un'invenzione disruptive

### Note

Disruptive: si intendono quelle invenzioni in grado di rivoluzionare il
funzionamento di un mercato o di un settore arrecando danno alle grandi aziende
consolidate preesistenti. Un'invenzione totalmente nuova.

## Slide 30

Invenzione incrementale. La draisine a inizio 1800. dei signori mollicci che
non avevano voglia di camminare si appoggiavano a quest'affare che aveva un
telaio, un manubrio e due ruote e camminando andavano in giro senza dover
sopportare il loro peso. Quasi a fine ottocento sono nate le biciclette con la
trasmissione. Questo è un esempio di telaio alleggerito e forato all'interno
per una bicicletta.

### Note

Incrementale: si cerca di innovare qualcosa che esiste già. I vari esempi di
bicicletta sono delle invenzioni incrementali.

## Slide 31

Esiste ancora al giorno d'oggi un modo per brevettare dei telai di bicicletta?
Sì. qui è mostrato un brevetto spagnolo del 2012. Si cercava di renderle
leggere il più possibile. Per risolvere il problema di doverle costruire in
carbonio (troppo costoso) hanno deciso di farla in legno orientando le fibre in
modo tale da ottimizzare la performance. Una bici totalmente in legno quindi
altamente riciclabile cambia la forma del telaio ovviamente.

## Slide 33

Cartone e resina. L'obiettivo è quello di rendere la bicicletta ancora più
economica.

## Slide 35

Dal brevetto di Edison della lampadina posso arrivare a tanti altri brevetti
della lampadina. Uno dei primi della Philips dove ha risolto un problema
fondamentale: come cambiare tutte le lampadine a incandescenza con bulbi con
all'interno dei diodi e risparmiare molto in corrente elettrica? ho tutti i
portalampade a bulbo e quindi posso sfruttarli per costruire una lampadina a
led che si inserisce in un normalissimo portalampada. 

1. Se guardiamo bene gli inventori sono tutti cinesi. Sull'elettronica i cinesi
   stanno dominando il mondo dall'inizio degli anni 2010. Il centro di ricerca
   sui LED Philips è in Cina.
2. Le domande di brevetto subiscono un esame. Questo comporta che l'esaminatore
   va a controllare se esistono i requisiti di brevettabilità, quindi se il
   brevetto è nuovo o no. L'esaminatore ha un database come Espacenet ma più
   sofisticato. Quando trovano brevetti anteriori relativi ad una certa
   tecnologia cominciano a stendere un rapporto di ricerca andando a citare
   quali sono le tecnologie più vicine a quelle che sto cercando di brevettare
   che potrebbero minare i vari requisiti. Se quindi ho una pubblicazione in
   mano e vado a guardare il rapporto di ricerca posso ricostruire la storia
   della tecnologia risalendo del tempo. Dal brevetto della Philips posso
   risalire al brevetto di Edison perchè è stato citato nei rapporti di
   ricerca.
3. Un'altra informazione utile: posso vedere chi cita il brevetto, chi sta
   sviluppando tecnologia simile alla mia. In questo modo posso vedere quali
   possono essere eventuali contraffatori o chi mi vuole aggirare. La
   pubblicazione brevettuale fornisce molte informaizoni non solo tecniche ma
   anche di strategia aziendale che possono essere utilizzate dall'imprenditore
   per costruire la sua impresa e capire come comandare la sua impresa.

## Slide 36

Si può brevettare di tutto. è stratificabile la tutela? se ho inventato un buon
telaio per una nuova bicicletta posso monopolizzare la tecnologia per fare il
telaio della bici, la trasmissione. La normativa in passato è stata dubbiosa
sulla stratificazione di più tutele sullo stesso prodotto perchè queste hanno
durate che sono diverse ma recentemente hanno chiarito in modo inequivocabile
che posso tutelare per lo stesso prodotto (i.e. la nuova moto della Yamaha) sia
la forma estetica, sia il nome, sia il segno distintivo, sia la tecnologia che
inserisco al suo interno, stratificando le tutele.

Elenco delle tutele possibile:

- Brevetto per invenzione
- Estetica del prodotto, packaging e brochure (design?)
- nome del prodotto


- Si può brevettare la combinazione di queste cose in elenco? L'aspetto
  estetico e quello funzionale insieme no. Non si può depositare una domanda di
  brevetto sul fatto che una certa forma è bella. Posso brevettare la funzione
  con il brevetto di invenzione e l'aspetto estetico con un altro strumento che
  è il modello o design. Non posso brevettare le due funzioni insieme.
  Certamente se brevetto un nuovo motore perchè ha la capacità di seguire un
  ciclo termico diverso dal ciclo Otto o dal diesel e mi invento un ulteriore
  ciclo termico certamente lo posso fare e poi magari nello stesso brevetto
  posso inserire  molti dettagli costruttivi di come è fatto questo ciclo
  termico e molte altri aspetti che possono essere invenzioni separate oppure
  invenzioni dipendenti. Dipende dalla strategia: posso fare un solo brevetto
  scegliendo l'invenzione prediletta e tutte le altre che inserisco sono
  dipendenti da quella (ossia se brevetto le due ruote della bicicletta posso
  anche nello stesso brevetto come dipendente mettere la tramissione ma se
  metto la tramissione non potrò contrastare la contraffazione della sola
  trasmissione ma quella solo delle due ruote con la trasmissione). Scelgo qual
  è il massimo comune divisore. Se questo è il ciclo Paolo Crippa e non quello
  Otto o Diesel non posso andare a dire "mi hai copiato su un camion o su un
  autovettura", peggio per te perché il camion lo faccio utilizzando un ciclo
  diverso dal tuo. "Il camion l'ho inventato io" "no, solo se accoppiato al
  ciclo Crippa".

## Slide 38

Il Design registrato è l'aspetto estetico dei prodotti. La forma estetica di un
prodotto o una parte del prodotto.

Il trateggio in gergo brevettualistico dice all'esaminatore che si vuole
tutelare la forma complessiva del telefono ma non anche delle sue icone e dei
suoi dettagli. Non è possibile tutelare qualsiasi forma estetica ma ci sono dei
requisiti:

1. Nuova worldwide. Bastano piccoli dettagli per capire le differenze
2. Carattere individuale (equivalente all'inventività ai brevetti per
   invenzione): il tecnico di settore guardando quel design coglie le
   differenze nell'impressione generale che gli dà il design.

- Criticità: anche qui la predivulgazione ammazza il design ma molte lobby
  hanno lavorato su questo punto facendo sì che la norma venisse modificata
  negli anni e si arrivasse ad un equilibrio in EU, USA etc dove se si porta un
  oggetto che viene accolto dal pubblico io ho fino ad un anno di tempo per
  andare a fare la registrazione del modello.
- I costi per fare un design sono ridotti perchè quello che domina non è più la
  scrittura a parole delle rivendicazioni ma quello che domina sono le immagini
  che inserisco nella domanda di design. Mettere un certo numero di immagini
  nella domanda di design è più facile perchè l'espressione più classica degli
  ingegneri è quella di fare un disegno senza stare lì a ragionare a parole
  qual è il massimo comune divisore di cui parlavamo prima.  I costi di tutela
  del design sono più contenuti e posso depositare tante forme diverse purchè
  rimanga nella stessa classe di deposito. Le tasse di mantenimento si pagano
  ogni 5 anni (per i brevetti invece ogni anno) e questi strumenti durano 25
  anni dal deposito.

## Slide 41

In questa slide è mostrata la cover dell'iPad. Questa ha una forma estetica
interessante, tutelata con un design worldwide tra cui un design comunitario.
Esiste nel design la tutela centralizzata comunitaria, cosa che non esiste
ancora nel brevetto per invenzione. Il brevetto europeo è una convenzione per
arrivare a un brevetto ma poi devo nazionalizzarlo, devo validarlo. Sulla
sinistra ho è il brevetto europeo sulla stessa custodia. 

Perchè un brevetto insieme al design?

La custodia ha un design diverso dal solito ma anche funzioni particolari:
questa cover permetteva grazie al fatto di avere degli elementi magnetici di
accendere e spegnere il dispositivo ed essendo segmentata con quattro diverse
porzioni potevo ripiegarle di modo tale da far diventare la cover un supporto
fisico.  Queste funzioni che non sono estetiche ma sono funzioni possono essere
tutelate grazie alle caratteristiche fisiche di come è fatta questa cover
grazie al brevetto per invenzione.

## Slide 42

Perchè l'anno di grazia? a livello comunitario si è deciso nel 2002 che
chiunque pubblichi un design che abbia i requisiti soddisfatti ottiene una
tutela automatica senza necessità di registrazione ma solo di 3 anni. Vado al
salone del mobile portando una forma nuova e la presento io otterrò
automaticamente una tutela per 3 anni. Questo qualche volta ha salvato degli
imprenditori sprovveduti che non hanno fatto tutele.

## Slide 43

Il modello d'utilità è molto simile al brevetto ma con una durata dimezzata.
Nasce per contrastare un problema fondamentale riscontrato in Germania. In
questa nazione infatti nel momento in cui si deposita la domanda di brevetto, a
differenza di ciò che accade in Italia, non posso attivare l'enforcement fino a
quando il brevetto non è stato concesso. Il brevetto per modello d'utilità
permette dopo essere stato concesso (attesa 3 mesi) di attivare l'enforcement
confidando nel fatto che la tecnologia sarà poi concessa anche come brevetto
per invenzione.

La dottrina e la giurisprudenza italiana ha scelto che i modelli d'utilità
devono tutelare delle configurazioni, conformazioni di oggetti noti
caratterizzati da una maggior praticità, comodità e facilità d'impiego.

L'esempio che si può fare è quello dello scrocco di una maniglia: il meccanismo
di apertura è ormai noto quindi non è tutelabile mediante il brevetto per
invenzione. Quello che invece è tutelabile è l'ergonomicità con la quale si
apre la porta mediante il brevetto per modello d'utilità.

Il brevetto per modello d'utilità si rifà agli stessi requisiti del brevetto
per invenzione: l'ergonomicità, la praticità, la comodità deve essere del tutto
nuova e unica.

## Slide 47

- La cover per iPad: com'è possibile che qualcosa di coperto da brevetto sia in
  realtà così copiato con merce a prezzo ridotto? Quando qualcuno commette un
  reato e ciò ha un impatto sulla collettività quest'ultima si attiva per
  contrastarlo. Quando invece si tratta di un illecito a danni del monopolio
  temporaneo di qualcuno la comunità non ha interesse a contrastarlo ma è il
  detentore del diritto che deve attivarsi per poter far valere il suo diritto
  di monopolio temporaneo. Questo si ripercuote in azioni (e quindi spese)
  legali. Molti dei clienti di Crippa hanno investimenti ingenti per
  contrastare la contraffazione perché si vuole proteggere i propri brevetti
  impedendo agli altri di sfruttarli commercialmente. In determinati casi la
  contraffazione ha anche ripercussioni penali, come testimoniano casi di
  contraffazioni di farmaci o di alimenti. Se non mi muovo non ottengo nessun
  risultato ovviamente.

## Slide 49

Esercizio 1: Perchè è importante un'attenta definizione delle rivendicazioni.

Si consideri il Windsurf al momento della domanda di brevetto e lo stato della
tecnologia dell'epoca. La barca a vela esisteva già da moltissimo tempo. La
tavola da surf era già nota inoltre si devono considerare:

1. Mettendo insieme delle tavole da surf ed assicurandole in un certo modo ad
   una vela era più semplice trasportarle in acqua. Esistono delle differenze
   con il windsurf? Quelle che vengono in mente sono due: l'assenza del timone
   e il movimento della vela che, oltre alla rotazione lungo l'asse verticale,
   è permesso anche in inclinazione di tale asse.
2. Il kitesurf. Questo era brevettato ancor prima del windsurf. Nel kite il
   vento viene intercettato dalla fila che è imbracciata dall'utilizzatore per
   poi passare alla tavola l'azione che esercita. Il boma è assicurato alla
   tavola tramite una fune le cui cime vengono utilizzate per imbracciare la
   vela. Questa fune non esercita una forza di taglio ma solo in direzione di
   tiro (caratteristica propria delle funi) e non può nemmeno flettere.
   Trasmettere la forza del vento solamente attraverso la fune vorrebbe dire
   orientare la vela sempre e soltanto di modo che la risultante delle forze
   agenti su essa sia allineata con la direzione della fune, impedendo la
   manovra del mezzo. L'azione del vento quindi è trasmessa alla tavola del
   kitesurf tramite le gambe dell'utilizzatore e solo in piccola parte tramite
   la fune. Nel windsurf invece la forza viene trasmessa in modo più simile a
   quanto avviene nella barca a vela, attraverso l'albero. Quest'albero è
   mobile e permette tramite l'inclinazione e la rotazione di manovrare il
   mezzo.

Le rivendicazioni sono scritte in modo tale da costituire il "massimo comune
divisore" o requisito minimo tale da poter affermare di essere diverso dallo
stato della tecnica (dimostrando quindi novità ed inventive step).

Per poter costruire le rivendicazioni è necessaria un'analisi accurata della
soluzione che propongo a partire da un problema tecnico di fondo. Si stilano le
caratteristiche che risolvono tale problema e comprendo come metterle insieme
per capire quali sono gli effetti tecnici che comportano.

## Slide 58

Esercizio 2: ambito navale. La propulsione della nave è effettuata mediante un
propulsore principale con delle eliche in grado di spingere una gran mole
d'acqua e aventi una potenza dell'ordine dei MW. La manovra della nave è
possibile grazie ad un timone che è in grado di intercettare il flusso d'acqua
e di deviarlo a seconda della direzione verso cui si vuole spostare la nave. Il
problema di questa soluzione è evidentemente associato alla perdita di
efficienza dovuta ai moti turbolenti del flusso d'acqua che si generano quando
si vuole alterare la spinta naturale del flusso d'acqua attraverso il
propulsore principale (dissipazione di energia). Il brevetto che consideriamo
in quest'esempio vuole risolvere questa problematica. Le possibili soluzioni
che possono essere considerate sono le seguenti:

1. Inserire il propulsore in una gondola collocata al di sotto dello scafo
   della nave e in grado di ruotare. La rotazione della gondola produce la
   manovra della nave. Questo tipo di soluzione è parecchio costosa e anche la
   manutenzione non è semplice dato che bisogna mettere a secco la nave.
2. Il propulsore principale viene lasciato in posizione e fisso mentre si
   considerano dei propulsori secondari (steering devices), montati su pod
   (idea simile alla precedente), di dimensione ridotta. La Acker Finnyard ha
   proposto una soluzione di questo tipo.

La Acker Finnyards quindi per poter depositare il brevetto ha stilato le
rivendicazioni di modo che ci sia un MCD di base. Alcune rivendicazioni
possiedono delle locuzioni inclusive e queste rendono possibile la
disambiguazione degli aspetti tecnici che altrimenti non sarebbero evidenti nel
caso in cui il brevetto fosse portato in tribunale. Nel caso di una
rivendicazione della Acker Finnyards è stato compiuto qualcosa che nell'ambito
brevettuale è insolito: si tratta dell'eliminazione di un elemento costruttivo:
di solito in un brevetto si ragiona in positivo, aggiungendo quindi elementi
all'idea mentre qui si è eliminato il timone per poter limitare la portata del
monopolio.

Dopo aver fatto domanda la Finnyard ha ottenuto nel search report la citazione
di un documento che invalida il loro brevetto. L'anteriorità di Mitsubishi
aveva considerato già dei propulsori secondari in grado di ruotare ma, siccome
non ha mai prodotto navi di quel tipo e non ha mai rivendicato come dev'essere
veicolata la potenza all'interno dei propulsori, la Finnyards era pronta a
modificare le sue rivendicazioni in modo appropriato. La limitazione della
potenza complessiva dei steering propulsion devices a valori inferiori alla
metà di quella dei propulsore principale è stata la strategia per poter
ottenere il requisito della novità sulla Mitsubishi.

Il problema ora è che la Wärtsilä Finland ha fatto opposizione in quanto tra le
rivendicazioni un loro brevetto simile a quello di Finnyard aveva anche una
rivendicazione che includeva quella appena scritta da quest'ultimi. La
Finnyards ha quindi due possibilità: andare a ridurre la portata del monopolio
trovando tecnicismi che rendono la loro idea nuova oppure il brevetto della
Finnyard viene annullato.

## Slide 66

Domanda: Che succede se l'opposizione arriva in ritardo rispetto al limite
massimo previsto? Le finestre temporali in cui è possibile fare opposizione
sono perentorie: non spostabili. I nove mesi dalla pubblicazione costituiscono
questa finestra temporale e non ci si può opporre al di fuori di questi. è però
sempre possibile, nel 20 anni di durata del brevetto, andare a contestarlo nei
vari tribunali degli stati che lo hanno riconosciuto oppure, nel caso di
Wärtsilä Finland, di fare un counter claim nel caso la Finnyard volesse
attaccare il loro brevetto, annullandolo.

## Slide 67

L'interferenza: un prodotto rientra nell'ambito delle parole di una
rivendicazione. Si ha un contraffazione quando un giudice chamato a decidere
dell'interferenza emette una sentenza che dice che c'è la contraffazione.

Si ha interferenza quando si verifica la sovrapposizione tra una soluzione
tecnica e l'arte nota di un brevetto rivendicato.

È di fondamentale importanza un'attenta definizione delle rivendicazioni con un
adeguato wording: si hanno effetti diversi nella tutela a seconda delle parole
scelte.

Bisogna distinguere lo sviluppo di una soluzione nuova ed innovativa
dall'autorizzazione a mettere in commercio quel prodotto perché non
interferisca con nessun altro che ha fatto quella cosa.

## Slide 68

Diritti del brevetto: negli Stati Uniti si semplifica dicendo "trarre profitto"
dalla tecnologia brevettata. In Italia invece si sono delineati i vari punti e
sono qui riportati:

Senza un'autorizzazione non si può:

- produrre, usare, mettere in commercio, vendere, importare
- applicare il procedimento
- mettere in commercio, vendere e importare il prodotto direttamente ottenuto
  con il procedimento brevettato. Quindi brevettando un procedimento tutelo
  anche il prodotto.

## Slide 69

Facciamo un esempio: consideriamo la Draisine. Facciamo finta che abbia come
rivendicazione il possedimento di due ruote, una sella e un telaio. è possibile
brevettare la bicicletta avendo come prior art la sola Draisine? Posso anche
realizzarla? Dipende dalla rivendicazione fatta per la Draisine. Si può
brevettare la trasmissione che nella Draisine non c'è. Se costruisco la
trasmissione ho i requisiti per brevettarla. Posso, in subordine, brevettare
anche il veicolo con la trasmissione (bicicletta), oltre alla sola
trasmissione. è possibile però vendere la bicicletta? ebbene no perché questa
possiede tutte le caratteristiche rivendicate dalla Draisine, quindi
l'inventore della bicicletta deve fare prima accordi con quello della Draisine
prima di mettere in vendita l'invenzione. In genere si effettuano negoziazioni
tra le due parti e si può parlare anche di cross-licensing nel caso si voglia
decidere di produrre la bicicletta insieme (ci si scambia le licenze
tecnologiche). Esempi riportati nelle seguenti slides mostrano una specie di
Segway che è brevettabile in quanto utilizza un sistema di controllo e
sensoristica che sicuramente è inventivo e nuovo ma è sempre in contraffazione
letterale quando si tratta di vendere l'oggetto in quanto la rivendicazione
della Draisine copre esattamente anche quell'elemento.

## Slide 74

È necessario che la giurisprudenza, e quindi il legislatore, definisca i vari
tipi di contraffazione:

- letterale: quando si ritrova nel prodotto di presunta contraffazione la
  rivendicazione che riassume complessivamente l'insegnamento brevettato.
- per equivalenti: Quando non si ritrova nel prodotto di presunta
  contraffazione tutta la dichirazione di esclusiva ma, A ciò che non è stato
  riscontrato è sostituito da qualcos'altro che ha la stessa funzione. In
  particolare si deve applicare il test FWR (same Function, same Way, same
  Result)
- indiretta: fornitura di un componente unicamente destinato alla realizzazione
  di un dispositivo brevettato.
	- contributory infringement: componente appositamente realizato o
	  adattato per essere usato in contraffazione di un brevetto
	- induced infringement: contraffazione ottenuta su suggerimento attivo.
	  Un esempio è la modifica del componente per avere un funzionamento
	  rivendicato.

Nella vita vera le aziende che producono componenti che possono essere parti di
progetti più grandi vanno ad affinare sempre di più anche se sono in
contraffazione di modo che ottengano un'efficienza sempre crescente.
L'obiettivo finale è quello di potersi confrontare poi con l'inventore del
progetto grande per licenziare la miglioria, diventare un suo fornitore di
componenti efficienti e, nel migliore dei casi, fare cross-licensing per
vendere prodotti migliorati inisieme a lui (in questo caso devo aver brevettato
in uno Stato in cui il progetto grande non protetto).

## Slide 75

La libera attuabilità

Sorge un problema fondamentale: cosa deve fare l'imprenditore che vuole
costruire un componente nuovo da inserire in un progetto più grande, non
sapendo se può farlo o no? è come muoversi in un campo minato in quanto
potrebbero darci fastidio altre proprietà intellettuali. Come può
l'imprenditore garantire di non essere contraffattore di qualcos'altro se
volesse inserire un nuovo componente? Deve verificare componente per componente
che non sia in contraffazione oppure va a valutare le licenze oppure compra
solo da fornitori che hanno il brevetto su quella tecnologia. Queste verifiche
sono dette FTO (Freedom To Operate, in it. aka libera attuabilità). Fare FTO
significa che quando faccio R&D dovrò cercare la soluzione ottima non solo dal
punto di vista ingegneristico ma anche dal punto di vista legale, in modo da
evitare di entrare nei monopoli temporanei di qualcun altro. L'ottimo
ingegneristico sta convergendo verso l'ottimo legale. Sistemi complessi, alla
cui base vi è anche l'IA,  calcolano le soluzioni ideali trovandone svariate
che potrebbero risolvere il problema anche in termini legali.

## Slide 84

Esempio di brevetto con strategia difensiva: La Nespresso (Nestec) produce
delle macchine del caffè le quali accettano capsule aventi un certo formato e
costituite da un rivestimento in alluminio. Il problema importante si riferisce
alla produzione dei rifiuti legati all'uso di quest'ultime: lo smaltimento di
alluminio unito all' organico. Alcune Compagnie si sono attivate per produrre
delle capsule compostabili che permettono un miglior riciclo. Il problema dalla
parte di Nespresso è ora quello relativo ai guadagni sulle capsule che vengono
così deviati verso aziende terze. Per risolverlo hanno adottato una serie di
uncini laterali che, nel caso delle loro capsule non creano problemi nella fase
di estrazione del caffè mentre nel caso del prodotto concorrente l'operazione
avrebbe un rendimento scarso. Si dà il caso che l'azienda che produce capsule
compostabili (ECC: Ethical Coffee Company) abbia brevettato anche gli uncini
laterali, avendo individuato precedentemente questo punto debole. La ECC può
dunque utilizzare il brevetto degli uncini a suo vantaggio facendo causa a
Nespresso che utilizza la tecnologia senza autorizzazione. In questo modo un
brevetto ne sta tutelando in realtà un altro.

## Slide 95

Il documento brevettuale è standardizzato. Questo significa che chiunque è
capace di comprendere in modo immediato informaizoni utili. La
standardizzazione permette di poter eseguire ricerche capillari e di tracciare
cronologicamente la tecnologia.

## Slide 98

Il rapporto di ricerca è una parte fondamentale che indica chi ha fatto
un'invenzione simile, che richiama o identica a quella di cui si fa domanda.
Naturalmente anche in questo documento si parla di standardizzazione mediante
delle sigle.

## Slide 99

Ricerche brevettuali

Le brochure industriali, le pubblicazioni scientifiche, ecc.. non coprono che
una parte della tecnologia nota. Una gran parte di quella tecnologia è raccolta
nei brevetti. Il brevetto per poter essere pubblicato deve rispondere dei
requisiti fondamentali riconosciuti dalla Legge. La tecnologia così raccolta
viene definita "enabling disclosure": tecnologia raccontata correttamente, pena
annullamento del brevetto.

## Slide 106

Perché si fa ricerca brevettuale?

1. Capisco se sono il primo ad inventare qualcosa oppure no (*Brevettabilità*)
2. Capisco se esistono già dei brevetti anteriori su cui si basa il mio
   (*libera attuabilità*). Da ciò dipende la possibilità di vendere il prodotto
   poi.
3. È uno strumento di raccolta di infomraizoni su chi produce una data
   tecnologia (*scouting tecnologico*).
4. Serve a *monitorare* i concorrenti per capire cosa stanno brevettando.

## Slide 120

Esercizio: siamo dipendenti di un'azienda di estrusioni di alluminio. Siamo in
crisi e abbiamo bisogno di un prodotto nuovo per poterci risollevare. Si vuol
puntare sulle racchette da sci, per cui ci si documenta per capire qual è lo
stato dell'arte e i brevetti che esistono in quell'ambito. Sul database
esistono due modelli con arpionismo cinghia/manopola che permette alle due
parti di essere scollegate e ricollegate agevolmente ma questi brevetti sono
ancora validi quindi per non dover pagare la licenza si cerca in letteratura
delle soluzioni alternative. Scopriamo che il brevetto "Trigger S" è stato
depositato nel 2006 ma concesso solo nel 2017 e questa cosa è strana. I
registri delle conversazioni tra mandatari e Uffici Brevetti sono Pubblicamente
consultabili quindi possiamo controllare dove stava il problema. Una terza
parte ha segnalato l'anteriorità di un brevetto decaduto che mina quello nuovo
di Leki. Ecco fatto! Possiamo copiare liberamente il modello del 1988 senza
incombere in problemi legali perché la tecnologia passata è liberamente
copiabile.

## Slide 148

*A cosa serve il brevetto?*

1. A imporre un monopolio temporaneo nel tempo (pagando le tasse lo mantengo in
   vita). Ciò conferisce il diritto di esclusiva allo sfruttamento economico.
2. Controllo i fornitori che costruiscono la mia invenzione andando ad
   obbligarli a vendere solo alla mia azienda per non favorire i concorrenti.
3. Evita di farsi controllare dal cliente
4. Dichiara la paternità di un'invenzione
5. Patrimonializza (da start-up a impresa milionaria come nel caso dell'azienda
   comprata dalla Metronik)
6. Raccoglie informazioni. Il "nuovo petrolio" è costituito dalle informazioni
   che si possono raccogliere utilizzando una data tecnologia. Elon Musk può
   decidere di rilasciare una licenza gratuita di gran parte dei brevetti di
   Tesla con obiettivo quello di avere una diffusione più veloce sul mercato di
   queste invenzioni. Quello che richiede a chi ne fa uso è di fornire delle
   informazioni utili su come sono state costruite, i risultati ecc. In questo
   modo è come se Tesla avesse tantissimi dipartimenti di ricerca e sviluppo.
7. Arma per ocntrastare la contraffazione
8. Uno degli strumenti, in mano allo Stato, che dimostra chi davvero fa
   innovazione da chi non la fa. Tramite il brevetto lo Stato può decidere di
   fornire incentivi fiscali a privati/aziende.

## Slide 151

Queste sono le due keyword:

- Strategia: nelle rivendicazioni, nello scopo e nell'oggetto dei brevetti
- Tempismo nell'adottare le strategie.

### Domande

Perchè conviene brevettare come singoli e non come aziende? A livello fiscale
il soggetto giuridico che brevetta viene tassato sulla base della cittadinanza,
quindi se un tizio vive alle Cayman riceverà una tassazione molto ridotta
rispetto a quella Svizzera o Italiana. Considerando che poi il tizio
riceverebbe anche i soldi dalle licenze dell'invenzione questo costituirebbe un
vero e proprio illecito in quanto è palese che il brevetto è stato creato nei
laboratori e non alle Cayman e quindi i guadagni sono dell'azienda che
dev'essere anche tassata.

### Riferimenti Bibliografici

Slides: "Lezione 16Mar_(Crippa)"
