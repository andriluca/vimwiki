# Lezione 1 --- Brevetti e requisiti

## La definizione di brevetto

Il brevetto è un documento tecnico-giuridico che conferisce al titolare un
diritto esclusivo di vietare agli altri la realizzazione di quanto rivendicato
e, soprattutto, vieta la vendita del dispositivo. In altre parole, consiste nel
diritto, limitato temporalmente, di esclusiva allo sfruttamento economico e
commerciale dei frutti di un'invenzione (aka opera d'ingegno). È un diritto
*negativo*: non autorizza la realizzazione dell'invenzione rivendicata ma serve
ad escludere altri dal farlo (in termini giuridici è detto *ius escludendi
alios*). In questo modo, non autorizzando la realizzazione dell'invenzione,
permette all'inventore di poterla realizzare anche se non tutelata con un
brevetto.

Chi possiede un brevetto per invenzione:

- Deve verificare che non esistano delle leggi che impediscono la
  commercializzazione del prodotto (i.e. per i principi farmaceutici è
  necessario non solo il brevetto ma anche l'autorizzazione all'immissione in
  commercio da parte dell'Agenzia del Farmaco).
- Non è automaticamente autorizzato a realizzare l'invenzione ma deve
  verificare la liberta di attuazione (aka Freedom to Operate), ricercando
  l'esistenza di brevetti dipendenti di cui il titolare deve richiedere le
  licenze. Questo avviene nel caso di un'invenzione che costituisce un
  miglioramento di un dispositivo brevettato da altri titolari.

Non è ovvio che il brevetto sia limitato temporalmente perché, considerando ad
esempio i marchi, questi non decadono se continuano ad essere utilizzati.
Esistono dunque durate variabili per le varie proprietà intellettuali.

## Innovazione ed invenzione a confronto

Innovare non significa inventare e queste due cose sono diverse dall'invenzione
brevettata.

- *Innovazione*: Soluzione applicata e sfruttata.
- *Invenzione*: Soluzione originale ad un problema tecnico.
- *Invenzione brevettata*: un'invenzione con i requisiti definiti dalla legge
  per diventare un monopolio temporaneo.

### I tipi di innovazione

Esistono le innovazioni:

- **Incrementali**: elaborate a partire da competenze interne all'azienda e a
  partire da qualcosa che esiste già.  L'azienda è composta dall'imprenditore,
  gli immobili, le persone, i macchinari, i brevetti, dalle conoscenze e idee
  delle presone che stanno all'interno. Se le persone parlano solo all'interno
  dell'azienda si ottengono, tranne in qualche raro caso, delle migliorie
  incrementali delle soluzioni. Un esempio di questo tipo di innovazione è la
  draisine (antenata della bicicletta).
- **Radicali**: si è capito, facendo innovazione, che è molto interessante
  avere un colloquio continuo con soggetti esterni all'azienda (i.e.
  Università, consulenti, fornitori, clienti, concorrenti). In questo modo
  delle competenze esterne entrano nell'azienda andando a creare un'innovazione
  più radicale. La lampadina di Edison è un esempio di questa tipologia.
- **Disruptive**: si intendono quelle invenzioni in grado di rivoluzionare il
  funzionamento di un mercato o di un settore arrecando danno alle grandi
  aziende consolidate preesistenti. Un'invenzione totalmente nuova in grado di
  rendere più accessibili e disponibili i prodotti/servizi. Un esempio di
  questa tipologia d'innovazione è quello dell'accelerometro capacitivo
  costruito nello stesso wafer del processore della ST microelectronics.

## Il razionale dei brevetti (e delle IP in genere)

Che senso ha avere un sistema di tutela della proprietà intellettuale? Uno di
questi motivi è il diritto di esclusiva allo sfruttamento economico e il
diritto di vietare agli altri di copiare il proprio. La domanda che sorge è la
seguente: è lecito oppure no copiare? Se si copia durante un esame si viene
buttati fuori esattamente per lo stesso motivo per cui è vietato copiare
scaricando musica da internet: perchè esistono dei diritti non registrati che
nascono con la creazione dell'opera. L'opera è il compito in classe, il brano
musicale, il video su youtube. Con la sola creazione di quest'opera si ha il
diritto di copyright.  Esistono dei diritti non registrati che nascono con la
creazione dell'opera e non si può piratarli.

La tecnologia lavora in modo diverso: se la brevetti (e dunque possiede i
requisiti) hai un'esclusiva, altrimenti è di tutti. Non brevettarla significa
regalare la tecnologia, anzi è stupido non copiare ciò che è liberamente
disponibile. È inutile a chiunque andare a reinventare l'acqua calda quando si
possono rivolgere sforzi inventivi ad altro. Se qualcuno ha inventato la ruota
anni fa è meglio usarla e basta.

A cosa serve il brevetto? 

1. *A imporre un monopolio temporaneo* (pagando le tasse lo mantengo in vita).
   Ciò conferisce il diritto di esclusiva allo sfruttamento economico.
2. *A controllare i fornitori* che costruiscono la mia invenzione andando ad
   obbligarli a vendere solo alla mia azienda per non favorire i concorrenti.
3. *Evita di farsi controllare dal cliente*.
4. *Dichiara la paternità di un'invenzione*.
5. *Patrimonializza* (da start-up a impresa milionaria come nel caso
   dell'azienda comprata dalla Medtronic).
6. *Raccoglie informazioni*. Il "nuovo petrolio" è costituito dalle
   informazioni che si possono raccogliere utilizzando una data tecnologia.
   Elon Musk può decidere di rilasciare una licenza gratuita di gran parte dei
   brevetti di Tesla con obiettivo quello di avere una diffusione più veloce
   sul mercato di queste invenzioni. Quello che richiede a chi ne fa uso è di
   fornire delle informazioni utili su come sono state costruite, i risultati
   ecc. In questo modo è come se Tesla avesse tantissimi dipartimenti di
   ricerca e sviluppo.
7. *Arma per contrastare la contraffazione*
8. Uno degli strumenti, in mano allo Stato, che *dimostra chi davvero fa
   innovazione da chi non la fa*. Tramite il brevetto lo Stato può decidere di
   fornire incentivi fiscali a privati/aziende.

## Le caratteristiche di un brevetto

- Per ottenere un brevetto *l'invenzione deve soddisfare dei*
  [**requisiti**](#cap:requisiti).
- *Il brevetto viene concesso a chi ha compiuto lo sforzo creativo ma talvolta
  non è una singola persona a farlo*. Il ricercatore di "AstraZeneca", per
  esempio, fa parte di un pool di ricercatori che comunque sono assunti
  dall'impresa che fornisce laboratori, materiale, formazione, accordi
  commerciali per produrre il brevetto. Quindi non è semplice indicare chi
  effettivamente detiene il diritto di possesso del brevetto. Come si gestisce
  questo complesso insieme di diritti che ci sono? Normalmente il brevetto
  viene concesso all'impresa di questi inventori e a quest'ultimi è
  riconosciuto il diritto di paternità dell'invenzione (il nome verrà ricordato
  sulla normativa brevettuale come se fosse un diritto morale → simile diritto
  d'autore). L'impresa invece possiede il diritto di sfruttamento commerciale
  di questo brevetto.
- Il diritto a possedere il brevetto non lo si detiene immediatamente ma si ha
  solo il diritto a chiederlo in quel momento. *Il diritto a sfruttare
  commercialmente il brevetto viene concesso solo dopo una procedura nella
  quale vengono verificati i requisiti per ottenere i brevetti*. Il diritto di
  brevetto sorge solo quando l'ente terzo (agenzia di concessione dei brevetti)
  lo assegna. Solo allora e subordinatamente a questa procedura. Questo non
  capita ad esempio con il diritto d'autore che sorge immediatamente con la
  canzone ad esempio. Una delle implicazioni di tutto questo è che possiamo
  avere un'invenzione e di non brevettarla. Può essere quindi circolata
  liberamente o essere utilizzata.
- *Alla scadenza del periodo di esclusiva il brevetto diventa libero*: decade
  il diritto ad esercitare l'utilizzo in esclusiva da parte del proprietario.
  Ciò significa che il brevetto è un diritto temporaneo perché per sua stessa
  natura sorge in modo temporaneamente limitato. Quando decade allora tutta la
  società potrà utilizzare tutto questo senza dovere nulla all'inventore
  direttamente.
- *Concesso alla prima innovazione*: Non alle successive perchè vogliamo
  incentivare solo il primo che ci arriva e che gli altri copino. Non vogliamo
  continuare a reinventare la ruota e non vogliamo ritardi nella divulgazione.
  Solo il primo avrà il brevetto, incentivando le persone a recarsi all'ufficio
  brevetti per depositare quel brevetto, nel timore di non essere i primi.
- *Non attribuisce un premio alla scoperta* ma dà soltanto il diritto ad
  esercitare un'esclusiva temporanea. Se poi quest'esclusiva sarà sufficiente
  ad incassare di più questo lo determinerà il mercato. Non si sta dando dei
  soldi all'inventore ma solo un diritto ad ottenere un privilegio. Se questo
  privilegio vale oppure no dipende dalla risposta del mercato. Questo
  semplifica il tutto perché non devo quantificare quanto è importante
  l'invenzione ma è il mercato a farlo. Vengono scoraggiate quindi le
  innovazioni inutili.
- *Il brevetto è concesso solo a condizione di fornire una spiegazione
  dettagliata dell'innovazione per minimizzare i tempi e costi di copia alla
  scadenza* ([**sufficiente descrizione**](#cap:suff-desc)).
- *Non è gratuito ma bisogna pagare delle tasse annuali*. Questo obbliga a
  riflettere sulla necessità del diritto ad esclusiva: verrà richiesto solo se
  effettivamente si ha un beneficio.
- *Obbligo di concessione ad un equo prezzo in determinati casi* (i.e. Per un
  farmaco essenziale). Se il legislatore ritiene che l'invenzione sia un bene
  essenziale di pubblica utilità, anche se ha dato il diritto allo sfruttamento
  in esclusiva, potrebbe anche dover obbligare l'inventore a utilizzarlo perchè
  non vuole che questo impedisca agli altri l'utilizzo dell'invenzione e quindi
  di risolvere un problema essenziale. Il diritto in esclusiva di sfruttamento
  di un'invenzione di pubblica utilità, nel caso non fosse utilizzata,
  creerebbe dei danni notevoli alla società. I governi nazionali hanno
  normative che affermano il diritto dello Stato (March-in Right) ad importi di
  usarlo e se non lo usi posso dare delle licenze ad un equo prezzo ad altri
  che lo useranno al posto tuo. Tu sarai remunerato lo stesso attraverso delle
  licenze e stabilirà lo Stato l'equo prezzo che quindi obbligherà alla
  produzione.
- È necessario che il titolare del brevetto richieda l'estensione in altri
  paesi, così da limitarle in paesi in cui è inutile vi siano. (*limitazione
  geografica*)
  
## Usi leciti dell'invenzione brevettata

Tutte queste cose sono lecite indipendentemente da quando vengono realizzate
dall'azienda:

- *Uso privato e non commerciale*
- *Sperimentale*: posso fare degli esperimenti su quanto rivendicato in una
  domanda o in un brevetto concesso, l'importante è non commercializzare quel
  prodotto.
- Le *prove e i test* che sono necessari per l'ottenimento dell'autorizzazione
  dell'immissione in commercio (AIC). 
- Il deposito di una richiesta di AIC non è considerato contraffazione se
  effettuato un anno prima alla scadenza del certificato di protezione
  supplementare che invece ha il titolare del brevetto.
- *Esenzione galenica*: sulla base di prescrizione medica il farmacista ha il
  diritto di preparare un medicinale coperto da brevetto.
- *Preuso*: chiunque nei 12 mesi precedenti la data di deposito di una domanda
  di brevetto, abbia fatto uso nella propria azianda dell'invenzione, può
  continuare ad usarla nei limiti del preuso (purchè si possa dimostrare che
  l'ha fatto prima del deposito della domanda del brevetto del concorrente).
  può commercializzare in questo caso ma sempre nei limiti del preuso che
  stabilisce una quantità ben definita.
  
## La validità del brevetto

Il brevetto ha una validità nazionale. In alcuni casi può avere una validità
regionale perchè utilizzo una procedura comune, esiste un trattato
internazionale che afferma che questi stati si sono organizzati in un certo
modo e hanno adottato una procedura comune di esame di concessione. Di solito
il brevetto ha validità nazionale: vale nello stato in cui viene depositata la
domanda di brevetto, è stata richiesta e ottenuta la concessione del titolo. Ci
sono diversi enti sovraregionali che sono qui elencati:

1. *EPO*: ufficio europeo brevetti in cui ci sono 38 paesi contraenti di cui 27
   membri EU.
2. *EAPO*: fasi regionale euroasiatica.
3. *ARIPO*: fase sovranazionale africana (EN).
4. *OAPI*: fase sovranazionale africana (FR).
5. *GCCPO*: organizzazione che riguarda i paesi del Golfo (Arabia Saudita, Yemen,
   Barhein, ecc.)

In ognuno di questi casi si ha un'unica procedura di deposito e di esame. cio
semplifica la concessione del titolo. in alcuni casi non serve fare una
convalida nelle fasi nazionali (nei paesi aderenti i trattati). Nel caso
dell'ufficio europeo brevetti invece è necessaria questa convalida, quindi il
titolare deve scegliere in quali paesi devo convalidare poi il titolo. Non
facendo quest'operazione la domanda viene considerata come se fosse stata
ritirata.

## Lo European Patent Office (EPO) e l'estensione internazionale

L'EPO è l'ufficio che gestisce i diritti brevettuali di una compagine di paesi
più o meno ubicati nella zona dell'Europa. L'EPO esiste dapprima della
Commissione Europea e attualmente aderiscono attraverso degli accordi di
carattere bilaterale (del singolo stato con l'EPO) tutti i paesi che sono
evidenziati in rosso.

![Paesi membri dell'EPO](http://documents.epo.org/projects/babylon/eponet.nsf/0/8C003885190F73D2C1257EEE002E4EBB/$File/EPO-coverage_of_european_patents_map_as_of_1.11.2019_en.png)

Sono tutti paesi della CE più altri paesi.  Si può notare anche la Turchia e il
Regno Unito anche se non fanno parte della UE, così come la Svizzera. Quelli
azzurri hanno rapporti privilegiati con l'EPO senza però farne parte a tutti
gli effetti. La cosa più importante da comprendere è che EPO è diverso da UE.
EPO è un ente sovranazionale a cui tanti dei paesi europei hanno assegnato
dell'operatività sui brevetti. Rimane il nostro ufficio brevettuale di
riferimento dal momento che l'Italia ha l'UIBM (Ufficio Italiano dei Marchi e
dei Brevetti con sede a Roma presso il Ministero dello Sviluppo Economico). Il
nostro ufficio è in grado di relazionarsi direttamente con l'EPO che ha due
sedi (a Monaco, in Germania e a Rijswijk, sobborgo de L'Aia, Olanda).
Attraverso queste due sedi gestisce i brevetti in collaborazione con gli uffici
brevettuali nazionali dei paesi membri.

Quello che si vuole sottolineare in [**questo video**](https://youtu.be/GVFO-aAoHuc), 
in particolare nei grafici in cui si mostrano anche gli altri paesi non EPO, è
che esistono delle normative internazionali che consentono di estendere, una
volta posseduto il brevetto, internazionalmente. Il brevetto può essere esteso
solo in paesi che riconoscono il brevetto originato in un altro paese. Esiste
una normativa internazionale che è stata siglata da tutti i Paesi membri della
WTO. La risposta breve è che se l'inventore fa parte di un Paese membro della
WTO (come più o meno tutti tranne la Corea del Nord) allora può avere che gli
altri paesi della WTO accettino le tue priorità (i.e. se sono Huawei e deposito
il brevetto in Cina, con dei requisiti da fare entro i 12 mesi e chiedo,
tramite l'ufficio cinese, di portare il mio brevetto anche all'EPO. Questo
tecnicamente si risolve in un nuovo filing che viene aperto all'EPO. Questa EPO,
avendo giurisdizione rispetto tutti i suoi paesi membri, a sua volta riesaminerà
l'applicazione brevettuale e sarà in grado di concedere o rifiutare il
brevetto). Dal punto di vista dell'EPO le application sono sia originali,
ossia che partono dai suoi paesi e vanno verso altri, ma anche application di
paesi esterni che chiedono estensioni nei paesi di copertura dell'EPO.

Dal video inoltre emerge la superiorità delle application di paesi esterni
rispetto a quelle originali. Principalmente gli investimenti sugli sviluppi di
nuove tecnologie stanno avvenendo sempre più in Asia.

### Note

EPC: European Patent Convention: normativa a livello comunitario che regola il
brevetto europeo.

## Mantenimento del brevetto

Un brevetto sia a livello di domanda ma anche dopo che è stato concesso deve
essere mantenuto attivo e per fare ciò occorre pagare delle tasse (in alcuni
casi annuali).

US: le tasse sono calcolate dopo la concessione del brevetto ogni 3.5 anni.  Se
voglio mantenere un brevetto US per tutta la durata di 20 anni dovrò pagare 3
tranches.  Se non pago le tasse di mantenimento il titolo decade, il brevetto
non è più e chiunque può replicare l'invenzione, utilizzarla e
commercializzarla senza problemi in quel paese. Le tasse vanno pagate
all'ufficio che sta esaminando quell'invenzione. per esempio in IT: vanno
pagate al quinto anno. All'Ufficio Europeo Brevetti a partire dal 3 anno dal
deposito della domanda Europea.

## Il grace period (aka periodo di grazia)

Periodo non presente in tutte le legislazioni ed è un periodo che va da 6 a 12
mesi a seconda della legislazione dello stato considerato che consente
all'inventore di divulgare l'invenzione e successivamente, tenendo presente che
ci sono 6-12 mesi dalla divulgazione, depositare la domanda di brevetto.

Il periodo di grazia non è contemplato dalla convenzione sul brevetto europeo e
quindi non la si può fare. Se voglio ottenere la concessione di un brevetto
europeo non devo pubblicare, vendere il prodotto o mostrarlo alle fiere di
settore altrimenti il diritto sarebbe largamente pregiudicato. Divulgando
informazioni infatti sto "sporcando" il brevetto e questo non avrà più una
piena proprietà intellettuale

## Esaurimento del diritto (comunitario)

Esaurimento del diritto può avvenire in queste condizioni:

- il diritto di esclusiva su un prodotto brevettato termina con il primo atto
  di vendita da parte del titolare del diritto o con il suo consenso perchè per
  esempio è stato licenziato ad un'altra società e quindi essendo licenziatario
  esclusivo può vendere il prodotto.
- Chiunque abbia acquistato il prodotto lo può rivendere
  senza il consenso del titolare del brevetto. non lo può commercializzare se
  non lo ha acquistato dal titolare o dal licenziatario esclusivo. Questo si
  applica solo nell'unione europea ma non vale per le vendite extra UE.

## Il brevetto dal punto di vista economico

Come mai la normativa legale è parsimoniosa, tende a non concedere il brevetto
tanto facilmente e, quando lo concede, lo fa sempre in forma molto limitata
(temporalmente e geograficamente)?  Esaminando le caratteristiche notevoli
dell'invenzione: questa è un'idea, un artefatto tecnico che avrà un principio
di funzionamento. Primariamente è qualcosa che permette di realizzare ciò che
non era stato realizzato prima.  L'idea possiede quindi un valore perché
riuscire a realizzare qualcosa che prima era inesistente produce un effetto
particolarmente positivo. In fin dei conti è un'attività inventiva e come tale
è di carattere immateriale: posso raccontarla e qualcun altro può produrla. In
questo senso questo bene, l'idea, ha due caratteristiche molto importanti che
gli economisti chiamano:

- **non rivalità**: Un brevetto e in generale un'idea è non rivale nel senso
  che quando comunico la mia idea la continuo a possedere.  Non posso mai
  trasferirla spogliandomi di questo bene → il bene si duplica anziché
  spostarsi di mano. Diverso è per un bene materiale, il cui consumo è rivale
  (se la uso io è mia, se la passo a qualcuno non è più mia). Se possiedo
  un'idea di come risolvere il problema tecnico posso utilizzarla sia io che
  gli altri a cui l'ho divulgata godendo del beneficio del bene "idea" in modo
  non rivale, non pregiudicando il consumo da parte di altri.
- **non escludibilità**: il godimento del bene è non escludibile nel senso che
  se avessi una matita e la regalassi non potrei più utilizzarla ma se poi
  riuscissi a riprenderla riuscirei ad escludere nuovamente quello a cui
  l'avevo regalata in primo luogo. Questo non accade per quanto riguarda un
  bene immateriale: non può essere dimenticato dalla persona che l'ha ascoltato
  se mi pentissi di averlo detto.

Per queste caratteristiche le idee inventive sono di fatto difficilmente
appropriabili (sono imperfettamente appropriabili, parlando da economista).
Imperfettamente appropriabili: mentre posso avere una proprietà di un oggetto e
gestire il godimento del bene, se questo fosse immateriale il mio godimento
sarebbe imperfetto e non riuscirei a farlo mio in modo completo perché se mi
sfuggisse e si diffondesse senza che io riuscissi a controllarlo non riuscirei
a riprendermelo indietro perché è non escludibile. Di fatto succede che se una
persona producesse un artefatto, quello sarebbe suo e quindi è nei chiari
diritti di proprietà su questo. Potrebbe patire il fatto di essere espropriato
di quest'idea senza che nessuno gli dia qualcosa in cambio. È difficile far
valere i propri diritti quando l'oggetto è un'idea immateriale.

### Problemi economici legati all'invenzione

I brevetti (e non solo) risolvono due problemi economici legati alla natura
immateriale dell'invenzione.

Si risolvono i problemi economici in due tempi:

1. Si dà un monopolio per incentivare l'inventare.
2. Lo si limita temporalmente, geograficamente e nello scopo per tenere il più
   possibile ridotto il regime di inefficienza generato dal monopolio.

#### Primo problema economico

La crescita economica dipende dalla produttività: la capacità di estrarre
valore dalle risorse. L'innovazione consente di ottimizzare lo sfruttamento
delle risorse, permettendo di ottenere una maggior produzione (lo si interpreti
sempre nell'ottica di beni materiali). Molte persone sarebbero disposte ad
accettare una remunerazione certa producendo beni materiali (i.e. agricoltori).
Questo significa che i diritti di proprietà che possiedono sono forti per via
della natura stessa dei beni prodotti. In queste circostanze non saranno poi
molte le persone disposte ad inventare poiché, di fatto, una persona anticipa
il problema e quando giungerà il risultato di un successo, questo potrebbe
essergli espropriato senza che gli si dia conto.

Il problema è legato alla R&D che è principalmente ad alta intensità di
capitale, possiede risultati incerti e soggetti a rischi (potenziali eventi
avversi non assicurabili). È anche possibile che, nonostante si abbia successo,
qualcun altro possa copiare l'idea, andando quindi a beneficiare
dell'innovazione senza che si renda conto all'inventore di questa.

##### Soluzione

La soluzione al primo problema economico è trovata nella pratica: fornire un
diritto esclusivo allo sfruttamento dell'opera di ingegno. In questa maniera il
diritto non si forma da solo perché il carattere di immaterialità rende
difficile gestire questo diritto ma posso introdurlo per legge. Anche se tu hai
sentito parlare di questa idea e tecnicamente sei in grado di riprodurla
faresti un'attività illegale nel riprodurla perché è stato stabilito di dover
dare un diritto in esclusiva all'inventore. Viene introdotto artificialmente,
attraverso il sistema normativo, una forzatura all'esclusiva.  L'inventore può
ora non temere che se la sua invenzione diventasse pubblica altri possano
utilizzarla perché anche se la conoscessero non potrebbero trarne beneficio ma
devono chiedere a lui se possono legittimamente utilizzarla. La diffusione
avviene senza danno per il suo sfruttamento economico.

##### Soluzione alternativa

Il brevetto non è l'unico sistema per risolvere questo problema economico.
Esistono altri metodi: ad esempio, nella tradizione inglese, si utilizzavano
dei premi in denaro forniti a coloro che erano in grado di risolvere dei
problemi di un determinato interesse. Questi funzionano quando è chiara a chi
li mette in palio l'utilità dell'invenzione. Il vincitore incassa e si comincia
subito ad utilizzarla senza preoccuparsi di diritti e contenziosi legali. In
molti casi però non sappiamo ciò che serve. Il sistema brevettuale fornisce la
neutralità necessaria, senza dover stabilire cosa è necessario fare ma lasci
gli inventori liberi. I premi esistono sia fatti da governi nazionali ma anche
su applicazioni di open innovation: kaggle.com etc: ci sono imprese che
propongono dei problemi tecnici e cercano soluzioni in cambio di un determinato
premio.  Con questo sistema invii l'idea a chi di interesse senza farla vedere
agli altri e se poi venisse selezionata vi sarà una transazione tra impresa e
singolo ma non viene divulgata.

#### Secondo problema economico

Questo sorge come conseguenza della risoluzione del primo problema economico.
Un diritto in esclusiva ad una persona crea il problema del monopolio della
persona o dell'impresa a questa particolare tecnologia. Questo è un problema
dal punto di vista economico perché il monopolio di fatto produce una
condizione di prezzo superiore a quella del prezzo di mercato in regime
concorrenziale (il monopolista di solito sceglie un prezzo alto che massimizza
le proprie entrate). Questo prezzo alto fa si che in realtà si patisca della
perdita secca di monopolio (c'è una parte della produzione che avremmo potuto
fare senza perderci ma che non facciamo, perdendo godimento, perché il
monopolista ottiene un sovrapprezzo rispetto al prezzo normale di mercato).  In
concorrenza si ha un prezzo di vendita più basso perchè bisogna calibrare il
prezzo sulla base anche della concorrenza (→ il prezzo sarà più basso e
aumentano le quantità di beni scambiati → profitto maggiore). Essendo in
monopolio questo impone un prezzo più alto, vendo meno quantità di beni perdo
quindi il surplus.  Il nocciolo del discorso è che il monopolio, rispetto alla
concorrenza è un sistema più inefficiente. Ogni volta che c'è un monopolio i
consumatori stanno un po' peggio. Se introducessi un monopolio per consentire
il brevetto avrei una perdita generale di efficienza. Il monopolista inoltre
non ha convenienza a produrre tutta la quantità massima producibile. Un altro
svantaggio è quello che l'inventore potrebbe voler tenere l'invenzione per sé,
privando la società della possibilità di benessere e sviluppo che sarebbe ora
possibile. Inoltre altri sperpererebbero tempo e denaro per reinventare la
stessa cosa (invenzione dissipatrice).

##### Soluzione

Soluzione: Il monopolio esiste ma in forma temporanea. Il monopolio non è
valido per sempre ma è limitato nel tempo (mai superiore a 20 anni per i
brevetti).  Questa durata può decadere prima (obsolescenza, l'inventore è
sazio, etc...), quindi è bene che il monopolio venga sciolto il prima
possibile. In questo modo si può godere in concorrenza della tecnologia che
prima era brevettata.  Il monopolio viene dato limitato nel tempo ma anche in
altri modi, ad esempio nello scopo: voglio dare l'invenzione solo ed
esclusivamente per l'attività inventiva di quell'inventore. Ad esempio se
l'inventore ha ricombinato delle parti già esistenti non voglio di nuovo
remunerare quei singoli pezzi a lui non dovuti ma solo ciò che è frutto del suo
ingegno, ciò che lui ha aggiunto.  Limitazione geografica: se l'inventore
chiede di avere un'esclusiva in un paese e non in altri significa che il
compenso dato da quel paese è sufficiente a giustificare i suoi costi e non ha
senso che io estenda al di fuori di quel paese il regime inefficiente del
monopolio. Dal punto di vista della collettività, è necessario che vi siano
abbastanza incentivi da indurre una persona ad inventare ma, una volta
inventato, la cosa migliore sarebbe "espropriarlo". Chiaramente espropriandolo
porterebbe l'inventore a non creare più, perdendo dunque in efficienza
dinamica.

## La politica dei brevetti

Perché i brevetti vengono concessi per periodi limitati? Perché è difficile
estenderli?

Il razionale della normativa brevettuale è quello di contemperare l'interesse
del singolo (inventore) ad avere dei proventi dalle sue attività inventive ma
anche della collettività a godere dei benefici di ciò che è stato scoperto e
che può migliorare la condizione di tutti. Parte dell'applicazione del diritto
e dell'economia che sceglie come modulare la normativa per trarre il massimo
beneficio posibile si occupa quindi di quanto dovrebbe durare un brevetto (ad
ora 20 anni per i paesi WTO). Ogni tanto vi sono delle revisioni. La durata
corretta dovrebbe essere un numero di anni sufficienti per far sì che
l'inventore venga equamente compensato del suo sforzo.

La durata (lenght) di un brevetto è il numero di anni in cui è assicurato il
diritto di esclusiva.

L'ampiezza (breadth - scope, aka ambito di tutela) del brevetto: numero delle
varianti dell'innovazione protette da un solo brevetto.

La durata ottima del brevetto è:

- Minore per prodotti con domanda anelastica (quando il consumo dipende poco
  dal prezzo).
- Inversamente proporzionale rispetto all'ampiezza dell'applicazione
  brevettuale 
- Direttamente proporzionale ai costi d'innovazione
- Dipende dal tasso di sconto (cresce o cala a seconda dell'inflazione).

La normativa brevettuale ha come data fondamentale il 1883, anno della
Convenzione di Parigi. Questa è tutt'ora in vigore e ha stabilito delle regole
comuni per discriminare ciò che è brevettabile. Se il paese aderisce alla
Convenzione. non può più rubare un'idea da un paese e importarla nel proprio
come se fosse sua.

## I requisiti di brevettazione {#cap:requisiti}

I brevetti hanno una specifica caratteristica: il loro diritto sorge soltanto
nel momento in cui viene assegnato da un Ufficio. Se sono un inventore ed ho
un'idea inventiva e la produco quest'idea non è automaticamente mia in termini
di diritti di proprietà. Ciò che ho è il diritto a rivendicare la paternità
dell'idea ma è più un diritto morale che non possiede conseguenze economiche.
Questi diritti economici sorgono solo a condizione di rispettare una serie di
requisiti che vanno verificati da un Ufficio esterno durante una procedura
d'esame che è teso proprio a verificare che vi siano dei requisiti a fronte dei
quali è possibile rilasciare questo diritto. Se questi ci sono l'Ufficio
rilascia il brevetto e nel momento di rilascio l'assegnatario (o titolare) del
diritto di sfruttamento commerciale dell'idea potra rivendicare i diritti di
proprietà piena su questo artefatto dell'ingegno, quindi anche i suoi diritti
economici.  

Quali sono questi requisiti di brevettazione? Questi requisiti sono
sostanzialmente simili in tutti i paesi che aderiscono ai trattati
internazionali relativi ai brevetti, in particolare alla Convenzione di Parigi.
Tutti i paesi che aderiscono al WTO e che hanno ratificato la Convenzione di
Parigi hanno accettato di rispettare dei requisiti descritti nella suddetta
convenzione. Questa convenzione ha una descrizione che è stata fatta in tempi
risalenti, quando la tecnologia era chiaramente diversa da quella che abbiamo
oggi ed è abbastanza vaga nella descrizione. La normativa internazionale
funziona in questo modo: Un paese ratifica una convenzione, questa non diventa
automaticamente testo di legge nel Paese di destinazione ma il Paese che
ratifica la convenzione si impegna a creare una normativa sul suo territorio
nazionale che rispetta le linee guida fissate da questa Normativa
Internazionale. Quindi lo Stato Nazionale è sovrano rispetto alla sua
possibilità di legiferare però dovrà essere stilata all'interno delle linee
guida che ha accettato di ratificare a livello internazionale. Ne consegue che,
per grandi tratti, la normativa di tutti i vari paesi è conforme e si rifà
sempre allo stesso testo: la convenzione internazionale; ma ogni paese può
avere delle leggi che modificano leggermente o attuano questa normativa in modi
parzialmente diversi già dal punto di vista normativo. Oppure la normativa di
un paese deve essere interpretata: quando nascono contenziosi che vengono poi
portati in tribunale, c'è in giudice che deve risolvere una controversia che si
situa in una zona grigia della normativa dov'è necessario per chi fa attuare la
legge, il giudice, interpretare le intezioni del legislatore. Deve dare
l'interpretazione di un testo che alla fine è scritto in frasi che talvolta
sono parzialmente ambigue o lasciano dei margini di arbitrarietà. Quello che
noi osserviamo sul territorio nazionale è il frutto di una generale normativa
internazionale che si traduce poi in una specifica normativa scritta nel
linguaggio di ciascun paese da chi ha il potere legislativo in ciascun paese.
In ciascun paese viene attuata dal sistema giuridico e quindi i giudici possono
aver interpretato queste leggi in modi specifici. Quando parliamo dei requisiti
di brevettazione possiamo parlare di requisiti generali ma poi ogni singolo
paese ha delle interpretazioni e delle piccole differenze che dipendono da
queste caratteristiche elencate. Tendenzialmente vengono mostrate ora le linee
guida generali e poi in seguito si tratteranno alcuni dettagli della normativa
italiana e di quella internazionale. Verranno anche mostrate quali sono le
principali zone grigie dove le differenze tra un paese ed un altro sono più
evidenti.

La Convenzione di Parigi indica tre requisiti di brevettazione indicati
espressamente come tali. Leggendo accuratamente se ne intravede però anche un
quarto descritto nelle "pieghe" di questa convenzione. In generale bisogna
ricordarsi che i requisiti principali sono tre, un quarto e eventualmente un
quinto (specialmente in base all'interpretazione che è stata data dalla
normativa italiana). Se qualcuno chiede in prima battuta quanti sono i
requisiti per brevettare la risposta è sempre e solo tre.

1. **Invenzione**: è detto anche di industrialità. La Convenzione di Parigi non
   fornisce una definizione di invenzione e lo stesso fa la European Patent
   Convention. In realtà la convenzione dice cosa *non* è un'invenzione. In
   generale il requisito è quello di avere un carattere industriale: si intende
   essere un'attività atta a produrre un effetto di carattere industriale, cioè
   non una creazione artistica o una descrizione del mondo ma un effetto di
   trasformazione o di funzionalità di un oggetto su una serie di cose. Il
   requisito ci dice che l'invenzione è qualcosa che ha una caratteristica
   tecnologica nel senso dell'hardware. È un oggetto in grado di produrre un
   certo effetto desiderato. è quindi necessario che ci sia sempre uno scopo
   ben preciso e desiderato. Non possiamo quindi brevettare ciò che è una
   semplice descrizione dello stato della natura che però è una descrizione e
   non è tesa a produrre un effetto desiderato. Questa convenzione di Parigi
   elenca tutte le applicazioni di carattere *non* industriale:

   1. **Le scoperte scientifiche** non possono essere brevettate. La scoperta
      scientifica di per sè spesso è una *descrizione del mondo*, quindi non ha
      un carattere di industrialità perchè non determina un'azione o un effetto
      desiderato. Quando Einstein descrive la teoria della relatività generale,
      di fatto quello che sta facendo è descrivere quello che crede sia il
      funzionamento del Mondo del mondo della Fisica Universale. Questa
      descrizione non ha un carattere industriale perchè non fornisce delle
      indicazioni su quello che ci posso fare io con questa conoscenza.
      L'industrialità dice in che modo posso organizzare una conoscenza o una
      tecnologia per produrre ciò che desidero. quindi di per sè non si
      potrebbe brevettare la teoria della relatività poiché si tratta della
      descrizione del mondo.  Ciò nondimeno, considerando gli algoritmi GPS che
      consentono di mappare la posizione: questi comunicano con un satellite
      che restituisce poi un segnale. Questi algoritmi incorporano le leggi
      della relatività generale di Einstein e questi algoritmi, nei paesi in
      cui gli algoritmi sono brevettabili, sarebbero brevettabili perchè non
      sono una descrizione del mondo ma un modo per descrivere un fine (ossia
      essere in grado di muoversi sulla siperficie terrestre comunicando con un
      satellite che è nello spazio e riuscendo ad effettuare le trasmissioni).
      Viceversa la distanza tra la Terra e il satellite è tale per cui se non
      venissero utilizzate le leggi della relatività generale questo satellite
      manderebbe un segnale che non potrebbe essere captato perchè la posizione
      sulla terra sarebbe cambiata. Un algoritmo di quel tipo deve incorporare
      quelle leggi. *Se si parla di un'invenzione che incorpora delle leggi di
      Natura per ottenere un fine che desidera allora quello è brevettabile
      perché ha un carattere industriale, teso ad ottenere un fine ed uno
      scopo*.
   2. **I metodi matematici**, inoltre, non sono brevettabili poichè sono una
      forma di descrizione del mondo oppure un modo di rendere in forma
      matematica leggi ed assiomi. Algoritmi software qui sono citati perché
      questa è la convenzione dello European Patent Office. In realtà la
      convenzione di Parigi non parla di software perchè, ovviamente è stata
      stilata nel 1800.  È **zona grigia \#1**, in quanto alcuni considerano
      gli algoritmi software, in quanto di fatto traducibili in codice binario,
      una rappresentazione della realtà similmente a quella riportata dagli
      algoritmi matematici, quindi non brevettabili perché manchevoli del
      requisito dell'invenzione.  Altri, appellandosi alla definizione di
      industrialità, affermano che un algoritmo software è qualcosa che vuole
      produrre un effetto quindi, anche se si parla di un metodo matematico,
      questo dovrebbe essere tutelato.  L'Europa esclude gli algoritmi software
      dal brevettabile mentre gli USA li include per le ragioni sopra indicate.
      In particolare presso EPO è possibile ottenere un brevetto per un
      algoritmo quando questo è incorporato all'interno di un Hardware di una
      macchina (i.e. avendo un circuito elettronico che contiene una scheda che
      contiene una parte di un algoritmo, l'insieme di HW e SW è brevettabile
      ma solo il SW no).
   3. **Creazioni artistiche**: fotografie, arti applicate, letteratura, etc.
      Una **zona grigia** però è quella che vede i design degli oggetti. Una
      sedia ergonomica, o con un design la cui funzionalità è quella di essere
      bella esteticamente, ha un design che può essere brevettato, anche il
      Copyright è consentito. Posso avere brevetto per modelli e disegni
      ornamentali in cui è possibile non solo proteggere un oggetto ergonomico,
      quindi atto ad avere effetto funzionale, ma questo effetto può essere
      anche solamente estetico: brevetto un oggetto che ha la funzionalità di
      essere bello.
   4. **Metodi di trattamento chirurgico e terapie**: le procedure che un
      chirurgo utilizza per eseguire un intervento chirurgico. Il protocollo
      chirurgico e terapeutico non è brevettabile.
   5. **Specie e varietà di piante ed animali**: è sempre a partire da una
      variazione di natura e queste specie di fatto sono sempre presenti in
      natura. Il selezionatore non è un inventore in questo caso. Questo
      requisito si applica solo ai cosiddetti animali "superiori", gli esseri
      visibili, ossia non micro-organismi. Questi ultimi sono brevettabili,
      quindi se ho un batterio che utilizzo in un processo chimico per
      trasformare una sostanza in altro, questo è brevettabile come processo
      chimico per ottenere un certo risultato. **Zona grigia \#2**:
      l'ingegneria genetica --> non solo siamo in grado di modificare
      micro-organismi ma anche animali superiori, ad esempio i mammiferi e
      anche qui la normativa è entrata sotto stress.  Questi animali OGM
      rientrano nell'esclusione dell'articolo 5 oppure si devono considerare
      l'opera dell'ingegno dell'uomo perchè la natura non sarebbe stata in
      grado di generare da sola questi? Un esempio di organismi geneticamente
      modificati è il "roundup ready" (sementi modificate per essere resistenti
      ad un certo erbicida) di Monsanto e l'oncomouse (topi con caratteristiche
      genetiche predisposte al cancro).  Primo tema: una persona quindi può
      appropriarsi di altri esseri viventi?  si può fare solo se le speci sono
      inferiori ossia micro-organismi, senza diritti. Secondo tema: considero
      appropriabile una cosa che io ho direttamente contribuito a produrre,
      ossia un animale o specie geneticamente modificata --> c'è un opera
      inventiva. L'individuo interviene direttamente operando un'azione
      inventiva. Si pongono però dei problemi di liceità? 1) è lecito
      rivendicare diritti di proprietà su organismi viventi 2) Questi organismi
      viventi potrebbero avere dei diritti che limitano il loro uso. Se io
      induco il cancro in una cavia di ratto sto modificando geneticamente un
      essere vivente per indurre sofferenza, quindi è legittima una cosa che
      induce sofferenza? se si trattasse di un umano? la tortura di un essere
      umano non sarebbe brevettabile perché manca del requisito di liceità. Se
      si tratta di un animale l'interpretazione è più dubbia. Questa sofferenza
      all'animale viene inferta per un motivo che in verità è anche meritevole,
      ossia trovare trattamenti per curare effettivamente delle persone.
      **Interpretazione del legislatore**: il motivo della sofferenza
      dell'animale superiore deve essere giustificato da un motivo più che
      buono. Il motivo deve più che compensare la sofferenza dell'animale.

2. **Novità**: Il criterio più utilizzato per scartare i brevetti. Ciò che non
   era noto ad una persona esperta sullo stato dell'arte e non era stato
   divulgato al momento del deposito. Se le persone esperte non lo considerano
   noto allora il requisito della novità è soddisfatto. La divulgazione rende
   la cosa nota, quindi invalida la novità.  Questo requisito è stato applicato
   in modi diversi nel mondo (esiste il grace period negli Stati Uniti:
   l'autore può ancora ravvedersi dall'aver divulgato la sua invenzione e può
   ancora ottenere il brevetto. Questo per i 12 mesi).  Questa divulgazione che
   invalida la novità non deve essere per forza in forma scritta ma può essere
   fatta in forma orale (relazione ad un convegno ad un pubblico) o sotto forma
   di un prototipo ad una fiera di settore. La caratteristica di novità
   introduce il concetto della priorità è una data a cui si può fare
   riferimento per dire che una certa cosa era nota ad una certa persona. Se
   depositassi oggi un brevetto e qualcun altro va in un'altra sede brevettuale
   WTO e deposita qualcosa di analogo in modo assolutamente indipendente ad un
   certo punto quando emergerà l'esistenza di due cose uguali si vuole
   stabilire la priorità, confrontando la data di deposito delle due parti,
   dandola a quella anteriore. Per i sistemi con il grace period questa
   priorità si può far retrodatare al momento in cui l'inventore ha iniziato a
   divulgare una certa cosa.

   *Eccezioni (valide anche per paesi senza grace period)*:

	1. Divulgazione a specifiche fiere e esposizioni incluse in un elenco.
	2. Furto d'idea: l'azienda che ha subito il furto effettua una denuncia
	   a suo carico, avendo prove che affermano la colpevolezza di (ex)
	   collaboratori e in tal caso il brevetto rubato viene invalidato
	   perché manchevole del requisito di novità.
	3. Particolari fiere di settore in cui è possibile divulgare senza
	   perdere il requisito di novità.

3. **Originalità (o inventive step o non banalità)**: mentre la novità dice che
   la cosa deve essere nuova di per sè, nell'originalità quello che può essere
   nuova è la combinazione di due cose esistenti. È non banale una cosa che
   combina due pezzi esistenti ma in forma nuova. Facciamo un esempio: telefono
   flap in cui lo schermo all'apertura non si comportava da schermo ma da
   specchio che rifletteva l'immagine. Alla pressione di un tasto lo schermo
   sarebbe diventato uno schermo vero e proprio. In questo caso l'invenzione è
   la combinazione delle due cose già esistenti (telefono flap + specchio). La
   combinazione era così banale o ha richiesto un elemento di originalità tale
   per cui vogliamo dare all'inventore una tutela. La novità e l'originalità
   sono utilizzate anche in ambito del Copyright. Nel Copyright è dominante il
   criterio di originalità, il quale predomina su quello di novità in quanto,
   in quest'ambito, è difficile avere originalità assoluta.

Gli altri *requisiti minori* sono:

4. **Liceità** in termini etici e morali: Il primo tra i requisiti minori. Non
   è possibile brevettare qualcosa che sia contraria alla morale e al buon
   costume. Quest'ultimi sono due concetti astratti che vanno calati nella
   specifica società di riferimento e nel momento storico in corso: non è raro
   che questi requisiti si modifichino nel tempo (oggi è immorale e un domani è
   accettata). È comunque possibile rigettare perché un'invenzione non etica e,
   molto spesso, tanto gli esaminatori che la corte di giustizia si trovano in
   difficoltà di interpretare nello specifico questa legge per stabilire che
   cosa è lecito e cosa no e ciò pone considerazioni di etica legate alla
   tecnologia. Esistono particolari casi in cui la brevettazione è stata di
   fatto esclusa dalla giurisprudenza:
   - le tecniche di tortura,
   - gli embrioni umani (non è etico consentire il diritto di proprietà su un
     essere vivente, per di più umano anche se OGM)
   - la clonazione umana 
   - la selezione genetica tesa a discriminare o all'eugenetica (idea che vuole
     selezionare alcuni tratti perché li considero migliori di altri). Le *zone
     grigie* sono state trattate precedentemente (oncomouse etc).
5. **Esaustività nella descrizione** del brevetto (La sufficiente descrizione):
   bisogna descrivere il contenuto del brevetto minuziosamente, tale da poter
   garantire la replicabilità da parte di esperti di settore. È un criterio
   soggetto ad interpretazione perchè è ovvio che è difficile determinare cosa
   costituisce una sufficiente descrizione. È altrettanto complesso però
   rigettare completamente un brevetto poiché la sua descrizione risulta non
   sufficiente.  Tipicamente si verificano solo dei ritardi. Non è obbligatorio
   però spiegare la scienza dietro il brevetto, l'importante è fornire quella
   descrizione minuziosa dei passaggi che portano al risultato. Non è
   strettamente richiesto al brevetto di essere scientificamente valido, così
   come non viene effettuata un'analisi scientifica del contenuto del brevetto
   da parte dell'esaminatore.

## Il materiale brevettabile

Cosa si può brevettare? Tutto ciò che è nuovo worldwide.  Anche se, in un posto
sperduto, avessero sviluppato una certa soluzione dapprima sconosciuta al resto
del mondo e riuscissero a fare una divulgazione che fosse sufficientemente
appresa dalla collettività e qualcuno negli Stati Uniti andasse a depositare la
stessa idea all'ufficio brevettuale, il brevetto negli Stati Uniti risulterebbe
nullo per mancanza di novità.

Non deve essere solo nuova una soluzione ma anche inventiva ossia non una
conseguenza ovvia della combinazione di soluzioni già note. L'Ufficio Brevetti
Europeo si è costruito una logica per ragionare su quello che significa
combinare documenti noti per arrivare a stabilire se esiste o meno il requisito
dell'inventività in una domanda di brevetto. Ovviamente devono essere tutte
soluzioni che hanno un'applicazione industriale.

*Negli US* è consentita anche la brevettazione di:

- Materiale originale: materiale che portiamo direttamente sottoforma di
  campione all'ufficio e lo brevettiamo portandone un campione.
- Modelli di business: descrizione di processi (di carattere immateriale) atti
  a produrre uno scopo, per esempio il 1-click-buy di Amazon. In Europa non
  hanno il requisito dell'invenzione, quindi non possono essere brevettati.

**Non** sono invenzioni:

- Le mere idee (senza realizzazione pratica): Non tutte le idee possono
  diventare brevetti ma soltanto le applicazioni industriali e quelle che hanno
  i requisiti di novità e inventività. Le invenzioni che hanno un carattere
  tecnico. Non sono invenzioni le mere idee, le formule matematiche, le cose
  che non hanno una ricaduta "pratica".
- Il software: di per sè non è brevettabile ma, in un certo senso, non è così
  perché fa qualcosa (e.g. muove una macchina, acquisisce dei dati e li elabora
  ecc). Se ha un effetto tecnico, il software è brevettabile.  Di per sè le
  righe del codice non sono brevettabili. 
- I metodi per fare business: se invento un modo per giocare in borsa, lavorare
  sugli hedge fund e faccio milioni di euro quel metodo non è brevettabile.
- Terapie mediche, operazioni chirurgiche e ciò che non è moralmente giusto sia
  bloccato.

## Cosa fare prima di depositare il brevetto

Cosa si deve fare quando si intende depositare una domanda di brevetto:

1. Non pubblicare.
2. Non vendere il prodotto.
3. Non fare lezioni/presentazioni della tecnologia. A meno che lo faccia ad un
   numero ristretto di persone legate contrattualmente al silenzio. Questi
   legami contrattuali si chiamano patti di riservatezza o Non-Disclosure
   Agreements (NDA).
4. Richiedere appena possibile un brevetto è complicato quindi bisogna
   affidarsi a qualcuno che lo sappia scrivere, altrimenti si sta investendo
   inutilmente.

I primi tre punti sono molto importanti se si vuole brevettare in paesi in cui
non è contemplato il grace period.

## I motivi di nullità di un brevetto

### La sufficiente descrizione non rispettata {#cap:suff-desc}

L'invenzione dev'essere descritta in modo sufficientemente chiaro e completo
perché ogni persona esperta del ramo possa attuarla. Questo non è un requisito
di legge per avere un brevetto ma un motivo di nullità. La collettività trova
assurdo dare un monopolio ad un'impresa ma lo fa perché, altrimenti, nessuno
investirebbe centinaia di milioni di euro per sviluppare, ad esempio, un nuovo
vaccino se poi il tizio di turno lo copia pedissequamente e ci guadagna perchè
magari è più bravo a produrlo e a distribuirlo. È giusto che, se l'inventore
investe in R&D, abbia un ritorno economico per lo meno temporaneo. L'inventore
in cambio deve raccontare l'invenzione in modo tale che, al termine
dell'esclusiva temporanea del proprietario del brevetto, sia possibile copiare
l'invenzione e sfruttarla liberamente. Solo al termine del diritto di monopolio
temporaneo si è legittimati dunque alla copia del dispositivo.

Esiste l'equilibrio virtuoso del sistema della collettività tra la divulgazione
dell'invenzione (avendo una tutela temporanea nel tempo) e la possibilità di
copiare quello che non ha più una tutela temporanea nel tempo.

Se qualcuno notasse, ad esempio, che AstraZeneca facesse il brevetto di un
certo vaccino, potrebbe studiarlo e inventarne uno nuovo per aggirare il
brevetto di AstraZeneca risolvendo problemi che ha riscontrato. *È quindi
possibile percorrere varie strade tecnologiche parallele*.  Questi sono stati
giudicati dal legislatore degli aspetti positivi.

Se non si rispetta questo patto, mentendo nella descrizione brevettuale così da
impedire la copia, il brevetto verrebbe annullato. Questo accade molto di
frequente in chimica mentre raramente in meccanica per ovvi motivi.

Un esempio di brevetto rifiutato per non sufficiente descrizione è quello di
Antonio Meucci, il vero inventore del telefono. Egli lo aveva inventato e aveva
depositato una provisional (domanda di brevetto temporaneo) negli Stati Uniti
ma non ne ha mai descritto in maniera sufficiente e completa il principio di
funzionamento (come si trasforma il movimento di una membrana e di una bobina
elettromagnetica in una corrente elettrica che può essere trasmessa in un
cavo).

### L'estensione oltre il contenuto della domanda iniziale

Altro motivo di nullità è se l'oggetto del brevetto si estende oltre il
contenuto della domanda iniziale.

Facciamo un esempio: X è il primo ad arrivare nel territorio degli indiani
d'America a inizio '800. Pensava che questo territorio non fosse di nessuno.
Prende quattro paletti al catasto e corre nella prateria, pianta i quattro
paletti e stabilisce i confini del suo territorio, torna al catasto e lo
registra pagando le tasse... Il territorio è ora legalmente suo.  Arrivano
altre persone in secondo luogo e, come prima cosa, osservano il territorio che
hanno davanti e si accorgono che il territorio rivendicato da X non ha la fonte
dell'acqua e la miniera d'oro che si vedono in lontananza. Queste persone
quindi vanno a prendere quel territorio. X si accorge di ciò, prende i paletti
e li sposta per includere entrambe le risorse. *Non si può fare questo in
ambito brevettuale*. 

Se ho deciso che il contenuto di un certo brevetto è di un certo tipo non lo
posso più modificare nel tempo. Questo è un requisito pesantissimo perchè,
combinato al requisito della novità assoluta, è una gran limitazione: scegliere
il momento giusto in cui fare la domanda di brevetto in un processo di ricerca
e sviluppo è difficile. Prima devo mantenere la segretezza o regolare i
rapporti con consulenti, fornitori, clienti per ottimizzare la R&D, così che
rimanga tutto a me e non perda il requisito della novità ma, dall'altra parte,
devo aver capito fino in fondo l'invenzione affinchè sia completa la sua
divulgazione e non debba poi cambiarne i confini.

Esistono dunque due strategie per fare un brevetto:

- Si imposta una buona ricerca e un regolamento adeguato dei rapporti coi
  soggetti che partecipano alla R&D. In un secondo momento si brevetta, quando
  l'invenzione è effettivamente matura.
- Si brevetta a raffica. La prima, la seconda, la terza idea che emerge. Devo
  stare però attento perchè i primi brevetti "uccidono" quelli che vengono dopo
  perchè diventano loro stessi una divulgazione.

Un brevetto per funzionare dev'essere pubblicato. È ovvio perchè, se devo
raccontare a tutti l'invenzione, significa che il sistema deve aver previsto
che la domanda di brevetto, così come è stata scritta, venga pubblicata
affinchè tutti vedano dove sia il monopolio e come copiare l'invenzione quando
questa cade in pubblico dominio.

Nei brevetti non si può cambiare i confini della tutela durante la vita del
brevetto ma solo prima. Ci sono dei trucchi per evitare questi problemi ma
hanno effetto limitato.

## La struttura di un brevetto

La pubblicazione brevettuale è un documento tecnico-giuridico che ha una
struttura che si è andata codificandosi nel tempo in funzione degli esami,
della Legge e dell'uso della Legge che è stata fatta sia a livello
amministrativo dall'ufficio Brevetti Europeo che dai giudici che sono stati
chiamati a valutare se i brevetti azionati posseggano o meno i requisiti di
brevettabilità.

Il brevetto è costituito da **quattro sezioni** (tutte queste sono facilmente
consultabili dalle banche dati come Espacenet):

1. *Frontespizio* (dati bibliografici e riassunto dell'invenzione):
	- Titolo
	- Numero di pubblicazione (che termina con un kind code)
	- Codici di classificazione
	- Data e numero di priorità
	- Abstract
	- ...
2. *Descrizione tecnica*:
	- Stato della tecnica: problemi riscontrati nella prior art e
	  descrizione del problema tecnico risolto.
	- Riassunto: vantaggi dell'invenzione rispetto alla tecnica nota
	  (scientifica e brevettuale).
	- Descrizione dei disegni/formule (opzionale).
	- Descrizione dettagliata.
	- Esempi di realizzazione.
3. *Rivendicazioni*:
	- parte pre-caratterizzante: colloca l'invenzione nel quadro della
	  tecnica nota (Dichiarazione di scienza)
	- parte caratterizzante: indica lo scopo che l'inventore intende
	  perseguire (Dichiarazione di volontà).
	- parte di transizione: ciò che la rende aperta o chiusa ([**leggi
	  qui**](#cap:riv))
4. *Disegni o formule di struttura* (opzionale)

Il documento brevettuale è standardizzato. Questo significa che chiunque è
capace di comprendere in modo immediato informazioni utili. La
standardizzazione permette di poter eseguire ricerche capillari e di tracciare
cronologicamente la tecnologia.

Una parte fondamentale della pubblicazione è il rapporto di ricerca. Esso
indica chi ha fatto un'invenzione simile, che richiama o identica a quella di
cui si fa domanda.  Naturalmente anche in questo documento si parla di
standardizzazione mediante delle sigle.

### Kind codes

Sono dei codici associati alle domande brevettuali (cominciano per A) e ai
brevetti concessi (cominciano per B). Questi cambiano se si considera l'EPO o
l'USPTO.

| Codice              | Significato                                                                 |
| :---                | :---                                                                        |
| **Procedura EPO**   |                                                                             |
| *A1*                | Domanda di brevetto pubblicata con rapporto di ricerca.                     |
| *A2*                | Domanda di brevetto pubblicata senza rapporto di ricerca.                   |
| *A3*                | Pubblicazione separata del rapporto di ricerca.                             |
| *A4*                | Rapporto di ricerca supplementare.                                          |
| *A8*                | Correzione del frontespizio di un documento A1/A2.                          |
| *A9*                | Completa ristampa di un documento A1/A2/A3.                                 |
| *B1*                | Brevetto concesso.                                                          |
| *B2*                | Brevetto concesso dopo modifica dovuta ad opposizione.                      |
| *B3*                | Brevetto concesso dopo una procedura di limitazione.                        |
| *B8*                | Correzione del frontespizio di un documento B1/B2.                          |
| *B9*                | Completa ristampa di un documento B1/B2.                                    |
| **Procedura PCT**   |                                                                             |
| *A1*                | Domanda di brevetto pubblicata con rapporto di ricerca.                     |
| *A2*                | Domanda di brevetto pubblicata senza rapporto di ricerca.                   |
| *A3*                | Pubblicazione separata del rapporto di ricerca con frontespizio modificato. |
| *A4*                | Pubblicazione delle rivendicazioni modificate.                              |
| **Procedura USPTO** |                                                                             |
| *A*                 | Brevetti concessi prima del 2 gennaio 2001.                                 |
| *A1/A2/A9*          | Domande di brevetto pubblicate dopo il 2 gennaio 2001.                      |
| *B*                 | Brevetti concessi dopo del 2 gennaio 2001.                                  |
| *B1*                | Domanda di brevetto US non è stata pubblicata dopo 18 mesi.                 |
| *B2*                | Domanda di brevetto US è stata pubblicata nei 18 mesi.                      |

### Le condizioni di brevettabilità

Esse riguardano come viene scritta la domanda di brevetto:

- *Sufficienza di descrizione*: una domanda di brevetto dev'essere scritta in
  modo sufficientemente chiaro affinchè un tecnico del settore possa
  replicarla.
- *Unità d'invenzione*: dipende da come scrivo la domanda di brevetto. nella
  stessa domanda posso descrivere due o più invenzioni ma devo considereare che
  poi l'ufficio competente concede il titolo solo per un'invenzione quindi
  bisogna scorporare la domanda iniziale in tante domande di brevetto quante le
  invenzioni rinvenute dall'esaminatore.
- *Chiarezza delle rivendicazioni*: dipende da come queste vengono scritte.
  Devono essere scritte in modo chiaro e deve esserci supporto nella
  descrizione. Leggendo la descrizione devo trovare tutte le caratteristiche
  tecniche essenziali che ho rivendicato nella domanda di brevetto.

### La stesura delle rivendicazioni { #cap:riv }

Le rivendicazioni determinano l'ambito di tutela brevettuale.
Esistono due tipi di rivendicazioni:

- *Aperta*: accompagnata da locuzioni quali "che comprende ...", "caratterizzata
  dal fatto che ..." o affini. Significa che la rivendicazione potrebbe
  includere un insieme più ampio di elementi.  (quindi tende ad allargare
  l'ambito di tutela)
- *Chiusa*: accompagnata da locuzioni quale "che consiste di ..." o affini.
  Limita l'ambito di tutela.

La funzione dell'esaminatore è quella di verificare quanto è già noto nello
stato della tecnica e di limitare l'ambito di tutela di un brevetto per non
concedere brevetti troppo ampi, che andrebbero inevitabilmente ad interferire
con quelli di altri brevetti. L'effetto di questa funzione è una modifica delle
rivendicazioni (accorpando o emendandole).

In questa sezione sono raccolti due esempi che mostrano quanto sia importante
la corretta scrittura delle rivendicazioni.

#### Esempio 1: Windsurf

Si consideri il Windsurf al momento della domanda di brevetto e lo stato della
tecnologia dell'epoca. La barca a vela esisteva già da moltissimo tempo. La
tavola da surf era già nota inoltre:

1. Mettendo insieme delle tavole da surf ed assicurandole in un certo modo ad
   una vela, era più semplice trasportarle in acqua. Esistono delle differenze
   con il windsurf? Quelle che vengono in mente sono due: l'assenza del timone
   e il movimento della vela che, oltre alla rotazione lungo l'asse verticale,
   è permessa anche l'inclinazione di tale asse.
2. Il kitesurf. Questo era brevettato ancor prima del windsurf. Nel kite il
   vento viene intercettato dalla vela che è imbracciata dall'utilizzatore per
   poi passare alla tavola l'azione che esercita. Il boma è assicurato alla
   tavola tramite una fune le cui cime vengono utilizzate per imbracciare la
   vela. Questa fune non esercita una forza di taglio ma solo in direzione di
   tiro (caratteristica propria delle funi) e non può nemmeno flettere.
   Trasmettere la forza del vento solamente attraverso la fune vorrebbe dire
   orientare la vela sempre e soltanto di modo che la risultante delle forze
   agenti su essa sia allineata con la direzione della fune, impedendo la
   manovra del mezzo. L'azione del vento quindi è trasmessa alla tavola del
   kitesurf tramite le gambe dell'utilizzatore e solo in piccola parte tramite
   la fune. Nel windsurf invece la forza viene trasmessa in modo più simile a
   quanto avviene nella barca a vela, attraverso l'albero. Quest'albero è
   mobile e permette tramite l'inclinazione e la rotazione di manovrare il
   mezzo.

Le rivendicazioni sono scritte in modo tale da costituire il "massimo comune
divisore" o requisito minimo tale da poter affermare di essere diverso dallo
stato della tecnica (dimostrando quindi novità ed inventive step).

Per poter costruire le rivendicazioni è necessaria un'analisi accurata della
soluzione che propongo a partire da un problema tecnico di fondo. Si stilano le
caratteristiche che risolvono tale problema e comprendo come metterle insieme
per capire quali sono gli effetti tecnici che comportano.

#### Esempio 2: Sistema di guida per navi

La propulsione della nave è effettuata mediante un propulsore principale con
delle eliche, in grado di spingere una gran mole d'acqua e aventi una potenza
dell'ordine dei MW. La manovra della nave è possibile grazie ad un timone che è
in grado di intercettare il flusso d'acqua e di deviarlo a seconda della
direzione verso cui si vuole spostare la nave. Il problema di questa soluzione
è evidentemente associato alla perdita di efficienza dovuta ai moti turbolenti
del flusso d'acqua che si generano quando si vuole alterare la spinta naturale
del flusso d'acqua attraverso il propulsore principale (dissipazione di
energia). Il brevetto che consideriamo in quest'esempio vuole risolvere questa
problematica. Le possibili soluzioni che possono essere considerate sono le
seguenti:

1. Inserire il propulsore in una gondola collocata al di sotto dello scafo
   della nave e in grado di ruotare. La rotazione della gondola produce la
   manovra della nave. Questo tipo di soluzione è parecchio costosa e anche la
   manutenzione non è semplice dato che bisogna mettere a secco la nave.
2. Il propulsore principale viene lasciato in posizione e fisso mentre si
   considerano dei propulsori secondari (steering devices), montati su pod
   (idea simile alla precedente), di dimensione ridotta. La Acker Finnyards ha
   proposto una soluzione di questo tipo.

La Acker Finnyard quindi per poter depositare il brevetto ha stilato le
rivendicazioni. Alcune tra queste possiedono delle locuzioni inclusive e queste
rendono possibile la disambiguazione degli aspetti tecnici che altrimenti non
sarebbero evidenti nel caso in cui il brevetto fosse portato in tribunale. Nel
caso di una rivendicazione della Acker Finnyard, è stato compiuto qualcosa che
nell'ambito brevettuale è insolito: si tratta dell'eliminazione di un elemento
costruttivo. Solitamente in un brevetto si ragiona in positivo, aggiungendo
quindi elementi all'idea mentre qui si è eliminato il timone per poter limitare
la portata del monopolio.

Dopo aver fatto domanda, la Acker Finnyard ha ottenuto nel search report la
citazione di un documento che invalida il loro brevetto. L'anteriorità di
Mitsubishi aveva considerato già dei propulsori secondari in grado di ruotare
ma, siccome non ha mai prodotto navi di quel tipo e non ha mai rivendicato come
dev'essere veicolata la potenza all'interno dei propulsori, la Acker Finnyard
era pronta a modificare le sue rivendicazioni in modo appropriato. La
limitazione della potenza complessiva dei steering propulsion devices a valori
inferiori alla metà di quella dei propulsore principale è stata la strategia
per poter ottenere il requisito della novità sulla Mitsubishi.

Il problema ora è che la Wärtsilä Finland ha fatto opposizione in quanto, tra
le rivendicazioni un loro brevetto simile a quello di Acker Finnyard, aveva
anche una rivendicazione che includeva quella appena scritta da quest'ultimi.
La Acker Finnyard ha quindi due possibilità: andare a ridurre la portata del
monopolio trovando tecnicismi che rendono la loro idea nuova oppure il brevetto
della Acker Finnyard viene annullato.

## L'interferenza e i diritti conferiti al brevetto

Si ha interferenza quando si verifica la sovrapposizione tra una soluzione
tecnica e l'arte nota di un brevetto rivendicato. È di fondamentale importanza
un'attenta definizione delle rivendicazioni con un adeguato wording: si hanno
effetti diversi nella tutela a seconda delle parole scelte.

Inoltre è necessario distinguere lo sviluppo di una soluzione nuova ed
innovativa dall'autorizzazione a mettere in commercio quel prodotto perché non
interferisca con nessun altro abbia contribuito alla realizzazione di questa.

### Bicicletta e draisine, un'esempio di interferenza

Consideriamo la draisine. Facciamo finta che abbia come rivendicazione il
possedimento di due ruote, una sella e un telaio. È possibile brevettare la
bicicletta avendo come prior art la sola draisine? Posso anche realizzarla?
Dipende dalla rivendicazione fatta per la draisine. Assumendola come quella ad
inizio paragrafo, si può brevettare la trasmissione che nella draisine non c'è.
Se costruisco la trasmissione, ho i requisiti per brevettarla. Posso, in
subordine, brevettare anche il veicolo con la trasmissione (bicicletta), oltre
alla sola trasmissione. È possibile però *vendere* la bicicletta? ebbene no
perché questa possiede tutte le caratteristiche rivendicate dalla draisine,
quindi l'inventore della bicicletta deve stringere degli accordi con quello
della draisine prima di mettere in vendita l'invenzione. In genere si
effettuano negoziazioni tra le due parti e si può parlare anche di
cross-licensing nel caso si voglia decidere di produrre la bicicletta insieme
(ci si scambia le licenze tecnologiche). Esempi riportati nelle slides (secondo
riferimento) mostrano una specie di Segway che è brevettabile in quanto
utilizza un sistema di controllo e sensoristica che sicuramente è inventivo e
nuovo ma è sempre in contraffazione letterale quando si tratta di vendere
l'oggetto in quanto la rivendicazione della draisine copre esattamente anche
quell'elemento.

![Draisine, l'antenata della bicicletta.](https://upload.wikimedia.org/wikipedia/commons/4/42/Draisine1817.jpg){ width=250px }

![Contraffazione letterale, se fosse ancora legalmente attivo il brevetto della Draisine.](http://www.airwheel.net/images/Airwheel_A3_spec-2.jpg)


## I tipi di contraffazione

È necessario che la giurisprudenza, e quindi il legislatore, definisca i vari
tipi di contraffazione:

- **Letterale**: quando si ritrova nel prodotto di presunta contraffazione la
  rivendicazione che riassume complessivamente l'insegnamento brevettato.
- **Per equivalenti**: Quando non si ritrova nel prodotto di presunta
  contraffazione tutta la dichirazione di esclusiva ma, a ciò che non è stato
  riscontrato è sostituito da qualcos'altro che ha la stessa funzione. In
  particolare si deve applicare il test FWR (same Function, same Way, same
  Result) per comprendere se qualcuno è in contraffazione.
- **Indiretta**: fornitura di un componente unicamente destinato alla
  realizzazione di un dispositivo brevettato.
	- *Contributory infringement*: componente appositamente realizzato o
	  adattato per essere usato in contraffazione di un brevetto.
	- *Induced infringement*: contraffazione ottenuta su suggerimento
	  attivo.  Un esempio è la modifica del componente per avere un
	  funzionamento rivendicato.

Nella vita vera le aziende che producono componenti che possono essere parti di
progetti più grandi vanno ad affinare sempre di più anche se sono in
contraffazione di modo che ottengano un'efficienza sempre crescente.
L'obiettivo finale è quello di potersi confrontare poi con l'inventore del
progetto grande per licenziare la miglioria, diventare un suo fornitore di
componenti efficienti e, nel migliore dei casi, fare cross-licensing per
vendere prodotti migliorati inisieme a lui (in questo caso devo aver brevettato
in uno Stato in cui il progetto grande non protetto).

## La libera attuabilità

Sorge un problema fondamentale: cosa deve fare l'imprenditore che vuole
costruire un componente nuovo da inserire in un progetto più grande, non
sapendo se può farlo o no? È come muoversi in un campo minato in quanto
potrebbero darci fastidio altre proprietà intellettuali. Come può
l'imprenditore garantire di non essere contraffattore di qualcos'altro se
volesse inserire un nuovo componente? Deve verificare componente per componente
che non sia in contraffazione oppure va a valutare le licenze oppure compra
solo da fornitori che hanno il brevetto su quella tecnologia. Queste verifiche
sono dette FTO (Freedom To Operate, in it. aka libera attuabilità). Fare FTO
significa che quando faccio R&D dovrò cercare la soluzione ottima non solo dal
punto di vista ingegneristico ma anche dal punto di vista legale, in modo da
evitare di entrare nei monopoli temporanei di qualcun altro. L'ottimo
ingegneristico sta convergendo verso l'ottimo legale. Sistemi complessi, alla
cui base vi è anche l'IA,  calcolano le soluzioni ideali trovandone svariate
che potrebbero risolvere il problema anche in termini legali.

### Brevetto con strategia difensiva

La Nespresso (Nestec) produce delle macchine del caffè le quali accettano
capsule aventi un certo formato e costituite da un rivestimento in alluminio.
Il problema importante si riferisce alla produzione dei rifiuti legati all'uso
di quest'ultime: lo smaltimento di alluminio unito all' organico. Alcune
Compagnie si sono attivate per produrre delle capsule compostabili che
permettono un miglior riciclo. Il problema dalla parte di Nespresso è ora
quello relativo ai guadagni sulle capsule che vengono così deviati verso altre
aziende. Per risolverlo, hanno adottato una serie di uncini laterali che, nel
caso delle loro capsule, non creano problemi nella fase di estrazione del caffè
mentre nel caso del prodotto concorrente l'operazione avrebbe un rendimento
scarso. Si dà il caso che l'azienda che produce capsule compostabili (ECC:
Ethical Coffee Company) abbia brevettato anche gli uncini laterali, avendo
individuato precedentemente questo punto debole. La ECC può dunque utilizzare
il brevetto degli uncini a suo vantaggio facendo causa a Nespresso che utilizza
la loro tecnologia senza autorizzazione. In questo modo un brevetto ne sta
tutelando in realtà un altro.

## Il valore del brevetto

Brevettare è un modo per certificare la presenza della tecnologia all'interno
di un'azienda.

- Se volessi negoziare. Se volessi comprare un'idea di qualcuno non posso
  chiedere a quel qualcuno di rivelarmi l'idea liberamente, altrimenti potrei
  andare in giro a diffonderla a tutti, non mantenendola segreta. Se però tu
  prendi l'idea e la incapsuli in un brevetto (monopolio temporaneo) e poi me
  lo vendi a quel punto l'idea diventa mia e nè tu nè qualcun altro la può
  fare. Oppure si può anche decidere di licenziarla a determinate persone per
  poterla utilizzare in determinati Stati. Quindi queste persone possono
  utilizzarla perchè possiedono il diritto di monopolio temporaneo che, se non
  viene rispettato, verrà attivato mediante enforcement da parte di un giudice
  di un tribunale dello Stato. Con il monopolio temporaneo posso rendere
  negoziabile una proprietà immateriale.
- Il brevetto rende visibile perchè, siccome dev'essere una pubblicazione,
  tutti devono sapere dov'è il mio monopolio non solo per poter rispettarlo
  quando è ancora un monopolio ma perchè quando cade in pubblico dominio devono
  poterlo copiare. Ciò lo rende visibile.
- Il brevetto patrimonializza: si crea una differenza di valore aziendale (in
  termini di stato patrimoniale, sezione beni immateriali) quando si possiede
  della tecnologia brevettata.  Facciamo un esempio: il Politecnico di Milano
  fa una start-up. Questa possiede al suo interno della tecnologia non
  brevettata e dei dipendenti non legati da un contratto di non concorrenza. Se
  il Politecnico decidesse di vendere tale start-up, che garanzia avrebbe il
  potenziale acquirente nei riguardi di possibili "ammutinamenti" da parte del
  personale e successive ricostituzioni dell'azienda ex novo? Assolutamente
  nessuna. L'acquirente dovrebbe quindi esaminare la tecnologia brevettata
  prima di acquistarla e di fornire lavoro anche a chi la sappia
  utilizzare/costruire, ovviamente sotto contratto di non divulgazione. Per cui
  non basta avere solo la licenza d'utilizzo di una tecnologia ma è necessario
  anche il know-how.

## Glossario

- *Applicant*: 					Chi richiede la concessione del brevetto (una persona fisica o
  						un azienda, ente, università).
- *Assignee*: 					Titolare del brevetto, della privativa.
- *Claims (rivendicazioni)*: 			Costituiscono una delle sezioni più importanti di una domanda di
  						brevetto perchè definiscono l'ambito di tutela del brevetto stesso.
- *Divisional patent application (divisionali)*: Domande di brevetto che derivano da una precedente domanda quindi 
						collegate a una data di priorità: probabilmente nella domanda iniziale 
						venivano descritte due o più invenzioni e, siccome viene concesso un 
						solo brevetto per un'invenzione, l'inventore deve depositare le 
						successive domande di brevetto.
- *Enforcement*: 				Poter azionare una domanda di brevetto o un brevetto concesso
  						contro un potenziale contraffattore. Si intendono quindi tutte le 
						procedure che servono per azionare una domanda di brevetto.
- *Granted Patent*: 				Brevetto concesso. Ha superato tutte le fasi di esame e
  						quindi è concesso da un Ufficio brevetti nazionale.
- *Innovazione*: 				Soluzione applicata e sfruttata.
- *Invenzione*: 				Soluzione originale ad un problema tecnico.
- *Invenzione brevettata*: 			Un'invenzione con i requisiti definiti dalla legge per diventare un 
						monopolio temporaneo.
- *Negoziare*: 					Rendere un certo bene trasferibile a qualcuno.
- *Office Action*: 				Tutte le azioni ufficiali che l'esaminatore richiede (i.e.
  						modificare le rivendicazioni, modificare la descrizione della domanda di
  						brevetto).
- *Patent Application*: 			Domanda di brevetto, quindi ancora in fase di esame.
- *Prosecution*: 				Prosecuzione ossia una serie di passaggi che portano la
  						domanda di brevetto alla concessione.
- *Provisional patent application (domande provvisorie)*: Non sono disponibili in tutte le legislazioni ma è prassi 
						comune nella pratica US e di altri paesi ma non in EU.
- *Ratifica*:					Approvazione.
- *Search report (rapporto di ricerca)*: quel documento redatto dall'esaminatore dell'Ufficio brevetti che sta 
						analizzando la domanda di brevetto e che contiene una serie di 
						citazioni di documenti anteriori (prior art).
- *Specification (o description)*: 		si intende la descrizione tecnica della domanda o del brevetto concesso.
- *Written opinion (opinione scritta)*: 	è quella che serve per interpretare il rapporto di ricerca.  
						Mentre il rapporto di ricerca è un elenco di documenti di arte nota 
						nell'opinione scritta l'esaminatore fornisce le motivazioni. Ci dice 
						perchè è citato un documento e se quel documento è citato per mancanza 
						di novità o di attività inventiva.

## Riferimenti Bibliografici

Lezione: "01_Brevetti_e_requisiti", "Lezione 16Mar_(Crippa)", "procedure di
brevettazione".
