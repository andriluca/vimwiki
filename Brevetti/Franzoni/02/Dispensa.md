# Lezione 2 --- Diritti brevettuali e altre Proprietà intellettuali

## Come sorgono i diritti brevettuali {#cap:fasi-diritto}

Il diritto di sfruttamento economico in esclusiva nasce sempre e solo al
rilascio del brevetto.  Fino a quel momento inventore e assegnatario hanno solo
i "diritti in formazione". E dal momento...

- *Dell'invenzione* si ha diritto a depositare il brevetto.
- *Della priorità* si ha diritto a rivendicare la novità. Non è così ovvio: se
  depositassi un brevetto in cui descrivo una scoperta il documento mi dà la
  priorità a oggi di saper fare una cosa. Decido poi però di non continuare con
  l'esame perché magari la tecnologia non sarebbe commercializzabile oppure che
  avrei avuto un'idea ancora migliore etc...  Quindi ci chiedono la fee per
  continuare a fare l'esame e io non la pago, lasciando decadere la mia
  domanda. Questa domanda di brevetto comunque verrà pubblicata tra 18 mesi
  anche se non ho dato seguito al brevetto e da quel momento in poi comunque di
  fatto si invaliderà qualunque novità di eventuali successivi brevettatori →
  il fatto di aver depositato di quest'idea fornisce il diritto a rivendicare
  la priorità anche se poi non posso più ottenere il brevetto perché l'ho
  lasciato decadere ma in questo modo ho evitato che altri brevettino. Posso
  anche decidere strategicamente di fare un'application brevettuale e di
  lasciarla decadere quando intendiamo liberare un'invenzione ma vogliamo
  essere sicuri che nessun altro la brevetterà a seguire. Quindi per evitare
  che qualcuno mi impedisca di utilizzarla liberamente faccio ciò.
- *Da quando il deposito è noto a terze parti*, 18 mesi dopo il deposito
  (quando l'ufficio brevettuale pubblica agli altri): sorge il diritto di
  chiedere il risarcimento a chi continua ad usare il brevetto. *Ignorantia
  legis non excusat*, l'ignoranza della pubblicazione non è una scusa legittima
  per continuare ad utilizzare l'invenzione rivendicata.
- *Del rilascio del brevetto*: ho il pieno diritto a rivendicare la sua
  esclusiva per tempo limitato e nei paesi in cui è esteso e limitatamente ai
  claim all'interno del suo brevetto.

Il diritto naturale: riservato al padre o alla madre di un invenzione, sorge al
momento della creazione e rimane sempre all'infinito.

Per il copyright il diritto sorge nell'istante preciso della creazione, quando
sorge il diritto morale del ricordare l'autore. Nel caso del software è
comunque possibile depositare l'oggetto per cui si chiede il diritto d'autore
ma questo deposito non è fatto al fine di ottenere il diritto poiché questo c'è
già.  il deposito viene quindi fatto per rendere facile provare l'esistenza del
mio diritto.  Se ho depositato una canzone che ho scritto, chiunque copi questa
canzone può essere immediatamente sanzionato perché ho una data certa a cui
posso dimostrare che quella canzone era mia. Se non depositassi la canzone e
qualcuno la copiasse, io posso lo stesso portare in tribunale chi mi sta
copiando ma avrò l'onere di provare che io ho scritto la canzone
precedentemente (magari con più laboriosità).

## I diritti conferiti al brevetto sono in movimento

*Diritti conferiti al brevetto*: negli Stati Uniti si semplifica dicendo
"trarre profitto" dalla tecnologia brevettata. In Italia invece si sono
delineati i vari punti e sono qui riportati:

Senza un'autorizzazione non si può:

- Produrre, usare, mettere in commercio, vendere, importare.
- Applicare il procedimento.
- Mettere in commercio, vendere e importare il prodotto direttamente ottenuto
  con il procedimento brevettato. Quindi brevettando un procedimento tutelo
  anche il prodotto.


I diritti conferiti dal brevetto sorgono durante la procedura nell'ordine
indicato alla [**precedente sezione**](#cap:fasi-diritto) e sono sempre in
movimento nel tempo perché, durante la procedura, accrescono fino ad arrivare,
al momento del rilascio, al diritto pieno.  Anche dopo che il rilascio è
avvenuto (dopo la concessione), questi diritti possono essere in movimento per
tante ragioni:

1. Potrei aver protetto alcuni paesi ma poi *potrei decidere di far cadere il
   brevetto* in quei paesi. Posso non pagare la fee per mantenere il brevetto
   in un particolare stato, se mi rendo conto che non è indispensabile. Il
   diritto quindi decade in quel paese e tutti possono utilizzare il brevetto
   in modo libero.
2. Per tutte le azioni che potrebbero sorgere in tribunale: contenziosi legali
   tra concorrenti a causa di copie di brevetti. Il concorrente in tribunale
   potrebbe affermare che il brevetto esistente potrebbe essere manchevole di
   un determinato requisito, magari quello di novità perché lo stava già
   utilizzando. Se il concorrente quindi riesce a dimostrare questo punto e il
   giudice gli dà ragione il brevetto viene revocato. E mi metterà anche nella
   condizione in cui va a modificare anche ex-post i diritti che io credevo di
   aver maturato.

I diritti conferiti dal brevetto non sono scritti nella pietra ma evolvono sia
per effetto della procedura che dopo la concessione per l'effetto di rinnovi o
per l'azione che altri potrebbero fare nei tribunali.

## Litigations ed enforcement

Un brevetto viene concesso da un ufficio brevettuale che ha giurisdizione sulla
nazione che lo riconosce (il brevetto italiano ha giurisdizione italiana;
brevetto europeo è rispettoso della giurisdizione di ogni stato che l'inventore
ha chiesto di coprire). Ogni stato è sovrano rispetto ai diritti che rilascia
sul suo territorio: i tribunali di quello stato possono pronunciarsi nuovamente
sul brevetto nel corso di qualche procedura di giudizio. Ciò significa che se
portiamo in tribunale qualcuno perché ci sta copiando un'invenzione quello che
succede è che noi portiamo le prove sul fatto che il concorrente ci stia
copiando e lui, invece di difendersi negando le accuse, tipicamente dice che il
brevetto esistente non è davvero valido, in quanto l'invenzione era manchevole
di qualche requisito (di solito la novità). Se il brevetto non doveva essere
concesso i suoi diritti erano nulli fin dal principio, ragion per cui chiede al
giudice che lo annulli.  Il giudice valuta nel merito questa rivendicazione,
tipicamente avvalendosi di periti perché inesperto dal punto di vista
tecnologico. Se i periti confermano l'esistenza di elementi invalidanti sin dal
principio, il giudice può decidere che il brevetto è nullo, andando a modificare
retroattivamente i diritti che erano stati conferiti.  Anche dopo che il
brevetto è stato assegnato può ancora modificarsi il diritto che si ha per
opera dei tribunali ordinari di ciascun paese di destinazione.  Siccome i paesi
di destinazione sono molti non c'è ordine di coordinamento delle decisioni dei
giudici perché ogni paese è sovrano rispetto alla giurisdizione sul suo
territorio, allora anche le decisioni dei giudici possono essere diverse nei
vari stati. Ci troviamo ad avere un diritto che è eventualmente fluido e si può
muovere addirittura in modi diversi nei vari paesi.  Questo avverrà tramite le
opere dei tribunali. Quanto forte La tenuta del brevetto continuerà a
proteggermi durante il giudizio dipenderà dall'efficacia nel continuare a
tutelarlo ma anche dalla stesura iniziale: parte della valutazione del merito
di cosa è nuovo, di cosa è inventivo e di cosa rispetta i requisiti dipende da
quanto bene siamo riusciti a definire con parole e disegni l'invenzione in modo
tale da mostrare questi aspetti che soddisfano i requisiti nella parte di
scrittura. 

Il caso in cui un brevetto viene portato in tribunale e finisce per essere
valutato nuovamente è minoritario rispetto al panorama dei brevetti (fatto 100
i brevetti rilasciati nel corso di un anno quelli che finiranno in lite saranno
certamente una minor parte). Il problema è che sono la minor parte più di
valore: qualunque cosa rilasciata ma ininfluente perché non ha mercato o perché
la tecnologia è andata in una direzione diversa (applicazioni alternative etc.)
non verrà litigata in tribunale. Invece quando un brevetto è davvero di valore
ci si può attendere l'imitazione da parte dei concorrenti e se ciò accade
bisogna essere pronti a portarli in tribunale per difendere il brevetto.  Sono
poche le litigation ma fastidiose perché sui brevetti con valore maggiore.

Le litigation brevettuali, i casi di conflitto in tribunale relativi ai
brevetti, non sono solo forti per quei brevetti di valore ma sono anche
aumentate per azioni delle Non-Practicing Legal Entities (NPLE).

Le litigation sono cresciute in numero nel tempo. I costi associati alle
litigation possono essere molto elevati se confrontati con quelli della
procedura.

Una delle ragioni per cui tutto questo sta succedendo è l'esistenza delle NPLE.
Queste sono degli operatori che non hanno una parte manifatturiera/commerciale
(per questo non-practicing). Sono imprese che non hanno una parte che utilizza
dei brevetti per produzione e vendita ma studi legali di avvocati esperti in
materia brevettuale la cui specialità consiste nell'acquistare dei brevetti in
vendita perché gli inventori li possedevano ma non riuscivano ad utilizzarli
o a venderli come avrebbero voluto. Gli avvocati li raccolgono per pochi soldi perché l'inventore
vuole disfarsene in pratica, dopodiché trovano all'interno di questi brevetti
degli ambiti in cui è possibile dire che il brevetto di qualche società florida
sta facendo infringement dei brevetti che questi hanno comprato. NPLE vanno in
tribunale citando le società e chiedendo il risarcimento dei danni. Le società multinazionali
che si sono viste attaccare in questa maniera contestano il fatto che queste
non sono practicing entities, quindi non hanno intenzione di utilizzare i
brevetti ma che sollevano casi inesistenti in modo pretestuoso per estorcere
denaro a loro stesse o alle società che invece utilizzano questi brevetti
(contestando il fatto di essere non-practicing come un demerito). Gli studi
legali invece contestano il fatto che i grandi colossi industriali utilizzano
l'eccesso di potere nei confronti dei piccoli inventori per non retribuirli
delle loro invenzioni e continuare a ovviare senza curarsi dei diritti di chi è
più fragile e qiundi loro si farebbero paladini di questi diritti perché
raccogliendo questi brevetti hanno la forza legale per contestarli
direttamente. Le NPLE sono anche chiamate patent trolls e certamente sono
responsabili di una parte considerevole della crescita delle litigation negli
ultimi anni. Questo è vero soprattutto negli Stati Uniti dove le NPLE sono
state oggetto di una forte indagine del Congresso ma al termine della quale non
si è deciso di fare azioni particolari quindi attualmente non esiste divieto o
obbligo di essere una practicing entity per muovere un'azione in giudizio
contro chi eventualmente ci sta defraudando del diritto brevettuale.

## Procedura dell'EPO

![La procedura EPO](Figure/Procedura.png)

**L'Application è il giorno del deposito**. Può avvenire online o in una delle sedi
della Camera di Commercio in ogni provincia. *Pago la fee del deposito e abbiamo
una data di filing (deposito) e una di priorità (anteriorità)* che di solito
coincidono o possono avere uno scarto di qualche giorno in cui l'ufficio
verifica i pagamenti e che i documenti siano validi.

Dal momento dell'application **inizia un periodo in cui il brevetto è segreto**.
L'autore può divulgarlo oppure no. In questo periodo di segretezza **l'inventore
può pagare una seconda fee per poter sottoporre il brevetto all'esame**. Se ad
esempio un azienda in Italia volesse brevettare qualcosa e volesse far
esaminare all'EPO, questo brevetto sarebbe inviato in prima battuta all'ufficio
dell'Aia in Olanda, dove si svolge il primo esame. È *un esame non binding* (non
vincolante: vengono solo segnalati i problemi che il brevetto può avere): un
esaminatore apre il brevetto e fornisce una prima opinione sul fatto che il
brevetto rispetti i requisiti visti alla lezione precedente. Esaminatore scrive
il search report (1-2 pagine) in cui si segnalano dei documenti che potrebbero
pregiudicare la novità o l'inventive step. Alcune volte in questo documento si
riportano dei documenti che l'esaminatore richiede vengano citati
dall'inventore per rendere la descrizione più completa. Questo documento viene
poi spedito all'applicant. *Questo viene fatto di solito entro i primi 18 mesi*,
quindi quando non è ancora stata pubblicata l'application. Se così avvenisse,
allo scadere dei 18 mesi verrebbe pubblicata sia l'application che il search
report. Altrimenti viene pubblicata l'application in prima battuta e, alla
disponibilità del search report, questa viene ripubblicata con l'aggiunta (il
kind code verrà modificato in questo caso ma questo è un dettaglio che sarà
comprensibile in una lezione di Barbieri). La stessa cosa viene fatta anche dal
WIPO (Worldwide International Patent Office): pubblica anche il search report.

**Al momento del deposito dell'application l'inventore ha 12 mesi di priorità per
estendere ad altri paesi**. Se venisse presa in considerazione l'estensione,
viene fatto un altro deposito in questi paesi con altri codici brevettuali (jp,
kr, us...) che si rifà all'application italiana ma che di fatto è un altra
domanda di brevetto. E partono i vari esami per questi vari uffici ma con un
anno di ritardo. Ciascun esame è indipendente dagli altri.

Espletato il primo esame, **l'applicant** (un altro modo per dire inventore) **può
chiedere di effettuare l'esame vero e proprio pagando un'altra fee**. Riformulerà
la domanda modificando sulla base del search report ricevuto dal primo ufficio.
Si vanno a rifinire i claim (le rivendicazioni) eventualmente riducendoli per
evitare di andare a pestare aree già coperte da altri brevetti e rendendo
l'esame più solido. Si cerca di rendere il documento più solido possibile. Il
secondo esame viene fatto a Monaco (18-20esimo mese). L'esaminatore è diverso
dal primo e non ha l'obbligo di pensarla esattamente come il primo. La sua
opinione è binding, cioè vincolante: scrive una lettera indicando se il
brevetto è manchevole di qualche requisito e che ha intenzione di rifiutarlo a
meno che non venga refinito in modi acconci. Inizia un dialogo con questo
esaminatore nel corso del quale l'applicant può decidere di ridurre il brevetto
ulteriormente fino ad una forma in cui l'esaminatore è concorde che rispetta i
requisiti. L'applicant può anche decidere di rinunciare al brevetto.

Se la questione va avanti il secondo esaminatore può decidere di accettare o
revocare il brevetto. **Nel momento del rilascio si apre un periodo di 3 mesi
nel quale terze parti possono decidere di fare opposizione** (questa cosa
esiste solo in Europa e in nessun altro paese): un concorrente utilizza una
tecnologia simile e ritiene di non averla potuta brevettare perché non la
ritiene nuova, quindi quando nota che qualcuno la sta brevettando fa
opposizione perché eccepisce che alla luce di alcuni documenti c'è un errore e
non dovrebbe essere considerata come nuova. Fa una domanda di opposizione.
Durante questi 3 mesi si raccolgono tutte le domande di opposizione.  Se non
c'è nessuna domanda di opposizione non accade nulla, altrimenti si apre la
procedura di opposizione e ci sarà un terzo esaminatore sempre a Monaco
(diverso dai primi due) e valuta se, alla luce di quello che è emerso, decide
ancora di confermare il brevetto oppure di revocarlo. Spesso viene revocato o
modificato.

**Se il brevetto è stato negato o revocato dopo l'opposizione, l'applicant può
addirittura ricorrere in appello**: un ulteriore esaminatore diverso dai
precedenti riesamina il caso e decide (questa è ultima possibilità).

Al termine, l'esame sarà vincolante. **Quindi se un brevetto esiste viene
pubblicato nuovamente ma come brevetto rilasciato**, non come domanda di
brevetto e da quel momento in poi i diritti sono perfetti.  Gli altri esami
vengono composti negli altri paesi e, quando lo accettereanno, lo
pubblicheranno con una data. Nel caso rifiutassero non c'è una pubblicazione di
rigetto ma una notizia nello status del brevetto.

## I diritti delle IP diverse dai brevetti

I brevetti non sono l'unica IP esistente nel panorama della protezione dei beni
immateriali.  *Proprietà intellettuale o IP*: tutta la materia della protezione
dei beni immateriali delle opere di ingegno.

Vedremo sei tipologie, non tutte ugualmente importanti ma spenderemo più tempo
sul diritto d'autore piuttosto che sugli altri.

- Diritti d'autore (aka copyright). All'interno di questi vedremo la protezione
  tramite copyright del software e delle banche dati.
- Brevetti per modelli d'utilità
- Protezione del design
- Marchi (non trattati molto in questo corso)
- Denominazione d'origine (quelle geografiche tipiche)
- Segreto industriale

## Il diritto d'autore

Il copyright è una delle protezioni più importante tra quelle menzionate. È
garantita agli autori di lavori originali e in particolare va a coprire tutto
ciò che era stato escluso dalla brevettazione perché manchevole del primo
requisito (i.e. Invenzione o industrialità). L'idea è che il brevetto può
essere concesso per qualcosa che ha un carattere tecnico, tipicamente un
hardware (qualcosa che è fatto proprio di materia fisica). Tutto il resto che
non è tecnico ma che comunque dispone di opera di ingegno e si tratta di lavori
originali e creativi vengono comunque protette da copyright idealmente.  È una
tutela diversa da quella brevettuale. Fanno parte chiaramente dell'ambito del
copyright tutte le arti dalla letteratura (sceneggiatura, libri, poesia), alla
musica (arrangiamenti originali di musica esistente), alle arti figurative, ...
, videogame con parte grafica con ausilio di software e storyboard, ecc.
Dovesse nascere una nuova tipologia di azione creativa sarebbe in grado di
proteggerla.

Cosa fornisce il copyright? Il diritto esclusivo all'autore allo sfruttamento
commerciale dell'opera che ha creato e il diritto morale ad essere ricordato
come l'autore dell'opera. Dal punto di vista del diritto morale, questo è
identico a quello del brevetto. Per quanto riguarda lo sfruttamento commerciale
anche qui l'azione che otteniamo è analoga a quella del brevetto ma con
un'importante differenza: nel caso di copyright **questo sorge immediatamente**
con la creazione mentre con il brevetto il diritto sorge solo nel momento in
cui, completata la procedura d'esame, ci assegnano il brevetto. Per l'autore il
diritto è immediato. Non appena l'opera viene prodotta abbiamo automaticamente
e immediatamente il diritto di proprietà.

### Confronto col brevetto

Si parla di creazione nel copyright a differenza del brevetto in cui si usa il
termine di invenzione.  Questo non significa che non possa essere registrato.
Esistono degli uffici in cui è possibile registrare il copyright. Ma a
differenza degli uffici brevettuali in cui ci si reca per avere il rilascio del
nostro diritto, nel caso della registrazione del copyright, questa non genera
il diritto ma rende più semplice ed immediato azionare la tutela, il famoso
enforcement. La registrazione del diritto d'autore avviene presso gli uffici
della SIAE (Società Italiana di Autori e Editori) in Italia, anche per il
software. Se siamo un autore ed andiamo a depositare l'opera creativa alla
SIAE, il giorno della registrazione non è diverso dagli altri dal punto di
vista del diritto, è semplicemente il giorno che rende più agevole
eventualmente l'andare in tribunale e dimostrare che un'opera era nostra perché
esiste una registrazione di una terza parte che a una certa data avevamo
depositato una certa cosa. 

Se capitasse che qualcuno copiasse e dobbiamo andare in tribunale, possiamo
portare la registrazione dicendo che si può dimostrare che, in una data
precedente a quella in cui questa persona ha cominciato a copiarmi, avevo
depositato questo e dunque è mio. Se invece non è stata fatta la registrazione
si può sempre andare in un tribunale ordinario ma dovremo provare a onere
nostro che noi avevamo creato l'opera precedentemente dell'entità giuridica che
ci sta copiando. La registrazione semplifica l'onere della prova ma non è un
atto che costituisce l'esistenza del diritto.

### La Convenzione di Berna

Anche il copyright è soggetto ad una convenzione internazionale, il che
consente, se si è un autore italiano, di avere i diritti garantiti non solo in
Italia ma in altri paesi che aderiscono alle convenzioni internazionali, in
particolare alla WTO. Il Trattato Internazionale per il copyright è la
Convenzione di Berna del 1886 (analogamente a quello che costituisce il
Trattato di Parigi per i brevetti). Questa convenzione concede al proprietario
il diritto esclusivo a:

- *riprodurre e a replicare l'opera*: non è possibile per una persona terza
  stampare dei poster di un'opera artistica senza il permesso dell'autore. 
- *Fare del materiale derivato*: se sono uno scrittore non è possibile tradurre
  il mio materiale in altra lingua e pubblicarlo senza approvazione;
- *Distribuire le copie*: se stampo un libro la possibilità di replicare l'opera
  in varie copie e distribuirla deve essere autorizzata da me.
- Esibire/esporre in pubblico l'opera.

Fornisce diritti pieni all'autore dell'opera. La durata del copyright è materia
più fluida rispetto ai brevetti. Nei brevetti c'erano anche diverse durate fino
al 2001 poi con dei trattati aggiuntivi (non all'interno del trattato di Parigi
ma firmati nel 2001 nei famosi TRIPs) si è arrivati ad una durata omogenea per
tutti i paesi della WTO che è fissata per un massimo di 20 anni. Per quanto
riguarda il copyright la situazione è più fluida: paesi diversi hanno durate
dei copyright diversi. Talvolta le durate dei copyright dipendono dal tipo di
materia che si va a proteggere (i.e. ci sono paesi in cui la durata del
copyright sulla musica è diversa dalla durata del copyright sui film). La
durata può variare da un paese all'altro ma la Convenzione di Berna stabilisce
un minimo: qualunque paese che aderisce alla WTO deve garantire almeno il
minimo stabilito ossia tutta la durata della vita dell'autore più 50 anni oltre
la morte dell'autore. Questo concede ai posteri di ereditare una parte della
ricchezza che l'autore ha generato. In particolare nel '93 la Commissione
Europea ha definito una durata uguale per tutti i paesi che aderiscono
all'Unione Europea, fissata in modo abbondante (vita dell'autore + 70 anni
oltre la morte) andando ad estendere il requisito della Convenzione di Berna.
Questo vale in tutta la UE. Naturalmente questo principio che sembra semplice è
in realtà più complesso perché spesso gli autori sono molteplici e,
tipicamente, non muoiono lo stesso giorno, quindi si apre il problema delle
opere collettive e l'interpretazione in questo caso è spesso di dare 70 anni
dopo la morte dell'ultimo degli autori viventi.

### Requisiti per il Copyright

Requisiti per ottenere il copyright. Per i brevetti ce n'erano solo tre con due
aggiuntivi. Similmente avviene per i diritti d'autore ma in questo caso ve ne
sono solamente due perché, ovviamente, il requisito dell'industrialità (si
chiedeva che si trattasse di un artefatto tecnico atto a produrre un effetto)
dei brevetti non è applicabile nell'ambito copyright poiché si vuole proteggere
tutto ciò che non ha il requisito dell'invenzione. Rimangono però gli altri due
requisiti:

- *Novità*: l'opera deve essere originale ossia nuova, che non esisteva prima e
  creata dall'autore.
- *Originalità (aka non banalità o inventive step)*: Quest'opera dev'essere
  nuova non soltanto perché è la mera combinazione di qualcosa di esistente ma
  questa combinazione di cose esistenti dev'essere lo sforzo di un effetto
  creativo importante: dev'essere stato non banale produrre questa nuova
  combinazione.  *Un esempio*: se sono un DJ che prende una canzone per
  inserirla nel jingle sotto una base musicale. Sto utilizzando una canzone
  esistente con una base (che può anch'essa essere esistente perché utilizzata
  in altre canzoni o fa parte delle basi standard). Ho due elementi che non
  sono nuovi e li combino insieme per ottenere una nuova opera creativa. Quello
  che devo andare a valutare è se la combinazione di queste due cose è
  sufficientemente creativa da costituire una nuova opera. Se la combinazione è
  banale, non richiede un grande sforzo creativo e non vedo una nuova opera ma
  la copia di due cose che erano già esistenti.

Il vero requisito non è quindi la novità nel diritto d'autore ma lo sforzo
creativo si è dovuto fare nella combinazione delle cose esistenti per produrla,
quindi l'originalità. Questo è facile da capire per nei remix di canzoni o
remake di film.

Tutte le volte che esiste questo sforzo di non banalità anche se stiamo
utilizzando elementi precedenti c'è una nuova creazione che merita, secondo il
legislatore, la protezione del diritto d'autore. Questo non significa che
possiamo utilizzare a nostro piacimento qualsiasi elemento usato in passato ma
bisogna chiedere i permessi all'autore della canzone per inserirla nel remake.
Una volta ottenuto il loro permesso abbiamo un nuovo copyright e chiunque
riprodurrà la nostra combinazione dovrà a noi i diritti d'autore. Gli altri
pagheranno noi se vogliono utilizzare quell'opera.

Anche nella normativa del diritto d'autore c'è una menzione relativa al fatto
che il materiale da proteggere è sempre qualcosa di non contrario alla morale o
al buon costume (criterio della liceità). Questo requisito è meno stringente
rispetto a quello dei brevetti. Anche questo eventualmente può essere
considerato in tante normative. Il buon costume nelle arti viene valutato in
modo più lasco che non negli aspetti inventivi ed industriali.

### Diritti d'autore sul Software

Il software nell'interpretazione dell'UE non è proteggibile con il brevetto
perché si considerano escluse dalla brevettazione tutte le formule matematiche
e gli algoritmi del computer venivano considerati esclusi perché classificati
come tale dall'UE.  L'esclusione per mancanza del requisito dell'invenzione di
per ciò stesso li ha fatti rientrare nella protezione del Copyright. È una
tipologia di opera creativa molto importante perciò ha gemmato una sua
normativa specifica che si appoggia su quella del Copyright e quindi
riconosciuta a livello internazionale in virtù della ratifica della Convenzione
di Berna ma negli ordinamenti giuridici dei vari paesi ha preso la forma di un
diritto self-standing chiamato "diritto d'autore sul software". In particolare
i paesi europei prevedono la concessione di diritto d'autore sul software a
partire dal 1991, anno in cui hanno stabilito di escludere la brevettazione del
software e di fornire protezione attraverso il copyright. Ogni paese ha poi le
sue normative perché la UE non ha la possibilità di fare leggi che hanno
efficacia immediata negli stati nazionali ma fa delle direttive a cui gli stati
devono adeguarsi, delle linee guida generali della normativa e poi ogni stato
nazionale deve fare delle leggi che riflettono queste linee guida ma con dei
gradi di libertà nell'interpretazione. Ogni paese ha le proprie normative e le
proprie procedure di registrazione. In Italia il software si registra presso la
SIAE.

Negli USA, in cui il software è coperto da brevetto, questo è anche
proteggibile con il copyright. Una doppia protezione del software. Sono
arrivati a questa risoluzione perché all'estero la protezione copyright è
prevalente rispetto a quella brevettuale e per ragioni di omogeneità è più
facile per le società gestirla come un copyright.

### Diritti d'autore sui Database

Una protezione analoga a quella del software è quella dei database che sono
sempre più importanti nell'economia contemporanea, dato che le Big Tech vendono
informazioni quindi la raccolta e il mantenimento delle informazioni per loro è
l'artefatto principale e c'era la necessità di concedere un diritto d'autore
rispetto all'originalità delle banche dati che vengono prodotte. In questo caso
la normativa si basa sul copyright. Una banca dati con dati nuovi è sempre
nuova, quindi il requisito è quasi sempre automaticamente rispettato.
L'originalità richiede che non ci sia solo una copia di un database esistente
ma che ci sia una nuova organizzazione di dati o una profondità di dati
diversa, quindi uno sforzo superiore rispetto alla mera copia. In particolare
nella UE la copertura del diritto d'autore sul database è di 15 anni di durata,
quindi diversa rispetto a quella degli altri copyright.

## Brevetti per modelli d'utilità

Il modello di utilità è un dispositivo che migliora la facilità d'uso di un
macchinario, utensile, oggetto o processo o una delle sue parti. Si tratta di
un tipo di IP che non esiste in tutti gli stati. È comune in diversi paesi
europei ed esiste anche in Cina. È possibile ottenere una copertura al WIPO ma
l'EPO non consente il deposito di brevetti per modelli d'utilità. Il brevetto
per modello d'utilità è molto simile ad un brevetto: molti lo interpretano come
un "petty patent" o brevetto piccolo, "insignificante". L'idea è che ci sono
molte cose che sono nuove ed inventive ma che hanno chiesto uno sforzo
inventivo piccolo, quindi il terzo dei requisiti, l'inventive step, è
manchevole perché abbastanza banale quello che è stato aggiunto. Nei paesi in
cui ci sono tante imprese manifatturiere che fanno tanti miglioramenti piccoli
ed incrementali dei prodotti che però hanno un valore forte per l'impresa, il
legislatore ha organizzato una tutela anche per questo tipo di attività
inventive modeste. I paladini di questo tipo di concessione sono quelli aventi
molte piccole-medie imprese (PMI). Di solito la ricerca e sviluppo di grande
entità ha capitali finanziari tali che spesso le imprese medio-piccole non
hanno. È un tipo di protezione che aiuta le piccole-medie imprese ad avere
delle IP.

Non esiste un esame vero e proprio per il brevetto per modelli d'utilità: si
deposita il brevetto ed eventualmente se qualcuno volesse azionare la tutela,
attivare l'enforcement, bisognerà provare che il brevetto aveva i requisiti al
momento del deposito. Una volta si faceva così anche per il brevetto in Italia.
Prima si registrava come con la SIAE e non c'era una verifica dei requisiti in
quel momento ma qualora vi fosse stato un problema e si finiva in tribunale si
poteva tirare fuori la registrazione ed in quel momento si esaminava se i
criteri erano rispettati al momento della registrazione. Era il giudice a
valutarlo durante la procedura ma non esisteva l'esame. La stessa cosa è
rimasta per i brevetti per modelli di utilità. La durata è più breve del
brevetto e varia da un paese all'altro e normalmente le durate più comuni sono
tra i 7 e i 10 anni.  In Italia si da una durata massima di 10 anni ma è molto
difficile che un nuovo dispositivo per modello di utilità rimanga nuovo e
quindi competitivo per 10 anni perché essendo miglioramenti piccoli si
esaurisce il loro valore in pochi anni.

Nasce per contrastare un problema fondamentale riscontrato in Germania. In
questa nazione infatti nel momento in cui si deposita la domanda di brevetto, a
differenza di ciò che accade in Italia, non posso attivare l'enforcement fino a
quando il brevetto non è stato concesso. Il brevetto per modello d'utilità
permette dopo essere stato concesso (attesa pari 3 mesi) di attivare
l'enforcement confidando nel fatto che la tecnologia sarà poi concessa anche
come brevetto per invenzione.

La dottrina e la giurisprudenza italiana ha scelto che i modelli d'utilità
devono tutelare delle configurazioni, conformazioni di oggetti noti
caratterizzati da una maggior praticità, comodità e facilità d'impiego.

L'esempio che si può fare è quello dello scrocco di una maniglia: il meccanismo
di apertura è ormai noto quindi non è tutelabile mediante il brevetto per
invenzione. Quello che invece è tutelabile è l'ergonomicità con la quale si
apre la porta mediante il brevetto per modello d'utilità.

Il brevetto per modello d'utilità si rifà agli stessi requisiti del brevetto
per invenzione: l'ergonomicità, la praticità, la comodità deve essere del tutto
nuova e unica.

## Design e modelli

Il design consente di proteggere una particolare forma esteriore (aspetto
estetico) di un prodotto o di una sua parte. Questa forma esteriore può essere
particolare perché risulta da linee, colori, forme, ornamenti che conferiscono
un **carattere individuale** all'oggetto. La normativa del design fa
riferimento al carattere individuale conferito all'oggetto grazie alla ricerca
sull'aspetto esteriore di un prodotto. I criteri (*requisiti*) coinvolti sono:

1. *novità*: un oggetto non deve essere esistente al momento della creazione,
   quindi una copia di qualcosa che c'era già
2. *carattere individuale (equivalente a inventive step/originalità)*: la
   combinazione di colori e forme conferiscono un aspetto unico distinguibile e
   separabile dalle altre cose al nuovo oggetto. Questo viene valutato da un
   esperto nel settore del design.
   
- **Criticità**: la predivulgazione è lesiva della novità. Esiste però un
  periodo di grazia di 1 anno per US e EU.
- **Vantaggi**: Costi ridotti per fare un design perché si tratta di utilizzare
  disegni e non parole, quindi non devo scrivere nulla dal punto di vista
  legale ma mostrare in modo chiaro la mia idea. Posso depositare più disegni o
  modelli all'interno della stessa domanda.

Protegge contro le copie identiche ma in generale contro tutte le copie atte a
riprodurre la stessa impressione generale: anche se vario leggermente le linee,
il colore etc ma chi la osserva ha la stessa impressione generale del prodotto
coperto la copia è considerata una copia di design servile e pertanto non potrà
essere esente dalle sanzioni previste.

La durata del design è di 5 anni ma è rinnovabile con durate di 5 anni in 5
anni fino ad un massimo di 5 anni (25 anni complessivamente).

I design possono essere registrati in Italia ma anche, tramite una procedura
unica che l'Unione Europea ha creato, presso EUIPO. Il deposito presso
l'ufficio europea è chiamato Community Design (CD).

Ottenuto il design, inoltre, è possibile sovrapporre il brevetto per modelli
ornamentali e quindi proteggerlo con una duplice azione.

### Design non registrato

Nei paesi europei, dal 6 marzo 2002, la protezione del design, avente i
requisiti, è garantita anche se non è registrato. In questo caso non è
necessariamente la normativa del Copyright a cui si fa riferimento ma questa
normativa del Copyright è sempre un riferimento esterno a cui il legislatore
pensa. Il design sorge dal momento della creazione. Tipicamente nei paesi della
UE si è stabilito che un design che non è stato registrato ha diritto soltanto
ad una tutela che dura per 3 anni, dopo di che scade. Viceversa 25 anni in 5
anni rinnovabile.


### La procedura del community design

Bisogna depositare una domanda. Questo deposito avviene presso l'ufficio
dell'EUIPO ad Alicante, Spagna (anche in forma telematica). Esiste un esame che
serve a verificare l'esistenza dei requisiti che abbiamo indicato
precedentemente. Se l'esaminatore valuta che non è presente uno di questi
requisiti allora può rifiutare il design, altrimenti registra il design: è
richiesto il pagamento di una fee e, conseguentemente avviene la pubblicazione.
Andando sul sito dell'EUIPO si può scorrere nel database in forma analoga a
quanto si fa per i brevetti, visualizzando i vari documenti di registrazione
con le schede grafiche dei vari oggetti depositati nel design. C'è una
pubblicazione del contenuto del design. La procedura, una volta fatta,
conferisce un diritto che può essere rinnovato a meno che non venga invalidato
da azioni successive perché si scopre la manchevolezza dei requisiti.

## Marchi (o trademarks)

I marchi sono la forma di IP più largamente diffusa in assoluto. È utilizzato
dall'impresa e dalle persone singole per identificare i suoi prodotti. Può
essere costituito da un nome, una combinazione di numeri e lettere, ecc.

Esempi di marchi: la combinazione di scritte con una certa grafica (D&G, Gucci,
etc), le parole non scritte (ondina di Nike), suoni (jingle accensione Mac).

Una delle caratteristiche dei marchi è quella di identificazione dell'impresa
ma ha il potere di essere utilizzato al di là dell'offerta che l'impresa in
quel momento fa. Prendiamo Amazon come esempio: era nato come e-commerce
digitale di libri ma in seguito ha diversificato l'offerta non solo in generale
al marketplace e all'e-commerce ma anche a servizi aggiuntivi differenti come
ad esempio quello della fruizione di contenuti multimediali. Il potere del
marchio consente di diversificare partendo da un livello di notorietà che è
inarrivabile per qualunque entrante che non abbia già un marchio. Ciò significa
che il marchio di per se può essere venduto come un oggetto che ha un valore
perché se trasferito a terze parti trascina con se la notorietà da parte della
clientela anche se chi produce il servizio cambia completamente. Questa è la
ragione per cui in molti casi quando un'impresa va in bancarotta gli unici
asset di valore che l'impresa conserva quando non è più in grado di produrre
profitto spesso sono i marchi che spesso vengono venduti e vengono riciclati.

Requisiti:

1. *No nome generico*. Non posso appropriarmi di un nome che identifica una
   categoria di oggetti. Una sedia non può essere depositata col nome
   distintivo "sedia". Spesso il nome non nasce generico ma lo può diventare
   perché un oggetto diventa popolare e quindi il brand viene identificato come
   la categoria di oggetti in generale (e.g. Kleenex, il Cellophane, ecc).  Chi
   ha un marchio deve fare attenzione che il suo nome non diventi generico per
   identificare l'intera categoria di attributi. Non si possono utilizzare nomi
   fuorvianti, che inducano a pensare attributi non esistenti. Non si possono
   usare identità private di soggetti che non hanno espresso il loro consenso:
   non posso usare il nome di una persona a mio piacimento senza il consenso.
2. *Use it or lose it*. Non è sufficiente registrare un marchio ma è anche
   *necessario utilizzarlo di modo che effettivamente quest'efficacia
   distintiva esista nei fatti*. Bisogna dimostrare che un marchio identifichi
   agli occhi del consumatore l'unicità e l'associazione di quel logo, forma,
   suono con una certa impresa. Se il consumatore non lo identifica perché il
   marchio non è stato utilizzato (l'impresa non agisce per favorire
   quest'identificazione) il marchio per ciò stesso decade. La creatività si
   esaurisce e non possiamo continuare a pensare che si possano registrare
   tutti nomi possibili e immaginabili sperando poi che qualcuno in seguito li
   usi e di ottenere un compenso da un'antica registrazione. Il marchio quindi
   decade se non utilizzato entro 5 anni oppure se il nome diventa comune. Se
   si riesce dimostrare durante un giudizio che il marchio non identificava
   perché non c'erano state azioni per far sì che questo accadesse il marchio
   alla fine non tiene e non conferisce la protezione che dovrebbe conferire. 

La caratteristica del marchio è che il diritto che viene conferito è il diritto
a sfruttare in esclusiva questo marchio ma, a differenza di tutti gli altri
diritti mostrati fino ad ora, questo è illimitato. Se chi lo possiede continua
ad utilizzare il marchio allora questo diritto non scade mai. Può essere
costantemente rinnovato e tipicamente ogni 10 anni. Non c'è però una scadenza,
si può andare avanti all'infinito. Questo diritto all'utilizzo esclusivo
consente di vietare la copia o l'imitazione della copia fatta in modo da sviare
la clientela inducendola a ritenere che un certo prodotto fosse dell'impresa
identificata dal marchio. Non solo la copia servile ma anche la copia che attua
la "dilution", sviamento della clientela e raggiro, confusione rispetto al
fatto che questo prodotto sia effettivamente di quest'impresa.

Nel caso in cui il marchio diventi nome generico e quindi rappresentativo di
una certa categoria di prodotti allora non potrà essere rivendicato l'utilizzo
della parola che lo identifica mentre il marchio, il packaging e tutto il resto
rimangono coperti.

Nell'UE i proprietari dei marchi possono proteggersi in ciascun paese ma è
anche possibile da quando esiste l'ufficio europeo EUIPO ottenere un community
trademark, un marchio comunitario, chiamato CTM. Se il deposito avviene a
livello europeo la copertura viene estesa ai paesi dell'Unione Europea.

È possibile ottenere una protezione attraverso lo WIPO di Ginevra grazie ad un
protocollo firmato a livello internazionale nei paesi aderenti al WIPO (Madrid
Protocol) ed è possibile, attraverso il passaggio dall'ufficio dello WIPO,
ottenere una protezione planetaria del marchio.

### CTM: Procedura di registrazione Trademark (EUIPO)

Diagramma di flusso della procedura per avere la registrazione del marchio
presso l'EUIPO (la copertura CTM, comunitaria). Similmente al deposito del
design si deve effettuare un filing e successivamente un esame. Il risultato
può avere come risultato il rigetto del marchio. Nel caso in cui l'esame tiene
e ci sono i requisiti il marchio viene pubblicato ed è nuovamente disponibile
un database nel quale è possibile verificare quali sono questi marchi. Se si
entra in questo database si possono vedere i loghi e quello che occorre. Dopo
la pubblicazione è anche possibile fare opposizione in modo simile a quello che
accade all'EPO per i brevetti: se qualcuno ha da eccepire che un certo marchio
non doveva essere rilasciato perché non rispettava uno dei criteri, può fare
opposizione nel qual caso c'è un nuovo esame al termine del quale l'ufficio
deciderà se accettare o invalidare la registrazione. Nel caso di rigetto è
prevista una possibilità di appello e di avere un nuovo esame.

## Denominazione d'origine

Denominazione d'origine: quella protezione che viene assegnata per identificare
dei prodotti che possiedono una particolare qualità o reputazione, qualora la
reputazione sia associata all'origine geografica, territoriale di questo
prodotto. Di solito si tratta di prodotti agricoli, alimentari che vengono da
una tradizione territoriale e molto spesso sono denominazione d'origine come
quella del parmigiano, vini, DOCG e tutte le identificazioni che ultimamente
sono state depositate attraverso il circuito dello Slow Food. Slow Food è
altamente attivo nella protezione dei prodotti alimentari e agricoli tipici e
delle varietà regionali e normalmente il tipo di protezione che utilizza è la
denominazione d'origine. Siccome l'Italia ha moltissimi prodotti che derivano
dalla sua tradizione culinaria è estremamente attiva a livello europeo. La
protezione è consentita in registrazione in Italia e presso la UE e consente di
impedire la falsificazione ossia che una persona che non rispetta un requisito che
tipicamente è quello dell'appartenenza regionale e/o dell'utilizzo di un
disciplinare nella procedura che deve seguire per elaborare i prodotti, venga
fermato. Se non si rispettano i requisiti si può essere portati in tribunale e
questo può impedire che i prodotti vengano commercializzati o importati. Il
tribunale può anche utilizzare il cattivo uso del marchio: se sono un
produttore che appartiene all'area geografica che consente di dare il marchio
ma applico male il disciplinare o lo eludo gli altri possono richiedere un
risarcimento perché con la mia azione posso rovinare il bene collettivo che è
la distinguibilità e la caratteristica del prodotto.

## Segreto industriale

Segreto industriale, chiamato anche informazioni segrete. Si tratta del fatto
che un'impresa può decidere di mantenere segrete delle procedure che utilizza
normalmente. Spesso riguarda i processi industriali della manifattura: quando
si fa un prodotto si ha organizzato il processo produttivo e il modo in cui
sono ottimizzati i flussi, gli accorgimenti nella scelta e combinazione dei
materiali etc. tutte queste cose sono atte a conferire una serie di
caratteristiche che forniscono una qualità speciale ad un prodotto e si può
decidere in questi casi di brevettarli se possiedono i requisiti sufficienti
(novità, inventive step, industrialità) o proteggerli con il brevetto per
modello d'utilità ma siccome queste ptorezioni scadono e sarebbero difficili da
azionare in giudizio (i.e. enforcement) la cosa più comune che le aziende fanno
è che quando vengono fatte queste innovazioni vengono tenute segrete.

Cosa può fare il legislatore a riguardo? È possibile ancora per un'impresa
portare in giudizio qualcuno che abbia rubato un segreto? Avevamo detto che si
voleva dare tutela del brevetto solo a chi in cambio facesse disclosure (ossia
mostrare a tutti come si fa una certa cosa). Ci stiamo chiedendo se si può
tutelare una persona che non voleva fare disclosure ma che vuole lo stesso
avere garanzia che il suo segreto venga tutelato e ciò pone notevoli problemi
perché, se una cosa è segreta, è difficile per una terza parte accertarsi della
sua esistenza.

La soluzione: l'impresa può mantenere segrete delle procedure che si ritiene
generare caratteristiche o proprietà distintive o qualunque cosa produca un
vantaggio ma per fare ciò devo mettere in atto delle soluzioni che tendano a
proteggere nei fatti questo valore. Se ho dei dipendenti che vedono e sanno
come si lavora all'interno e sono a conoscenza del segreto, devo fare in modo
che nel loro contratto sia espressamente indicato che questo ciò che loro
vedono è segreto e che sono tenuti a non divulgarlo e che anche qualora
andassero a lavorare per la concorrenza non potrebbero raccontarlo e che azioni
una serie di tutele che evitano a chiunque di poter vedere coi suoi occhi una
certa cosa: se ho visto una cosa che nessuno stava proteggendo devo essere
tutelato anche io perché in buona fede ritengo di copiare qualcosa che nessuno
ha inteso proteggere. Per esempio bisogna fare in modo che non sia possibile
accedere liberamente all'impianto produttivo (una delle ragioni per cui molto
spesso quando si entra in un impresa bisogna consegnare un documento, ci danno
un badge e possiamo entrare solo in determinate aree e non in altre perché
l'impresa deve dimostrare che sta efficacemente facendo capire alle persone che
ciò che vedono non è disponibile a loro discrezione ma che si intende mantenere
segreto). Se queste azioni vengono effettuate in modo accurato allora il
legislatore garantisce all'impresa la possibilità di tutelarsi qualora una
persona non in buona fede cerchi di rubare questo segreto. L'idea è che se per
scarsa protezione, poca cautela da parte dell'impresa questa cosa venga resa
nota, si può vedere perché faccio fare dei filmati che vadano in televisione o
su internet e mi curo poco di proteggere questa cosa e il segreto si diffonde
allora in quel caso non si può azionare la tutela. Se io invece sono accurato
nel proteggere e qualcuno, apposta, viene e vuole filmare per riprodurre o fare
azioni tese a rubare questo segreto (i.e. assumere tutti i dipendenti al fine
di trasferire il loro know-how nella loro impresa) allora posso azionare la
tutela e chiedere che il concorrente venga sanzionato perché sta mettendo in
atto dei comportamenti sleali, aka concorrenza sleale. Il segreto industriale è
quindi tutelato attraverso la normativa contro la concorrenza sleale. Il
problema è che però, siccome sono io a dover mantenere segreta questa cosa
dovrò dimostrare che la terza parte sta facendo un'azione illegale e tesa a
rubarmi qualcosa, quindi è mio l'onere della prova e non lui che deve
discolparsi e poi devo anche provare che io stavo mettendo in atto tutto ciò
che era in mio potere per mantenere questo segreto come tale. In queste
condizioni posso andare dal giudice a chiedere il risarcimento per un furto di
un segreto industriale, altrimenti non si tratta di una protezione azionabile.

## Tabella riassuntiva IP

Sintesi rispetto alle IP con indicazioni riguardo costi minimi (esclusi i
professionisti richiesti). Importanti per utilizzo operativo.


| IP                                | Istituzione                            | Durata massima (anni)                                         | Procedura                | Costo della procedura (€)                            | Tempo medio |
| :---                              | :---                                   | :---                                                          | :---                     | :---                                                 | :---        |
| Brevetto per invenzione           | Ufficio brevetti nazionale, EPO, WIPO  | 20                                                            | Esame                    | Italia: 120-600, EPO: ~4500                          | 2 anni      |
| Brevetto per modello d'utilità    | Alcuni Uffici brevetti nazionali, WIPO | 10                                                            | Registrazione            | Italia: 120 i primi 5 anni, poi 500                  |             |
| Brevetto per modello ornamentale  | Uffici brevetti nazionali, EPO, WIPO   | 25                                                            | Esame                    | Uguale al bevetto                                    | 2 anni      |
| Community design (registrato)     | Uffici brevetti nazionali, OHIM, WIPO  | 25                                                            | Esame, 2 anni            | Per i primi 5 anni: 350, discendente per quelli dopo | 6 settimane |
| Community design (non registrato) | ---                                    | 3                                                             | ---                      | 0                                                    | immediato   |
| Community Trademark (CTM)         | Uffici brevetti nazionali, OHIM, WIPO  | Rinnovabile indefinitamente se utilizzato negli ultimi 5 anni | Esame                    | 900 + 150 per ogni classe aggiuntiva                 | 13 mesi     |
| Copyright per Software            | Tutti gli uffici brevetti europei      | Vita dell'autore + 70                                         | Registrazione come prova | 270                                                  | immediato   |

## Stratificazione della tutela

Le protezioni delle invenzioni richiedono di utilizzare un'insieme di cose:
possiamo avere un brevetto ma anche coprire un prodotto con un marchio in
aggiunta ed identificare il prodotto con un marchio che consenta di avere una
seconda generazione del prodotto al di là del fatto che il brevetto scada o che
la tecnologia che si sta utilizzando diventi obsoleta e quindi venga sostituita
con dell'altra, etc. 

Il senso di trattare i marchi in questo corso non è solo quello della
protezione della proprietà intellettuale ma anche il fatto che molto spesso per
proteggere un'invenzione anche di carattere industriale si utilizzano anche i
marchi.

La normativa in passato è stata dubbiosa sulla stratificazione di più tutele
sullo stesso prodotto perchè queste hanno durate che sono diverse ma
recentemente hanno chiarito in modo inequivocabile che posso tutelare per lo
stesso prodotto (i.e. la nuova moto della Yamaha) sia la forma estetica, sia il
nome, sia il segno distintivo, sia la tecnologia che inserisco al suo interno,
stratificando le tutele.

Elenco delle tutele possibile:

- Brevetto per invenzione
- Estetica del prodotto, packaging e brochure (design?)
- nome del prodotto

### Domande

Si può brevettare la combinazione di queste cose in elenco?  L'aspetto estetico
e quello funzionale insieme no. Non si può depositare una domanda di brevetto
sul fatto che una certa forma è bella. Posso brevettare la funzione con il
brevetto di invenzione e l'aspetto estetico con un altro strumento che è il
modello o design. Non posso brevettare le due funzioni insieme.  Certamente se
brevetto un nuovo motore perchè ha la capacità di seguire un ciclo termico
diverso dal ciclo Otto o dal Diesel e mi invento un ulteriore ciclo termico
certamente lo posso fare e poi magari nello stesso brevetto posso inserire
molti dettagli costruttivi di come è fatto questo ciclo termico e molte altri
aspetti che possono essere invenzioni separate oppure invenzioni dipendenti.
Dipende dalla strategia: posso fare un solo brevetto scegliendo l'invenzione
prediletta e tutte le altre che inserisco sono dipendenti da quella (ossia se
brevetto le due ruote della bicicletta posso anche nello stesso brevetto come
dipendente mettere la tramissione ma se metto la tramissione non potrò
contrastare la contraffazione della sola trasmissione ma quella solo delle due
ruote con la trasmissione). Scelgo qual è il massimo comune divisore. Se questo
è il ciclo Paolo Crippa e non quello Otto o Diesel non posso andare a dire "mi
hai copiato su un camion o su un autovettura", peggio per te perché il camion
lo faccio utilizzando un ciclo diverso dal tuo. "Il camion l'ho inventato io"
"no, solo se accoppiato al ciclo Crippa".

### Un'esempio di stratificazione della tutela

Si prenda l'esempio della cover di iPad. Questa ha una forma estetica
interessante, tutelata con un design worldwide tra cui un design comunitario.
Esiste nel design la tutela centralizzata comunitaria, cosa che non esiste
ancora nel brevetto per invenzione. Il brevetto europeo è una convenzione per
arrivare a un brevetto ma poi devo nazionalizzarlo, devo validarlo. Sulla
sinistra ho è il brevetto europeo sulla stessa custodia. 

Perchè un brevetto insieme al design?

La custodia ha un design diverso dal solito ma anche funzioni particolari:
questa cover permetteva grazie al fatto di avere degli elementi magnetici di
accendere e spegnere il dispositivo ed essendo segmentata con quattro diverse
porzioni potevo ripiegarle di modo tale da far diventare la cover un supporto
fisico.  Queste funzioni che non sono estetiche ma sono funzioni possono essere
tutelate grazie alle caratteristiche fisiche di come è fatta questa cover
grazie al brevetto per invenzione.

