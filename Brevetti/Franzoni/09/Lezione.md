# Lezione 9 --- Valore intangibile e Valutazione dei brevetti

## Slide 1

Da questa lezione in poi si tratterà di una visione più manageriale. I brevetti
sono il mezzo per raggiungere il vantaggio competitivo o la capacità di essere
competitivi sul mercato e diffondere le proprie idee e innovazioni, estrarre
profito dalle opere d'ingegno etc. Il brevetto quindi non è il punto d'arrivo
ma d'inizio che deve essere poi sviluppato per ottenere il giusto compenso per
l'attività innovativa dal mercato e ciò richiede di avere abilità manageriali e
di comprensione delle logiche d'impresa che riguardano gli asset immateriali.

Lo scopo è comprendere come i brevetti e le altre IP vengono intese nella
pratica delle imprese.

## Slide 3

Costi dei brevetti e quanto essi gravino sulle imprese. 

- *Scrittura*: L'impresa comincia a sostenere dei costi, al netto di quelli di
  R&D, a partire dalla scrittura del brevetto. è estremamente delicata la
  modalità di scrittura di questo brevetto. Se è scritto bene sin dal principio
  poi passerà facilmente l'esame e sarà, successivamente, più semplice la
  difesa del brevetto. Una volta che il brevetto è scritto esso può essere solo
  ridotto e non ampliato quindi conviene "corazzarlo" molto al momento del
  deposito perché nel tempo gli eventuali attacchi lo danneggeranno.
- *Procedura*: Esistono dei costi classici "out of the pocket": denaro che
  bisogna mettere sul tavolo per ottenere un brevetto. Sono il minimo
  necessario per attivare la procedura. Questi costi non sono normalmente la
  parte preponderante dei costi che le imprese devono sostenere per avere un
  brevetto ma quella minima. La slide 8 riporta i costi della procedura
  all'UIBM. Il deposito cartaceo a differenza di quello telematico è variabile
  a seconda del numero di pagine depositate. Gli importi richiesti sono
  ridicoli. La procedura è tanto più costosa quanto più vogliamo estendere in
  vari paesi.
- Validazione: questa fase segue la procedura internazionale (EPO, WIPO). Ogni
  paese richiederà documenti per validarlo all'interno dello stato, in
  particolare la traduzione nella lingua ufficiale.
- Manutenzione: Una volta che il brevetto è assegnato ci sono i costi di
  rinnovo (in ogni paese coperto). Se non li si pagano ciò fa decadere il
  brevetto nel paese in cui si è dimenticato di pagare.
- Litigation: nel caso in cui ci siano liti in tribunale causati dai brevetti.
  Tutte le litigation sono molto costose. La slide 7 riporta una tabella già
  vista in precedenza. Quindi se il brevetto coprisse più paesi dovrei coprire
  litigation in ciascun paese e non si ha la garanzia che le corti nazionali
  faranno una decisione in comune (questo è un problema che verrà considerato
  nella trattazione del brevetto unitario). Il risultato è un brevetto a
  macchia di leopardo.

## Slide 4

La tabella riportata nella slide quantifica gli effettivi prezzi che le imprese
pagano al di là dei costi della proceduraper ottenere un brevetto.

Le due colonne mostrano i costi suddivisi per procedura (a destra quella
europea diretta, a sinistra quella europea passando per un deposito PCT).

- Euro direct: in questo caso sono coperti sei stati (rappresentativo della
  copertura media all'interno dell'EPO), ossia due in più rispetto al minimo da
  designare. Il brevetto "medio" ha solo sei rivendicazioni. La procedura di
  mantenimento per 10 anni costa 4.4k€. I costi di representation (fee che
  rappresentano il titolare del brevetto) sono circa 9630€. Ci sono i costi di
  validazione che comprendono quelli di traduzione 3.4k€. La traduzione non è
  fatta da chiunque ma da persone qualificate con certificazione EPO. Esistono
  poi degli ulteriori costi (terzo blocco) relativi al rinnovo validi per dieci
  anni. Il totale è sconfortante.
- Euro PCT: ancora peggio del precedente in termini di costi.

Queste stime hanno allarmato i paesi UE perchè per coprire gli interi US con
brevetti equivalenti i costi si aggirano attorno i 12k€ quindi meno della metà.
Anche per questo motivo si vuole ricorrere al brevetto unitario.

### Note

PCT: Patent Cooperation Treaty.
Attorney: costi legali

## Slide 5

La Commissione Europea, a partire circa dal 2005, ha riflettuto sul fatto che questo sistema appena citato fosse estremamente inefficiente per l'Europa e si è cominciato ad impostare il Brevetto Unitario (aka Brevetto Europeo con effetto unitario). Questo è stato fatto con i seguenti obiettivi:

1. Ridurre i costi elencati prima. Lo studio fatto nel 2005 ha generato il tentativo di generare un sistema unico per l'europa. Si vuole ottenere un comportamento unitario a livello Europeo (come se l'Europa fosse un unico stato). Quando deposito un brevetto, automaticamente scatta la copertura per tutti gli Stati dell'Unione.
2. Avere un giudizio unico in tutti i paesi: non si vuole che nel caso di litigations le decisioni delle corti dei vari stati siano difformi.

Il costo del brevetto unitario sarebbe comunque più contenuto rispetto ai 31k visti prima.

Nel 2012 è stato siglato un accordo tra i paesi della Commissione Europea per ottenere un brevetto unitario. Durante la complessa negoziazione si sono valutate varie problematiche:

1. La traduzione: i costi sono molto elevati. Cosa succede però per i paesi non
   anglofoni nel momento in cui si ha una sola lingua (i.e. inglese)? Molti
   paesi come la Francia e la Germania erano preoccupati, specialmente
   quest'ultima che deposita molti brevetti a livello europei. Quando si è
   arrivati alla ratifica nel 2012 alcuni paesi, infastiditi per i problemi
   linguistici, non hanno ratificato. L'Italia era tra queste ma poi si è
   ravveduta nel 2015 e ha siglato il patto. L'Inghilterra inizialmente aveva
   aderito ma con la Brexit è stata esclusa. La Polonia ha aderito a metà in
   quanto sussiste un secondo problema.
2. La sovranità nazionale: volendo avere un trattamento dei brevetti uguale in
   tutti i paesi (un'unica decisione a livello comunitario) avremmo il problema
   che molti paesi perdono sovranità nazionale poiché ci sarebbe un giudice che
   prende una decisione in un paese dell'Unione che poi avrà effetto anche sui
   territori degli altri paesi. Questo causa una perdita della sovranità della
   magistratura dei paesi nello specifico. Per risolvere il problema si è
   pensato di creare una Corte unitaria in materia dei brevetti (Unified Patent
   Court) con sola giurisdizione in materia brevettuale per tutti i paesi che
   fanno parte del Brevetto Unitario. La Polonia, che ha aderito "a metà"
   perché ha firmato il Brevetto Unitario ma non vuole la Corte Unificata
   Europea.

In questo momento il Brevetto Unitario è pending perché tutti gli stati devono
ratificare la convenzione attraverso delle leggi nazionali. L'Italia l'ha già
fatto. Si sta aspettando la Germania che è lo stato più importante in materia
brevettuale. Questa manca perchè la perdita di sovranità della magistratura in
materia brevettuale non è molto gradita ad alcuniche hanno sollevato due volte
delle eccezioni di costituzionalità di questo trattato presso la Corte
Costituzionale tedesca. Quest'ultima ha tenuto bloccato il giudizio affermando
che era necessario rifare la votazione in Bundestag (Novembre 2020) ma è stato
sollevato un nuovo giudizio di costituzionalità e siamo in attesa di capire
cosa farà la Germania. Quando questa si deciderà è probabile che entrerà in
vigore questo processo.

Cosa succederà a livello pratico? Il deposito avverrà direttamente all'EPO che
gestirà il brevetto unitario senza dover creare un nuovo ufficio, lo rilascerà
nel caso sia rispettoso dei requisiti e questo sarà in vigore in tutti i paesi
aderenti. Il brevetto avrà tre lingue: Inglese, Francese e Tedesco. Nel momento
in cui si apriranno delle litigation, la corte con sovranità a livello Europeo
prenderà una decisione unificata. La corte avrà la sede di primo grado a Parigi
e tale sede avrà due distaccamenti riguardanti materie specifiche: Fisica e
hardware a monaco e Farmaceutica e chimica è vacante. La sede vacante è contesa
da Amsterdam (dove si trova l'Agenzia del Farmaco Europea) e Milano.

## Slide 9

Come posso estrarre ricavi dai brevetti? in un modo simile a quello che accade
a chi vuole sfruttare un appartamento di proprietà. Il primo modo è sfruttando
questo appartamento nel vero senso del termine, il secondo modo è vendendo la
proprietà del brevetto e il terzo è affittandolo. Questo paragone riflette
efficacemente quello che accade anche in ambito dello sfruttamento brevettuale:

1. Uso diretti: Ho un prodotto innovativo, lo metto sul mercato e posso
   sfruttare dei prezzi di vendita più alti rispetto alla concorrenza perchè
   riflette un prezzo di monopolio. A differenza della concorrenza perfetta in
   cui le concorrenze stabiliscono dei prezzi di mercato praticamente uguali
   quindi non vi sarebbe un profitto elevato, nel caso del monopolio il
   monopolista riesce a fissare il prezzo stabilendo un premium price e riesce
   quindi a ottenere un ricavo dai brevetti.
2. Vendita del brevetto a terze parti. Non è facile vendere brevetti. In questo
   caso chi possiede il brevetto si "spoglia" della proprietà, lo notifica agli
   uffici che dovranno poi riassegnare il brevetto sostituendo il nome
   dell'assegnatario con il nuovo titolare.  
3. Mantenere la proprietà del brevetto ma concessione a titolo oneroso del
   diritto a sfruttamento commerciale. Quando si fornisce il diritto all'uso a
   terzi pur mantenendo la proprietà intellettuale si sta facendo un contratto
   chiamato "Licenza d'uso". Questi contratti affermano solitamente in quali
   paesi può essere usato questo prodotto, se è possibile usarla per un
   prodotto solo o per più prodotti ecc. Chi vende l'uso è detto licenziante.

Cos'accade nella pratica? Estrarre ricavi dai brevetti è estremamente
difficile. Non è possibile conoscere quanto le aziende ricavino dai vari
brevetti perché queste sono informazioni riservate ma indizi concordanti
affermano quanto sia difficile avere un ricavo da questi.

- Circa la metà dei brevetti assegnati viene lasciato decadere nei primi anni
  dopo l'assegnazione.
- Solo il 40% dei brevetti arriva ai 10 anni di vita.
- Solo il 20% dei brevetti arriva fino ai 20 anni. quasi tutti questi sono
  brevetti della chimica e farmaceutica.

Molte poche imprese riescono a ottenere profitti.

- Philips, Qualcomm e Thompson, tre società che possiedono portafogli
  brevettuali molto ricchi, generano da .5 a 2 miliardi di dollari all'anno in
  licenza di brevetto.
- Negli USA il 99% dei ricavi è generato solo dal 40% degli assegnatari quindi
  i ricavi sono molto polarizzati. è necessario avere portafogli molto grandi e
  un'attività sviluppata e professionale per estrarre ricavi dai brevetti
  
Quali sono le ragioni del fatto che i ricavi dei brevetti sono così difficili da ottenere?

- Il mercato dei brevetti è illiquido (ci sono poche transazioni. pochi cercano
  di comprare un brevetto e pochi cercano di vendere brevetti.). Il mercato è
  piccolo perché ci sono pochissimi operatori specializzati in questo tipo di
  transazione. Questi operatori fanno:
	1. 1-to-1 broker deal: Un'azienda come ICAP ha un database di
	   acquirenti e sanno a chi può interessare un certo tipo di brevetto.
	   Propongono un 1-to-1 deal e ICAP intascano delle commissioni da
	   intermediari, esattamente come fanno le agenzie immobiliari nella
	   compravendita di case.
	2. Aste brevettuali: ICAP o altri operatori hanno creato queste. Non
	   sono molto diffuse. Nell'asta succede ciò:
		1. Si fa un catalogo con tutti i brevetti e il loro valore di
		   base.
		2. Un giorno e un'ora precisa in cui il battitore che mostra i
		   vari brevetti, li propone al pubblico che decide se andare
		   al rialzo oppure no.
		3. Il brevetto viene dato al miglior offerente.

	  Il problema è che anche quando si cerca di vendere molto spesso
	  vengono comprati dalle Non-Practicing Entities (NPE, aka Patent
	  Trolls o Patent Sharks).

## Slide 12

### Il valore degli intangibili

Gli intangibili: ci serve la proprietà intellettuali perché questi beni non
sono rivali e non sono escludibili e servono dei diritti per proteggerli.

Parliamo della parte economica/manageriale degli intangibili. Pensiamo a
com'era il mondo in passato: il grosso delle risorse (assets) di un'impresa era
di natura tangibile: terreni, fabbricati, impianti industriali, uffici,
capannoni, merci, beni. Questi asset vengono inseriti nello stato patrimoniale
come immobilizzazioni materiali (tangible). I bilanci delle imprese tempo fa
davano importanza a queste immobilizzazioni materiali che costituivano una gran
parte del valore complessivo. Nei bilanci esiste però anche una voce separata
che quantifica le immobilizzazioni immateriali, quelle non costituite da cose
fisiche ma per esempio sono gli IP visti in questo corso e dal famoso
avviamento: cespite di bilancio che quantifica il fatto che un'impresa che
funziona in attività vale di più di un'impresa in liquidazione. Quando vengono
venduti gli asset per stralcio in realtà questa vendita vale meno della vendita
dell'impresa che funziona. Questo significa che l'impresa avviata da sola vale
più degli asset che la compongono. Questo valore nei bilanci viene chiamato
avviamento. In inglese è chiamato goodwill (buone prospettive).

### Note

Cespite (fonte Treccani): Nella terminologia giuridica, finanziaria e
commerciale, fonte, sorgente (di reddito, di entrata, di guadagno).

## Slide 13
## Slide 14
## Slide 15
## Slide 16
## Slide 17
## Slide 18
## Slide 19
## Slide 20
## Slide 21
## Slide 22
## Slide 23
## Slide 24
## Slide 25
## Slide 26
## Slide 27
## Slide 28
## Slide 29
## Slide 30
## Slide 31

## Riferimenti Bibliografici

Slides: "Lezione 7 Valutazione intangibles (14 maggio)"
