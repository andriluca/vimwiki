# Lezione 7 --- Testimonianza Lazzarin (in English)

## Slide 1

Leonardo Helicopter Division.

- Lazzarin: IP manager of Leonardo Helicopter Division.
- Paola Bassi: trainee at Leonardo in IP division.

Aim of the presentation: provide a real life frame with IP specialist point of view.

## Slide 2

A short agenda reported in here.

1. Company overview
2. IP Specialist --- role and responsibilities:
	1. Strategic tasks
	2. Operative tasks
3. Aerospace and defence IP Scenario. An example of how we use IP scenario. How
   we support the company in the strategical decision in IP point of view.
4. Questions

## Slide 3

Leonardo is a global company in areospace, defence and security. It's divided into 4 divisions:

1. elicopters
2. defense electronics and security
3. aeronautics
4. Space: made of join ventures with other big company

## Slide 4

Some figures to focalize the position of the azienda.
They invest an important sum in R&D and this is at the basis of intellectual prperty.

## Slide 5

50k employees. The azienda is spread around the world and this means that the
protection must also be spread all over the world.

## Slide 6



## Slide 7

commercial range.

## Slide 8

Military/Government range.

dual use, naval and battlefield.

## Slide 9

TiltRotor

## Slide 10

A new product: without pilot for civil and military mission.

## Slide 11

Supporting the fleat ???

## Slide 12

Lecture material from here.
Let's enter the role of IP specialist.

## Slide 13

What's up in this department. In the office we can say that in this point of view the ??? 

IP specialist is the important figure in the department.

1. friends:
	1. patent attorney: they represent the azienda
	2. Cimpany ip network: specific network of specialist has been defined. they work in specific areas and they have all IP formation because
2. Toolkit:
	1. patent watching
	2. patent search
	3. freedom to operate
	4. professional tools
	5. ipm annual budget

Strategic task:

1. IP Scenario definition: it's important to define ???
2. IP protection: strategy decision, an important responsibility in IP division.
	1. TRL: technology ready levels (from 1 to 8)
3. Legal issue:
	1. legal prosecution.
	2. third party opposition. Decision on suing concorrenza.
	3. contract/JV, IP agreement.

Operative Task:

1. Pantetnability Analysis
2. IPR "lifecicle": it's normally 20 years but sometimes it's not necessary to keep it for 20 year and we need to decide which we want to abandon. A simple approach on this: the first gate is the technical solution is going to the products or not (so if it enters the market or not). if protection goes to the market we can decide to keep it, otherwise we don't maintain it. Sometimes we decide to maintain the patent just in one country and not on the other.
3. Patent Exploitation/Scouting: finding out if someone is infringing the technical solution protected by patient. keeping an eye on especially in expos. The level of litigation is not so high in this sector.


## Slide 14

just some example of our patents.

## Slide 15

Competitive scenario. it's a tool to understand who's the owner of IPR or technical domain and who can represent for us an opportunity if we want to create some intervention or a risk for our business (because the scenario can represent if i am an obstacle to the IP exploitation or not ).

## Slide 16

right part: top 10 patent owners.


## Slide 17

How can we use A&D (aerospace and defence) map seen to the previous slide?
There are different needs of the company. for instance Enhanced Rotorcraft.
right part: technologies that can come handy to solve the needs. Graphene falls down to material processes as clusterization. if we go back to the previous slide we can find out that on the left part we find that motorization is occupying the majority of ???. if i want to improve the engine of helicpoters we have to fight a lot because there are lots of patents for technologies. Even the maturity level of technology is important.

Let's take another example. graphene technology. in the previous slide it falls down to the cathegory of material processes in the graph on the left --> 4871 patents: we have interesting number of patents where we can find many information about material processes and we can search through them the ones related to graphene. We can say that a patent database should be clusterized and read really well.

## Slide 18

Thank you for your attention. Questions.

1. Which answer do we take if someone infringes a patent? there are different
   measures to investigate. Internet helps a lot to it. We are talking about
   product ??? in the company there are forums in which there are people
   talking about the technology improvement and the technology ready level.
2. Graphene: what is the purpose on this field? knowing that our task is
   supporting the company in the decision we know some reasons. it has a lot of
   characteristic in the structural and in the electrical domain. we don't know
   how to use this technology, that's why ???
3. If we have a patent under military secret how do we know that a particular
   ??? ? in case of military secret the definitions of military secret states
   that we need to have some devices and procedures to keep under control the
   secret. if someone else built your solution you are obligated to
   demonstrated that it can maintain the secret ??? this is why the military
   secret is not useful to protect the IP. Bell V ??? Bell decided to protect
   the machine with normal patent. This is a demonstration that the patent is
   the best solution for protection with respect to military secret.
4. What are required skill in IP manager? Aeronautic engineer. the request at
   the beginning was to setup a good ip management system. ???

## Testimonianza Paola Bassi

### Slide 1



### Slide 2

Urban air mobility,
methods and tools
first result
what is the benefit of the work
how it is to work with Leonardo

### Slide 3

Urban air mobility: lot of people are going to move to the city. how are we going to move and commute? Aerial vehicle

### Slide 4

Task: characterization of urban air mobiity though intellectual property.

### Slide 5

Methods:

1. preparing the research: she used some keywords.
	1. given keywords
	2. study the weight of keywords
	3. find new keywords
3. collecting patent
5. iteration

### Slide 6

In the first table for the topic of propulsion we have 5 words given at the beginning. then we can see the enlargement of the list with keyword found by herself. in the end there is the last version subdivide into categories.

### Slide 7

She ended the research and presented the work to the collegues. these are the points presented

1. publication trend
2. main classification codes
3. trend
4. who are the major players and who had the major number of patents
5. ???

### Slide 8

Some consideration made for each topic. 

### Slide 9

??? riascolta da qui
