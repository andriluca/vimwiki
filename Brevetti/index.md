# Brevetti

| Lezione                      | Argomento                                | Rielaborata | Slides comprensibili | Giorni | Link(s)                                                                                             |
| :---                         | :---                                     | :---        | :---                 | :---   | :---                                                                                                |
| [01](Franzoni/01/Lezione.md) | Brevetti e requisiti (1)                 | Sì          | Sì                   | 23.02  | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=4b6c95ccd6a74574a890df2d7d70da1a |
|                              | Brevetti e requisiti (2)                 | Sì          | Sì                   | 02.03  | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=99a79e3e01454628bffa297890a43a9b |
| [02](Franzoni/02/Lezione.md) | Diritti brevettazione e IP               | Sì          | ?                    | 09.03  | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=feaefaa14c704fb68d8ac4ffccd56ebd |
| [03](Crippa/03/Lezione.md)   | Testimonianza Crippa                     | Sì          |                      | 16.03  | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=914cdb88256e4ea9ab32646ab2dd5588 |
| [04](Barbieri/04/Lezione.md) | Procedure Brevettuali (1)                | Sì          | Sì                   | 23.03  | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=f633659b97fc406090eb1de799142ca1 |
|                              | Procedure Brevettuali (2)                | Sì          | Sì                   | 23.03  | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=cd1b77df5c704f1a967e925911acff74 |
| [05](Barbieri/05/Lezione.md) | Ricerche di Prior Art (1)                | Sì          |                      | 30.03  | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=084d99aaa70b463ca01cb12643ebe13a |
|                              | Ricerche di Prior Art (2)                | Sì          |                      | 30.03  | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=0a1360c007aa44f897d3fba027199679 |
| [06](Barbieri/06/Lezione.md) | Ricerche Brevettuali + esercitazioni 1-2 | Sì          |                      | 09.04  | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=3c06c136460541bda8e9181201d08550 |
| [07](Lazzarin/07/Lezione.md) | Testimonianza Lazzarin                   | Appunti     |                      | 16.04  | Non registrata per motivi di Copyright                                                              |
| [08](Barbieri/08/Lezione.md) | Marchi e Design                          | Sì          |                      | 04.05  | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=77e7fef6575a405694908d5caec70700 |
| [09](Franzoni/09/Lezione.md) | Valore degli Intangibles                 | No          |                      | 14.05  | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=0f4ba74d9e0d4f7bb1111e2632a9405d |
| [10](Barbieri/10/Lezione.md) | (1)                                      | No          |                      | 18.05  | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=d4621c7f90884422ae9abf4429a1a135 |
|                              | (2)                                      | No          |                      | 18.05  | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=c4c008717853471393cb72074d6471c1 |
| [11](Franzoni/11/Lezione.md) |                                          | No          |                      | 21.05  | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=f22ea6d31c8d4ac7bafcf508ca8d33a1 |
| [12](Casucci/12/Lezione.md)  | Testimonianza Casucci                    | No          |                      | 25.05  | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=b2fecf67a9154eb683cae2c4785dfb02 |
| [13](Franzoni/12/Lezione.md) |                                          | No          |                      | 28.05  | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=dbeedaf88e3142ca9e70b57380441c48 |


# Modalità d'esame

Esame semplice, a quanto pare può essere scritto oppure orale a seconda del
numero di persone che lo deve svolgere.  C'è il salto d'appello nel caso la
valutazione sia insufficiente.

# Dispense

|                               | Argomento                      |
| :---                          | :---                           |
| [01](Franzoni/01/Dispensa.md) | Brevetti e requisiti           |
| [02](Franzoni/02/Dispensa.md) | Diritti brevettuali e altre IP |
| [04](Barbieri/04/Dispensa.md) | Procedure brevettuali          |
