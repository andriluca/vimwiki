# Introduzione al corso

## Informazioni logistiche

Le lezioni verranno tutte registrate e la piattaforma Webeep raccoglierà i link.

## Obiettivi del corso

Dobbiamo astrarre il nozionismo imparato in triennale per poter essere in grado
non solo di comprendere un circuito ma anche di progettare la soluzione ideale.
Questo genere di corso non è ricchissimo di nozioni. I concetti qui trattati
non vengono assimilati subito, ma è necessario un tempo per maturare i concetti
e sviluppare nuovi approcci ai problemi. Si suggerisce di seguire e fare
esercizi quando richiesto. Lasciarsi indietro le tematiche porta a scarsissimi
risultati in termini di valutazione sulla prova d'esame.

L'obiettivo è fornire conoscenze necessarie a comprendere e utilizzare le più
recenti tecnologie nella valutazione e progettazione di strumentazione
biomedica. Ci si sofferma su principi di funzionamento, sulle applicazioni
cliniche, diagnostiche e terapeutiche (i.e. applicazioni biomediche) e sulle
architetture dei sistemi di elaborazione e trasmissione dei dati.

## Programma del corso

- *Architettura generalizzata di un sistema di misura*: blocchi funzionali
  presenti nei vari sistemi di misura. 
- *Comprensione dei singoli blocchi*. A partire dal front-end analogico:
  partendo a valle del sensore per poi proseguire nell'elaborazione del
  segnale.  Questo corso è strettamente collegato al corso di Aliverti di
  Sensors. Si parte da sensori già condizionati però per concentrarci solo
  sulle tematiche d'interesse.
- Peculiarità approccio sistemi analogici vs digitali. Vantaggi e svantaggi.
- Principi di *funzionamento dei processori* e come le diverse funzioni vengono
  implementate nei dispositivi presenti sul mercato oggi. Quali sono i concetti
  che stanno dietro. Esistono diverse tipologie di processori, ciascuna con
  delle caratteristiche da conoscere.
- *Sviluppo del firmware*: programmi che, una volta caricati sul controllore,
  servono a controllare l'esecuzione delle particolari funzionalità.
- *Problematiche relative al real-time*. Nella realtà in cui viviamo dobbiamo
  considerare un risultato giusto fornito per tempo. Un air-bag deve scoppiare
  nel momento appropriato per limitare i danni dell'urto di una macchina.
- *Cenni di elettronica di potenza* per circuiti di alimentezione e controllo
  di attuatori.
- *Trasmissione dati in ambito biomedico*. Rispetto allo scorso anno viene
  ridotta la porzione sui dettagli dei mezzi di trasmissione e su certi
  protocolli. Verranno trattati i sistemi wireless e IoT, in quanto molto
  diffusi.  Quali sono le potenziali applicazioni trattate.
- Come definire le varie specifiche dei dispositivi biomedici. Non dipendono
  solo da ciò che dobbiamo misurare ma anche dal contesto in cui i macchinari
  operano (in mobilità o in terapia intensiva).  Si studia anche il ventilatore
  meccanico, che oltre a monitorare si aziona per ventilare il paziente.

### Applicazioni biomediche d'interesse:

- Strumentazione per il monitoring (rilevazione parametri fisiologici): Monitor
  per terapia intensiva, funzione dei vari moduli.
- Imaging diagnostico: Ecografo. Questa parte non consiste in uno studio del
  principio di funzionamento (verrà fornito solo un cenno) ma nell'analizzare
  come i processori e i dispositivi di potenza vengono impiegati per realizzare
  il funzionamento di tale dispositivo.

## Attività di laboratorio

Modello simile a quello dello scorso anno. La piattaforma è il microcontrollore
Arduino. Questo è una serie di dispositivi hardware che vengono utilizzati con
una suite di software che rende la programmazione di questi dispositivi molto
semplice. Non vogliamo utilizzare la piattaforma di Arduino ma vogliamo
scrivere direttamente il codice del processore senza l'uso di librerie che
semplificano la scrittura del codice. Vogliamo sporcarci di più le mani andando
a scrivere.

## Modalità di verifica

Esame scritto e orale. Prova impegnativa. 3 ore di tempo per lo scritto. 

### Esame scritto

Consiste in quattro domande:

1. Esercizio di progettazione. Verrà descritto un dispositivo che può avere
   impiego biomedico (e.g. pulsossimetria ...) quali sono gli elementi a
   disposizione, le specifiche del dispositivo. Lo scopo sarà progettare
   hardware e firmware in grado di soddisfare i vari requisiti. Più della metà
   del tempo che ci vedrà impegnati e più della metà della valutazione.
2. Teoria. Si risponde bene se si ha studiato.
3. Teoria. Si risponde bene se si ha studiato.
4. Piccolo esercizio che serve per dimostrare la capacità di implementazione di
   alcune funzioni.
   
Fatto ciò, se il voto espresso in trentesimi supera il 16 si può accedere
all'orale che è più di conferma e conoscenza personale. Ci si incontra uno a
uno per confermare il risultato ottenuto nello scritto, rivedendo alcuni
aspetti e, se non ci sono distonie tra quello fatto all'orale e il risultato
dello scritto, questo si conclude con la conferma del giudizio (da +2pt a
-infinito).

Si mantiene il voto dello scritto in tempo indefinito per un anno. La
presentazione all'esame invalida il voto precedente.

Il voto si può sempre rifiutare, valutando che comunque all'orale c'è il +2
massimo.

## Testi consigliati

Per realizzare questo corso vi sono argomenti che vengono affrontati in diversi
corsi. Il front-end analogico (amplificazione etc), la parte di
microprocessori, la visione in ambito biomedico etc. Non esiste un unico testo
ma vi sono diverse fonti online.

Non sempre la preparazione ricevuta negli anni precedenti è in linea con le
aspettative degli approcci noti al Professore.

## Valutazione

Il voto sarà in decimi per ogni esercizio dello scritto. Nella creazione del
voto finale, vi saranno domande più difficili di altre e quindi i pesi relativi
per generare il voto in trentesimi sono diversi. Una metà del tema d'esame sono
associati all'esercizio 1 mentre abbiamo circa 6-7 punti per le domande di
teorie e 8 punti per l'esercizio 4. Chi va sopra il 30 ha un'alta probabilità
di prendere la lode.

*Consiglio*: prima eseguire le domande e poi concentrarsi sull'esercizio 1 che
assorbe tutto il tempo a disposizione.

Le linee di codice non sono così tante nel TdE. La sintassi non è essenziale ma
il Professore vuole comprendere quali sono i passaggi svolti dalle subroutines,
dall'ISR, la comunicazione tra quest'ultima e la main e il controllo
dell'hardware progettato. Negli anni passati si dava circa 30% del voto
all'hardware e il restante al firmware. Da non sottovalutare l'hardware. Ogni
componente dev'essere dimensionato correttamente.

# Lezione 1 --- Lo strumento di misura

L'oggetto d'interesse del corso è *lo strumento di misura*. Bisogna capire
inizialmente cos'è e quali sono i blocchi che lo costituiscono. Lo schema
generalizzato del sistema di misura è l'insieme dei blocchi funzionali. Non si
descrive alcun dispositivo fisico nel merito ma uno generico. Il dispositivo
fisico (i.e. reale) rappresenta un sottoinsieme di questi blocchi.

**Strumento di misura**: dispositivo in grado di effettuare delle misure
quantitative. Lo strumento fornisce informazioni riguardanti grandezze fisiche.

Nel caso biomedico, le grandezze misurate sono di natura biologica o medica.

Si considerino alcuni esempi riportati: un termometro a mercurio e stetoscopio.
Questi strumenti possono essere anche semplici e forniscono delle misure
qualitative. Lo stetoscopio non è classificato propriamente come uno strumento
di misura ma serve a fornire delle informazioni più di tipo qualitativo. Le
applicazioni biomediche modificano le specifiche dei dispositivi in modo
importante. Si pensi, ad esempio, che il termometro da cucina è sicuramente
diverso rispetto al termometro che viene utilizzato per misurare la temperatura
corporea non solo sul range ma all'interazione che il dispositivo ha con il
paziente. Da ciò discende un numero significativo di requisiti e specifiche che
sono tipiche di queste applicazioni.

La terapia intensiva utilizza un ampia gamma di dispositivi biomedici (e.g.
ventilatore meccanico, monitor, sistemi di controllo della temperatura, etc)
Questi sistemi devono fornire informazioni e, nella maggior parte dei casi,
co-operare tra di loro con lo scopo di fornire le informazioni alla cartella
clinica informatizzata per poter essere disponibli al professionista per le
valutazioni cliniche sul decorso della patologia.

Per quanto riguarda la strumentazione biomedica questi dispositivi diventano
de-facto biomedici quando i loro scopi diventano medici (diagnostici,
terapeutici o di misurazione). La misurazione delle variabili fisiologiche
permette per identificare fenotipi di pazienti e situazioni patologiche.

I dispositivi biomedici hanno due possibili ambiti e funzioni:

1. *Misura*: offrire esclusivamente informazioni su alcune variabili.
2. *Somministrare terapie*: erogare farmaci, grandezze di tipo elettrico, gas
   (e.g. ventilatore meccanico), etc.
   
![Struttura generalizzata di un sistema di misura\label{fig:struttura_generalizzata}][struttura_generalizzata]

Facendo riferimento alla Figura \ref{fig:struttura_generalizzata} si possono
identificare diversi blocchi funzionali:

- *Il paziente*, l'"oggetto" d'interesse. 
- Sensore (uno o più): Il primo interfacciamento col paziente è rappresentato
  dai sensori, strutture che trasducono le grandezze fisiche d'interesse in
  grandezze elettriche (i.e. potenziale, tensione). 
- Tali grandezze elettriche sono diverse da sensore a sensore e da strumento a
  strumento, per cui c'è bisogno che siano *interfacciate* con lo strumento
  (i.e. modificate, adeguate, standardizzate) in termini di grandezza,
  caratteristiche elettriche, impedenza etc. *Interfaccia*: fornisce in modo
  adeguato il segnale all'unità di elaborazione (aka di computazione).  
- Il blocco di computazione deve fornire alla fine una certa grandezza con
  l'unità di misura corretta. Se vogliamo andare a misurare la temperatura del
  paziente, usando un trasduttore trasformiamo la temperatura in un potenziale,
  una variazione resistenza o altre grandezze di tipo elettrico che hanno un
  legame noto con la variabile d'ingresso ma che ora ha modificato l'unità di
  misura.  All'uscita del blocco d'elaborazione l'utente vorrà conoscere la
  temperatura in gradi, quindi questo blocco di elaborazione avrà lo scopo di
  compensare gli effetti delle trasduzioni e alterazioni che sono state
  effettuate a livello del sensore e degli amplificatori che sono serviti per
  garantire l'interfacciamento col blocco di elaborazione del segnale.
  L'elaborazione può diventare sempre più complicata fino ad arrivare a fare
  classificazioni del segnale e all'identificazione  di condizioni critiche
  (e.g.  segnale ECG).
- Nel caso di dispositivi terapeutici si devono considerare anche gli
  *attuatori* all'interno dello schema, disposti lungo un percorso parallelo
  con direzione opposta a quello della catena di acquisizione del segnale. Il
  blocco di elaborazione determina il tipo d'azione da svolgere verso il
  paziente e questa viene mediata da attuatori (e.g. nel ventilatore meccanico
  delle valvole apriranno il passaggio alla miscela d'ossigeno secondo
  parametri che sono stati impostati dal clinico). 
- Gli attuatori hanno bisogno di variabili elettriche appropriate. Se ho delle
  valvole di tipo voice-coil, ad esempio, vengono attuate da un meccanismo che
  agisce sulla base della quantità di corrente che passa attraverso un
  avvolgimento e vogliamo garantire che questa corrente sia effettivamente
  erogata agli attuatori, una quantità di corrente decisamente maggiore di
  quella erogata dai segnali prodotti dai controllori (saranno necessari dei
  transistor BJT e un'alimentazione esterna). Questa considerazione ci fa
  pensare che siano, anche in questo caso, necessari dei *circuiti di
  interfacciamento* in grado di far si che il segnale generato come comando
  dell'attuatore diventi adeguato alle caratteristiche elettriche di potenza
  dell'attuatore stesso.

Ci si deve soffermare approfonditamente sui vari blocchi funzionali che
costituiscono il sistema di misura per poter determinare come agire in fase di
progettazione di un dispositivo del genere.

## Caratteristiche fondamentali dei blocchi funzionali. 

### Sensore

- Identificazione del *parametro appropriato*. Una delle principali
  problematiche è relativa all'accesso alla variabile d'interesse. Ad esempio,
  la gittata cardiaca per essere misurata ha una necessità di interventi
  invasivi. Il flussimetro dev'essere inserito all'interno del corpo aprendo il
  paziente. In alternativa è possibile misurare indirettamente delle variabili
  che possono contenere la variabile d'interesse. Nel caso della gittata
  cardiaca posso utilizzare un sistema ad ultrasuoni e, misurando lo
  scostamento doppler del riflesso dell'onda a ultrasuoni che può essere
  prodotta, posso ottenere delle informazioni relative al flusso sanguigno
  indirettamente. La variabile fisica che si misurerà quindi non sarà una
  velocità, bensì uno scostamento doppler di frequenza. Il sensore quindi dovrà
  rilevare il parametro *appropriato* che non è detto sia il parametro oggetto
  del nostro dispositivo ma un parametro che consente di determinare,
  eventualmente in combinazione con altri, il valore delle variabili
  d'interesse.
- I sensori devono avere una *risposta in frequenza adeguata* al contenuto in
  frequenza della variabile che stiamo studiando.
- Deve garantire un'*interfacciamento sicuro col materiale biologico che viene
  studiato*. L'interazione col paziente è estremamente importante e dobbiamo
  garantire che quest'interazione non produca danni diretti e/o indiretti al
  paziente. Non solo dobbiamo essere sicuri, al collegamento del paziente con
  un ventilatore meccanico, che le pressioni non arrivino mai ad essere
  pericolose per il paziente in alcun caso, nemmeno nel caso di
  malfunzionamento del dispositivo (azione diretta) ma anche garantire che al
  termine del trattamento di un paziente e all'inizio del trattamento di un
  altro non vi siano rischi di cross-contaminazione (i.e. patogeni di un
  paziente sono trasferiti ad un altro perchè rimasti all'interno del
  dispositivo).

### Attuatore

- Capacità di erogare l'agente, la variabile di controllo cui sono destinati.
- Controllare parametri biochimici, biofisici e bioelettrici.
- Devono garantire un'interfacciamento sicuro per il materiale biologico e per
  il paziente.

### Interfaccia

È necessario creare un interfacciamento in cui:

- *Caratteristiche elettriche e dinamiche del sensore/attuatore siano
  compatibili con unità di elaborazione*. Quest'ultime si attendono una certo
  range di tensioni in ingresso nel caso dell'acquisizione del segnale. Ogni
  sensore ha una sua uscita che può essere differente in tensione. Le grandezze
  devono essere adeguate, modificate ed essere poi compatibili col blocco di
  elaborazione. Non possiamo "sparare dentro" un segnale troppo elevato. 
- Si preservi il SNR (Signal to Noise Ratio). Non si vuole peggiorare questo
  rapporto.
- *Si garantisca che i segnali di comando non mettano a rischio il
  funzionamento, ad esempio, dell'attuatore*. Se questo attuatore ha massime
  correnti di esercizio, il controllore deve garantire che queste correnti non
  siano mai superate anche nel caso di malfunzionamento/errore del firmware.
- *Si rispetti l'ampiezza di banda dei sensori/attuatori*.
- Si abbia un'interfacciamento *safe*. Bisogna garantire una grande attenzione
  anche alle stesse macchine. Un esempio: si consideri un paziente in
  fibrillazione ventricolare (identificato da un monitor ECG). Questo
  dev'essere rianimato con un defibrillatore, che eroga per breve periodo un
  elevato potenziale per depolarizzare tutte le cellule e tentare di
  ripristinare il pacemaker naturale del nostro cuore. Queste tensioni sono
  dell'ordine di centinaia di Volt per periodi brevi e nel momento in cui
  applico queste tensioni, esse arrivano anche al monitor ECG che, abituato ad
  amplificare tensioni di mV ora si trova 100V. Se non avessi un'interfaccia
  correttamente dimensionata renderei un monitor ECG monouso o disposable,
  oltre a non sapere nemmeno se il paziente è stato rianimato correttamente o
  no.

### Unità d'elaborazione

- Riportare informazioni per essere utilizzate correttamente da umani.
  L'*informazione dev'essere accurata* e facilmente fruibile.
- Controllare il funzionamento dell'intero sistema evidenziando eventuali
  anomalie. Dare un'informazione sbagliata non significa solo perdere
  accuratezza ma anche indurre il clinico nell'utilizzare una strategia
  terapeutica potenzialmente dannosa e controproducente per il paziente.
  Esistono sempre possibili malfunzionamenti e guasti.  Noi vogliamo garantire
  che l'informazione che viene erogata sia la più affidabile possibile.
- Data storage and communication, parte dedicata alla comunicazione mediante
  rete.
- Signal processing: l'informazione non viene direttamente misurata ma bisogna
  elaborare correttamente il segnale.
- Avvisare l'utente in presenza di valori anormali del paziente e di anomalie
  del sistema (perdita di performance) mediante allarmi prima che la situazione
  diventi dannosa.
- Classificazione del dato: identificazione automatica di alcuni pattern e
  informazioni che identificano situazioni patologiche predefinite. Non tutti
  sanno leggere ed interpretare adeguatamente le informazioni riportate.
  Un'identificazione automatica dell'evento potrebbe essere uno step-forward
  nella potenziale diagnosi dell'alterazione.

## Modello generalizzato di strumento di misura

Dalla struttura generalizzata dello strumento di misura possiamo andare ad
esplorare dettagliatamente ciascun blocco funzionale.

![Modello generalizzato.\label{fig:modello_generalizzato}][modello_generalizzato]

Si parte dalla catena di acquisizione del segnale a partire dal paziente per
finire al display davanti al medico.

Declinando nel dettaglio i blocchi funzionali presenti all'interno di questo
schema è possibile identificare due diverse fasi all'interno dell'interfaccia
(aka front-end):

1. Amplificazione del segnale. S'intende la modifica delle ampiezze del
   segnale. Questa può essere sia amplificazione che attenuazione in
   determinati casi.
2. Filtraggio del segnale.

Dopo il blocco di front-end si giunge al blocco di elaborazione (signal
processing). Questo è collegato ad un dispositivo dedicato denominato display.
Il display non è necessariamente un dispositivo di output a colori o uno
schermo touch-screen ma anche un elemento che mostri in modo facilmente
fruibile le informazioni all'operatore. Qualunque strumento utilizzato per
comunicare con l'utente verrà denominato "display".

Normalmente le variabili gestite dal sistema di front-end sono le ampiezze del
segnale (da cui deriva la presenza dello stadio di amplificazione) e il
contenuto in frequenza del segnale (da cui deriva la presenza dello stadio di
filtraggio).

La registrazione/trasmissione del segnale si trova a valle dell'elaborazione,
così da comunicare un'informazione già depurata e non grezza.

Alcuni sensori hanno bisogno di una determinata energia da essere misurata
(e.g. si pensi all'assorbimento dell'energia luminosa nella pulsossimetria, la
radiazione X per produrre poi un segnale per la TAC). Bisogna fornire un
segnale di base e si misura poi l'attenuazione della radiazione
elettromagnetica ad esempio. Un altro esempio è la pressione fornita dal
macchinario che misura la pressione arteriosa.

### Linea di retroazione

L'utilizzo di blocchi di elaborazione sempre più evoluti e la conoscenza sempre
più approfondita di sistemi di controlli automatici e della possibilità di
intervenire in maniera adeguata per modificare le caratteristiche di alcuni
sistemi hanno portato all'implementazione di sistemi di controllo a retroazione
per il governo di tutti i blocchi funzionali della catena d'acquisizione. Se il
sistema d'elaborazione volesse fornire informazioni sullo stato di questi
blocchi d'acquisizione e dell'attuatore, questa catena di retroazione prima era
visto come azione di erogazione della terapia al paziente ma questo può fornire
utili informazioni ed impostazioni per adeguare il funzionamento di tutti gli
altri blocchi funzionali. Ad esempio se voglio misurare un certo tipo di
grandezza, essa viene amplificata per sfruttare al meglio il range del
campionatore e dell'ADC. Se misuro l'ampiezza che sto studiando e valutando e
scopro che quest'ampiezza è troppo bassa potrei modificare sull'amplificazione
del segnale e aumentare il guadagno dell'amplificatore di modo da riuscire a
coprire comunque tutto l'intervallo di campionamento dell'ADC. Se ho un ADC a
10 bit su 5V se ho un massimo di tensione di 1V allora la risoluzione sarebbe
1/5 di quella che avrei su 5V. Posso quindi amplificare questo segnale in modo
da garantire una risoluzione costante in percentuale del valore letto. Ciò
aumenta notevolmente le performance del dispositivo. La retroazione può
avvenire anche sul filtraggio (e.g. ho componenti a determinate frequenze che
sono fastidiosi). La retroazione può intervenire anche sul blocco funzionale
che serve ad ottimizzare il funzionamento del sensore (e.g. pulsossimetro
utilizzato da un bambino con dito piccolo e pelle sottile vs omone con dito
grosso. Essendo interessato a piccole variazioni attorno ad un valore se ho la
possibilità di controllare l'intensità luminosa generata dal diodo LED posso
fare in modo che la radiazione che colpisce il fotodiodo sia sempre nel range
di funzionamento ottimale del fotodiodo e non sia nè troppa nè troppo poca).
Quindi i sistemi di controllo e feedback intervengono a livello di attuatore ma
anche ottimizzando i parametri operativi dei diversi blocchi funzionare di modo
da migliorare le performance del sistema nei diversi contesti cui è sottoposto.

### Calibrazione del sistema di misura

Nel caso di guasto l'unità di elaborazione deve generare un allarme. È
necessario allarmare l'utente quando qualunque di questi blocchi modifica le
sue performance (i.e. il sensore si rovina e cambia la funzione di
trasferimento. l'amplificatore altera il guadagno, i filtri alterano la
frequenza di taglio etc). Se queste cose non vengono identificate si induce
all'errore il clinico. Si inietta un segnale noto nella catena d'acquisizione e
successivamente si effettua una lettura per identificare eventuali errori. Se
leggo un valore diverso da quello atteso qualcosa non va. Molto spesso è
difficile andare ad effettuare una calibrazione accurata tutti i giorni
intervenendo sulla stessa variabile fisica, quindi a monte rispetto al sensore.
Tipicamente la calibrazione viene fatta nel momento della produzione e con
valutazione periodica in funzione del livello di criticità del dispositivo
nell'utilizzo sui pazienti. Se devo fare spesso la calibrazione posso iniettare
il segnale a valle del sensore e testare il resto dei blocchi funzionali. Posso
identificare tutti gli altri guasti ma non quelli del sensore in questo caso.

### Alimentazione

Chiamata anche power supply, l'alimentazione di tutti questi blocchi dev'essere
sempre opportunamente dimensionata. Molto spesso questo blocco non viene
indicato perchè è naturale che sia incluso in questa tipologia di strumenti.
Vedremo diversi modi per alimentare un circuito, dalle batterie ai più
complessi alimentatori di tipo switching.

L'alimentazione è un aspetto critico per diversi motivi:

1. *Sicurezza*. I guasti potrebbero creare pericoli per paziente e/o medico.
   Tipicamente l'alimentazione presenta tensioni di rete da una parte (voltaggi
   molto alti). È da tenere presente che la distribuzione di rete elettrica è
   poco controllata ed è soggetta a variazioni anche molto importanti (basti
   considerare cosa accade se un fulmine colpisce un palo della luce in una
   casa in campagna. I fili che portano corrente a casa possono portare molte
   più correnti). La progettazione efficiente dell'alimentazione evita
   potenziali rischi per gli utenti.
2. Gestendo correnti e tensioni elevate, nel caso di guasto, ho degradato
   qualsiasi funzione all'interno di questo dispositivo. Avendo un guasto a
   qualunque dei sottoblocchi il resto del dispositivo può identificarlo e
   prendere contromisure, minimizzando il rischio per il paziente.  Lo
   spegnimento causa, nel caso di ventilatore meccanico, l'arresto anche della
   ventilazione tramite attuatore.
3. Batterie a ioni di litio: possono facilmente esplodere in determinate
   condizioni di carica (ossia se sottoposte ad un assorbimento eccessivo di
   carica).

## Componenti di uno strumento biomedico

La catena di misura prevede i seguenti elementi:

1. Grandezza da misurare.
2. Sensore.
3. Condizionamento del segnale.
4. Circuiti di elaborazione del segnale.
5. Dispositivi di presentazione del risultato.
6. Alimentatori.
7. Dispositivi di memorizzazione.
8. Interfacciamento con altri sistemi e trasmissione dei dati.

In questo corso si andranno a studiare dal terzo blocco funzionale in poi, i
restanti sono trattati nel corso di Aliverti. Prima di studiare come sono
implementati i blocchi funzionali appena elencati è necessario avere conoscenza
sulle tecnologie di base di questi (come implementeremo il blocco di
elaborazione). Non è detto che tutti i blocchi servano sempre ma a seconda del
dispositivo che si vuole realizzare.

### Elaborazione di segnali

Il blocco di elaborazione caratterizza più degli altri blocchi funzionali le
performance del dispositivo stesso. L'elaborazione può essere sviluppata con
macchine che utilizzano segnali analogici o digitali. Oggi si utilizzano i
segnali digitali.

#### Segnali analogici vs. digitali

Il front-end analogico è molto importante in quanto molti sensori restituiscono
l'informazione spontaneamente in formato analogico. I sistemi digitali
considerano l'esistenza di componenti di tipo analogico per il front-end mentre
per quanto riguarda il resto del dispositivo si parla di tecnologia digitale.

La caratteristica dei segnali è diversa a seconda della natura. Alcune sono
migliori per uno scopo, altre per altri.

##### Segnali analogici

Attraverso l'ampiezza, essi rappresentano direttemente il valore di grandezze
fisiche che possono assumere tutti gli infiniti valori in un certo intervallo
finito (e.g. 0-5V). L'ampiezza di una tensione può rappresentare direttamente
l'ampiezza di una temperatura. I segnali analogici hanno una capacità di
rappresentazione molto elevata: con una sola grandezza fisica posso
rappresentare un numero infinito di possibili valori anche in un intervallo
finito. Quali valori si utilizzano per i segnali analogici? Si cerca di
standardizzare questi valori per esigenze di progettazione (potrei utilizzare
filtri in diverse applicazioni). Tipicamente i valori di segnali da 0 a 10 V,
da -5 a 5V. Alcuni standard utilizzano la corrente al posto della tensione:
0-20 mA o 4-20mA (chiamato anche zero vivo).
  
*Rappresentazione grafica del segnale*: Alcuni sensori riescono a fornire una
relazione lineare tra il valore della variabile misurata e l'uscita del
sensore. Questa è la relazione che si preferisce. Non sempre si può avere
questo tipo di relazione e quindi bisogna comprendere la forma di queste curve.
Durante la fase di calibrazione vado a registrare a quale grandezza fisica è
associata una grandezza elettrica.
  
*Zero vivo*: non includono il valore di zero della grandezza studiata per la
rappresentazione della stessa. L'intervallo 4-20mA non include lo zero perchè
consente così di identificare il guasto del sistema. Se qualcuno tagliasse il
cavo del sensore il sistema lo rileverebbe subito
  
**Aspetti positivi**:

- *Molti sensori generano spontaneamente segnali analogici* (e.g.  variazioni
  di potenziali). Quindi l'uso di tecnologie analogiche permette di elaborare
  direttamente il segnale del sensore.
- Esistono *dispositivi a funzionalità intrinseca semplici* per l'elaborazione
  del segnale (i.e. amplificatori operazionali).
- *Gli errori*, tipicamente, *sono limitati e proporzionali all'entità dei
  disturbi*. In condizioni operative normali abbiamo informazioni sul disturbo.
- È sufficiente *un solo conduttore* per trasmettere in tempi trascurabili gli
  infiniti valori numerici in un certo intervallo.
- La *ricchezza di informazioni* è altissima.
- Il *tempo di trasmissione* del segnale avviene *è molto ridotto*.
   
**Aspetti negativi**:

- Sono *poco precisi nel tempo e poco immuni alle perturbazioni*. Avendo un
  operazionale in configurazione non invertente, i valori delle resistenze che
  utilizzo sono dimensionate dal produttore con un certo valore nominale ed una
  certa tolleranza. Se definisco qual è il guadagno che voglio ottenere avrò,
  se replico il filtro, sempre diversi operazionali. Inoltre la resistenza
  cambia al variare della temperatura. Non posso essere estremamente preciso
  nella determinazione del guadagno.
- Le operazioni eseguibili da dispositivi analogici si riducono a *somme
  algebriche, trasformazioni logarimtiche e operazioni integro/differenziali*.
  Se volessi fare una FFT di un segnale il circuito sarebbe molto complesso.
- *Non è possibile realizzare memorie permanenti di segnali analogici*.  Il
  condensatore può essere caricato e il potenziale in teoria rimane costante
  finchè non viene sottratta o aggiunta della carica. Questo è l'elemento più
  efficace per memorizzare ma è impossibile far mantenere i propri valori
  infinitamente. Il movimento di elettroni anche minuscolo fa perdere
  precisione e l'informazione. Per leggere il condensatore devo collegare un
  circuito di lettura che andrà a sottrarre degli elettroni in
  quest'operazione, andando a impattare sull'ampiezza del segnale memorizzato.
  è un limite importante nelle applicazioni biomediche in quanto vogliamo avere
  uno storico delle variabili misurate.
- *Ogni elaborazione dei segnali analogici produce un degrado della
  precisione*.  Se genero un rumore questo altera il segnale, anche di
  pochissimo.  Quest'alterazione è dentro il segnale, che diventa un tutt'uno.
  Se faccio entrare questo segnale all'interno di molteplici blocchi funzionali
  dell'elaborazione analogica avrò una combinazione degli effetti su questo
  segnale che, di volta in volta, inducerà altri errori. Questo pone un *limite
  fisico all'utilizzo di un certo numero di passi (i.e. blocchi) funzionali*. A
  un certo punto avrò un segnale indistinguibile dal rumore che ho aggiunto.
  Per esempio vogliamo trasportare un segnale da Milano all'Australia. Le
  caratteristiche elettriche del filo, la resistenza in particolare, attenuerà
  il segnale nel momento in cui vogliamo far passare questo segnale. Questa è
  la ragione per cui è necessario effettuare un'amplificazione del segnale con
  molti stadi ogni decina di kilometri. Se ognuno di questi stadi introduce un
  piccolo errore, il piccolo errore che viene introdotto alla fine del primo
  tratto di trasmissione verrà aggiunto al segnale ed amplificato al secondo
  blocco di trasmissione e così via. Andando a studiare il SNR si
  comprenderebbe che crolla all'aumentare del numero degli stadi di
  amplificazione. Esiste dunque un limite fisico alla possibilità di inserire
  blocchi di elaborazione.  
  
##### Segnali digitali

Rappresentano l'informazione con simboli di un alfabeto finito con una
corrisponenza convenzionale tra tali simboli e i valori assunti dal segnale.
  
A ciascun simbolo associamo un intervallo di valori. Tendiamo a utilizzare due
simboli, lo zero e l'uno (aka LOW e HIGH). Un numero così limitato di simboli
non permette una gran risoluzione del segnale. Come faccio a rappresentare
valori vicini?  Si utilizza una codifica per poter rappresentare una serie di
valori diversi. Mettendo tanti simboli in fila posso avere una precisione
maggiore.
  
  $N=\text{trunc}(\log_{D}{S})+1$
  
- S: cardinalità dell'alfabeto da rappresentare (i.e. numero di valori finiti
  che si desiderano rappresentare).
- D: cardinalità dei digit (quanti diversi simboli sono rappresentabili in un
  digit).
- N: numero di digit richiesti per descrivere S.
  
Il dispositivo che utilizza segnali digitali funziona sempre con tensioni
analogiche ma che le utilizza per codificare segnali digitali.
  
**Segnali digitali binari e margine di rumore**

![Digitali binari.\label{fig:digitali_binari}][digitali_binari]
  
Il sistema binario è definito a partire da un alfabeto,
utilizzato per rappresentare grandezze, composto da due sole cifre (LOW, HIGH).
Questa rappresentazione viene normalmente ottenuta nel momento in cui
realizziamo circuiti elettronici per portare informazione.  Quest'informazione
è sempre codificata da una variabile di tipo analogico (i.e. potenziale
elettrico) infatti sarebbe improprio affermare che "zeri e uni viaggino". 
Le tensioni e/o le correnti, quindi segnali analogici, vengono trasmesse. Queste però non codificano direttamente un
valore che vogliamo rappresentare ma una cifra (i.e. una lettera dell'alfabeto
binario). Questo grafico rappresenta degli esempi di associazione tra valori
del segnale analogico (e.g. tensione) e HIGH o LOW (i.e. le due cifre che si vogliono
rappresentare).  Quando progettiamo il circuito di tipo digitale, vorremmo che
un range di possibili tensioni venisse associato al valore LOW e un range di
possibili tensioni venisse associato al valore HIGH. La codifica LOW viene
ottenuta quando la tensione che viene rapportata sul conduttore è inferiore ad
un certo valore di soglia. La stessa cosa accade quando introduciamo una tensione
per codificare HIGH. Quindi il grafico ci dice che tutto quello che è
rappresentato da massa a RiL (sulle ascisse) viene utilizzato per codificare
LOW.  Da RiH in poi (alimentazione positiva?) viene utilizzato per
rappresentare HIGH. Questi sono i range che sono utilizzati dal circuito
ricevente il segnale all'ingresso per interpretarlo. Se fornisco al circuito
ricevente un valore inferiore a RiL (i.e. massimo valore richiesto in ingresso
per far codificare LOW al ricevente). In maniera simmetrica, esiste
RiH (i.e. minimo valore di tensione che serve per codificare HIGH all'ingresso
del ricevente). Quando vengono definiti i valori caratterizzati con R (RiH e
RiL), si stabilisce che l'utilizzatore vuole in ingresso un segnale che
rispetti queste caratteristiche. Lo scopo dell'utilizzatore o ricevente è
quindi quella di interpretare il segnale in ingresso che viene fornito dal
generatore a monte.  Posso realizzare i due circuiti con caratteristiche
diverse e, quando si progettano sistemi di tipo digitale, bisogna fare in modo
che le specifiche che vengono utilizzate per il ricevitore siano diverse
rispetto alle specifiche del generatore. Le specifiche del generatore vengono
rappresentate dalle grandezze che iniziano per G (GuL e GuH). GuL è il valore
massimo garantito in uscita al generatore che codifica per LOW. Ciò significa che il sistema
generatore del segnale fa in modo che quando deve scrivere il valore LOW la
tensione in uscita sarà inferiore o uguale alla tensione GuL. La stessa cosa accade per
HIGH: il generatore darà in uscita un valore superiore o uguale a GuH. I due
range appena descritti sono inferiori in ampiezza rispetto a quelli che sono
definiti dal dispositivo utilizzatore.  Facendo ciò, è possibile realizzare un
dispositivo con un elevata immunità ai disturbi di tipo elettrico.
Considerando il caso LOW, se il generatore producesse una tensione compresa nel
suo normale intervallo e a questa venisse sommato un rumore che spostasse la
tensione tra GuL e RiL, l'utilizzatore continuerebbe a codificare LOW. Da ciò
deriva che se il segnale in ingresso all'utilizzatore si trova nell'intervallo
di codifica LOW dello stesso (compreso tra massa e RiL), questo codificherà
sempre per LOW. Ciò accade anche su HIGH, considerando gli intervalli
appropriati.  Il *margine di rumore* è la massima ampiezza che un rumore
sovrapposto al segnale analogico può avere senza comportare un'alterazione
dell'interpretazione del segnale stesso. Se il rumore sovrapposto al segnale ha
una massima ampiezza "prevista" inferiore a queste ampiezze non vi saranno
problemi di codifica. Questo è un meccanismo che consente di utilizzare un
vettore intrinsecamente esposto a rumore (i.e.  segnale analogico) per
veicolare un'informazione di tipo digitale eliminando l'effetto del rumore
stesso. Si sta, di fatto, eliminando l'errore analogico dalla trasmissione
digitale del nostro segnale.  Operativamente, i margini di rumore sono le distanze tra le
grandezze G e le loro rispettive R, in relazione alla codifica (i.e. RiL-GuL e
GuH-RiH). In questo modo, siccome il segnale viene rigenerato ad ogni
passaggio, si possono avere un numero virtualmente illimitato di passi di
elaborazione, senza che questi modifichino il risultato delle elaborazioni.
  
Il margine di rumore è il concetto più importante per la distinzione delle
caratteristiche dei segnali digitali rispetto a quelli analogici. Il fatto di
aver associato un certo intervallo di tensione ad un particolare simbolo,
consente di realizzare dei collegamenti tra circuiti che consentono di
eliminare quel valore di rumore analogico che comunque si somma al segnale
(infatti nei circuiti digitali si utilizzano delle tensioni, quindi vettori
analogici per trasferire l'informazione HIGH/LOW). La codifica
dell'informazione non è più un'associazione tra valore di tensione e valore
rappresentato ma tra range di valori di tensione e un simbolo (HIGH o LOW).
Questi simboli, in sequenza, vengono utilizzati per costruire un numero.

Riprendendo l'esempio del cavo da Milano all'Australia, se avessi stadi di
amplificazione che utilizzassero questa logica di trasmissione, è facile
intuire che ad ogni stadio di amplificazione il rumore analogico verrebbe
automaticamente cancellato dall'utilizzatore, quindi allo stadio successivo il
segnale verrebbe completamente rigenerato. È dunque possibile applicare un
numero di stadi di amplificazione (e in generale di elaborazione) virtualmente
infinito.
  
**Aspetti positivi**:

- *Rappresentazione con esattezza e senza incertezza simboli di un alfabeto
  finito*.
- L*'aumento di precisione (cardinalità dei diversi simboli che voglio
  rappresentare) ha costo (cardinalità dei digit) crescente con il logaritmo*.
  All'inizio la modifica è piccola aggiungendo alcuni bit. Aggiungendo molti
  bit i valori che riesco a codificare diventano moltissimi anche solo
  aggiungendone uno (si pensi al passaggio da 9 a 10 bit: da 512 a 1024
  valori). Per ogni bit raddoppio l'alfabeto codificato.  Raddoppio la
  codifica.
- *Semplicità e robustezza dei circuiti di generazione e riconoscimento* dei
  segnali.
- Elevata *immunità ai disturbi*.
- Semplicità dei sistemi di memorizzazione. Flip flop: una volta alimentato
  memorizza il bit a tempo indefinito.
- Semplicità circuiti per elaborazioni semplici. i.e. porte and, or.
- Avendo la possibilità di rigenerare il segnale è possibile fare facilmente
  FFT e altre elaborazioni a partire da elementi di semplice elaborazione.
  Questi blocchi, pur essendo molto più semplici nella logica rispetto a quelli
  analogici possono compiere elaborazioni complesse dato che è possibile
  mettere insieme una quantità illimitata nello spazio e nel tempo dato che
  questi passaggi non portano una degradazione del segnale. 
  
**Aspetti negativi**:

- *Gli errori possono modificare bit meno significativi e più significativi
  indifferentemente*. Se succede l'impatto è potenzialmente devastante e può
  alterare in maniera significativa il tutto. Quindi è imprevedibile l'effetto
  che si può generare nel caso di errore e bisogna considerare delle misure di
  controllo adeguate. Il guasto può quindi avvenire, con la stessa probabilità,
  a livello delle cifre più significative e meno significative. L'entità
  dell'errore non è più proporzionato all'entità del disturbo, come avveniva
  per il segnale analogico.
- L'interpretazione dei segnali necessita una decodifica, di modo da essere
  interpretabile il segnale digitale. il tachimetro di un'automobile è
  maggiormente interpretabile con un colpo d'occhio ad una lancetta piuttosto
  che da un numero che la descrive.
- Occorrono tanti bit per rappresentare informazioni ricche. 10 bit = 10 fili
  oppure 1 filo ma con più tempo per la trasmissione.
- Tanti requisiti temporali. Se ho 16 fili voglio sapere quando tutti i 16 fili
  sono pronti.
- Bisogna convertire il segnale proveniente da un sensore da analogico a
  digitale per poter poi procedere. (affrontando tutti gli errori).  Nella
  maggior parte dei casi è necessario introdurre ADC per poter utilizzare
  circuiti di elaborazione che utilizzano tecnologia digitale.

**Le memorie digitali, il caso delle RAM dinamiche**

Il segnale digitale non è altro che un'astrazione: un modo alternativo di
interpretare la tensione, che invece costituisce un segnale analogico. Se non
posso memorizzare una tensione come è possibile realizzare una memoria digitale
a tempo indefinito? L'approccio più semplice è quello delle RAM dinamiche.
Queste sono utilizzate in qualunque computer, dispositivo di elaborazione dati.
Questi sono dei condensatori. L'utilizzo che se ne fa è particolare, in quanto
circuiti prima attribuiscono ai circuiti una certa tensione (ad esempio per
codificare HIGH, 5V). Questo condensatore, per la condizione reale del
componente, perderà della carica e quindi anche tensione scenderà con un
andamento esponenziale. Avendo una resistenza grande la caduta di potenziale
avverrà molto lentamente ma posso calcolare la costante di tempo. Le RAM
dinamiche, per evitare che valori HIGH vengano letti LOW periodicamente vanno a
leggere la tensione del condensatore e se questa è alta riscrivono il valore
5V, ristabilendo HIGH. Le RAM, in definitiva si comportano da dispositivi
utilizzatori del segnale, considerando il diagramma visto precedentemente.
Quindi è possibile per il progettista di RAM dimensionare dei tempi di
sicurezza in cui la diminuzione del potenziale ai capi dei condensatori si
trovi ancora all'interno del range per cui la RAM dinamica codifichi per HIGH.

## Macchina a funzionalità intrinseca vs. macchina a funzionalità programmata

![Modelli a black box della macchina a funzionalità intrinseca e programmata.\label{fig:intrinseca_vs_programmata}][intrinseca_vs_programmata]

- *Funzionalità intrinseca*: il funzionamento dipende dal modo in cui il
  dispositivo è stato costruito. Una volta configurato, il dispositivo rimane
  finchè non lo cambio fisicamente. Un esempio: costruisco una porta AND
  utilizzando dei transistor e ottengo così una macchina a funzionalità
  intrinseca, stessa cosa se costruissi un amplificatore operazionale in
  configurazione non invertente.
- *Funzionalità programmata*: Un sottogruppo di dispositivi digitali. La
  funzionalità intrinseca di queste macchine è quella di eseguire un programma.
  Ciò significa compiere una serie di istruzioni diverse la cui sequenza è
  determinata da un programma. Naturalmente le istruzioni devono essere
  immagazzinate in dispositivi di archiviazioni per rendere effettivo il
  funzionamento. Le istruzioni permettono alla macchina di implementare delle
  funzioni che sono diverse da quelle per cui la macchina è stata costruita.
  L'insieme della macchina a funzionalità intrinseca  e del programma che è
  stato implementato costituisce un oggetto con funzioni diverse da quelle
  della stessa macchina a funzionalità intrinseca. Dal punto di vista
  funzionale sono distinguibili da quelle a funzionalità intrinseca dal
  diagramma riportato. La funzione dell'oggetto dipende quindi dal suo
  hardware, che rimane lo stesso e ha lo scopo di eseguire programmi, e dal
  software (i.e. firmware, sequenza di istruzioni) che fa in modo che
  complessivamente sviluppino una funzione nuova.

## Vantaggi dell'elaborazione digitale

Il tipo di approccio a funzionalità programmata possiede dei notevoli vantaggi,
che sono qui presentati.

- *Programmabilità*: realizzare un dispositivo elettronico costa fatica e
  tempo. ogni volta che voglio fare semplici modifiche, questi richiedono, dal
  punto di vista industriale, dei cicli di lavorazione, costi e tempi molto
  elevati. Se volessimo realizzare un amplificatore per un sensore e, ad un
  certo punto è necessario cambiare il guadagno, fisicamente dovremmo prendere
  e smontare le resistenze per montare quelle nuove, se si trattasse di un
  dispositivo a funzionalità intrinseca. Avessi 100k dispositivi dovrei
  effettuare la stessa operazione per tutti questi circuiti, operazione che
  richiede moltissimo tempo. Qualsiasi modifica implicherebbe un'iterazione di
  100k volte. Nel mondo biomedicale, la certificazione dei dispositivi è
  inoltre molto complessa e costosa. Cambiando un componente non solo è
  necessario disegnare nuovamente lo schematico della PCB, cambiare i programmi
  dei sistemi di montaggio di tale board, allineare le liste d'acquisto di
  approvvigionamento materiali delle linee di produzione ma anche effettuare la
  certificazione ed essere adeguata ai requisiti di di sicurezza elettrica.
  Tutto ciò per cambiare marginalmente la funzione di un dispositivo, per avere
  impatti molto importanti sulla funzionalità di esso. Se questa funzione è
  eseguita da un programma, questo sarebbe vantaggioso perchè basterebbe
  cambiare delle righe di codice. Dal giorno dopo sono in grado di installare
  nei nuovi dispositivi la suddetta funzione senza cambiare la scheda. I nuovi
  dispositivi avranno questa funzione implementata nativamente e i dispositivi
  vecchi potranno essere aggiornati alla nuova versione. L'operazione appena
  descritta ha dei costi bassissimi e molto comoda da effettuare. L'impatto che
  ha un approccio di questo tipo alla tecnologia è enorme e ha determinato un
  utilizzo a volte sproporzionato di tecnologie a semiconduttori (i.e.
  microprocessori) per realizzare certi tipi di funzioni. Si pensi alle luci
  delle biciclette che lampeggiano in diverse modalità. è semplicissimo
  realizzare il circuito per far lampeggiare queste luci ma, utilizzando la
  normale componentistica e realizzando una macchina a funzionalità intrinseca,
  la spesa sarebbe superiore di quella che considera la logica a funzionalità
  programmata. Sistemi embedded: all'interno di un singolo dispositivo è stato
  messo un computer e un programma che all'esterno non appare come calcolatore
  (i.e. non possono avere più funzioni diverse). Il processore che includono è
  general purpose, che può essere coinvolto in diverse applicazioni, che però è
  stato inserito (da cui "embedded") nel dispositivo e non è più visibile come
  tale dall'esterno. La competenza del singolo sta su questi due aspetti: 

	1. Il software (SW): l'algoritmo e la sua implementazione. Si scrive
	   software di basso livello, chiamato così perchè è più vicina alla
	   macchina. Questo firmware modifica i contenuti dei registri della
	   macchina stessa, le periferiche etc. 
	2. L'Hardware (HW): La macchina che esegue questo algoritmo. È
	   necessaria una piena conoscenza di come questi sistemi elettronici,
	   microprocessori inclusi, funzionano per poter fare in modo che il
	   software modifichi correttamente il funzionamento della scheda
	   elettronica che è stata realizzata.

- *Riproducibilità dei circuiti*: Problema molto importante. Quando si
  considera l'amplificazione analogica, non è possibile avere delle resistenze
  esattamente uguali da mettere in due amplificatori separati. Da ciò deriva
  che le performance di due amplificatori differiranno sempre, anche se in
  termini non significativi. Se le variazioni sono limitate e rimangono
  all'interno delle specifiche del sistema, ciò non costituisce un problema. Se
  le alterazioni sono invece consistenti, prevalentemente quelle relative alla
  fase iniziale nella catena dell'acquisizione del segnale (che quindi sono
  soggette a tutte le elaborazioni negli stadi successivi, vedendo amplificato
  il loro impatto nel risultato finale), si ricorre al metodo utilizzato nei
  vecchi televisori a tubo catodico. Sulle PCB sono presenti dei piedini con
  etichetta "TPx", dove x rappresenta un certo numero. A questo punto per fare
  in modo che tutti i prodotti avessero le stesse caratteristiche all'interno
  delle specifiche era necessario effettuare una calibrazione del sistema prima
  della sua messa in commercio. Questa era possibile mediante delle resistenze
  variabili (aka trimmer). La regolazione mediante trimmer serviva per
  compensare gli effetti dovuti alle tolleranze dei vari dispositivi
  utilizzati. Alla fine della produzione vi era il reparto di calibrazione che
  prendeva segnale da TP, visualizzavano il valore in uscita quando erano stati
  iniettati dei segnali noti in ingresso e andavano a variare i trimmer e i
  componenti modificabili per compensare le tolleranze. Il costo è importante
  ed è di difficile riproducibilità. Per quanto riguarda la logica a
  funzionalità programmata, se considerassimo algoritmi implementati a livello
  di microprocessore, l'amplificazione viene identificata come il prodotto tra
  un segnale ed un guadagno. L'operazione matematica viene effettuata da una
  macchina che implementa una codifica numerica digitale e avrà sempre lo
  stesso risultato su qualunque macchina esegua tale sistema.
- *Ripetibilità*: L'elaborazione effettuata a livello software non è affetta da
  quelle variazioni che i dispositivi di tipo elettrico possono subire nel
  tempo e/o al variare di altri parametri. Non esiste solo il problema delle
  tolleranze delle resistenze ma bisogna considerare anche la variazione di
  queste come conseguenza della variazione della temperatura e/o del tempo di
  vita.

## Condizionamento (Front-end analogico)

È necessario standardizzare i blocchi di elaborazioni di modo che che possano
servire a molteplici applicazioni. Il blocco di elaborazione, nel nostro caso,
sarà un microprocessore con annesse le sue periferiche. Siccome si dovranno
trattare dei segnali analogici sarà indispensabile possedere un convertitore
analogico-digitale (ADC). Tutte queste periferiche vengono realizzatein molte
copie a bassissimo costo grazie alla tecnologia della realizzazione dei
microchip. È possibile integrare in un unico chip di silicio moltissimi
componenti e produrli a basso costo se sono tutti uguali. Poi in base alla
funzione che voglio implementare saranno attivati determinati componenti.

Il front-end è l'unica che viene ancora realizzata con approcci dal punto di
vista analogico. Si vedranno ora dei classici richiami di elettronica. Ci si
focalizzerà quando quei circuiti singoli vengono collegati uno all'altro.
esempio un'operazionale ad un RC passivo. Cosa accade? basta unire le funzioni
di trasferimento o questi due elementi interagiscono tra di loro?

- Adattamento d'impedenza.
- Amplificazione: modificare l'ampiezza di un segnale
- Filtraggio: alterazione del contenuto in frequenza del segnale
  (attenuazione/amplificazione di determinate componenti).
- *Isolamento*: molto importante perchè spesso i segnali sono di tipo elettrico
  (il sensore potrebbe essere a contatto col paziente). I sensori operano in un
  contesto particolarmente difficile da operare. Un malfunzionamento potrebbe
  esporre a tensioni troppo elevate il paziente e/o l'operatore. Dobbiamo
  essere estremamente cauti nell'evitare che possibili guasti possano essere un
  rischio per il paziente e/o per gli operatori. 
- Campionamento: opzionale e caratterizza i blocchi funzionali con tecnologia
  digitale.

### Adattamento d'impedenza

![Il modello dell'adattamento d'impedenza.\label{fig:adattamento}][adattamento]

La logica del sistema di misura ha come origine quella di dover comunicare
un'informazione tra due sistemi, il generatore e l'utilizzatore del segnale.
Questi due elementi sono tra loro collegati.  Quando questi circuiti vengono
collegati, è impossibile trasferire informazione senza trasferire dell'energia
e, con essa, elettroni. Questi elettroni alterano sia le condizioni operative
del generatore del segnale che quelle dei circuiti che lo ricevono. Questo
problema lo si vede modellizzato (i.e. si considerano caratteristiche
essenziali e semplici che descrivono il fenomeno) col circuito a sinistra. Vin
è l'interfaccia tra ciò che sta a monte (a sinistra, il generatore) e ciò che
sta a valle (a destra, il ricevitore). Il ricevitore viene modellizzato come un
carico, un'impedenza. Questo perchè qualunque sistema che prevede una tensione
in ingresso è costituito da almeno un dipolo: un collegamento verso massa (i.e.
per la tensione di riferimento) e un'ingresso. Da questo ingresso posso vedere
un'impedenza verso massa.  Non si parla di resistenza ma d'impedenza perchè i
sistemi possono funzionare anche in regime alternato (pur valendo le stesse
leggi fisiche).  La Z non viola la generalità del problema: qualunque rete è
rappresentabile con un'impedenza d'ingresso spegnendo tutti i generatori e
calcolandola. Zgen e Vgen non sono altro che l'equivalente di Thevenin del
generatore. La rete complessa può essere tradotta in questo equivalente.  La
parte a sinistra rappresenta quindi il circuito del generatore senza ledere la
generalità di esso.  Avendo questi due moduli, è possibile comprendere cosa
accade se si fa passare la corrente. 

A destra della figura è rappresentato un esempio pratico di un possibile
sistema di misura. Scollegando l'utilizzatore la tensione ai due capi del
dipolo (Vin) sarà esattamente uguale a Vgen. Collegando Zmis, si verifica la
produzione di corrente che attraversa Zmis e anche Zgen. Zgen produce una
tensione proporzionale alla corrente che scorre in essa. Il circuito indica
implicitamente l'importanza di Zmis. *Se Zmis è enorme, la corrente che scorre
nel circuito sarà piccola, quindi minore sarà l'effetto della Zgen*. Quindi è
necessario che il sistema che produce l'informazione abbia in uscita Zgen
piccola (i.e. la caduta di potenziale su Zgen è proporzionale sia alla corrente
che vi scorre che al valore stesso di Zgen) e un sistema ricevitore con Zmis
grandissima. Quando progettiamo il blocco isolato che dev'essere inserito nella
catena di misura bisogna ragionare di modo da avere un'impendenza d'ingresso
molto grande e un'impedenza d'uscita molto bassa rispetto a quella d'ingresso
del blocco successivo. Come la si valuta numericamente?  Guarda la formula
sotto.  Il rapporto tra Vin e Vgen è idealmente pari a 1.  Questo ci dice
quindi che il rapporto tra le impedenze dev'essere 0.

A destra è mostrato un esempio applicativo: uno strumento di misura (la cui
resistenza è Rm) si trova in in parallelo ad un potenziometro. Se lo strumento
di misura ha una Rm molto bassa in relazione alla resistenza del ramo inferiore
del potenziometro (i.e. lo strumento ha un'impedenza maldimensionata) la curva
in uscita non sarà una retta ma risulterà altamente non lineare, come mostrato
in figura.

### Amplificazione

Rivediamo:

- Non invertente.
- Invertente.
- Differenziale.

Quando dobbiamo progettare un amplificatore dobbiamo definire qual è il
guadagno e sappiamo che un amplificatore reale deve avere anche una banda
passante del segnale. Il comportamento sarà poi diverso a seconda della banda
del segnale su cui vogliamo operare. Vogliamo anche sapere quanti ingressi
vogliamo avere (i.e. differenziale oppure ingresso singolo riferito a massa).
Bisogna considerare anche l'impedenze d'ingresso e uscita. Le altre sono
caratteristiche tipicamente non lineari che caratterizzano le differenze del
dispositivo reale da quello ideale lineare.

Si parte da un riferimento di opamp ideale.

1. Uscita esclusivamente funzione degli ingressi. (non vi è rumore)
2. Guadagno in anello aperto è reale (i.e. non si introducono sfasamenti) e
   infinito.
3. Impedenze d'ingresso infinite e in uscita nulla. Una corrente non entra
   nell'operazionale e la tensione in uscita è indipendente dal carico.

Regole in anello chiuso:

1. I due morsetti hanno la stessa tensione.
2. Le correnti ai morsetti d'ingresso sono entrambe nulle.

#### Amplificatore invertente

L'uscita ha segno opposto rispetto a quella d'ingresso. Se si utilizzasse un
ingresso positivo rispetto a massa il segnale in uscita è negativo. Significa
che è necessaria un'alimentazione duale (sia positiva che negativa). Quanto
vale l'impedenza d'ingresso e quella d'uscita. L'impedenza d'uscita è nulla
nella versione ideale. In ingresso dovremmo avere impedenza infinita per
definizione. Ma noi abbiamo una massa virtuale e quindi Vi vede qualcosa di
diverso dall'operazionale. L'impedenza d'ingresso è proprio Ri. La
configurazione circuitale altera proprio le caratteristiche. Se Ri è troppo
bassa altero il passaggio d'informazione tra quello che sta a monte e quello
che sta a valle. Non posso mettere un valore nemmeno troppo grandi perchè delle
correnti indotte potrebbero generare effetti indesiderati.

#### Amplificatore non invertente

La tensione viene applicata al morsetto non invertente. Questo sistema è in
anello chiuso. La retroazione va sempre sul meno perchè altrimenti
l'amplificatore "esplode" e non funziona correttamente.

Cos'accade al guadagno? La tensione in uscita ha stesso segno rispetto a quella
in ingresso. Il guadagno è "1+" qualcosa quindi il segnale può solo amplificare
e non ridurre l'ampiezza di quello d'ingresso. Se il prof ci dicesse di ridurre
la tensione. Sono obbligato a usare il non invertente per scalare il segnale
d'ingresso. Un altro modo per scalare sarebbe un partitore di tensione. Se uso
un partitore di tensione e a valle devo fare un'altra operazione devo tenere
conto l'adattamento di impedenza. Se metto a valle un invertente ho impedenza
in ingresso infinita quindi sono a posto.

- Impedenza in ingresso infinita
- Impedenza in uscita: zero

Cos'è il buffer? Se avessi Rf=Ri=0 imporrei guadagno pari a 1. Questo non
cambia il valore di tensione in uscita. Che senso ha utilizzarlo? Tornando
all'adattamento d'impedenza potrei risolvere un problema gigantesco. Se
mettessi il buffer dopo il partitore (o trimmer) avrei un accoppiamento molto
bello e eviterei gli errori visti precedentemente. Se tra trimmer e strumento
di misura avessi messo un buffer avrei ottenuto la curva lineare e non quella
che più si discosta.

Quindi per scalare potrei usare partitore e buffer.

#### Amplificatore differenziale

Lo sfruttiamo per INA (INstrumentation Amplifier).


#### Altri amplificatori 

Per risparmiare risorse è possibile utilizzare una configurazione a due opamp,
la b di pagina 12, slide 2.


### Filtraggio

Un sistema che lascia inalterate alcune componenti spettrali e che attenua
fortemente le altre.

- Passabasso: guadagno unitario in frequenze basse e guadagno zero per quelle
  che stanno sopra la frequenza di taglio.
- Passaalto: guadagno unitario in frequenze alte e zero per quelle che stanno
  al di sotto della frequenza di taglio.
- Passabanda
- Arrestabanda. Hanno utilizzi interessanti. La rete elettrica ha 50Hz in
  Italia e 60Hz in USA. Si utilizzano filtri specifici per arrestare il rumore
  elettromagnetico ambientale attorno a quella frequenza.

Filtri reali. Ci sono molti parametri da dover considerare per progettarlo
correttamente. Ci sono molti aspetti del filtro stesso da dover considerare.

#### Passa-basso

Le funzioni di trasferimento sembrano esssere equivalenti. Se non ho bisogno di
metterci guadagno perchè non uso una RC. Va bene solo se protetta da sistemi di
disaccoppiamento del impedenza. La strategia attiva/passiva è dettata
dall'accoppiamento dell'impedenza perchè determina quali blocchi è opportuno
usare oppure no.

- Passivo
- Attivo

Esistono configurazioni invertenti e non invertenti per i vari filtri.

#### Approssimazioni

Per effettuare filtri maggiori al secondo ordine si fanno delle scelte:

- Butterworth: approccio più comune.
- Chebyshev: massimizzare la pendenza, riduzione della banda di trasmissione.
  Il guadagno tende a essere più variabile per questo filtro.
- Bessel: cercare di conservare la morfologia del segnale ottimizzando la
  risposta all'impulso.

Questa scelta ha dei pro e dei contro per ciascuno.  è giusto per capire che
esistono diversi approcci ma non ci dilunghiamo su questi.

Si cercano circuiti che introducono poli e zeri in una posizione determinata.
Con il filtro RC si ottengono queste relazioni. Quando si calcolano queste
equazioni l'ipotesi è che non ci siano correnti che escono dal circuito ma
questo non è vero perchè se collego un carico a valle avremo una variazione
della caratteristica Vo/Vin. Se a valle ho impedenza infinita e uno a monte con
impedenza nulla avremmo che le equazioni qui descritte sono effettivamente
descrittive del comportamento del filtro.

Altre configurazioni di filtro prevedono l'utilizzo di amplificatore invertente
e a seconda del filtro che si vuole realizzare si posiziona il condensatore in
certi modi per ottenere amplificatore e filtraggio.

Filtro integratore: nella linea di retroazione ha una sola capacità.

La configurazione non invertente è altresì possibile per i filtri che abbiamo
considerato prima. Sono più semplici da alimentare.

Si trovano nelle slide anche delle configurazioni di passa-banda (combinazione
tra passa-alto e passa-basso).

Vi sono altre configurazioni (HALF SELECTION etc) che utilizzano componenti
diversi alfamoso RC. Qui si considerano altri elementi per fornire delle simili
funzioni di trasferimento. Se volessimo fare un filtro per il controllo di
attuatori o per togliere correnti molto elevate (per alimentare grossi motori)
si possono cercare filtri che non utilizzano RC ma che utilizzano LC. Casi
molto particolari e non di nostro interesse.

Per quanto riguarda le configurazioni classiche per ordini sopra il primo
mettiamo in cascata più filtri. esistono configurazioni circuitali che solo
grazie ad un operaizonale ci permettono di fare un filtro di secondo ordine.
Non sono rare queste configurazioni ma non è indispensabile sapere le funzioni
di trasferimento. Esistono due configurazioni: sallen-key con caratteristiche
articolate (settabili mediante tool di progettazione il problema è relativo
all'ottimizzazione in relazione ai valori commerciali di resistenza e
capacità). esiste sallen-key per passa-alto. Multiple feedback (low-pass e
high-pass) è la seconda configurazione. Non da imparare a memoria ma da sapere
che esistono.

Viene mostrato un software dedicato chiamato filterlab.

Le ultime slides sono di natura tecnica (quelle riferite alla lezione 3).
Dobbiamo sempre riferirci a componenti ideali ma in realtà andiamo a comprare
componenti che si comporti in maniera più simile al modello che stiamo
utilizzando. Quando compriamo un componente questo approssima solamente
l'equazione che abbiamo usato per descriverlo e in molti casi
quest'approssimazione è sufficiente ma se andiamo su frequenze elevate o tempi
di carica/scarica lunghi le non idealità si manifestano e se voglio realizzare
dispositivi che lavorano in questi regimi devo utilizzare modelli diversi che
meglio descrivono il comportamento di questi in quel regime. Il condensatore
ideale è rappresentabile come nella slide ma in realtà nel dispositivo reale
esistono delle correnti di scarica e se lo voglio utilizzare in periodi di
carica/scarica molto lunghi devo modellizzarlo (aggiungo resistenza in
parallelo). Ad alte frequenze le cariche faranno fatica a spostarsi e quindi in
serie alla capacità avrò un induttore.

Viene descritto come si comporta un dielettrico. Questo determina il
comportamento del dispositivo.

Tutti i vari fenomeni vengono integrati dal most general model of a real
capacitor.

Il grafico sopra mostra che tanti fattori influenzano il valore della capacità
di un condensatore fisico. C varia con la temperatura ma anche in funzione
dell'invecchiamento.

Vengono mostrate le varie famiglie di condensatore divise per dielettrico etc.
Il dielettrico cambia la capacità mantenendo dimensioni piccole e garantisce
isolamento in determinate condizioni.

### Isolamento

Isolamento tra il circuito che mettiamo a monte e quello che mettiamo a valle.
Questa è una caratteristica che nasce per proteggere il paziente da potenziali
esposizioni di correnti e tensioni pericolose.

- Isolamento: se riferiti all'isolamento galvanico finalizzata a proteggere il
  paziente.
- Circuiti di protezione: protezione del dispositivo. Aspetto importante perchè
  questi devono essere molto robusti e funzionare nella maggior parte dei casi.
  Vogliamo che il dispositivo funzioni correttamente così da non essere
  pericolosi per il paziente.

Quando parliamo di isolamento dobbiamo tenere conto di eventi catastrofici che
possono accadere. Devo avere sistemi che devono funzionare anche in caso di
questi eventi catastrofici. Vogliamo un punto che non ci sia nemmeno un filo
che mette in comunicazione ciò che sta a monte con quello che sta a valle. Come
posso trasmettere il segnale oltre la barriera? Dobbiamo trovare un modo per
trasferire il segnale. Questo modo viene fatto tramite due soluzioni:

- Circuito ottico.
- Circuito magnetico.

#### Isolamento tramite optoisolatore

Come funziona il dispositivo ottico?  A sinistra nel diagramma abbiamo il
soggetto con variabili fisiche, il trasduttore che trasforma le variabili in
elettriche e poi questo circuito qui. Non si ha un collegamento implicito o
esplicito con l'elemento di destra. Esplicito o implicito: quando ho dei
circuiti le masse vengono tracciate con elementi indipendenti. è comunque
implicita l'esistenza di un conduttore che collega le masse tra di loro. Questo
circuito non ha nemmeno una massa in comune. Si usa la banda ottica per
trasmettere il segnale. Un diodo led illumina attraverso una barriera isolante
ma trasparente. Dall'altra parte vi è un fotodiodo che trasforma l'informazione
in elettrica e poi avviene una traduzione dell'informazione. Il LED illumina
sia due fotodiodi identici. Se riusciamo a fare questa cosa nel momento della
produzione della radiazione potremmo garantire il passaggio di una corrente
inversa uguale dei due FD. ???

Esistono due diverse famiglie di optoisolatori:

1. Passaggio di informazioni di tipo analogico. La tensione d'ingresso e quella
   d'uscita sono legate da una funzione di trasferimento.
2. Passaggio d'informazione di tipo digitale. è più facile perchè il segnale
   digitale deve codificare per due diversi simboli. è più semplice andare a
   capire se lo stato è HIGH o LOW rispetto al comprendere l'intensità
   luminosa. Se la luce è sopra soglia il LED è acceso, altrimenti spento.

#### Isolamento tramite trasformatore

???

Utilizzare modulatore. Uso la modulazione di un segnale. Il trasformatore fa da
passa-alto. Usiamo il segnale per modulare un secondo avente ampiezza costante.
La modulazione di ampiezza considera un segnale avente frequenza costante e una
variazione dell'ampiezza proporzionale al segnale in ingresso al modulatore. Il
segnale che vogliamo trasmettere ha diverse componenti. Questo è utilizzato per
determinare la caratteristica di un secondo segnale denominato portante. Il
segnale oscilla a 10kHz. L'ampiezza di questo segnale è determinato
dall'ampiezza del segnale in ingresso (modulaizone in ampiezza). Ci sono alcuni
inconvenienti:

- l'ampiezza è determinata dalla caratteristica del viaggio che deve fare tra
  sorgente e destinazione. L'ampiezza del segnale che ricevo varia per via
  della presenza o modifica del percorso tra l'antenna ricevente e quella che
  emette. Le alterazioni del percorso che attenuano il segnale alterano il
  contenuto informativo.

Come alternativa si utilizza la modulazione di frequenza. La portante di 10kHz
non sarà costante a quella frequenza ma sarà modulata variando questa frequenza
del segnale stesso. Posso ascoltare la musica in macchina anche se il segnale
può variare in ampiezza.

Ritornando alla modulaizone in ampiezza possiamo riottenere il segnale facendo
l'inviluppo del segnale: unire tutti i puntini dei picchi. Come fare a
ritornare al segnale in ampiezza qua sopra? uso un passa basso ma così facendo
elimino la portante e l'informaizone che contiene. Il passabasso lo uso ma a
valle di un diodo. In questo modo si ottiene il segnale leggermente
squadrettato che verrà poi reso più smooth con opportuni passa-basso.

Il demodulatore avrà un circuito che fa l'inviluppo. Il blocco di sopra quindi
serve per trasmettere il segnale. Serve per ritornare al segnale modulante del
sistema di sinistra.

Anche l'alimentazione è necessaria. I circuiti vicino al paziente deve essere
veicolata. Uso un trasformatore. Ho un'alimentazione in continua che entra in
un oscillatore e a vicino al paziente ho un ponte raddrizzatore per portare al
paziente l'alimentazione. Così garantisco un isolamento galvanico tra una parte
di circuito a diretto contatto col paziente e un'altra parte di circuito
lontano.

### Campionamento

Se le tecnologie che abbiamo a disposizione a valle sono digitali ho bisogno di
questo blocco funzionali. Questo accade quando abbiamo dei controllori. 

Il campionamento caratterizza strumenti di tipo digitale. è costituito da vari
elementi che consentono di trasformare il segnale analogico in digitale.

Vi sono du fasi:

1. Campionamento spaziale: si decide la sorgente da campionare. Questa fase è
   svolta dal multiplexer (MUX).
2. Campionamento temporale: si decide in quali momenti convertire il segnale da
   A a D. Questa fase è svolta dal campionatore (S&H).

### Multiplexer (MUX), campionamento spaziale

Si vuole ottenere segnali da una pluralità di risorse. Nella gran parte dei
casi il contenuto in frequenza non è particolarmente elevato e per risparmiare
nello sviluppo delle tecnologie di campionamento possiamo utilizzare un solo
campionatore che viene collegato a diversi ingressi. Così abbiamo una riduzione
dei costi: invece di avere 10 ADC ho 1 ADC e un multiplexer che stabilisce
quale delle linee vengono collegate a questo. Questo accade se la velocità
della commutazione è più alta del contenuto in frequenza dei segnali in
ingresso. Se ho 5 canali da campionare la frequenza del campionatore deve
effettuare 5 campionamenti per ogni deltat. Centinaia di migliaia di operazioni
al secondo sono possibili con ADC odierni. Studieremo come funzionano circuiti
di questo tipo. Il multiplexer è quell'interruttore selettore che permette di
utilizzare 1 adc per un unico segnale in ingresso. il multiplexer mette in
collegamento diverse linee in ingresso, un selettore e una linea in uscita. Se
volessi campionare un ingresso diverso dovrò spostare la leva di conseguenza.
Il ruolo del multiplexer è quello di selezionare l'uscita. Ci sono vari
parametri del mux:

- numero di canali
- velocità di commutazioni
- numero di commutazioni (vita)
- resistenza a circuito chiuso e a circuito aperto. virtualmente vorremmo che a
  cc sia 0 e a ca sia infinita ma questo non è possibile in dispositivi reali.

A valle del mux vi è il campionatore che va a prendere e congelare il valore di
tensione che il segnale ha in un certo tempo. Viene chiamato S&H. vi sono 3
terminali: due analogici e uno digitale. Quello digitale stabilisce se il S&H è
in campionamento o mantenimento.

### Campionatore (Sample and Hold, S&H), campionamento temporale

Il grafico a mostra la variazione del segnale analogico e del segnale in uscita
al sample and hold.

Il grafico b mostra invece il segnale sul pin digitale. In modalità Sample (S)
abbiamo che ingresso e uscita coincidono. Ciò significa che l'ingresso viene
riportato fedelmente in uscita. In modalità Hold (H), il segnale in uscita
smette di riprodurre il segnale in ingresso e si mantiene costante all'ultimo
valore registrato nella fase S precedente. Questa svolge una funzione di
memoria analogica. Sappiamo che questa memoria non può funzionare a tempo
indeterminato ma ha un certo periodo di tempo in cui si deve ottenere la
misura.

Dal punto di vista fisico e reale vi sono dei tempi di risposta non nulli ma
devo aspettare il tempo che è proporzionale alla differenza dei due segnali. Il
tempo è maggiore rispetto a quello di apertura ...

Esistono diverse realizzazioni circuitali che insegnano come funziona.

FIGURA

1. Se il dispositivo è in S: ho un buffer collegato al condensatore. Quando
   sono in H non ho più retroazione sul buffer. L'impedenza vista dal
   condensatore è infinita (i.e. quella del morsetto negativo dell'opamp) e
   l'unica cosa che determina il potenziale è la tensione sul condensatore. La
   tensione idealmente rimane all'infinito.  L'inconveniente di questo circuito
   è quello legato alla costante di tempo.  Prendo il datasheet
   dell'operazionale, considero le resistenze di leakage del condensatore e
   però *dobbiamo considerare anche il carico attaccato a V2*.  L'impedenza di
   carico sarà in parallelo rispetto alla Rleakage e alla Zamp. L'impedenza
   vista ai capi del condensatore sarà data quindi dal parallelo dei tre
   componenti.
2. Per questo motivo si utilizza questa configurazione circuitale. Abbiamo
   standardizzato cosa sta a valle del condensatore: un operazionale in
   configurazione non invertente. Possiamo determinare in modo accurato la
   costante di tempo del dispositivo. Questo è possibile perchè stiamo usando
   un buffer solo per l'adattamento d'impedenza. Se avessimo messo solo un
   buffer dopo il condensatore sarebbe stato diverso. Siccome il potenziale in
   uscita su V2 è uguale alla tensione V1 è possibile effettuare un
   collegamento diretto tra la linea di retroazione dell'opamp a valle e il
   morsetto negativo dell'opamp a monte. In altre parole, la retroazione
   dell'opamp a monte è stata collegata a V2 e scollegata dal condensatore.
   Esiste un vantaggio nella configurazione riportata rispetto alla costante di
   tempo. Il condensatore è collegato ad un ingresso all'operazionale. Se
   avessi collegato solo il buffer avrei collegato due ingressi
   all'operazionale al condensatore. Dimezzerei la costante di tempo del
   circuito in configurazione di hold. Si migliorano le performance dinamiche
   del circuito di un fattore 2. Questo perchè vogliamo avere una costante di
   tempo il più lunga possibile per avere il valore più preciso al momento
   della conversione.

Il blocco funzionale del convertitore analogico-digitale (ADC), il responsabile
del campionamento, in realtà incorpora tre differenti blocchi.

![Schema a blocchi del convertitore analogico-digitale\label{fig:diagramma_adc}][diagramma_adc]

1. Multiplexer: viene spesso incorporato negli ADC con lo scopo di usare un
   convertitore con molti canali in ingresso. Questo effettua il campionamento
   nello spazio.
2. Campionatore (Sample and Hold, S&H): la conversione da analogico a digitale
   prevede due funzioni: il campionamento nel tempo e nello spazio. Nel tempo
   considerare un segnale che varia nel tempo e definire degli istanti
   temporali nei quali effettuare la conversione numerica del dato analogico
   che il segnale assume in quell'istante.
3. Il convertitore vero e proprio: il valore analogico discretizzato nel tempo
   viene convertito nella migliore approssimazione offerta dalla codifica in
   digitale.

### Il convertitore vero e proprio

Campionato nel tempo un segnale analogico, è necessario trasformarlo nella
codifica (sequenza di cifre) che possano esprimere un numero che meglio
approssima il valore analogico che avevemo in ingresso. Usiamo quindi i
dispositivi di conversione. Esistono diverse famiglie di convertitori.

Caratteristiche:

- Tecniche.
- Velocità di conversione.
- *Risoluzione*: Bit usati per esprimere i valori in ingresso. A seconda del
  numero di bit, è possibile indicare qual è il numero di livelli con cui viene
  diviso il valore del fondo scala del dispositivo (FS). Se avessimo un ADC che
  lavora da 0 a 1V con risoluzione di 1 bit, si potranno distinguere due
  possibili valori, ossia se l'ingresso è più basso o più alto di 0.5V. Se
  utilizzassimo invece 10 bit sarà possibile avere una risoluzione pari a
  $\dfrac{1}{1024} V=1mV$. Il vantaggio della codifica digitale è che,
  nonostante sia necessario un minimo numero significativo di bit per
  rappresentare una cardinalità significativa di valori, sta nella relazione
  logaritmica tra la cardinalità dell'insieme di simboli che sono possibili
  rappresentare e il numero di simboli considerati per la rappresentazione che
  consente una crescita importante. Con pochi bit in più la risoluzione diventa
  molto più elevata dei millivolt.
- Errori di conversione (aka errore di quantizzazione): quanto il valore in
  uscita si discosta da quello reale in ingresso. L'ADC compie sempre
  un'approssimazione, ricordando il limite dei segnali digitali di poter
  rappresentare un insieme finito di valori. Nel caso di segnali analogici
  questo problema non si verificava perchè si rappresentano degli insiemi
  infiniti di valori. Quando effettuiamo una quantizzazione, necessariamente
  compiamo un'approssimazione. Il range possibile di questi valori è
  $E_{q}=\dfrac{FS}{2^{N}}$ con FS: fondo scala, N: numero di bit dell'ADC.
  Questa relazione indica l'ampiezza dell'intervallo di tensione che viene
  rappresentata dalla stessa codifica numerica. L'errore spazia tra -LSB/2 e
  +LSB/2.
- Ripetibilità: se per cinque volte il convertitore riceve un ingresso uguale,
  è in grado di fornire un'uguale uscita?
- Semplicità dei circuiti di servizio. Il dispositivo non è mai usato fine a se
  stesso ma con utilizzatore (il blocco di elaborazione) che poi utilizza
  l'informazione campionata. Il blocco d'elaborazione normalmente s'interfaccia
  col dispositivo di conversione, impartisce il comando di campionamento e poi
  acquisisce il dato una volta che la conversione è completata. è quindi
  necessario avere una capacità di comunicazione tra il sistema di elaborazione
  e l'ADC e ciò viene mediata dai circuiti di servizio. Il tipo di circuiti
  necessari per un certo dispositivo fanno propendere nella scelta finale.
  Tipicamente i circuiti di servizio necessitano di una sorgente di clock.
- Sempicità dell'interfacciamento con microelaboratori. Questo punto è molto
  simile al precedente ma in relazione all'uso dell'ADC da parte di un
  microprocessore.
- Costo

#### Caratteristiche degli errori

Il dispositivo reale possiede degli errori, in aggiunta a quello di
quantizzazione che invece è intrinseco al concetto di digitalizzazione, quindi
indipendente dal comportamento reale del dispositivo ADC.

Questi errori possono essere presenti singolarmente o in combinazione. Per
interpretare i grafici riportati è necessario considerare:

1. la retta continua come la relazione tra uscita dell'ADC ideale (i.e. valore
   analogico teorico: l'intervallo di valori per i quali l'ADC codifica
   l'ingresso è costituito da un solo valore) e l'ingresso analogico all'ADC.
2. la retta tratteggiata rappresenta la stessa relazione di quella continua ma
   per dispositivi affetti dal particolare errore
3. I "gradini" rappresentano i livelli d'uscita che l'ADC reale associa agli
   specifici intervalli di tensione in ingresso.

Errori commessi dal dispositivo reale:

- Errore di offset: scostamento della curva caratteristica dell'ADC a destra o
  a sinistra rispetto al valore analogico teorico. L'errore che viene commesso
  in questo caso è costante in qualunque conversione stia commettendo. Questo
  tipo d'errore è facilmente risolvibile: una volta identificatane l'entità, è
  sufficiente eseguire una somma/differenza per compensarne l'effetto. L'errore 
- Errore di guadagno: In questo caso vi è un'errore sul coefficiente angolare
  della retta. L'errore è proporzionale all'ampiezza del segnale che si sta
  campionando. L'errore sarà quindi nullo nell'origine ma aumenterà sempre più
  all'aumentare dell'input. Anche quest'errore è facile da compensare mediante
  operazioni algebriche.
- Errore di non linearità: quest'errore è applicato su tutto l'arco di
  conversione. L'errore più difficile da risolvere.
- Errore di linearità differenziale: causa un'alterazione dell'ampiezza degli
  intervalli di quantizzazione.

#### Tecniche di conversione

- SAR: la tecnologia più largamente utilizzata. Semplice dal punto di vista
  hardware. Rispetto alla flash ha un solo comparatore. Ad ogni iterazione
  viene determinato il valore di un bit. 
- A rampa: ...
- A inseguimento: si utilizza un contatore di tipo up/down. Attraverso
  l'ingresso digitale del contatore questo aumenta (diminuisce) di un unità ad
  ogni colpo di clock se impostato in up (down). La linea di clock del
  contatore u/d è connessa ad un oscillatore con una frequenza fissa.
  L'ingresso digitale u/d è collegato ad un comparatore di tensione. Il
  comparatore ha agli ingressi il segnale analogico da convertire e la
  conversione digitale-analogico del segnale in uscita al contatore. Se il
  segnale analogico è maggiore (minore) della conversione il contatore sarà in
  up (down), quindi il valore della conversione digitale-analogico aumenta
  (diminuisce).

- convertitori tensione/frequenza: il timer è connesso alla linea di enable del
  contatore e stabilisce il periodo temporale prefissato che dev'essere
  "diviso" al numero di impulsi contati. L'optoisolatore invia il numero di
  impulsi registrati nel periodo di tempo. VCO restituisce un'onda quadra con
  frequenza proporzionale alla tensione in ingresso. Vantaggio: uso una linea
  digitale per codificare (in frequenza) un valore analogico, al posto di tanti
  bit.

## Ripasso C

### Algoritmo

È una serie finita e non ambigua di istruzioni (passi eseguibili) ripetibili un
numero finito di volte, che consente di ottenere la soluzione di un problema.

Le istruzioni eseguibili possono essere scritte in un linguaggio particolare,
detto linguaggio di programmazione. L'utilizzo di questi linguaggi è necessario
affinché i particolari dispositivi possano comprendere le istruzioni che
vogliamo siano eseguite.

### Linguaggi

I microprocessori comprendono il linguaggio macchina. Una successione di 0 e 1.
è impossibile per un umano scrivere direttamente in questo linguaggio. Si è
quindi eseguito un passaggio intermedio che consiste nell'associare ad un nome
breve una sequenza predefinita di zeri e uni (e.g. sum: 001101). In questo modo
si associa automaticamente a questa stringa la serie di zeri e uni che
vogliamo. Questo primo passaggio viene fatto tramite il linguaggio assembler. I
traduttori passano queste parole inglesi a sequenze di zeri e uni. Questo
linguaggio assembler però è ancora troppo vicino a quello macchina, quindi
difficilmente utilizzabile dagli esseri umani per poter svolgere delle
operazioni complesse.

Per poter risolvere il problema sono stati sviluppati dei linguaggi di alto
livello. Un esempio di questo è C.

I passaggi per ottenere il linguaggio macchina a partire da quello ad alto
livello sono due:

1. Mediata dal compilatore. Da linguaggio ad alto livello ad assembly.
2. Mediata da assembler. Traduzione in linguaggio macchina.

In questo modo si passa dalla stesura di un algoritmo in linguaggio ad alto
livello ad uno macchina.

### Ciclo di sviluppo software

Per scrivere un firmware ci sono 5 step.

1. Editing: si scrive l'algoritmo nel linguaggio ad alto livello. L'output a
   questo livello è il file sorgente ('.c' e '.h'). Questi file raccolgono
   l'algoritmo vero e proprio.
2. Compilazione:
	1. Precompilazione: controlla che non vi siano errori di sintassi
	   all'interno del codice.
	2. Direttive di preprocessore. Nei nostri file utilizziamo delle
	   funzioni che sono state definite da altre o che sono state definite
	   da noi in altri file e che è necessario richiamare per poter
	   eseguire il codice. Prima di compilare quindi il compilatore deve
	   prendere le parti di codice "mancanti" e le inserisce adeguatamente.

L'uscita di questa fase è il programma oggetto ('.o'). Questo non è ancora
adeguato perché vi sono dei buchi che contengono parte di codice che è
specifica per quel dispositivo e che quindi è scritta da qualche altra parte. È
quindi necessaria un'ulteriore fase.

3. Linking. Collega le parti dispositivo-specifiche. Il linker va a vedere
   nelle librerie le parte di codice che serve a generare il programma
   eseguibile. Si ottiene quindi questo programma eseguibile.
4. Loading: Bisogna trasferire ora l'eseguibile sul processore, di modo che
   questo possa eseguire quelle istruzioni. Loading copia il codice
   direttamente sul dispositivo che deve poi eseguirlo. Delle volte serve
   dell'hardware, un programmatore, che interfaccia il dispositivo al computer,
   delle altre invece il programmatore è built-in.
5. Esecuzione: il dispositivo esegue il programma un'istruzione per volta.

### Formato di programma in C.

Si consideri l'esempio dell'Hello World.

\#include rappresenta la direttiva di preprocessore. Sta dicendo che il
preprocessore deve andare a cercare nella libreria 'stdio.h' una funzione, la
printf.

Quindi ciascuna istruzione preceduta da '\#' costituisce una direttiva al
preprocessore.  Successivamente vengono le dichiarazioni di variabili,
costanti, funzioni. La funzione main non può mancare in un programma C. Poi vi
sono le subroutine.

### La gestione dati in C

I dati sono sequenze di 0 e 1 che vengono messi nella memoria RAM e questi
possono essere variabili o costanti. La differenza tra variabili e costanti è
che le prime sono modificabili. Entrambe sono definibili mediante un nome. 

Le variabili hanno le seguenti proprietà di:

1. Il nome (e.g. pressioneSistolica)
2. Il tipo di dato (uint8_t ...)
3. Il valore
4. L'indirizzo in memoria

### Puntatori

All'interno della cella di memoria non contiene il valore della variabile ma
l'indirizzo della variabile d'interesse. Quando lo si dichiara bisogna
utilizzare il tipo della variabile a cui si punta.

### Array

Sono dei vettori che possiedono lo stesso tipo di dato.

### Strutture

Definisce un modello che individua un'aggregazione di tipi di dato fondamentali.

### Enumeratori

Avendo un elenco finito di termini (e.g. giorni della settimana) è possibile
elencarli con il loro nome.

```c
typedef enum {LUN, MAR,
		MER, GIO,
		VEN, SAB,
		DOM} Week;
		
Week oggi=LUN;

printf("%d", oggi); // Quest'istruzione restituirà 0

Week oggi=LUN+1;

printf("%d", oggi); // Quest'istruzione restituirà 1

Week oggi=MER;

printf("%d", oggi); // Quest'istruzione restituirà 2
```

### Operatori particolari

#### '.' e "->"

```c
typedef struct{
	uint8_t x;
	uint8_t y;
	uint8_t z;
	} Pixel;
	
Pixel pixel;

pixel.x=10; // '.' permette di popolare un elemento di una struct.
pixel.y=20;
pixel.z=30;
```

```c
typedef struct{
	uint8_t x;
	uint8_t y;
	uint8_t z;
	} Pixel;
	
Pixel *pixel;

pixel->x=10; // "->" permette di popolare un elemento di un puntatore a struct.
pixel->y=20;
pixel->z=30;
```

#### Operazioni bit a bit

- Shift di bit

Spostando verso destra o sinistra un numero (studiandone la rappresentazione in
bit), otterrei una divisione per due e una moltiplicazione per due
rispettivamente.

```c

uint8_t a = 1;
printf("%d", a);
a<<=2; // sposto a sinistra di 2.
printf("%d", a);
a>>=1; // sposto a destra di 1.
printf("%d", a);

```

- AND (&). Equivale alla porta logica AND applicata su ciascun bit dei dati
  coinvolti.
- OR (|). Equivale alla porta logica OR applicata su ciascun bit dei dati
  coinvolti.
- Complemento a 1 (~): il bit cambia da 0 a 1 o da 1 a 0 a seconda dello stato
  iniziale. Questo accade per tutti i bit della variabile su cui si applica.

##### Maschere

In alcuni esercizi, è richiesto di porre un bit (e.g. il quinto) di una
variabile di tipo uint8_t a 1 senza modificare il valore degli altri. Per fare
ciò basta mettere in OR questa variabile con la maschera (1<<5).

In altri esercizi, è richiesto di annullare un bit (e.g. il quinto) di una
variabile di tipo uint8_t senza modificare il valore degli altri. Per fare ciò
basta mettere in AND il complemento a uno della maschera (1<<5).

#### Cast e conversioni di tipo

Esistono delle gerarchie: float>long>int>short>char. Quando faccio
un'operazione tra due dati di tipo diverso il compilatore convertirà
automaticamente il tipo che occupa la gerarchia più bassa in quello a gerarchia
più alta.

Quando abbiamo un'assegnazione si svolgono prima tutte le operazioni a destra
dell'uguale considerando la gerarchia appena spiegata ma poi il tipo di dato
finale è quello stabilito dalla variabile a sinistra dell'uguale. Per cui se a
destra vince float ma a sinistra ho un int l'assegnazione fa vincere il tipo
int. 

### Istruzioni di controllo

If else.

### Istruzioni di controllo condizionale

Switch case

### Cicli

for, while, do while.

### Funzioni

Al termine dell'esecuzione di una funzione, le variabili al suo interno (i.e.
variabili locali) vengono eliminate e non possono essere più utilizzate.

È possibile passare quindi le variabili da una funzione all'altra tramite copia
oppure tramite indirizzo.

Se voglio che una variabile sia cambiata da più funzioni, la posso rendere
globale.


## Fondamenti di Elettronica

### La corrente elettrica

La carica di elettroni che attraversa un conduttore. il rapporto incrementale
della carica in un dato tempo.

$I=\frac{dq}{dt}$

I conduttori possono avere diverse proprietà. Alcuni hanno più cariche libere,
altri meno. Per fare in modo che gli elettroni si spostino da un punto
all'altro, quindi che producano corrente elettrica, è necessario applicare una
differenza di potenziale ai capi del conduttore.

Per convenzione, quando parliamo di corrente elettrica e di flusso di
elettroni, i due versi sono opposti tra di loro. In altre parole indichiamo il
verso di movimento delle cariche positive.

Il conduttore presenta una resistenza che dev'essere vinta per trasportare la
corrente da un punto all'altro.

La resistenza dipende dalle proprietà elettriche (e.g. conducibilità) e
geometriche del materiale che stiamo utilizzando (e.g. lunghezza).

$R = \rho \cdot \dfrac{l}{A}$

Se il conduttore è molto lungo, la resistenza aumenta. Diminuisce se è largo
(grande area trasversale).

Anche fattori ambientali influiscono sulla resistenza di un materiale. Uno di
questi è la temperatura, che fa variare il comportamento dei materiali a
seconda del tipo di essi.

### Legge di Ohm

Resistenza, intensità di corrente e tensione applicata sono regolate da una
legge, la Legge di Ohm.

$V = R \cdot V$. 

Possiamo inoltre andare a calcolare la potenza che stiamo utilizzando. In
questo modo possiamo comprendere quant'è la potenza dedicata ad una parte di
circuito. Se questa è completamente resistiva si dissiperebbe molta energia.
Questo parametro è dungue particolarmente importante.

$P = V \cdot I = \dfrac{V^{2}}{R} = I^{2}R$

### Resistenze in serie e parallelo

- In serie: resistenza totale è la somma delle singole
- In parallelo: il reciproco della resistenza totale è pari alla somma dei
  reciproci.

### Corrente alternata

In regime alternato, non si ha più corrente continua ma alternata. Oscilla in
modo sinusoidale. In particolare la rete domestica ha un'ampiezza di 220V e una
frequenza d'oscillazione di 50Hz. La differenza rispetto al regime continuo sta
nella forma d'onda e nel modo in cui scorrono gli elettroni all'interno. Si
possono utilizzare tutti i ragionamenti visti per la corrente continua a patto
di utilizzare il valore efficace per le tensioni e per le correnti. Questo
valore è indicato normalmente con la sigla RMS (i.e. Root Mean Square).
Utilizzando il valore efficace posso applicare la legge di Ohm.

$V_{RMS} = \dfrac{V_p}{\sqrt{2}}$

$I_{RMS} = \dfrac{I_p}{\sqrt{2}}$

Legge di Ohm (RMS):

$$ V_{RMS} = R \cdot I_{RMS} \rightarrow P_{RMS} = V_{RMS} \cdot I_{RMS} $$

Nei circuiti trattati nel corso le tensioni e le correnti considerate saranno
principalmente in regime continuo.

### Condensatori

Componente elettronico classico. Permette di accumulare carica, come in una
batteria. Esso è costituito da due piastre collocate una di fronte all'altra.
Applicando una tensione a queste piastre, queste si caricheranno e creeranno un
campo elettrico tra le due.

Le leggi da ricordare sono le seguenti:

$$Q = C \cdot \Delta V \rightarrow I = C \cdot \dfrac{dV}{dt}$$

Esistono diversi tipi di condensatori:

- A valore fisso: e.g. un condensatore a 100 nF rimarrà tale per tutta la
  durata del componente. Ne esistono di due tipi:
  - Al tantalio: non polarizzati. Posso inserirli in qualunque verso nel
    circuito senza avere il rischio di danneggiarli.
  - Elettrolitici: Polarizzati, quindi possiedono un verso d'inserimento
    indicato sul packaging.
- Variabili: e.g. meccanici (una parte di condensatore può muoversi, variando
  le proprietà di questo).

Una capacità reale ha in parallelo una resistenza di leakage ($R_{L}$) che
permette il passaggio di carica da una parte all'altra del condensatore.

Ad altissime frequenze il modello da considerare è più complesso e tiene conto
dei vari parassitismi insiti al componente (dovuti alla presenza di induttanze,
capacità e resistenze interne).

Il parallelo aggiunge area alla superficie del condensatore (che nel caso del
condensatore piano si trova a numeratore della capacità), quindi l'equivalente
sarà dato dalla somma delle singole capacità.

L'equivalente della serie è il reciproco della somma dei reciproci.

#### Carica e scarica di un condensatore

Per quanto riguarda la carica dei condensatori ci si riferisca a questi grafici.

![Circuito di carica.][condensatore_carica_circuito]
![Grafico di carica.][condensatore_carica_grafico]

Si osservi che la corrente che scorre attraverso la resistenza (quindi anche
attraverso il condensatore) è inizialmente massima ($I_R (0) =
\dfrac{V_s}{R}$). Questo perchè la tensione ai capi del resistore è
inizialmente pari a $V_s$, dato che è scarico. Al caricarsi del condensatore,
sia la tensione che la corrente decade esponenzialmente verso valori nulli.

$$ V_R (t) = V_s \cdot e^{-\frac{t}{\tau}} $$
$$ I_R (t) = \dfrac{V_s}{R} \cdot e^{-\frac{t}{\tau}} $$

Con $\tau = RC$ costante di tempo calcolata considerando la resistenza a
partire dai morsetti del condensatore. Le due curve $V_R (t)$ e $I_R (t)$ hanno
valori iniziali diversi perchè dipendenti dalla legge di Ohm.

La tensione ai capi del condensatore si accumula con una legge esponenziale:

$$V_C (t) = \dfrac{1}{C} \int{I \cdot dt} = V_s \cdot (1 - e^{-\frac{t}{\tau}})$$

---

Per quanto riguarda la scarica dei condensatori ci si riferisca a questi grafici.

![Circuito di scarica.][condensatore_scarica_circuito]
![Grafico di scarica.][condensatore_scarica_grafico]

Il circuito è diverso rispetto al precedente: il resistore è ora connesso in
serie al condensatore che, essendo carico, si comporta come una fonte
d'alimentazione. La corrente quindi fluisce dal condensatore al resistore fino
a quando la tensione ai capi del primo non è ricondotta a massa.

La tensione ai capi del condensatore, essendo in parallelo a quella del resistore, avrà la stessa decaduta.

$$V_C (t) = V_R (t) = V_s \cdot e^{-\frac{t}{\tau}}$$.

La corrente che scorre nel circuito verrà scalata sempre in relazione alla
legge di Ohm.

$$I_R(t) = \dfrac{V_R (t)}{R} = \dfrac{V_s}{R} \cdot e^{-\frac{t}{\tau}}$$

### Induttore

Utilizzando un induttore si va a produrre un campo magnetico. Quando esso viene
attraversato da una corrente variabile, questo è soggetto alla legge di Lentz.
All'opposizione dell'induttore alla variazione di corrente, si genera un campo
magnetico che dipende dai valori di variazione di corrente. È vero anche il
contrario: quando l'induttore viene attraversato da un campo magnetico, può a
sua volta generare corrente. è importante ricordare che questo genere di
oggetti lavora sotto il regime alternati. Se diamo una tensione continua ai
capi di un'induttore accade che, trascorso il transitorio, l'induttore divente
solo una resistenza molto bassa.

### Semiconduttori

Le tecnologie che si basano su questo tipo di materiali permettono di realizzare componenti con diverse funzioni.

#### Diodi

Se correttamente alimentati i diodi sono componenti che impediscono il
passaggio di corrente o che lo favoriscono a seconda della tensione ai capi.

I diodi zener sono una tipologia particolare che funzionano anche se vengono
fornite delle tensioni molto negative, stabilizzando la tensione attorno ad una
particolare tensione. Possiedono anche essi una tensione di breakdown al di
sotto della quale il componente non funziona più correttamente.

#### Fotodiodo

Il fotodiodo è un componente in grado di fornire un segnale analogico (corrente
o tensione a seconda del condizionamento che subisce) dipendente dall'intensità
della radiazione incidente su di esso.

#### Transient Voltage Suppressor Diodes (aka TVS)

Per ridurre i pulsi ESD (ElectroStatic discharge), i pulsi EFT (Electrical Fast
Transient). Quando una tensione superiore a quella di breakdown di questa
tipologia di diodi viene fornita al circuito (a causa di una delle condizioni
sopra citate) la corrente in eccesso generata viene indotta all'interno di
questo componente. Questo serve in particolare per limitare l'alimentazione di
carichi.

[*Diodo TVS, video che spiega il principio di funzionamento.*](https://www.youtube.com/watch?v=oKACLpBYhxU)

#### Silicon-controlled rectifiers (SCR)

![Rappresentazione circuitale del componente SCR.][scr]

Questa tipologia di componente presenta tre morsetti:

- Anodo (A)
- Catodo (C)
- Gate (G)

Il comportamento è il seguente:

- L'*apertura del canale* avviene fornendo un segnale specifico al gate G.
- La *chiusura del canale* avviene **solo** con la rimozione sia del segnale
  sul gate G che della tensione su anodo A e catodo C.

![Esempio di utilizzo di un SCR.\label{fig:scr_esempio}][scr_esempio]

Nella figura \ref{fig:scr_esempio} viene riportato un esempio d'utilizzo. Si
noti che, azionando il bottone $S_1$, il segnale sul gate è tale da aprire il
canale del SCR. Al rilascio del bottone, il canale rimarrà aperto perchè la
tensione ai capi del componente non è stata rimossa, quindi il carico
continuerà ad essere alimentato. Solamente alla pressione del bottone $S_2$ il
ramo di destra non sarà più collegato ad alimentazione e questo disabiliterà
anche il canale di SCR, il quale potrà mantenere il circuito aperto, una volta
rilasciato $S_2$.

#### TRIAC (aka TRiode for Alternating Current)

Equivale ad un SCR il cui ambito d'utilizzo è la corrente alternata. Questo
significa che la principale differenza sta nel sostenere entrambi i versi di
scorrimento della corrente. Inoltre per poter azionare/escludere il componente
è sufficiente fornire segnale al gate G.

![Rappresentazione circuitale del componente TRIAC.][triac]

#### Transistors

Sono dei componenti costituiti da strati di silicio p ed n sovrapposti e
servono principalmente per modulare l'alimentazione fornita ai carichi
connessi.

Ne esistono di varie tipologie a seconda della tipologia di controllo che si
vuole realizzare.

- *Bipolar Junction Transistor (aka BJT)*. Questo è un transistor **controllato
  in corrente**. 
  
  ![Rappresentazione circuitale di un BJT.][bjt] 
  
  La condizione per poter attivare il dispositivo è quella di imporre una
  tensione $V_{BE} = 0.6V$ positiva nel caso npn (sulla sinistra), negativa nel
  caso pnp sulla destra. La corrente che scorre da collector ad emitter nel
  caso npn (viceversa nel caso pnp) è proporzionale alla corrente che scorre
  attraverso base. In particolare: $$I_C = \beta \cdot I_B$$ con $\beta$
  current gain, un valore che dipende dalla natura del transistor (compreso tra
  10 e 500). Questo transistor è utilizzato nel caso in cui si vogliano fornire
  elevate quantità di corrente.
- *Junction Field-Effect Transistor (aka J-FET)*. A differenza del precedente
  questo è **controllato in tensione**: a seconda del voltaggio fornito al gate
  G, si modula l'opposizione al passaggio della corrente tra drain D e source
  S. In particolare:
  
	- I J-FET a *canale p*: maggiore la $|V_{GS}|$ (con segno positivo) e
	  maggiore sarà la resistenza opposta dal canale (in regime di
	  saturazione).
	- I J-FET a *canale n*: maggiore la  $|V_{GS}|$ (con segno negativo) e
	  maggiore sarà la resistenza opposta dal canale (in regime di
	  saturazione).

- *Metal Oxide Semiconductor Field-Effect Transistor (aka MOSFET)*. Questi sono
  *comandati in tensione*. La tensione al gate controlla l'apertura del canale
  tra drain e source. In particolare:
	
	- I MOSFET a *canale p*: maggiore la $|V_{GS}|$ (con segno positivo) e
	  minore sarà la resistenza opposta dal canale (in regime di
	  saturazione).
	- I MOSFET a *canale n*: maggiore la  $|V_{GS}|$ (con segno negativo) e
	  minore sarà la resistenza opposta dal canale (in regime di
	  saturazione).

- *Insulated Gate Bipolar Transistor (aka IGBT)*. Una via di mezzo tra i MOSFET
  e BJT: sono **comandati in tensione** ma possono gestire grandi correnti come
  BJT.
  
  ![Rappresentazione di IGBT.][igbt]
  
- *Transistor Darlington*. Sono composti da una cascata di due BJT. La loro
  caratteristica principale è l'altissimo guadagno in corrente pari al prodotto
  delle due $\beta$ relative a ciascun BJT.
  
  ![Rappresentazione di un transistor Darlington.][darlington]

### Amplificatori operazionali (aka op amps)

L'op amp è un IC che amplific la differenza di potenziale ai morsetti
d'ingresso. In particolare: 

- Se la tensione ai capi del terminale "+" è superiore a quella registrata al
  terminale "-", l'uscita satura ad alimentazione positiva $+V_s$.
- Se la tensione ai capi del terminale "+" è inferiore a quella registrata al
  terminale "-", l'uscita satura ad alimentazione negativa (o massa in caso di
  alimentazione non duale) $-V_s$.

È dunque possibile realizzare due diverse configurazioni in anello aperto: un
comparatore non invertente e un comparatore invertente. Nel primo caso, il
segnale in ingresso è iniettato sul morsetto "+", andando a modulare un'uscita
avente lo stesso segno dell'ingresso. Nel secondo invece il segnale sarà
iniettato sul morsetto "-" e l'uscita varierà con un segno opposto a quello
dell'ingresso.

#### Op amp in feedback negativo

Tipicamente il guadagno dell'op amp può essere controllato fornendo un feedback
all'ingresso.

Le configurazioni a feedback negativo utilizzate sono la non invertente,
l'invertente e il buffer.

- *Non invertente*: $V_{out} = (1 + \dfrac{R_2}{R_1}) \cdot V_{in}$.
- *Invertente*: $V_{out} = -\dfrac{R_2}{R_1} \cdot V_{in}$.
- *Buffer*: $V_{out} = V_{in}$. è una tipologia di non invertente con guadagno
  unitario. Esso è particolarmente utile per disaccoppiare i circuiti a monte
  da quelli a valle (disaccoppiamento d'impedenza).

#### Feedback positivo e trigger di Schmitt

Scambiando i terminali "+" e "-" nelle configurazioni viste prima, si ottengono
degli op amp a feedback positivo. Questi sono utili nel momento in cui si
vogliono realizzare dei trigger di Schmitt in configurazione invertente
(tensione in ingresso al morsetto "-") e non invertente (tensione in ingresso
al morsetto "+").

##### Invertente

Si considerino i seguenti grafici.

![Trigger di Schmitt invertente.\label{fig:schmitt_invertente}][schmitt_invertente]
![Caratteristica del trigger di Schmitt invertente al variare dell'ingresso.\label{fig:schmitt_invertente_caratteristica}][schmitt_invertente_caratteristica]
![Diagrammi di ingresso e uscita al trigger di Schmitt invertente.\label{fig:schmitt_invertente_out_vs_in}][schmitt_invertente_out_vs_in]

Si imponga per semplicità $v_{ref} = 0$

$v_{out} = (1 + \dfrac{R_1}{R_2})\cdot v_{in}$

$v_{in} = (\dfrac{R_2}{R_1 + R_2})\cdot v_{out}$

Considerando che $V_{t+} > V_{t+}$ e sostituendo appropriatamente nella formula
gli ingressi con le uscite:

$v_{t+} = \dfrac{R_2}{R_1 + R_2} \cdot v_{oH}$

$v_{t-} = \dfrac{R_2}{R_1 + R_2} \cdot v_{oL}$

Osservando il secondo grafico:

- Quando $v_s \geq v_{t+}$ l'uscita satura a $v_{oL}$.
- Quando $v_s \leq v_{t-}$ l'uscita satura a $v_{oH}$.


##### Non invertente

Si considerino i seguenti grafici.

![Trigger di Schmitt non invertente.\label{fig:schmitt_non_invertente}][schmitt_non_invertente]
![Caratteristica del trigger di Schmitt non invertente al variare dell'ingresso.\label{fig:schmitt_non_invertente_caratteristica}][schmitt_non_invertente_caratteristica]

Si imponga per semplicità $v_{ref} = 0$

$v_{out} = -\dfrac{R_1}{R_2} \cdot v_{in}$

$v_{in} = -\dfrac{R_2}{R_1} \cdot v_{out}$

Considerando che $V_{t+} > V_{t+}$ e sostituendo appropriatamente nella formula
gli ingressi con le uscite:

$v_{t+} = -\dfrac{R_2}{R_1} \cdot v_{oL}$

$v_{t-} = -\dfrac{R_2}{R_1} \cdot v_{oH}$

Utilizzando questo tipo di feedback è possibile generare due diverse soglie per
saturare diversamente l'uscita. Viene riportato l'esempio di un trigger di
Schmitt non invertente. Questo saturerà l'uscita a $+V_{CC}$ quando la tensione
in ingresso è maggiore della soglia $V_{t+}$ a $-V_{CC}$ quando la tensione in
ingresso è inferiore rispetto alla soglia $V_{t-}$.

Il grafico degli ingressi e delle uscite è simile a quello visto nel caso
invertente ma con l'onda quadra ribaltata.

### Porte Logiche

L'elettronica digitale utilizza diversi componenti per effettuare operazioni
tra diversi segnali. Vengono omesse nella descrizione le versioni negate delle
porte.

#### NOT (INVERT)

![][not]

È una porta avente un singolo ingresso ed un'uscita.

| IN   | OUT  |
| :--: | :--: |
| 0    | 1    |
| 1    | 0    |

#### AND

![][and]

È equivalente ad una moltiplicazione tra numeri binari. L'elemento neutro di
quest'operazione è "1".

| A    | B    | OUT  |
| :--: | :--: | :--: |
| 0    | 0    | 0    |
| 0    | 1    | 0    |
| 1    | 0    | 0    |
| 1    | 1    | 1    |


#### OR

![][or]

È simile ad una somma tra numeri binari, non del tutto equivalente. L'elemento
neutro di quest'operazione è "0".

| A    | B    | OUT  |
| :--: | :--: | :--: |
| 0    | 0    | 0    |
| 0    | 1    | 1    |
| 1    | 0    | 1    |
| 1    | 1    | 1    |

#### XOR

![][xor]

È più simile alla somma tra due numeri binari (si veda la trattazione dei campi
finiti in [*questo video*](https://www.youtube.com/watch?v=izG7qT0EpBw&t=1001s)
se si è curiosi).

| A    | B    | OUT  |
| :--: | :--: | :--: |
| 0    | 0    | 0    |
| 0    | 1    | 1    |
| 1    | 0    | 1    |
| 1    | 1    | 0    |

### Latch Set-Reset (aka SR latch)

![][srlatch]

| S    | R    | Q    | ~Q   | Condizione     |
| :--: | :--: | :--: | :--: | :--:           |
| 0    | 0    | 0    | 1    | Memorizzazione |
| 0    | 1    | 0    | 1    | Reset          |
| 1    | 0    | 1    | 0    | Set            |
| 1    | 1    | 0    | 0    | Non utilizzato |

[*Questo video*](https://www.youtube.com/watch?v=KM0DdEaY5sY) mostra
dettagliatamente il funzionamento.

### Latch e flip flop di tipo D

Per comprendere la differenza tra flip-flop di tipo D e latch di tipo D, fare riferimento ai due seguenti video:

- [*Parte 1: Da un SR Latch a un D Latch.*](https://www.youtube.com/watch?v=peCh_859q7Q)
- [*Parte 2: Da un D Latch "modificato" a un Flip-flop di tipo D*](https://www.youtube.com/watch?v=YW-_GkUguMM)

![Latch (flip flop) di tipo D.][dlatch]

Il latch di tipo D condivide una struttura simile al latch SR. L'attivazione di
questa struttura avviene solo a fronte di un segnale di enable: quando EN è
high, è possibile memorizzare il bit, altrimenti no. Il dato è messo in input a
due porte AND il cui secondo ingresso è la linea di enable. Il primo ingresso è
costituito dal dato per la prima AND e dal dato negato per la seconda.

Il flip flop di tipo D è esattamente lo stesso circuito del latch di tipo D ma
che effettua le memorizzazioni sul fronte di salita di un clock in input ad
esso. Il problema che si deve risolvere per progettare un flip flop è quindi
legato all'edge detecting. La strategia utilizzata è quella di utilizzare un
filtro passa alto RC per poter estrarre solo la componente ad alta frequenza
del clock.

Si presenta ora la tavola di verità di un flip flop di tipo D.

| D    | CLK    | Q    | ~Q   | Modalità       |
| :--: | :--:   | :--: | :--: | :--:           |
| 0    | Rising | 0    | 1    | Reset          |
| 1    | Rising | 1    | 0    | Set            |
| X    | X      | Q    | ~Q   | Memorizzazione |

### JK flip flop

![Flip flop di tipo JK.][jkflipflop]

[*Questo video*](https://www.youtube.com/watch?v=F1OC5e7Tn_o) mostra
dettagliatamente il funzionamento.

Una delle condizioni inutilizzate del latch SR era quella avente entrambi gli
ingressi a 1 (invalid state o racing). Il flip flop JK vuole utilizzare anche
questa configurazione implementando un toggle.

Partendo da un SR latch controllato mediante una linea di enable, si riporta
alle due porte AND in ingresso gli stati logici delle uscite come feedback. Il
risultato, considerando entrambe le linee d'ingresso impostate con high, è una
condizione di toggle perchè solamente una delle due porte AND in ingresso
cambierà lo stato della propria uscita.

| CLK    | J    | K    | Q      | ~Q     | Modalità       |
| :--:   | :--: | :--: | :--:   | :--:   | :--:           |
| Rising | 0    | 0    | Q      | ~Q     | Memorizzazione |
| Rising | 0    | 1    | 0      | 1      | Reset          |
| Rising | 1    | 0    | 1      | 0      | Set            |
| Rising | 1    | 1    | toggle | toggle | Toggle         |
| X      | X    | X    | Q      | ~Q     | Memorizzazione |

### Flip flop JK master-slave

![Flip flop JK master-slave][jkmasterslave]

| ~PRE | ~CLR | CLK    | J    | K    | Q      | ~Q     | Modalità       |
| :--: | :--: | :--:   | :--: | :--: | :--:   | :--:   | :--:           |
| 0    | 1    | X      | X    | X    | 1      | 0      | Preset         |
| 1    | 0    | X      | X    | X    | 0      | 1      | Clear          |
| 0    | 0    | X      | X    | X    | 1      | 1      | Non usata      |
| 1    | 1    | Rising | 0    | 0    | Q      | ~Q     | Memorizzazione |
| 1    | 1    | Rising | 0    | 1    | 0      | 1      | Reset          |
| 1    | 1    | Rising | 1    | 0    | 1      | 0      | Set            |
| 1    | 1    | Rising | 1    | 1    | toggle | toggle | Toggle         |

Questa tipologia di flip flop possiede sia le caratteristiche asincrone del
latch (dovute alla presenza dei segnali PRE e CLR negati) che quelle sincrone
del flip flop di tipo D.

### Shift register

![Rappresentazione schematica di uno shift register.][shiftregister]

È costituito da una fila di flip flop di tipo D aventi una linea di clock in
comune. L'ingresso di ciascuno è collegato all'uscita adiacente. Sono disposti
dal bit meno significativo (quello più vicino alla sorgente di dati seriali) al
bit più significativo (l'ultimo della fila). La linea di clock è attivata
solamente quando il dispositivo che vuole scrivere serialmente è pronto a
farlo.

### Counter

![Rappresentazione schematica di un counter.][counter]

È realizzato con una cascata di jk flip flop, in modo da sincronizzare
opportunamente i toggling di ciascun bit.

### Multiplexer

È un componente in grado di selezionare un canale da fornire in uscita tramite
un sistema di switch digitali.

### Demultiplexer

Un componente che riporta il dato in input in uno degli output a cui è
connesso. Quest'output è selezionato mediante dei selettori digitali.

### Batterie

Sono fonte d'alimentazione per dispositivi portatili. Le caratteristiche importanti sono:

- Tensione nominale: differenza di tensione misurata ai capi
- Capacità: quantità di energia che la batteria può erogare in un dato
  intervallo di tempo (in Ah).

### Fusibili

Componenti progettati per proteggere circuiti da correnti eccessive. Se
correnti troppo elevate attraversano il filo presente all'interno del fusibile,
questo fonde e si crea un circuito aperto. Per poter riutilizzare il circuito è
necessario prima sistemare il problema che ha causato l'assorbimento elevato e
poi rimpiazzare il fusibile con uno nuovo.

[struttura_generalizzata]: 	Figure/struttura_generalizzata.png	{width=500px}
[modello_generalizzato]: 	Figure/modello_generalizzato.png
[digitali_binari]: 		Figure/digitali_binari.png		{width=400px}
[%digitale_vs_analogica]: 	Figure/digitale_vs_analogica.png
[intrinseca_vs_programmata]: 	Figure/intrinseca_vs_programmata.png	{width=400px}
[adattamento]: 			Figure/adattamento.png
[%diagramma_adc]: 		Figure/diagramma_adc.png
[condensatore_carica_circuito]:	Figure/condensatore_carica_circuito.png
[condensatore_carica_grafico]:	Figure/condensatore_carica_grafico.png {width=250px}
[condensatore_scarica_circuito]:Figure/condensatore_scarica_circuito.png
[condensatore_scarica_grafico]:	Figure/condensatore_scarica_grafico.png {width=250px}
[scr]: 				Figure/scr.png				{width=250px}
[scr_esempio]:			Figure/scr_esempio.png			{height=250px}
[triac]:			Figure/triac.png			{height=150px}
[bjt]:				Figure/bjt.png				{height=150px}
[igbt]:				Figure/igbt.png				{width=50px}
[darlington]:			Figure/darlington.png			{height=50px}
[schmitt_invertente]:			Figure/schmitt_invertente.png				{height=180px}
[schmitt_invertente_caratteristica]:	Figure/schmitt_invertente_caratteristica.png		{height=100px}
[schmitt_invertente_out_vs_in]:		Figure/schmitt_invertente_out_vs_in.png			{height=170px}
[schmitt_non_invertente]:		 Figure/schmitt_non_invertente.png			{height=120px}
[schmitt_non_invertente_caratteristica]: Figure/schmitt_non_invertente_caratteristica.png	{height=120px}
[not]:				Figure/not.png				{height=50px}
[and]:				Figure/and.png				{height=50px}
[or]:				Figure/or.png				{height=50px}
[xor]:				Figure/xor.png				{height=150px}
[srlatch]:			Figure/srlatch.png			{height=100px}
[dlatch]:			Figure/dlatch.png			{height=150px}
[jkflipflop]:			Figure/jkflipflop.png			{height=150px}
[jkmasterslave]:		Figure/jkmasterslave.png		{height=150px}
[shiftregister]:		Figure/shiftregister.png		{height=200px}
[counter]:			Figure/counter.png			{height=250px}
