# Lezione 2 --- Microcontrollori

## Strumentazione virtuale realizzata tramite calcolatori general-purpose e schede di interfacciamento AD/DA

Affrontiamo lo sviluppo del blocco di elaborazione con tecniche digitali.
Esistono diversi approcci. Uno di questi è quello di utilizzare, a livello
hardware, dei computer con delle schede d'acquisizione e, a livello software,
LabView per acquisire dati. Questo tipo di applicazione è molto adottato
nell'ambito di ricerca. Per una serie di problematiche non discusse, questa
soluzione non è però ideale per le applicazioni proposte dal corso.

La seconda soluzione è quella che considera i microcontrollori. Questi sono dei
piccoli computer che incorporano tutto ciò che serve per controllare
dispositivi di tipo elettronico: l'interazione avviene direttamente con
hardware di strumentazione. 

### Smart sensors, un'applicazione dei sistemi basati su microprocessori

La disponibilità di tecniche di lavorazione digitale basata su microprocessori
a basso costo ha reso anche possibile la realizzazione di sistemi chiamati
"smart sensors". Questi sono minuscoli elementi che, anzichè interfacciarsi con
un'unica unità d'elaborazione che acquisisce (a valle del front-end) dei
segnali analogici provenienti da diversi sensori (a valle di un AMUX),
incorporano tutta la parte di acquisizione ed una prima parte di elaborazione
del segnale.

Inoltre, anzichè avere un'uscita analogica e dover gestire in seguito
l'acquisizione, la conversione A/D e la compensazione delle non linearità del
sensore per ottenere poi una temperatura accurata (ad esempio), questi
dispositivi incorporano: il sensore, il front-end, la conversione A/D e una
componente di elaborazione del segnale che, tramite una memoria di
calibrazione, effettua la compensazione di tutti gli effetti noti di questo
sensore. 

Perciò, per ottenere il dato, si estrarrà dalla conversione del valore in bit
il valore in temperatura che sarà poi trasmesso al processore dall'interfaccia
di comunicazione digitale. Con questo tipo di tecnologia non esiste alcun filo
che riporta un segnale analogico ma è già tutto digitalizzato.  Per fare ciò,
significa che l'azienda è riuscita a realizzare unità di elaborazione a
bassissimo costo destinate a questa funzione e in grado di effettuare gli step
di elaborazione e trattamenti del segnale nel singolo chip. In questo modo non
si usa più il convertitore all'interno dell'unità di elaborazione ma si usa
semplicemente l'acquisizione del dato digitale attraverso l'interfaccia di
comunicazione (e.g. I2C, SPI ...).

## Struttura di strumentazione basata su microprocessore

È interessante comprendere il principio di funzionamento delle unità di
elaborazione.

![Diagramma di un dispositivo a funzionalità programmata per la realizzazione di strumentazione.\label{fig:disp_strum}][disp_strum]

In questo diagramma sono facilmente riconoscibili dei blocchi funzionali già
messi in evidenza in altri corsi d'informatica, in relazione a com'è fatto un
elaboratore elettronico.

**CPU (Central Processing Unit, Microprocessore)**: questo sistema è il
"cervello" del nostro dispositivo. Esso si comporta da master: la macchina
all'interno del dispositivo che determina le tempistiche e le funzioni che
devono essere eseguite. Tutti gli altri dispositivi, invece, sono slaves:
rispondono alle richieste che vengono generate solo ed esclusivamente,
nell'accezione più semplice, da questa CPU. Per poter funzionare, la CPU ha
bisogno di una serie di elementi attorno. 

Questi elementi facenti parti del diagramma possono essere identificati per
*categoria*:

- *Memorizzazione*: è necessario mantenere l'informazione nel tempo. Vi sono
  diverse tipi di tecnologie per implementare queste funzioni. Nello schema
  sono indicati come "Data store". Questa funzione è importante per due diversi
  aspetti: la memorizzazione dei dati (e.g. a delle variabili temporanee con
  cui fare dei calcoli); la memorizzazione delle istruzioni del programma:
  questo è essenziale dal momento che la CPU richiede poi le istruzioni che
  devono essere eseguite.
- *Comunicazione* (e, più in generale, interazione) verso il mondo esterno: è
  offerta da particolari dispositivi, le periferiche esterne. Alcune di esse
  sono destinate alla comunicazione con utente "umano" (e.g. tastiera, mouse,
  monitor, stampanti) e altre servono per far comunicare più elaboratori
  insieme: le interfacce di comunicazione. 
- Quando l'elaboratore elettronico viene utilizzato per fare strumentazione è
  necessario aggiungere il *digitizer*. Più genericamente esiste una serie di
  periferiche che, anzichè essere progettate per comunicare con l'uomo, sono
  progettate per comunicare con dispositivi elettronici che sono attorno al
  sistema. Quella più importante è l'ADC poichè sarà indispensabile elaborare
  segnali analogici provenienti dall'esterno. La stessa tipologia di circuito
  viene adoperata anche quando si intende utilizzare un normale calcolatore
  general-purpose per fare strumentazione: si aggiunge la scheda che
  rappresenta la periferica di conversione analogico-digitale. 
  
Estremizzando questo concetto nella realizzazione di strumentazione: se fossimo
interessati alla realizzazione di una macchina per il caffè, non avremmo
bisogno di hardware quali tastiera, stampanti (etc...), ma solo dell'hardware
veramente necessario a farla funzionare correttamente. In generale sono
macchine con le quali interagiamo solamente attraverso dei tasti. Tutta
l'interfaccia del sistema di elaborazione complesso non esiste in realtà negli
strumenti.  Realizziamo delle macchine con gli stessi blocchi funzionali ma che
possiedono una selezione predefinita di interfacce che consentono ai
microprocessori di comunicare facilmente con un mondo di segnali elettrici.
Noi studieremo ADC come periferica ma anche le porte digitali di
ingresso/uscita (I/O ports) che consentono, attraverso l'esecuzione di
software, di accendere/spegnere un segnale digitale. Un filo di queste porte
può essere messo a 5V o 0 a seconda del programma, oppure c'è la possibilità
che il programma vada a leggere se su un altro pin è presente LOW o HIGH.
  
## La macchina di Von Neumann

Per riuscire ad usare al meglio queste tecnologie è necessario conoscerle nel
dettaglio e non solo capire come avviene l'esecuzione della singola istruzione,
come funziona il processore, come il processore vada a recuperare le istruzioni
dalla memoria e a scrivere sulle periferiche, ma anche a livello hardware come
le macchine siano in grado di compiere tutto ciò.

Per poter risolvere il problema è necessario scomporre in vari blocchi ed
analizzarli singolarmente. Il quadro sarà chiaro solo alla fine della
trattazione.

Prima è necessario capire come il processore interagisce col mondo esterno per
poi arrivare verso il core e l'unità di elaborazione per eseguire i singoli
passi. Capire il funzionamento dei singoli moduli realizza la comprensione del
sistema complessivo. Nel diagramma precedente, le linee rappresentano delle
comunicazioni di tipo funzionale e logico, dei flussi di informazione. Bisogna
considerare che tutte queste funzioni che devono essere implementate sono
complesse e richiedono un notevole flusso di informazione e una notevole
precision nella gestione delle tempistiche con le quali i flussi sono
trasmessi.  Il processore non deve solo trasmettere e ricevere ma anche fungere
da "direttore d'orchestra" fornendo i tempi con i quali queste operazioni
devono avvenire. I microprocessori nascono con diverse architetture.  Noi ne
vedremo alcune. 

Il punto di partenza che meglio rappresenta il sistema base è l'architettura di
Von Neumann.  In quest'architettura la CPU è considerata master sia per
informazioni che per tempistiche. Esiste un clock di sistema collegato alla CPU
che poi trasmette tutte le informazioni sul tempo alle periferiche, i suoi
slaves. All'esterno, esistono due particolari tipologie di dispositivi con cui
il processore comunica:

1. Memorie per dati e programma.
2. Periferiche per la comunicazione col mondo esterno.

La comunicazione avviene tramite il **sistema dei bus**. La codifica binaria ha
un aspetto negativo: per poter trasmettere un numero significativo di valori
dobbiamo raggruppare molti bit, quindi molti fili. Queste informazioni vengono
trasmesse sui bus, insieme di fili che corrono parallelamente e che trasmettono
queste informazioni. Normalmente esistono tre fasci di fili che collegano CPU
con periferiche esterne: l'*address bus*, il *data bus* e il *control bus*.

1. *Address bus, il bus degli indirizzi (16 bit)*: Ci serve per indicare agli
   altri elementi in ascolto dove deve avvenire l'operazione richiesta dalla
   CPU. Vengono associati degli indirizzi univoci a celle di memoria e
   periferiche ai quali il processore farà riferimento per poterne fare uso.
   Un processore tipico delle nostre applicazioni possiede 16 bit di address
   bus. 16bit = 64k possibili valori rappresentabili. È onodirezionale poichè
   in questo caso semplificato il processore fa da master ed è l'unico che
   effettua chiamate ai dispositivi mediante l'address bus, quindi sarà l'unico
   a porre diversi potenziali su queste linee.
2. *Data bus, il bus dei dati (8-16bit)*: normalmente 8bit è l'architettura
   classica. Bidirezionale perchè trasferisce l'informazione da CPU ad unità
   esterne oppure viceversa (rispettivamente scrittura e lettura). Le
   operazioni effettuate vengono descritte dalla prospettiva della CPU. La
   bidirezionalità non implica che vi siano due generatori di tensione che
   vogliono imporre contemporaneamente il potenziale sullo stesso filo causando
   eventuali cortocircuiti ma si tratta di un'alternanza di monodirezionalità.
   L'hardware utilizzato per garantire la bidirezionalità è necessario
   solamente su questo bus, nei casi più semplici.
3. *Control bus, il bus di controllo (2bit)*: In alcuni casi bidirezionale. Ci
   serve per comunicare il tipo di operazione che s'intende eseguire e quando
   il processore è pronto a farlo.

Quando parliamo di comunicazione dei dati: associare ad un numero una sua
codifica (e.g. ASCII, binario, decimale, esadecimale) è qualcosa che viene
gestita al momento della scrittura del firmware. La cella di memoria non è
stata costruita per immagazzinare fisicamente un carattere o un numero ma solo
dei bit.  È il progettista del firmware ad associare a questi bit un certo
significato che poi dev'essere correttamente interpretato dal processore per
poter realizzare la funzionalità desiderata. 

Il processore ha *visibilità di un numero di celle tanto grande quanto il
numero codificato dall'address bus* (**spazio di indirizzamento del
processore**), *le quali sono in grado di contenere sempre e soltanto un certo
numero di bit pari a quelli del data bus*.  

Il processore non discrimina le celle, non fa nulla se è parte di una
periferica, di una porta o altro. Il processore può anche scrivere su celle che
magari non hanno alcuna funzione o su cui non è associata alcuna periferica.  È
l'interpretazione che fornisce il software, e quindi il processore
nell'esecuzione delle istruzioni, che attribuisce un senso al dato ma per il
resto, fisicamente,  le celle sono tutte uguali. 

### Cicli di lettura e scrittura

I cicli di lettura e di scrittura fanno in modo che il processore inserisca (ad
esempio) le informazioni nella memoria RAM, recuperi informazioni dall'ADC,
recuperi il numero che rappresenta la prossima istruzione che il processore
deve eseguire. Tutto ciò e sulla serie di bit che viene scritta nella memoria.

La comunicazione con il mondo esterno mediata dal processore è dunque
riducibile a queste due sole operazioni: *lettura e scrittura*.

#### I diagrammi di lettura e scrittura

Le operazioni di read e write avvengono con delle tempistiche ben definite.
Queste sono diverse da processore a processore. Sono riportati dei diagrammi di
esempio dei cicli di lettura e scrittura. 

Tipicamente questi diagrammi sono semplificati: non vengono riportati gli stati
logici di ogni filo dei bus ma solamente quando avvengono delle commutazioni
sui bus specifici. In particolare, in riferimento a:

- Address bus: lo stato contemporaneo LOW HIGH iniziale identifica un certo
  stato del bus degli indirizzi. Non ci è dato sapere quale sia lo stato logico
  di ogni filo del bus e non è nemmeno necessario per la comprensione delle due
  operazioni. è un modo per esprimere la presenza di fili in configurazione
  HIGH o LOW.
- Gli incroci e le diramazioni delle linee indicano il momento esatto in cui è
  possibile che avvenga la commutazione dello stato di un bus.
- Data bus: la linea che si trova in una posizione intermedia tra HIGH e LOW
  indica lo stato di alta impedenza di qualunque buffer tri-state (i.e. sia
  periferiche che processore).
- R/W: la doppia linea indica che non importa l'operazione che stava avvenendo
  prima o che avverrà dopo.

Cosa è descritto in questi diagrammi:

1. Il clock fornisce l'informazione che riguarda la base dei tempi. Il
   processore con clock da 20MHz o da 4GHz so che l'intervallo temporale tra
   una commutazione e l'altra del clock avrà tempi diversi. Bisogna tenere a
   mente che il processore è master: se la periferica non è abbastanza veloce
   la CPU non se ne accorge. è necessario che la periferica fornisca le
   informazioni nei tempi descritti in questo diagramma. Per poter sapere
   questi tempi bisogna sempre partire dal clock del processore. Questo clock
   potrebbe anche essere modificato, usando dei quarzi diversi, nei propri
   range di operabilità.
2. Address bus
3. R/W
4. Strobe
5. Data bus

**Ciclo di lettura**

![Rappresentazione dello stato dei bus durante il ciclo di lettura.\label{fig:lettura}][lettura]

Quando inizia il ciclo di lettura, il R/W diventa HIGH. Dal momento della
caduta del clock in corrispondenza dell'inizio del read cycle alla caduta del
clock successiva il R/W rimane alto. Simultaneamente all'alzata del R/W abbiamo
che l'address bus ha commutato (A0-A15: lì ogni filo ha assunto il valore
specifico che rappresenta un indirizzo). Notiamo poi che lo strobe viene
abbassato con una certa latenza rispetto all'alzata R/W. Questo perchè lo
strobe dev'essere l'ultimo ad abbassarsi perchè è la linea di accertamento che
sia tutto "a posto". Il processore, il master, che ha cablato le tempistiche
che *devono essere rispettate dalle periferiche*, dopo un po' di tempo si
aspetta che le linee D0-D15 assumano un valore. Quando si abbassa lo strobe e
il processore ha mandato un indirizzo sull'address bus, il processore si
aspetta che la periferica scriva il suo valore sui fili del data bus. Il
processore sa che, in quella fase, può andare a leggere i bit sul data bus
attivando un buffer che vada a registrare queste informazioni (ad esempio un
latch). A questo punto lo strobe torna HIGH e vogliamo che anche la periferica
che sta scrivendo il numero rilasci il bus, rimettendo le sue uscite in
configurazione ad alta impedenza. Ciò non avviene esattamente quando lo strobe
diventa HIGH perchè la periferica ha dei circuiti che, per elaborare il
cambiamento dello strobe, hanno bisogno di un tempo minimo. Le operazioni che
hanno bisogno tempo sono l'AND interno della periferica e la commutazione di
ogni buffer tri-state a questa associato.

**Ciclo di scrittura**

![Rappresentazione dello stato dei bus durante il ciclo di scrittura.\label{fig:scrittura}][scrittura]

È compiuto in più passi. Il clock è utilizzato per scandire le fasi iniziali in
due sottofasi.

Dopo la prima commutazione utile per il ciclo di write, viene scritto il valore
dell'indirizzo sull'address bus.

Dopo mezzo periodo di clock, il R/W viene messo LOW dal processore.

Lo strobe scatta un semiperiodo dopo, esattamente quando il processore va a
scrivere il dato sul bus dei dati.

Si mantiene la configurazione per un ciclo di clock, dopodichè lo strobe torna
HIGH ma il dato rimane sul bus per un po' di tempo. Questo perchè quando il
processore invia il segnale di stop, ora che la periferica lo riceve,
intercorre del tempo. Se il processore simultaneamente al comando di stop
variasse lo stato dei bit, andrebbe a variare il valore in lettura alla
periferica stessa prima di rendersi conto che era necessario fermarsi. è
necessario dunque considerare un margine temporale di sicurezza in cui il
processore mantiene il dato sul bus e con il quale ci si assicura che la
periferica memorizzi il valore corretto. Il dato rimane sul bus per mezzo
periodo di clock, alla fine del quale commuta lo stato di R/W non è più
mantenuto LOW ma può cambiare. Concluso il periodo di clock il ciclo di
scrittura è realizzato.

### Interfacciamento con periferiche: i bus di comunicazioni

Come implementare i vari blocchi:

- Address bus: Dove il processore opera.
- Data bus: Contiene il dato letto o da scrivere.
- Control bus: Informazioni su tempistiche e operazione da effettuare.  ha due
  fili ma dipende dal tipo di processore che si utilizza.  Questo offre più
  spazio per la creatività e si può decidere come organizzare il modo in cui il
  processore dice alle celle cosa vuole fare. La lettura/scrittura è gestita da
  1 bit ma vogliamo anche definire le tempistiche del funzionamento. Uno degli
  aspetti negativi dei segnali digitali era proprio il fatto della diversa
  tempistica di commutazione degli stati d'uscita dei vari fili (capacità
  intrinseca diversa). Bisognerà quindi aspettare un certo istante temporale
  prima di poter interpretare il dato scritto/letto. Quindi c'è anche da
  implementare un bit che stabilisce il tempo. Questi sono i due bit minimi del
  control bus. R/W, Strobe (attivo basso). È possibile anche avere due bit per
  le due diverse operazioni: un filo per la read e uno per la write.
  L'indipendenza dei due fili permette di attivarli solo quando il tempo per
  effettuare tale operazione è stato raggiunto.  
  
#### Address decoder, indirizzare periferiche con diverso numero di celle
  
Ritornando a R/W e strobe, esiste un problema da risolvere però: il numero di
celle delle periferiche è diverso a seconda del tipo di periferica che si
utilizza. Devo fare in modo che i dispositivi singoli che vedono un
sottoinsieme dello spazio d'indirizzamento vengano attivati solo e soltanto
quando il processore vuole scriverle/leggerle. È necessario avere un hardware
esterno che gestisca l'indirizzamento delle celle in questo spazio complesso.
Questa funzione è realizzata dall'address decoder. Un processore sarà collegato
ad un certo numero di elementi che hanno un certo spazio di indirizzamento più
piccolo dello spazio d'indirizzamento del processore, altrimenti (nel caso in
cui lo spazio d'indirizzamento della periferica sia coincidente con quello del
processore) non si riuscirebbe ad avere spazio a sufficienza per gestire altro
che quella periferica. 
  
Facciamo un esempio. La periferica riportata in figura possiede tre linee per
gli indirizzi. Queste permettono l'indicizzazione di un totale di otto celle
interne ad essa. Questo significa che la periferica interna ha 8 celle di
memoria. Per poterla utilizzare è necessario posizionarla all'interno dello
spazio di indirizzamento del processore (assegnare un indirizzo a 16 bit alle 8
celle di questo oggetto). La periferica ha il delle linee address che servono
per accedere alle celle al suo interno.  L'address decoder (o circuito di
decodifica degli indirizzi) va a prendere i fili dell'address bus che non
possono essere gestiti internamente dalla periferica.  Quest'address decoder
decide se il numero grande che dice in quale parte della memoria sto operando
sta rappresentando o meno lo spazio in cui ho messo la periferica.  L'address
decoder quindi stabilisce se il processore sta chiamando lo spazio riservato
alla periferica per cui è stato costruito. Questo dispositivo è l'oggetto che
serve per assegnare l'indirizzo di memoria dello spazio d'indirizzamento del
processore. è l'unico hardware che dobbiamo progettare ogni volta che vogliamo
fare un dispositivo di elaborazione basato su microprocessore perchè quando
facciamo questo dispositivo decidiamo le periferiche che vogliamo inserire.
Ognuna di queste periferiche avrà un suo numero di linee d'indirizzo e per
stabilire dove inserirle nella memoria fisica prima creo una mappa della
memoria e poi la implemento fisicamente facendo in modo che tali dispositivi
rispondano a quel particolare indirizzo fisico.

1. **Mappa della memoria**: vogliamo fare in modo che il nostro dispositivo
   includa una serie di periferiche e che queste vengano assegnate allo spazio
   della memoria. La mappa della memoria viene rappresentata in questo MODO. Un
   rettangolo con alla destra gli indirizzi che rappresentano il numero della
   cella. Gli indirizzi sono espressi in esadecimale perchè vi è una
   corrispondenza tra una cifra e quattro bit in rappresentazione binaria. è
   facile da scrivere come codifica quando vogliamo fare un riferimento diretto
   al codice binario sottostante: ogni singola cifra dell'esadecimale è
   rappresentativa di un numero finito di bit (4 bit). Col decimale non era
   così perchè se volessi rappresentare i numeri che vanno da 0 a 7 li devo
   rappresentare con una cifra ma questa va anche oltre. La conversione tra
   binario ed esadecimale è molto semplice. Gli indirizzi riportati possiedono
   quattro cifre esadecimali per un totale di 16 bit, esattamente quelli
   dell'address bus. L'operazione di "piazzamento" delle periferiche nello
   spazio d'indirizzamento che ha dimensione 0xFFFF. Andiamo a considerare il
   primo modulo: Peripheral hardware registers. Gli "scaffali" di questa
   periferica sono 0x0080, quindi rappresento l'altezza di questo blocco in
   relazione al numero di scaffali di tale periferica. Si passa alla seconda
   periferica da inserire, la RAM. Il progettista può decidere di collocarla
   adiacente alla prima oppure in altri spazi in memoria. Il punto in memoria
   in cui si collocano i dispositivi è irrilevante per il processore, basti
   rispettare le zone di memoria eventualmente riservate a funzioni particolari
   e stare attento a non creare delle sovrapposizioni tra le aree di memoria
   dei dispositivi. È necessario che ad un indirizzo solo una periferica
   risponda.
2. **Progettazione dell'hardware**. Le periferiche sono già state realizzate,
   basta comprarle. Quello che devo progettare è l'address decoder che fa in
   modo che una periferica che abbia 128 byte di memoria risponda solo quando
   il processore scrive numeri che vanno da 0x0080 a 0x0100. Per il resto la
   periferica funziona con le proprie celle adiacenti tra di loro. Dobbiamo
   solo stabilire quando l'indirizzo che sta chiamando il processore
   rappresenta la cifra che noi abbiamo attribuito a questa periferica. Per
   poter fare quest'operazione ci riferiamo al CIRCUITO mostrato. Questo
   circuito rappresenta dal punto di vista fisico il funzionamento del sistema.
   La periferica che si vuole utilizzare ha una serie (sue) linee di indirizzi
   d'ingresso, la cui codifica è corrispondente al numero di "scaffali"
   presenti al suo interno. Essendo 10 le linee d'indirizzo in ingresso vi
   saranno 1024 "scaffali" al suo interno. Questi A0-A9 vengono presi
   considerando i numeri meno significativi dell'address bus del processore.
   Noto che il numero di fili che saranno utilizzati dalla periferica è
   inferiore a 16, il numero complessivo dei fili dell'address bus del
   processore. I bit meno significativi indicano l'informazione relativa a
   quale scaffale è d'interesse per il processore. Essendo lo spazio
   d'indirizzamento della periferica limitato ai soli 10 bit meno
   significativi, questa non può riconoscere da sola se il processore vuole
   operare o meno con gli scaffali che la riguardano. Per poter riconoscere se
   il processore sta chiamando lei è necessario, per il progettista, realizzare
   l'address decoder che controlla il contenuto dei fili relativi agli
   indirizzi più significativi che non sono input dalla periferica stessa. Il
   progettista quindi gestisce la differenza di dimensione. La dimensione più
   piccola è invece gestita dalla periferica. È necessario solo *controllare
   quando il numero a 16 bit che il processore sta scrivendo sull'address bus
   rientra nello spazio che il progettista aveva allocato per la periferica
   d'interesse*. Se la periferica dell'esempio fosse posizionata alla fine
   dello spazio d'indirizzamento del processore (considerando la fine a 0xFFFF)
   sarà necessario realizzare un circuito che controlli quando i 6 bit più
   significativi assumono valore HIGH. Anche solo uno dei bit più significativi
   a LOW avrebbe come effetto per la periferica quello di non essere chiamata.
   **Com'è fatto il circuito di decodifica degli indirizzi?** Tutte le
   periferiche hanno un circuito di chip select (CS) che abilita un enable,
   necessario ad attivare il circuito della periferica. Questo chip select è
   fatto apposta per essere collegato alla decodificadegli indirizzi. Devo fare
   in modo che il CS attivi la periferica solo e soltanto quando il processore
   voglia operare con gli scaffali che si trovano all'interno della periferica.
   Quindi a monte del CS si mette una porta AND di modo che quando vi saranno
   tutti HIGH sugli ingressi (sempre in relazione all'esempio) dà un comando
   alla periferica solo quando gli ingressi sono come ci si aspetta dal
   progetto. INSERIRE DIAGRAMMA. è necessario quindi fornire in uscita un
   segnale che abiliti la periferica quando la codifica dei 6 bit di indirizzo
   più significativo è uguale a quella che il progettista ha definito nello
   spazio di memoria come appartenente alla periferica in questione.

![Esempio: address decoder per una periferica avente 1024 bytes.\label{fig:spazio_indirizzamento_processore}][spazio_indirizzamento_processore]

*NB*: più il numero affianco al bit è grande e più questo è significativo.

Il gioco sta nell'inserire i NOT negli ingressi dell'address decoder (i.e.
dell'AND) in modo che la codifica che voglio ottenere generi in uscita il
segnale che abilita la periferica. Se ad esempio voglio attivare la periferica
a partire dalla cella 0x0000 basterà che i 6 bit più significativi siano tutti
negati, così l'AND commuta uscita. Il motivo per cui si utilizza questo address
decoder è per rendere le periferiche CPU agnostiche.

Si verifica ora un altro problema: le periferiche non dispongono dello stesso
numero di scaffali, quindi i circuiti di decodifica dovranno gestire un numero
di fili diversi a seconda della tipologia di periferica.

Esistono anche dei metodi più semplici per risparmiare sulle grandi porte AND
che si avrebbero nel caso si utilizzassero molte periferiche con pochi
"scaffali".

Le informazioni relative al progetto della mappa della memoria, che risultano
in tutti i circuiti di decodifica, non interessano al processore di per se
stesso ma a chi dovrà progettare il firmware per operare con la macchina
stessa.

##### La decodifica non completa

Abbiamo un microprocessore (MPU: micro processing unit). Quest'oggetto ha una
comunicazione col mondo esterno costituita dai bus dati, indirizzi e controllo.

- Guardando il diagramma e osservando il D[7:0] e una linea che va a tutte le
  periferiche: bus dei dati. è un tipico processore.
- A[15:0]: bus degli indirizzi. Abbiamo 16 bit.
- ROM da 8k, RAM da 8k e porta seriale. In dispostivi reali potremmo ritrovarci
  come in questo caso in cui il numero di dispositivi che dobbiamo collegare è
  estremamente limitato. In questo caso quest'elaboratore prende e restituisce
  informazioni mediante seriale. Le celle che servono a far funzionare la porta
  seriale sono 16 perchè gli indirizzi interni della seriale vanno da A[0] ad
  A[3]. L'utilizzo complessivo della memoria è poco più di 16k e, ricordando
  che lo spazio d'indirizzamento del processore è di 64k celle, è possibile
  indicizzare correttamente le risorse. Questa è la situazione classica che
  capita.

Nel caso della codifica completa è necessario fare una mappa della memoria
indicando dove collocare i dispositivi e realizzare poi un circuito di
decodifica degli indirizzi per ognuna delle periferiche. L'address decoder per
ROM e RAM è simile: una porta AND con 3 ingressi. Per quanto riguarda la porta
seriale invece c'è bisogno di una porta AND a 12 ingressi. Questo è il modo per
fare una perfetta decodifica completa. Questa decodifica è tipica dei
dispositivi che sono destinati a poter essere ampliati in futuro.

Esiste un secondo approccio, quello dei dispositivi embedded. Questa tipologia
vede il dispositivo come non espandibile dal punto di vista hardware in futuro:
la macchina del caffè non andrà a minare bitcoin. Progettato l'hardware per il
dispositivo abbiamo finito così. Volendo noi risparmiare anche soldi nella
produzione della macchina, evito di usare degli AND giganteschi e uso sistemi
come quello in figura. L'address decoder non è più uno per dispositivo e non
usa tutti i fili che dovrebbe. Utilizza solo i due bit più significativi
A[15:14] e poi esce con 3 CS (chip select). In questo modo la decodifica non è
completa.

Le fasi per la decodifica non completa sono le seguenti:

1. Stabilire il numero di periferiche da utilizzare e scelgo il numero di bit
   più significativi da utilizzare in modo tale da indicizzarle tutte. Nel
   nostro caso le periferiche da utilizzare sono tre e si necessitano due bit.
2. Suddividere lo spazio d'indirizzamento del processore in tante parti uguali
   quante le potenziali combinazioni utilizzando il numero di bit del punto 1.
   Le combinazioni possibili sono $2^2=4$.
3. Allocare le periferiche in diversi slot considerando che la prima cella di
   ciascuna avrà i bit in ingresso alla periferica a LOW.

![Esempio di decodifica non completa.\label{fig:decodifica_non_completa}][decodifica_non_completa]

Ciascuna partizione ha associata una codifica dei bit più significativi che
quando saranno in ingresso all'address decoding logic, abiliteranno la linea di
enable corrispondente alla periferica verso la quale il processore vuole
operare. È da notare che esistono dei bit inutilizzati dalla codifica. In
questo esempio A[13] non è utilizzato nell'indicizzazione dei bytes della ROM e
della RAM e A[13:4] della porta seriale. Ciò significa che, qualsiasi sia il
valore di questi al momento di un'operazione con le rispettive periferiche, i
bytes letti o scritti saranno sempre i medesimi. Questo si ripercuote nello
spazio d'indirizzamento del processore con un "mirroring" dei bytes della
periferica in tutta la partizione dedicata ad essa. Per risolvere tale problema
si deve specificare quali sono gli indirizzi a cui accedere e quali sono quelli
riservati.

Risparmiare sull'hardware significa risparmiare spazio sul dispositivo,
risparmiare costi avendo meno dispositivi all'interno, risparmiare consumo.

### Buffer tri-state, la bidirezionalità dell'informazione

In primo luogo devo fare in modo che vi siano più elementi che vadano a
scrivere sullo stesso filo, poi come fare in modo che il processore o una
periferica possa una volta essere un writer e poi un reader. Per gestire
un'alternanza d'informazione avremo bisogno della logica del buffer tri-state.

Dobbiamo ricordarci che è facile leggere da parte di tanti lo stato di un filo.
Se ho un filo posso avere tanti ingressi collegati ad esso. Il limite fisico
alla generazione di tensione è che si può avere solo un generatore, un solo
output sul filo. Per riuscire a controllare in modo adeguato quest'attivazione
da parte di diverse periferiche o del processore di un'uscita si utilizzano
questi buffer tri-state, circuiti digitali dotati di un ingresso in più,
chiamato output enable e legato all'abilitazione dello stato d'uscita in
configurazione "alta impedenza" (aka stato "Z"). Con un buffer tri-state è
possibile trasmettere in uscita HIGH o LOW solo se l'interruttore è abilitato,
altrimenti la linea d'uscita è scollegata. Lo stato ad alta impedenza apre
l'interruttore.  Impdenza alta significa che si dispone di un dispositivo che
permette di avere un generatore di tensione, buffer, ma di poter attaccare il
generatore al filo ed esercitare l'attribuzione al filo del potenziale che deve
essere generato oppure scollegarlo. Scollegando il filo il generatore non è più
collegato e ciò consente di abilitare sempre e solo uno solo di questi buffer
che comandano una linea comune. Ad esempio tutti i buffer che siano collegati a
D0, ogni volta che accade qualcosa al massimo uno solo dei buffer collegati
alla linea è attivo e gli altri no.

Nell'esempio di prima l'operazione richiesta dal processore è Read, quindi la
periferica deve scrivere, per cui l'AND interna alla periferica serve per
abilitare le linee d'uscita alla periferica.

### Le memorie

Le definizioni di queste memorie sono nate molto tempo fa.
La differenza tra le due è nella tecnologia

- ROM (Read Only Memory): Una memoria che non può essere scritta. In realtà
  questo serve perchè si rendeva il contenuto indelebile, indipendentemente
  dallo stato del dispositivo. Questo tipo di memoria è persistente. Pensando
  alla SD card questa è permanente ma può essere scritta, quindi la definizione
  è arcaica. Esistono vari tipi di memorie ROM:

	- Fuse ROM: matrice fatta con fili sottili. Il principio su cui si basa
	  l'archiviazione su questa tipologia di memorie prende spunto dai
	  fusibili. I fusibili sono dei dispositivi di protezione. Questi hanno
	  all'interno dei fili sottili che fanno passare una certa corrente. Se
	  la corrente supera una soglia, il fusibile si brucia e non passa più
	  corrente. Questo tipo di componente serve per proteggere le macchine.
	  Questi fusibili poi possono essere cambiati per ripristinare il
	  circuito. Questa tecnologia è usata per creare l'informazione
	  all'interno della memoria. Quando volevo scrivere l'informazione,
	  facevo passare molta corrente nei bit in cui volevo codificare
	  l'informazione LOW. Programmata la memoria rimane così 
	- Masked ROM: creo circuiti che nasce con un silicio che contiene già
	  l'informazione che voglio contenga. Queste memorie hanno un ampio
	  costo. Ha l'inconveniente che se volessi cambiare un solo bit dovrei
	  riprogettare tutto il circuito.
	- EPROM: La finestrella a loro disposizione era per utilizzare
	  radiazione ultravioletta per modificare lo stato di carica del
	  dispositivo. L'informazione rimane scritta finche non espongo a raggi
	  UV. Questa è stata un primo tentativo di memoria persistente
	  riscrivibile.
	- EEPROM - FLASH: la differenza sta nella tecnologia di cancellazione.
	  Non c'è più bisogno dei raggi UV ma di un processo di tipo elettrico.

- RAM (Random Access Memory): Volatilizza quando il dispositivo è spento. Una
  volta le informazioni digitali erano registrate su nastri magnetici che si
  muovevano in avanti e all'indietro. Il computer scriveva sul nastro magnetico
  in un certo modo e poi si spostava per andare ad utilizzare il nastro per
  un'altra cella. Le letture quindi erano sequenziali. Il passo successivo è
  stato quello di realizzare delle memorie il cui contenuto potesse essere
  letto senza svolgere il nastro. RAM statiche: dei flip-flop, matrici in cui
  ogni bit è un flip-flop. La stabilità dei dispositivi è data dal fatto che vi
  siano dei bistabili. Togliendo l'alimentazione tolgo lo stato noto e quindi
  perdo quell'informazione. Il flip-flop fisicamente è fatto da alcune
  componenti elettroniche, quindi bisogna allocare fisicamente dello spazio per
  i flip-flop. Quando ho bisogno di molta memoria dobbiamo fare in modo di fare
  delle celle piccole. Quindi oltre alle ram statiche vi saranno anche RAM
  dinamiche. i nostri dispositivi hanno 256 Bytes di ram. RAM dinamica:
  tecnologia sviluppata per garantire la miniaturizzazione dei circuiti per
  tenere molta informazione. Semplificando la cella singola (usando
  condensatori) si può complicare a piacere i circuiti di servizio. Per fare in
  modo che il condensatore mantenga la carica uso dei circuiti che misurano lo
  stato del condensatore regolarmente e ristabiliscano la tensione HIGH nel
  caso sia carico. Questi circuiti sono detti di refresh e vanno spesso a
  leggere e scrivere i valori. Il dinamismo è dovuto proprio a quest'azione di
  refresh.

Usiamo due tipi di memoria perchè la CPU per eseguire un programma deve sapere
quali sono le istruzioni da eseguire. Queste istruzioni sono memorizzate da
qualche parte e vorrebbe trovare queste informazioni. Ho sempre bisogno di un
po' di ROM per sapere le istruzioni. La RAM la posso usare nel modo più
flessibile possibile perchè posso metterci dentro programmi e dati. Word viene
caricato in RAM per essere utilizzato. Bisogna sempre avere della memoria FLASH
per far partire il BIOS, un programma che si trova in questa memoria FLASH. Nei
nostri sistemi normalmente le proporzioni RAM/ROM sono diverse e tipicamente si
usa più ROM che RAM perchè la seconda si usa quasi esclusivamente per i dati.

### Flip-flop di tipo D e latch di tipo D, le differenze

Per costruire un latch di tipo D si parte da un S-R latch, costituito da due
porte NOR opportunamente connesse tra loro.

Questo latch subisce una modifica agli input andando a considerare il set come
il NOT del reset. Un'ulteriore modifica considera un'ulteriore linea di input,
l'enable. In questo modo è possibile settare e resettare solamente quando la
linea di enable è HIGH.

Il flip-flop, a differenza del latch ha, sulla linea di enable un edge
detector, in grado di rilevare il rising edge del clock fornito al circuito di
prima. In questo modo solo a fronte di rising edge è possibile cambiare il bit
memorizzato.

## Il Microprocessore, uno sguardo all'interno

Il numero di blocchi funzionali che lo costituiscono è veramente limitato.

1. Unità logico aritmetica (ALU). è la macchina che fa i calcoli all'interno
   del processore. Una calcolatrice non molto complessa. Fanno somme con segno,
   shift destri e sinistri di bit. Consente di fare anche dei test logici e di
   vedere se sono soddisfatte certe condizioni. Ad esempio il risultato
   dell'operazione è zero o diverso da zero?
2. *Instruction fetch and decode*: Instruction fetch: il processore deve
   eseguire programmi ossia sequenze di istruzioni. L'operazione di fetch di un
   microprocessore consente di acquisire dalla memoria esterna l'istruzione,
   ossia il numero che rappresenta ciò che il processore deve compiere come
   prossima istruzione. Il processore interpreta una serie di bit come un
   istruzione. Questo numero non è diverso dagli altri presenti nei vari
   scaffali. Quando si scrive il programma si fa in modo che l'accesso ad una
   cella serva per scrivere o leggere le informazioni di un dato mentre faccio
   in modo che altre celle siano destinate a contenere quei numeri che il
   processore va a leggere come istruzioni. Blocco di circuiti che si occupa di
   fare una read ad un indirizzo in cui il processore si aspetta che il numero
   che va a leggere rappresenti un'istruzione. Il decode è la fase successiva:
   il processore prende il numero e lo trasforma nelle commutazioni di circuiti
   che servono per eseguire l'istruzione che è stata caricata.
3. *Accumulatore*: registro destinato alla gestione dei dati. Ce ne possono
   essere più di uno.
4. *Program counter*: registro che ricorda qual è la prossima istruzione che il
   processore deve eseguire. Se il mondo del processore è costituito da una
   serie di "scaffali", come fa la CPU a seguire il programma? Il processore
   tiene in memoria il numero che rappresenta qual è la cella del suo spazio di
   indirizzamento che contiene la prossima istruzione che il processore deve
   compiere. Questa è un'informazione da address bus, costituisce un indirizzo
   la cui cella dev'essere letta dal processore. Quest'indirizzo viene messo in
   quello che viene chiamato program counter. Il program counter viene
   aumentato ogni volta che il processore esegue un'istruzione. Ciò consente di
   eseguire una dopo l'altra le istruzioni. Perchè non utilizzare un normale
   contatore? Quest'oggetto deve anche tenere conto del fatto che, in funzione
   del risultato di alcune istruzioni, il contenuto del program counter può
   essere cambiato. Si consideri quest'esempio: un ciclo for. Il processore ad
   un certo punto entrerà in una serie d'istruzioni che viene eseguita una
   volta, poi si fa un test e a seconda che il test passi oppure fallisca non
   devo andare avanti ma devo tornare alla prima istruzione di questo blocco.
   Per fare in modo che il processore faccia un'operazione di questo tipo, il
   program counter sarà caricato con l'indirizzo della prima delle istruzioni
   del ciclo for.
   
Quindi program counter e accumulatore sono dei semplici registri. L'elettronica
che gli sta intorno e che gli permette di svolgere l'una o l'altra funzione li
rende particolarmente speciali. 

### Registri e latch

![Rappresentazione di un registro come insieme di latch. Da notare come le uscite siano controllate mediante buffer tri-state\label{fig:registri}][registri]

Elementi di memoria costituiti da un certo numero di flip flop di tipo D tanti
quanti sono i bit che costituiscono questo registro. Se considerassimo un
processore con un bus dati da 8bit, l'accumulatore sarà costituito da 8 flip
flop di tipo D. Questi flip flop hanno in comunione la linea di clock.  Il
processore utilizza questi flip flop per mantenere le informazioni che servono
per eseguire le istruzioni. Alcuni di questi registri sono dedicati al dato
(fare operazioni etc.), altri servono per gestire il fluire del programma.

Sono composti da 8 latch: 8 fili in ingresso ad 8 flip flop di tipo D, un unica
linea di clock. Quando si ha il colpo di clock, i bit che sono scritti
all'ingresso dei flip flop vengono memorizzati in essi e se venissero
modificati i valori delle tensioni in ingresso ai flip flop, l'uscita rimane
uguale a quella che il flip flop ha assunto al momento in cui è stato dato
l'ultimo colpo di clock.

### Primo processore: ST7

Si consideri ST7. Lo SCHEMA viene riportato.

A sinistra, si può notare come è stato realizzato il microprocessore che sta
all'interno dell'ST7.  A destra si vedono i bus dei dati e indirizzi. Dobbiamo
capire solo come funzionano le cose a sinistra, a partire dal blocco con
scritto MAR, andando verso il basso.

- C'è la ALU. Questa, dato che deve fare conti, ha bisogno delle linee che
  portino dentro gli operandi e dei fili che portino verso l'esterno il
  risultato dell'operazione. Per fare somme o sottrazioni abbiamo bisogno di
  almeno due operandi (A e B). Gli ingressi degli operandi sono rappresentati
  con delle linee blu. Lo standard della rappresentazione della ALU la
  rappresenta a forma di Y. Questa calcolatrice avrà anche delle linee digitali
  in ingresso che le dicono quale operazione fare. Se ce ne fossero solo 4
  possibili avrei bisogno di soli 2 fili.

Come faccio ora ad avere i due operandi messi nei latch della ALU e come
gestisco il fatto di avere una macchina che, eseguendo un programma, compie dei
calcoli. Questa macchina, esclusivamente all'interno, anzichè avere un unico
bus dati e un bus degli indirizzi si utilizzano dei registri che sono noti al
processore e non c'è bisogno di indirizzarli perchè vengono utilizzate per
implementare le funzioni. Dentro il processore vi sono tanti fasci di fili e,
tipicamente, ciò che si muove sono dati all'interno del processore, quindi i
fili rappresentati in blu sono tanti quanti i bit del bus dati. Quindi se
utilizzo un processore a 8bit, tipicamente queste linee blu sono rappresentate
da 8 fili. Anche quando noi parliamo di un processore a 64bit ci riferiamo al
parallelismo con in quale il processore è in grado di effettuare dei calcoli,
quindi fisicamente rappresenta il numero dei bit del bus dati (aka larghezza
del bus dati). Questa larghezza corrisponde alla larghezza dei registri,
dell'accumulatore etc. Ogni volta che faccio un operazione la faccio
coinvolgendo quel numero di bit. L'address bus può avere diversa larghezza ma
questa rappresenta solo la quantità di scaffali che possiamo avere. Un
processore a 64 bit compie un'operazione su 64bit. Un calcolo del genere con un
processore a 8bit è possibile ma sarebbe necessario più tempo per raggiungere
il risultato.

La larghezza del bus dati caratterizza i flussi d'informazione mostrato in blu.
I registri sono dei latch. Andiamo a visionare cosa è presente in questi
registri (la stessa cosa è presente in AX, BX, CX, DX). I fili che entrano in
questi registri sono gli stessi che escono dalla ALU e che poi entrano nel bus
dati. Le uscite dei flip flop (dei latch di ogni registro) saranno poi
collegati al bus dei dati ma non direttamente perchè si rischierebbero dei
corti, bisogna frapporre dei buffer tri-state all'uscita di ogni cella del
registro. Questo buffer tri-state disaccoppia le uscite e fa in modo che queste
siano attivate solo quando necessario. La linea di comando dei buffer tri-state
è comune a tutti i bit del registro perchè noi vogliamo leggerlo o scriverlo
completamente. S e T sono altri due latch.

Perchè con questi elementi il processore riesce a compiere diverse operazioni?
Facciamo degli esempi. 

1. Test logico: il contenuto del registro AX è uguale a zero. Il processore
   deve attivare con la giusta sequenza i comandi sui fili opportuni. In
   particolare bisogna fare in modo che questo numero in AX raggiunga l'ALU.
   Nel fascio X, inizialmente, nessuno scrive e tutti i tri-state sono aperti
   (alta impedenza). Per vedere il contenuto di AX su X bisogna attivare il
   comando dei buffer tri-state di AX. In questo modo l'informazione che c'è in
   AX viene trasferita ai fasci di fili X. Quindi ora il fascio X rappresenta
   il numero dentro in AX. Il processore deve dare ora un comando di latch a T
   (clock a T). Quest'ultima operazione fa in modo di trasferire l'informazione
   che era presente su X alla ALU. La ALU riceve dal dato i comandi per fare
   questo test, esegue il test e questo viene riproposto in uscita della ALU.
2. Astrazione più complessa: si vuole mettere in AX il risultato della somma
   tra AX e BX. Ci sono quindi due operandi, devo farne la somma e poi scrivere
   il risultato da una parte. Come faccio con questi circuiti a ottenere il
   risultato? Come prima cosa devo far arrivare che sia il contenuto di AX che
   il contenuto di BX arrivi alla ALU. Alla ALU però arriva un fascio solo,
   quindi per far arrivare i due numeri avrò due fasi diverse. Per prima cosa
   attivo il buffer tri-state di AX, il dato arriva in ingresso sia di S che di
   T. Dò un colpo di clock a T, a questo punto il dato che ho in AX viene
   presentato sul lato destro della ALU e rimarrà stabile. Disabilito ora
   l'uscita del tri-state di AX. Abilito le uscite di BX che sarà in grado di
   scrivere i valori di potenziali dei fili X. Quando ciò è avvenuto si attiva
   il clock di S. Il contenuto di BX si ritrova a essere nel secondo ingresso
   della ALU. In questo modo la ALU ha i due operandi pronti per essere
   trattati. Diamo con i fili laterali il comando di fare la somma e, sui fili
   che escono dalla ALU abilitiamo la scrittura sull'uscita tramite buffer
   tri-state. Il fascio Y porta ora il risultato dell'operazione appena
   eseguita. Ora devo mettere il risultato in AX e, essendo questo in Y, basta
   fornire un colpo di clock ad AX per scrivere tutto nel latch. AX ora avrà
   come contenuto la somma di AX e BX.

Si può notare che X e Y, oltre ad essere collegati alle uscite e agli ingressi
dei registri, sono connesse anche al data bus. Questo consente di includere
nelle operazioni anche le periferiche, al prezzo di gestire adeguatamente i
cicli di lettura e scrittura del dato.

1. Si vuole fare la somma di AX con il contenuto della cella 2726.
   Quest'operazione quindi non coinvolge solo dei numeri all'interno dei
   registri del processore ma con dei valori che si trovano fuori. Per AX:
   attiviamo il buffer tri-state di AX, andiamo a fare il latch in T. Per
   andare a prendere il dato da una memoria o una periferica? Sarà necessario
   fare una read scrivendo nella linea del bus di indirizzi il valore 2726,
   viene attivata la read e lo strobe va basso. Quando il dato è disponibile
   sul data bus. Dando un colpo di clock a S, riporto il dato come secondo
   operando della ALU. Togliendo lo strobe e dopo aver atteso un certo periodo
   di tempo, le linee di bus dati tornano ad essere ad alta impedenza. L'ALU fa
   la somma, il risultato poi verrà messo in AX.
4. Operazione di scrittura. Voglio far uscire AX sulla cella 122. L'istruzione
   è write alla 122 il contenuto di AX. Per implementare l'istruzione basta
   scrivere il valore su X e poi effettuare un'operazione di write sul data bus
   mettendo l'indirizzo giusto sull'address bus.
5. Il risultato dell'ALU possa andare direttamente alla nostra cella di memoria
   senza essere messo su AX posso usare la linea Y. Quando ALU scrive su Y può
   coinvolgere anche il bus dei dati e se in quel momento attivo il ciclo di
   write e il dato lo faccio mettere dalla ALU, ecco che il dato può finire
   direttamente in una periferica o in memoria.

Ogni processore è progettato seguendo particolari filosofie e scelte
progettuali a seconda dell'impiego che se ne dovrà fare. Uno degli impieghi più
interessanti in ambito biomedico è quello dei processori adibiti al digital
signal processing (DSP). Questo possiede un unità ALU che ha anche la
possibilità di fare un'operazione di MAC (moltiplicazione e accumulo del
risultato) che è elemento fondamentale dei modelli ARMA. Questo elemento
hardware rende molto più veloce la computazione di questa tipologia di
risultati.

Ciò che è comune a tutti questi approcci è l'esistenza di ALU, di registri che
sono connessi mediante dei fasci di fili (bus interni) che non vengono più
utilizzati con indirizzi e dati perchè trasmettono solamente il dato come
informazione.

È necessario però soffermarsi sulla gestione dell'indirizzo. Il PC è il program
counter, un registro in cui l'informazione è l'indirizzo dove si va a prendere
la prossima istruzione da eseguire. Esiste un altro elemento in grado di
comunicare direttamente col bus degli indirizzi. A volte infatti gli operandi
della ALU vengono presi dalle celle che si trovano fuori dal processore. Per
accedere ai dati fuori non posso attivare la linea corrispondente ai registri
che voglio leggere ma dovrò avere accesso al mondo esterno utilizzando R/W e
address bus. Quindi quando devo fare un'operazione in cui tra gli operandi ho
anche l'indirizzo di una cella, ho bisogno di avere un registro che mi consenta
di veicolare quest'informazione all'address bus. Il MAR è il registro in cui
metto l'indirizzo della cella che voglio andare a leggere o a scrivere. Il MAR
è un ponte tra i numeri che circolano nel bus dati e l'address bus per poter
consentire di fare un indirizzamento verso una cella che sta all'esterno. 

Da queste considerazione è possibile dedurre che i registri AX, BX, ..., DX,
siccome dovranno contenere dei dati, avranno una larghezza pari al bus dati.
Per quanto riguarda PC e MAR, siccome dovranno contenere degli indirizzi,
dovranno avere larghezza pari a quella dell'address bus.

### Secondo processore: più complesso e sofisticato

Quando si ha in mente di costruire macchine con hanno l'obiettivo di aumentare
le performance anche il processore può essere modificato. Performance più
elevate significa un maggior prezzo da far pagare ma anche un maggior
investimento per la sua produzione.

QUESTA macchina possiede molti registri e questo è comodo perchè il processore
può contenere molte informazioni senza dover utilizzare cicli di read/write e
tempi d'attesa. Al posto di avere un fascio X si hanno due bus: A e B.

Aggiungere un bus a quella linea modifica il sistema visto prima con la
SEGUENTE configurazione. Aggiungendo un bus non sto solo tirando un sacco di
fili in più ma sto anche aggiungendo molti buffer tri-state per gestirlo.

![Rappresentazione dell'interfaccia di un registro con due bus\label{fig:registri_due_bus}][registri_due_bus]

L'informatica teorica ci insegna che se esiste un minimo set di istruzioni che
si possono eseguire, qualunque algoritmo può essere implementato da quel minimo
set d'istruzione. Allora qual è il senso ha fare delle cose in più se qualunque
algoritmo può essere implementato anche dalla macchina vista precedentemente?
La differenza sta effettivamente nel tempo di esecuzione per compiere un dato
calcolo. Una macchina di questo tipo è più potente. Con potenza si intende
implicitamente il numero di operazione, quanti dati è in grado di elaborare in
unità di tempo. L'unità di misura è Mips (Milion of instructions per second).

Se voglio fare un operazione per includere due operandi era necessario fare due
passaggi in sequenza perchè il bus che fa passare il dato era uno solo. Quindi
prima dovevo far passare AX e poi far passare BX sullo stesso bus e metterlo
nell'altro latch. Se il canale è uno e devono passare due dati significa che
devo aspettare il doppio del tempo relativo al passaggio di un dato. Se
utilizzassi una configurazione a due bus e volessi fare un'operazione a due
operandi basta dire: abilito il tri-state di AX e scrivo il contenuto nel bus
A. Abilito il secondo blocco di tri-state di E di modo che vada a scrivere il
numero sul bus B. Questi due arrivano alla ALU e fare direttamente l'operazione
senza questi due passaggi che erano necessari prima. La velocità che ci
s'impiega è molto più elevata, giustificando anche la spesa per utilizzare un
processore così performante.

### La control unit

L'implementazione della singola istruzione avviene attraverso una serie di
comandi generati da una control unit. Questa control uniti deve leggere
l'istruzione (il cui caricamento è eseguita mediante fetch dell'istruzione).
All'interno del processore, i circuiti della control unit traducono
l'istruzione, che è effettivamente un numero, in una sequenza di attivazioni di
buffer tri-state, clock dei flip-flop che costituiscono i vari latch e
registri, impostazioni di multiplexer o comandi digitali necessari
all'impostazione dell'operazione che l'ALU deve eseguire sui dati.

Un esempio di come può essere implementata un'istruzione da parte del
processore:

1. *Fetch dell'istruzione*: il processore recupera l'istruzione dalla memoria.
2. Incremento il PC (program counter): Il processore ha bisogno di conoscere
   dove si trova la prossima istruzione da eseguire. Il Program Counter (PC) è
   il registro che contiene l'informazione relativa all'indirizzo della
   successiva istruzione. Questo registro può avere diversi nomi (aka IP,
   instruction pointer). Questo registro ha un numero di bit pari alla
   larghezza del bus degli indirizzi, contrariamente ai registri che sono
   deputati all'elaborazione dei dati che invece dispongono di un numero di bit
   pari alla larghezza del bus dati. Mentre la comunicazione a bus esterna al
   processore, basata sull'utilizzo di bus dati, degli indirizzi e di controllo
   per garantire l'indirizzamento di singole celle nelle operazioni di lettura
   e scrittura, nel caso delle comunicazioni interne al processore si parla
   effettivamente di fasci di cavi. All'uscita dei vari flip-flop si avranno
   dei buffer tri-state allo scopo di controllare che la scrittura sia
   effettuata da un solo elemento. Si prepara il program counter all'esecuzione
   dell'istruzione successiva. Nelle macchine più semplice l'istruzione è
   codificata da un solo byte. Ciò significa che l'istruzione viene letta
   effettuando una singola operazione di read. La prossima istruzione sarà
   nella cella successiva (che ha perciò un indirizzo immediatamente successivo
   rispetto a quello della cella che è stata caricata). In realtà, alcuni
   processori hanno dei set d'istruzione che considerano istruzioni che hanno
   un numero variabile di bit che le compongono. Questo perchè, alcune
   istruzioni, possono essere caratterizzate dall'incorporare degli operandi
   (e.g. sommo al contenuto di AX un certo numero. Questo genere d'istruzione
   incorpora quel numero). Tali istruzioni possono richiedere il caricamento di
   più byte. Nel momento in cui il control unit carica il primo byte
   comprendendo quale tipo di istruzione è codificata, viene eseguito il
   completamento del caricamento (nel caso in cui il processore abbia la
   possibilità di avere più byte) e alla fine di quest'operazione viene
   comunque incrementato il program counter in modo che punti alla prossima
   istruzione da eseguire. La lettura dell'istruzione che ha portato
   all'incorporare nella control unit questa informazione, viene eseguita e
   quindi è necessario interpretare opportunamente l'istruzione in termini di
   operazioni più semplici.
3. *Decodifica dell'istruzione*: il processore prende il numero dell'istruzione
   e vede cosa deve fare per implementarla. È simile ad avere un libro di
   ricette: ogni sezione contiene una particolare ricetta. Come si può indicare
   qual è il piatto da realizzare? basta dare l'informazione relativa alla
   prima pagina della ricetta che si vuole utilizzare. l'istruzione non fa
   altro se non rappresentare la prima pagina, il punto del libro, che contiene
   tutta la sequenza di operazioni che il processore deve compiere per
   realizzare quella istruzione. Le attività del processore dipendono da quali
   sono le sequenze di queste operazioni che bisogna fare. Alcune di queste
   operazioni possono richiedere di modificare il flusso dell'esecuzione delle
   istruzioni. Se aumentassimo in modo incrementale l'IP, significa che
   staremmo dando una lista puramente sequenziale di istruzioni. Cosa viene
   fatta prima e cosa dopo viene determinato esclusivamente dalla posizione di
   quest'istruzione all'interno della memoria del programma del nostro
   processore. Un programma, per poter implementare qualunque algoritmo, deve
   avere la possibilità di modificare un flusso. In particolare la modifica del
   flusso avviene in funzione dei dati. Le istruzioni che fanno ciò sono ad
   esemio if(condizione){codice} else {altro_codice}. Se "condizione" è vera
   viene eseguito "codice", altrimenti il processore salta quella porzione di
   codice ed esegue "altro_codice". Il blocco "altro_codice" invece viene
   saltato nel momento in cui "condizione" è vera. Un altro esempio sono i
   cicli for e while. Abbiamo una ripetizione di un blocco di codice e alla
   fine dell'esecuzione, a seconda del risultato di un test logico su una
   condizione, si decide se bisogna ricominciare ad eseguire il blocco di
   codice a partire dalla sua prima istruzione oppure se si può andare avanti
   ad eseguire il codice successivo. Per realizzare questi cambiamenti: basta,
   nel caso in cui bisogna cambiare il flusso di istruzioni da sequenziale ad
   un flusso con salti non bisogna far altro che caricare all'interno di IP il
   valore dell'indirizzo dell'istruzione che bisogna eseguire, nel caso in cui
   questa non sia la prossima istruzione della sequenza registrata in memoria.
   Quindi nel caso del ciclo for, una volta giunti all'ultima istruzione del
   blocco di codice, se devo ritornare alla prima istruzione del blocco, verrà
   caricato l'indirizzo di questa. Le istruzioni che determinano un mutamento
   del flusso delle istruzioni sono dette istruzioni di salto. Questo salto può
   essere effettuato avanti o indietro ma che non segue il cammino regolare.
   Queste istruzioni fanno sempre parte del "libro di ricette" di qualunque
   processore e ne esistono di due tipi: di salto normale o non condizionato
   (qualunque cosa succeda) e di salto condizionato (faccio un salto se si
   verifica una certa condizione: e.g. ALU ha come risultato di un test logico
   vero e non falso). Perchè due tipologie? Nel ciclo for il salto è sempre
   condizionato il salto è sempre condizionato, come nel while. Nel caso
   dell'if else se "condizione" è vera devo fare "codice" altrimenti
   "altro_codice" ma se ho "condizione" vera ed eseguo il "codice" non dovrò
   eseguire "altro_codice" nonostante questo si trovi subito dopo "codice" in
   memoria. Questa cosa può essere fatta solo con un salto non condizionato
   perchè la condizione era quella che aveva portato ad eseguire il primo
   blocco di codice e non quello successivo. Queste istruzioni modificano
   direttamente il contenuto del program counter. 
4. Se invece l'istruzione richiede accesso alla memoria, viene determinato
   l'indirizzo del dato. Supponiamo che l'istruzione sia "somma ad AX il
   contenuto della cella 27238". Quest'operazione richiede alla CPU di
   utilizzare il sistema di bus esterno. Questo sistema di bus esterno
   dev'essere utilizzato scrivendo nell'address bus l'indirizzo che vogliamo
   andare a caricare. Questa cosa viene compiuta dal registro MAR che consente
   di trasferire all'address bus dei dati che possono essere presenti nei vari
   registri del processore. è possibile anche fare anche "prendi la cella il
   cui indirizzo è contenuto nel registro AX" e fare indirizzamenti come se
   fosse un vettore. Quando diciamo a[3] cerchiamo nella memoria dedicata ad a
   il contenuto della terza cella. Questo accade attraverso l'implementazione
   delle istruzioni dedicate. Se abbiamo bisogno di prendere dei dati dalla
   memoria, ciò richiede di trovare l'indirizzo della memoria che può essere
   scritto all'interno dell'istruzione stessa (i.e. lettura diretta) oppure
   all'interno di registri (i.e. lettura indiretta) e questo numero dev'essere
   trasferito nel MAR.
5. A questo punto il dato viene caricato nella memoria attraverso l'esecuzione
   di una read. Questa avviene nella seguente modalità: il MAR inserisce
   l'indirizzo che si vuole leggere sul bus degli indirizzi, il processore
   assegna R/W al valore R, il processore fornisce lo strobe. A questo punto il
   circuito di decodifica avrà attivato la linea di chip select della
   periferica d'interesse. All'interno di questa periferica vi è l'and che
   riceve il CS, il R/W e lo strobe. Quando le tre condizioni sono verificate
   l'and attiva le linee d'uscita dei buffer tri-state che così possono
   scrivere il dato in uscita alla periferica. Il dato viene scritto sul data
   bus. Nel caso del processore complesso visto precedentemente questo dato
   viene mandato direttamente in ALU attraverso il multiplexer. Se avessi
   voluto compiere la somma del dato contenuto in una cella con quello
   contenuto nel registro AX avrei dovuto azionare contemporaneamente
   l'azionamento del buffer tri-state che va a scrivere l'uscita di A sul bus B
   ed ecco che ora la ALU si ritrova ad avere su un lato il dato proveniente
   dalla cella e dall'altro quello proveniente dal registro A. Sull'uscita
   quindi vi sarà il risultato dell'operazione sul bus data out. Questo può
   essere utilizzato per scrivere il risultato in un registro attivando il
   relativo clock oppure la scrittura su MBR che è l'equivalente del MAR ma che
   consente la scrittura del dato sul data bus. La scrittura del dato è
   un'operazione simile a quella della lettura. Come prima cosa si imposta
   l'indirizzo a livello del MAR, poi si imposta il R/W su W, si dà lo strobe.
   
Accumulatore: nel caso delle macchine semplici che dispongono di pochi registri
e poche istruzioni, spesso per limitare il numero di istruzioni possibili c'è
un registro che viene usato preferenzialmente per contenere il risultato di
un'operazione. Quando abbiamo il risultato dall'ALU, basta selezionare il clock
da attivare. L'istruzione che dice "scrivo il valore del risultato sul registro
C" e "scrivo il valore del risultato sul registro A" devono essere diverse.
Quindi significa che se volessi avere la possibilità di scrivere il risultato
su qualunque registro dovrei avere tante istruzioni. Potremmo avere diverse
istruzioni per operazioni simili ma che coinvolgono registri differenti in cui
avviene la scrittura. Ciò creerebbe un inutile ampliamento del "libro delle
ricette" del processore senza avere dei veri vantaggi. Si tende ad utilizzare
un registro  designato alla scrittura del risultato dell'operazione e viene
chiamato accumulatore perchè dentro di sè può accumulare il risultato di tante
operazioni compiute in sequenza.
   
MBR: registro interno al processore che serve per la gestione del flusso dei
dati. Dentro MBR abbiamo dei fili collegati tramite buffer tri-state diretti
verso AMUX, e dall'altra parte è come avere un latch con tristate che collega
gli stessi fili del data bus. Quando effettuo una read, verrà abilitato il
tristate che porta il dato verso AMUX ma non abilitiamo l'uscita del tristate
di questo registro. Quando scrivo il dato verso l'esterno questo viene latchato
(i.e. memorizzato) con un clock e, quando deve avvenire l'operazione di
scrittura, si abilita l'uscita dei tri-state verso il data bus. Nelle macchine
più semplici può non esserci perchè si utilizza il registro accumulatore.
Questo oggetto lo si utilizza se vogliamo dare al processore la possibilità di
scrivere il risultatodirettamente sul bus tramite un'operazione di write.
Nell'ST7, processore più semplice, se volessi compiere la scrittura del
contenuto dell'accumulatore dovrei utilizzare l'operazione "scrivi il contenuto
di AX all'indirizzo di questa cella". Così facendo si semplificano i circuiti
che costituiscono il processore e quest'operazione viene svolta lo stesso
pagando però il costo di un tempo operativo maggiore.
   
#### Terzo processore: PIC e l'estrema semplicità

Un processore progettato con l'ottica di semplicità dal punto di vista
hardware. è semplice sia dal punto di vista del numero di istruzioni che delle
risorse hardware. è presente solo il minimo indispensabile. Questo processore
però è molto veloce. Si vuole compensare la complessità con la velocità. è una
seconda strategia alla complessità. Posso fare cose semplici ma l'hardware che
mi serve può essere più veloci nell'eseguirla. Con il PIC posso avere che, nel
tempo in cui ST compie un'istruzione complicata, nel PIC ne compio 5 semplici.

Questa macchina dispone, all'uscita della ALU, di un accumulatore. Questo è
l'unico registro per la gestione del dato di questo processore. Esiste un altro
registro, quello che contiene l'indirizzo della prossima istruzione,
l'equivalente del program counter.
   
## Architettura di Harvard

Una variazione sul tema rispetto all'architettura di Von Neumann. Questa
architettura ha un insieme di due gruppi di bus di comunicazione verso il mondo
esterno. Si tratta di un processore che al posto di avere un unico address, un
unico data e un unico control bus in uscita, raddoppia queste strutture. Da un
lato address, control e data bus gestiscono tutti i dati del processore e
dall'altro lato address, control e data bus gestiscono le informazioni relative
al programma. I programmi sono numeri e una volta che abbiamo deciso dove si
trova il programma rispetto a dove si trovano i dati, nell'architettura di
Von Neumann. Ogni volta che il processore con architettura di Von Neumann deve
accedere al suo spazio d'indirizzamento per accedere ad un'istruzione o per
accedere ad un dato, deve sempre utilizzare i 3 bus, da qui l'utilizzo di MAR e
MBR che servono a gestire il fatto che i dati e gli indirizzi possono essere
utilizzati anche per indirizzare il programma. Posso separare le due cose per
avere più efficienza in alcuni casi. Se ho due sistemi di gruppi di bus diversi
(uno per programmi e uno per dati) questo rende la gestione della comunicazione
verso il mondo esterno molto più efficiente. Il program counter non sarà mai
collegato verso l'address bus della data memory ma si collegherà alla linea
degli indirizzi che va varso la program memory. Ciò mi permette di non dover
più creare la condivisione delle risorse e posso cercare di parallelizzare il
più possibile le attività. Ogni volta che accedo al sistema dei bus posso
accedere ad una cella alla volta. Devo accedere ad una cella se faccio una
lettura di un'istruzione e faccio un'altra lettura di una cella se voglio
andare a leggere il dato. Questo diventa un collo di bottiglia (i.e. condizione
che rallenta, pone dei dubbi sull'utilizzo della tecnologia) per la possibilità
del processore di compiere istruzioni in maniera molto rapida. Se moltiplicassi
i canali di comunicazione, ridurrei però il traffico d'informazioni. Questo può
però rendere più efficiente l'esecuzione di queste istruzioni. Ci sono però dei
costi da considerare:

1. Ci sono più fili per collegare la CPU con il resto del mondo. Non c'è più un
   gruppo solo di fili ma due. Stiamo parlando di circa 26 fili per gruppo per
   avere un totale di più di 50 fili. Questi occupano spazio a livello di
   circuito e aumenta la complessità della CPU.
2. Una volta deciso quanti "scaffali" metto nella program memory e quanti nella
   data memory non ho più la possibilità di di determinare durante l'esecuzione
   di un programma come utilizzare questi scaffali. Questo era possibile con
   un'architettura di Von Neumann. Se volessi utilizzare un computer che deve
   realizzare funzioni molto diversi l'uno dall'altra (general purpose) il
   fatto di avere tutto lo spazio di indirizzamento comune crea il vantaggio di
   poter essere flessibile alla dimensione del programma che bisogna allocare
   in RAM: se il programma occupa molto spazio in RAM posso utilizzare poi
   poche celle per i dati e se questo occupa poco spazio allora le celle per i
   dati saranno di più. Perdo quindi in flessibilità. All'interno di sistemi
   embedded non mi interessa molto avere della flessibilità per quanto riguarda
   il rapporto della memoria dedicata ai dati e la memoria dedicata al
   programma: una lavatrice non diventerà mai una macchina per fare ECG. Quindi
   l'architettura di Harvard può funzionare nei sistemi embedded. Un bus
   dedicato al flusso dati garantisce la possibilità, a parità di costo di
   hardware (capacità di commutazione di transistor) di avere una capacità di
   gestire molti più dati. Se si pensa di voler ottenere una macchina per
   effettuare una compressione MPEG o un'elaborazione di uno stream digitale
   che dev'essere effettuato in tempo reale è chiaro che avere un'architettura
   come quella di Harvard possa essere molto più efficiente perchè evito di
   congestionare troppo i bus. Uso Harvard *se la macchina deve macinare tanti
   dati*.

I due gruppi di bus vengono etichettati con dei nomi particolari:

- Data bus system: l'insieme dei tre bus deputati alla comunicazione con
  memoria contenente dati e periferiche.
- Program bus system: l'insieme dei tre bus deputati alla comicazione con la
  memoria contenente le istruzioni del programma.  Il dato nel caso del program
  bus system è il numero che codifica per l'istruzione.

Il Microchip PIC gestisce sistemi embedded ed è basata sull'architettura di
Harvard. Anche l'Atmel AVR è un microcontrollore RISC (Reduced Instruction Set
Computer, processore che nasce sotto la filosofia di essere veloce a scapito di
avere un libro di ricette con poche opzioni, semplici) che utilizza un
approccio con architettura di Harvard.

### Quarto processore: Atmel AVR, il processore di Arduino

Processore a 8bit (bus dati) con architettura di Harvard. La maggior parte
delle istruzioni vengono eseguite in un colpo di clock. 

Ogni processore ha un set di istruzioni che il progettista deve conoscere per
poterlo programmare. Da una parte abbiamo la capacità di scrivere delle
funzioni facilissime in C. Ci dev'essere in mezzo qualcosa che faccia
trasformare un'operazione che non può essere ottenuta tramite il set di
istruzioni base del processore e la faccia diventare implementabile attraverso
l'uso di tante istruzioni del processore. Il vantaggio che posso trovare è che
una volta che definisco un linguaggio di programmazione, questo è un linguaggio
formale cioè descrive in maniera univoca un algoritmo. Questo significa che
posso sviluppare programmi che possono effettuare la traduzione da un
linguaggio all'altro. Mentre la traduzione del linguaggio naturale è un task
molto difficile mentre quello di programmazione è un linguaggio formale quindi
quella scrittura definisce in maniera univoca l'algoritmo che voglio
implementale. L'univocità permette di avere software che fanno una traduzione
sicuramente corretta. Se voglio programmare in C e trovo un processore il cui
instruction set è molto vicino a questo linguaggio per avere una corrispondenza
più diretta. La traduzione da C a linguaggio macchina non avviene a costo zero:
è corretta perchè si tratta di una traduzione da un linguaggio formale ma ciò
non significa che porti ad un codice molto efficiente dal punto di vista del
linguaggio macchina del processore che deve implementare il programma. Il
codice che risulta dopo la traduzione risulta non ottimizzato. Se riuscissi a
scrivere l'algoritmo utilizzando l'instruction set del processore riuscirei a
rendere l'esecuzione molto più veloce. D'altra parte la scrittura del sorgente
in C rende possibile la portabilità su diversi processori perchè non v'è
bisogno di conoscere l'instruction set di ciascun processore per poter definire
l'algoritmo da implementare ma ci si può concentrare solo sull'algoritmo
stesso. Il traduttore poi lo adatterà sulla base dell'elaboratore cui il codice
è destinato.

---

è possibile comprendere cosa significa avere tante o poche istruzioni.
Considerando il PIC 16 (RISC), avere poche istruzioni significa averne 35. Il
libro delle ricette del PIC 16 ha 35 ricette. Viene anche indicato che ogni
istruzione viene eseguita nello stesso tempo. Le istruzioni vengono descritte
per come sono organizzate.

L'esempio riporta cosa significa avere un'istruzione. Il PIC usa
un'architettura di Harvard e nel data bus del sistema di bus della program
memory (flusso che porta istruzioni dalla program memory al processore) è largo
14 bit. Questo data bus ha 14 bit perchè sono abbastanza sia per poter
codificare 35 istruzioni che per poter contenere gli argomenti di queste
istruzioni. Tali istruzioni sono organizzate in modo da utilizzare al meglio
questi 14 bit. A seconda del tipo di istruzione v'è un numero variabile di bit
che vengono dedicati alla definizione di quello che viene chiamato OPCODE.
Questo opcode è l'operation code, il numero che identifica quale istruzione il
processore sta utilizzando, il numero della pagina iniziale della ricetta. I
bit non utilizzati dall'opcode sono liberi e li posso utilizzare per poter
fornire un argomento. Esempio: somma di una costante al contenuto
dell'accumulatore: inserisco il valore costante all'interno dell'istruzione
stessa. L'uso dei 14 bit consente di impacchettare gli opcode che identificano
le 35 operazioni lasciando spazio per gli argomenti delle varie istruzioni.

---

Come può un circuito elettronico gestire l'esecuzione di tutti questi passi. La
control unit gestisce l'istruzione facendo fetch. Il numero rappresenta
l'istruzione. Come può essere implementata l'istruzione? con una serie di
operazioni, attivazioni e disattivazioni di linee logiche. Tutte le linee di
comando non vengono rappresentate nei vari schematici. La control unit per far
in modo che venga eseguita un'istruzione, avrà un suo "libro di ricette" in cui
vi è una serie di istruzioni spiegate nel dettaglio. In queste ricette vi sono
gli stati di ciascun filo che la control unit ha per poter gestire tutti i vari
registri. La control unit deve impostare le linee logiche ad ogni passo. Ogni
pagina ha una serie di righe che mi dice che rappresentano un'operazione. La
control unit si interfaccia con una specie di memoria perchè ad ogni cella di
questa contiene una serie di operazioni. Processore RISC: che possiede poche
istruzioni. Questo tipo di memoria dev'essere operativa fin dall'accensione del
processore. è una ROM interna, una masked ROM.

## Hardware aggiuntivo per migliorare l'efficienza del processore

Per fare meglio il lavoro della CPU si utilizzano degli hardware particolari,
facoltativi.  

### Instruction pipelining

Si cerca di realizzare dei processori che eseguano istruzioni
velocemente. L'instruction pipeline aumenta la velocità d'esecuzione. Un
processore all'inizio fetcha l'istruzione. I circuiti di fetching e quelli che
eseguono le istruzioni risultano indipendenti. Quando uno funziona l'altro no.
Se volessimo mantenere la massima efficienza vorremmo effettuare le cose in
parallelo. Prendo un istruzione, divido le fasi in cui questa viene eseguita.
Fetch ed execute. Utilizziamo due livelli. ogni istruzione viene tradotta a due
livelli: fetch e execute. Ciascuna riga riporta un clock della CPU. La prima
istruzione viene fetchata ed eseguita. Dopo che viene fetchata ed eseguita io
chiedo ai ciruciti di fetch di caricare l'istruzoine successiva. Se in
parallelo all'execute 1 io facessi il fetch di 2 riuscirei ad utilizzare un
solo colpo di clock al posto di mettercene due. Raddoppio la velocità. Cosa
succede se faccio un jump invece? Solo quando chiamo subroutine e la eseguo mi
accorgo che non devo fare fetch di 4 ma devo fare un fetch diverso,
all'indirizzo giusto della prima istruzione della subroutine. Io provo e mi
porto avanti e se scopro che mi son portato avanti per nulla butto via quello
che ho preso e riprendo da dove sono. Ogni volta che fluisce sequenzialmente il
programma il processore è veloce il doppio. quando jumpo ho bisogno di due
colpi di clock per caricare correttamente l'istruzione. In generale questi
eventi sono rari e la performance media è vicina alla velocità doppia.
L'execute avrà impatto sulla fetch nei casi di jump.

### Lo stack

Un elemento opzionale ma molto gradito è lo stack. Questa è una pila, un modo
di gestire la memoria. Abbiamo sempre visto l'accesso delle read e write con i
bus. Utilizza sempre la stessa memoria ma in modo diverso. Posso fare in modo
che alcune celle della memoria vengono utilzzate con funzioni diverse. Lo stack
è una pila, un modo di memorizzare su parte di memoria RAM. Ogni volta che
faccio accesso alla memoria normale devo tenere conto dell'indirizzo della
cella e per fare ciò ho bisogno di tenere da qualche parte un numero di bit con
larghezza di address bus che tenga conto di questa. Ci sono dei casi in cui
sarebbe più comodo avere un posto in cui avere informazioni che so che
utilizzerò a breve termine senza dover avere un vero e proprio indirizzo. La
pila è un modo per fare questo. Creo un vincolo, ossia quello di accedere
sempre e soltanto a seconda della struttura pila che sto utilizzando alla prima
o all'ultima cella che la compone. Uso la memoria come una pila in cui metto
dentro cose di cui ho bisogno. Ho un vincolo sull'uso di questa memoria ma in
questo modo non dipendo dall'indirizzo. Due opzioni: la pila dei piatti (prendo
l'ultimo che ho messo) LIFO: last in, first out. La coda è un FIFO (First In,
First Out). In FIGURA si mostra uno stack che viene utilizzato per ottenere
informazioni.

Lo stack pointer è l'indicatore che dice in quale cella dello stack posso
mettere in memoria qualcosa. Per usare questa memoria come stack devo usare due
funzioni: push e pop. Il push mette informazioni e il pop la tira via. Il push
e pop fanno in modo di acceder alla memoria ma non contengono un indirizzo! Il
costo che si paga è che non posso scegliere quale cella voglio prendere ma
posso prendere solo l'ultima informazione che è stata messa dentro. Quando
pusho D0 lo stack pointer viene incrementato. Avviene un altro push e il
processore lo mette nella cella puntata dallo stack pointer e poi incremento
SP. quando faccio un pop prima decremento lo stack pointer e poi leggo il dato.
Il contenuto della cella ora può essere riscritto in quella cella. La lettura
toglie dalla pila l'informazione. Questo strumento permette la memorizzazione
temporanea di informazioni. Quando scrivo programmi è necessario questo stack.
Lo stack pointer è qualcosa che deve tenere un informaizone e dev'essere
incrementata. Per il progettista bisognerà aggiungere un registro in più se ho
lo stack. Questo registro avrà tanti bit quanti quelli dell'address bus e che
tenga il punto di dove sono arrivato a scrivere. il push prenderà un contenuto
di un registro, ad esempio A. Write all'address dello SP e poi incrementa SP.
la pop invece fa la cosa inversa: decrementare lo SP, read all'indirizzo
indicato da SP. La pop ha bisogno invece dell'indirizzo di dove vado a mettere
poi il contenuto dell'ultima cella dello stack. push a, pop b: il contenuto di
A viene messo in stack e poi viene spostato in B. Alcune macchine al posto
dello stack vi è una soluzione simile. Alcune hanno solo una pila con un
piatto. Bisogna fare molto attenzione a non andare oltre la dimensione dello
stack altrimenti andrei a variare dei contenuti importanti del sistema.
Macchine più sofisticate hanno dei sistemi di protezione che fa in modo che lo
stack pointer non possa andare oltre un certo range. Questo genera una
schermata blu. Molti dei processori che utilizziamo non hanno una protezione di
questo tipo e un errore sulla stima dell'utilizzo dello stack durante
l'esecuzione può causare crash di sistema.

### L'interrupt (aka ISR o Interrupt Service Routine)

Questa è un sistema che serve a rendere il processore molto più efficiente nel
gestire eventi che possono avvenire all'esterno. *Eventi sincroni*: seguono il
tempo macchina del processore e tutto è predeterminato. Qualunque evento
sincrono che succede è conseguenza dell'utilizzo di istruzioni. Quando ho una
macchina che interagisce col mondo esterno ha a che fare con diversi altri
elementi. Il programma deve essere in grado di capire se accadono eventi
esterni d'interesse, ad esempio se passa qualcosa davanti ad un sensore, il
lettino è arrivato a fine corsa... Tali eventi avverranno quando capita. 

Quali sono le strategie di gestione di tali eventi? 

- Posso fare controlli continui (Polling). L'esempio classico è controllare se
  qualcuno viene a casa nostra. Se qualcuno viene a trovarmi dovrei essere in
  grado di capire quando viene. Non è un evento sincrono perchè non so quando
  prepararmi all'evento. Ogni cinque minuti guardo se c'è qualcuno fuori dalla
  porta. L'interruzione dell'attività che si sta compiendo fa perdere un sacco
  di tempo e la maggior parte delle volte gli eventi non si verificano. Se
  arriva qualcuno e noi siamo appena andati via perdiamo un sacco di tempo fino
  a quando non andremo ad aprire. 
- Per risolvere ciò si usa il campanello. Noi che siamo dentro sappiamo quando
  arriva l'evento asincrono. Questa cosa viene replicata nei processori
  mediante l'interrupt. È possibile tradurre il campanello in termini di ciò
  che accade quando il processore esegue il programma. Allo stesso modo in cui
  una persona effettua un'interruzione dell'attività e l'esecuzione di una
  serie di azioni che gestiscono la chiamata del campanello, il processore
  arresta il flusso della main() per rispondere all'interruzione con una
  particolare funzione, la ISR. La ISR serve quindi per rispondere
  all'interruzione del processore. Questa parte di programma serve a gestire
  l'evento del campanello. Conclusa l'ISR, il processore ritorna alla
  successiva istruzione della main() che è necessario esegua. Un altro esempio
  è dato da uno studente che sta leggendo gli appunti per affrontare un esame e
  che viene interrotto da qualcuno che bussa alla sua porta. Nel momento in cui
  l'interruzione è gestita, lo studente potrà ritornare a leggere dal punto in
  cui era stato interrotto.
  
Dovrò trovare dei sistemi hardware che mi consentano di fare questa cosa
particolare: sto eseguendo il main program. Quando arriva il campanello vado a
fare ISR, una volta finito torno di nuovo al main program. L'interruzione del
programma principale dev'essere trasparente al main program stesso. Pensando ai
due programmi come se fossero separati, la main() non si accorge di essere
interrotta ma è stata "congelata" e poi "scongelata". Ha modo la main() di
accorgersi che qualcosa è accaduto oppure no? C'è un effetto fatto dalla ISR? è
un effetto puramente temporale. Il tempo di esecuzione del programma sarà
quello della main e quello delle varie interruzioni. Questo strumento ci
permette di avere controllo sulle periferiche esterne che generano eventi
asincroni. Si pensi ad esempio al timer del forno. Una delle funzioni delle ISR
è quella di garantire una corretta gestione del tempo per risolvere molti dei
requisiti real time. 

### Watchdog timer

Questa periferica è molto tipica nei sistemi embedded. Tutti i circuiti che
servono per gestire dei tempi (e.g. durate, frequenze, etc) sono detti timer.
Questo timer avverte quando c'è qualcosa che non va, un arresto dell'esecuzione
del programma. È importante perchè, quando usiamo sistemi embedded e una
funzione viene a mancare per qualsiasi evento raro possa accadere,
potenzialmente i danni che possono accadere sono catastrofici. Può esistere un
sistema di sicurezza con un hardware esterno al processore, indipendente dalla
CPU. Questo oggetto è un timer che riceve dal processore un reset. È come un
timer luci per le scale di un condominio. Se una persona continuasse a premere
a intervalli regolari il tasto di accensione, la luce rimarrebbe accesa. Sto
dando un comando di reset. Lo stesso accade per i watchdog timer. Se il
processore non fornisce il comando di reset, il watchdog timer scatena un
interrupt al processore e il progettista ha l'occasione di gestire gli eventi
di emergenza estrema. Questi comandi non avvengono in situazioni normali. Il
processore mette in sicurezza gli elementi critici del sistema (si pensi ad
esempio ad un ventilatore meccanico: non si vuole che il sistema si blocchi
nello stato di gonfiaggio) e successivamente avviene un riavvio del sistema. Il
riavvio permette di iniziare da uno stato noto. Il watchdog timer fa in modo
che la macchina non perda le funzioni durante l'esecuzione del software a causa
di errori di tipo software stesso. Sistemi di questo tipo sono utilizzati
perchè la logica programmata apre ad una miriade di possibilità ma la
maggioranza degli errori è di natura software.

## Il microcontrollore

Microcontrollori: rappresentano una gran famiglia di dispositivi derivati da
microprocessori sviluppati per il controllo di sistemi hardware.

I microprocessori possono essere classificati:

- general purpose: usati per elaborazione di dati senza sapere qual è il tipo
  di dati. Flessibilità del processore. Anche montati possono avere una grande
  variazione sul tipo d'impiego a seconda dei programmi che si installano.
- embedded: microcontrollori. Il microcontrollore non ha la capacità di variare
  più di tanto la sua funzione, una volta implementato nel dispositivo.
- DSP (digital signal processor): processori nati per essere usati per
  applicazioni che vedono l'elaborazione dati. Alcune funzioni matematiche
  tipiche del digital signal processing sono implementati a livello hardware.
  Ci permette fare calcoli particolari (modelli ARMAX). Se inserisco un MAC
  (Multiply and ACcumulate) posso eseguire quelle funzioni più velocemente.

Microcontrollore: metto nello stesso chip quello che mi serve per controllare
circuiti elettronici per poter avere un controllo adeguato dei circuiti
elettronici da parte del processore. Microcontrollori = CPU + Memorie (RAM e
FLASH) + periferiche per comunicare con altro hardware (ADC per vedere una
tensione analogica, DAC, Porte di ingresso/uscita digitale, etc). Molti dei
microcontrollori che useremo portano all'esterno i piedini solo le linee di
interfacciamento delle loro periferiche e non quelle di address bus, data bus e
control bus. Uso la "SCATOLA GIALLA" mostrata per misurare cose dal sensore.

I microcontrollori hanno necessariamente: microprocessore, memorie, sistemi di
interfacciamento periferiche per la gestione tempo e porte d'ingresso/uscita.
Nello stesso silicio si integra questo computerino per interfacciarsi poi col
mondo esterno.

### ST7, microcontrollore molto utilizzato

Architettura di Von Neumann. è un processore comune, normale. Atmel e Pic si
discostano un po' dal modello generale. Il microcontrollore è già dotato di
tante cose (memoria e periferiche). Questo riduce flessibilità. Se volessi una
certa applicazione devo capire quali sono i requisiti hardware per poter far in
modo che il sistema sia ottimale all'applicazione che voglio fare. Normalmente,
quando compro un processore general-purpose faccio riferimento solo alla
velocità, qui avrò il processore e lo stesso processore può essere declinato in
vari dispositivi a seconda della RAM installata, etc. Il produttore prevede che
il core, il microprocessore che sta dentro, possa avere attorno più o meno
periferiche. Il processore quindi sarà sempre lo stesso ma la dotazione delle
periferiche son diversi. Le famiglie di controllori condividono lo stesso core
ma diversi optional intorno. I microcontrollori quindi vanno studiati a livello
di core e di famiglia di dispositivi. Cercherò tra i membri della famiglia
quello che ha le periferiche che mi servono. Maggiori sono le risorse e
maggiore è il costo, quindi bisogna pensare bene alle risorse.

Il microcontrollore contiene RAM, ROM e tutte quelle periferiche che servono
per interfacciare la macchina col mondo esterno fatto di dispositivi
elettronici. Dato che diverse applicazioni possono richiedere diverse
proporzioni delle varie risorse (memorie e periferiche) e dato che ad una
maggior quantità di risorse è associato anche un costo di produzione (e quindi
un prezzo di vendita) più elevato, anche se il processore di base è sempre lo
stesso, i produttori di microcontrollori sviluppano una serie di membri della
famiglia dei microcontrollori. ST7 è un core caratterizzato da un
microprocessore. Non è possibile comprare solo il microprocessore ma associato
ad un certo set di periferiche. Andando sul sito del produttore si scopre che
esistono molti dispositivi aventi lo stesso core e caratterizzati da diversi
set di periferiche e memorie. Posso scegliere il mix più adatto alla
particolare applicazione d'interesse.

Quando si studiano i microcontrollori è necessario prima studiare il
funzionamento del core, che è comune alle famiglie, e in secondo luogo si
studiano tutte le periferiche delle quali si potrebbe aver bisogno. Infine si
sceglie il dispostivo singolo e si studiano le caratteristiche relative alle
connessioni elettriche, alla piedinatura, etc.

#### Core di ST7

Macchina estremamente semplice:

- Unità aritmetica a 8bit. La sua matematica si limita alla gestione di 8 bit
  per volta: tutte le volte che si vogliono compiere calcoli che hanno bisongo
  di rappresentare numeri che hanno un insieme di valori con cardinalità
  superiore a 256 (max esprimibile con 8 bit), saranno necessarie più
  iterazioni e quindi sarà necessario più tempo per svolgere l'operazione.
- Esistono 6 registri interni: 

	- Accumulatore
	- Program counter
	- Stack pointer (optional "obbligatorio")
	- Due registri, X e Y, che sono detti indirizzi indici. I registri
	  indici possono produrre numeri non solo per fare calcoli ma anche per
	  indirizzare la memoria. Il numero contenuto nel registro può essere
	  usato sia per essere inserito nella ALU ma anche sull'address bus. Il
	  contenuto di X e Y ha una possibilità d'utilizzo che prevede il fatto
	  che l'uscite di questi possano finire nell'address bus ed essere
	  usati come indirizzo. È utile avere questi registri indici nel
	  momento in cui è necessario creare una struttura come un array o un
	  vettore. Tale vettore ha un nome che indica come si chiama
	  quell'insieme di variabili e un indice che dice quale delle N
	  variabili che costituiscono il vettore io sto effettivamente
	  indicando. Posso usare questi registri per fare un accesso all'array
	  andando a metterci dentro l'indirizzo in memoria del primo elemento
	  che costituisce l'array e, se volessi accedere al terzo dovrò sommare
	  a quest'indirizzo tre volte la larghezza del dato che è contenuto
	  nell'array. Questo calcolo può essere eseguito utilizzando l'ALU e il
	  risultato può essere usato in X e Y per creare l'indirizzo ed andare
	  a prendere il dato corretto. Questo rende più veloce l'utilizzo
	  dell'accesso ai dati in memoria quando si utilizzano strutture come
	  gli array.
	- Esiste un code condition register ed è diverso da processore a
	  processore.  Raccoglie quelle linee che fanno interagire la control
	  unit con il software e fa in modo che i risultati di qualche
	  operazioni possono essere noti al programma. Spesso contiene i
	  riporti dei calcoli effettuati dalla ALU. Un bit può essere
	  utilizzato per capire se il valore dell'ultimo test era zero oppure
	  no. Può essere utilizzato in modo tale che alcune parti del
	  processore funzionino in modo differente a seconda di come viene
	  settato un determinato bit. Mette insieme tutte quelle impostazioni
	  che sono importanti per far comunicare il programma con l'hardware di
	  gestione del processore.
	- Controller block: aka control unit.

- Esistono delle interfacce comuni ai vari membri della famiglia, quindi che
  compongono il core ST7:

	- Un oscillatore on-chip (la frequenza del clock, essendo definita da
	  un condensatore non è stabilissima.  Non è molto affidabile per
	  applicazioni che richiedono una precisione molto elevata). Questo
	  componente serve per generare un segnale di clock. L'hardware che
	  serve per generare il segnale di clock è già all'interno di questo
	  chip. Normalmente questi dispositivi possono essere attivati, nel
	  caso non ci fossero particolari esigenze di precisione temporale,
	  inserendo una capacità all'esterno oppure non inserendo nulla. In
	  questo caso la frequenza del clock, essendo definita da una capacità
	  che dipende con la temperatura e con il tempo di utilizzo, il clock
	  avrà una frequenza non stabile. Nel caso invece in cui si avessero
	  dei requisiti temporali importanti (e.g. campionamento di un segnale
	  con una determinata frequenza da mantenere stabile nel tempo) ho
	  bisogno di un oscillatore che funziona in maniera più accurata. Si
	  può considerare lo stesso hardware di modo da utilizzare dei
	  cristalli di quarzo che garantiscono una stabilità della frequenza di
	  lavoro molto più elevata sia nel tempo che nella riproducibilità nei
	  vari dispositivi. L'orologio diventa un vero orologio, anche molto
	  accurato.
	- Un blocco di reset fa in modo che la macchina parta, al suo avvio o a
	  valle del reset di un dispositivo, con uno stato noto. Visto che si
	  tratta di una macchina che esegue programmi e che fa un'istruzione
	  dopo l'altra cambiando stato tra i molti disponibili a seconda
	  dell'esecuzione delle varie istruzioni, quando parto è necessario
	  essere sicuri di partire nello stesso modo. I registri devono essere
	  inizializzati con valori corretti, che il processore vada a prendere
	  la prima istruzione nello stesso posto, etc. I circuiti hardware che
	  fanno in modo che all'avvio lo stato della macchina sia noto e
	  riproducibile costituiscono il blocco di reset di cui il
	  microcontrollore è dotato. Tutta la gestione e gli stessi bus dei
	  dati e degli indizzi, con i vari circuiti di  decodifica
	  dell'indirizzo che attivano le varie periferiche che sono sullo
	  stesso chip, sono all'interno del processore. Avere tutto all'interno
	  di un unico chip di silicio rende queste macchine isolate verso il
	  mondo esterno. Tutte le linee dei bus sono interni al
	  microcontrollore e non sono accessibili all'esterno. Alcuni
	  microcontrollori di fascia alta, che dispongono di una piedinatura
	  più ampia e che permette loro di potersi interfacciare con più
	  libertà, possono avere verso l'esterno anche il collegamento dei bus.
	  Se non c'è accesso diretto ai bus, le periferiche di cui si dispone
	  sono solo quelle interne al chip al silicio.
	- Interrupt controller: hardware che serve a fare l'interrupt,
	  funzionamento della macchina che consente di seguire un codice in
	  maniera asincrona su richiesta hardware esterna. L'hardware che fa in
	  modo che, una volta giunto il segnale "campanello", venga eseguita
	  una funzione particolare, è chiamato Interrupt Controller.

Vediamo come sono collegati i dispositivi. 

![Diagramma delle connessioni logiche nel controllore ST7.\label{fig:st7}][st7]

In questa sezione si mostra uno schematico non dettagliato ma che serve a
comprendere le connessioni logiche all'interno dei blocchi funzionali interni
al dispositivo. I quadratini sul bordo esterno rappresentano il passaggio
dell'informazione elettrica verso il mondo esterno attraverso il piedino del
circuito integrato. L'infomrazione contenuta in tutti quei fili che non sono
connessi ad quadratini non potrà essere convogliata all'esterno del
controllore. 

- Oscillatore, generatore di clock. Può essere configurato per funzionare con
  quarzi oppure con condensatori.
- Control Unit, ALU, Registri etc. costituiscono il processore (MPU). Questo
  comunica con la program memory e la RAM tramite il sistema dei bus.
- Watchdog timer (WDT) che attiva il reset al momento del blocco
  dell'esecuzione del codice, LVD (Low Voltage Detector) sistema per aiutare il
  processore ad aumentare la sua affidabilità: se ho una mancanza o problemi
  sulla tensione di alimentazione (e.g. la tensione sta andando sotto una certa
  soglia) l'hardware potrebbe non funzionare più in modo ottimale; tale
  dispositivo controlla il livello della tensione di modo da scatenare
  un'interrupt in grado di mettere il dispositivo in sicurezza prima che si
  spenga o si riavvii. Questo accade perchè negli alimentatori, la componente
  capacitiva che serve a stabilizzare la tensione in uscita permette se
  scollegata ed in alcuni casi, una caduta del potenziale con costante di tempo
  elevata. Così facendo il processore ha tutto il tempo per svolgere le ultime
  operazioni di sicurezza prima che il dispositivo si spenga effettivamente.
- Linea di reset esterno: garantisce alla macchina di essere riavviata.

##### I registri

- Accumulatore: a 8bit. Il dato al suo interno può essere utilizzato come
  operando. Questo è il destinatario delle operazioni logiche o aritmetiche.
- Indici X,Y: registri a 8 bit che vengono utilizzati sia per 

	- Mantenere dati temporanei per fare operazioni, etc.
	- Creare l'effective address, l'indirizzo reale. Ottenere il valore di
	  indirizzo di una cella come risultato di un'operazione.

- Program counter: 16bit. Uno spazio d'indirizzamento del processore di 64k.
- Stack pointer: 16bit. Il MSB (most significant byte) è fissato a livello
  hardware. Quando si studiano queste macchine si ha a che fare con il lavoro
  di qualcuno che ha condensato gli sforzi con l'obiettivo del risparmio delle
  risorse. Ciò che non è utile non viene considerato. Noi sappiamo che lo stack
  è quella parte di memoria manipolabile dal processore mediante le istruzioni
  di push e pop e che consente di avere una gestione più veloce del mettere o
  togliere dati dalla memoria RAM. Quanta RAM posso utilizzare per fare lo
  stack? Non tutta ma solo quella che effettivamente serve al progettista.
  Quindi lo stack è per sua natura limitato rispetto al totale spazio
  d'indirizzamento. Quando il processore effettua push e pop va a incrementare
  e decrementare il valore contenuto nello stack pointer. Ciò significa che lo
  stack pointer è un contatore up-down, di per se stesso. È qualcosa di più di
  un semplice latch di tipo D. Se volessi muovermi in una regione piccola
  allora è inutile costruire un contatore up-down a 16 bit che costa molto
  quando ci si muove sempre su e giù attorno ad un certo valore. Siccome è
  proprio il progettista di ST7 a decidere dov'è la mappa della memoria, decide
  lui anche dove collocare le celle della pila dello stack: setta lui i primi
  bit più significativi. Non possiamo andare oltre un certo numero di variabili
  memorizzate in stack. Posso andare a leggere e scrivere un totale di 256
  valori indirizzabili, corrispondenti a quelli codificati dagli 8 bit
  variabili. Cosa succederebbe se andassi oltre questi 256? andando oltre si
  avrà overflow di stack (il riporto viene eliminato perchè la zona costante è
  hardcoded). Tutte le variabili memorizzate nello stack vengono perse. Questo
  significa morte certa del sistema. Per questo motivo le procedure ricorsive
  non sono molto utilizzate su macchine come queste.
- Code condition register: Questo registro è variabile in larghezza e dipende
  dal produttore del microcontrollore. Nel caso del ST7 è costituito da 5 bit.
  Ciò non significa che quando il processore lo legge, leggerà effettivamente
  solo 5 bit. Questo non accade perchè la lettura e la scrittura sono sempre in
  relazione alla larghezza del bus dei dati. Gli altri 3 bit che separano dagli
  8 del data bus, che non sono usati, sono dati a caso e il software non dovrà
  utilizzarli. Tra i 5 bit abbiamo:

	- *H* (Half carry o riporto): quando il calcolo produce un riporto
	  avremo un carry. Questo accade nel momento in cui si esegue una somma
	  (ADD) e una somma con carry (ADC, somma che tiene conto di un
	  eventuale riporto che è risultato dal passo precedente). L'istruzione
	  ADC è un'istruzione molto importante nel momento in cui si voglia
	  effettuare un'operazione con dati aventi numero di bit più elevato
	  della larghezza del data bus. Se il processore dovesse eseguire la
	  somma di due valori a 16 bit dovrebbe prima considerare le componenti
	  meno significative (i primi 8 bit) di entrambi i dati ed esegue la
	  ADD, successivamente considera le due parti più significative ed
	  esegue una somma con riporto ADC. Se anche la seconda somma genera un
	  riporto andando oltre i 16 bit e il processore non lo considera,
	  quest'informazione viene persa mandando il risultato della somma in
	  overflow. In questo caso l'overflow si genera perchè si stava
	  utilizzando una dimensione del dato inferiore a quella che in realtà
	  era necessaria per memorizzarlo. (overflow all'interno di operazioni
	  matematiche).
	- *Z* (Zero bit): Il risultato dell'ultima operazione è zero.
	- *N* (Negative bit): Il risultato dell'ultima operazione è negativo.
	  Alcune istruzioni come la JNZ (jump if not zero) e la JZ (jump if
	  zero) accedono direttamente questo bit.
	- *C* (Carry/Borrow out, riporto).
	- *I* (Interrupt mask): Il progettista firmware può decidere di
	  abilitare le interrupt oppure no (rispettivamente ponendo a LOW e a
	  HIGH).  Questo equivale ad abilitare la modalità "non disturbare" al
	  processore. Questo bit è molto importante per gestire l'esecuzioni di
	  parti di codice che sono critiche.
	  
  L'accesso hardware a questi bit permette al programmatore del firmware di
  implementare delle funzioni diverse dalle istruzioni.

![Registri di ST7 e loro inizializzazioni all'avvio del processore.\label{fig:st7-reg}][st7-reg]

La figura rappresenta i vari registri e fornisce varie informazioni: il numero
di celle che compone ciascun rettangolo corrisponde ai bit del relativo
registro. I reset values sono i valori che il circuito di reset fa assumere a
ogni bit tra quelli che determinano lo stato della macchina all'avvio della
stessa. Le lettere X indicano che il bit non è impostato ad un valore noto. I
circuiti di inizializzazione dei registri aggiungono complessità all'hardware
che, se non è richiesta, è da escludere. Un caso indispensabile di
inizializzazione vede il program counter. Questo perchè la prima cosa che la
macchina farà al momento dell'accensione sarà il fetch della prima istruzione
da eseguire, quindi una read ad un indirizzo di memoria cui corrisponde la
prima istruzione del programma.  Lo stack pointer ha il MSB fissato
dall'hardware.  Il code condition register ha i primi tre bit fissati a 1
dall'hardware e non hanno un significato vero e proprio, gli altri sono settati
ad un certo valore noto per evitare che vi siano risultati inattesi. I circuiti
di reset, per riassumere, preparano tutto ciò che non può essere impostato con
le prime istruzioni.

**Reset vector**: massimo grado di flessibilità nell'utilizzo della memoria del
processore. Dove si trova la prima istruzione del programma all'interno dello
spazio d'indirizzamento del processore? È possibile creare un vincolo che
stabilisce il punto della memoria in cui risiede il programma e ciò non è
flessibile: se volessi gestire in modo diverso le istruzioni non potrei farlo
in questo modo. La seconda soluzione utilizza un sistema di vettori (un
concetto diverso da quello che li intende in C. Se ho un vettore di interi X,
la variabile X è l'indirizzo alla prima cella, che contiene perciò il primo
intero dell'array. Il vettore è quindi un indirizzo). Non si vincola nella
memoria il blocco in cui si trova tutto il programma ma solamente due celle:
0xFFFF e 0xFFFE contengono un vettore, un indirizzo che rappresenta dove si
trova la prima istruzione che il processore deve eseguire. Quindi il processore
non andrà ad eseguire il programma a partire da una cella di memoria prefissata
dall'azienda produttrice ma, facendo una read a queste due celle, carica il
loro contenuto nel program counter. Il processore potrà quindi effettuare il
fetch della prima istruzione subito dopo e stabilire il corretto funzionamento
della macchina. Ora il progettista può inserire la prima istruzione in
qualunque punto dello spazio d'indirizzamento del processore.

**Gestione dello stack**: Si utilizza sempre push e pop. Nel caso di overflow il
processore effettua un roll over ritornando ad un valore alto (si parla più di
underflow). Questo processore non indica il caso in cui avvenga lo stack
overflow.Non vi sono sistemi di protezione. Il sistema operativo può fare molte
cose ma le altre applicazioni no. Vi sono livelli d'esecuzioni a livello
hardware e certe istruzioni possono essere eseguite dal sistema operativo e
certe altre, in caso di altre applicazioni, hanno autorità più basse. Quando
l'applicazione viene eseguita c'è la possiblità di mettere controlli hardware
per fare in modo che ogni applicazione può utilizzare delle risorse fino ad un
certo punto, senza sforare. Questo accade per evitare che vi siano dei write in
celle di memoria in cui si trova il contenuto del sistema operativo e per
evitare la morte certa del sistema. Appena un'applicazione intende scrivere in
una posizione proibita della memoria viene generata un'interrupt e quel
processo viene bloccato. Il sistema operativo procede all'uccisione del
processo e alla chiusura dell'applicazione senza creare danni ad altri
programmi e al sistema operativo stesso.

![Manipolazione della memoria stack, esempi di chiamata a subroutine e di interrupt.\label{fig:stack-manip}][stack-manip]

Come funziona l'interrupt? Lo stack è importante per poter eseguire alcune
funzioni. Vi sono due tipologie di funzioni: l'interrupt e la chiamata a
subroutine. Perchè si utilizza lo stack? Entrambe queste chiamate implicano
l'interruzione di un codice ed un salto ad un'altra parte di codice che viene
chiamata (potenzialmente) da tante parti rispetto al programma principale.

- *Chiamata a procedura*: parte di codice che dev'essere ripetuta in tante
  parti diverse durante l'esecuzione del programma. Le varie istruzioni della
  funzione possono essere utili in diversi punti dell'algoritmo. Possono
  esistere due soluzioni al problema:

	1. Copiare il codice che serve per implementare la funzione ogni volta
	   che è richiesto dall'algoritmo. Questa soluzione è estremamente
	   inefficiente dal punto di vista del consumo della memoria di
	   programma.
	2. Isolare la parte di codice che implementa la funzione. Questa verrà
	   richiamata ogni volta che è necessario. Ogni volta che il processore
	   si ritrova l'istruzione CALL subroutine, la control unit non
	   prosegue l'esecuzione del codice all'istruzione successiva a quella
	   appena eseguita ma all'indirizzo in cui è salvata in memoria
	   la prima istruzione della sequenza che rappresenta la subroutine. 

  La soluzione ideale è la seconda ma presenta comunque un *problema*. Non è
  possibile effettuare un semplice jump alla prima istruzione della subroutine
  perchè altrimenti il processore, una volta terminata la funzione, non
  saprebbe in quale punto della main ritornare. È necessario dunque tenere
  traccia del punto in cui il processore era arrivato prima della chiamata a
  funzione. Lo stack risulta uno strumento ideale per la gestione di questo
  problema. Quando si esegue una chiamata a subroutine, la control unit esegue
  due push (un processore con uno stack a 8 bit e un program counter a 16 bit
  può accedere al contenuto del program counter in gruppi da 8 bit per volta)
  andando a salvare il contenuto del program counter su stack. Quando il
  processore sta eseguendo l'istruzione CALL subroutine, il program counter
  contiene l'indirizzo all'istruzione successiva al termine della subroutine
  stessa.  Questo avviene perchè, durante la fase di fetch, il processore
  carica l'istruzione "CALL subroutine" aggiornando il program counter. Alla
  fine del fetch (i.e. quando il processore sta iniziando l'esecuzione di "CALL
  subroutine") il program counter punta all'istruzione che succede "CALL
  subroutine". Essa rappresenta il punto nel codice in cui l'esecuzione si è
  interrotta. Il processore, eseguendo "CALL subroutine", salva
  quest'informazione effettuando due push sullo stack (push PCL e push PCH),
  solo allora aggiorna il program counter con la prima istruzione della
  subroutine.  A questo punto, dopo aver eseguito push PCL e push PCH e la
  subroutine è terminata, l'istruzione RET (return from subroutine) esegue pop
  PCH e pop PCL in quest'ordine.  Quest'operazione ricarica sul program counter
  questi due byte e il program counter si ritrova pronto all'esecuzione
  dell'istruzione successiva alla chiamata a procedura. Questo meccanismo fa in
  modo di lasciare traccia del punto in cui il processore si trova in qualunque
  caso (anche se una procedura ne chiami una seconda).
- L'*interrupt* è invece una chiamata a procedura in cui, anzichè essere il
  codice ad attivare la control unit con l'esecuzione dell'istruzione,
  quest'attivazione avviene dall'hardware, dall'interrupt controller. È
  un'evento esterno a generarla. Il processore caricherà sul program counter
  l'indirizzo della prima istruzione dell'ISR interrompendo l'esecuzione del
  codice principale e alla fine dell'ISR si ritornerà al punto di partenza nel
  main. Vi è una differenza con la chiamata a procedura: nella chiamata a
  procedura il processore si aspetta di dover andare ad eseguire dell'altro
  codice perchè riceve l'istruzione CALL subroutine. Il codice incorpora la
  serie degli eventi sincroni, quindi previsti dal processore che potrà così
  conoscere quando accadranno o sono accaduti. Cambieranno quindi i valori dei
  registri interni (CC, A, ...) ma il processore, conoscendo quando avviene la
  chiamata a procedura, può preventivamente sarvarne i contenuti in memoria
  (i.e. stack). Ciò avviene perchè il contenuto dei registri sarà poi
  modificato dalla subroutine. Nel caso dell'interrupt il processore non ha il
  tempo di salvare autonomamente il contenuto dei registri.  Il problema è che
  quando viene interrotta l'esecuzione del codice in maniera asincrona (i.e. non è
  previsto nella sequenza delle istruzioni) è necessario avere dell'hardware che
  permetta non solo di ritornare all'indirizzo di partenza ma che anche il
  contenuto dei registri della macchina sia esattamente quello che c'era nel
  momento della chiamata a interrupt.  Concretamente la chiamata a interrupt
  salva PCL e PCH, come la chiamata a subroutine, e i registri CC, A e X.
  Quando il processore ha terminato l'esecuzione dell'ISR, (IRET, interrupt
  return) verrà eseguito il pop del contenuto dei registri salvati al momento
  dell'inizio dell'interrupt.  Questo è il motivo per cui è possibile
  riprendere l'esecuzione del programma senza che venga creata un'alterazione
  del contenuto dei dati. Il processore nel caso delle interrupt, deve
  riprendere nello stesso punto e nello stesso stato in cui era stato
  interrotto.
  
	- *Lo stesso punto*: l'istruzione cui il flusso del codice era arrivato
	  prima di essere interrotto.
	- *Lo stesso stato*: contenuto dei suoi registri.

  Osservando bene la figura è possibile notare che, dopo aver fatto il push dei
  suddetti registri, viene effettuato dall'interrupt stesso il push del
  registro Y. Questo perchè la chiamata a interrupt deve salvare lo stato della
  macchina ma ogni volta che il salvataggio in memoria da parte del processore
  richede tempo prezioso (si pensi ai cicli di scrittura). Se l'ISR modificasse
  il registro Y e non fosse salvato su stack il processore avrebbe perso
  l'informazione ivi contenuta. Se le operazioni che devono essere compiute
  dall'ISR sono resource-intensive, sarà necessario considerare il salvataggio
  completo, altrimenti il processore esegue un salvataggio più "minimalista",
  ottimizzando la velocità d'esecuzione. Questo è importante poichè meno tempo
  intercorre tra la richiesta d'esecuzione dell'interrupt e l'esecuzione della
  prima istruzione dell'ISR (aka latenza) e più responsivo sarà il processore
  all'evento hardware.  Bisogna anche considerare che i controlli delle
  interrupt vengono eseguiti necessariamente tra due istruzioni e non possono
  essere effettuati a metà dell'esecuzione di un istruzione. Ciascuna
  istruzione ha da considerarsi atomica. La durata delle istruzioni potrebbe
  influire sulla latenza sopra citata. Bisogna anche notare che è fortemente
  sconsigliata l'implementazione di funzioni ricorsive su questi sistemi in
  quanto vi sarebbe un largo utilizzo dello stack con possibilità di stack
  overflow.

**Interrupts vectors**: la stessa logica con la quale è stato concepito il
reset vector viene utilizzata per indicizzare la prima istruzione delle ISR.
Esistono delle celle all'interno della memoria che raccolgono i bytes
dell'indirizzo della prima istruzione da eseguire nel caso avvenga una chiamata
asincrona. A differenza dell'ISR, una chiamata sincrona viene effettuata
tramite opportuna istruzione, che si rifà ad un preciso indirizzo in memoria
(la label della subroutine corrisponde al particolare indirizzo associato). Una
volta chiamata l'ISR tramite hardware, dovrà essere gestita dalla control unit
che indicizzerà il flusso di esecuzione delle istruzioni a seconda del
contenuto delle apposite celle vettore. ST7 sfrutta le celle subito prima la
penultima: da lì in poi si possono inserire gli indirizzi delle ISR. 

Per comprendere la gestione dei diversi eventi hardware da parte dei processori
è necessario considerare un'analogia. Una casa di campagna ha due campanelli:
uno sulla porta d'ingresso e l'altro sul retro. Esistono due principali
soluzioni per differenziare la risposta ad uno di questi eventi hardware:

- Si ha a disposizione *un unico blocco di celle vettore in cui immagazzinare
  l'indirizzo della prima istruzione dell'ISR* che, in questo caso, sarà
  l'unica per ogni evento hardware associato all'interrupt. L'interrupt quindi
  scatenerà la stessa risposta per ogni evento hardware impostato ma sarà
  possibile differenziarla una volta che il processore è a conoscenza di quale
  periferica l'ha generata. Il processore inizia l'ISR e chiede quale
  periferica l'ha scatenata. Non si hanno label per ciascun evento hardware.
  Ciò significa che stiamo considerando lo stesso suono per entrambi i
  campanelli.  Il proprietario di casa (il processore) dovrà controllare
  ciascuna porta (periferica) per capire dove hanno suonato. L'*interrupt
  controller* è molto semplice in questo caso: ogni periferica avrà la linea
  che attiva l'interrupt in OR con le altre. Qualunque linea si attivi avrà
  come risultato l'attivazione dell'uscita alla porta logica che quindi
  scatenerà l'interrupt del processore.
- È possibile avere *tanti diversi indirizzi per altrettante ISR diverse*. In
  questo modo implemento una sorta di etichettatura associata ai diversi eventi
  hardware, così da differenziare le risposte da parte del microcontrollore.
  Ciò significa che i due campanelli hanno suoni diversi, quindi il
  proprietario di casa sa già dove recarsi al momento esatto in cui qualcuno
  suona alla porta. I circuiti che dovranno gestire gli eventi hardware saranno
  più complessi rispetto a quelli della soluzione precedente ma sarà possibile
  differenziarne le risposte con diverse ISR specifiche. Sicuramente una
  soluzione di questo tipo avrà una latenza inferiore rispetto a quella della
  soluzione precedente.  Le interrupt vengono differenziate dall'*interrupt
  controller*: Questo ha tanti ingressi, fili indipendenti che possono scorrere
  nel control bus. Questi fili giungono all'interrupt controller che, nel caso
  in cui il processore gestisca vettori multipli (i.e. multiple ISR), saranno
  molti. L'interrupt controller interagisce con la control unit arrestandola
  nel momento in cui si verifica un'interrupt e provocando l'esecuzione del
  micro-codice che prepara il processore alla risposta a tale evento (push PCL,
  push PCH ...). Quando la control unit deve caricare sul program counter
  l'indirizzo della prima istruzione dell'ISR, l'interrupt controller mette a
  disposizione l'informazione su quale dei fili ha generato quell'evento. La
  control unit ora può essere indirizzata ai corretti vettori cui fare
  riferimento per il fetch dell'istruzione corretta.

L'interrupt consente di mantenere l'esecuzione del flusso logico di istruzioni
ma non viene mantenuto il flusso temporale. L'interrupt arresta l'esecuzione
della main per eseguire qualcos'altro. Al termine dell'ISR il processore
riprende l'esecuzione della main. Questo è il principio dell'implementazione
dell'esecuzione parallela di programmi: sebbene temporalmente siano eseguiti in
serie, questi logicamente lo sono in parallelo. Questo significa che si dovrà
gestire i due flussi (main e interrupt) indipendentemente, avendo l'accortezza
di realizzare delle interazioni tra questi, volte alla soluzione di un certo
problema. L'approccio qui descritto è noto come *programmazione di processi
concorrenti*. Per poter gestire i flussi d'esecuzione che si muovono in
parallelo si utilizzano delle *variabili globali* che possono essere modificate
dai processi concorrenti e fungono da interfaccia vera e propria. In questo
modo è possibile immagazzinare il risultato di un campionamento in una
variabile indipendentemente dall'esecuzione della main. Quest'ultima potrà
utilizzare poi il dato immagazzinato per fare di calcolo.


Come posso programmare un microcontrollore? Devo poter scrivere all'interno
delle memorie FLASH (ROM) il programma che ho compilato. Quelle istruzioni
devono essere fisicamente trasferite alla memoria persistente del sistema.
Quando il bus non è accessibile all'esterno dovrei utilizzare un numero di fili
ridotto per poter trasferire l'informazione dall'esterno alla memoria flash
interna. Quando viene attivato il dispositivo dedicato, questo comunica con un
interfaccia esterna chiamata programmatore che va a scrivere i bit della
memoria ROM per programmare il microcontrollore. Una volta questa cosa veniva
fatta solo scollegando il processore, programmando il processore e poi
rimettendolo all'interno del circuito. Ultimamente è possibile aggiornare il
firmware senza dover scollegare il processore stesso. Programmazione in-situ:
senza dover disconnettere il processore dal circuito elettronico
dell'applicazione finale. Esistono diversi fili che possono essere utilizzati e
questo tipo di programmazione ci mette un po' di tempo. Noi utilizzeremo
sistemi derivati da questi. Questo è un aspetto importante che richiede dei
circiuti necessari solo in fase di programmazione. Durante l'attività normale
del controllore non saranno più necessari.

L'altra cosa è il circuit debugging: il debuggin è quella fase in cui si è
scritto un programma e bisogna andare a vedere perchè il programma non fa
quello per cui non è stato progettato. Bug: le prime volte in cui erano fatte
queste operazioni vedevano errori hardware dovuti alla presenza di insetti che
si inserivano tra le valvole del computer. Questa procedura della rimozione
degli errori ora è più semplice. In matlab si possono utilizzare i breakpoints
per comprendere il valore delle variabili durante l'esecuzione del codice. è
possibile quindi identificare l'origine di quest'errore. Quando lavoriamo con
l'embedded è il processore che sta facendo eseguire il codice. Il problema è
che il codice del controllore non è compilato per la stessa architettura del
mio pc (utilizzo di un crosscompilatore). Quindi è difficile utilizzare dei
breakpoint all'interno di questi dispositivi. All'interno di questi processori
è stato aggiunto dell'hardware apposito per il debugging. Questi circuiti
programmatori posso quindi sia programmare il processore, che sapere lo stato
di registri e quello che sta accadendo nel processore. Questo è un approccio
curioso: chi fa debuggin lo fa chi sviluppa il firmware. Sembrerebbe uno spreco
di hardware. Lo metto su tutti e lo uso su pochi. In realtà il concetto è che è
più costoso gestire più versioni e che diverse versioni si comportino poi nello
stesso modo. Questo è chiamato In-Circuit Debugging (ICD).

Ritornando alle interrupt. ST7 dice che tutte le interrupt possono essere
mascherate tramite il bit I del CC. Mascherare le interrupt vuol dire staccare
il campanello, il telefono. Non voglio che il processore venga interrotto da
eventi hardware. Quando arriva un'interrupt e io ho lasciato attiva la capacità
di fare cose alla interrupt il processore salva lo stato del processore, quindi
il contenuto dei suoi registri (PC, CC, ..). Tutte le altre interrupt vengono
mascherate invece (scelta particolare di ST). Come un processore decide di
gestire le interrupt chiamate vicine è una scelta dei produttori. I processori
di fascia alta usano priorità e si lasciano interrompere. Se la priorità è alta
puoi interrompere il processore. Con l'approccio delle priorità però ho bisogno
di un grande stack. L'approccio di ST7 è quello di finire una telefonata per
volta. La reutrn from interrupt ripristina il contesto (riporta lo stato a
com'era prima dell'interrupt) e poi tolgo la mascheratura delle interrupt. La
priorità, nel caso in cui più interrupt chiamassero allo stesso tempo, viene
assegnata dall'indirizzo, più grande l'indirizzo e maggiore è la priorità.

???


### PIC16F87

Il core è PIC16. La lettera F è associata alla tipologia di memoria (flash).
L'ultimo numero identifica il packaging e di tipologia di hardware.

Macchina estremamente semplice. Non ha uno stack vero e proprio ma una
soluzione alternativa meno flessibile. Nonostante la semplicità può realizzare
applicazioni complesse.

Ci vorranno meno istruzioni su ST7 per effettuare le stesse cose ma ciò non
implica che ST7 sia più veloci. Sarà necessario fare poi calcoli relativi alla
velocità d'esecuzione di ciascuna istruzioni per capire qual è il dispositivo
più veloce.

#### Instruction Pipelining

Due livelli di pipelining: fetch ed execute.

#### Gestione memoria

???

---

Due indirizzi speciali ???

---

#### Interrupt

Ho un solo livello d'annidamento. Quindi si avrà solo main e interrupt.

## Interfacciamento con le periferiche

Il processore considera la periferica esattamente come una memoria, non sa come
distinguerla. Vediamo ora una porta ingresso-uscita. Il processore potrà usare
la periferica facendo read e write a degli indirizzi particolari. I registri
della periferica vengono usati per la configurazione o per immagazzinare il
dato di questa. Un registro di configurazione può dire alla periferica se è un
ingresso o un uscita. Sviluppiamo ora l'I/O port. Quando questa è come ingresso
mi consente di leggere cosa c'è al piedino della porta indicata. Iniziamo con
la porta d'ingresso. La scrittura implica anche una memoria, una traccia. Come
il pulsante e l'interruttore.

*NB*: per semplificare la rappresentazione delle porte si rappresenta solamente
un pin ma si ricorda che queste sono costituite da un numero di pin pari alla
larghezza del bus dati.

### Porta d'ingresso

![Schematico di una porta d'ingresso.\label{fig:input}][input]

In questa sezione si rappresenta un pin di una porta d'ingresso. Questa è
utilizzata abilitando il buffer tri-state quando una fase di lettura
all'indirizzo della porta viene richiesta dal processore. Nello schematico,
ADDR rappresenta l'uscita di un AND che codifica per l'indirizzo della porta in
questione (e.g. 0x0000).

### Porta d'uscita

![Schematico di una porta d'uscita.\label{fig:output}][output]

A differenza della porta d'ingresso, quella d'uscita richiede un elemento in
grado di memorizzare il dato scritto. Ciò è necessario per evitare che, una
volta conlcusa l'operazione di scrittura, il pin risulti flottante.

### Porta d'uscita rileggibile

È interessante combinare le due precedenti porte per realizzare una periferica
che lavori sia in lettura che in scrittura. Utilizzando lo stesso numero di
componenti della porta d'ingresso e della porta d'uscita è possibile realizzare
una porta d'uscita rileggibile.

![Schematico di una porta d'uscita rileggibile.\label{fig:readable_output}][readable_output]

### Porta d'ingresso/uscita

Potrebbe essere più utile avere la flessibilità di gestire ciascun pin
appartenente alla porta come se fosse un ingresso o un uscita digitale
indipendentemente dagli altri. Questo è possibile introducendo un registro di
configurazione (DDR o Data Direction Register) in grado di memorizzare lo stato
logico di un buffer tristate che abilita la porta in scrittura (se impostato
HIGH) o in lettura (viceversa).

Da notare che ADDR DDR e ADDR PORT sono le uscite di due AND che codificano
rispettivamente per l'indirizzo del registro di configurazione della porta
(e.g. 0x0001) e per l'indirizzo della porta in questione (e.g. 0x0000).

![Schematico di una porta d'ingresso e uscita.\label{fig:input_and_output}][input_and_output]

Uno dei limiti di questa porta è che il DDR non risulta rileggibile, quindi è
necessario tenere presente il valore che si è dato ai vari bit di tale
registro.

### Maschere di bit

Non ricordando il contenuto di un registro, se si volesse semplicemente imporre
un particolare bit ad HIGH o LOW si dovrebbe intervenire eseguendo
un'operazione bit a bit sul contenuto di tali registri. Questo perchè le linee
di output enable e di clock sono comuni a tutti i flip-flop del registro dati
della porta. Le operazioni che verranno eseguite per commutare gli stati logici
di determinati bit utilizzeranno degli operandi particolari chiamati maschere.
Prima di effettuare le operazioni è buona norma salvare il contenuto del dato
in una variabile d'appoggio e operare con quella. Solo al termine della
manipolazione il contenuto della variabile sarà riportato sul registro.

#### Impostare bit a HIGH

È necessario mantenere lo stato logico di ogni bit e impostare un particolare
bit a HIGH. Per fare ciò bisognerebbe mettere in OR bit a bit il contenuto del
registro con una maschera avente 0 nelle posizioni dei bit il cui stato logico
debba mantenersi invariato (i.e. 0 è l'elemento neutro dell'or bit a bit) e 1
nelle posizioni dei bit il cui stato logico debba variare.

| A    | B    | A OR B |
| :--: | :--: | :--:   |
| 0    | 0    | 0      |
| 0    | 1    | 1      |
| 1    | 0    | 1      |
| 1    | 1    | 1      |

![Rappresentazione grafica di OR bit a bit con maschera.\label{fig:or_bit}][or_bit]

È possibile impostare più bit a 1 effettuando un OR tra la variabile (o
registro) da modificare e una maschera costituita da un OR tra le maschere che
mettono a 1 i singoli bit.

Quindi se si volesse mettere ad 1 il bit in posizione 0 ed il bit in
posizione 5 si può produrre il seguente codice:

```c

unsigned char x;

// Metto a HIGH il bit in posizione 0 e quello in posizione 5.
x |= (1 << 0) | (1 << 5);

```

#### Impostare bit a LOW

È necessario mantenere lo stato logico di ogni bit e impostarne uno a LOW. Per
far ciò bisognerebbe mettere in AND bit a bit il contenuto del registro con una
maschera avente degli 1 nelle posizioni dei bit il cui stato logico debba
mantenersi invariato (i.e. 1 è l'elemento neutro dell'AND bit a bit) e 0 nelle
posizioni dei bit il cui stato logico debba variare.

Da notare che la maschera così ottenuta corrisponde al complemento a uno della
maschera avente a 1 il bit la cui posizione corrisponde a quella del bit del
registro che si vuole imporre a zero. Nell'esempio infatti: 0b11101111 =
~0b00010000.

| A    | B    | A AND B |
| :--: | :--: | :--:    |
| 0    | 0    | 0       |
| 0    | 1    | 0       |
| 1    | 0    | 0       |
| 1    | 1    | 1       |

![Rappresentazione grafica di AND bit a bit con maschera.\label{fig:and_bit}][and_bit]

È possibile impostare più bit a 0 effettuando un AND tra la variabile (o il
registro) da modificare e una maschera costituita un AND tra le maschere che
mettono a 0 i singoli bit oppure, considerata la legge di De Morgan,
effettuando l'AND tra la variabile (registro) da modificare e il complemento a
uno dell'OR tra le maschere che impostano ad 1 il bit che in realtà si vuole
mettere a 0.

Quindi se si volesse mettere ad LOW il bit in posizione 0 ed il bit in
posizione 5 si può produrre il seguente codice:

```c

unsigned char x;

// Metto a LOW il bit in posizione 0 e quello in posizione 5.
x &= ~((1 << 0) | (1 << 5));

```

### Porte I/O digitali con option register (il caso di ST7)

![Circuito di un pad di una porta I/O digitale di ST7.\label{fig:io_st7}][io_st7]

ST7 non usa solo un registro di configurazione per impostare la porta in
ingresso o uscita, ma crea un altro circuito di configurazione, l'option
register (aka OR) all'indirizzo 0x0002. Questo registro, se la porta è
impostata come ingresso, permette il collegamento di un resistenza di pull-up
interna opzionale, garantendo quindi una forzatura ad una tensione di 5V quando
l'interruttore è aperto.  L'altra opzione è quella relativa ad open drain e
push-pull nel caso di porta d'uscita.  Il push-pull garantisce che l'uscita
digitale abbia sempre una tensione impostata ad HIGH o LOW.  L'altra
configurazione (i.e. open drain) è basata sulla possibilità di portare verso
massa il segnale (nel caso di codifica LOW) ma non impone il valore di tensione
positiva (nel caso di codifica HIGH) mantenendo il pad in modalità floating. 

I due MOS che abbiamo visto nella configurazione finale: quando vorrò ottenere
una tensione positiva dovrò mettere HIGH sul gate del mos che si trova sopra.
Quando voglio imporre LOW attivo quello che sta in basso. Quindi se il
processore ha impostato il pin in push-pull (i.e. il contenuto di OR è HIGH),
dovrà fornire un segnale di attivazione al p-mos (sopra) o all'n-mos (sotto) a
seconda che voglia scrivere HIGH o LOW rispettivamente. Se il processore si
trovasse in open drain e il segnale in ingresso ai MOS avesse valore HIGH non
vi sarebbe alcuna attivazione dei MOS: la linea di drain del p-mos sarebbe
aperta (i.e. alta impedenza) e l'uscita del pin risulterebbe flottante; se il
processore scrivesse sul DR il valore LOW, la linea che porta verso massa
(appartenente a n-mos) sarebbe connessa alla linea d'uscita che quindi
risulterebbe LOW. La configurazione di open drain avrebbe una maggior utilità
se vi fosse collegata una resistenza di pull-up che condiziona l'uscita, nel
caso di valore diverso da LOW, ad HIGH in maniera equivalente ad una
configurazione di push-pull.

Andiamo a considerare ora i circuiti equivalenti generati dalle diverse
condizioni dei registri DDR e OR.

1. *Input*: DDR è HIGH e OR può essere HIGH o LOW a seconda che si voglia
   oppure no una resistenza di pull-up. Questa resistenza porta verso l'alto la
   tensione applicata al pad. Ciò è particolarmente utile nel caso in cui si
   voglia utilizzare uno switch, ad esempio, in quanto non sarà necessario
   inserire resistenze esterne al circuito. È presente un trigger di Schmitt e
   in uscita vi è un segnale che va al data bus. Il passaggio dell'informazione
   sarà certamente dettato dalla presenza di un buffer tri-state comandato dal
   processore (mediante decodifica dell'indirizzo del DR della porta, strobe
   basso e operazione di read). Il trigger di Schmitt è un buffer che, anzichè
   utilizzare una sola soglia per discriminare se la tensione in ingresso è da
   rappresentarsi come LOW o HIGH, ne utilizza due creando un'isteresi.
   Quest'isteresi serve per essere sicuri di fornire una caratterizzazione ben
   definita del segnale, andando a rimuovere il rumore che potrebbe essere
   sovrapposto ad esso. è quindi qualcosa in più del margine di rumore visto
   nella prima lezione: il margine di rumore toglie istante per istante il
   valore del rumore mentre il trigger di Schmitt raccoglie la storia del
   segnale e, a seconda della salita o della discesa della tensione, commuta il
   risultato digitale.
2. *Open drain*: il pin in uscita viene pilotato da un n-mos che, a seconda che
   l'input sia LOW o HIGH, forzerà in uscita un valore LOW oppure flottante. La
   scelta di utilizzare una resistenza di pull-up condiziona lo stato logico
   HIGH in uscita.
3. *Push-pull*: l'uscita della porta viene collegata all'uscita del DR
   attraverso dei MOS in configurazione push-pull.  L'uscita push-pull utilizza
   due MOS. Quello avente il not sul gate è il p-mos mentre l'altro è l'n-mos
   classico. Si utilizzano questi perchè una volta imposto un segnale digitale
   si attiva solo uno di questi due a seconda che vi sia un segnale LOW o HIGH.
   Questo consente di forzare una tensione verso HIGH o verso LOW
   rispettivamente. 
   
Vi sono anche delle linee chiamate "Alternate output". Per garantire un elevata
flessibilità di questi circuiti nelle periferiche non vi sono solo porte
d'ingresso/uscita digitale ma anche altre periferiche (ADC, timer, porte
seriali, etc.). Questo set di periferiche sono utilizzate per controllare le
linee fisiche che vengono portate verso l'esterno. Per garantire flessibilità,
quindi, la maggior parte dei microcontrollori inserisce, a titolo d'esempio,
una porta seriale le cui linee utilizzate per la comunicazioni non sono
dedicate solo e soltanto a tale periferica perchè, se un progettista non ne
avesse bisogno, non verrebbero utilizzate. I progettisti di microcontrollore
cercano di condividere il pad che supporterà quel dispositivo tra diverse
periferiche. Un certo pad quindi sarà una linea di TX di trasmissione dato di
una seriale ma anche il pin 3 della porta A. Queste periferiche però sono
mutuamente esclusive. Per aggiungere la flessibilità nell'utilizzo della
periferica uso due fili che giungono dalla periferica che sta condividendo la
risorsa pad.  L'alternate output fornisce il dato che la periferica on-chip
vorrebbe scrivere su tale piedino esterno. La periferica però non è detto che
abbia in gestione quel pad, poichè in condivisione. Esisterà quindi un altro
registro di configurazione con un bit che verrà utilizzato per determinare
quale periferica ha il diritto di utilizzarlo. Per abilitare l'uso di una
periferica o meno si bisognerà scrivere opportuni registri. Vi saranno dei
registri nella periferica porta seriale (quindi *esterni* rispetto alla porta
I/O che si sta considerando) tra cui vi saranno dei registri di configurazione
nei quali uno dei bit sarà utilizzato per abilitare o meno l'uso della
periferica porta seriale. Il registro deputato a questa funzione è detto
"Alternate enable".  L'alternate enable bit quindi abilita la periferica
on-chip per quel particolare pin.  Imponendo a HIGH l'alternate output bit la
periferica avrà il controllo del pin (sfruttando la configurazione in
push-pull).

L'analog input si considera nel momento in cui il segnale letto sul pad debba
essere convertito da un ADC.

L'alternate input si considera quando si vuole utilizzare un input digitale per
una periferica diversa dalla porta digitale (i.e. RX della porta seriale).

Si notino nell'immagine anche i buffer tri-state (DR SEL, DDR SEL, OR SEL).
Questi buffer hanno la funzione di permettere la rilettura del dato che è stato
immagazzinato su quei registri.

Il circuito riportato in basso invece è ciò che serve all'ingresso per poter
generare un'interrupt al processore. Posso quindi configurare i circuiti di
interrupt per fare in modo che, se il buffer tri-state accanto alla
combinational logic sia attivo, la porta generi una chiamata ad interrupt al
nostro processore. Il filo che porta l'informazione relativa all'interrupt non
appartiene ai bus dei dati e indirizzi.
   
### L'importanza dell'inizializzazione

È indispensabile, per poter gestire al meglio la configurazione delle
varie periferiche, una fase d'inizializzazione nella quale il firmware
istruisce l'hardware sui moduli necessari.

In particolar modo bisogna dedicare molta attenzione alla fase d'avvio di un
dispositivo. Si consideri il caso di una porta digitale che si interfacci col
comando di scoppio di un airbag. Se il pin dedicato di tale porta, al momento
d'avvio, si trovasse in uno stato d'ingresso anzichè d'uscita si verificherebbe
uno stato di indeterminazione in cui non è possibile stabilire se il potenziale
su quel pin sia HIGH o LOW. In applicazioni particolarmente critiche è
consigliato, nella sua soluzione più semplice, l'utilizzo di un resistore di
pull-up o di pull-down sul pin in uscita. Questa forzerà lo stato logico LOW o
HIGH a seconda della condizione safe del dispositivo a cui è connesso. Non è
tuttavia consigliabile questo genrere di soluzioni quando si deve costruire
strumenti alimentati da batterie non molto capienti in quanto lo stato logico
che attiva il dospositivo induce il passaggio di corrente attraverso il
resistore, la quale dissipa potenza. Si ricorda che il corretto dimensionamento
di tale resistore prevede valori elevati. Questo per impedire il passaggio di
correnti elevate per mantenere la tensione sul pin.

## Altre periferiche del microcontrollore

Alcune di queste verranno analizzate approfonditamente durante laboratorio.

*NB*: Molta della teoria che viene trattata durante le ore di laboratorio
potrebbe essere richiesta all'orale.

### Convertitore analogico digitale (ADC)

La tecnologia più comune degli ADC integrati nei microcontrollori classici è
quella ad approssimazioni successive (SAR). Come possiamo far si che un ADC
possa essere visto dal processore come periferica? Il processore scrive e legge
celle non distinguendo la provenienza del dato. Tutto quello che ha a che fare
con il controllo delle periferiche si basa sugli stessi principi che abbiamo
considerato anche per il controllo della porta I/O digitale. Avremo delle celle
utilizzate per comunicare l'informazione che viene elaborata dalla periferica
(il Data Register che contiene il risultato della conversione). Ci saranno
anche registri utilizzati per configurare correttamente la periferica a seconda
di quali sono i nostri obiettivi. Nel caso di ST7 si utilizza uno dei tre
registri per l'interfacciamento con ADC on board. Questo è un registro a 8 bit:

CH0 CH1 CH2 vanno al MUX analogico. Questo MUX fa in modo che il convertitore
possa essere condiviso tra diversi ingressi analogici. Posso quindi dire di
convertire il primo ingresso, il secondo o quello che vogliamo. Ci sono AINx
ingressi. Il numero massimo d'ingressi che possiamo avere, guardando com'è
configurato il sistema in figura è di 8 ($2^3$). Ciascuno dei quadratini che
costituisce il registro altro non è che il solito flip-flop di tipo D con il
buffer per rileggere collegato ad un circuito di decodifica degli indirizzi che
dà l'ok alla scrittura e alla lettura quando viene proposto sul bus degli
indirizzi quello associato registro ADCCSR (ADC Control Status Register).
Quindi significa che se andrò a scrivere un numero, questo avrà i tre bit meno
significativi che corrispondono al canale che si desidera campionare. In questo
registro non ci sono solamente quei tre bit ma ve ne sono altri tre ad essere
utilizzati per una particolare funzione: ADON accende l'ADC e che comincia a
far funzionare la conversione; EOC fornisce il segnale di "end of conversion":
il SAR per fare la sua conversione deve svolgere una esrie di iterazioni (una
per ogni bit che viene decodificato. Nel nostro caso 10 iterazioni) e ciò
richiede del tempo. Il processore che richiede la conversione deve attendere un
po'. è necessario che però il processore venga avvisato nel momento in cui la
conversione è terminata. L'EOC fa proprio questa cosa.

Ad esempio, si vuole attivare l'ADC sul canale 2. Sarà necessario quindi
scrivere su ADCCSR il valore 0x22, che corrisponde a impostare i bit relativi
al CH sul secondo canale e il bit di ADON. Dal main sarà quindi necessario
andare a leggere il valore del registro relativo a EOC con opportuna maschera
(i.e. 0x80) e attendere che diventi diverso da zero. Nell'istante in cui accade
il processore può effettuare la conversione e ricevere il dato correttamente
campionato. La lettura del dato avviene a partire da due registri (i.e. ADCDRH
e ADCDRL). Sono necessari due registri perchè il dato viene campionato a 10
bit, quindi un registro da 8 bit sarebbe insufficiente per raccogliere
l'informazione. I nomi che vengono scelti solitamente per questi registri sono
sempre mnemonici: ADC (convertitore A/D) DR (Data Register) H (High). Su ADCDRH
ho gli 8 bit più significativi. IN ADCDRL vi sono i due meno significativi. In
questo registro quindi, per non sprecare gli altri 6 bit, li si utilizza per
altre configurazioni dell'ADC o modalità operative. La velocità di conversione
è un parametro molto importante. A seconda delle applicazioni che voglio
realizzare posso stabilire una velocità elevata ma con un rumore maggiore sulla
conversione effettuata oppure una velocità inferiore che però fornisce un
risultato più accurato. Per poter ridurre la conversione si consideri il clock
del processore. Questo è comune alle periferiche. La frequenza di clock viene
inviataa dei MUX digitali e il bit SPEED del ADCCSR fornisce l'indicazione su
quale linea dev'essere selezionata all'uscita del MUX digitale: quella avente
frequenza pari a quella della CPU oppure una frequenza dimezzata. In realtà non
è l'unico modo che si può utilizzare per ridurre la velocità ma anche
utilizzando il secondo MUX analogico presente alla cui uscita può presentare
una frequenza pari a un quarto di quella della CPU a seconda del valore assunto
dal bit SLOW nel registro ADCDRL. Perchè è importante selezionare una velocità
di lavoro ottimale per ADC? Per stabilire il trade-off tra accuratezza del
risultato e velocità con la quale il risultato viene calcolato. Inoltre perchè
a priori le macchine possono lavorare con frequenze di clock molto diverse. Più
una macchina è veloce, meglio è dal punto di vista computazionale ma peggio è
dal punto di vista dei consumi energetici. Se ho una macchina veloce che, per
la maggior parte del tempo, esegue cicli for o while per rimanere in attesa.
L'esecuzione di tanti cicli d'attesa comporta una spesa energetica maggiore se
paragonata all'esecuzione di pochi cicli d'attesa (considerando lo stesso tempo
d'esecuzione del programma). Quindi a seconda dell'applicazione si può decidere
di utilizzare un clock più basso se voglio preservare la durata della batteria
oppure più elevata. Questo sistema mi permette di definire, in base al clock
fornito al processore, di fornire all'ADC una frequenza di lavoro che sia
ottimale per la sua conversione. AMP SEL e AMP CAL sono specifici
dell'implementazione su ST7: è possibile configurare lo stadio d'ingresso della
tensione analogica (i.e. l'amplificatore potrà funzionare da buffer o fornendo
un guadagno di 8) prima di arrivare al convertitore. In questo modo ho la
possibilità di attivare l'amplificatore interno e non si necessiterà di
un'amplificazione esterna per condizionare i segnali in ingresso al SAR.

Qualche commento sulla scelta di progettazione dei registri dati dell'ADC di
ST7. Sembrerebbe una scelta strana ma in realtà è ben pensata in quanto molto
spesso la conversione non richiederà l'utilizzo di tutti e 10 i bit. A volte
basterebbe avere una conversione effettuata su valori bassi. Se organizzo il
dato più significativo in modo che sia allineato a sinistra, la lettura di
ADCDRH fornisce già la conversione a 8 bit del dato.

Come ricostruisco il dato a 16 bit nel momento in cui si hanno i due registri
che portano una parte di informazione ognuno.  Bisogna dichiarare una variabile
a 16 bit (la più piccola che possa contenere i 10 bit del dato uscente
dall'ADC). Il contenuto del registro ADCDRL viene opportunamente mascherato in
modo che solamente i due bit meno significativi di tale registro vengano
riportati sulla variabile. L'ultimo step consiste nel riportare su tale
variabile anche il contenuto di ADCDRH, ossia gli 8 bit più significativi.
Quest'operazione consiste in un OR bit a bit tra il risultato precedente e il
contenuto del registro ADCDRH shiftato di due bit sulla sinistra. Si faccia
riferimento alla rappresentazione grafica.

![Rappresentazione delle istruzioni necessarie al salvataggio del contenuto dell'ADC\label{fig:adc_bit}][adc_bit]

### Timer

Il timer è una periferica che ha come obiettivo quello di aiutare il programma
a gestire efficientemente le relazioni col tempo che passa. Un esempio: LED
deve lampeggiare con frequenza nota. Usando solo i cicli for non possiamo
inserire altre istruzioni oltre a quella che commuta lo stato del LED in quanto
modificherebbero la frequenza di lampeggio. La soluzione alternativa sarebbe
quella di utilizzare il meccanismo delle interrupt che però non sono azionate
sulla base di eventi fisici particolari (quindi paragonabili alla pressione di
un tasto) ma dovrà gestire il fluire del tempo. Per fare ciò possiamo
immaginare il problema del tempo dal nostro punto di vista. L'orologio va
sempre alla stessa velocità e fornisce il riferimento temporale al processore
qualunque cosa questo stia facendo. Il timer quindi non è altro che l'orologio
del microcontrollore. Il timer contiene un contatore che può essere a 8 o 16
bit. Questo contatore ha delle linee che indicano in che direzione questo può
contare (up/down), la linea che fornisce l'impulso che è necessario contare e
poi la linea che esegue il reset del contatore. Il contatore viene alimentato
da una linea che può giungere da diverse sorgenti ma nella maggior parte delle
applicazioni questa arriva dal clock della CPU. Possiamo modificare attraverso
un divisore programmabile (aka prescaler) la frequenza di clock, facendo
arrivare al contatore delle frequenze diverse. Il contatore conta gli eventi.
Quando il contatore arriva ad overflow il timer fornisce un comando che può
essere utilizzato per generare un'interrupt. Posso quindi impostare quindi
questo contatore come se fosse un timer da cucina, inserendo al suo interno un
numero a partire dal quale comincia a contare e una volta che questo
dispositivo raggiunge lo zero viene generata l'interrupt. Grazie ad una
tecnologia del genere posso progettare un led che abbia un lampeggio di
semiperiodo di 450ms. Definisco il prescaler che dà la frequenza, vedo quanti
colpi di clock il timer dovrà contare per fare 450 ms e inserisco 0xFFFF meno
il numero di colpi di clock appena calcolato nel registro. Il contatore quindi
salirà fino a 0xFFFF e poi chiamerà l'interrupt. Nell'ISR ci sarà la riga di
codice che fa commutare il pin relativo al LED.

Questi timer possono essere forniti di diversi optional a seconda dei segnali
periodici che si vuole generare. Ad esempio, se volessi generare un segnale PWM
potrei sfruttare proprio il timer. Posso generare un'onda quadra con duty cycle
che può essere cambiata su una frequenza base dev'essere costante. Per generare
un'onda del genere senza che il processore debba fare solo quello posso
utilizzare il timer. In particolare potrei utilizzare due timer: uno che
stabilisca il tempo di on e l'altro che stabilisca quello di off.

---

## Esempio di codice: la barra a diodi led

In questo codice si vuole realizzare una semplice macchina a stati che vuole
controllare il valore di una variabile campionata e mostrarlo tramite
un'interfaccia utente costituita da una barra di diodi LED. Si noti la presenza
di due particolari codifiche:

1. Quella della barra a led per mostrare il valore della variabile misurata nel
   tempo.
2. Quella utilizzata per mostrare il minimo ed il massimo della variabile.

```c
#include <pic.h>
#include <inttypes.h>

// DEFINES
#define NUM_LEDS		8 // (2^3)

// Si rimappano i 256 possibili valori con la codifica della barra di NUM_LEDS
// diodi LED.  (2^8) / N_LEDS = 2^5. Shifto il valore 5 bit a destra.

#define SHIFTS				5 
#define MEASUREMENTS		4 // Numero misurazioni da mediare.

// MACROS
#define MAX(A,B) \
	A > B ? A : B
#define MIN(A,B) \
	A < B ? A : B

// Variabili globali
unsigned char min, max, valore;
unsigned char LED, UNO;

// Dichiarazioni
void init();
void init_minmax();
void loop();
void do_measurements();
void show_minmax();
void show_variable();

void my_delay();

void main(){

	init();
	loop();

}

void init(){

	// Scrivo LOW su ogni LED di PORTB.
	PORTB = 0x00;
	// Abilito PORTC in input mode (per PIC: tutto a 0).
	TRISC = 0x00FF;
	// Abilito PORTB in output mode.
	TRISB = 0x0000;

	init_minmax();
	
}

void init_minmax(){

	// Inizializzo il minimo e il massimo.
	min = 0xFF;
	max = 0x00;

	// Controllo il corretto funzionamento di ogni led.
	PORTB = 0xFF;
	my_delay();
	PORTB = 0x00;
	my_delay();

	// Inizializzo due variabili
	LED = 0;
	UNO = 1;

}

void loop(){

	while(1){

		// Misuro e faccio calcoli.
		do_measurements();

		// Stato_1: pressione tasto al bit (1<<PINC0).
		if(!(PORTC & 0x01))
			init_minmax(); // Reset.

		// Stato_2: pressione tasto al bit (1<<PINC1).
		if(!(PORTC & 0x02))
			show_minmax(); // Mostro minimo e massimo assoluto.

		// Stato_3: nessun tasto premuto.
		else
			show_variable(); // Mostro la variabile.

	}

}

void do_measurements(){

	// Questa funzione non è completa ma manca la read_adc().
	
	// Inizializzo la variabile per accumulare le misurazioni.
	valore = 0;

	// Accumulo sulla variabile alcune misurazioni.
	for (uint8_t i = 0; i < MEASUREMENTS; i++) valore+=read_adc();

	// Faccio la media
	valore /= MEASUREMENTS;

	// Considero solo alcuni bit più significativi.
	valore >>= SHIFTS;

	// Trovo massimo assoluto e minimo assoluto.
	max = MAX(valore, max);
	min = MAX(valore, min);

}

void show_minmax(){

	// Si utilizza una codifica diversa dalla barra a diodi LED.

	// Accendo solo i LED che rappresentano min e max.
	LED |= (1<<(min-1)) | (1 << (max-1));
	PORTB = LED;
	my_delay();
	
	LED = 0;
	PORTB = LED;
	my_delay();

}

void show_variable(){

	// Si utilizza una codifica a barra di diodi LED.

	// Creo la variabile da riportare sulla barra a diodi LED
	for (uint8_t i = 0; i <= valore; i++){

		LED |= UNO;
		UNO <<= 1;

	}

	// scrivo il risultato su PORTB.
	PORTB = LED;

	// Reinizializzo le variabili al nuovo uso.
	LED = 0;
	UNO = 1;

}

void my_delay(){ for (uint16_t i = 0; i < 30000; i++); }
```

## Esempio di codice: PPG (approccio fotoplettismografico)

Un firmware completo e affidabile che implementi funzioni per gestire le
anomalie e condizioni di funzionamento non previste e che possano mandare in
crash il sistema non è semplice e veloce da realizzare bensì richiede del tempo
al suo sviluppo con un lavoro significativo. Nei temi d'esame vi saranno
sicuramente delle approssimazioni per poter garantire uno sviluppo che sia
fattibile all'interno della tempistica dedicata. Una di queste semplificazioni
potrebbe essere quella di considerare delle particolari forme d'onda come
target. Queste possiedono caratteristiche che vengono considerate immutabili
per semplificare il problema.

Si vuole visualizzare l'andamento della pressione sanguigna arteriosa
attraverso un approccio fotoplettismografico (i.e. con pulsiossimetro). È
possibile andare a determinare qual è la concentrazione relativa di Hb
ossigenata rispetto a quella deossigenata andando a considerare l'assorbimento
di radiazione luminosa a due diverse lunghezze d'onda (rosso ed infrarosso).
Questo è possibile estraendo un valore medio che si nota su tutto il sangue che
viene attraversato da queste lunghezze d'onda. Quando noi vogliamo fare la
pulsiossimetria ad un paziente lo facciamo perchè vogliamo determinare il
valore di emoglobina ossigenata nel sangue arterioso e non a valle dello
scambio. Bisogna cercare uno strumento quindi per andare a discriminare i
contributi del sangue venoso da quello arterioso. Per far ciò si può sfruttare
la pulsatilità del sangue arterioso: il letto capillare si comporta come un
filtro passabasso (resistenza elevata e compliance di tutti i vasi che
collegano dall'aorta fino a tutto il circolo sistemico). Se vado a vedere nelle
arterie quindi avrò una componente pulsatile e, a valle di questo filtro
passabasso, nel circolo venoso non si vedrà più. La componente pulsatile è
quindi interamente dovuta al sangue arterioso.

Semplifichiamo il problema andando ad identificare un unico vaso centrato nel
dito al posto di avere molti vasi in diverse posizioni. Misuro l'attenuazione
tenendo conto che se ho una componente pulsatile i punti B e C variano in
funzione della pressione generata dal cuore che è pulsatile. Il diametro varia
con la pulsazione modificando quindi l'assorbimento della radiazione rossa ed
IR. I pulsiossimetri forniscono anche informazioni riguardanti la pulsazione,
in quanto è un'informazione semplice da ricavare a partire dalla grandezza che
già era stata misurata per ottenere la saturazione.

Si vuole quindi realizzare un sistema PPG per la stima non invasiva della
pressione arteriosa (semplificazione forte. Potrei misurare la dilatazione più
che la pressione. La dilatazione è legata alla pressione attraverso la
compliance del vaso che può variare).

Si vuole svolgere una parte del problema: quella che serve per determinare la
forma d'onda del segnale PPG ed estrarre alcuni parametri.

Abbiamo una sorgente luminosa LED e un fotodiodo. L'obiettivo è quello di
misurare la variazione della quantità di sangue contenuta nei vasi del dito
come funzione della variazione dell'assorbimento della luce tra punto A e punto
D. La parte di tessuto compresa tra A e B e tra C e D mantiene invariate le
proprietà d'assorbimento.

Si considera il circuito di condizionamento e di amplificazione del segnale già
effettuato e ci si concentra sulla parte algoritmica.

Ci interessa trovare il massimo e minimo locale all'interno di una finestra
limitata. Questo per evitare che eventuali scostamenti del valore di base
facciano risultare difficile l'individuazione dei punti di massimo e minimo
locali.  Per poter far ciò dobbiamo avvalerci del concetto di derivata e, in
particolare, il cambiamento del segno della derivata identifica se si è trovato
un massimo o un minimo relativo. Quindi non è necessario calcolare il valore
della derivata facendo un rapporto incrementale (che dal punto di vista
computazionale richiede un po' di risorse) ma semplicemente una differenza tra
valori.

[8.11]

Vi erano delle funzioni bloccanti che interrompono il flusso per far avvenire
la conversione A/D. una funzione che inizializzava la frequenza di
campionamento. Vi sono anche due altre funzioni (start e stop campionamento).
La funzione readADC() mantiene bloccato il flusso del codice. La funzione
restituisce -9999 se il dato non viene letto per tempo. Com'è possibile avere a
disposizione questo tipo di funzione? Bisognerà realizzare del firmware per
creare un "campionatore virtuale". Queste funzioni vengono realizzate mediante
una collaborazione tra hardware e software. Le interrupt ci vengono molto in
aiuto perchè il requisito temporale del campionamento è ben definito. L'unico
modo efficiente ed accurato per il processore per seguire il tempo in maniera
precisa ed accurata è quello di utilizzare un timer per farsi avvisare quando è
trascorspo un certo periodo di tempo. Oggi proviamo ad implemnetare queste
funzioni come se fosse un esercizio. Si consideri il campionamento di un solo
canale.

```c

void startADC(void);
void stopADC(void);
void initADC(int F); // F: frequenza di campionamento
int getADC(void); // Funzione bloccante

```

A noi serve ora una funzione che, a seconda del processore, implementerà delle operazioni standard per:

- impostare il canale.
- dare il start of conversion. Poi vi sarà una linea (EOC) che ci dice la
  disponibilità del nostro dato. Quando il dato sarà poi disponibile si potrà
  fare la lettura del dato stesso. Quindi questa funzione ha un corrispettivo
  diretto con l'hardware. Quindi di fatto avremo solo questa funzione readADC()
  per implementare tutto il resto.

```c

int readADC(void); // attiva il campionamento e restituisce il campione.

```

A noi serve però anche il timer. Dobbiamo inizializzare il timer ad una nostra
frequenza. Si crea quindi un'interfaccia software per avere la configurazione
hardware.

```c
int setTimer(int F);
```

A questo punto serve solo l'ISR associata al timer.

```c
ISR();
```

C'è bisogno di due flussi d'esecuzione: il "pizzaiolo" (aka ISR) e il cameriere (aka main).

Una variabile globale fa comunicare i due processi. Questa è allocata e
gestibile da tutte le funzioni. 

Per inizializzare il tutto si potrebbe utilizzare la initADC().

In realtà però la init non dice di produrre le pizze da quel momento ma
inizializza solamente la frequenza.

```c

void initADC(int F){
	setTimer(F);
}

```

Serve una variabile che dica se l'ADC deve campionare o no. Questo si fa
mediante una variabile globale.

```c

uint8_t run;

```

Questa variabile dev'essere però inizializzata dentro init per evitare di
campionare prima di essere startata.

```c

uint8_t run;

void initADC(int F){
	setTimer(F);
	run=0;
}

```

La init dovrebbe essere chiamata in fase di inizializzazione nella main().

Ora dobbiamo implementare la start e stop. Queste dovranno cambiare lo stato di
run.

```c

void stopADC(){

	run = 0;

}

void startADC(){

	run = 1;

}

```

Dal punto di vista dell'economia dell'utlizzo delle risorse utilizzando run
oppure la frequenza di campionamento come variabile globale è diverso.
Quest'implementazione è un filo meno efficiente. Il processore comunque salva
lo stato dei registri del punto in cui il codice è arrivato sullo stack per
effettuare una chiamata ad interrupt, esegue l'ISR ed esce subito. Per questo è
suboptimale.

Andiamo ora a scrivere la getADC() e l'ISR() in parallelo.


```c

int dato;	// Variabile globale associata al valore campionato.
uint8_t newdato; // Variabile globale associata alla presenza del dato campionato oppure no. (1): prodotto, (0): letto.

// Funzione cameriere.
int getADC(void){	

	int valore;
	
	// Aspetto fino a quando il dato non è stato campionato
	while(!newdato);
	// Per evitare che il dato venga sovrascritto 
	// da un'eventuale interrupt vado a salvarlo prima ancora di 
	// restituirlo in una variabile locale.
	valore = dato;
	// Inizializzo la flag.
	newdato = 0;
	return valore;
}

// Funzione pizzaiolo.
ISR(){
	
	// Testo dev'essere eseguita l'ISR.
	if(run){
		
		// Testo se non era già stato campionato un dato in precedenza.
		if (!newdato) {
		
			dato = readADC();
			// Inizializzo la flag per leggere il dato.
			newdato = 1;
			
		} 
		
		// Molteplici campionamenti non letti, condizione d'errore.
		else dato = -9999;
	}
	
}

```

Il pizzaiolo, però, abbassa la bandierina prima di prendere la pizza quindi è
rischioso perchè la pizza presa potrebbe essere diversa da quella che è stata
prodotta in precedenza a causa di un'interrupt scatenata, con bassa
probabilità, nel momento sbagliato. per fare ciò si utilizza una variabile
locale interna che verrà ritornata al posto del dato stesso.


---

[disp_strum]:				Figure/disp_strum.png				{height=400px}
[lettura]: 				Figure/lettura.png				{height=400px}
[scrittura]: 				Figure/scrittura.png				{height=400px}
[spazio_indirizzamento_processore]: 	Figure/spazio_indirizzamento_processore.png	{height=400px}
[decodifica_non_completa]: 		Figure/decodifica_non_completa.png		{height=400px}
[registri]: 				Figure/registri.png				{height=400px}
[registri_due_bus]: 			Figure/registri_due_bus.png			{height=400px}
[st7]: 					Figure/st7.png					{height=400px}
[st7-reg]: 				Figure/st7-reg.png				{height=400px}
[stack-manip]: 				Figure/stack-manip.png				{height=400px}
[input]: 				Figure/input.png				{height=400px}
[output]: 				Figure/output.png				{height=400px}
[readable_output]: 			Figure/readable_output.png			{height=400px}
[input_and_output]: 			Figure/input_and_output.png			{height=400px}
[or_bit]: 				Figure/or_bit.png				{height=400px}
[and_bit]: 				Figure/and_bit.png				{height=400px}
[io_st7]: 				Figure/io_st7.png				{height=400px}
[adc_bit]: 				Figure/adc_bit.png				{height=400px}
