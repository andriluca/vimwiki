# Lezione 3 --- Logiche programmabili

## Dispositivi programmabili (o meglio configurabili)

Anzichè svolgere un programma sono hardware configurabile. Questo significa che
assumono una configurazione determinante il comportamento finale dei
dispositivi finali che li incoroprano.  Il progettista sceglie una
configurazione fisica vera e propria. Questi sono un'intermedio tra un
funzionalità intrinseca e funzionalità programmata.  Mettono a disposizione una
serie di periferiche. Vi sono varie linee di connessipone che possono essere
configurate e collegate a quello che voglio. è come se avessi molti moduli
collegabili. Il dispositivo non esegue il programma in fase di programmazione
ma realizza solo l'hardware necessario a quello. Si possono collegare in questi
dispositivi diverse porte logiche per realizzare diverse reti logiche e
macchine a stati finiti (inserendo dei flip flop in grado di memorizzare lo
stato in cui il dispositivo si trova in quel momento).

PAL, PLA, ROM, GAL, FPGA etc. 

Spesso sono in collaborazione con microcontrollori. La generazione di ritardi è
gestita tutta da questi dispositivi programmabili che creano delle periferiche
vere e proprie ad hoc.

## Principi di funzionamento

Sono una serie di dispositivi che hanno una serie di variabili d'ingresso ed
una serie di variabili in uscita. Esiste la possibilità di collegare a questi
dispositivi in modo da renderli delle macchine a stati finiti. ???

L'esempio più semplice e banale di dispositivi programmabili può essere
costituito da una memoria ROM.

Il metodo più semplice di scrivere una logica combinatoria è quella di scrivere
delle tabelle di verità che associano agli ingressi le rispettive uscite.
Questa tabella della verità può essere utilizzata per programare una memoria
ROM per dare in uscita quello che voglio. Viene programmata una volta sola e ho
creato la funzione che restituisce la tabella della verità. Questo non è il
sistema pià efficiente dal punto di vista implementativo.

### Macchine di Mealy

Si mettessero dei flip-flop alla fine della ROM ???.

### Mappe di Carnot

Posso scrivere le tabelle di verità in modo più efficiente.  Parto da una
tabella della verità minimizzando il numero di equazioni necessarie alla
descrizione di questa. Posso avere una variabile o il suo complemento a uno
(aka la negazione della stessa). Creo un hardware in cui ho variabili e i loro
complementi. Questi fili li mando a delle porte AND nelle quali posso definire
quali di questi ingressi vengono collegati all'AND e quali no. A seconda
dell'equazione che voglio implementare attivo gli interruttori che mi servono.
Le uscite degli and vengono inviate a delle porte OR. Questa configurazione è
detta PLA. 

Le "x" nei vari AND RAPPRESENTATI sono quelli che entrano nell'AND.

## Proprietà dei dispositivi programmabili

### Programmabilità

- Programmabilità one-time: Simile alle memorie ROM basate su fusibili
  descritte nella prima lezione.
- Riprogrammabili: per velocità e compattezza alcune soluzioni vengono
  implementate mediante una memoria RAM. Queste macchine di solito hanno la RAM
  collegata ad una ROM e vanno a caricare le informazioni qui contenute
  all'avvio del dispositivo. Le si utilizza per questioni di velocità.
- Le reti con RAM permettono di *riconfigurare* il comportamento del
  dispositivo durante il funzionamento dello stesso, impattandone sulla
  flessibilità.
  
### Topologia delle connessioni:

- Globali: costituite da lunghe connessioni.
- Locali: costituite da connessioni brevi.
- Gerarchiche: costituite da un insieme di connessioni lunghe e brevi.
- Matrici programmabili

### Architettura delle celle

- Semplici
- Complesse

A seconda della tipologia di tecnologia avrò diverse performance.

## Topologia delle connessioni

La disposizione delle connessioni presenti in questa tipologia di dispositivi è
tale per cui si cerca di risolvere un problema di viabilità (similmente a
quelli che devono essere risolti per collegare diversi centri abitati)
associato ad un trade-off delle proprietà fisiche che ciascuna tipologia di
connessione possiede.

### Connessioni globali (fili lunghi)

Fili lunghi sono associati ad una capacità (devo iniettare molte cariche per
modificare il potenziale). Ho il vantaggio di una maggiore semplicità per realizzarle e per collegare ma non ho una ...

### Connessioni locali (fili corti)

Realizzati con tanti piccoli cavi che realizzano collegamenti corti. Il
collegamento viene fatto creando continuità tra le linee che collegano le celle
desiderate. Ho performance più elevate quando si considerano delle brevi distanze.

### Gerarchica (mette insieme i due precedenti sistemi)

Un po' di linee lunghe globali e un po' di locali. Si combinano i vantaggi
dell'uno e dell'altro. Qui aumenta la complessità degli algoritmi di routing
(i.e. che collegano le celle).

### Matrici programmabili

Switch che consentono di collegare alcuni ingressi con le uscite. Questa configurazione a matrice viene utilizzata per creare quest'approccio per le connessioni gerarchiche. Vi sono dei blocchi arancioni che sono gli elementi che voggliamo collegare tra loro. Mettiamo le matrici agli incorci delle vie di modo da collegare le celle nel modo desiderato.

Si ottiene infine quindi una sorta di connessione che incorpora lunghe e brevi
percorrenze. Le matrici fanno in modo di avere collegamenti diretti (lunghezza
1), di lunghezza 2 o superiore per saltare 2 o più matrici. per andare a
distanza elevata possiamo usare una combinazione di lunghezze elevate e corte.

## Logiche programmabili a due livelli

Ad esempio il PLA che usa piano AND e piano OR.

---

Vi sono dispositivi più complesse ad esempio PLD, FPGA. Anziche collegare AND e
OR metto degli elementi più sofisticati. Nel PLD non abbiamo solo AND e OR ma
anche flip-flop (che permette la creazione della macchina di Mealy) e ???

CPLD: con connessioni di tipo globali. Hanno una dimensione molto elevata.
Queste possono avere una complessità superiori. Gli elementi di base, i
blocchetti che hanno parte di AND e OR e flip-flop sono tra loro omogenei.
Questo rende più semplice la programmazione di questo dispostivo. Quindi la
programmazione sarà più semplice e sarà più facile ottimizzare le connessioni.
Se volessi usare un sottoinsieme di questi elementi però andrei a "sprecare" le
risorse inutilizzate.

FPGA (Field Programmable Gate Array): Molto utilizzati in gran parte delle
applicazioni. Queste vengono utilizzato per implementare funzioni in tempi
molto molto rapidi (nel caso in cui il codice sia molto lento). Hanno
connessioni di tipo locali e alcune celle sono di dimensioni maggiori con
complessità variabili. Posso avere celle con fnzioni dedicate che possono
trasformare la cella in complessa. Ad esempio potrei avere un moltiplicatore
hardware che è molto diverso dalle celle con porte AND e OR. Naturalmente non
avrò bisogno di molti moltiplicatori. Questi dispositivi sono molto versatili
che sono capaci di cambiare da un'applicazione all'altra semplicemente
riprogrammando il dispositivo. La complessità delle macchine si associa agli
algoritmi di routing: quelli che partono dalla descrizione della funzione per
arrivare ai blocchi che implementano tale funzioni e ai collegamenti necessari
a realizzarla.

Esistono dispositivi che incorporano delle CPU all'interno dell'FPGA mettendo a
disposizione la flessibilità della configurazione hardware con la possibilità
di eseguire del codice, controllato dal processore. Gli ecografi in cui vi sono
trasduttori piezoelettrici che funzionano da generatori e da sensori del
segnale. Si usa una FPGA che gestisce la parte veloce e la parte lenta invece
dalla CPU.

## Progettazione dispositivi a logica programmabile

Esistono diversi approcci tutti equivalenti perchè definiscono in maniera
univoca le funzioni del dispositivo.

1. *Disegno dello schematico (schematic entry)*. Il progettista che era abituato
   a realizzare componenti collegando le porte logiche scriveva lo schematico
   per rouscire a definire la funzione del dispositivo. è difficile da gestire
   qunado le dimensioni del problema sono elevate. È difficile effettuare un
   "copia e incolla" della funzione.
2. *Hardware Description Language (HDL)*: come dei linguaggi di programmazione
   che però non descrivono una sequenza temporale e logica di eventi ma una
   relazione logica tra variabili. è possibile usare quindi una forma testuale
   per descrivere qual è la funzione che si vuole ottenere.
3. *Flow chart*
4. *Macchina a stati*

### HDL

VHDL e Verilog. Non esistono cicli for e while ma solo descrizioni di stati e
relazioni logiche. è possibile utilizzarlo per implementare qualunque funzione.

Una volta descritto l'hardware dovranno esserci dei programmi che traducano la
descrizione nell'equivalente in chiusure di connessioni in grado di riprodurre
la funzione all'interno del dispositivo configurabile. Questa è un'operazione
simile a quella di compilazione per quanto riguarda il firmware.

### Debugging

Com'è possibile effettuare il debugging? Posso usre simulazioni funzionali e,
una volta scritte tutte le funzioni posso scrivere programmi che simulino la
funzione, ricreando in ambiente software, ad una serie di ingressi forniti,
cosa succede alle uscite del sistema. Quando facciamo le simulazioni non
sappiamo quanto ci vorrà per far cambiare lo stato d'attivazione di una linea
poichè le capacità sono diverse a seconda della capacità dei fili. Questa è una
simulazione funzionale perchè non tiene conto di questo. 

Una volta terminata la simulazione funzionale passo alla sintesi, definisco su
un particolare dispositivo quali sono le connessioni che devono essere
attivate. è la stessa cosa della compilazione per un particolare dispositivo.

Terminata la sintesi produco il circuito finale, viene inserito sul si singolo
chip. Questa è una fase di "linking", in cui dopo che ho definito la struttura
logica vado a definire

Routing: instradamento. L'algoritmo cerca le strade per collegare i vari
componenti.

Quando ho definito come chiudere le connessioni se stabilisco il dettaglio
hardware del dispositivo posso andare a comprendere qual è lo schematico del
dispositivo e fornire a spice che è un simulatore elettrico che mi permette di
andare a vedere se i tempi garantiti dall'implementazione soddisfano i
requisiti con precisione del nanosecondo. Questa è detta simulazione post
layout. è importante farla perchè potrei accorgermi di eventuali connessioni
errate che sono state assegnate dagli algoritmi di routing ed è possibile
andare a forzare altre soluzioni.
