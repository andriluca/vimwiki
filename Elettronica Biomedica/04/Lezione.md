# Lezione 4 --- Reti e comunicazione dati

Il concetto alla base della comunicazione è molto semplice: si vuole
trasmettere un dato da un sistema che lo genera ad un altro che lo riceve.
Esistono molti protocolli di comunicazione (IP e TCP). Si cercherà di capire il
senso del protocollo e come questi sono collegati insieme per fare in modo che
un PC collegato ad una rete WiFi possa trasmettere un dato ad un mainframe
situato in Australia.  Si prenderà in considerazione il sistema ISO-OSI: un
diagramma che costituisce la linea guida di questa sezione.

## Il problema

Una rete di calcolatori composta da vari nodi (i.e. ciascun calcolatore) in
grado di generare e/o ottenere informazioni. I nodi producono informazione che
dev'essere condivisa. Questa condivisione è possibile utilizzando degli
approcci topologici (geografici) che possono essere diversi tra loro. Il link è
il collegamento fisico tra due diversi nodi. Tra i mezzi fisici che possono
essere utilizzati nella realizzazione di questo collegamento è incluso il
classico filo di rame. Il collegamento tra due nodi non è una vera e propria
rete. Questa si realizza quando vi sono più di due nodi.

Possono esservi delle reti:

- *A mezzo condiviso*: si pensi, a titolo d'esempio, alla voce che trasmette
  l'informazione (i.e. suoni) attraverso un mezzo comune (i.e. l'aria) a tutti
  i i presenti in una stanza.
- *Punto a punto*: consentono a più unità di avere più connessioni. Alcuni nodi
  sono dei veri e propri centri di smistamento dell'informazione. In questo
  sistema la comuncazione tra i nodi può prendere strade diverse.

## Obiettivi

- *La condivisione dell'informazione*: uno dei motori più importanti.
- *La condivisioni delle risorse*: alcune risorse che possono essere costose
  nel dispositivo stesso e che vengono utilizzate occasionalmente possono
  essere condivise. Si pensi alla stampante: chi la possiede non la utilizza
  costantemente e può metterla in condivisione con altre persone dello stesso
  ufficio o abitazione per poter condividerne le funzionalità.
- *Comunicazione*: si pensi a Whatsapp, Skype, Teams.
- *Accesso a risorse remote*: Se si dispone di una risorsa remota posso
  accedervi lo stesso. Si pensi alle server farm in grado di immagazzinare
  tutte queste informazioni che vogliamo condividere. Questa è una cosa molto
  comoda nel caso in cui si perda un dispositivo.
- *Alta affidabilità*

## Classificazione delle reti

La prima classificazione è legata all'estensione di queste:

- LAN: Local Area Network. Hanno estensione pari all'edificio. Più vicini si
  trovano i dispositivi e maggiori sono le velocità di trasmissione.
- WAN: Wide Area Network, reti geografiche. Sono considerate pubbliche (i.e.
  utilizzate da tanti utenti diversi). La priorità in questo caso è la portata
  dell'informazione, la larghezza del canale.
- Reti di reti: realtà locali vengono interconnesse con reti di tipo
  geografico per arrivare poi a creare l'architettura che è alla base di
  internet che è costituito da reti locali, rappresentate da questo tipo di
  connessione con un unico mezzo di comunicazione e nodi che fanno parte di una
  rete più grande.

## Terminologia

- *Intenet*: reti di reti che possono essere tra di loro eterogenee.
- *Sottorete*: sottoinsieme di router e delle linee di trasmissione.
- *Rete*: riguarda tutti gli elementi. è la definizione più ampia.
- *Internetwork*: la collezione di più network.

- *Nodo*: le entità in grado di generare o di utilizzare informazioni.  I nodi
  inizialmente erano costituiti solamente da calcolatori general purpose ma,
  successivamente, anche sistemi per la gestione del traffico delle connessioni
  (switch e router) sono stati inclusi in questa categoria. Oltre a questi vi
  sono altri dispositivi che oggi sono diventati dei nodi: una stampante di
  rete, il NAS (Network Attached Storage), etc. Questi nodi possono essere
  anche strumenti di misura, monitor che riportano le informazioni alla
  caposala e nella cartella clinica elettronica dello stesso paziente. Oggi
  anche solo degli smart switch (i.e. Shelly) che si installano facilmente
  nelle case sono dei nodi in questa rete.
- *Link*: elementi che consentono la trasmissione fisica del dato tra un nodo e
  il successivo.
- *Topologia di rete*: il modello geometrico finalizzato a rappresentare le
  relazioni di connettività, fisica o logica, tra gli elementi costituenti la
  rete (i.e. nodi).

  
## Topologia di rete

Esistono varie tipologie di rete. Queste vengono classificate in base alla
tecnologia trasmissiva e alla topologia, in:

- *Reti di tipo broadcast*: hanno un unico canale di comunicazione condiviso.
  Ciascun dispositivo comunica con tutti gli altri dispositivi connessi alla
  rete. È necessario che l'informazione trasmessa su questa rete sia disposta
  di un identificativo (i.e. l'indirizzo) riguardante il destinatario.
  L'informazione però viene sempre ricevuta da tutti i nodi di questa rete.
  L'indirizzo del destinatario allegato al pacchetto fa in modo che gli altri
  ignorino quest'informazione che è però sempre condivisa.  Esiste per questo
  motivo un significativo problema che riguarda la sicurezza e privacy dei
  dati. Per funzionare correttamente c'è bisogno sia dell'indirizzo ma anche un
  controllo del mezzo condiviso. È opportuno vi siano delle regole che facciano
  in modo che l'uso della risorsa unica avvenga in modo corretto. Se due nodi
  volessero comunicare assieme, l'informazione viene distorta ad esempio. Per
  questo esistono i media access control (aka MAC), protocolli che vengono
  utilizzati nelle reti più diffuse.
- *Reti di tipo punto a punto*. Queste consistono di un insieme di connessioni
  fra coppie di elaboratori. Il modem ha un collegamento punto a punto con la
  centrale telefonica che permette di collegare la rete broadcast domestica ad
  una rete più grande. Non è d'interesse comprendere da chi provenga
  l'informazione in questo caso ma come fare in modo che questa arrivi al
  destinatario, ovunque questo si trovi all'interno della rete. Nel pacchetto
  dev'essere inserita la direzione che il pacchetto deve seguire. Questo
  avviene mediante degli algoritmi di routing ad-hoc.  Il pacchetto, per poter
  giungere a destinazione, può dover attraversare uno o più nodi intermedi.
  Nel caso delle reti punto a punto le reti sono create da link che non
  condividono l'informazione globalmente. Il problema vero e proprio si sposta
  ora sull'instradamento: cercare il percorso migliore che il pacchetto può
  seguire per giungere al destinatario. La complessità degli algoritmi cresce
  esponenzialmente con il numero di collegamenti disponibili, diventando presto
  un problema che non può essere esaustivo con algoritmi che analizzano tutti i
  possibili percorsi perchè sono troppe possibilità e si ricorre all'euristica
  per cercare una soluzione abbastanza buona rispetto al tempo necessario e
  alla potenza di calcolo per determinarla. Sarà necessario aggiungere
  dell'informazione (overhead) per poter attivare la ricezione del dato da
  parte del nodo interessato. Si incapsula quindi l'informazione in un
  pacchetto per poter essere indirizzato correttamente.


## Topologia

![Rappresentazione di una rete WAN.](Figure/WAN.png)

Bisogna tener conto che, anche se nella maggior parte dei casi le reti LAN
utilizzano tecnologia di comunicazione broadcast, non è detto sia una
condizione necessaria. Esistono sistemi di comunicazione che non utilizzano un
mezzo condiviso e che possono essere utilizzati per creare delle reti LAN.

Per quanto riguarda la parte centrale della rete abbiamo visto che anzichè
avere dei nodi che comunicano tra loro attraverso un mezzo condiviso vi sono
nodi che comunicano con connessioni destinati esclusivamente alla connessione
di due elementi alla volta. In questo caso gli elementi chiamati router sono
tra loro interconnessi da una serie di collegamenti punto a punto. Per fare in
modo che la comunicazione possa giungere a nodi che non sono tra loro
interconnessi da una linea, è necessario che, attraverso questi nodi, sia in
grado non solo di generare e ricevere informazioni ma anche di fare il lavoro
di instradamento del pacchetto di informazione nel caso in cui il nodo non sia
il destinatario dell'informazione stessa. L'informazione dovrà viaggiare quindi
attraverso altri nodi che riceveranno l'informazione, scopriranno che questa
non è destinata a loro e la reinvieranno su un altro link per avvicinarsi al
destinatario. Il processo di instradamento richiede un certo tipo di analisi ed
ottimizzazione (e.g. gestione del percorso migliore per inviare velocemente il
pacchetto).  Vi è una certa ridondanza in queste reti: posso seguire più
percorsi per arrivare alla stessa destinazione. Vi sono molteplici vantaggi
dovuti alla ridondanza delle connessioni:

- *Migliore distribuzione del traffico dell'informazione*. Se una strada è
  trafficata di pacchetti posso deviare quelli nuovi su una strada secondaria
  che è più lunga ma essendo più libera può garantire un utilizzo migliore
  delle risorse e dei tempi di trasmissione più brevi.
- *Robustezza*: se avessi un danno ad un link, la ridondanza permette al
  pacchetto di raggiungere comunque la destinazione.

## Topologia della rete
  
*Rete broadcast* hanno un unico canale di comunicazione condiviso. È necessario
che l'informazione trasmessa su questa rete sia disposta di un identificativo
riguardante il destinatario in modo tale che solo quello riceva il messaggio.
L'informazione però viene sempre ricevuta da tutti coloro che fanno parte di
quella rete. L'indirizzo del destinatario allegato al pacchetto fa in modo che
gli altri ignorino quest'informazione che è però sempre condivisa. Esiste un
problema molto importante che riguarda la sicurezza e privacy dei dati. Per
funzionare correttamente c'è bisogno sia dell'indirizzo ma anche un controllo
del mezzo condiviso. È opportuno vi siano delle regole che facciano in modo che
l'uso della risorsa unica avvenga in modo corretto. Se due nodi volessero
comunicare assieme l'informazione viene distorta ad esempio. Per questo
esistono i media access control, protocolli che vengono utilizzate in reti più
diffuse

*Rete punto a punto*: Sono caratterizzate dalla necessità di attraversare più
nodi per arrivare a destinazione. Nel caso delle reti punto a punto le reti
sono create da link che non condividono l'informazione. Il problema vero e
proprio si sposta sull'instradamento, cercare il percorso migliore che possiamo
far seguire al pacchetto per giungere al destinatario. La complessità degli
algoritmi cresce esponenzialmente con il numero di collegamenti disponibili,
diventando presto un problema che non può essere esaustivo con algoritmi che
analizzano tutti i possibili percorsi perchè sono troppe possibilità e si
ricorre all'euristica per cercare una soluzione abbastanza buona rispetto al
tempo necessario e alla potenza di calcolo per determinarla.

## Realizzare fisicamente la comunicazione

Il problema da risolvere è il seguente: come fare in modo che l'informazione
viaggi da nodo a nodo.

Esistono diversi livelli con i quali possiamo considerare come l'informazione
viaggi attraverso un sistema. Qui si parla del livello fisico, ossia come viene
realizzato elettricamente il collegamento e l'instradamento dell'informazione
nelle reti punto a punto.

- *Commutazione di circuito*: Un sistema ben noto dal passato (i.e. reti
  telefoniche). Il sistema funziona con l'esistenza di diverse linee di
  collegamento fisiche (i.e. grossi cavi), ciascuna coppia di fili faceva
  passare una conversazione. Ogni numero che veniva digitato faceva commutare
  MUX per creare collegamento fisico per attaccare tra di loro le coppie di
  fili e realizzare un collegamento tra i due capi della linea telefonica.
  L'esempio lampante è quello della centralinista che deve smistare una
  telefonata. Questa collega il telefono di casa ad una coppia di fili
  disponibili per chiamare una città lontana. Il sistema offre una serie di
  vantaggi e di svantaggi. *Vantaggi*: una volta che ho stabilito il
  collegamento fisico ho una garanzia della capacità trasmissiva del canale. Ho
  collegato fisicamente i fili e quelli possono essere usati con una certa
  banda passante (l'informazione che posso far passare dipende dalle
  caratteristiche della linea). Una volta che essa è stata realizzata per
  garantire il passaggio dell'informazione relativa alla voce, il collegamento
  può essere usato solo da chi genera e chi utilizza quella connessione. Questo
  è un vantaggio per gli utenti: la comunicazione di una qualità ben definita è
  garantita. È uno *svantaggio* per l'operatore telefonico perchè significa che
  ho un numero finito di fili e se creo un collegamento dedico una coppia di
  fili a quel collegamento indipendentemente  da quale sia la quantità
  d'informazione che viene fatta passare nel filo. Se facciamo passare tante
  informazioni lo uso bene ma altrimenti faccio passare delle infomrazioni
  ridotte, sprecando la risorsa del canale di comunicazione.  Se poi si satura
  il numero di linee disponibili non avrò più la possibilità di comunicare con
  altri.
- *Commutazione di pacchetto*: Questo è un sistema che è stato sviluppato man
  mano che la tecnologia ha permesso di realizzare dispositivi digitali
  sofisticati a basso costo. Si utilizza un sistema di comunicazione nel quale
  le informazioni vengono suddivise in elementi chiamati pacchetti: "blocchi di
  dati" con una dimensione minima e una massima definita (NB: se volessi
  trasmettere un contenuto informativo più grande della dimensione massima del
  pacchetto conviene ripartirla). Una volta che l'informazione è incapsulata
  nel pacchetto non si necessita più un mezzo di comunicazione avente una gran
  quantità di fili ma i pacchetti saranno inseriti nello stesso mezzo di
  trasmissione. Il *modello* al quale questo sistema fa riferimento è quello
  *postale*: quando voglio mandare una lettera prendo l'informazione e la
  incapsulo in un involucro che ha sovrapposto l'indirizzo, una codifica che
  dice qual è il destinatario dell'informazione. La lettera poi condivide una
  parte del percorso con altre lettere e un'altra parte con altre. Questo
  sistema di comunicazione cerca di ottimizzare l'efficienza del sistema di
  trasporto: se devo trasportare una lettera da Milano a Roma metto tutte
  quelle aventi quella particolare destinazione sullo stesso mezzo e si fa in
  modo che questo l'unico che le trasporti in quel luogo. Il vantaggio è
  l'efficienza: se non vengono inviate informazioni non viene occupato il
  canale inutilmente. Il silenzio lascia spazio ad altri che stanno
  comunicando. Il canale viene quindi utilizzato in modo molto più efficiente
  poichè passa solo l'informazione che deve passare. Lo *svantaggio* è che non
  ho più la garanzia della quantità di informazioni che posso far passare. È
  difficile gestire ciò perchè se ho molte nodi che generano pacchetti sulla
  stessa linea posso andare a saturare il canale di comunicazione. Mentre con
  la linea telefonica di una volta, se la ricezione è buona e mi trovo via
  cavo, avrò sempre una comunicazione ininterrotta. Nel momento in cui uso
  Skype o altri software VoIP se vi sono molte persone che utilizzano la rete,
  la comunicazione diventa difficile e non si ha più la garanzia che i
  pacchetti arrivino a destinazione e che si possa comunicare in maniera
  intellegibile. Questo sistema è talmente efficiente da essere diventato lo
  standard de facto delle comunicazioni: molti sistemi telefonici attuali,
  nonostante siano state messe a punto delle strategie per garantire la
  comunicazione del dato, si basano su sistemi a commutazione di pacchetto. La
  possibilità di utilizzare algoritmi di instradamento sempre più sofisticati
  che possono garantire determinate bande a certi tipi di comunicazione,
  togliendole ad altri sistemi con priorità più bassa, è resa possibile da
  elevate capacità di calcolo in tempo reale e potenti processori per non avere
  ritardi nella trasmissione.
  
Router: entità che ha come scopo quello di prendere l'informazione e rigirarla
all'uscita per farlo raggiungere a destinazione. Questi sistemi utilizzano un
approccio del genere: ricevono il pacchetto, lo memorizzano per poi leggerlo e
decidere cosa farne. Una volta deciso il suo destino, controlla se la linea è
disponibile o impegnata nella trasmissione e non appena è libera inoltra il
messaggio. Un buffer interno è indispensabile proprio perchè vi è una fase di
attesa in cui altri dispositivi possono intervenire nella scrittura sul mezzo
di comunicazione. Questo tipo di collegamento è di tipo punto a punto e vengono
dette "store and forward" perchè memorizzano e poi inoltrano il pacchetto.
  
## Tipi di servizio

Si passa a ragionare su un altro livello. Concetto diverso è invece relativo ai
tipi di servizio che il sistema di comunicazione offre chi ne fa utilizzo.

Com'è possibile che la rete telefonica si appoggia sul sistema a commutazione
di pacchetti? Nel modello precedente, il servizio telefonico connetteva il
circuito ma ora connette il pacchetto. Questa è la differenza che sussiste tra
quello che l'utente percepisce come servizio e come questo servizio viene
implementato dal sistema che realizza fisicamente la nostra rete. Queste due
cose possono essere separate: sono **indipendenti**. Skype digitalizza la voce
e la invia attraverso un sistema a commutazione di pacchetto ma lo si utilizza
esattamente come un servizio orientato alla connessione che *fisicamente* erano
realizzati come servizi orientati alla connessione (i.e. servizi telefonici di
un tempo). Posso definire in maniera indipendente quali sono le qualità e le caratteristiche del servizio che erogo rispetto a come poi fisicamente nè implemento il funzionamento. Posso dire cosa voglio ottenere come circuito digitale (i.e. *caratteristica funzionale*) e poi stabilire se concretizzarlo con un dispositivo a funzionalità programmata, con FPGA o con reti logiche (i.e. implementazione). 

Servizi: come viene erogata la prestazione all'utente. Questi determinano quali
sono le caratteristiche ideali del sistema che sta al di sotto.

È possibile avere servizi orientati alla connessione implementati attraverso
sistemi a commutazione di pacchetto ma anche servizi non orientati alla
connessione ma che implementano tecnologia orientata alla connessione.

Esistono servizi:

- *Orientati alla connessione*: servizi che hanno la caratteristica del
  servizio telefonico. Questo sistema ha fasi ben definita di apertura della
  connessione, invio del dato e chiusura della connessione. Nel sistema
  telefonico faccio un numero di telefono, aspetto il segnale di libero e una
  volta che il canale è aperto lo uso per far passare i dati. Quando è finita
  la connessione, il canale viene chiuso. È importante che quando si utilizza
  un servizio orientato alla connessione i pacchetti abbiano tempi di trasporto
  paragonabili (i.e. compiano lo stesso percorso) e la sequenza stessa con cui
  vengono trasmessi sia mantenuta (questo perchè la sequenza stessa è
  portatrice d'informazione). Si vuole garantire che l'ordinamento dei
  pacchetti sia first in, first out (aka FIFO) facendo seguire ai pacchetti un
  percorso appropriato.
- *Senza connessione*: Sistema avente come modello il sistema postale. Messaggi
  diversi possono seguire percorsi diversi e non ho alcuna garanzia
  sull'ordinamento dei messaggi. Posso mandare tre cartoline e ci mettono tempi
  diversi per arrivare a destinazione.

È importante dire che il sottostrato di comunicazione fisica può essere ben
diverso rispetto al servizio offerto.

### Qualità del servizio

Per quello che riguarda le qualità del servizio vi sono tante qualità che sono codificate e che hanno un senso definito e che stabilisono come un particolare protocollo può soddisfare o meno i requisiti dell'applicazione. 

Le più importanti nella parte della comunicazione dati sono:

- *Affidabilità*: servizi che possono garantire che l'informazione arrivi a
  destinazione.
- *Con garanzie sul ritardo*: conosco anche il tempo massimo che un certo
  pacchetto necessiterà per raggiungere la destinazione.
- *Con garanzie sul jitter del ritardo*: Relativo alla variabilità del ritardo.
  Se io mando un pacchetto con una certa costanza (a una frequenza definita)
  questo viene ricevuto con un certo ritardo ad intervalli di tempo uguali a
  quelli che sono stati usati per inviare il messaggio.  La variabilità
  dell'intervallo di tempo è un'informazione importante.

#### Affidabilità

Dal punto di vista biomedicale vi sono strategie utili per fare in modo che vi
sia una garanzia sulla possibilità di avere riscontro sull'effettiva
comunicazione del dato ma nel momento in cui abbiamo un sistema di
comunicazione dati questo significa avere una distanza tra le due unità cui è
associata una vulnerabilità intrinseca delle linee di comunicazione. Se il cavo
venisse interrotto bruscamente la connessione sussisterebbe. Un sistema
affidabile permette di far fronte alle anomalie che si possono verificare
poichè potrebbero essere facilmente identificate.

L'affidabilità viene garantita attraverso verifiche che hanno un costo. Il
primo approccio sviluppato in passato per garantire affidabilità dei servizi
che non erano intrinsecamente affidabili è stato quello della ricevuta di
ritorno (una cartolina allegata al messaggio che vuole essere consegnato che
verrà firmata dal destinatario e che notificherà l'avvenuta consegna). Si
utilizza sempre lo stesso mezzo di comunicazione per trasmettere al mittente
l'acknowledgement relativo alla ricezione del messaggio da lui inviato. Questo
meccanismo complica il sistema: il mittente invia una lettera con ricevuta di
ritorno ed è come se ne inviasse due; la cartolina giunta a destinazione viene
rispedita al mittente; se il mittente non riceve indietro la cartolina non sa
se è dovuto allo smarrimento del messaggio e della cartolina nella prima
spedizione oppure della sola cartolina nella seconda. Il mittente dovrà quindi
inviare nuovamente il messaggio, opportunamente etichettato come copia, con la
cartolina. È necessario etichettarlo in questo modo perchè se il primo
messaggio inviato venisse recapitato, si potrebbe rispondere due volte alla
ricevuta di ritorno relativa allo stesso messaggio, incombendo in problemi di
varia natura (e.g. doppi pagamenti o simili).  In questo modo, nei nostri
sistemi, si introduce l'overhead: esso non aggiunge ulteriore informazione ma
ha come unico scopo quello di garantire la qualità e l'erogazione di certi
servizi che non sono erogati dal sistema di comunicazione primario. Ogni
modifica delle caratteristiche del sistema di comunicazione corrisponde ad un
costo: l'utilizzo del sistema di comunicazione non per trasmettere dati ma per
trasmettere gli indirizzi che non costituiscono un dato vero e proprio ma che
occupano la banda. 

Se un livello non offrisse alcun servizio affidabile, se tale funzione è
desiderata, dovrà essere fornita da almeno uno dei livelli superiori. Cosa sono i livelli superiori? Se il servizio postale non può erogare un servizio affidabile, devo costruire degli artifici che lo utilizzano in maniera diversa in modo da migliorarne la qualità (e.g. con ricevuta di ritorno). Ciò però non limita in alcun modo il trasporto di lettere senza ricevuta di ritorno perchè queste viaggiano sempre nello stesso strato di comunicazione. Ciò che viene fatto è quindi un modo di usare questo sistema di comunicazione fisico che garantisca servizi più evoluti e diversi che vengono erogati verso l'alto.

Uso quindi il sistema per comunicare queste informazioni anche se non sono di
vero interesse per la comunicazione stessa ma per il suo corretto
indirizzamento. Costruisco sul sistema di comunicazione fisico un metodo per
implementare quel servizio.

Si vedono alcuni esempi di cosa è necessario avere a seconda dell'applicazione
che si vuole realizzare:

- *Collegamento affidabile e orientato alla connessione*: e.g. trasferimento di
  file. Avrò bisogno di un sistema di comunicazione che sia affidabile e
  connection oriented. Se ho un file ho una sequenza ben definita di byte e
  questa dev'essere garantita. Non posso mischiare L'insieme di bytes. Inoltre
  vorrei mandare tutti i bytes a destinazione, altrimenti il file trasferito
  non sarà identico a quello inviato inizialmento.
- *Collegamento non affidabile orientato alla connessione*: L'esempio è quello
  della trasmissione voce e video tramite VoIP. Sarà necessario mantenere la
  sequenza ma non ho la necessità che ci sia tutto il contenuto, ma che sia
  trasmesso per tempo. È meglio avere una comunicazione discontinua piuttosto
  che finire la comunicazione mezz'ora dopo.
- *Collegamento non affidabile e non orientato alla connessione*: L'esempio è
  quello di inviare messaggi pubblicitari di posta elettronica. Si inviano
  pacchetti dati e se non arrivano non è drammatico. 
- *Collegamento affidabile e non orientato alla connessione*: L'esempio è
  quello di quando si vuole reinviare la cartolina di ritorno. La cartolina
  infatti non è costituita da dati di per se ma solo dall'indirizzo del
  mittente. Non è necessario che il collegamento sia orientato alla connessione
  perchè il pacchetto non ha bisogno di un particolare ordinamento ma
  dev'essere affidabile in quanto deve giungere a destinazione.

## Il modello di ISO-OSI

Per garantire che possiamo utilizzare dei sistemi che forniscono certi tipi di
servizi per poi erogare servizi migliori, vengono realizzati dei sistemi che
vengono definiti dalla modalità che viene mostrata.

Quando si fa riferimento all'esempio delle poste che inviano il messaggio (che
sta sotto) e la ricevuta di ritorno (che sta sopra), "sotto" e "sopra" fanno
capo a due concetti costruiti l'uno sull'altro. Il modello che viene utilizzato
quando si considera la comunicazione avviene su tanti livelli differenti. Per
riuscire a caratterizzare correttamente la comunicazione è necessario definire
quali sono i livelli che devono essere implementati e quali sono i servizi che
devono essere erogati da tali livelli. La modalità con cui si realizzano i
sistemi di comunicazione è quella MOSTRATA. Host1 e Host2 sono le unità che
vogliono comunicare tra loro (uno genera e l'altro riceve). Questi sistemi
comunicano tra di loro attraverso un loro protocollo. Per effettuare la
comunicazione però, in realtà, non è detto che il protocollo di comunicazione
abbia un collegamento fisico diretto. Il protocollo è una serie di regole che
definiscono il linguaggio con cui i due host comunicano ma che non determinano
necessariamente come quest'informazione venga realmente trasmessa. Per fare in
modo di trasmettere quest'informazione, l'host 1 che vuole parlare con host 2
deve fare questa cosa tramite un servizio che viene erogato da un livello
sottostante. Questo comunicherà a sua volta con il sistema sottostante omologo
che è presente nell'host 2. L'esempio classico è quello della comunicazione
verbale: quando le persone comunicano mettiamo insieme molti protocolli e
sistemi di comuncazione diversi tra loro. Una parte è l'utilizzo della voce che
trasmette frequenze attraverso variazioni di pressione che si propagano
nell'aria. La voce può portare informazioni che possono essere codificate in
maniera diversa: è possibile avere lingue diverse. La lingua non è legata alla
voce però ma che possiede delle regole grammaticali. Trasformando la lingua in
una serie di fonemi e chiedendo a sistemi sottostanti in grado di trasportare
l'informazione (i.e. sistema vocale) e alle orecchie di tradurre le vibrazioni
nei suoni ricevuti, sarà possibile ritradurre grazie alla grammatica la parola.
Se conosco e faccio parlare un sistema ad alto livello con lingua italiana,
cambiando quello che sta sopra e sta sotto, ho che i due sistemi sono tra loro
indipendenti. Il meccanismo di trasmissione a fonemi funziona anche con
qualunque altra lingua. Posso cambiare lingua usando sempre il sistema
sottotante ossia la trasmissione che è quello di trasmissione dell'informazione
audio, mettendo sopra delle regole di comunicazione diverse. Posso cambiare
grammatica per far comunicare i due host mantenendo l'utilizzo dei servizi
offerti dalla trasmissione del fonema. La possibilità di lavorare a livelli
crea molti vantaggi perchè se conosco che un sistema di comunicazione funziona
trasmettendo il fonema posso poi dire che se volessi andare a distanza basterà
creare il telefono che trasporta la voce non più attraverso la vibrazione di
pressione dell'aria ma tramite la conversione della vibrazione in un segnale
elettrico e poi una riconversione del segnale elettrico in un segnale acustico.
Posso quindi implementare servizi sottostanti alla lingua italiana che
permettono la trasmissione dell'informazione con metodi e su distanze diverse
senza dover cambiare una virgola del protocollo che ci sta sopra. La
comunicazione avviene attraverso una molteplicità di passaggi attraverso questi
livelli che garantiscono la possibilità di creare erogazione di servizi
cambiando protocolli (quindi regole) sottostanti senza dover cambiare quello
che ci sta sopra. L'utente ha la potenzialità di passare dal telefono con
tecnologia a commutazione di circuito all'uso di Skype con trasmissione di
pacchetti attraverso la rete TCP/IP senza nemmeno rendersene conto! Questo è
dovuto al fatto che i servizi che si volgliono offrire sono sempre gli stessi
indipendentemente dall'implementazione dei livelli più bassi dello schema.

Per riuscire a caratterizzare in maniera concreta tutti gli aspetti che possono
riguardare la comunicazione dei dati, si è deciso di definire un modello (aka
ISO-OSI). La necessità era quella di identificare, ad un livello astratto,
quali sono le possibili funzioni e caratteristiche dei sistemi di comunicazione
in modo da isolare livelli diversi che garantiscono funzioni via via più
evolute da quello più basso a quello più alto sia per poter capire come
funziona un sistema e definirne le caratteristiche in modo adeguato che per
poter definire in maniera chiara quali sono le funzioni erogate da un sistema e
quali dall'altro. Ciò consente di poter modificare in seguito l'implementazione
di livelli specifici senza dover modificare il restante. Se ogni volta che
venisse cambiata la codifica dei bit sul sistema di comunicazione GSM dovesse
essere modificata anche la grammatica italiana sarebbe impossibile la
realizzazione di un sistema di comunicazione. 

Il successo dei sistemi di comunicazione è quello della possibilità di
incapsulare e definire opportunamente i diversi livelli. Una volta fatto ciò, è
possibile definire le tecnologie implementate andando a cambiare come viene
realizzato uno di questi strati. Tutto il resto non cambia.

Il modello ISO-OSI ha identificato sette diversi livelli. Questi vengono
utilizzati per garantire la comunicazione. I livelli vanno dal basso verso
l'alto con livelli d'astrazione e complessità del protocollo man mano che ci si
sposta verso l'alto. Il settimo livello è detto application, ossia quel livello
che utilizza effettivamente l'informazione (l'utente dell'informazione). I
livelli che stanno sotto va più nel dettaglio fino a giungere al livello uno
chiamato anche livello fisico. è importante notare che il livello fisico è
l'unico livello nel quale avviene la comunicazione. Tutto il resto è
l'applicazione di una serie di protocolli che cambiano il contenuto
d'informazione e serve per implementare le varie funzioni ma la trasmissione
avviene sempre e soltanto a quello che è chiamato livello fisico. Quando il
nostro computer usa Skype, vi è l'application che digitalizza la voce e la
manda al destinatario. La strada che viene compiuta è determinata da un
protocollo [continua da qui].


7 livelli. Vanno dall'alto verso il basso con complessità crescente verso
l'alto.Application: utente dell'applicazione. Il livello 1 è detto livello
fisico. Questo livello è quello in cui realmente avviene la comunicazione.
Quello che sta sopra è un insieme di protocolli che cambia l'informazione che
serve per implementare le varie funzioni. La trasmissione avviene sempre e
soltanto tramite il mezzo fisico però. I due host hanno tutti questi livelli
implementati. C'è però una presenza di subnet interni che si fermano solo ai
primi livelli: quando ho deciso di trasmettere la voce non mi serve trasmettere
anche la grammatica italiana per trasmettere solo dei byte e non interessa
saperlo. Hanno solo i primi tre livelli implementati. Quando definisco un
livello questo è definito da un protocollo di comunicazione che definisce come
i due sistemi allo stesso livello comunicano tra di loro. Questo livello si
interfaccia con i livelli sottostanti e sovrastanti con l'erogazione del
servizio.

La strada dell'informazione, anche se logicamente avviene tra i vari livelli,
in realtà sale e scende nei vari membri per giungere poi a destinazione.

Questi sette livelli garantiscono la rappresentazione di qualsiasi sistema di
comunicazione possibile.  Lo strato "network" consente di fare l'instradamento.
Quindi come lo schema generalizzato del sistema di misura posso considerare
sottoinsiemi a seconda delle applicazioni.

Spostandosi verso il basso i servizi richiederanno requisiti sempre più
stringenti. Potrei dover suddividere il messaggio in diversi messaggi. H3 fa in
modo che poi il pacchetto a livello 4 sia ricostruito in modo consistente.
Quando arrivo a livello fisico potrei avere delle regole ancor più stringenti
per far avvenire la comunicazione in modo adeguato. Molto spesso si utilizzano
sia header che tail per far capire quando è stata ultimata la trasmissione.
Tutto ciò ha un costo: il messaggio M viene sommerso dall'overhead che pesa e
utilizza il mezzo di comunicazione. è necessario che quest'overhead sia ridotto
al minimo per rendere la comunicazione efficiente.

## Concetti chiave

- Servizio: ciò che viene offerto ai livelli sovrastanti.

	- Ethernet: standard di comunicazione di rete basata su cavo. Arriviamo
	  ad una velocità nell'ordine di Gb/s. Il cavo ethernet è costituito da
	  8 doppini. Ethernet è una comunicazione locale. Non ha il livello 3
	  che garantisce ???. Inaffidabile. Il messaggio non passa al di là
	  della sottorete. La comunicazione è uni/multi/broadcast: a un utente
	  singolo, tutti gli utenti o a un sottoinsieme di questi.
	  L'affidabilità è demandata a livelli successivi perche questa costa
	  in termini d'utilizzo della rete. Se la mettessi a livelli bassi
	  avrei un costo alto anche quando questo costo non è richiesto. Se
	  implemento l'affidabilità a livello alto spenderei il costo solo dove
	  è effettivamente richiesto.
	- IP: protocollo che definisce internet. L'internet utilizza questo
	  protocollo. Non affidabile, end-to-end (invio di un pacchetto tra due
	  unità), unicast (in una direzione sola).
	- TCP: protocollo di livello 4 (transport). è affidabile (gestisce le
	  ricevute di ritorno), end-to-end, bidirezionale, byte stream
	  (connection oriented, orientato alla connessione), quindi posso
	  buttare dentro i dati e questi vengono mandati dall'altra parte.

- Interfaccia di servizio: la parte del physical layer è realizzata fisicamente
  ma la codifica del dato viene fatta dai processori delle macchine. Uso delle
  funizoni di libreria (API) che posso chiamare per accedere a questi servizi.
  Questi sono utilizzate per aprire e chiudere comunicazioni.

## Internet - TCP/IP

Ha origine con ARPANET, attivato dal DoD americano. Il problema era: il sistema
telefonico va bene per trasmettere dati ma se qualcuno interrompe le linee
abbiamo bisogno di qualcosa d'altro. L'idea che è stata alla base era quella di
interconnettere nodi in maniera indipendente dai gestori telefonici. Non c'era
più un ente che doveva smistare l'informazione ma ogni nodo poteva fare il suo
lavoro anche in seguito a malfunzionamenti. La rete trovava il percorso da sola
per giungere a destinazione anche in casi di malfunzionamenti. Bisogna capire
che questo sistema doveva avere caratteristiche per essere distribuito. La cosa
bella del sistema è che definendo solo i livelli centrali, lascio liberi i
livelli sotto e quindi posso cambiarlo senza che questo modifichi il livello
IP. I protocolli sono la lingua che parlano le macchine. Quindi cambiando solo
le implementazioni basse i sistemi continuano lo stesso a funzionare. Questo
crea vantaggi e svantaggi: IPv6 e IPv4. Il numero di byte utilizzato per
definire l'indirizzo di ogni singolo host connesso alla rete era sufficiente
allora ma ora non lo è più. Se si passasse in IPv6 vi sarebbero macchine che
non funzionerebbero correttamente.

Il TCP/IP è diverso da OSI perchè stabilito precedentemente al modello OSI.

Modello a clessidra: a livello basso abbiamo ???

## Link di comunicazione

Conduttore in rame:

- Doppino telefonico: due fili isolati vengono arrotolati. Si utilizzano perchè se abbiamo un campo elettrico variabile questo induce una corrente all'interno dei fili. Ciascuna spira adiacente è orientata in modo opposto rispetto a quelle accanto, quindi annullerebbe il campo magnetico. Ethernet utilizza 4 di queste coppie. 
- Doppino schermato: uno schermo metallico fornisce un'ulteriore shermatura da disturbi esterni.
- Cavo coassiale: cavo dell'antenna.

Altri mezzi:

- Fibre ottiche: nei grandi centri la rete dati si sta spostando su fibra con
  vantaggio una grandissima larghezza di banda e un'immunità ai disturbi.
- Onde radio
- Collegamenti via satelliti
- Microonde

## Collegamento fisico

- codifica

## Reti di calcolatori: collegamenti punto a punto

La comunicazione di questi sistemi.

- Seriali: mando i bit in sequenza su un singolo filo.
- Paralleli: come il sistema di comunicaizone bus 
  
A parità di prestazioni ho che se devo andare più lontano avrò bisogno di cavi
grossi per la comunicazione in parallelo. Per la seriale i cavi son piccoli
pagando però sulla trasmissione che deve avvenire in sequenza con certi tempi.

### Sistemi di comunicazioni seriale

- Sincroni: trasmettono anche l'informazione relativa al tempo. Se noi diciamo
  che la comunicazione seriale dev'essere implementata in sequenza devo essere
  accurato sulla definizione del tempo. I due orologi dei due sistemi avranno
  una differenza di performance. Quindi ho due strategie: aggiungo un filo e
  mando il clock (seriale sincrono)
- Asincroni: Fanno a meno del filo in più del clock e mettono in pratica
  strategie per evitare che vi siano delle asincronie dei clock delle macchine.

#### Esempio di interfaccia sincrona: SPI

è un protocollo di trasmissione sincrona. Le periferiche sfruttano pochi fili
che però per la maggior parte delle applicazioni sono sufficienti. SPI non è
molto ben definita. è fatta da un massimo di 4 linee: CS, CLK e una o due linee
di dati a seconda che sia bidirezionale contemporanea. Il protocollo non
utilizza transceiver (dispositivi che adeguano i livelli di tensione analogica
a quelli standard per la comunicazione) ??? Sistemi di questo tipo hanno
bisogno di definire chi è il master e chi è lo slave.

#### Esempio di interfaccia asincrona: RS232

Uno dei sistemi più utilizzati a livello dei sistemi biomedicali. è uno
standard più rigoroso che definisce interfaccia di comunicazione tra elementi
che possono essere esterni l'uno all'altro.

Perchè si usano tensioni così alte? per migliorare il rapporto segnale rumore.

è possibile vedere nella rappresentazione come i dati vengono organizzati. la
RS232 permette di usare 7 bit. Come si codifica il bit? Se volessimo scriver
eil numero 48 (in binario 0b00110000) a 9600 bps. Quando so qual è la velocità
di trasmissione posso ottenere la durata di ogni singolo bit. In questa
comunicazione non abbiamo un clock in comune. I fili vengono utilizzati per
trasmettere una codifica binario e non ho un modo per dire che non sta
trasmettendo nulla. Quando non sto trasmettendo si manteine costante, poi
diventa high. Bisogna mettersi d'accordo sulla velocità di trasmissione. Il
trasmettitore e il ricevitore devono sapere la tempistica di tramissione di un
singolo bit. Le macchine devono sapere a priore la velocità di tramsissione a
priori. Se è uno stndard a velocità singola non ho problemi. Come faccio ad
iniziare a trasmettere? Se ci siamo accordati ad una velocità di 1 bps: chi
trasmette ad ogni secondo trasmetterà un bit. L'altro appena vede accesa la
luce. Il problema sta nell'identificare lo stato di non trasmissione
indipendentemente dalla trasmissione dello stato 0. Essendo il filo sempre
quello dovrò aumentare la complessità aumentando il tempo per trasmettere. Vi
sarà un bit di start che garantisce che dall'altra parte il ricevitore capisca
quando cominci la trasmissione. Dall'altra parte chi trasmette fa un lampeggio
con la pila (i.e. bit di start). Abbiamo bisogno di mettere degli stop
codificato come qualcosa trsmesso di opposto al bit di start. il bit di stop è
-12V. Se volessi trasmettere molti HIGH dovrò sempre lasciare la luce accesa e
chi riceve non sa se ha smesso di trasferire bit o no. Man mano che la
comunicazione va avanti gli orologi possono essere sfalsati. L'errore del clock
ha una relazione integrale. Più passa il tempo e maggiore è l'errore. Non posso
andare avanti all'infinito con questa RS232 e dovrò determinare la larghezza
massima del frame per poter trasmettere correttamente. all'inizio dell'utilizzo
venivano inviati 8 bit per volta. Per 3 bit di payload ho due bit di overhead.
Oltre al bit di start e di stop viene spesso aggiunto un bit di parità. Questa
è una delle strategie per identificare se durante la trasmissione è avvenuto un
errore o meno. Non solo si massimizza l'SNR ma se viene cambiato lo stato di un bit può essere catastrofico a seconda di dove si trovi. Questa strategia permette di identificare l'errore. Parità: conta se il numero degli high è pari o dispari. Quindi in questo modo invio un bit per identificare se il numero di HIGH è pari o dispari. Se per qualche motivo un bit è stato alterato verrà registrato dal bit di parità. Non vengono identificate le alterazioni di più bit.

Transceiver: trasla i livelli di tensione utilizzati dal sistema di
comunicazioni in quelli utilizzati dall'elettronica digitale (quindi dei
microcontrollori) che utilizza quei segnali.

## Periferiche USART

UART: altamente configurabili.

Universal Synchronous Asynchronous Receive/Transmit. È possibile implementare a livelo software questi algoritmi semplicemente. 

1. bisogna sapere la codifica
2. il firmware deve prendere il byte e ci mette un bit di start, poi ci metto dentro

## Modem

Prendono l'informazione e la usano per modulare il segnale in un certo modo. La
modulazione viene scelta per essere efficiente nella trasmisione dei singoli
bit. Stanno diventando obsoleti

## Sistemi a BUS condiviso

Cosa significa avere condivisione del mezzo? Un esempio viene mostrato. Ogni
nodo è connesso ad un unica linea di comunicazione. Il potenziale elettrico è
visibile a tutte le unità che sono connesse. Questo ha vantaggi dal punto di
vista dei cablaggi. Nella rete punto a punto bisogna avere uno o più
collegamenti. Qui invece si ha solamente una connessione. Questi sistemi
caratterizzano gli standard di reti più comuni (e.g. Ethernet, WiFi).

- Mezzo condiviso: tanti nodi (aka host) lo condividono
- Quando più utenti parlano questa cosa risulta in collisioni che fanno
  malinterpretare il dato che si intende trasmettere.

Sono necessari protocolli per gestire in maniera adeguata come si condivide il
mezzo. Queste regole devono essere note e utilizzate da ogni attore del
sistema.

Nel momento in cui si parla io occupo il mezzo della comunicazione. Se qualcun
altro parla diventa difficile discernere cosa dice ognuno. Le regole
d'educazione dicono che se qualcuno parla bisogna aspettare il proprio turno
per occupare il mezzo. Queste sono il MAC che noi utilizziamo, il protocollo
che viene utilizzato dagli umani per comunicare.

### Media Access Control Protocols

Come si implementano queste regole?

è possibile suddividere il canale. lo si suddivide in canali più piccoli per far passare l'informazione di tanti. Questo canale può essere suddiviso temporalmente. Ognuno ha un quanto di tempo per parlare. Se però volessimo parlare per tanto tempo e l'interlocutore non dicesse nulla perderemmo solo tempo.

è possibile avere una suddivisione dinamica, fatta di turni che possono essere
alternati tra le persone. Quando non si ha nulla da dire si passa il turno
immediatamente al successivo. In questo modo non vado a occupare banda. Questi
sono chiamati "token ring". Chi ha il token può parlare, altrimenti no. Ce lo
si passa all'interno del ring. Lo devo passare con delle regole: occupo per un
certo quanto di tempo e poi passo il testimone al prossimo che può parlare
oppure no. Il token dal punto di vista digitale è un messaggio, nulla di
fisico. Questi sistemi sono molto efficienti e vengono usati per alcune applicazioni industriali. Una di queste criticità è però quella che deve gestire delle anomalie: cosa faccio se perdo il token? Se succede questo bisogna ripristinare il token e se esiste solo uno che può farlo, se muore non può ristabilire la funzionalità della rete.

Posso fare in modo che la collisione avvenga e gestisco il problema a posteriori. Risolvo il problema quando si verifica. Quando magari si sta parlando in due ci si interrompe e uno dei due ricomincia.

- Se due vogliono usare lo stesso mezzo, questo genera una collisione.
- Se non manda nulla nessuno il canale è in idle.
- Bisogna fare in modo che ognuno utilizzi il canale uno per volta.

Come posso fare quindi per gestire le collisioni?

### Evoluzione della gestione dei protocolli

### Aloha

Un precursore di Ethernet. È necessario monitorare lo stato di boe oceaniche,
sistemi che sono molto lontani tra di loro. Si utilizza un canale radio. Lo
spettro elettromagnetico però è un mezzo preziosissimo. L'utilizzo di 500 boe,
ciascuna con una frequenza dedicata, costerebbe molto in termini economici a
chi le gestisce. Si è pensato a come utilizzare un solo canale per trasmettere
queste informazioni. Se una boa deve trasmettere, questa invia l'informazione.
Il ricevitore fornisce un segnale di acknowledgement ogni volta che riceve il
dato. Se la boa non riceve l'acknowledgement, questa può inferire che la
trasmissione non è avvenuta, quindi ritrasmette. La ritrasmissione però implica
un rischio di collisione successiva. Per risolvere ciò si "lancia un dado": si
aspetta un numero di secondi randomico. Finchè il dado non ottiene numeri
diversi vi sarà una collisione. Quando faccio una ritrasmissione, se ho una
collisione il canale è stato utilizzato inutilmente per un certo quanto di
tempo. Se il canale è usato poco quest'approccio del dado va bene ma se aumento
l'utilizzo non va bene. Servono degli strumenti per effettuare l'analisi delle
performance di trasmissione.

Si identifica come *collisione completa* la sovrapposizione completa tra due
frame. Si identifica come *collisione parziale* una collisione che non avviene
completamente sul frame ma sull'intersezione dei pacchetti.

Quando aumento la quantità di dati da trasmettere avrei che sia una collisione
completa che quella parziale butta via risorsa canale. la collisione completa è
la situazione migliore perchè perderei meno tempo per occuparlo. Quando mando
pochi dati ci avviciniamo asintoticamente alla curva ideale ma se aumentiamo
avremmo un'efficienza massima del 18%.

#### Osservazioni 

è un approccio molto semplice e facile da implementare. L'aspetto negativo è
che queste regole non minimalizzano le collisioni. Stiamo quindi usando il
canale in modo inefficiente. Butto via la capacità di trasmettere del mezzo di
comunicaizone. Facendo alcuni calcoli è possibile ottenere la capacità
comunicativa del canale. Ho una strada che fa passare dei dati: se uso sempre
quel protocollo per ...  Se uso il canale con pochi dati ho un'efficienza
maggiore (rispetto ai dati che vorrei far passare, ossia la retta nera). Quando
aumento il traffico auomentano anche la collisioni. Queste collisioni causano
un aumento del tempo di utilizzo del canale. Riesco ad avere un massimo al 18%
della capacità di comunicazione. Quando supero una certa soglia le
ritrasmissioni sono maggiori dei dati correttamente trasmessi. Riascolta perchè
è un casino.

R è la capacità di trasmissione che viene fatta sulla stima dei dati inviati.


## Slotted ALOHA

Come facciamo a migliorare la trasmissione del dato?

Andando a vedere il diagramma precedente: nel caso dell'aloha puro vi sono 3 condizioni:

1. assenza di collisioni
2. collisione completa
3. collisione parziale: solo una parte dei pacchetti è sovrapposta.

Le tipologie due collisioni hanno un impatto diverso: quando è parziale butto via tutta la capacità trasmissiva per tutto il tempo di trasmissione dei pacchetti. Quando invece ho una collisione completa perdo solo il tempo uguale alla trasmissione di un pacchetto. Se avessimo una base temporale comune saremmo a cavallo. Divido il tempo in quanti e faccio in modo che si trasmetta in questi. Questo però richiede una sincrinizzazione delle unità e non è il massimo

## Protocollo Ethernet

Lo standard è evoluto nel tempo andando a trasmettere sempre più velocemente. è un tipo CSMA/CD. è stato implementato quello che facciamo quando parliamo con qualcuno. In primo luogo se uno parla qualcun altro non parla. Questo viene implementato dal carrier sense. Questo non è ovvio: esistono situazioni in cui non si può capire se è libero o no il canale. Se sto utilizzando una connessione via cavo è semplice. Se uso il WiFi questo ha una copertura limitata nella portata. La persona al centro può parlare con chi sta all'estrema sinistra e all'estrema destra ma questi ultimi due fanno fatica a farlo. Questo perchè non è itilizzato dal wifi.

Multiple Access: ???

Collision detection: Supponiamo che ascolto e supponiamo che nessuno stia parlando. Se siamo in due a parlare si crea un attimo di confusione che viene tradotta in arresto dei due e uno dei due inizia a parlare, poi il carrier sense fa in modo che l'altro non parli. Questo è un evento ancora più raro per via del carrier sense. Riconosco la collisione e faccio in modo che tutti capiscano che è avvenuta la collisione. L'approccio che dice "se ho una collisione mi fermo" è proprio questo.

1. Vedo se qualcuno manda informazione
2. Aspetto fino alla fine.
3. Mando il pacchetto, continuando ad ascoltare eventuali collisioni.
4. Se non avviene collisione il pacchetto è stato inviato. Non è necessario
   ricevere un acknowledgement.
5. Se avviene una collisione blocco la comunicazione ed aspetto un tempo
   casuale.Volendo limare l'approccio per ottimizzare l'uso del canale non
   faccio proprio un tempo casuale. Nel sacchetto metto un numero di tempi
   finiti (6, 100, etc). Se devo trasmettere qualcosa e pesco 99 e l'altro
   pesca 1 io aspetto un'infinità e spreco canale. Se invece ho un numero di
   tempi finiti in cui aspettare più piccolo ottimizzo meglio il tempo di
   utilizzo del canale. Quando il carico di rete aumente la probabilità di fare
   lo stesso numero aumenta, quindi aumenta il numero di facce del dado che sto
   utilizzando. Il numero di tempi finiti che utilizzo aumenta con il carico
   della rete (exponential back off). Parto sempre col dado piccolo e vado
   avanti aumentando sempre più facce, aumentandole esponenzialmente. Questo
   minimizza il numero di ritrasmissione e il ritardo temporale che posso
   ottenere in caso di trasmissioni con basso carico della rete.
   
La bellezza di quest'approccio è che non c'è bisogno di avere una base di tempi comune e funziona qualunque sia la variabilità del sistema. La rete continua a funzionare sempre in maniera autonoma. 

### Osservazioni

Fare un dispositivo che lo implementi è facile. La cosa difficile è implementare il real time. Questa rete permette di trasmettere in maniera ottimizzata quando conosco il carico della rete. Nel caso token ring posso calcolare il massimo ritardo in maniera. Con Ethernet non so in modo efficace quanto dovrò aspettare.

Un opportuno segnale elettrico sulla linea determina la collisione. Tutte le unità capiscono quindi che è avvenuta una collisione e bloccano tutto. Nel caso di segnali elettrici gli unici che possono rendersi conto sono le unità che stanno scrivendo. Queste unità che scrivono quindi generano il segnale.

### Grafici

Tempo di propagazione vs distanza fisica delle unità. L'asse x identifica la
distanza fisica tra le unità nella rete. A, B, C, D sono 4 unità equidistanti
nella rete e comunicano in ethernet.  L'asse y è il tempo. Il diagramma
rappresenta la trasmisioine dei pacchetti fatti da B e D nel tempo. B in giallo
e D in rosso. Quanto è lungo il pacchetto che voglio inviare? la dimensione
fisica del pacchetto è rappresentata dalla linea verticale al tempo $t_0$. Se
trasmetto a t_0 la trasmissione è finita e il tempo anche. Lo spazio percorso
nelle unità di tempo è identificato dalla pendenza (velocità di trasmissione).
B comincia a parlare che il canale è libero, così come D perchè non si era
ancora accorto del fatto che il canale è occupato. C non può accorgersi della
collisione però e prende buoni quei dati. Il primo ad accorgersi è D e propaga
il jam signal. In questo modo tutti sanno che è avvenuta la collisione e inizia
l'exponential back off.

Se cominciassi a diminuire il numero di pacchetti che invio otterrei dei tempi
di collisione ???. C'è un vincolo al funzionamento del sistema: la dimensione
del pacchetto. è necessario che vi sia un pacchetto con una dimensione tale
???. Questo dipende dalla velocità di trasmissione. Alcune info vengono messe
nell'header del pacchetto. Se devo mandare meno roba alla fine uso lo stesso
pacchetto ma comunque vuoto dove non serve.

64 byte:

- MAC address, si scrivono 6 byte. Mittente
- Destinatario,
- 2 byte per dirmi che tipo di pacchetto è.
- 4 byte di CRC: sistema per essere sicuro che il dato è corretto. Una sorta di parità. C'è parità, checksum e CRC che cerca di minimizzare il numero di errori tipici.
- 46 bytes di dati. Se abbiamo meno di 46 dati allora il sistema aggiunge dati per averne un totale di 46.

### Pacchetto Ethernet

- Preambolo: equivalente a quello che è il bit di start. Quest'ultimo serviva alla seriale per identificare l'inizio della trasmissione del pacchetto. La seriale usa il bit di start e stop perchè essendo asincrona ho un errore integrale che aumenta nel tempo. Più errore ho e peggio è nella trasmissione. Perchè riusciamo ad essere migliori in questo sistema di trasmissione? da RS232 è cambiato qualcosa in termini tecnologici ma qui ethernet dice, al posto del bit di start, 7 bytes con un pattern 101010 seguito da un pattern 101011. Dal punto di vista elettrico ho un'onda quadra con periodo pari a due bit (ogni bit rappresenta un semiperiodo). Quest'onda quadra viene usata per sincronizzare il punto di partenza ma anche la velocità dell'orologio. Posso sincronizzare la durata dei singoli bit con l'orologio di chi riceve. L'errore è stato ridotto moltissimo. Se devo trasmettere tanti dati questa cosa serve.

#### Codifica Manchester

invece di codificare 1 come mantenimento di tensione la codifico come tensione da alto a basso e 0 come da basso a alto. Questa codifica ha il vantaggio ???

### Collegamento fisico

Inizialmente ogni calcolatore aveva un collegamento a T con cavo coassiale. Il cavo creava una rete. Se si stacca da una parte non funziona più nulla. Se dal punto di vista elettrico questo è condiviso dal punto di vista del cablaggio si è voluto utilizzare una soluzione più efficace. Ogni aula ha delle prese di rete. Ogni presa va ad un HUB, quegli armadi con tanti fili quante le prese di rete nel nostro sistema. Sembrerebbe un sistema punto a punto ma in realtà queste prese sono delle multiple. Grazie alla disponibilità delle tecnologie elettroniche dei pacchetti se aggiungo agli HUB l'intelligenza posso utilizzare meglio i sistemi di comunicazione: leggo cosa sta arrivando e se quello che ho ricevuto è destinata a una macchina collegata a me la mando, altrimenti no (switch).


## Gestione rete

Network interface controller: circuiti integrati nell'equivalente di un microcontrollore dedicato e questi vengono visti come periferiche rispetto al processore principale. Quando uso protocolli di questo tipo devo avere un adeguata memoria di sistema per gestire i pacchetti da inviare.

## Protocolli a token


## Comunicazione wireless

Ci si è spostati verso questo sistema per motivi di difficoltà della
realizzazione delle infrastrutture. L'ultimo km che separa la centralina dalla
singola presa telefonica è quella più complicata. Quest'infrastruttura è più
semplice da gestire. Funzionano in circa decina di metri. Questi sistemi hanno
bisogno di un access point, un'unità che garantisce la possibilità di accedere.
L'access point che sta in mezzo. Vi è una collision avoidance invece di una
collision detection.

## TCP/IP

Quando abbiamo studiato iso-osi avevamo citato il livello network. Come è possibile far comunicare due diverse sottoreti? possiamo far saltare l'informazione da una rete all'altra tramite un router. Qualunque sia la dimensione della sottorete è possibile gestire solo alcune centinaia di host. Cosa succede quindi? quando vogliamo collegare reti bisogna usare dei protocolli particolari sopra i livelli 1 e 2 che consentono di migrare da una rete all'altra. Uno dei problemi da risolvere è quello relativo al mac address, l'identificativo unico di una scheda ethernet. è costituito da sei bytes che devono essere unici. Non tanto perchè possono verificarsi conflitti ma perchè nel caso in cui una persona diversa da me ma con il mio stesso indirizzo il sistema comunicativo non funzionerebbe più. Quando parliamo di una dimensione di una rete più elevata l'unicità diventa fondamentale dato che queste macchine sono tra loro interconnesse. è necessario avere indirizzi univoci.

Il protocollo dominante è quello TCP/IP. 

### Storia di TCP/IP

La rete internet costituita dal set di questi due protocolli: tcp e ip. IP è il livello network mentre tcp è il livello transport. Questo sistema è nato con arpanet, utilizzato a livello militare. Questa era una rete di comunicazione che sia sufficientemente dinamica da variare l'instradamento in caso di guasti.

Bisognava però trovare una soluzione per utilizzare dei sistemi diversi tra di loro. Questo viene fatto dai livelli più elevati.

La suddivisione iniziale era quella di creare 4 diverse
linee: applicazioni (programmi che utilizzano la rete per
fare qualcosa): protocollo ftp, telnet (fa funzionare
terminali a distanza), email. Qualunque applicazione che
usa queste reti lavora a questo livello. Queste si basano
sulle info dei livelli inferiori. Sotto vi è il layer di
trasporto, molte volte realizzato col protocollo TCP
(oppure UDP). Questi costruiscono sopra qualcosa che sia
più affidabile (sappiamo se la comunicazione è arrivata o
no), orientato alla connessione (i dati arrivano nello
stesso ordine o no). Questo layer di trasporto costruisce
queste cose su un layer più semplice che garantisce il ...
. Questo è fatto dall'interet protocol. Questo IP si
appoggia su un layer costituito da device drivers.

Andando a confrontare TCP/IP con il modello ISO-OSI abbiamo che il livello fisico e data link sovrapposto con host to network. Il livello network è internet e il transport è transport. Perchè non definire host-to-network? PErchè vogliamo fare in modo che possa funzionare con qualsiasi implementazione degli strati inferiori. Questa parte non è definita perchè in questo modo è possibile implementare divrsi physical link e data link senza impattare sul funzionamento di internet.

La mancanza della definizione di host-to-network permette proprio l'esistenza di internet.

Passando da un layer alto ad uno basso aumenta
l'informazione da mandare. Il layer a livello transport
aggiunge header. Ciascun livello ci attaca un header
particolare. Il livello ethernet ci allega sia un header
che un tail o trailer. Se avessimo usato wifi o fibra
ottica avremmo avuto un diverso frame di dati.

Per come funzioina questo sistema di comunicazione il riferimento per descrivere internet è quello di una clessidra. Partendo dall'application noi abbiamo tantissime tipologie di applicazioni, queste applicazioni usano un loro protocollo di comunicazione (i.e. http, https). Una volta stabiliti i protocolli da utilizzare con l'applicazione si usa il transporte tutti questi si appoggiano sul protocollo IP. Sotto IP posso avere tanti altri protocolli per la gestione dei data link: ethernet, PPP, etc.

### Come fa IP a gestire questa funzione?

Se volessimo inviare un byte ad una macchina qualunque in
un punto sperduto nel mondo questa è gestita da IP. Si
pensa che sia un protollo difficile ma in realtà abbiamo un
overhead che aiuta l'instradamento. Come funziona il
protocollo IP?

1. Dobbiamo sapere chi è il destinatario e chi è il
   mittente. L'indirizzo IP è in 32 bit e mette insieme 4
   bytes. Si voleva un numero grande e ai tempi si pensava
   a 32 bit. Può essere definito in formato digitale con il
   punto. Il mac address è definito in formato esadecimale
   con due punti in separazione.  Tutti i calcolatori del
   mondo sono identificati da un indirizzo IP unico. 
2. Si definisce la subnet. L'indirizzo viene scritto in
   modo gerarchico: il civico, la via, la città, la
   nazione. è possibile ritrovare una struttura di questo
   tipo anche in IP. Se voglio andare a leggere questo
   numero noi sappiamo che ... Il sistema politecnico
   assegna un IP i cui primi numeri sono sempre uguali. I
   byte più significativi indicano la macro area della rete
   in cui mi trovo. Il dettaglio è dato dal byte meno
   significativo. Quindi abbiamo un numero significativo di
   bit ma vincolato. Questo limita molto e non voglio
   mantenere ciò in una rete. è possibile avere una rete a
   cui collego solo qualche roba (e.g. 10 macchine) oppure
   un sacco di macchine (e.g. politecnico). Non c'è una
   struttura gerarchica predeterminata di questi bit ma
   siamo noi che definiamo quanti sono gli host e quanti
   sono i bit da dare alla sottorete.

Utilizzando la subnet mask è utilizzata per implementare il
protocollo ip. ci sono una serie di elementi che parlano
tra loro (sottorete) che può arlare col mondo esterno
tramite un router. è come avere un condominio con tante
palazzine e vi è un servizio postale costituito dalla posta
interna e quella esterna. Se voglio mandare una lettera ad
aliverti scrivo una lettera nella casella posta interna e
il postino fa il recapito. Se voglio mandarla fuori dal
politecnico io metto la lettera in una casella speciale per
mandarla fuori. Questo stesso approccio viene usato dal
protocollo IP. Questo indirizzo ip è fatto da 4 byte e io
voglio sapere se il destinatario fa parte della mia rete
oppure no. A seconda di quest'informazione posso gestire
l'invio all'esterno (ossia al gateway) oppure no. Come
capisco? tramite l'informazione aggiuntiav ossia la subnet
mask. Esempio: 4 numeri: 255.255.255.0: questa corrisponde
a 0xFF per 3 volte. ho 3 byte a 1 e 1 byte a 0: la
sottorete a cui sono connesso utilizza gli ultimi 8 bit per
identificar l'host e gli altri per la sottorete. Perchè si
chiama maschera? perchè fa in modo che IP capisca cosa fare
se è destinato ad una macchina esterna o interna. Faccio
l'AND tra indirizzo e subnet mask. L'AND fa passare tutti i
bytes più significativi. Tengo il risultato in memoria. Il
pacchetto che devo inviare all'esterno viene messo in AND.
Il risultato viene confrontato col risultato precedente: se
le due subnet sono uguali sono a posto: mando nella posta
interna, se invece i due numeri sono diversi allora devo
indicizzare ad una sottorete diversa: devo mandarlo al
gateway ossia la macchina che ha accesso al mondo esterno.

- Indirizzo *IPv4*: indirizzo (in teoria) unico del nostro
  computer.
- *Subnet mask*: 255.255.240.0. La sottorete è fatta da più
  di 255 macchine.
- *Gateway*: indirizzo del "postino"
- *Indirizzo fisico*: è il MAC address. Questi sono 6 bytes
  in esadecimale.
- *DHCP*: protocollo utilizzato per rendere la vita più
  facile. Prima si doveva chiedere l'indirizzo IP, la
  netmask e il gateway. Come facciamo ogni volta a
  ricordarmi tutte le robe ogni volta che ci spostiamo. è
  un sistema di automatizzazione degli indirizzi IP. Si
  vede nella rete un server DHCP che fa la funzione del
  network manager e fornisce un indirizzo.
- *Lease*. Ogni volta che il sistema fornisce dei numeri ci
  da anche una scadenza, come le mozzarelle. Questa è il
  lease. Quando è raggiunta la scadenza la macchina chiede
  un nuovo indirizzo. Scadenza a breve termine perchè le
  aule sono frequenti ad un cambio dinamico di persone che
  sono all'interno.
- *DNS (domain name service)*: Un altra coppia di numeri che
  sono ndirizzi ip. Questo non è essenziale ma semplifica
  l'utilizzo. questo non è essenziale e affronta il
  problema di attribuire dei nomi simbolici ad un indirizzo
  ip. Se volessi andare sul sito del politecnico
  utilizzerei l'URL, non l'ip del politecnico. Si
  utilizzano delle ip tables che associano ai link agli ip.
  Perchè due DNS? se perdo l'elenco telefonico è una
  scocciatura. L'elenco telefonico non ha nulla a che fare
  con l'affidabilità della comunicazione ma se non ce l'ho
  faccio fatica ad usare la rete. Questo è il motivo per
  cui se ne danno solitamente due. Nel caso una delle due
  fosse guasta utilizzo l'altra.
- *IPv6*: il v4 non basta per la gestione degli IP. Bisogna
  cambiare tutto il protocollo IP. Se cambio una cosa su IP
  tutte le macchine del mondo dovrebbe cambiare altrimenti
  rimarrebbe fuori dalla rete. Quando si è visto che v4 era
  piccolo si è deciso di aggiungere qualche byte in più
  sfruttando il fatto che aggiungendo dei bit raddoppio di
  volta in volta il numero di calcolatori connessi. Da una
  certa data è stato realizzato un ibrido ipv4 e v6 così da
  poter gestire meglio la dimensione sempre crescente delle
  reti.
  
  
Come facciamo oggi ad avere così tanti calcolatori connessi
ad una singola rete domestica? In realtà si usa un sistema
ciamato NAT che traduce gli indirizzi IP che permette di
utilizzare ... Il poli ha una posta interna. Se mando una
lettera al poli non serve il dettaglio. Basta che scrivo
piazza leonardo e poi è il postino interno che sa dove
mandarlo. Il protocollo nat fa qusesta roba qua. Ci sono
tantissime sottoreti interne che possono anche a vere lo
stesso indirizzo. Il gateway toglie l'indirizzo locale e
mette l'indirizzo interno (piazza leonardo davinci). Da
fuori vedrò solo un unico indirizzo esterno ????
riascoltare. Il punto positivo è che risparmio indirizzi IP
ma posso mandare solo cose, non posso ricevere. Non posso
essere direttamente contattato dall'esterno.

La sicurezza. Le informazioni viaggiano da macchina a
macchina e potrebbero esserci malintenzionati che vogliono
sniffar pacchetti e possono essere utilizzate da loro.
Esistono degli strumenti per proteggere calcolatori dagli
attacchi hacker che sfruttano vulnerabilità dei
calcolatori. La connessione è bella in un mondo di "puri".
Bisogna stare attenti.

Il discorso MAC address e reti a condivisioni del mezzo:
tutto quello che invio è accessibile agli altri. Posso
programmare la scheda di rete in modo da avere un mac
address uguale al suo. Si usa sniffing. Se le informaioni
si lasciano in chiaro quelle informazioni possono essere
ricevute e interpretate facilmente.
