# Lezione 5 --- Sistemi di monitoraggio

Alcuni problemi: il monitoraggio del paziente in condizioni critiche.
Introduzione dei monitor utilizzati in terapia intensiva.

Una serie di dispositivi partendo dalle misure che questo dispositivo deve
realizzare. La compoente elettronica è fondamentale ma dev'essere attentamente
progettata tenendo a mente anche dei requisiti importanti per poter garantire
la sicurezza del paziente e macchina.

Quanto è importante mettere insieme mettere insieme un po' di fisiologia,
chimica, elettronica e informatica. Questo è un valore aggiunto perchè non
siamo specializzati in una cosa ma sappiamo parlare diversi linguaggi e se si
ha dimistichezza con questi aspetti è più semplice realizzare questi strumenti.
La realizzazione di questi strumenti è assolutamente interdisciplinare. Faremo
come esempio la storia di una problematica della realizzazione di
strumentazione:

- Monitor di ventilazione: misurare la pressione arteriosa ha una certa
  complessità e per fare una misura in diversi contesti.

Non si parla di hardware ma perchè vogliamo utilizzarli, concentrandoci sul
contesto della misurazione. Il monitoraggio del paziente in condizizoni
critiche: osservazioni (cosa s'intende lo vedremo dopo) ripetute o continue
(definizione scritta da medici. Una misura non sarà continua ma avrà una
frequenza di campionamento più alta) o misure (fa parte del DNA dell'ingegnere:
ad un numero associo un valore) del paziente, delle funzioni fisiologiche e
della funzione di apparati a supporti alla vita (non solo del paziente stesso
ma anche della strumentazione stessa perchè anch'essa è importante) col fine di
prendere decisioni sul trattamento del paziente includendo quando intraprendere
degli interventi terapeutici e valutando l'efficacia di questi interventi. 

Questa frase è scritta da medici per medici e definisce come i medici vogliono
operare. Ci sono sempre aspetti tecnici.

Lo scopo è monitorare dei pazienti clinici. Posso identificare diversi gruppi:

1. Pazienti che hanno sistemi di regolazione fisiologici instabili: sistemi che
   determinano l'omeostasi del paziente che non funziona in maniera corretta.
   L'anello chiuso che determina la sopravvivenza non funziona correttamente
   per diversi motivi:
	1. Alterazione patologica
	2. Situazioni gestite per via di un trattamento Esempio il sistema
	   respiratorio a causa di un carico di farmaci elevato di un certo
	   tipo oppure un'alterazione volontaria (e.g. paziente sedato).

Il clinico deve regolare i parametri di funzionamento dello strumento per poter
regolare l'omeostasi del paziente.  Questi sono pazienti che hanno bisogno del
monitoraggio.

2. Pazienti che sembrano avere una regolazione funzionante ma stanno vivendo in
   una condizione che può farlo passare da questa situazione alla situazione 1.
   Rischiano perchè hanno ndicaioni chiare che possono risultare
   nell'alterazione del funzionamento del sistema di regolazione.
3. Pazienti che non hanno una diagnosi chiara di una patologia ma che hanno una
   probabilità di sviluppare una condizione che poi porta al rischio.
   Monitoraggio leggero
4. Pazienti in uno stato fisiologico critico. Condizione di mezzo tra la 2 e la
   1.

I primi 3 pazienti hanno delle condizioni diverse. Se il paziente è nello stato
1 è più propenso ad avere interventi invasivi. Le condizioni del paziente
definiscono le specifiche dei dispositivi che si realizzano per andare a
trattare i pazienti.

## Sistemi di monitoraggio

Parlando di un paziente in ospedale. vi è un'infrastruttura che all'interno di
reparti in paesi sviluppati è presente un sistema monitor a posto letto vicino
al paziente e che offre a chi lo gestisce di poter vedere i parametri vitali.
Il monitoraggio dev'essere continuo, quindi sono connessi tra loro una rete che
serve a riportare ad una centrale di monitoraggio (dove sta il caposala) per
capire come stanno i pazienti. Questo sistema è collgato a trasmettitore
telemetrici che inviano parametri "via radio" alla centrale di monitoraggio.

### Parametri d'interesse

Devono caratterizzare i principali sistemi:

- ECG e HR e pressioni sanguigne per il sistema cardiovascolare. Esistono
  approcci invasivi (e.g. uso di catetere) e non invasivi.
- Respiratorio: frequenza respiratoria (impedenziometrica o ventilatore),
  ventilazione e parametri vventilatori, saturazione arteriosa (SpO2 ossia la
  saturazione di O2 e pulsossimetria).

Si veda un esempio di monitor di terapia intensiva. Questo è costituito da
moduli che effettuano misurazioni e moduli che mostrano a schermo. Non si hanno
solo numeri ma anche grafici. Alcune variabili consistono in una combinazione:
le diverse tracce dell'ECG sono mostrate insieme all'HR che sarebbe derivabile
facilmente dal grafico ma poco fruibile al momento. La ridondanza non è uno
spreco in questo caso perchè il medico guarderà le tracce per vedere
un'alterazione della mofologia mentre il numero quando vuole vedere
l'alterazione del ritmo cardiaco.

ibp: invasive blood pressure

Di fatto sono degli strumenti che includono nello stesso sistema diversi
strumenti di misura. Questo per rappresentare diverse variabili d'interesse
medico. Abbiamo quindi un insieme di parametri che caratterizzano l'attività
cardiaca, il sistema cardiovascolare (e.g. caratterizzazione della pressione
arteriosa), il sistema respiratorio (e.g. respiratory rate, SpO2 del sangue
arterioso), lo stato metabolico (e.g. temperatura, emissione CO2). Si parla di
una macchina che fa molte misure. Quando abbiamo capito cosa deve fare questa
macchina è necessario capire come progettarla.

Per capire quali sono i blocchi necessari bisogna fare riferimento alla
struttura generalizzata di un sistema di misura biomedico. Questo schema, pur
essendo generalizzato, non rappresenta esattamente quello che vogliamo
realizzare nella progettazione del monitor ma si riferisce solamente alla
catena di misurazione di un singolo parametro. Tante misure comportano quindi
una combinazione di altrettanti schemi generalizzati. Il fatto che il display
rappresenti un sistema per veicolare all'utente le informazioni relative a
tante misure significa che ogni variabile fisica avrà una parte iniziale nel
dispositivo e tutte dovranno poi convergere per convogliare tutte le
informazioni ad un unico display.

## Architetture dei sistemi di monitoraggio

![Approccio con unico elaboratore e molteplici catene di front-end.](Figure/sistema_monitoraggio_architettura_1.png)

![Approccio con moduli di misura "intelligenti".](Figure/sistema_monitoraggio_architettura_2.png)

Esistono diversi approcci (i.e. architetture) alla realizzazione dei monitor:

1. *Unico elaboratore, tante catene di front-end analogico*. Fu il primo
   approccio standard. La prima parte della catena di acquisizione viene
   riprodotta in tante parti quanti sono le variabili fisiche da misurare e
   manteniamo il resto (seconda parte della catena). La parte da ripetere
   comprende i blocchi funzionali dal trasduttore al filtraggio compreso
   (front-end). Ciò significa che il segnale in uscita a questa catena è ancora
   analogico e necessita dunque dell'elaborazione. Creiamo quindi un
   dispositivo nel quale abbiamo moltiplicato le parti relative alle prime fasi
   di trasduzione fino al front-end. Il blocco di elaborazione dovrà contenere,
   per poter gestire la mole di catene di front-end, un multiplexer di segnali
   analogici. In questo modo il sistema d'elaborazione può gestire ciascun
   canale analogico. L'elaboratore effettuerà poi il campionamento della
   grandezza e ne effettua l'elaborazione necessaria per poter produrre su
   display i risultati relativi alle elaborazioni specifiche per ogni singola
   variabile fisiologica registrata. Questo era l'approccio che consentiva di
   ottimizzare l'utilizzo della risorsa d'elaborazione (i.e. unico processore
   utilizzato in condivisione).  Il vantaggio era la riduzione dei costi. Lo
   svantaggio è che si necessita un blocco di elaborazione che deve gestire
   diverse specifiche elaborazioni. Il processore doveva essere
   sufficientemente potente da poter gestire i diversi software (la cui
   complessità dipende dai parametri d'interesse e dagli algoritmi di
   elaborazione). L'altro svantaggio è legato alla scarsa flessibilità nel caso
   di una singola unità d'elaborazione condivisa.
2. *Sistema a moduli "intelligenti".* Un'importante feature è la modularità di
   un sistema. Il motivo per cui si sceglie di implementare questa feature è
   che ciascun paziente possiede diverse necessità e si ripercuote sulla
   misurazione un numero variabile di parametri fisiologici, quindi anche sulla
   flessibilità di un sistema di monitoraggio per poter gestire tali misure. Si
   ripete quindi, in questo caso, anche il blocco di elaborazione del segnale
   con il relativo blocco di **controllo e retroazione** (i.e. si utilizza una
   porzione della catena d'acquisizione più ampia rispetto a quella
   dell'architettura precedente) per ogni variabile da misurare. Ciò
   corrisponde alla creazione di veri e propri moduli di misura in grado di
   effettuare localmente elaborazioni specifiche su segnali cui fanno
   riferimento. Un modulo di misura non è solo costituita dal front-end
   analogico ma diventa uno strumento di misura vero e proprio, privato della
   possibilità di visualizzare verso l'esterno la variabile. L'uscita a ciascun
   modulo sarà dunque il segnale specifico già elaborato localmente.
   Realizzando dei veri e propri sistemi di misura completi ed autonomi che
   comunicano mediante interfacce di comunicazione dati (RS232, ethernet, etc)
   con il sistema d'elaborazione centrale, è possibile implementare i blocchi
   di controllo in retroazione la cui realizzazione sarebbe difficile in
   architetture simili alla precedente.  L'elaborazione è strutturata a livello
   gerarchico: ciascun microcontrollore locale gestisce la misura delle singola
   variabili fisiche (e.g. ECG, SpO2) e le informazioni vengono convogliate a
   livello del microcontrollore che gestisce il sistema di monitoraggio. Questo
   sistema è molto più flessibile perchè è possibile avere una rete di
   comunicazione a bus condiviso e inserire tanti moduli quanti quelli relativi
   alle variabili d'interesse. Ogni processore del modulo gestisce in modo
   ottimizzato e parallelo la serie di processi di elaborazione della singola
   variabile da esso gestita.  L'elaborazione locale delle variabili è seguita
   poi dall'invio dei risultati all'elaboratore centrale del sistema di
   monitoraggio. Ogni modulo è dotato della sua autonomia per ottimizzare le
   performance del sistema, migliorare il controllo dei sistemi stessi (e.g. Il
   sistema centrale può controllare gli eventi anomali relativi ai singoli
   moduli e alcuni moduli possono controllarsi vicendevolmente sfruttando
   opportuni watchdog timers o altre strategie). Il microcontrollore del
   monitor deve riconoscere i moduli collegati, generare la visualizzazione
   grafica dei risultati e gestire la trasmissione dei dati da e verso sistemi
   esterni.

## Identificazione dei requisiti (standard)

Il percorso è cambiato molto col tempo. All'inizio si partiva dall'analisi
delle caratteristiche del paziente che venivano eseguite e riportate dalle
società mediche. Esse riguardavano la necessità relativa alla gestione del
paziente. Da queste si estraevano le informazioni necessarie alla definizione
delle specifiche. Questa parte del lavoro viene compiuta ancora oggi dal
progettista ma limitatamente ad alcune nuove applicazioni (non ancora
standardizzate), altrimenti si fa riferimento a delle normative (e.g. UNI
italiana affiliata ad ISO definisce tutti gli standard che caratterizzano i
requisiti minimi dei vari dispositivi più comune). Monitor cardiopolmonari,
ventilatori meccanici (aka VM) e dispositivi diagnostici che sono ben definiti
nella loro esistenza sul mercato sono ben descritti da norme che descrivono nel
dettaglio quali sono le specifiche di accuratezza, sicurezza (elettrica,
meccanica e biochimica).

### Esempio dell'anestesia, gli standard di riferimento

Si considerino, a titolo d'esempio, alcuni documenti per la definizione delle
specifiche tecniche relative al monitoraggio di un paziente sotto anestesia.

Vi sono una serie di standard di riferimento. Il documento ISO è scritto in
maniera molto comprensibile ad un ingegere. É un elenco piuttosto pedissequo di
feature, test e altro. É sempre importante però riferirsi a documenti clinici
per capire il contesto perchè le macchine che progettiamo sono destinate a
operare in un contesto in cui non esiste solo la nostra macchina ma una
pluralità di altre figure (pazienti, medico, infermieri, tecnici etc). Si
possono notare degli esempi di standard che sono state prodotte dalle società
scientifiche che riguardano il monitoraggio sull'anestesia e da queste è
possibile prendere degli spunti interessanti.

#### Standard I --- Informazioni sulla modalità dell'anestesia e parametri da misurare

Quando si fa anestesia dev'esserci sempre personale qualificato in grado di
gestire l'anestesia nella stanza. Per cui questa cosa significa che l'anestesia
non può essere gestita da chiunque altro. Ciò che è più di nostro interesse è
la parte in cui si cita cosa si vuole misurare e perchè. É necessario controllare
l'ossigenazione, la ventilazione, la circolazione e la temperatura.

#### Standard II --- L'ossigenazione

Il documento ci dice perchè è necessario controllare l'ossigenazione: dobbiamo
garantire che vi sia un'adeguata concentrazione d'ossigeno nei gas inspirati e
nel sangue durante la fase d'anestesia.

si ritorna alla prima definizione di monitoraggio nella quale era indicato che
non si vuole solo monitorare il paziente in situazione critica ma anche il
funzionamento di tutti quegli apparati dai quali la sopravvivenza del paziente
dipende. Eseguendo una ventilazione che dev'essere data al paziente dobbiamo
anche essere sicuri di sapere qual è la conentrazione di O2 nell'aria che è
prodotta della macchina e non dal paziente.

Ci fornisce i metodi per poterlo fare: bisogna usare un misuratore d'ossigeno
che allarmi nel caso sia troppo bassa la concentrazione di O2.  La macchina
d'anestesia è costituita da ventilatori meccanici che usano un circuito chiuso
perchè spesso devono somministrare l'anestetico attraverso gas.

1. L'espirato del paziente viene passato attraverso un contenitore con calce
   sodata, che rimuove la CO2, e il gas viene poi riciclato. Il paziente, però,
   ha sottratto a quel gas una certa quantità d'ossigeno che quindi dev'essere
   reimmessa dalla macchina. Il sistema a circuito chiuso rimuove la CO2 e
   reimmette O2. Bisogna monitorare che la quantità d'ossigeno sia corretta
   perchè, se per qualche motivo tecnico la valvola che gestisce l'immissione
   d'O2 non dovesse aprirsi, la concentrazione di O2 crollerebbe. Quest'evento
   dev'essere identificato dalla macchina prima di scoprire che il paziente non
   è più ossigenato. Se le variabili escono da certi limiti, deve verificarsi
   un allarme in quanto condizioni che mettono a repentaglio la sopravvivenza
   del paziente. 
2. Per quanto riguarda l'ossigenazione è necessario utilizzare un metodo
   quantitativo. Questa misura viene fornita da pulsossimetri ma, in caso di
   mancanza o fallimento di questi dispositivi, bisogna intervenire effettuando
   altre misure. Gli operatori devono quindi essere in grado di gestire il
   failure delle tecnologie. Il dispositivo deve comunicare il suo stato di
   failure, così che l'operatore possa cambiare dispositivo oppure usare
   alternative. Per questo motivo nei documenti viene riportato anche ciò che
   serve al clinico per l'esecuzione della procedura di anestesia.

#### Standard III --- La ventilazione

Bisogna garantire che la ventilazione sia adeguata. La ventilazione è la
quantità di gas che esce ed entra nei polmoni. Questa variabile deve essere
continuamente valutata. Dobbiamo valutare segni clinici qualitativi (se il
paziente si gonfia e sgonfia ritmicamente) se la macchina in circuito chiuso
che accumula gas durante inspirio ed espirio attraverso una bag (una sorta di
concertina che si alza ed abbassa). Osservando la bag che si gonfia e sgonfia
ed ascoltando se vi sono dei suoni, è possibile verificare se avviene la
ventilazione. Non è tutto qui ma bisogna anche monitorare la quantità di CO2
nel gas espirato. Questo perchè uno dei problemi che possono verificarsi è che
la ventilazione fornita al paziente non stia seguendo il percorso corretto
(e.g. Intubazione dell'esofago al posto della trachea). Questi dispositivi
d'interfacciamento possono essere inseriti in modo scorretto. Se il paziente è
intubato nell'esofago vediamo lo stesso la bag gonfiarsi e sgonfiarsi ma il
paziente non sta effettivamente ventilando. Uno dei metodi piùsicuri per
verificare se ciò è accaduto è di andare a misurare la CO2 espirata: se ho
molta CO2 in uscita il paziente sta ventilando correttamente, altrimenti è
necessario estubare. Un ventilatore meccanico necessita di avere dei sistemi in
grado di identificare eventuali disconnessioni immediatamente attraverso un
allarme acustico. Il ruolo dell'allarme acustico è fondamentale: gli operatori
stanno effettuando diverse manovre e non possono prestare continuamente
attenzione alle macchine. Un suono d'allarme però è ben udibile e mette in
allerta (per esprimere il concetto con termini utilizzati in questo corso basti
pensare all'allarm come se fosse un'interrupt).

#### Standard IV --- Circolazione

Metodi: elettrocardiografo. L'ECG dev'essere visualizzato continuativamente
dall'inizio dell'anestesia fino a quando il paziente non lascia la stanza.
Bisogna essere sicuri che vi sia una pressione arteriosa adeguata e che il
sangue si stia muovendo correttamente nel circolo principale. Il documento
inoltre riporta che la frequenza di misurazione della pressione arteriosa è di
almeno ogni 5 minuti. Questo significa che non è una misura continua come l'ECG
ma che questa variabile ha una dinamica più lenta. Gli strumenti per la misura
di pressione di tipo invasivo possono effettuare misurazioni di tipo invasivo
possono registrare continuamente la variabile ma quelli non invasivi invece
possono fare misure una volta ogni tanto, consentendoci di utilizzare tecniche
non invasive. La ridondanza dei sistemi di misurazione aumenta l'affidabilità
della misura (si sente il polso, pletismografia, stetoscopio per sentire il
battito).

#### Standard IV --- Circolazione

Temperatura corporea: bisogna controllare la temperatura e riportarla. La cosa
importante non è critica dal punto di vista tecnologico ma bisogna tener conto
che la misura della variabile temperatura del sensore non è sempre detto che
sia quella del corpo ma bisogna garantire che la misura venga effettuata in
modo da dare informazioni specifiche sulla temperatura corporea e non dello
scaldatore che c'è di fianco.

Bisogna comunicare poi le informazioni registrate con il sistema informativo
ospedaliero.

## Moduli utilizzati in un sistema di monitoraggio di ICU

Ripartiamo dai tipi di dato che vengono utilizzate nel monitoraggio del
paziente di terapia intensiva. Possono essere raggruppata per variabili di tipo
continuo, campionato, codificati (di tipo qualitativo) o testo utilizzato per
annotare eventi particolari che possono essere d'utilità clinica. 

Variabili sulla sinistra sono quelle che sono tipicamente oggetto di misura dei
monitor da terapia intensiva, eventualmente in combinazione con moduli o
dispositivi dedicati come ventilatori meccanici. 

Per quanto riguarda le variabili campionate vi sono quelle che derivano dalle
analisi da parte del laboratorio di analisi oppure temperatura.

Variabili codificate: variazioni di colore, postura del paziente, infusioni di
fluidi, farmaci, defibrillazione, anestesia etc.

### Misure di pressione

Il ciclo cardiaco è quello che produce moltissime informazioni utili.

È possibile notare le diverse pressioni in gioco e quella che è più d'interesse
è quella in aorta.

Una delle variabili d'interesse è la pressione, che cambia in modo continuo a
seconda del distretto dove la si vuole misurare. La pressione del circolo sarà
maggiore in aorta, all'uscita del cuore e sarà la più bassa possibile
all'ingresso del ventricolo. Questo è dovuto al fatto che il sangue ha
attraversato tutte le resistenze offerte da arterie, capillari, vene. La
caduta di pressione a cavallo delle resistenze è proprio quella che rappresenta
la differenza di pressione prodotta dalla pompa cardiaca. Il cuore è pulsatile
e quindi a seconda di dove la misuriamo ci si aspetta che vari anche dal punto
di vista dinamico. In uscita al cuore abbiamo strutture elastiche che vengono
sottoposte ad una variazione pulsatile di pressione. Questa struttura alla fine
delle arterie si suddivide in capillari. Questi forniscono una vera e propria
resistenza che si unisce a quella dei vasi a valle. La resistenza idraulica,
assieme alla compliance dei vasi crea un circuito RC che in questo caso si
comporta da passabasso e dunque la pulsatilità presente in fase iniziale si
smorza. Questa è una delle caratteristiche che si sfrutta per estrarre
l'informazione sulla porzione di emoglobina ossigenata nel sangue arterioso con
la pulsossimetria.

La variabile finale avrà una sua dinamica che potrà essere riportata con il suo
valore medio ma anche a livello delle fluttuazioni della variabile. Di solito
la variabile pressione arteriosa è espressa non tanto in termini di pressione
media ma in termini di pressione sistolica (massima) e diastolica (minima).

L'ipertensione arteriosa è una delle condizioni che richiede la quantità
maggiore in assoluto di farmaci.

#### Tipologie di misure di pressione

Misure di pressione fatti in contesti diversi:

1. Non invasivi
2. Invasivi:
	1. Con presenza di trasduttori posizionati al di fuori del vaso
	   (extravascolare).
	2. con presenza di trasduttori intravascolari.

##### Metodo non invasivo (o indiretto)

Molto semplice nella sua modalità. Si usa una cuffia per applicare una
pressione nota all'esterno di un arto. Sono dei palloncini a forma di bracciale
connessi ad un tubo. Si applicano ad arti in cui si può applicare pressione
direttamente sull'arteria (primo tratto delbraccio). Normalmente il tubetto è
dotato di pompetta in gomma con un rubinetto per aprire uno sfiato verso
l'esterno. Chiudendo la vite, la valvola si chiude ed è possibile gonfiare. Si
parte da palloncino sgonfio (pressione atmosferica). La pressione è riportata
su un misuratore di pressione (manometro). Questo dispositivo è detto
sfigmomanometro. Lo stetoscopio viene utilizzato per auscultare i rumori.
Chiudo la valvola e rigonfio la cuffia con la pompetta. Gonfiandosi, la cuffia
va a comprimere l'arteria (considerando come incomprimibili gli altri tessuti)
e, se supera la massima pressione che durante il ciclo cardiaco è applicata
all'arteria il vaso è collabito e abbiamo bloccato il sangue. Aprendo la vite,
la cuffia si sgonfia. Quando la pressione è più alta della massima nel vaso non
si sente nulla. Nel punto 2 la pressione del vaso diventa superiore rispetto a
quella della cuffia, quindi avrò una leggera riapertura del vaso.  Questo
periodo aumenta al ridursi della pressione applicata. Le aperture causano
vibrazioni meccaniche che causano suoni dette anche suoni di Korotkoff.  Sono
dei battiti intensi.  Una volta che siamo arrivati alla posizione dopo l'evento
4 e la pressione applicata è minore della minima all'interno del vaso non vi è
più questa chiusura e riapertura. Quando il battito si sente lontano ho
raggiunto la minima. Nel punto in cui questi battiti intensi vengono auscultati
in primo luogo si ha il massimo valore di pressione (sistolica), nel momento in
cui i battiti cominciano ad essere percepiti in lontananza si ha la minima
(diastolica).

---

Volendo implementare un dispositivo (semplificato) che utilizzi la stessa
logica.  Dobbiamo essere in grado di identificare i suoni di Korotkoff. Ci
vorrà un sensore di pressione inserito nella cuffia, al posto della pompetta si
utilizza un compressore.  Si potrebbe misurare il suono direttamente con un
microfono ma in realtà basterebbe valutare l'andamento dinamico del segnale
pressorio tramite un sensore di pressione. Il suono altro non è che vibrazione
pressoria. Analizzando il segnale ottenuto dalla misura della pressione
all'interno del manicotto concentrandosi in particolare sulle caratteristiche
dinamiche, otterremmo un diagramma simile al SEGUENTE.

Analizzando la curva di svuotamento è evidente la natura esponenziale. La
cuffia può essere paragonata ad un condensatore, così come la valvola ad una
resistenza al flusso. Fisicamente quindi sta avvenendo uno "svuotamento" di una
capacità su una resistenza (in maniera analoga ad un circuito RC: al posto
della corrente ho il flusso e al posto della tensione ho la pressione).

Questa procedura presenta però delle criticità. Una di queste è legata alla
velocità con la quale viene svuotata la cuffia, ossia la pendenza della curva
di pressione.  Questa dev'essere in grado di essere abbastanza lenta da
permettere una misurazione accurata ma non troppo da dare fastidio al paziente.
Quando la pressione minima è stata raggiunta e calcolata, si aumenta la
velocità con cui la cuffia viene svuotata per non far attendere paziente
inutilmente.

La derivata della pressione rispetto al tempo corrisponde ad un filtro passa
alto con il quale si evidenziano i contenuti in alta frequenza di nostro
interesse. Il contenuto in frequenza iniziale è elevatissimo (per via della
cuspide) poi si hanno delle variazioni minori (dovute al battito cardiaco). I
suoni di Korotkoff vengono registrati proprio in corrispondenza di quest'ultime
variazioni. Il delta di pressione, filtrato col passa alto, è ridotto nella
fase iniziale, aumenta in secondo luogo ed infine si riduce di nuovo.

Per quanto riguarda l'algoritmo col quale è possibile ricavare
quantitativamente i valori di pressione sistolica e diastolica, è possibile
utilizzare un approccio oscillometrico. Si definiscono i valori di pressione
sistolica e diastolica a partire dalla seguente analisi del segnale filtrato in
passa alto. La variabile da controllare consiste nella differenza tra il
massimo valore di pressione filtrata e il minimo per ciascun battito, quindi il
delta picco-picco di pressione filtrata in passa alto. Si identifica quindi il
momento in cui il delta di pressione picco-picco ha raggiunto la massima
ampiezza. Si registra l'ampiezza massima raggiunta e la si associa alla
pressione media del vaso.  Per rintracciare la pressione sistolica si procede
confrontando a ritroso i valori passati di delta di pressione picco-picco con
il massimo delta appena calcolato.  Quando un valore passato raggiunge il 55%
(valore ottenuto sperimentalmente) è stato trovato il valore di pressione
sistolica.  Per rintracciare la pressione diastolica si confrontano i delta di
pressione picco-picco successivi con il massimo delta fin quando non si
raggiunge l'85% sperimentale. Rintracciate sia la pressione sistolica che la
diastolica, si apre la valvola di scarico e le si mostra a schermo.

Effettuamo quindi una misura in maniera automatica sostituendo allo stetoscopio
con un'analisi che consente di estrarre la componente in alta frequenza durante
lo svuotamento della cuffia. Per fare questa cosa dovrò tenere traccia di tutti
i delta incontrati, che dovranno essere opportunamente salvati in un array.
Trovato il picco massimo devo rintracciare nel passato e scoprire quando una
differenza superava il 55% di questo valore massimo e trovare poi quando il
valore picco-picco scende al di sotto della soglia impostata per la diastolica.

---

Nello **strumento reale** di misura della pressione arteriosa presente
all'interno di un monitor per la terapia intensiva si hanno diversi elementi.

- Cuffia. Questa ha due tubetti perchè quando si gonfia non si misura anche il
  rumore dovuto ai picchi del compressore e posso misurare in maniera più
  accurata la pressione interna.
- Valvola di auto-azzeramento: esiste un problema di drift del sensore e se lo
  si lasciasse inalterato lo zero non rimarrebbe fisso ma varierebbe. Si vuole
  effettuare le cosiddette misure di zero prima di poter utilizzare questo
  sensore sul paziente stesso. Quindi vado a vedere cosa dice il sensore quando
  questo è sottoposto a pressione atmosferica, leggo il valore e questo sarà,
  nell'equazione di calibrazione del sensore, l'intercetta. Bisognerà quindi
  toglierlo dalla lettura di modo che la pressione misurata parta proprio da
  zero. Questa procedura è semplice da fare: stacco il tubetto dalla cuffia,
  espongo il sensore in atmosfera, faccio la misura e poi ricollego il tutto.
  Operativamente è semplicissimo da fare ma si vuole automatizzare il tutto in
  modo tale che se la misura della pressione arteriosa deve avvenire ogni 5
  minuti non debba richiedere al personale infermiere di recarsi dal paziente
  per effettuare manualmente l'azzeramento. Tutte le volte che voglio fare uno
  zero in un sensore si cercano di costruire degli oggetti che consentano di
  effettuare questa misura senza l'intervento esterno. Questo dispositivo
  operativamente collega il sensore di pressione verso l'esterno anzichè
  collegarlo verso il manicotto. Altro non è che una valvola ON/OFF collegata
  ad una porta digitale in uscita. Attivando la porta questa valvola si apre e
  il controllore può misurare il valore dello zero tramite l'opportuno sensore.
  Normalmente la valvola è aperta mentre la chiusura avviene attivamente, con
  richiesta di corrente. In condizioni di riposo quindi la valvola rimane
  chiusa.
- Amplificazione della pressione della fascia e delle oscillazioni nella
  pressione della fascia: I due segnali tra loro sono molto diversi in termini
  di full scale range. La pressione nella fascia infatti ha un FSR di centinaia
  di mmHg mentre le oscillazioni sono di pochi mmHg. Se si utilizzasse lo
  stesso circuito d'amplificazione per entrambe le misure ed un ADC con uno
  scarso numero di bit non si riuscirebbero a registrare correttamente le
  ampiezze delle oscillazioni nella pressione della fascia. Questo porterebbe a
  dei risultati inattesi una volta applicato l'approccio oscillometrico. Si
  utilizzano due circuiti d'amplificazione separati: si amplifica semplicemente
  il segnale proveniente dal sensore per registrare la pressione nella fascia
  mentre si effettua un filtraggio in passa alto ed una successiva
  amplificazione nel caso delle oscillazioni nella pressione di fascia. Il
  condensatore in parallelo per effettuare anche una fase di passa basso,
  determinando la banda vera e propria del segnale.
- Il multiplexer serve a commutare i due segnali in ingresso così da effettuare
  le misurazioni con un singolo ADC.
- Sistema di gonfiamento: compressore.
- Sistema di sgonfiamento: valvola.
- Valvola di scarico: Se si ha solamente la valvola di scarico, lo sgonfiamento
  avviene con una certa costante di tempo e dinamica esponenziale. Questa
  costante di tempo dev'essere sufficientemente lenta da garantire una
  misurazione corretta. È anche necessario uno svuotamento più veloce della
  cuffietta (i.e. costante di tempo più rapida). Quello che si fa è mettere una
  seconda valvola che svuota invece più velocemente la cuffietta. Questa
  valvola viene aperta solo quando ho misurato la minima. Di solito questa
  valvola è costituita da un solenoide. L'avvolgimento "tira" un attuatore
  metallico che apre il condotto.
- Interruttore per pressione eccessiva: questo è isolato rispetto al
  controllore. È uno degli elementi che si inserisce sempre quando si
  progettano dispositivi che applicano energia al paziente. Se creano sistemi
  di sicurezza che funzionino anche nel caso in cui il microcontrollore non
  funzioni come deve. Si ipotizzi ad esempio che il processore si blocchi allo
  stato di gonfiamento della cuffietta o che il MOS che comanda il compressore
  vada in cortocircuito. Per evitare che si danneggi il sistema e/o il paziente
  per via delle alte pressioni presenti si utilizza questo dispositivo. È una
  molla che comprime una sferetta sull'uscita di un tubo. Quando la pressione
  nel tubo produce una forza sull'elemento occlusivo una forza superiore a
  quella della molla, la sfera si sposta lasciando fuoriuscire l'aria. Si
  tratta quindi di una valvola di sicurezza. 

Il miglior distretto corporeo dove effettuare la misura è nella parte alta del
braccio, dove si ha un accesso diretto all'arteria. L'unico problema è dovuto
alla scomodità per l'utilizzo quotidiano. Alcuni dispositivi sfruttano il polso
come distretto corporeo in cui effettuare la misura. Questo non è sicuro come
la parte superiore del braccio e quindi gli algoritmi d'analisi devono
effettuare internamente delle correzioni.

L'accuratezza del sistema di misura di pressione è determinata dall'accuratezza
del sensore di pressione, il giusto posizionamento, l'analisi del metodo
oscillometrico ma anche il controllo della pendenza della curva di pressione
durante la fase di sgonfiamento. La pendenza della curva ha un impatto sulle
capacità di effettuare la misura. A seconda di quanto è ripida la pendenza si
avrà una variazione differente tra la pressione misurata in un battito rispetto
all'altro. La cosa sgradevole di questa cosa è che l'accuratezza non dipenda in
assoluto dalla pendenza. Ciò che determina l'accuratezza è il rapporto tra la
pendenza della curva e la frequenza cardiaca del nostro paziente. Se il
paziente ha una frequenza cardiaca molto elevata è possibile identificare molto
elevata, posso identificare bene la pressione sistolica anche con una pendenza
molto elevata. Se sono in bradicardia e ho una basssa frequenza cardiaca avremo
che la stessa pendenza può portare un'accuratezza ridotta.

Esistono per questo motivo delle variazioni sul tema nel quale, invece di avere
uno sgonfiaggio continuativo, questo viene eseguito seguendo una curva con una
serie di gradini di valore noto. Il sistema in questo caso raggiunge la
pressione massima che verifica non vi siano suoni, scende di livello in livello
di un certo quanto di mmHg per volta. Quando la valvola di sgonfiaggio è
chiusa, il sistema aspetta che siano stati registrati due battiti, in modo da
avere variazione oscillometrica ben determinata. Questo è un modo per
discretizzare la discesa e per rendere l'accuratezza indipendente dalla
frequenza cardiaca del paziente. L'accuratezza del sistema è limitata
all'altezza del gradino di pressione.

Esistono degli ALGORITMI ALTERNATIVI per rintracciare la pressione sistolica e
diastolica. Questi algoritmi si basano sull'analisi di un segnale che
corrisponde alla distanza dei due inviluppi ricavati dal segnale delle
oscillazioni. Posso quindi definire il massimo come pressione media e poi
applicare uno degli approcci: oscillometrico, tramite valori di derivata (cerco
i punti di massima pendenza). Questi algoritmi si basano su delle assunzioni
che cercano dei parametri quantitativi per trovare la miglior approssimazione
del parametro di pressione sistolica e diastolica.

Una serie di assunzioni limitano l'applicabilità in alcuni contesti. Ogni
paziente ha le sue alterazioni che possono cambiare per effetto della
patologia. Questa è la ragione per cui ad ogni metodo utilizzato vengono
associati dalle società mediche dei limiti sull'applicabilità. É necessario
trovare il trade-off tra l'ottenimento di una misura accurata e la possibilità
di effettuare misura in contesti meno controllati.

##### Metodi di tipo invasivo

Dal punto di vista dello strumento è di più facile realizzazione mentre dal
punto di vista del paziente è meno gradevole. Bisogna misurare la pressione
laddove essa si sviluppa, in particolare all'interno di un'arteria. Esistono
due tipologie di approcci: quello extravascolare e quello intravascolare (in
riferimento al punto in cui il sensore viene collocato per la misurazione).

**Extravascolare**

Il metodo che utilizza il trasduttore extravascolare è l'approccio più comune
nelle terapie intensive. Il trasduttore è un elemento critico nella misura
della pressione. Per poterlo applicare all'interno del vaso è opportuno
miniaturizzarlo tanto da includerlo in un catetere. Questo sensore inoltre
verrebbe esposto ai fluidi corporei del paziente (i.e. contaminazione) quindi
bisognerà poi procedere smaltendolo (nel caso in cui sia disposable) oppure
sterilizzarlo (con tutti i rischi di rovinare l'apparato).

La misurazione intravascolare avviene solo in particolari casi diagnostici ma
per un monitoraggio standard di pressione in terapia intensiva si utilizza
l'approccio extravascolare. Questo perchè i costi di produzione di tali
dispositivi sono relativamente inferiori.

Il catetere è introdotto a livello di un'arteria per rilevare la pressione
esternamente alla stessa. Esistono diverse forme di trasduttori.

Si utilizza un trasduttore extravascolare in una prima tipologia perchè alcuni
trasduttori sono troppo grossi per essere gestiti intravascolarmente.

La linea d'ingresso al trasduttore di pressione ha dell'eparina
(anticoagulante) e della soluzione salina, in modo da accogliere il sangue del
paziente senza che coaguli nel dispositivo.

Attorno alla sacchetta di soluzione salina vi è una cuffia che può essere
gonfiata per pressurizzare il liquido (ricordiamo che il catetere poi va in
arteria e non in vena).

**Intravascolare**

???

La trasduzione avviene all'interno del vaso e non all'esterno. Per avere valori
più diagnostici si utilizza una trasduzione meno affetta dal fenomeno di
passabasso dovuto alle linee di trasmissioni (presente nel caso
extravascolare).

Il catetere ha un elemento piezoresistivo in arteria che pone dei limiti sulla
misura sulla stabilità del sistema.

Esistono delle alternative interessanti che utilizzano una trasduzione
intermedia: da pressione a capacità di riflettere una radiazione, poi questa
verrà ulteriormente trasdotta.

abbiamo due fasci di fibre ottiche in parallelo: dal punto di vista fisico
abbiamo che la lunghezza tra LED e photodetector è di circa 1 metro. Queste
fibre terminano in una porzione avente una membrana deformabile. Si crea uno
specchio che riflette la luce che arriva dalle fibre ottiche. Questa luce viene
riflessa. Il raggio viene riflesso solo se si trova all'interno di un angolo
critico. Il fatto che lo specchio sia deformabile consente a più o meno
intensità luminosa registrata dal fotodiodo. Le variazioni di intensità non
sono lineari con la pressione. La caratteristica riportata è assolutamente non
lineare. Non è un problema perchè basterebbe una look-up table e dei
microcontrollore.  La roba brutta è il fatto che questa funzione porta ad avere
lo stesso valore d'uscita a fronte di diverse pressioni applicate. La funzione
non è monotona. Per risolvere il problema possiamo considerare una porzione
della curva caratteristica.  La parte che viola la monotonicità del sensore è
associata a valori della variabile in ingresso che non si riscontrano nella
pratica della misura. Si utilizza solo la parte associata all'operating range.
Se riusciamo ad avere il picco a circa 300-400cmH2O posso garantire l'utilizzo
di questi sistemi.

Per la misura della pressione sono sviluppati alcuni dispositivi. Questi si
basano sulla tonometria. Vogliamo misurare la pressione interna nel vaso. Col
non invasivo abbiamo applicato ad un distretto corporeo una pressione andando a
vedere quando il vaso si occludeva.  Applicando una tensione, creando un
appiattimento del vaso, si sviluppano delle forze di tensione. Queste forze non
possono ???

Altri sistemi consistono nell'applicazione dall'esterno una pressione. Non si
può sapere qual è la pressione all'esterno ma se devo farlo ....  combino
quest'approccio con la misura pletismografica. Vedendo l'assorbimento di luce
che è in funzione della quantità di sangue. All'aumento della intensità
luminosa aumenta la pressione esterna fornita. In questo modo si cerca di
misurare esternamente la pressione del vaso per mantenere costante il diametro
dell'arteria all'interno. La pressione del manicotto rappresenta la pressione
interna del vaso.

Ultimo robo. Posso posizionare la punta del catetere in diversi luoghi del
sistema cardiovascolare per misurare la pressione d'interesse. Naturalmente
quella che ci interessa di più è quella arteriosa. Il catetere di Swan Ganz è
un sistema per misurare pressioni per il controllo del circolo polmonare.
Questo è un circolo che ha meno bisogno di delta di pressione tra ingresso e
uscita per produrre ... Ha una sruttura che porta ad una vicinanza al sangue.
La membrana a livello capillare si hanno solo alcune cellule con matrice
extracellulare. Aumentando la pressione sul circolo polmonare si producono
delle rotture meccaniche del capillare che può portare a edema polmonare,
situaione molto critica. Per misurare la pressione in questo luogo vi sono due
approcci:

1. Ultrasuoni, ecografia. Si misura la dilatazione facendo delle assunzioni.
2. Più accurato, col catetere di Swan Ganz. Questo ha una punta con un
   palloncino che può gonfiarsi e sgonfiarsi. Vi sono tanti tubi connessi.
   Sgonfio il palloncino e inserisco il catetere nel cuore.  Passo in arteria
   polmonare. Quando sono nel ventricolo posso vedere la pulsatilità del cuore.
   Il palloncino offre una resisenza al flusso sanguigno che quindi si dirige
   verso l'arteria polmonare. ???

## Elettrocardiogramma e derivazioni

Richiami sui concetti base. Qual è la struttura base dei circuiti e del segnale
elettrocardiografico.

L'elettrocardiografo.

Si inseriscono resistenze da 10k a valle degli operazionali in quanto
proteggono nel caso di cortocircuiti in quello stadio.

Delle reti di diodi costituiscono una protezione dellamacchina stessa.

Differenza tra un diodo ideale e uno reale. Il diodo ha una soglia che dipende
dal semiconduttore. 0.6V è poco ma abbiamo dei segnali che provengono dal corpo
dell'ordine dei milliVolt. Usando dei diodi al silicio posso limitare a +0.6 e
-0.6V, circa 100 volte ciò che voglio misurare. 

Nel caso di defibrillatore la resistenza di 10k dev'essere ad alta potenza in
quanto se non lo fosse la tensione. La corrente che passa nella resistenza e
poi nei diodi è assolutamente limitata e quindi i diodi non si bruciano.

### La ventilazione meccanica ed il ventilatore meccanico

La ventilazione è il problema che la macchina deve risolvere. Quello della
ventilazione meccanica. Un soggetto, per qualche motivo, non può provvedere
autonomamente alla ventilazione polmonare. Questa è una fase respiratoria. La
respirazione parte dall'atto ventilatorio che sposta gas da e verso il polmone
ma anche lo scambio gassoso agli alveoli, il passaggio del O2 nel circolo
sanguigno e lo scambio a livello cellulare. Noi ci stiamo occupando della
ventilazione polmonare ossia lo spostamento dentro e fuori dal polmone. Perchè
è necessario? Perchè il paziente non è in grado di effettuare la ventilazione
durante un intervento sotto anestesia ad esempio. In caso di patologia invece,
se per qualche motivo il paziente ha un polmone che non funziona come dovrebbe.
Il ventilatore non cura, può modificare lo stato patologico ma la funzione
principale è quella di mantenere la ventilazione del paziente per un periodo
limitato di tempo.

Quando si studia questo problema si studia il fenomeno di trasporto di gas
attraverso condotti di macchina e paziente. In termini di flussi e pressioni
possiamo usare analogo elettrico del sistema. Un tubo non è altro che non una
resistenza. Al posto della tensione abbiamo la pressione (ciò che produce lo
spostamento del fluido) e al posto della corrente il flusso (volume nell'unità
di tempo). Possiamo quindi modellizzare vari aspetti del problema con
resistenze, inerzie e capacità. Il sistema respiratorio può essere semplificato
con tubi collegati a palloncini. Forma un albero binario simmetrico. Il sistema
respiratorio è ben bilanciato e rappresenta il sistema respiratorio sano. Le
malattie alterano in maniera eterogenea il polmone e questi modelli variano.

I gas sono comprimibili.

$C_g = \text{TGV}{\lambda(P_B - P{H2O})}$

L'inerzia è legata alla velocità della massa. La tempistica dell ???

### Atto meccanico

Come produrre l'atto meccanico? si applica una pressione positiva alle vie aeree.

Polmone d'acciaio: ???

La ventilazione meccanica a pressione positiva produce l'inspirio.

Si mostra un generatore ideale di pressione. Per produrre un inspirio ho un
generatore di pressione. Questo è un generatore in alternata. Abbiamo un
sistema chiuso in cui vogliamo portare flusso ma anche riprendere il flusso.
Abbiamo tubo e poi alveoli. Il modo di funzionare del sistema è quello di un
circuito RC.

???

## Misura dei flussi

Richiede un sensore che sia in grado di misurare qualsiasi molecola d'aria che
entra.

Ci sono 2 problemi:

1. Il gas è contaminato, quindi i sensori devono essere puliti o disposable.
2. Il gas è umido che possono accumularsi ed interagire con sensori.

Esistono diverse tecnologie:

- a turbina
- a ultrasuoni
- hot-wires

Pneumotacografo: resistenza al flusso di tipo lineare che produce un delta
pressorio proporzionale al flusso stesso. Creo una zona a livello del condotto
che produce una resistenza al flusso. La pressione è paragonabile all tensione
e il flusso è la corrente. Imponiamo una resistenza e basta misurare la
pressione ai capi per capire poi il flusso. Il paziente e la resistenza sono in
serie. La resistenza dev'essere piccola per non fare far fatica al paziente ma
sufficientemente grande da permettere una corretta misurazione.

L'utilizzo di mesh (forellini) è fatta per produrre flussi laminari. Se i
flussi fossero turbolenti la relazione sarebbe quadratica. Questo sistema ha un
provlema particolare dovuto all'umidità: il valore della resistenza cambia in
base alla quantità d'acqua presente all'interno dei forellini.

Per risolvere questo problema ci sono stati vari approcci. Si studia sempre il
delta di pressione e usano per creare resistenza una membrana che può essere
meccanica o plastica che viene spostata dal flusso. La resistenza del flusso a
questo sistema è data dalla sezione libera. La resistenza diminuisce a seconda
della sezione libera. Più è aperta la porta e meno resistenza oppone.

???

## Sensori a filo caldo (Hot-wire)

Utilizzano fili metallici caldi percorsi da corrente. La strutura è quella
riportata in figura. Attraverso questa file passa corrente e per effetto joule
ho una certa resistenza. Il principio è quello di mantenere la temperatura
costante, ad esempio a 50 gradi. Se soffio sul filo aumenta lo scambio di
calore tra filo e gas e se l'aria è più fredda porterà via calore. Più è veloce
il gas e maggiormente avverrà lo scambio gassoso. Misurando la corrente
necessaria per mantenere la temperatura costante troverò un parametro che lo
lega al flusso d'aria. Il vantaggio è che se tengo la temperatura elevata ho
anche un'immunità all'acqua e un sistema che funziona bene in sistemi umidi.
L'inconvenente è che questi sistemi dicono la velocità ma non la direzione. Per
dire la direzione possiamo usare un sensore di pressione per capire se va da
una parte o dall'altra oppure si usano due fili caldi con in mezzo un pezzo di
plastica che protegge dal vento. Il sensore che ha bisogno di più corrente è
quello esposto al flusso direttamente e quindi è facile determinare la
direzione.

## Sensori ad ultrasuoni

???

Oltre alla misura del flusso vi è un'altra considerazione.
Il volume del gas cambia a seconda della emperatura e
dell'umidità. Bisogna correggere il volume. ATP(S) ???
BTP(S) body temperature saturato con vapore d'acqua.
La pressione barometrica fa molto inoltre.

## Valvola espiratoria

La linea espiratoria è una linea che va ad una valvola che consente di aprire
l'espirazione quando il paziente espira altrimenti la chiude. Il paziente
dev'essere sempre un po' gonfio però e non lo mando esattamente a pressione
atmosferica. Per questo si usa una valvola PEEP. Questa valvola è ottenuta con
solenoidi (sistema voice coil). Il per nino si allontana o avvicina. Il
condotto ha una parte centrale collegata con l'ingresso al paziente ed un
diaframma di gomma. Quando do corrente il pernino tappa l'uscita di gas della
linea che va al paziente. Riducendo la corrente invece riduco la forza per
tappare l'uscita e comincerà a sfiatare dell'aria. Creo un controllo in anello
chiuso tra paziente e peep ???
