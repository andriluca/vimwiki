# Lezione 6 --- Ecografo

Il requisito temporale è *fondamentale* nel contesto ecografico. Di seguito si
riportano delle soluzioni di ecografi che utilizzano microcontrollori per
riuscire a soddisfare i requisiti dei diversi blocchi funzionali che servono
per creare l'immagine.

I **vantaggi** dell'ecografia sono i seguenti:

- Consiste in una tecnica essenzialmente *sicura e senza radiazioni
  ionizzanti*. *Non è dolorosa e può essere applicata senza conseguenze per il
  paziente*. 
- Non ha bisogno di infrastrutture, le macchine sono molto piccole e compatte.
- Consente di avere immagini in real-time (importante requisito per fasi
  interventistiche). Se volessimo effettuare un campionamento di una
  particolare zona di tessuto, riuscire ad avere una guida accurata di ciò che
  stiamo facendo è fondamentale. L'ecografia è uno fra i sistemi più comodi
  perchè possiamo vedere dove sta andando l'ago e quando l'ago ha raggiunto la
  posizione giusta per effettuare il prelievo. 

I **limiti** di questa metodologia:

- adatta esclusivamente a tessuti molli ma non adatta per strutture interne di
  polmoni (più in generale, ovunque sia presente gas) e ossa. In realtà
  ultimamente si stanno utilizzando gli ultrasuoni per effettuare diagnostica
  dei polmoni ma in questo caso si sfruttano di più gli artefatti
  all'interfaccia che la vera informazione sulla morfologia delle strutture.
  Nell'aria gli ultrasuoni vengono smorzati, non si propagano e non producono
  gli echi per identificare variazioni morfologiche rilevanti.
- La qualità della diagnosi ha bisogno di un'adeguata formazione
  dell'operatore. La qualità dell'immagine è un aspetto critico per poter
  riuscire ad effettuare una diagnosi corretta.

## Principio di funzionamento

### Fenomeno fisico

Sfrutta la riflessione che avviene al fascio di ultrasuoni nel momento in cui
il mezzo che lo trasmette modifica le sue proprietà meccaniche. Le proprietà
meccaniche del mezzo sono associate alla velocità di propagazione del fascio
ultrasonico. Le variazioni di queste portano dunque ad una differente velocità
di propagazione dell'onda acustica nel mezzo stesso. In Figura
\ref{fig:physical_principle} è possibile notare alcuni esempi di strutture
semplificate (corrispondenti a diversi strati di tessuto) con velocità di
propagazione del suono diverse per ognuna. All'interfaccia tra queste vengono
prodotti gli echi.

![Fenomeno fisico della riflessione del fascio ultrasonico.\label{fig:physical_principle}][physical_principle]

In Figura \ref{fig:physical_principle} i può notare un esempio di segnale
primario che vogliamo estrarre: l'ampiezza degli echi in funzione del tempo.
Nota la velocità di propagazione del segnale, avendo l'ampiezza degli echi in
funzione del tempo possiamo ricostruire la geometria, ossia quanto distano
queste strutture dal nostro sensore.

Considerati:

- la velocità di propagazione del suono: $v$
- Il tempo di rilevazione del primo eco (first reflection): $t_1$
- Il tempo di rilevazione del secondo eco (second reflection): $t_2$

È possibile ricavare facilmente il seguente sistema d'equazioni:

$\begin{cases} 2L_1 = v \cdot t_1 \\ 2L_2 = v \cdot t_2 \end{cases}$

Da cui si ricava lo spessore tra le due interfacce ($\delta$):

$$\delta = L_2 - L_1 = \dfrac{v\cdot (t_2 - t_1)}{2}$$

Per cui da una misura legata agli intervalli temporali è possibile ricavare
delle informazioni relative agli spessori di materiali con diverse proprietà
meccaniche.

### Step necessari

I passaggi per effettuare la misura:

- *Generazione del fascio ultrasonico*. Bisogna generare un impulso di
  ultrasuoni che viene trasmesso verso i tessuti.
- *Accoppiamento o adattamento d'impedenza acustica*. È necessario essere
  sicuri che l'impulso venga trasferito dalla sorgente (aka attuatore) al
  tessuto. 
- *Rilevare gli echi*. Questi sono di ampiezza estremamente limitata. È
  necessario ricordare infatti che l'attenuazione del segnale avviene in
  maniera esponenziale. Maggiore è il percorso del suono e minore sarà
  l'ampiezza dell'eco (vedi [**time gain
  compensation**](#time-gain-compensation)). Inoltre non tutto il suono viene
  riflesso all'interfaccia ma in parte viene anche rifratto. Il detector dovrà
  dunque essere in grado di percepire segnali anche molto piccoli in ampiezza.

### Problematiche e aspetti critici nella realizzazione della sonda

Ci interessa capire come si fa a realizzare dei sistemi di questo tipo. Vi sono
alcuni aspetti critici da considerare. Sono i seguenti.

1. Generazione del fascio di ultrasuoni e come applicazione di questo al
   tessuto e la ricezione degli echi. Si possono utilizzare diversi approcci:

	1. *Utilizzo di attuatore e sensore indipendenti*. Si impiega un
	   generatore di fascio ultrasonico e un sensore che possa ricevere gli
	   echi (intese come due strutture fisicamente separate).
	   Quest'approccio presenta delle difficoltà nella focalizzazione del
	   fascio in quanto le due strutture sono poste a distanze finite tra
	   loro.
	2. *Utilizzo di un unico attuatore/sensore*. Esistono materiali che
	   possono offrire sia la funzione di generazione del fascio
	   ultrasonico che di sensore di echi. Si tratta di **materiali
	   piezoelettrici** che costituiscono uno standard nello sviluppo di
	   soluzioni ecografiche. I piezoelettrici sono in grado di modificare
	   la carica depositata alle loro estremità in funzione della
	   deformazione fisica applicata (trasduttore o sensore di echi) e di
	   modificare la geometria nel momento in cui un potenziale è applicato
	   (i.e. attuatore o generatore di impulsi ultrasonici).

   Per la progettazione della sonda si considerano dunque i piezoelettrici.
   Questi sono sia in grado di modificare la quantità di carica ai loro capi
   nel momento in cui viene applicata loro una forza, sia di modificare la
   propria configurazione se un potenziale elettrico viene applicato. Dal punto
   di vista strutturale sono dei materiali ceramici ottenuti creando una
   sottile fetta di materiale che viene metallizzato ai due lati per
   creare due elettrodi. Questi elettrodi vengono utilizzati sia per applicare
   potenziale che produce la distorsione del reticolo cristallino (i.e.
   deformazione del materiale stesso) che per prelevare le variazioni di
   quantità di carica dovute alla deformazione fisica dell'oggetto data
   dall'applicazione di segnali e onde di pressione dall'esterno.

   Nelle sonde vengono ricavati tanti sensori e questo porta ad una fragilità
   dello strumento stesso. Non se ne utilizza solamente uno ma un array. Da una
   parte vi è del materiale nero isolante (dove c'è il layer conduttivo comune)
   mentre dall'altra vi è la metallizzazione. Quando si vuole realizzare un
   dispositivo a ultrasuoni (attuatore/sensore) bisogna partire dalle sue
   caratteristiche fisiche. Bisogna decidere qual è frequenza che si vuole
   utilizzare e su cui voglio effettuare l'analisi (e.g. da qualche MHz a 15
   MHz), progettare la struttura fisica del sensore stesso in modo che esso
   risuoni alla frequenza che ci interessa esplorare. Una volta scelto il
   materiale e definita la sua geometria, queste informazioni portano a
   definire qual è la caratteristica del sensore stesso che viene rappresentata
   tipicamente con una funzione di trasferimento riportata in Figura
   \ref{fig:us_characteristics} che dice quant'è di fatto l'ampiezza delle
   vibrazioni che possiamo ottenere al sensore mettendo al centro la sua
   frequenza di risonanza (e.g. si progetta una frequenza centrale di circa
   5MHz e a seconda della struttura posso avere una risposta diversa). Dalla
   Figura \ref{fig:us_characteristics} è possibile anche ricavare il fattore Q.
   
   ![Esempi di curve caratteristiche di dispositivi ad ultrasuoni.\label{fig:us_characteristics}][us_characteristics]
   
   Considerati:

   - Frequenza centrale di risonanza dello strumento: $f_0$
   - La larghezza della distribuzione delle frequenze (larghezza della banda): Bandwidth
   
   $$Q = \dfrac{f_0}{\text{Bandwidth}}$$

2. Garantire che il sensore progettato produca le forme d'onda che vengono
   trasmesse in maniera opportuna al tessuto che si vuole studiare.

   L'approccio che si utilizza normalmente per studiare le risonanze e i
   relativi accoppiamento tra i vari materiali è il seguente.
   Considerati:
   
   - Lo spessore del materiale piezoelettrico (quindi della slice): $L_c$
   - La velocità del suono: $c$
   
   $$f_{res} = \dfrac{n\cdot c}{2\cdot L_c} \rightarrow \boxed{L_c = \dfrac{n\lambda}{2}}$$
   
   Per ottenere risonanza al sistema dobbiamo fare in modo che lo spessore del
   dispositivo sia pari ad un multiplo della lunghezza d'onda associata alla
   frequenza che dobbiamo produrre. La lunghezza d'onda è definita dal rapporto
   tra la velocità di propagazione dell'onda nel mezzo (che dipende dalle
   proprietà meccaniche del mezzo considerato) e la frequenza. Questa velocità
   di propagazione nel mezzo varia se si considera un materiale ceramico
   rispetto ad un tessuto. Vorrei che lo spessore sia tale per cui all'interno
   di quello strato vi sia sempre un numero finito di lunghezze d'onda della
   frequenza che voglio utilizzare per la diagnosi. 
   
   Quando il cristallo si mette ad oscillare vi sono due **problemi**:
   
   1. Le oscillazioni **vengono trasmesse sia da un lato che dall'altro**
      mentre vorrei che l'onda generata si propaghi solamente verso il tessuto
      da solo uno dei due lati. Gli echi nella parte posteriore
      dell'attuatore/sensore sono indesiderati, pertanto vanno attenuati. Gli
      echi provenienti dalla parte posteriore del sensore generano del rumore
      se non si provvede ad attenuarli opportunamente. Dobbiamo fare in modo
      che l'energia prodotta nella parte posteriore della sonda venga
      dissipata. Per risolvere questo problema si utilizzano di solito dei
      materiali, i cosiddetti *backing materials*. Possiedono due principali
      caratteristiche: *una velocità di propagazione dell'onda acustica che sia
      compatibile con la velocità all'interno dell'attuatore/sensore* per fare
      in modo che non vi siano molti echi all'interfaccia col sensore stesso e
      *un altissimo livello d'attenuazione*: fare in modo che l'ampiezza del
      segnale che viene trasmessa e passa all'interno di questo, si attenui
      molto rapidamente durante il percorso. Il backing material non ha
      spessore infinito e, alla fine di questo, l'eco occorrerà ma la seconda
      caratteristica di questo materiale farà sì che l'ampiezza del segnale
      giunto al sensore sarà trascurata. Il livello d'attenuazione quindi
      dev'essere tale da non far registrare al sensore un'ampiezza degli eco
      rilevante nella parte posteriore. Lo spessore complessivo del sensore
      sembra essere maggiore, questo è dovuto alla presenza di questo backing
      material. Questo materiale si presenta gommoso ed attenua il segnale
      degli ultrasuoni. Il sensore è collegato attraverso un circuito
      elettronico flessibile (aka flexible PCB) ricco di piste che che portano
      il contatto elettrico di ognuno dei minuscoli sensori (in particolare
      delle parti superiori, quelle più esterne e separate tra loro) che
      costituiscono l'array all'elettronica di controllo. L'uscita va al cavo
      dello strumento.
   2. **Problema di mismatch o  disaccoppiamento dell'impedenza acustica**. Il
      sistema *deve ottimizzare l'accoppiamento e la trasmissione dell'energia
      acustica al tessuto*. Usando dei piezoelettrici, questi sono ceramici e
      la velocità di propagazione del suono in questi materiali è ben diversa
      rispetto a quella all'interno dei tessuti. Se accostassimo direttamente i
      piezoelettrici al tessuto produrremmo un enorme eco e l'energia prodotta
      dal trasduttore rimarrebbe nel trasduttore stesso e non verrebbe
      trasferita in maniera efficiente al paziente. La riflessione dipende
      dalla differenza di velocità di propagazione del suono nel mezzo:
      maggiore questa differenza, maggiore l'entità della riflessione. Abbiamo
      che l'energia acustica prodotta dal sensore non si trasferirebbe quindi
      in maniera efficiente (in percentuale, l'intensità trasmessa rispetto a
      quella incidente risulta del 18%). Per questo motivo si frammezza tra
      tessuto e sensore uno strato (o più) denominato *matching layer* che
      serve a garantire un miglior adattamento d'impedenza tra tessuto e
      trasduttore. Questo si presenta come un materiale gommoso che migliora il
      trasferimento dell'energia tra sensore e tessuto. 

   Una volta effettuato l'accoppiamento generatore/tessuto, è possibile
   produrre un fascio di ultrasuoni che entra correttamente nei tessuti.

### Fascio ultrasonico

Il fascio ultrasonico ha la caratteristica di essere divergente. Esistono due
aree: quella di Fresnel e di Fraunhofer. La prima fase, di lunghezza L, è
legata alla lunghezza d'onda e alle caratteristiche fisiche del dispositivo che
la genera ($L = \dfrac{d^2}{4\lambda}$). In questa fase, il fascio si muove
lungo la stessa direzione (formando di per sè un cilindro). Per lunghezze
maggiori (zona di Fraunhofer) il fascio divergerà. Da un punto di vista pratico
la divergenza significa vedere cosa è presente di fianco al sensore.
Quest'informazione è da escludere se si vuole andare ad analizzare una
particolare area posta di fronte al sensore, dato che genera solo rumore. Se
gli echi registrati provenissero da strutture che si trovano di fianco al
sensore la capacità di discriminare le informazioni sulla morfologia sarebbe
inferiore. Questo problema è analogo a quello che si ha scattando una
fotografia: il fotografo vuole raccogliere la luce che giunge da ogni punto e
che sia associato ad una posizione geometrica ben definita e se questa
relazione non è precisa si comincia a notare una sfocatura nel risultato. Lo
stesso avviene anche nell'ecografo.

#### Risoluzioni del dispositivo

Se si accoppiasse un singolo trasduttore al tessuto così com'è non sarebbe
possibile ottenere un fascio estremamente focalizzato ma vi sarebbero le due
zone appena descritte.

Si va quindi ad impattare sulla risoluzione del nostro dispositivo. Le
risoluzioni, a diffenza del caso di una macchina fotografica in cui quelle dei due
assi dell'immagine 2D sono determinate dallo stesso fenomeno fisico, sono
definite da fenomeni diversi: da una parte ho l'effetto della focalizzazione
del fascio ma dall'altra la profondità viene misurata come tempo d'attesa
dell'eco. A seconda dell'asse considerato nell'immagine finale avrò diverse
risoluzioni perchè i principi e i problemi fisici che portano a queste
risoluzioni sono diverse.

![Rappresentazione delle risoluzioni del trasduttore (omessa quella trasversale).][echo_transducer]

Nell'ambito ecografico si distinguono:

- *Risoluzione laterale*: perpendicolare al suono, parallela all'asse dove
  giace l'array di sensori. Dipende dalla morfologia del raggio ultrasonico
  (beam width), dalla distanza dal cristallo e dal diametro del cristallo.
- *Risoluzione assiale*: in direzione al suono. Dipende dalla frequenza del
  pulso ossia dalla risoluzione temporale nel sentire l'eco che torna e avere
  una buona trasmissione del segnale con una dinamica elevata in modo che non
  si creino dei ritardi nell'eco che possono portare a questa deformazione.
- *Risoluzione trasversale*: perpendicolare alle altre due. Dipende dalla forma
  del cristallo e della lente.

##### Determinare le risoluzioni assiale e laterale

Si prenda come esempio un punto usato come target e andiamo a vedere quanto il
nostro sensore è specifico nell'identificare l'eco che si può sviluppare da
esso. Se avessimo un fascio che si allarga (i.e. diverge) quello che succede è
che nel momento in cui il trasduttore si muove orizzontalmente lungo x (MOSTRA)
comincerò a sentire echi quando la parte destra del fascio intercetta il punto.
Continuerò a sentire echi fin quando il punto non esce dal fascio del lato
sinistro. Quindi non vedrò una risposta larga quanto l'oggetto ma quanto la
larghezza del fascio che intercetta l'oggetto. Più il fascio viene focalizzato
e più sarà semplice discriminare diversi punti vicini tra di loro.

- **Risoluzione laterale**. 

  ![Risoluzione laterale.][lateral_resolution]
  
  Si parte da dei phantom (prendo materiali che simula la velocità di
  propagazione del suono nel tessuto ossia gel con dentro oggetti con forme
  geometriche note e aventi una velocità di propagazione diversa, ad esempio
  delle barre). Una volta costruito il parallelepipedo con delle barre inserite
  all'interno, appoggio la sonda ecografica al di sopra e osservo l'immagine
  che ottengo. Ciò che vorrei ottenere è l'immagine di destra ma in realtà, se
  il fascio si allarga e diverge, quello che vedo è che ogni punto produce
  un'immagine più larga di quello che è la dimensione fisica e quindi vado a
  ottenere risultati simili a quello centrale (diversa morfologia).
- **Risoluzione assiale**. 

  ![Risoluzione assiale.][axial_resolution]
  
  Concettualmente è simile alla precedente ma lungo l'asse longitudinale del
  sensore. Nel caso in cui la risoluzione assiale sia particolarmente bassa si
  fondono insieme i punti nell'altra direzione rispetto a quella della
  direzione laterale.

### Focalizzazione

Questi strumenti utilizzano un approccio unico ed interessante per migliorare
l'aspetto della focalizzazione utilizzando l'approccio a microcontrollore.

Il problema, in questo caso, è che il fascio ultrasonico è focalizzato solo
nella prima parte e poi tende a divergere. È necessario compensare la
divergenza con un generatore nel quale il fascio non sia libero di muoversi
partendo da una sorgente parallela ma che abbia, geometricamente, una tendenza
a convergere verso un punto F, detto fuoco del sistema. Le prime due
implementazioni provengono da ragionamenti fatti nell'ambito dell'ottica.

- *Modifica alla geometria del piezoelettrico*. Al posto di utilizzare un
  sensore/attuatore piano, questo viene reso concavo in modo che le vibrazioni
  prodotte da ogni suo punto arrivino, allo stesso tempo, al fuoco del sistema.
  
  ![Piezoelettrico con geometria sferica.][spherical_transducer]
  
- *Frammezzare tra piezoelettrico e tessuto una lente acustica*. Questa
  consiste in un materiale che ha una velocità di conduzione del suono diversa
  da quella del piezoelettrico e del tessuto. In questo modo porto alla
  deflessione del fascio e alla sua focalizzazione.
  
  ![Piezoelettrico con lente acustica.][acoustic_lens]

Il problema dell'approccio derivato dall'ottica è che possiamo definire la
focalizzazione del fascio ma questa *dipende dalla caratteristica meccanica e
dalla posizione delle lenti*. Se volessimo andare a cambiare la posizione del
fuoco sarebbe difficile farlo in queste condizioni. Sarebbe necessario creare
un sistema di lenti acustiche in grado di muoversi tra di loro per spostare
questo fuoco e ciò non è di facile realizzazione.

Il fenomeno acustico però fornisce un ulteriore approccio che non sarebbe
realizzabile considerando la sola ottica classica:

- Suddividere la superficie superiore del piezoelettrico costituendo un array
  di attuatori/sensori. Questi elementi possono essere modulati nella loro
  deformazione attraverso l'applicazione di impulsi, opportunamente ritardati.

Tornando alle soluzioni precedenti la focalizzazione era resa possibile
alterando la geometria del sensore, quindi quando attivo e produco un fronte di
espansione che produce un aumento di pressione davanti questo avrà una
geometria diversa: il percorso compiuto dall'onda collocato lateralmente
risulta più lungo rispetto a quello che dovrebbe compiere l'onda al centro. Per
poter far arrivare tutte le onde allo stesso tempo è quindi necessario
modificare il ritardo con cui i vari contributi vengono generati in funzione
della distanza dal centro, in particolare le onde che intraprendono percorsi
laterali dovranno partire prima di quelle centrali per fare in modo che si
incontrino nel fuoco.

##### Beam steering, focusing e beamforming

L'effetto di *modulazione dei ritardi* viene effettuato nel seguente modo: il
trasduttore viene suddiviso in molti elementi (i.e. si utilizza un array di
trasduttori). Questi sensori possono essere controllati in maniera individuale.
Si considera un sottoinsieme dell'array (8-16 sensori su un array di 128 o 256)
facendo in modo che l'attivazione degli strati metallizzati esterni (quindi
l'attivazione dei singoli traduttori) avvenga con ritardi tali da realizzare un
effetto equivalente a quello di una lente acustica che convoglia ciascun beam
nel fuoco. Ad esempio se volessi utilizzare un certo fuoco devo calcolare le
distanze che devono essere percorse dal fascio d'ultrasuoni nelle varie
posizione della lente acustica "virtuale" per poter focalizzare il fascio
vengono generati degli impulsi che non sono uguali per tutti (non sono
sincroni) ma verrà generata una sequenza d'impulsi leggermente ritardata in
modo che gli elementi lontani vengano attivati prima di quelli centrali. Questa
soluzione è assai più flessibile in quanto si possono realizzare delle
focalizzazioni molto diverse alterando semplicemente i ritardi degli impulsi
con i quali si stimolano i trasduttori. Modulando i ritardi è possibile anche
realizzare il cosiddetto beam steering: si convoglia il fascio ultrasonico
lungo una direzione diversa da quella assunta in condizione di stimolazione
senza ritardi. In questo caso il ritardo varia linearmente con la distanza dal
primo sensore. La possibilità di cambiare il fuoco in cui convogliare l'energia
acustica (noto anche con il nome di beam focusing) sarebbe equivalente,
nell'ambito fotografico, a poter cambiare il fuoco della macchina senza
modificare apertura e caratteristiche fisiche della lente. Combinando beam
steering e focusing il risultato è una focalizzazione con un angolo rispetto al
fronte del dispositivo e prende il nome di beamforming.

Dal punto di vista pratico, prendiamo un sottoinsieme e produco una sequenza di
ritardi al fine di garantire la focalizzazione di un fascio ad una certa
distanza F (che dev'essere ogni volta calcolata). Si parte dalle
caratteristiche geometriche (larghezza del trasduttore, distanze tra
trasduttori), si calcola la distanza del fascio e il ritardo temporale sapendo
quant'è la velocità di propagazione del fascio nel mezzo. Conoscendo la
velocità di propagazione e le differenti distanze al variare del percorso dal
centro alla periferia della lente, è possibile calcolare i differenti $\Delta
t$ per ogni trasduttore e questo ritardo lo applico nel momento in cui fornisco
un segnale ai diversi trasduttori. 

Il problema del beamforming è da considerare anche durante la fase di ricezione
dell'eco. Mantenendo fisso il fuoco in posizione F, i trasduttori centrali
ricevono prima l'eco rispetto a quelli in posizione laterale per lo stesso
motivo citato prima: i percorsi laterali sono più lunghi da percorrere rispetto
a quelli centrali. Beamforming in ricezione significa che prima di sommare
insieme i segnali che giungono ai vari sensori dell'array per ricostruire il
segnale, bisogna introdurre un certo ritardo su ciascuno. Viene applicato un
ritardo ai segnali che giungono ai vari sensori prima di poter effettuare la
somma dei contributi.

## Blocchi funzionali che costituiscono l'ecografo

L'alimentazione è collegata alla sonda ecografica che possiede però due
funzioni (generatore e sensore del fascio ultrasonico). Per condividere le due
funzioni si ha che ciascuna filo esterno alla sonda (proveniente dai vari
elementi dell'array) deve interfacciarsi sia con il circuito che fornisce gli
impulsi necessarii alla generazione del fascio ultrasonico che
all'amplificatore del segnale (echi). 

La generazione dell'impulso è effettuata da un circuito chiamato pulser
applicando dei potenziali che cambiano rapidamente ai lati dei due elettrodi
del trasduttore. Avendo una massa comune e voglio effettuare una transizione di
tensione sarà necessario utilizzare un alimentatore avente una tensione duale
(una positiva e l'altra negativa rispetto a massa) e poi si alternerà
l'applicazione delle tensioni in modo da produrre il rapido cambiamento del
potenziale che produrrà di conseguenza la rapida variazione della geometria.
Questi dispositivi per generare un segnale denso richiedono un applicazione di
ampiezze elevate di segnale (da -60 a +50V). Questi circuiti si portano con se
il problema relativo all'isolamento elettrico della sonda.

L'eco deve essere amplificato. Esistono circuiti semplici per proteggere il pre
amplificatore e questi servono poichè vi è connessione tra il pulser e il pre
amplificatore. Una volta amplificato dobbiamo compensare il beamforming
d'ingresso introducendo ritardi per correggere la focalizzazione e poi bisogna
effettuare l'estrazione dell'inviluppo e la compensazione dinamica del
guadagno.

### Pulser, come funziona

I ritardi devono essere introdotti in maniera selettiva su un sottoinsieme
degli attuatori che costituiscono l'array. Ognuna delle righe che costituiscono
l'immagine viene ottenuta usando un sottoinsieme di questi (8-16). Per ottenere
l'immagine dobbiamo attivare i primi attuatori e poi cambiare di volta in volta
quelli da attivare. E necessario trovare una strategia che permetta di limitare
il numero di pulser e degli amplificatori al massimo numero di elementi in
parallelo per produrre una singolalinea che costituiranno l'immagine.

![Inizializzazione del sistema di generazione del fascio ultrasonico.\label{fig:echo_shift_reg_init}][echo_shift_reg_init]

In Figura \ref{fig:echo_shift_reg_init} vengono riportate le linee di
connessione della flexible PCB ai sensori/attuatori piezoelettrici. Le linee
sono connesse a degli switch digitali. Questi switch utilizzano come driver un
registro a scorrimento (shift register) avente un numero di bit pari al numero
complessivo di elementi della sonda (i.e. 128-256). Lo shift register modula lo
stato di connessione di ciascun attuatore al bus dei pulser (costituito da 8-16
fili). Così facendo è possibile limitare il numero di pulser utilizzati per
generare i fasci ultrasonici. 

Lo shift register viene inizializzato con valori LOW in tutti i suoi bit. Per
registrare la prima riga, la linea di dati dello shift register viene mantenuta
HIGH per un numero di colpi di clock pari al numero di pulser presente nel
sistema di generazione del fascio ultrasonico (e.g. 8). Il registro a
scorrimento che precedentemente aveva solo valori LOW, ora presenta 8 bit con
stato logico HIGH e modulerà quindi i primi 8 switch digitali. Sarà necessario
inviare ora una serie di LOW per shiftare gli 8 bit e quindi il sottoinsieme di
attuatori attivati in quell'istante. Questo permette di scorrere tutti gli
attuatori in una modalità simile a quella di una finestra mobile.

*NB*: Il pattern dei ritardi degli impulsi generati dai pulser deve essere
adattato ad ogni shift, tenendo ben presente l'ordine d'attivazione degli
attuatori.

![Spostamento della finestra di impulsi di generazione ultrasonici.\label{fig:echo_shift_reg_shifting}][echo_shift_reg_shifting]

Esistono limiti dal punto di vista tecnologico: l'uso degli interruttori
produce un aumento delle dimensioni della sonda e devo comunque mantenere
un'elettronica piccola. Il limite di spazio porta a scegliere circuiti che non
sono ottimizzati in termini di rumore. La qualità del segnale risente di questo
tipo di strategia.

La tecnologia per fare dei cavi è oggi migliorata tantissimo. Oggi si utilizza
un cavo che si interfaccia direttamente con 256 elementi. Questo consente di
avere sonde leggerissime quasi senza elettronica ma il problema è dovuto al
fatto di dover connettere 256 cavi alla macchina. Devo realizzare dei
connettori con 256 fili. Questo non è semplice considerando che i cavi e i
connettori sono la principale causa di guasti delle apparecchiature
elettroniche. Più fili ci sono e maggiori sono le probabilità di guasti. Se
voglio avere un buon accoppiamento devo avere una frizione meccanica elevata,
quindi sarà necessaria un'elevata forza d'inserzione (che cresce
proporzionalmente al crescere del numero di connessioni nel cavo). Esiste una
tipologia di connettori chiamati ZIF (Zero Insertion Force): sfruttano una
deformazione delle lamine. Anzichè avere maschio e femmina si hanno tante
lamelline di metallo, quando infilo il connettore nell'altro le lamine dei due
non sono a stretto contatto tra loro. Una volta inserito fino in fondo, si
ruota una leva metallica e le lamine metalliche vengono sciacciate da un lato.
La connessione meccanica non viene fatta al momento dell'inserzione ma in un
secondo momento, quando sono sicuro di aver inserito correttamente il
connettore.

Quindi i 260 pin ZIF che servono sia a collegare i sensori della sonda che a
memorizzare informazioni relative alla sonda stessa (convex o lineare,
frequenza di lavoro, etc). Ogni sonda è diversa dalle altre e porta con sè dei
parametri che devono essere comunicati all'ecografo, così che la macchina si
renda conto della tipologia di sonda utilizzata e non è necessaria alcuna
azione umana (che porta con se potenziali errori). Negli ecografi di fascia
alta sono presenti più prese per attaccare molteplici sonde. Questo perchè
consentono una maggior versatilità senza dover staccare e riattaccare ogni
volta le sonde. Si hanno poi tanti o pochi elementi ripetuti a seconda della
qualità che si vuole ottenere. 64 pulser e amplificatori significa un
parallelismo di 64 elementi per generare fasci ultrasonici.

### Beamforming in ricezione

L'eco arriva in momenti diversi ai vari sensori. Esistono due differenti
strategie per effettuare la somma dei contributi:

- *Linee di ritardo analogiche*: più semplice dal punto di vista di requisiti
  elettrici. I vari segnali provenienti dai sensori vengono inviati a circuiti
  elettronici che introducono un ritardo predeterminato dai valori dei
  componenti passivi utilizzati (sono chiamate linee di ritardo) in modo che
  alla fine il segnale che arriva prima viene rallentato e sommato agli altri
  che vengono rallentati meno. Questo approccio serve quindi per riallineare i
  segnali. Le linee di ritardo non sono programmabili ma oggetti fisici con
  ritardo fisico. Se volessi cambiare i ritardi a seconda del punto di fuoco
  che voglio ottenere posso ottenere delle linee di ritardo con dei ritardi
  predeterminati e delle matrici di collegamento che, una volta determinato il
  ritardo di ciascun attuatore/sensore, effettueranno le connessioni con quella
  il cui ritardo si avvicina di più, in approssimazione, a quello calcolato. La
  matrice dovrà essere cambiata ogni qualvolta mutino le caratteristiche della
  focalizzazione del fascio. 
- *Digitalizzazione con tempi differenti*. Utilizzando un numero di ADC elevato
  posso campionare il segnale ad ogni sensore e usare lo SOC (start of
  conversion, attivazione del S&H dell'ADC) come sistema per poter correggere
  il ritardo. Quindi il sample and hold di ciascun ADC non parte nello stesso
  istante: introduco dei ritardi che sono uguali a quelli che ho introdotto per
  la generazione del segnale per l'attivazione del convertitore. A questo punto
  riesco a campionare l'onda che arriva al sensore sullo stesso fronte. I
  ritardi fanno in modo che riesca ad avere il valore digitale in uscita al
  campionatore allineato al tempo corretto. Alla fine dovrò effettuare la somma
  dei vari segnali, solo dopo che avrò effettuato il campionamento del sensore
  più ritardato.
  
Una volta ottenuto il fascio bisogna trasformare l'ampiezza degli echi in
luminosità di pixel per produrre l'immagine. Il segnale RF è quello nero
riportato. L'ampiezza del segnale rappresenta la successione di variazioni di
impedenza acustica del tessuto che il fascio di ultrasuono ha attraversato.
Interessa l'ampiezza dell'eco. Maggiore è la differenza della proprietà
meccanica del tessuto e maggiore sarà la luminosità del riflesso. Per estrarre
l'inviluppo del segnale bisogna raddrizzarlo e successivamente effettuare il
filtraggio passa-basso.

#### Time-gain compensation

La compensazione dell'attenuazione che il nostro segnale ha nel momento in cui
attraversa il tessuto. L'eco dipende da quanta parte del segnale che è stata
riflessa. la parte di segnale riflessa dipende dalla differenza di impedenza
acustica all'interfaccia di due tessuti diversi. Questo non è l'unico fattore
che impatta sull'ampiezza del segnale perchè bisogna anche considerare
l'attenuazione dovuta al percorso effettuato dal suono. Quest'attenuazione è di
tipo esponenziale.

Nel caso presentato si considerano alcune interfacce di materiali che hanno la
stessa impedenza acustica. Questo significa che dovremmo vedere delle linee
della stessa altezza perchè la parte di segnale riflessa rispetto a quella
rifratta rimane costante dato che le due impedenze sono uguali. Il segnale che
ritorna ha un'attenuazione esponenziale: quando vado lontano il segnale ha
percorso un tragitto maggiore prima di ritornare al sensore. All'aumentare
della distanza l'attenuazione dovuta alla presenza del tessuto sul segnale
impatterà sulla sua ampiezza (riduzione esponenziale). Il livello di bianco
andrà a ridursi in profondità ma ciò non rappresenta le caratteristiche reali
del tessuto, quindi dò ad un amplificatore un guadagno che cambia in funzione
del tempo con in maniera esponenziale che serve a compensare l'attenuazione. È
detto time gain perchè più tempo l'ultrasuono trascorre viaggiando nel
materiale, maggiore è il percorso compiuto per arrivare alla sonda, maggiore è
l'attenuazione. Applicato il guadagno gli echi vengono equalizzati in maniera
corretta.

[physical_principle]:		Figure/physical_principle.png 		{height=200px}
[echo_shift_reg_init]:		Figure/echo_shift_reg_init.png 		{height=400px}
[echo_shift_reg_shifting]:	Figure/echo_shift_reg_shifting.png 	{height=400px}
[us_characteristics]: 		Figure/us_characteristics.png		{height=400px}
[echo_transducer]:		Figure/echo_transducer.png		{height=200px}
[axial_resolution]:		Figure/axial_resolution.png		{height=150px}
[lateral_resolution]:		Figure/lateral_resolution.png		{height=150px}
[spherical_transducer]:		Figure/spherical_transducer.png		{height=150}
[acoustic_lens]:		Figure/acoustic_lens.png		{height=150}
