# Esercitazione 1 --- Alimentazioni

Il modo in cui si alimentano i dispositivi è molto diverso a seconda del
dispositivo che si vuole alimentare. L'alimentazione è essenziale poichè, senza
di essa, nessun elemento attivo potrà funzionare correttamente. 

È necessaria al microcontrollore, alle periferiche e ad altri dispositivi
(attuatori, carichi, motori, sensori etc.). Ognuno di questi ha bisogno di una
diversa alimentazione (5, 3.3, 1.8V etc).  In generale gli attuatori hanno
bisogno di 12-24V come minimo, che devono essere portati sulla scheda per poter
pilotare ciò di cui il dispositivo ha bisogno.

Se volessimo individuare i blocchi funzionali dei dispositivi avremmo il blocco
di elaborazione con linea di clock che dev'essere alimentato.

Le varie fonti sono la rete elettrica, la batteria, altre fonti interne al
dispositive. Ci sono anche alimentazioni miste che utilizzano sia corrente che
batteria in condizioni critiche.

1. Da rete elettrica: più comune e che è presente sempre salvo eventi in cui
   salta. I dispositivi hanno bisogno di batterie di backup, senza le quali non
   funzionano dispositivi. L'alimentazione è in alternata a 220V RMS. Bisogna
   raddrizzare la corrente per poterla utilizzare per alimentare il
   controllore.
   
## Trasformatore

Una variazione di corrente genera un campo magnetico indotto che poi verrà
trasformato in corrente elettrica dall'altra parte.

Scegliendo opportunamente il numero di spire sull'avvolgimento primario e su
quello secondario potremo avere una certa uscita che vogliamo.

## Raddrizzatore

è ora necessario riportare nel semipiano positivo la tensione negativa. Per
fare ciò è necessario il ponte di diodi.

## Condensatori

La tensione è ancora oscillante all'uscita del raddrizzatore. Questo causerebbe
continue accensioni e spegnimenti del microcontrollore. Per cercare di
migliorare l'oscillazione potremmo utilizzare una capacità, in grado di
comportarsi come un filtro e che riduce le oscillazioni.

Quanto vale la capacità da mettere? Vi è una formula con cui si può capire il
valore delle capacità. Vi è una rule of thumb:

1. basse induttanze da alimentare: 4-10-100 microFarad.
2. alte induttanze (motori ed attuatori): ...

## Regolatori di tensione

Ancora dei ripple sono presenti e vogliamo stabilizzare il livello della
tensione. I regolatori, convertitori o stabilizzatori servono a questo.

- Lineari: la vout è sempre inferiore a quella in ingresso.
- Non lineari: la vout può essere inferiore o superiore a quella in ingresso.

### Lineari

Spiegazione del circuito. Il transistor in modalità mosfet come regolatore di
corrente ha bisogno di dissipare calore.

1. 78xx: usato per tensioni positive. xx stabilisce il valore a cui si
   stabilizza la tensione.
2. 79xx: usato per tensioni negative

Regolano molto bene l'uscita, eliminando i ripple. Hanno un difetto che sarebbe
l'efficienza energetica molto bassa e calore prodotto da dissipare. Se è
necessario alimentare carichi induttivi che richiedono dei picchi di corrente
non è consigliato utilizzare questa soluzione.

### Non lineare

Efficienza più elevata ma più ripple.

#### Regolatori switching

Questi sono utilizzati andando a modulare la tensione in uscita tramite
l'utilizzo di PWM (pulse width modulation). Questa modulazione permette di
portare in uscita una certa tensione.

La prima parte non ha un trasformatore prechè viene poi ridotta dopo la
tensione.

è composto da due parti:

- dc-dc converter:
	- step down
	- step up: permette di avere tensioni in uscita maggiori di quelle in ingresso
	- step up step down: permette sia tensioni più elevate che più basse
	- ...


##### PWM

Modulazione di onda quadra con certa durata, certa frequenza etc. Il transistor che abbiamo ...

Il rapporto tra Ton e Tt è il duty cycle.
