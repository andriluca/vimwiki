# Esercitazione 2 --- Attuatori

## Sistemi di output

### LED (Light Emitting Diode)

Consiste in un dispositivo in grado di produrre radiazioni (luminose luminose
ma non solo), a fronte di una corretta alimentazione. Entrando in conduzione,
il LED può far scorrere correnti molto elevate che potrebbero danneggiare il
componente stesso. Per questo motivo è necessario limitare la corrente che si
sta apportando.

È possibile distinguerli in due categorie, le quali necessiterannp di un
opportuno circuito di condizionamento:

- *a bassa potenza*: quelli tipicamente utilizzati per produrre avvisi
  luminosi.
- *ad alta potenza*: vengono alimentati esternamente al processore
  (l'assorbimento di corrente è elevato).

#### Condizionamento

Per fare ciò si pone il LED in serie ad un resistore che dev'essere
opportunamente dimensionato in base al punto di lavoro.

In particolare, i condizionamenti possibili possono essere i seguenti:

- *a bassa potenza*

  ![Condizionamento di un LED a bassa potenza.][ledbassapotenza]

- *ad alta potenza*

  ![Condizionamento di un LED ad alta potenza.][ledaltapotenza]

### LCD (Liquid Crystal Display)

È necessario impostare il giusto contrasto per fare in modo che sia leggibile
dagli utenti. Possiede:

- *Alimentazione* (pin $V_{DD}$ (+5V) e $V_SS$ (GND)).
- *Bus dati* (dal pin $DB_0$ a $DB_7$)
- *Pin di controllo*:

	- *R/W (read/write)*: imposta lettura/scrittura da/su schermo.
	- *RS (register select)*: seleziona il registro su cui operare
	  (instruction o data register).
	- *E (Enable)*: abilita/disabilita il dispositivo ad operare con le
	  altre linee.

- *Pin di contrasto*: ($V_o$).

#### Condizionamento

![LCD][lcd]

### Display OLED (Organic Light Emitting Diode)

Viene controllato mediante un interfaccia di comunicazione seriale (e.g. SPI).
Per comprendere meglio il funzionamento riferirsi a [*questo
video*](https://www.youtube.com/watch?v=7x1P80X1V3E&t=1467s) e a [*questo
link*](https://www.waveshare.com/0.96inch-oled-b.htm). Possiede:

- *Alimentazione* (pin $V_{DD}$ (+5V) e $V_SS$ (GND)).
- *Pin d'interfaccia SPI*:

	- *CLK*: Linea di clock della comunicazione seriale.
	- *MOSI*: Line di input dati.

- *CS*: Chip select (simile ad enable).
- *DC*: Segnale di comando (LOW: inserimento comando, HIGH: inserimento dato).

Da notare che non ha bisogno del controllo del contrasto.

#### Condizionamento

![Display OLED][oled]

### Attuatori elettromeccanici

Gli attuatori elettromeccanici si suddividono in due famiglie:

- Attuatori ON/OFF:

	- Relay
	- Elettrovalvola

- Attuatori proporzionali

	- Motori DC
	- Motori stepper

#### Relay (aka relè)

Sono degli interruttori pilotati elettricamente. Questi vengono utilizzati
quando:

- Si vuole *isolare* la parte a bassa potenza a monte da quella ad alta
  potenza.
- *Più circuiti vengono controllati dallo stesso segnale*.

L'utilizzo di questi dispositivi **implica un controllo del carico di tipo
ON/OFF** e non in velocità (e.g. PWM).

Questi relay possono essere:

- Elettromeccanici
- A stato solido (con optotransistor a disaccoppiare le due porzioni).

##### Pilotaggio

![Circuito di pilotaggio di relay.][relay]

Il relay comprende una bobina (i.e. un induttore) che dev'essere pilotata
correttamente per non danneggiare parti di circuito. Per risolvere il problema
della variazione istantanea della corrente che scorre attraverso l'induttore
(che non può avvenire bruscamente, altrimenti si genererebbe una tensione
elevata che danneggerebbe il dispositivo), viene posto in antiparallelo un
diodo di ricircolo.

Il transistor serve a far passare/bloccare la corrente nella bobina, quindi
modula l'azionamento del relay stesso.

#### Elettrovalvola

È un interruttore che consente il passaggio di un fluido. L'azionamento
meccanico è mediato da un attuatore comandato elettronicamente (un solenoide).

Il solenoide è composto da una bobina e un nucleo di materiale magnetico che
può muoversi in maniera libera o vincolata da molla.

##### Pilotaggio

![Circuito di pilotaggio di un'elettrovalvola][elettrovalvola]

### Attuatori proporzionali

Sono attuatori con una risposta modulabile.

![Classificazione dei motori.][motori]

I motori sono attuatori che convertono l'energia da elettrica a meccanica.
L'energia meccanica si realizza nella forma di rotazione angolare attorno ad un
asse che può essere utilizzata per azionare dispositivi quali pompe,
compressori, ruote, ecc.

In campo biomedicale vengono utilizzati per lo più motori *DC e Stepper*.

È possibile trasformare movimenti rotazionali in lineari mediante dei motori
stepper (si pensi a motori connessi ad ingranaggi e cremagliere).

#### Motori DC

Composti da due parti fondamentali:

- Statore: la porzione ferma
- Rotore: la porzione che ruota

Ciascuna di queste parti è costituita da bobine o magneti permanenti.

##### Motori brushed (a spazzole)

Il rotore (interno) è costituito da uno o più avvolgimenti o spire. Lo statore
(esterno) invece è costituito da una coppia di magneti permanenti. Quando il
rotore (i.e. le bobine) è percorso da corrente, il campo magnetico indotto si
oppone a quello dello statore, di conseguenza ruota per allinearsi al campo
magnetico dello statore. Quando però il campo magnetico indotto si allinea a
quello generato dai magneti permanenti, la spira si ferma. Per risolvere questo
problema bisogna utilizzare delle spazzole. Queste hanno lo scopo di *invertire
il verso della corrente che scorre nelle bobine del rotore*. Quando il motore
compie mezzo giro, la spazzola si stacca da un polo della spira e si attacca
all'altro. Quest'operazione inverte la corrente che scorre nella bobina. Le
spazzole sono delle componenti di grafite che commutano la direzione della
corrente ogni mezzo giro.

**Vantaggi**: 

- Sono molto efficienti ed economici (circa 7-8 euro).

**Svantaggi**

- Possono verificarsi scintille. Se ho frizione con le spazzole durante il
  movimento quello che succede è che dalla frizione possono avvenire delle
  scintille. Non è proprio desiderato in applicazioni medicali (specialmente in
  presenza di gas infiammabili).
- I residui prodotti dalle spazzole non indicano questo genere di motori per
  applicazioni "clean".
- Molto più soggetti a rottura. Se si rompono le spazzole devo cambiarle.

Conclusioni. Non si può utilizzare questo tipo di motore per macchinari a
stretto contatto con il paziente (e.g. vie aeree) o con gas infiammabili.

**Esempio: Motore eccentrico**

È un motore, di solito brushed, al cui albero è connessa una massa eccentrica
rispetto all'albero. Mettendo in rotazione la massa si creano delle vibrazioni
proprio perchè il baricentro è spostato. Ciò fa vibrare il dispositivo.

**Pilotaggio**

1. *Relay*. In questo modo posso solo accendere/spegnere il motore, senza
   modularne la velocità. Il motore potrà ruotare solo in una direzione.
2. *Transistor*: è possibile modulare la velocità in questo caso. La direzione
   di rotazione è sempre una sola. Il darlington (due bjt in cascata) ha un
   gran guadagno in corrente. Posso modulare in velocità a seconda della
   corrente che scorrerà attraverso il motore, la quale è modulata dalla
   corrente che scorre tra base ed emitter del transistor. 3. 
3. *Ponte H*: è costituito da quattro interruttori. Chiudendoli a coppie è
   possibile fornire correnti in una o nell'altra direzione al motore. In
   questo modo posso controllare la direzione, oltre alla velocità. La velocità
   è modulabile dai segnali forniti in ingresso ai mosfet.

##### PWM

Gli attuatori in cui le parti meccaniche possono assumere valori intermedi a
quello della posizione di inizio e fine corsa necessitano di essere pilotati
con tensioni proporzionali al movimento che si intende generare. Si utilizza la
Pulse Width Modulation (modulazione della durata dell'impulso).

Si genera un'onda quadra (0-5V). Un'onda quadra è identificata da 3 parametri:

- *Ampiezza*: 0-5V
- *Frequenza*: costante
- *Duty Cycle*: variabile in base al valore da codificare.

Per duty cycle s'intende il rapporto tra il tempo in cui l'onda quadra è ON (a
5V) e il periodo totale. Questo rapporto è espresso in percentuale.

$\text{DC} = \dfrac{T_{ON}}{T_{TOT}}$

Perchè si utilizza questa tipologia di modulazione? Se si effettua la
trasformata di Fourier dell'onda quadra così modulata abbiamo un primo picco
spettrale relativo al contributo in continua, poi abbiamo un picco alla
frequenza dell'onda quadra e i successivi ai multipli della frequenza dell'onda
quadra. Il contenuto di interesse è quello in continua, che dipenderà dal duty
cycle che abbiamo utilizzato. Per tenere la continua faccio un passabasso.

Se facessi un passabasso con frequenza di taglio superiore a quella dell'onda
quadra, quello che accadrebbe è che considererei anche contributi inutili
dovuti all'onda quadra.Se mettessi il polo del filtro prima della frequenza del
PWM eliminerei tutti i picchi spettrali e avrei solo la continua. 

Ma quindi devo fare un filtro per la PWM? In realtà no perchè il motore è
costituito da induttore e resistenze, quindi già esso è un filtro passabasso.
Il polo del filtro dipende da caratteristiche elettriche del motore (resistenza
ed induttanza) e caratteristiche meccaniche (attriti ed inerzia). Le
informazioni vengono reperite in un datasheet. Operativamente sarà necessario
reperire queste informazioni dal datasheet, ricavare la frequenza del polo
associato al filtro passabasso del motore e impostare una frequenza dell'onda
quadra maggiore rispetto a quella del filtro.

Con questo meccanismo è possibile modulare la corrente in ingresso ad un BJT,
ad esempio, permettendo quindi una diversa alimentazione di motori.

Ritornando al pilotaggio dei motori DC con ponte H. Se si vuole controllare
direzione e velocità è necessario utilizzare due porte AND. A ciascuna arriverà
il segnale PWM. Ad una il segnale di direzione e all'altra il negato. In questo
modo è possibile scrivere la seguente tabella di verità:

| PWM  | DIR  | Motore                        |
| :--: | :--: | :--:                          |
| OFF  | 0    | Spento                        |
| OFF  | 1    | Spento                        |
| ON   | 0    | 1-4 ON (rotazione oraria)     |
| ON   | 1    | 2-3 ON (rotazione antioraria) |

##### Motori brushless

Questa tipologia di motori non ha le spazzole. Il rotore è un magnete
permanente e lo statore è costituito da 3 avvolgimenti collegati a stella tra
di loro. Questi sono trifase e la commutazione non avviene automaticamente ma
dobbiamo agire sullo statore in modo che il motore giri nella posizione
desiderata. Ogni volta che il motore è quasi arrivato ad allineare il campo
magnetico è necessario disabilitare una bobina e attivarne un'altra. Electronic
Speed Control: vanno a leggere la corrente che faccio passare nelle bobine in
modo da switchare ogni volta la bobina che si vuole accendere. Il *numero di
bobine diviso 3 determina il numero di coppie di poli che un motore possiede*.
Maggior il numero di poli e maggiore il controllo del motore, maggiore la
potenza da fornire al motore.

**Esempio 1: compressore radiale**

Utilizzati per creare flussi di gas (aria + $O_2$). Si utilizzano i brushless a
causa di un'efficienza maggiore e dell'assenza di scintille durante il
funzionamento. Il case fa in modo di generare la pressione all'uscita del tubo,
prendendo l'aria dal foro centrale.

**Esempio 2: Pompa peristaltica**

È una pompa che permette di mantenere il fluido isolato da tutto il circuito
esterno. Se stiamo effettuando una trasfusione non è possibile utilizzare un
compressore radiale per motivi legati alla contaminazione del dispositivo.
Sarebbe uno spreco rendere un dispositivo del genere disposable. Utilizzando
invece una pompa peristaltica ha un tubo che può essere cambiato facilmente.
Esiste un problema di emolisi dovuto al meccanismo con cui questa pompa
funziona. Quindi il movimento del fluido è garantito dalla rotazione di una
struttura costituita da tre puleggie che vengono mosse in una direzione oppure
nell'altra.

**Pilotaggio**

È necessario comprendere in primo luogo come funziona l'effetto Hall.

![Rappresentazione di campi e forze nell'effetto Hall.][hall]

Si consideri una "lastra" di materiale semiconduttore per convenienza con
drogaggio p. Una corrente nota scorre lungo la direzione x. Immergendo la
lastra in un campo magnetico $\vec{B_0}$ orientato lungo z, i portatori di
carica positiva vengono deviati dalla direzione rettilinea a causa della forza
di Lorentz.

$\vec{F_B} = q\vec{v_x} \prod \vec{B_0}$

La forza di Lorentz è proporzionale al campo elettrico diretto trasversalmente
($\vec{E_y}$).

$q\vec{E_y} = q\vec{v_x} \prod \vec{B_0}$
$\vec{E_y} = \vec{v_x} \prod \vec{B_0}$

$\vec{v_x}$ è la velocità di deriva dei portatori di carica in direzione x.
Questa può essere espressa in termini di densità di corrente. In particolare:

$\vec{J_x} = q\cdot p\cdot \vec{v_x} \rightarrow \vec{v_x} = \dfrac{\vec{J_x}}{q\cdot p}$

Sostituendo:

$\vec{E_y} = \dfrac{\vec{I_x}}{q\cdot p \cdot w} \prod \vec{B_0}$

Il modulo del campo elettrico in direzione trasversale è proporzionale alla
differenza di potenziale in direzione trasversale (tensione di Hall):

$E_y = \dfrac{V_H}{w}$

Passando ai moduli quindi:

$\dfrac{V_H}{w} = \dfrac{I_x}{q\cdot p \cdot w \cdot t} B_0 \cdot \sin{\theta}$
$\dfrac{V_H} = \dfrac{I_x}{q\cdot p \cdot t} B_0 \cdot \sin{\theta}$

L'angolo $\theta$ è quello compreso tra la direzione di scorrimento della
corrente nella lastra e la direzione del campo magnetico esterno. Gli altri parametri dell'equazione sono:

- $V_H$: Tensione de Hall (da misurare).
- $I_x$: Corrente che scorre nel sensore di Hall (fissata).
- $p$: Numero di portatori di carica positiva (dipende dal materiale).
- $t$: Spessore (thickness) della lastra che costituisce il sensore.
- $q$: Carica elettrica di un portatore (costante).
- $B_0$: Modulo del campo magnetico (approssimativamente costante).

Utilizzando queste condizioni è possibile, misurando la tensione di Hall, avere
una stima dell'angolo $\theta$ (per piccoli spostamenti angolari) e quindi
conoscere l'orientamento del rotore di un motore brushless.

Alimentando due delle tre fasi in modo sequenziale il campo magnetico indotto
dallo statore (pari alla somma dei due campi magnetici indotti da ognuna delle
fasi), ruota. Il circuito di controllo fa in modo che il campo magnetico
generato dallo statore sia sempre in opposizione a quello del rotore, così che
questo non si fermi. Uno dei modi più semplici per controllare le commutazioni
delle fasi consiste nell'utilizzare una serie di sensori di Hall come sensori
di posizione. In base ai valori registrati da tali sensori è possibile
costruire una look-up table che associa a ciascuno stato le attivazioni
corrispondenti.

È curioso come il circuito di controllo dei motori brushless somigli ad un
ponte H ma con un ramo in più. In questo modo è possibile invertire
complessivamente tre fasi, per questo è detto "inverter trifase".

![Circuito di controllo di motore brushless][brushless]

**Vantaggi**:

- Non ci sono le spazzole
- Commutazione elettronica e non meccanica
- Non c'è caduta di tensione sulle spazzole. Questo aumenta l'efficienza
  elettrica del motore.
- *Meno manutenzione* perchè non ci sono le spazzole, quindi ci sono meno
  elementi da andare a sostituire in caso di guasti.

**Svantaggi**:

- *Costo elevato*. I sensori di Hall costano molto. Una soluzione a questa cosa
  sarebbe sfruttare le bobine che risultano inattive in un determinato momento,
  per registrare il segnale associato alla posizione del campo magnetico. In
  questo caso si potrebbe studiare la corrente indotta dal campo magnetico del
  rotore. A seconda della corrente indotta possiamo comprendere com'è
  direzionato il campo magnetico.
- *Controllo elettronico complesso e costoso*: ci sono gli ESC.

#### Motori stepper

Sono in grado di compiere degli step. Il progettista può impostare quanti step
il motore deve compiere. Ogni colpo di clock fornito fa effettuare uno step in
una direzione. È un motore brushless con un rotore per fermare il motore in
posizione giusta e per non tornare indietro. Le bobine però vengono attivate in
maniera differente rispetto ai brushless visti in precedenza: ciascuna fase è
costituita da due bobine disposte in posizioni opposte nello statore collegate
tra loro. Il verso d'avvolgimento delle due bobine nella stessa fase è opposto,
in modo tale da generare, da sole, un campo magnetico indotto con un certo
verso. L'attivazione delle bobine causa la generazione del campo magnetico
indotto e l'allineamento del campo magnetico permanente del rotore con quello
dello statore (avanzamento di uno step). Ogni step è caratterizzato da
spostamenti discreti e noti. Il motore stepper può quindi essere utilizzato per
controlli fini di posizione. Non è adatto a compiere rotazioni "veloci".

#### Voice coil

Attuatore voice coil. È all'interno delle casse. Il suono è generato dalla
vibrazione di una membrana che vibra. C'è un attuatore voice coil composto da
un magnete permanente e da bobine che si avvolgono attorno al metallo. Andando
a fornire o togliere corrente abbiamo che il corpo dell'attuatore va a muoversi
avanti e indietro. Muovendosi va a provocare delle vibrazioni che propagheranno
nella membrana, che produrrà suono, oppure la si può utilizzare come una
valvola PEEP, usata in ventilazione. La valvola PEEP serve a fine espirazione
per mettere una pressione costante. Andando ad aprire e chiudere una valvola
possiamo mantenere un livello di pressione costante andando a variare la
resistenza.

#### Valvole a ghigliottina

All'interno hanno una bobina e un nucleo magnetico permanente. Quando dò
tensione alla bobina sposto il magnete verso il basso. In fondo ho una
ghigliottina, una parte piatta che va ad occludere il canale. Funzionano
esattamente come le elettrovalvole viste inizialmente con la differenza che in
questo caso posso utilizzarle in modo proporzionale: a seconda della corrente
fornita è possibile determinare un diverso flusso. Il problema di queste
valvole è che, essendo realizzate con un induttore, questo immagazzina energia
e abbiamo un *ciclo d'isteresi*: quando l'induttore viene caricato e quando
viene scaricato si comporta in modo diverso.

#### Valvole proporzionali a spillo

Questa valvola è molto piccola (meno di 2cm di altezza e meno di 1cm di
diametro). All'interno è presente uno spillo che si muove alla stessa frequenza
del PWM che sta pilotando la valvola. Andando a chiudere e ad aprire
velocemente quello che ottengo è una sorta di valvola. La relazione tra il duty
cycle e la portata (flusso) è *lineare*. 

Il problema di questo tipo di valvole è che andando ad aumentare la frequenza
di stimolazione, il duty cycle ammissibile diminuisce, altrimenti si romperebbe
lo spillo. Perchè ci interesserebbe aumentare la frequenza? a seconda delle
proprietà della valvola, del fluido che utilizziamo, e dei circuiti che abbiamo
a monte e a valle, devo scegliere la frequenza PWM adeguata per avere l'effetto
di filtro passabasso che ci serve per la modulazione.

**Pilotaggio (delle ultime tre valvole proporzionali)**

Basta usare il circuito con transistor, diodo di ricircolo e valvola di fianco.

#### Buzzer

Quest'attuatore viene impiegato per effettuare segnalazioni acustiche. Questo
contiene una membrana che si muove con la stessa frequenza di stimolazione. Si
pilota come le tre precedenti e con un segnale PWM.

A cosa serve avere delle segnalazioni acustiche differenziate? I vari
macchinari producono suoni diversi a seconda dell'informazione che devono
produrre (e.g. suoni dell'HR ECG). Di solito si mettono due buzzer, in modo che
almeno uno dei due funzioni sempre, altrimenti si mette una resistenza
sull'emitter dei BJT. Controllando la corrente che scorre in questa resistenza
è possibile verificare il funzionamento del buzzer. Il guasto di quest'ultimo
implichebbe l'apertura del circuito.

### Micropompe piezoelettriche

Il piezoelettrico è un materiale che trasforma deformazione meccanica in
tensione o quantità di carica (e viceversa). Se applico una tensione ai capi il
piezoelettrico si deforma andando a creare una depressione all'interno della
camera. Questo permette d'immettere del flusso al suo interno. Fornendo un
valore opposto di tensione (lavorando quindi in alternata), è possibile
spingere il fluido all'esterno della camera. Ciò è possibile grazie a delle
valvole di non ritorno. La portata non è molto elevata.

## Input

Sistemi con cui gli utenti possono andare a comunicare con il microcontrollore.

### Pulsanti

Se tengo premuto il pulsante realizza il contatto, altrimenti no.

Esistono due diverse configurazioni: pull up e pull down. La resistenza serve
per limitare la corrente che sta scorrendo nel circuito. Il problema dei
pulsanti è il *rimbalzo*. Quando premo ho un transitorio in cui il segnale va
alto e basso. Esistono due soluzioni: antirimbalzo hardware e antirimbalzo
software.

#### Antirimbalzo hardware

Prima soluzione: antirimbalzo hardware. Un filtro passabasso filtra le
oscillazioni iniziali ed elimina le componenti in alta frequenza. Si mette $f =
10Hz$. Si usa un diodo per rendere la scarica più veloce.

#### Antirimbalzo sofware

Prima soluzione (meno consigliata).

```c
while(1){
	if(pulsante premuto){
		while(pulsante premuto);
		...
	}
}
```

Seconda soluzione (meno consigliata).

### Trasduttori

Trasformano una forma d'energia in energia elettrica.

#### Micro switch

Quando il dispositivo tocca terra creo un cortocircuito. Un vero e propro
pulsante. Questo dispositivo viene usato per alimentare, ad esempio, un
carrello con dei motori (si pensi alla stampante 3D). Un motore spinge la
testina della stampante prima contro un bordo e poi contro l'altro. In questo
modo chiude dei microswitch in sequenza e va a calcolare in tempo che ci mette
per andare dall'uno all'altro.

#### Potenziometro

È una resistenza variabile. Il cursore varia la partizione delle due resistenze
andando a variare la tensione in uscita ad esso.

#### Distanziometro

Alternativa al potenziometro che permette di andare a vedere la posizione.
Esistono due principi di funzionamento per il distanziometro: laser (basato
sulla luce) e acustico (basato sulle onde sonore). Si va a generare un'onda
sonora o laser, che attraversa una lente di trasmissione. Misurando lo
sfasamento tra le due onde (quella in uscita all'emettitore e quella ricevuta
in ingresso) è possibile capire qual è la distanza a cui si trova l'oggetto su
cui la prima ha avuto riflessione. Andando a misurare lo sfasamento temporale
tra le due onde e sapendo che la luce (o il suono) viaggia ad una certa
velocità posso ricavare la distanza dell'oggetto molto semplicemente.

#### Sensori di pressione

Costituiti da due camere: sopra e sotto. La differenza di pressione crea un
piegamento della membrana. Misurando il piegamento (con un piezoelettrico, una
piezoresistenza, strain gauge) misuro indirettamente la differenza della
pressione. Un esempio mediante un sistema piezoresistivo.

#### Encoder

*Encoder incrementale*: strumento che serve a misurare la velocità. È difficile
ricavare la velocità dall'accelerazione a causa della costante d'integrazione
che genera un drift. Saldando un disco con dei fori all'albero del motore
rotativo, ponendo un LED da un lato e un fotodiodo dall'altro, è possibile
contare quanti spike si ottengono in un certo istante temporale. Conoscendo
quest'informazione e quella relativa al numero di fori presenti è semplice
ricavare la velocità.

*Encoder assoluto*: utilizzato per conoscere la posizione del motore. Il motore
elettrico, prima dell'accensione della macchina a funzionalità programmata,
rimane nell'ultima posizione assunta. Il microcontrollore non ha salvato
quest'informazione. Ci sono due alternative quindi: 

- Utilizzare il metodo che utilizza i microswitch e il tempo che intercorre tra
  le due pressioni.
- Encoder assoluto.

Si utilizza una serie di griglie per capire dove si trova il motore.
Nell'esempio 8 livelli. Ciascuna griglia è associata ad una coppia
LED/fotodiodo. Il pattern assunto da ciascuna griglia è indipendente.
Combinando gli 8 stati dei fotodiodi è possibile ricavare in quale posizione si
trovi il motore.

#### Sorgenti di clock

Vi sono varie alternative per l'implementazione. Quella più usata (nei temi
d'esame) è quella che si basa su cristalli di quarzo (che sfrutta quindi
l'effetto piezoelettrico). Se alimentato, comincia a vibrare producendo un
segnale elettrico avente la stessa frequenza rispetto a quella di vibrazione.
Questo permette di avere un segnale costante con una frequenza definita che
possiamo utilizzare come clock. Questo segnale è di tipo analogico, quindi per
poterne ricavare un'onda quadra sarà necessario sfruttare dei trigger di
Schmitt con delle opportune soglie. I quarzi hanno un costo crescente con la
frequenza e la precisione d'oscillazione. Per risparmiare (se non sono
richieste particolari performances) è possibile utilizzare un circuito RC
connesso ad un trigger di Schmitt. La frequenza di clock non è esattamente
quella che ci si aspetta a causa delle tolleranze dei componenti e dei rumori
dell'op amp.

## Leggi di controllo

È necessario che, nei sistemi embedded, i dispositivi di input e quelli di
output interagiscano tra loro. Per far ciò si utilizzano delle leggi di
controllo.

### In anello aperto (aka open loop)

Forniamo una tensione all'attuatore e questo imporrà una certa azione (ad
esempio genererà una rotazione ad una certa velocità).

Utilizzata principalmente con i motori stepper. Il problema è che se variano le
condizioni iniziali potrebbe virtualmente cambiare l'uscita.

### In anello chiuso (aka closed loop)

Abbiamo un regolatore, un attuatore (compie movimento) e un trasduttore (misura
la velocità). Vedendo l'errore che c'è tra il target fornito e la velocità
reale il regolatore fornirà una tensione diversa all'attuatore in modo da
raggiungere il target specificato.

Si usa un motore brushless con un sensore. Andando a vedere l'errore tra il
target e la variabile in uscita si andrà a regolare la variabile controllata.

#### Esempio: servo motore

Un esempio di controllo in anello chiuso. Dal punto di vista del
microcontrollore si fornisce un segnale PWM che definisce l'angolo da assumere.
Esiste una relazione tra duty cycle su PWM e posizione angolare. Questo
componente ha un motore DC, dei meccanismi per ridurre la velocità di rotazione
e un potenziometro che dice in che posizione angolare si trova. Il
potenziometro va a variare il bilancio del partitore e a seconda della
posizione angolare in cui si trova il servo ho un certo valore di tensione in
input al motore.

### Regolatore PID

Si sommano tre contributi per regolare cose diverse.

- *Proporzionale ($K_p$)*. Tiene conto dell'errore calcolato in quell'istante.
- *Integrale ($K_i$)*. Permette di accumulare tutti gli errori e andare a
  raggiungere il valore corretto. Tiene conto della storia dell'errore
  dall'istante iniziale a quello attuale.
- *Derivativo ($K_d$)*. Quello che permette di andare ad attenuare le
  oscillazioni.

Ad ogni errore è associata una costante. Queste devono essere dimensionate
correttamente.

Come si stabiliscono le costanti di proporzionalità dei tre contributi? Vi sono
dei metodi matematici (e.g. Ziegler-Nichols) ma la verità è che per sistemi
complessi questi metodi non funzionano come dovrebbero. Operativamente quindi
si procede per trial-and-error (i.e. empiricamente).


[ledbassapotenza]: 	Figure/ledbassapotenza.png
[ledaltapotenza]: 	Figure/ledaltapotenza.png
[lcd]: 			Figure/lcd.png
[oled]: 		Figure/oled.png
[relay]:		Figure/relay.png
[elettrovalvola]:	Figure/elettrovalvola.png
[motori]:		Figure/motori.png
[hall]:			Figure/hall.png
[brushless]:		Figure/brushless.png
