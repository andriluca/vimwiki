# Embedded C

Let's start from OOP (Object-oriented programming), to go to event-driven
programming.

## Object oriented programming (OOP)

There's the misconseption that OOP can only be performed through the use of OOP programming language. Actually OOP is not a use of a specific language but a way to design the software based on three concepts

1. *Encapsulation*: information hiding and abstraction.
2. *Inheritance*
3. *Polymorphism*

### Encapsulation

After having written an header file with a typedef Shape in it,
it is possible to use this principle in order to produce
functions that are related to this kind of object.

This principle says that we disallow the access of the members
of the Shape struct directly and instead we provide a set of
functions specialized to work with the shape structure. An
esample of this is the way to initialize the Shape object, a
constructor.

```c
void Shape_ctor(Shape * const me, int16_t x0, int16_t y0);
```

The pointer is conventionally called me. This pointer should
always point to the same instance of the object we use the
const keyword. This keyword is used after the \* symbol because
the me pointer can't change but the shape instance pointed by
the pointer can (this is the main purpose of the constructor).

Let's write the ".c" file associated to this.

```c
void Shape_ctor(Shape * const me, int16_t x0, int16_t y0){
	me->x = x0;
	me->y = y0;
}
```

In this way we have created a class in c. The different fields
of the Shape type are *the attributes* while the function related
to this are *the methods (or operations)*.

It is possible to represent the class in a UML class diagram,
through which it's possible to create relationships among other
classes.

A class realizes the concept of encapsulation, because it
presents to the outside only the outer shell in form of the
operations, while the internal data and internal implementation
remain encapsulated inside the shell.

But, going back to the code, another important convention of
OOP is that the objects get initialized before any other
operations can be performed on them.

## Inheritance

Let's assume we want to create particular shapes like cyrcles,
squares, triangles. In order to do so we have to create,
according to OOP logic, different classes with specific
attributes and methods. If we consider them individually we
might find many of them that are shared though. We can use reuse the attributes and methods of the Shape class also in the specific shapes.

This property is called inheritance. It's the ability to define
new classes based on existing classes in order to reuse and
code organization.

How can we practically do that? Let's consider the rectangle example. Starting from the "rectangle.h" file:

```c
#ifndef RECTANGLE_H
#define RECTANGLE_H

#include "shape.h"

typedef struct {
	Shape super; // Inherited Shape

	// Rectangle specific attributes
	uint16_t width;
	uint16_t height;
} Rectangle

void Rectangle_ctor(Rectangle * const me,
			int16_t x0, int16_t y0,
			uint16_t w0, uint16_t h0);
void Rectangle_draw(Rectangle const * const me);
uint32_t Rectangle_area(Rectangle const * const me);

#endif
```

Once done that we can write the "rectangle.c" file.

```c
#include "rectangle.h"

void Rectangle_ctor(Rectangle * const me,
			int16_t x0, int16_t y0,
			uint16_t w0, uint16_t h0)
{
	Shape_ctor(&me->super, x0, y0);
	
	me->width = w0;
	me->width = h0;

}


void Rectangle_draw(Rectangle const * const me){
	// some pseudocode.
}

uint32_t Rectangle_area(Rectangle const * const me){
	return (uint32_t)me->width * (uint32_t)me->height;
}


```

In the constructor we have, initially instanciated the
inherited object, then we have instanciated the rectangle
related attributes.

It's possible to *upcast* rectangles in order to perform
operations to the objects that are inside that class. This will
modify the instance of the rectangle safely. The word upcast is
related to the way class diagram is built. In fact the arrow of
the generalization (another way to refer to inheritance) is
pointing to the class which is more generic. (in our case
Rectangle -> Shape).

A good way to think about the concept of generalization is to
refer to the biological classification. If we consider an house
cat, this is a felis and a mammal, so it inherits the methods
and attributes belonging to those, more generic, classes.

Let's discuss about the differences and similarities between
class inheritance and class composition. Inheritance can be
implemented by embedding an instance of the superclass inside
the subclass. This means that the subclass is a composition of
the superclass and some attributes and methods that are
specific to that subclass. This points to similarities between
inheritance and composition. In fact both approaches can
accomplish similar goals, but there's an important difference.
Inheritance is the "is a"-relationship which comes when we
embed the superclass as the *very first* attribute so that
every instance of the subclass can be treated as an instance of
the superclass.

By moving the superclass in a different position with respect
to the very first one, we still have composition (which is a
"has a"-relationship) but we can't perform upcasting like a
normal inheritance. This would break encapsulation because we
would have to access to the element inside the class to modify
the superclass. 

The superclass is also known as base class.

## Polymorphism

Is a uniquely OOP that has not a direct analogue in a procedural language like
C. We will learn how to emulate it in C. It's the ability to provide different
methods for the same inherited operation in the subclasses of a given class.

The methods "draw()" and "area()" are common to all the shapes and can be
generalized within the Shape class. The problem is that we can't define a
generic methods for these because we don't know anything about the shapes that
we are about to compute them for. OOP is all about separating the interface
between "what can be done" and "how can be done" (aka implementation). The
shape class could at least provide an interface about the draw() and area()
operations and worry about the implementation later. So the idea is to provide
some dummy code in the superclass "shape.h" file.

It is possible to upcast safely (in c++), so we can instanciate
this without any problem.

```c
// This in c++
Shape *ps = &r1;

ps->draw();
a = ps->area();
```

By performing operations in this way we are considering the pointer of the
declared type, so the methods are related to its class. In order to reference
to the subclasses and their methods it's necessary to use the keyword "virtual"
in the definition of the operations both in the superclass and the subclasses.
In this way, polymorphism is implemented.


