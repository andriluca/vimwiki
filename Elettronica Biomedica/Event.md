# Event-driven programming

We shall not mix sequential programming with the event-driven programming.

To be called event-driven a program must possess most of these characteristics.
Perhaps the property that sets an event-driven program most apart from the
sequential one is the *no blocking* inside the application level code.


