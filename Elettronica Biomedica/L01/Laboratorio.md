# Laboratorio 1

## Datasheet

Documento tecnico che contiene all'interno la descrizione dal punto di vista
tecnico e come operare per configurare correttamente il dispositivo.

Si possono trovare datasheet di diversi dispositivi elettronici.

Contiene anche le footprint dei microcontrollori: immagini che indicano la
piedinatura. Nel datasheet viene riportata la rappresentazione logica del
processore.

## Porte I/O

Servono per far comunicare il controllore con il mondo esterno.

Viene rappresentata la porta dell'ATMega328P. I registri che ci interessano
sono PORT (scrive in uscita), DDR (configura il pin come input o output) e PIN
(legge i pin). Scrivendo 1 su DDR: attivo il tristate e posso controllare
tramite il registro PORT la tensione sull'uscita. Quando DDR è LOW posso usare
solo il registro PIN per andare a leggere quello che è presente sul pin fisico.

## Struttura dei registri

## Scrittura dei registri

Si utilizzano opportune mascherature già viste a lezione.  All'interno di MPLAB
sono state create delle define che ci aiutano a impostare i registri. Si
sfruttano i nomi dei pin definiti dalle librerie per costruire la maschera
opportuna.

È interessante andare a creare delle maschere che impostano più bit. Dare
un'occhiata alla slide.

## MPLAB IDE

Ambiente di sviluppo complesso che permette di creare progetti che contengono
file .c e .h e di caricarli sul dispositivo. La struttura base è quella che
considera il text editor, una parte di gestione dei file sorgente, una parte
che descrive il controllore che stiamo utilizzando, una parte di output che
informa sugli errori di compilazione.

## Esercitazione

### Esercizio 1

Si chiede di accendere semplicemente il LED built in e di farlo rimanere acceso.

#### Soluzione 

Nella main è presente solo la funzione di inizializzazione che viene qui
riportata.

```c
void init(){
    
    // Inizializzazione del registro DDR.
    DDRB    |= (1 << DDB5);
    // Accensione del LED built-in di PORTB.
    PORTB   |= (1 << PORTB5);
    
}
```

### Esercizio 2

Si vuole far lampeggiare il LED built-in di PORTB. Si utilizzano due cicli for
annidati per creare un delay tra le commutazioni.

### Soluzione

```c

void init(){
    
    DDRB    |=  (1 << DDB5);
    PORTB   |=  (1 << PORTB5);
    
}

void loop(){
    
    while (1) {
        
        PORTB ^= (1<<PORTB5);
        for(uint8_t j = 0; j < 10; j++)
		for(uint16_t i = 0; i < 50000; i++);
        
    }
    
}

```

### Esercizio 3

Si vuole far lampeggiare in controfase due LED: uno built-in e l'altro esterno.

#### Soluzione

```c

void init(){
    
    DDRB    |=  (1 << DDB5) | (1 << DDB0);
    PORTB   |=  (1 << PORTB5);
    PORTB   &= ~(1 << PORTB0);
    
}

void loop(){
    
    while (1) {
        
	// Commuto le due uscite insieme.
        PORTB ^= (1<<PORTB5) | (1<<PORTB0);
        for(uint8_t j = 0; j < 10; j++) for(uint16_t i = 0; i < 50000; i++);
        
    }
    
}

```

### Esercizio 4

Si vuole accendere un LED alla pressione di un pulsante collegato ad una
resistenza di pull-up.

#### Soluzione

Sono riportate alcune define utili.

```c

#define DDRLED       (1 << DDB5)
#define DDRBUTTON    (1 << DDD7)
#define LED          (1 << PORTB5)
#define BUTTON       (1 << PIND7)

```

Funzioni di inizializzazione e ciclo infinito:

```c

void init(){
    
    // Imposto gli input.
    DDRD &= ~DDRBUTTON;
    // Imposto gli output.
    DDRB |=  DDRLED;
    
}

void loop(){
    
    while (1) {
        
        // Controllo quando l'interruttore impone LOW su PIND7.
        if(!(PIND & BUTTON))
            // Accendo il LED.
            PORTB |=  LED;
        else
            // Spengo il LED.
            PORTB &= ~LED;
    }
    
}

```
