# Laboratorio 2 --- Interrupt

La frequenza di clock del sistema è di 16MHz (Arduino Mega) che può subire un
prescaling. Questo prescaler ha una serie di bit che sono CS1[0:2] facenti parti di TCCR1B. In
uscita al prescaler avremo iuna frequenza del teimer che sarà pari alla
frequenza del sistema diviso il fattore di prescaling. 

La frequenza entra nella control logic del timer che si occupa di dire se il
conteggio avviene verso up o down e l'output del contatore viene salvato in
TCNT1, registro in 16 bit (0x0000 a 0xffff) che possono essere letti dal data
bus.

Il valore del contatore 0x0000 è in and con 0x0000 per dare il comando di
BOTTOM nella control logic e 0xffff attiva il comando di TOP. Quando si attiva
una delle due si attiva il TOV (Timer overflow). TOV è in AND con TOIE1 (Timer
overflow enable 1), bit contenuto nel TIMSK1 e con il pin che abilita le
interrupt globalmente attivato con sei(). Quando abbiamo l'overflow,
attivazione dell'interrupt sull'overflow e l'attivazione globale
dell'interrupt, si attiva l'ISR associata all'overflow del timer. Disegnare lo
schema (guarda registrazione).

Il tempo che ci si mette per arrivare da 0x0000 a 0xffff o viceversa dipende
dalla frequneza che abbiamo impostato per il timer.

$T_{conteggio} = \frac{1}{f_{timer}} = \frac{1}{\frac{F_{sys}}{presc}}$

$T_{0x0000 -> 0xffff} = \dfrac{1}{F_{timer}} \cdot \#_{conteggi} \cdot I -> sei()$

$= \frac{1}{F_Timer} * 65535 = ...$


$T_ISR = 1/F_Timer * #conteggi$

$F_ISR = F_TIMER/#conteggi\_max$


$F_ISR = 1/5

T_ISR = 65535 * presc / 16M$
