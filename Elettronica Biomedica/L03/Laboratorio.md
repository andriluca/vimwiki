# Laboratorio 3 --- ADC

## Ripasso di come funziona un ADC (SAR)

Nell'ATMega vi è un ADC SAR.

Visto da fuori ho bisogno di una V_in che va ai capi del S&H, il chip select
che va alto ogni volta che voglio fare una traduzione. La v_ref che determina
il full scale range e il clock: ad ogni colpo di clock avrò un bit in uscita al
SAR.

Il registro in uscita è a 10 bit nel mega. Si pone un bit alla volta dal più
significativo al meno significativo. Il nostro dac poi converte il valore in
analogico. Il comparatore compara vref/2 con la tensione in ingresso: se la
tensione in ingresso è maggiore della tensione del dac il bit più significativo
rimane a 1, altrimenti viene impostato a zero. Si procede in questo modo per
tutti i bit in uscita all'ADC. Ci servono, nel caso di un SAR a 10 bit 10 colpi
di clock per avere la conversione di una tensione analogica.  Quando il sar ha
finito la conversione alza la linea di EOC (i.e. end of conversion).

Analizziamo il diagramma temporale. Il tempo di sample dev'essere maggiore del
tempo in cui il chip select rimane high. T sample dev'essere maggiore della T
del clock perchè ci servono 10 colpi di clock per campionare effettivamente il
nostro dato.

## ADC del ATMega

è un sar a 10 bit. Inoltre abbiamo un mux a 8 canali (quelli della porta C).
Dicevamo che i segnali analogici vengono messi in portC perchè proprio lì
stanno gli ingressi del multiplexer.

Per quanto riguarda ???

La conversione pul essere triggerata automaticamente (ad esempio ad ogni
overflow del timer).

### Registri in gioco

un registro (ADMUX) serve per impostare la tensione di reference. Una buona
norma è quella di mettere un condensatore su AREF del mega per eliminare
disturbi.

Se REFS0 è a 0 ho una configurazione, altrimenti ne ho una seconda.

Questo registro serve anche per impostare il canale da andare a campionare.
Questo dipende naturalmente anche da dove vogliamo collocare la tensione
analogica da campionare. ADLAR: la giustificazione a destra o a sinistra. di
default è a zero (nel low ho gli 8 bit meno significativi)???

ADCSRA: Si occupa del prescaler. LA frequenza del clock dell'ADC è quella di
CPU (16M). PEr avere la risoluzione massima dell'ADC la frequenza dev'essere
compresa tra 50 e 200 kHz. Questo si ottiene andando a settare le linee
adeguate di questo registro. Se non si hanno dei prerequisiti elevati sulla
risoluzione possiamo farlo andare più velocemente, altrimenti lo si fa andare
lentamente ma con conversione più precisa. L'ultimo piedino da settare è
l'ADEN, che abilita la conversione. L'evento hardware interno ripulisce il bit
di ADEN, riponendolo a zero.

Da quando mando alto il bit di start conversion sevono 13.5 colpi di clock per
settare i due registri citati e poi 25 colpi di clock per la conversione. Nelle
fasi successive invece si velocizza di molto il tutto.

C'è l'opzione di avere l'autotriggering: l'ADATE. è necessario però anche
settare la source dell'evento che triggera la conversione. Ad esempio questa
potrebbe essere settata sull'overflow del timer.

### Come configurare l'ADC via codice

1. Seleziono la giusta reference
2. Selezionare il canale da cui acquisire il segnale
3. Scegliere la giustificazione (left o right). Lasciamo right normalmente.
4. Decidere la frequenza di conversione
5. Attivare l'ADC (mettendo alto ADEN).
6. Faccio partire la conversione mettendo a 1 lo start of conversion e aspetto
   che quel bit diventi low.
7. Leggere il dato, andandolo a comporre.
8. Se ci interessa poi possiamo convertire il valore in digit in un valore in
   volt.
