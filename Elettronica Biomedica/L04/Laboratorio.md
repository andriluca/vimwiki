# Laboratorio 4 --- PWM

Andando a variare il duty cicle si va a variare il valore medio dell'onda e si
modula diversamente l'attuatore. Abbiamo diversi picchi spettrali a frequenze
multiple. Per usare solo il valor medio dobbiamo buttare via tutti gli altri
picchi. Si utilizza la serie LR per ottenere un filtro passabasso. In questo
modo, filtrando, otterremo solo la componente continua del segnale.

La frequenza della PWM dev'essere molto maggiore rispetto alla frequenza di
taglio del filtro.

Volendo generare un'onda quadra con PWM su un pin di un controllore, si
avrebbero principalmente due opzioni:

- *Via hardware*: esiste una periferica dedicata alla generazione del segnale
  PWM. il valore del pin viene controllato con delle tempistiche definite da
  utente. Si utilizzano due registri: quello di periodo e quello di duty cycle.
  Si decide quindi il periodo che si vuole dare e il duty cycle. Un flip-flop
  set reset possiede due ingressi (i.e. set S e reset R) e un'uscita (q).
  L'uscita viene mantenuta (quando sia S che R sono in stato logico LOW) finchè
  uno dei due ingressi non cambia. Il raggiungimento da parte di S dello stato
  logico HIGH, se R è mantenuto a LOW, comporta per l'uscita Q l'impostazione
  dello stato logico HIGH. Quando R è impostata HIGH e S è LOW, l'uscita è
  portata a LOW. L'ingresso set è collegato all'uscita del comparatore che
  confronta il registro periodo con il registro timer. Nel registro periodo
  posso ad esempio mettere 100 conteggi. quando il registro timer raggiunge 100
  il comparatore avrà uscita alta. Il timer viene poi resettato. Nell'ATMega
  non esiste una periferica per produrre il PWM, quindi si risolve il problema
  intervenendo con l'altra modalità.
- *Via software*: Si sfrutta l'interrupt per commutare lo stato logico del pin.
  La risoluzione sul duty cycle è il numero di possibili valori di duty cycle
  che è possibile scegliere. L'interrupt, nel caso qui analizzato, si verifica
  10 volte al secondo, con una risoluzione del 10%. Si sceglie una risoluzione
  più o meno elevata a seconda dell'applicazione che si vuole realizzare. Una
  variabile contatore poi verrà incrementata (nell'ISR associata ad overflow
  del timer) fino al raggiungimento del numero complessivo di eventi nel
  periodo del PWM. Quando il contatore raggiunge il valore di soglia, si
  commuta lo stato logico del pin che si sta controllando in PWM.
  
## Regolatore

Un attuatore può essere regolato in anello aperto o chiuso.  Se si considera
l'anello chiuso, entra in gioco un regolatore che ha in input la variabile
misurata e quella desiderata (l'errore) che varia il comportamento
dell'attuatore. 

### Tipologie di regolatori

Il caso più semplice è quello di ON/OFF. Si consideri ad esempio il regolatore
del frigo: finchè la temperatura misurata è al di sotto della soglia il
compressore rimane spento, altrimenti viene acceso al massimo della potenza
finchè la variabile misurata non torna al di sopra della soglia in temperatura.

Un altro tipo è il regolatore proporizonale: una costante di proporzionalità
lega l'errore (tra la variabile misurata e quella desiderata) con l'uscita (in
termini di variabile controllata). Si consideri ad esempio un compressore
radiale. La variabile misurata è la pressione del gas mentre quella controllata
è la corrente passante nel compressore.

Un regolatore integrale: l'uscita è proporzionale all'integrale dell'errore.
Questo tiene conto della storia dell'errore e fornisce stabilità all'attuatore
contro variazioni istantanee dell'errore causate da un certo rumore.

Regolatore derivativo: l'uscita è proporzionale alla derivata dell'errore.
Segue bene le variazioni dell'errore. Il controllore agisce (la corrente
aumenta quando la derivata dell'errore diventa alta).

Il regolatore PID è la composizione dei tre sistemi già citati: un fattore è
proporzionale all'errore (P), uno al suo integrale (I) e uno alla sua derivata
(D). I coefficienti vengono assegnati solitamente in modo empirico
(trial-and-error) e dipendono intrinsecamente dall'applicazione.
