# State machines

This subject is closely related to event-driven programming and active objects.

## State

It's not necessary to remember the whole history of past events but just some
aspects of it. It's just necessary to remember the relevant history, which is
only what influences the future behavior of the system and events don't
contribute equally to that relevant history. For example an electronic
controller for computer keyboard generates either uppercase or lowercase
letters keycodes. This behavior depends wether the shift key has been pressed
or released but it does not matter which other keys have been pressed or
released or how many of them were pressed. Therefore the whole relevant history
of the keyboard can be reduced to just two classes: "normal" class, related to
lowercase letters and the "shifted" class, related to uppercase letters. This
leads to the most important concept of *state*.

State of a system is an equivalence class of past histories of a system, all of
which are equivalent in the sense that the future behavior of the system given
any of these past histories will be identical. The concept of state is the most
efficient representation of the relevant history. It is the minimal information
that captures only the relevant aspects for the future behaviors and abstracts
away all irrelevant aspects. 

The concept of state is linked to the concept of state machines. A state
machine is a set of all the states (equivalence class of past histories of a
system) plus the rules for changing from one state to another (aka state
transitions). These rules capture the fact that some event does contribute to
the relevant history while others do not. Those events influencing the future
behavior trigger the state transitions.

### State diagrams

A very nice aspect of state machine concept is that it's compelling graphical
representation in form of state diagrams. In the modern unified modeling
language (UML) notation, states are represented as rounded-corner rectangles
with the name of the state in the name compartment. And regular state
transitions are represented as arrows labeled by the triggering events. In the
UML we can also have the so-called "internal transitions", which do not cause
change of state but only execute actions in response to a given event. These
are represented just as triggering events inside states. The actions are listed
after the slash character and are allowed on both kinds of transitions
(internal and regular). Additionally, every state machines need to have an
initial transition that points to the default state, which is active when the
machine is created and before any events are dispatched to it.

A state machine can have many more feature and there are many kinds of state
machines used in software, including the modern hierarchical state machines
(aka UML statecharts).

One universal feature in all the architectures state machines is the
run-to-completion (RTC) event-processing: a state machine can process only one
event at a time. This means that the processing of one event must necessarily
finish before the processing of the next ones.

The RTC semantics universally assumed in all state machine formalisms matches
exactly the RTC semantics assumed in all event-driven systems, such as active
objects. In other words, state machines are the ideal mechanism to specify the
behavior of active objects.

## Required properties

- *Traceability*: the ability to find correspondence between the design of the
  diagram and the implemented code.
- *Extensibility*: if we want to add new state or new events to the code it can
  be easily performed.

## Implementation

It's required a state variable that include all the possible states. This can
be implemented by using an enum.

## Guard conditions


## Input-driven state machines

To appreciate their challenge let's consider digital circuits without internal
states (combinational circuits). These circuits are made out of digital ports
such as AND, OR gates and so on.
