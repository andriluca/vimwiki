# Svolgimento tema d'esame

## Curve di calibrazione

Sono delle curve che associano a dei valori in digit (livelli) un valore di una
determinata variabile (una tensione). Inoltre altre curve associano la tensione
ad una variabile fisica di interesse, ad esempio la corrente che scorre in un
circuito oppure una pressione. Tramite queste è possibile poi controllare tali
variabili fisiche.

## Codice

```c

// Includiamo gli header che servono.
#include <avr/io.h>
#include <avr/interrupt.h>

// Definiamo le porte.
// OUTPUT
#define		S1		1 << PORTB0
#define		E1		1 << PORTB1
#define		S2		1 << PORTB2
#define		E2		1 << PORTB3
#define		SUPPLY_02	1 << PORTB4
#define		PWM_MOT		1 << PORTB5
#define		LED		1 << PORTB6

// INPUT
#define		O2		1 << PORTC0
#define		START		1 << PORTC1

// Definiamo i canali dell'ADC.
#define		PS	0
#define		PRES	1
#define		PPAZ	2
#define		IMOT	3
// ...

// Costanti che consentono lo switching
#define		OFF	0
#define		ON	1

#define 	SENS_PMAX	//...
#define 	PRESS_SENS	//...
#define 	RES_PR		//...
#define 	RES_PMAX	//...
#define 	PR_TARGET	//...

#define 	MOT_NOM_CURR	//...
#define 	R_MOT		10

// Prototipi delle funzioni che realizziamo noi.
void InitPort(void);
void setup(void);
void loop(void);

// Prototipi delle funzioni che sono date dal Prof.
void InitPWM(uint freq);
void SetPWM(uint dc);

// Funzioni del micro (da non riportare).
void SetInterruptTimer(int);

void InitPort(void){

	// Impostiamo tutta PORTB come output.
	DDRB = 0xFF;
	// Impostiamo PORTC come input nei bit che ci servono.
	DDRC &= ~((1 << DDC0) | (1 << DDC1));
	// Inizializziamo il registro delle porte.
	// Si parte con le S chiuse...
	PORTB &= ~(S1 | S2);
	// ... e le E aperte per evitare problemi.
	PORTB |=  (E1 | E2);
	
	// Partiamo dalla condizione safe di motore spento.
	PORTB &= ~(SUPPLY_O2 | PWM_MOT | LED);
	
}

void setup(){

	InitPort();
	InitADC();
	// Decidiamo 100Hz perché è una buona frequenza di campionamento.
	SetInterruptTimer(100);
	InitPWM(10000);
	
}

int stato, erogazione, setaccio, compressore;

void loop(void){
	
	while(1){
		
		// Sistema OFF
		if(stato == OFF){
			
			// Aspetto la pressione del pulsante
			if(PINC & START){
			
				// Attendo il rilascio del pulsante.
				while(PINC & START);
				stato = ON;
				// Decidiamo di partire con setaccio 1.
				PORTB &= ~E1;
				PORTB |=  S1;
				// Inizializziamo la variabile che dice al processore quale setaccio è in azione.
				setaccio = 1;
				// Accendo il compressore.
				duty_cycle = 50;
				compressore = ON;
				
				// Leggo il sensore d'ossigeno che fornisce in uscita l'onda quadra.
				InitCounter();
				
				// Andiamo a spegnere il LED che da un errore precedente poteva essere acceso.
				PORTB &= ~LED;
				
			}
			
		} 
		// Sistema ON
		else {
			
			// Macchina a stati dei setacci
			
			if(setaccio == 1){
			
				// Aspetto che la pressione interna al setaccio raggiunga quella target.
				if(ps > PR_TARGET){
				
					// Cambio con setaccio 2.
					PORTB |=  (S2 | E1);
					PORTB &= ~(S1 | E2);
					setaccio = 2;
					
				}
			
			} else if(setaccio == 2){
			
				// Aspetto che la pressione interna al setaccio raggiunga quella target.
				if(ps > PR_TARGET){
				
					// Cambio con setaccio 1.
					PORTB |=  (S1 | E2);
					PORTB &= ~(S2 | E1);
					setaccio = 1;
				
				}
			
			}
			
			// Macchina a stati erogazione bolo.
			
			if(erogazione == OFF){
				
				// Controllo se è avvenuto uno zero crossing.
				if(ppaz_old > 0 && ppaz < 0){
					
					// Apro la valvola
					PORTB |= SUPPLY_O2;
					erogazione = ON;
				}
				
			} else {
				
				// Controllo il minimo
				if(ppaz_old2 > ppaz_old && ppaz_old < ppaz){
					
					// Chiudo la valvola di supply
					PORTB &= ~SUPPLY_O2;
					erogazione = OFF;
				}
				
			}
			
			// Macchina a stati del compressore
			if(compressore == ON){
				
				// Controllo che la corrente non sia minore di 0.4
				if(current < 0.4 * MOT_NOM_CURR){
					
					// Aumento il duty cycle
					dutycycle++;
					
				}
				
				// Controllo della pressione
				if(pres < RES_PR){
					
					dutycycle++;
					
				} else if (pres < RES_PMAX){
				
					dutycycle--;
				
				} else if (pres >= RES_PMAX){
				
					dutycycle = 0;
					compressore = OFF;
				
				}
				
			} else {
			
				// Monitorare la pressione del reservoir
				if(press < RES_PR){
				
					// Valore standard per l'accensione del compressore.
					dutycycle = 50;
					compressore = ON;
				
				}
				
			}
			
			setPWM(dutycycle);
			
		}
		
	}

}

float ps;
float ppaz, ppaz_old, ppaz_old2;
float current;

// Definiamo curve di calibrazione sotto forma di macro.
// Pressione setaccio.
#define READ_P_S	(float)readADC(P_S) * SENS_PMAX / 1023
// Pressione paziente.
#define READ_P_PAZ	(float)readADC(P_PAZ) * (2*PRESS_SENS / 1023) - PRESS_SENS
// Corrente.
#define READ_CURRENT	(float)readADC(I_MOT) * (5 / R_MOT * 1023)
// Pressione nel reservoir
#define READ_P_RES	(float)readADC(P_RES) * (SENS_PMAX / 1023)
// Concentrazione di O2
#define READ_O2		79 / 1000 * (float)(n_impulsi) - 58


void my_ISR(){

	if (stato == ON){
		
		// Effettuo il campionamento dei vari sensori
		
		// === Sensore di pressione dei setacci ===
		// Le letture dai sensori prevedono una calibrazione software.
		ps = READ_P_S;
		
		// === Sensore di pressione del paziente ===
		// Salvo il valore del campionamento precedente al passo precedente.
		ppaz_old2 = ppaz_old;
		// Salvo il valore della variabile precedente.
		ppaz_old = ppaz;
		ppaz = READ_P_PAZ;
		
		// === Corrente ===
		current = READ_CURRENT;
		
		// === Pressione reservoir ===
		pres = READ_P_RES;
		
		// === O2 ===
		// Un contatore viene incrementato 100 volte per discriminare l'intervallo di un secondo.
		counter++;
		if(counter == 100){
			
			// Leggo il numero d'impulsi fornito dalla funzione readcounter().
			n_impulsi = readcounter();
			
			// Sommo la concentrazione ad ogni istante.
			// La concentrazione viene decodificata sulla base del numero di impulsi contati in un istante.
			// La funzione messa a disposizione (readcounter()) effettua la decodifica.
			sum_conc += READ_O2;
			
			// Inizializzo counter.
			counter = 0;
			InitCounter();
			counter_sec++;
			
		}
		
		// Controllo su 10 minuti (aka 600 secondi)
		if(counter_sec == 600){
		
			// Calcolo la media delle misurazioni.
			mean = sum_conc / 600;
			
			// Controllo se è inferiore al 60% della concentrazione O2 (condizione critica).
			if(mean < 60){
				
				// Accendo il LED
				PORTB |= LED;
				
				// Spengo il compressore
				dutycycle = 0;
				
				// Apro le valvole di exhaust.
				PORTB |=  (E1 | E2);
				// Chiudo le valvole di supply.
				PORTB &= ~(S1 | S2);
				
				// Deseleziono i setacci
				setaccio = OFF;
				// Spengo il sistema
				stato = OFF;
			
			}
			
			sum_conc = 0;
			counter_sec = 0;
		
		}
		
	}

}

void main(void){
	
	setup();
	loop();
	
}


```
