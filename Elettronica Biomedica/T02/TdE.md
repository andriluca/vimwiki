# Tema d'esame --- Stampa 3D

Schermi LCD: cristalli che possono essere orientati mediante un campo elettrico
per potersi rendere più o meno opaco.

Bisogna identificare dove si trovano le informazioni relative all'hardware e al
software nel testo.

Se la corrente passante del LED è più bassa avrò una radiazione passante nel
LED inferiore quindi produco meno calore in unità di tempo. Istante per istante
vado a fare un'integrazione della corrente che passa nel LED ed infine otterrò
come risultato il valore TH_POLIMER.

Come si misura la corrente che passa in un circuito? Se la resistenza viene
alimentata da una certa tensione io conosco il valore della resistenza quindi
avrò che la corrente passante sarà uguale alla tensione diviso per la
resistenza che è nota. Conoscendo il valore della resistenza ottengo il valore
di i. Nel caso di un diodo LED noi non conosciamo il valore della resistenza
perchè il testo dice che cambia nel tempo quindi è una resistenza variabile.
Come posso generalizzare il problema? Noi abbiamo uno scatolotto soggetto a V e
con corrente passante. Quindi basterà mettere una resistenza nota in serie! io
so quanto vale la resistenza in serie quindi basterà misurare la tensione ai
capi della resistenza nota, dividendo per la resistenza ottengo la corrente. La
resistenza in serie dev'essere piccola se viene messa a monte il problema però
è che misuro una tensione piccola. Quindi dovrò trovare un trade-off di
resistenza per non andare ad incidere troppo sull'alimentazione del LED ma in
grado di registrare il potenziale. Usando una resistenza piccola però posso
utilizzare un non invertente per aumentare il guadagno del circuito. In realtà
non va bene perchè devo usare un amplificatore differenziale, altrimenti
amplificherei una tensione vicina già a 5V (questo se si mette la resistenza a
monte del LED). Se mettessi invece la resistenza dopo il LED posso benissimo
utilizzare un amplificatore non invertente (la resistenza è dopo LED e prima di
massa).

Se nel tema d'esame compare la parola circa: non è richiesta una gran
accuratezza nella determinazione del valore

## Flowcharts

![Flowchart relativo agli stati di pressione del pulsante e alla stampa.](Figure/tema20-21_button_stampa.png)
![Flowchart relativo agli stati che gestiscono accensione e spegnimento LED UV e il movimento del piatto.](Figure/tema20-21_led_move.png)

## Firmware

```c

#include <avr/io.h>
#include <avr/interrupt.h>

#define BASE 		0
#define APEX 		1
#define F_STEPPER 	1000

#define STOP		0


#define DOWN		0
#define UP		1

#define LED_UV			(1 << PORTD0)
#define LED_PRINTING		(1 << PORTD1)
#define LED_PRINT_FINISHED	(1 << PORTD2)
#define EN_STEP			(1 << PORTD3)
#define DIR_STEP		(1 << PORTD4)
#define CLK_STEP		(1 << PORTD5)
#define ENC_START		(1 << PORTD6)
#define ENC_END			(1 << PORTD7)

#define TX_LCD			(PINB & (1 << PINB0))
#define RX_LCD			(1 << PINB1)
#define CLK_LCD			(1 << PINB2)
#define RST_LCD			(1 << PINB3)

#define BUTTON			(PINB & (1 << PINB4))

#define ADC_CHANNEL0		0
#define R_SENSE
#define OP_GAIN+

#define DIGIT_TO_VOLT		(5.0/1023)
#define	PWM_CH0			0

int printerState;
int printingPhase;

int main(){

	// Inizializzazione delle porte.
	initPort();
	
	// Il timer che genera l'interrupt è impostato con overflow ogni 0.001s.
	setInterruptTimer(1000);
	
	// Lo stato iniziale del piatto è alla apice (STOP).
	movePlateTo(F_STEPPER, APEX);
	printerState = STOP;
	printingPhase = IDLE;
	
	while(1){
		
		switch (printerState){
			
			case STOP:
				
				// Controllo pressione prolungata
				if (!emergencyStop){
					if (BUTTON){
						// Attesa rilascio
						while(BUTTON);
						// Accensione LED rosso.
						PORTD |= LED_PRINTING;
						// Piatto posto in base
						movePlateTo(F_STEPPER, BASE);
						currentLayer = 0;
						printerState = PRINTING_STATE;
						printingPhase = IDLE;
					}
				} else {
					[...]
				}
				break;
				
			case PRINTING_STATE:
				break;
		}
	}
}

void movePlateTo(int freq, int moveTo){
	
	// Imposto il PWM hardware sulla linea di CLK dello stepper.
	initPWMHW(freq, PWM_CH0);
	setDutyCycle(50, PWM_CH0);
	
	switch(moveTo){
		
		case BASE:
			// La direzione è HIGH.
			PORTB |= DIR_STEP;
			break;
			
		case APEX:
			// La direzione è LOW.
			PORTB &= ~DIR_STEP;
			break;
	
	}
	
	// Abilitare lo stepper.
	PORTB |= EN_STEP;
	
	// Controllo del raggiungimento destinazione stepper.
	while(ENC_BASE && ENC_APEX);
	// Spegnimento dello stepper.
	PORTB &= ~EN_STEP
}

void movePlate(int direction, int freq, int micro_m){
	
	// L'approccio parametrico di questa funzione 
	// permette di renderla	utilizzabile in altri contesti.
	
	intPWMHW(freq, PWM_CH0);
	setDutyCycle(50,PWM_CH0);
	
	// Impostazione della direzione del motore.
	switch(direction){
		
		case DOWN:
			PORTB |= DIR_STEP;
			break;
		case UP:
			PORTB &= ~DIR_STEP;
			break;
			
	}
	
	// Abilitazione del motore.
	PORTB |= EN_STEP;
	
	// Attesa fine del movimento.
	// Ciascuno step è un colpo di clock.
	int n_steps = 0;
	
	while(n_steps < micro_m){
	
		if(PORTD & (1 << PORTD7)){
			
			while(PORTD & (1 << PORTD7));
			n_steps++;
			
		}
	
	}
	
	PORTB &= ~EN_STEP;
	
}

void LCD_setPixels(int currentLayer){

	// Ciascun intero ha 16 bit per ATMega328P.
	// La funzione readSD restituisce riga per riga i valori da assegnare
	// ai pixel dello schermo.
	int datiPixel[1024/16];
	int stratiTotali;
	int paritySum = 0;
	int parityCheck;
	
	for (int row = 0; row < 1536; row++){
	
		// Lettura riga row-esima.
		readSD(currentLayer, row, datiPixel, &stratiTotali);
		
		// Invio dei dati (16 per volta).
		for(int i = 0; i < 1024/16; i++){
			
			// Inizializzazione parità.
			paritySum = 0;
			parityCheck = 0;
			
			for(int j = 0; j < 16; j++){
				
				// Controllo se il bit j-esimo è a 1.
				if(datiPixel[i] & (1 << j)){
					
					// Invio 1.
					PORTB |= RX_LCD;
					// Accumulo il bit per la parità.
					paritySum++;
					
				} else {
					
					// Invio 0.
					PORTB &= ~RX_LCD;
					
				}
				
				PORTB |= CLK_LCD;
				
				// Attendo un istante per rendere efficace la comunicazione.
				for(int k = 0; k < 1000; k++); // Delay
				
				PORTB &= ~CLK_LCD;
				
				for(int k = 0; k < 1000; k++); // Delay
				
			}
			
			// Controllo parità calcolata dal controllore LCD 
			// per i 16 bit appena inviati.
			for(int w = 1; w <= 0; w--){
			// Verso il basso perchè semplifica 
			// il riordinamento dei bit con la maschera.
			
				PORTB |= CLK_LCD;
				for(int k = 0; k < 1000; k++); // Delay
				
				if(PINB & (1 << PINB5)){ // if(LCD_TX)
					parityCheck |= (1 << w);
				}
				
				PORTB &= ~CLK_LCD;
				for(int k = 0; k < 1000; k++); // Delay
			
			}
			
			// Controllo del primo bit di parità.
			if (!parityCheck & (0b10)){ // high solo se il secondo bit è 0
				LCD_sendRST();
				// Ricomincio i due cicli for in modo forzato.
				i = 0;
				row = 0;
				
			}
			// Controllo se vi è un errore nel secondo bit di parità.
			else {
				if (((parityCheck == 0b10) && (paritySum % 2)) 
				|| ((parityCheck == 0b11) && !(paritySum % 2))
				){
				
					LCD_sentRST();
					i = 0;
					row = 0;
				
				}
			}
			
			// non sono sicuro che vada qui
			return stratiTotali;
		}
		
	}

}

void ISR(){

	if (printingPhase == LED_UV_STATE){
		float v_rsense = readADC(ADC_CH0) * 5.0 / 1023;
		float i_rsense = v_rsense / (3 * 1);
		
		// Accumulo il valore di corrente
		exposure += i_rsense / ISR_FREQ;
	}
	
	if (BUTTON){
	
		counter++;
		requestPause = 1;
		
	}
	
	if (counter >= 5 * ISR_FREQ){
		
		printerState = STOP;
		emergencyStop = 1;
		
	}

}


```
