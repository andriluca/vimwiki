# Elettronica biomedica

| Lezione             | Argomento                                        | Difficoltà | Slides comprensibili | Data |
| :--:                | :--:                                             | :--:       | :--:                 | :--: |
| [01](01/Lezione.md) | Introduzione, lo strumento di misura e ripasso C |            |                      |      |
| [02](02/Lezione.md) | Microcontrollori                                 |            |                      |      |
| [03](03/Lezione.md) | Logiche programmabili                            |            |                      |      |
| [04](04/Lezione.md) | Reti e comunicazione dati                        |            |                      |      |
| [05](05/Lezione.md) | Sistemi di monitoraggio                          |            |                      |      |
| [06](06/Lezione.md) | Ecografo                                         |            |                      |      |

| Esercitazione              | Argomento     | Difficoltà | Slides comprensibili | Data     |
| :--:                       | :--:          | :--:       | :--:                 | :--:     |
| [01](E01/Esercitazione.md) | Alimentazione |            |                      | 06.10.21 |
| [02](E02/Esercitazione.md) | Attuatori     |            |                      | 18.10.21 |

| Laboratorio              | Argomento           | Difficoltà | Slides comprensibili | Data     |
| :--:                     | :--:                | :--:       | :--:                 | :--:     |
| [01](L01/Laboratorio.md) | Introduzione        |            |                      | 06.10.21 |
| [02](L02/Laboratorio.md) | Interrupt, timer    |            |                      | 18.10.21 |
| [03](L03/Laboratorio.md) | ADC e campionamenti |            |                      | 10.11.21 |
| [04](L04/Laboratorio.md) | PWM                 |            |                      | 10.11.21 |

| Temi d'esame     | Argomento          |
| :--:             | :--:               |
| [01](T01/TdE.md) | Reservoir, setacci |
| [02](T02/TdE.md) | Stampa 3D          |

[Embedded C](Embedded.md)
[Event driven programming](Event.md)
[State machines](State.md)
