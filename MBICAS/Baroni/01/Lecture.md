# Lecture 1 --- Introduction

## Slide 1

What we will focus on in this second module is the part of the Computer Aided
Surgery.  we have this mixed teaching activities: two hours slots for practical
lessons in presence for 50% of us and the other 50% in the remaining two hours
slots on thursdays.

## Slide 2

A very short summary of what we will do and see during CAS. When we talk about
CAS we always have to deal with two main phases of the whole workflow of CAS
regardless the specific surgical field in which the technologies and methods
are put in practice.

1. *Pre-operative phase*: we are acquiring all the data on the patient that we
   need in order to perform and implement a surgical plan. A phase in which
   according to a model of the anatomy and of the pathology of the patient the
   surgeon will decide the best way to face the surgical problem and the best
   way to operate the surgical plan, so to intervene on the specific patient.
   This will require some image processing skills:
	- Segmentation
	- Modeling of the structures of interest of the patient
	- Deciding parameters, directions, angles, point of entries and stuff
	  like that.  

   All these are performed in-silico. So there's a phase in which operator will
   working on a surgical planning workstation but, and that's important, on
   patient-specific data. We will use imaging data acquired on the patient
   (CTs, MRIs eventually integrated by PETs or other intrinsically 3D methodics
   which will allow the operator to build a patient-specific model and to
   establish all the numerical and quantitative parameters that will describe
   as a whole the surgical plan).
2. *Intra-operative phase*: it's the surgical plan realization in which we will
   have to worry about, as a first thing, how to transfer the quantitative
   dataset describing the surgical plan in the reality of the surgical room.
   This will not be a very easy problem because we will have to deal with:
	- Different reference system in which the data are described.
	- The patient may have changed between the pre-operative phase, in
	  which the images have been acquired, and the intra-operative phase
	- The use of intra-operative technology such as surgical robots or the
	  use of intraoperative technology for image acquisition for 3D
	  localization of tools because these will be technologies that will be
	  used during the surgery in order to verify the quality of the
	  transfer of the surgical planning in the reality of the
	  intra-operative phase and to, if we wish, monitor the way in which
	  the surgical plan has been realized in the reality of the operation
	  of the surgical intervention. 
	- Organ motion: the patient may have changed and it's not a rigid body.
	  All the scenario of CAS is built on the hypothesis that the patient
	  and the structures of interest in surgical procedure are more or less
	  unchangeable, so may be identified and treated as rigid body and this
	  is an hypothesis that we need to put in practice in order to build
	  the framework of CAS but we know from the very beginning that this is
	  not true, so the patient is a human living, changing and deformable
	  system. He may change between the moment in which we acquire the
	  images and the moment in which we have built the surgical plan that
	  may change during the intervention because of the surgical procedure
	  and we need to face all the problems that may lead to variation
	  between the anatomo-pathological structure and configuraiton of the
	  patient at the time of the pre-operative phase and at the time of the
	  different phases of the surgical interventions. There are specific
	  solutions built to measure, to counteract, the variation. we want to
	  take them into account in a sort of quantification of the safety
	  margin (of the reliability margin) that we will have to communicate
	  to the surgeons in such a way then when our technologies for
	  monitoring how the surgical plan has been carried out in the reality
	  of the surgery, the surgeon will know the reliability of the
	  indications that we will be displayed to him during the surgical
	  intervention as a way to monitor the way he is doing the procedure.

We will talk about the application of these technologies in specific surgeries
and these reference surgeries are:

- Neurosurgery, perticularly cranial neurosurgery in which the so-called
  neuronavigation is a very highly practiced of CAS and it's a state of the art
  of CAS, exemplifying case of CAS widely applied worldwide.
- Orthopoedic surgery: in particular spinal surgery but not only but other kind
  of orthopedical intervention in terms of replacement of joints such as hip,
  prosthesis implantation, knee prosthesis implantation and so on.
- Radiation oncology or radiotherapy: irradiation of oncological lesions by
  means of high energy x-ray photon beams or charged particle beams (H+ or C
  ions). This is a particular surgical intervention that does not foresee the
  presence of the operator beside the patient. the patient is alone in the
  treatment room and this is a kind of surgery that requires really a very
  precise definition of surgical plan and a surgery in which we will need to be
  really sure that what's going on inside the therapy bankers is exactly what
  we have planned and that has to happen.

## Slide 3

Logistical information: teaching material will be just for us and enough for us
to endeepen the knowledge on this subject. Also the practical lessons. If some
of us are interested in endeepening a specific topic there are two books. the
technological evolution is so fast that it's very hard to freeze the methods on
printed books. The evolution is so fast that there's not a proper standard.

## Slide 4

Exam: The traditional method is here reported on the slide for the final
examination of this module. there were two parts: a written text or part in
which, if the situation was normal, we would have received a set of numerical
exercises similar to the ones that we'll see during practical lessons and some
theoretical questions with multiple choices. The written exam will then be
corrected by Professors and if we have received at least 18 on 30 points we
will have access to the oral part with Baroni, Belotti or Paganelli. The
situation over the last year was not replicable, so Professor did anything
online and that's the way also of this year probably (if no further
communication is received). And changes that are imposed is that the written
part is not being held as traditionally but anything will be oral that in any
case will start from the solution of an easier numerical exercise (similar
again to the one seen in the practical lessons but solvable with online
contacts). If there's the possibility to restore the traditional modality with
the written exam we'll do that but now we are waiting if we have the chance to
do that.  Onced gained final mark on the seocnd module (after written + oral)
the final mark of the course will be the average of the one of the mark gained
in the first and second module. It's possible, within the academic year, to
undergo the examination of one module separated with respect to the other and
to keep valid the gained mark on either the modules.

## Slide 5

Let's underline the rationale of the course: why Biomed engineer should have
these skills? Because nowadays the paradigm of surgery, specifically in the
surgeries that were firstly mentioned has changed very rapidly and make
extensive use of technology that help surgeons to carry out the surgical
interventions according to a predefined surgical plan. the evolution is based
mainly on the availability in any hospital (at least in western world but not
only) on imaging technology that are applied on any patient, not only to
perform the diagnosis (so to establish the extent of the desease, the presence
of desease whereas the surgical intervention) but to have a clear picture from
the geometrical (quantitative) point of view of the specific characteristics,
location, extension, volume of the specific disease that requires surgical
intervention and all other anatomical structures that need to be spared by
surgical intervention and this is applied the more the higher is the required
accuracy in the surgical intervention. when we talk about accuracy we think
about geometrical accuracy .

Let's make this example: removal of oncological disease within the brain.

1. In the past as we see in the picture the patient has symptoms and there were
  no technologies available to make even a diagnosis of the presence of this
  disease but the patient has neurological symptoms so what the surgeon could
  do was opening the patient, looking inside and seeing what's wrong. If the
  surgeon saw something wrong then he removes the lesion. No worries about how
  much of the lesion is present in the brain, how big the lesion is, how close
  this lesion is to sensitive brain structure but anything was done at the
  moment and this is something that is not replicated anymore but it was done
  from the Egyptians.
2. Traditional approach that was valid until very recently is that technology
  were available at least to perform a diagnosis (look inside the patient
  without opening to establish the presence of a disease correlated to the
  symptoms that the patient was exhibiting). As soon as the indication for the
  surgery was established by the medical equipe then the surgical intervention
  was carried out without any sort of guidance, without establishing the best
  way to perform the surgical procedure. Diagnostic was done, patient was
  brought into the surgical table in the surgical room and the surgical
  procedure was completely given in the hands of the surgeons who, by means of
  their experience, were carrying out the surgical procedure in the best way as
  they could taking decision during the surgical intervention, looking inside
  the patient, trying to avoid sensitive structure and to remove all the lesion
  they were able to see and then closing the patient without indication,
  programs, plans to follow during the surgical intervention. 
3. The availability nowadays since 20 years of not only diagnostic technologies
  capable of bein intrinsically 3D, so of providing the surgeons with 3D
  representation of the anatomo-pathological configuration of the patient, the
  lesion and sensitive structure but also the availability of technologies for
  taking quantitative information that can be mounted and used inside the
  surgical room has opened the way to CAS, to a new paradigm of surgery. I
  don't use only the imaging technology in the preoperative phase to perform a
  diagnosic but i use this patient-specific representation to build a model of
  the structure I need to remove, of the sensitive structure I need to avoid
  and use these patient-specific models to establish parameters that will be
  mainly geometrical for the surgical intervention (where should i access the
  skull of the patient, which is the best way, the shortest path between the
  surface of the patient and the lesion i have to remove, what is the safest
  part to dig inside the brain of the patient to reach the lesion that is sited
  deep insthe brain. Safest: the path that allows me to avoid as much as
  possible sensitive structures). This is sometihng that nowadays by making use
  of multimodal imaging, not only a description of anatomy of the patient but
  also a description of the function of these sensitive structures that i need
  to avoid. This is something that i can do in-silico, sitting on a table,
  looking at the multimodal images i have acquired and fused together of the
  patient that need to undergo to surgery and i can establish on the paper
  that's on the computer the best way to perform the surgical procedure. this
  is only te beginning, then I'll have the chance to make use of
  intra-operative technology as we were saying before to be really sure that
  what I will be doing during the surgical procedure reflects what I have
  planned during the pre-operative phase that I'm sure will be the best way to
  perform surgical procedure. I will have the chance to obtain, from the
  technology installed in surgical room informaiton being acquired online on
  the patient during the surgical intervention. By means of the comparison
  between the informaiton that i get inside the surgical room with respect to
  the corresponding information being part of the surgical plan the surgeon
  will have a guidance available, in order to adapt and to follow properly the
  surgical plan and to take decisions with a guidance, a continuous flow of
  information coming from the treatment plan and being compared online with the
  information that I'm able to acquire on the patient during the surgical
  procedure so that i can control the way in which the surgical plan is
  transfered in the reality of the surgical information. this kind of guidance
  is typically done by images, so the kind of information I acquire during
  surgical procedure is mostly made by images I acquire from the patient: x-ray
  projection, x-ray volumetric acquisition, ultrasound images or something like
  that. That is why one of the basic term often associated with CAS paradigm is
  image guided surgery. From image guidance the current developing lines of
  researches of application in this scenario leads to the augmented reality
  paradigm: we can provide the surgeon the augmented scenario in which the
  information coming from the treatment plan and integrated with the
  information that i will acquire in surgical room can be put together and from
  this integraiton i can dig out specific matrix, numbers, indication or
  whatever in order to increment the level and the quantity of the information
  that the surgeon has available in order to orient himself much better in
  respect to the past (when no information were given to the surgeon during the
  intervention).
  
In the surgeries mentioned before (cranial neurosurgery, spinal, radiation
oncology) this third way is the reality. what we see on the right is what
happens worldwide in advanced surgical and oncological centers. This is why we
feel that biomedical engineer should be expert or should know the methods and
the technologies that underlie and support this new paradigm of surgery in
which images technologies, localization technologies, patient-specific models
are current practice especially in surgical fields mentioned.

## Slide 6

One example is navigation is surgery. here is reported a picture of what we
could see in surgical room for cranial intervention in whatever neurological
center even in Milan. The presence in surgical field not only of the operator's
hand but also insturment inserted by the patient and used by the surgeon that
carries these landmarks (similar to small dots). These are nothing else than
retroreflective markers, exactly similar to the ones used for motion and gait
analysis of humansthat are detected and reconstructed in 3D wrt a specific
reference system by an infrared localization system. This is one of the non
image-related technology that supports the prince of the applications of CAS
which is neuronavigation. Why navigation because the use of this kind of
technology when associated to the surgical plan being transferred correctly in
the reality of the surgical procedure really provides the surgeon with a
navigation tools, with a flow of information that allows the surgeon to
understand what he's doing to navigate his surgical actions inside the surgical
field according to the indicaitons coming from the treatment plan. it's a sort
of GPS: every time a surgeon required to be guided by this kind of
technologies, they are activated and the surgeon receives indication of what
he's doing or where is the tip of the instrument that he's inserting inside the
skull of the patient. He knows exactly where he is inside the very complex
situation in which the differenciation of tissue inside the brain typically is
not high so it's difficult even for an experienced surgeon with only his eyes,
to understand exactly whether he is at the limit of the oncological legion he
needs to remove, how far he is from the sensitive structures that he's clearly
indicating in the surgical plan but that is not clearly identifiable during the
reality of the surgical intervention. The localization by means of infrared
localization technology as we see from the picture of the tools used by the
surgeon, the localization of the tip of these tools inside the reality of the
surgical procedure provides the surgeon with very important indication and
information because he can understand by looking at the surgical plan where the
position of the tip of the needle or of the tool is mounted and is represented
and it can represent what he's doing, where he is, how far he is from this,
depending on the prospective, dangerous tip of the needle or of the escavator
that they used to remove brain tissues from sensitive structures that at the
time of the surgical plan definition was decided to be absolutely avoided in
order to limit the side effects of the surgical procedure.

## Slide 7

Another example: Cerebral biopsy with navigation: the biopsy is the insertion
of a needle through a very small and thin hole in the skull of the patient in
order to reach as seen in the right part of the figure a lesion which is inside
of the brain. The blue channel is the established plan of the needle at the
time of the surgical plan definition. It was decided that this (*indicating the
hole*) was the right point of insertion (shorter and safest path in this way to
insert the needle in the brain of the patient and reach the center of the
lesion to take out a little bit of tissue in order to perform an histological
caracterization of the lesion and understand what kind of lesion is the patient
suffering of). The red cross that we can see here is the position mounted,
projected on this slice of surgical plan MRI acquired beforehand on the
patient. It is the position of the tip of the needle at the time of the
surgical intervention. the yellow line rightly centered into the blue path is
the current position of the needle. the tip is the red cross. We can imagine
that during the execution of surgical procedure the surgeon which is not able
to look inside the brain because he's just inserting a needle, is able to
understand if the needle is following the predefined trajectory, he can
understand where the tip of the needle is, whether it is inside the lesion or
not and according to that he can decide how to proceed. It's a set of
technologies that gives surgeon sight inside structures that cannot be looked
at directly.

## Slide 8

A [video](https://www.youtube.com/watch?v=_BFTK6LWH5g) is being shown in this
slide that is still focused on navigation for brain tumor which is a typical
example as we were saying. This is given by a surgeon but it will mention at
least in this video much information and a lot of concepts, technologies and
methodologies that will be part of our course.  Even if we look at the slide
before in order to understand if we are doing the right thing well it's not
granted though, it's not easy to be sure that the representation that
ultimately is provided to the surgeon is correct and we all do understand that
this information needs to be correct or at least we need to know how much it is
correct. So this is the final result of the application of different technology
and methodologies to define surgical plan, to transfer in the right way the
surgical plan indication into the reality of the surgical intervention, to
obtain in a proper way the information during the execution of the surgical
procedure which are not trivial. We will need to learn how to do that in a
proper way because there's nothing more dangerous in health applied
technologies field than technologies and methods applied in the wrong way. If
we do that, the result is the worst thing that we can do. It's better not to
apply any technology and do the surgeon do his job otherwise. If we want and
pretend to provide to the surgeon a flow of information that we will design, as
we will design the technologies and the methodologies that will be applied to
come up with an indication like this to be provided to the surgeon (critical
information because used as a guidance) we need to be very sure that what we
are creating as information to be provided to the surgeon is correct. We have
to be rigorous and very sure of what we are doing and our calculation because
if we do something wrong we will provide to the surgeon a wrong information and
this is the most dangerous data we can provide to surgical operators. The
excercises we are called to solve for the examination need to be done correctly
and no error can be accepted. an error might lead to a disaster.

### The video

Image guided surgery has become an essential tool in brain tumor
surgery. the head, the skull, the skin are not showing where the tumor is and
in the old days before image guidance we would have to make a very large
opening in surgery in order to find the tumor. With image guided surgery we can
take that information: the MRI and CT and we can load it to a system very
similar to the GPS system used for our car. GPS for our car allows to navigate
around town and found a spot. with GPS image guided surgery we can navigate and
find the tumor.

Like GPS there are very important components: 

1. Camera: it emits some infrared light that shines down and reflects off these
   small reflective balls. 
2. Antenna: is attached to the operatory room table and it does not move. it
   serves as a reference. with that reference we can introduce the pointer.
3. Pointer: it has these reflective balls and we can point a spot on the skull
   or the scalp and that will then reference back to the camera and load in to
   the computer.

The image guide system shows different images that provide us a tridimensional
picture so that as we bring the pointer in and the camera sees that pointer, it
can find the tumor and find the path that's safe to go into the brain. That way
we can make a smaller safer opening to find the tumor and remove it.

In order to use image guided surgery in operative room there are several steps
we go through

1. MRI scan: before we come to operative room we have to do the MRI to diagnose
   the tumor. It's done ahead of time and sometimes we place spots called
   fiducials. These fiducials are markers that can then be seen on the MRI and
   can be used to register into the computer.
2. Registration: at least is the step in which we are going to bring the
   patient to match the 3D data from the MRI. The first step in doing this is
   to register the fiducials, the markers that we have placed at the MRI. And
   we do that by pointing to the fiducials with our pointer, the camera sees
   the pointer and the antenna and matches thus in the 3D map. Another way that
   we register the patient is by using the surface anatomy of the skin: using
   this device the camera can see the surface anatomy and it takes multiple
   spots that we can see filling up the skin there to obtain an accurate
   registration. once the registration is obtain we can then navigate to find
   the tumor.
3. Navigate to tumor: now that we have registered the patient we can use the
   GPS to find the tumor and we can navigate and map the tumor on the skull so
   that we can make the smallest opening possible. this allows us to perfomr
   craniotomy and nowadays we can make the smallest opening into the bone to
   allow me to work into the brain, find and remove the tumor.
4. Confirm tumor removal: here we are in surgery removing the brain tumor. we
   are using the pointer to examine the edges of the tumor inside the brain.
   the camera sees the pointer and displays its location on the computer
   screen. As we move the pointer around the position is shown by the green
   cross arrow and using that we can identify the edges of the tumors. we can
   check and verify that we have removed all the tumor before finishing all the
   procedures.

---

There are many different concepts in this video: we have recognized the two
main phases that we are seeing before:

1. MRI scan phase which is our pre-operative phase. in this phase the surgical
   plan is being defined here.
2. Registration procedure: 
	1. Point-based registration: using the fiducial
	2. Surface-based or non corresponding point registration: Acquiring the
	   entire surface of the face of the patient
	3. Based on images, so acquiring images (x-ray projections because they
	   are typically the ones available in surgical rooms).

Registration is always required in order to match the information of the
treatment plan, the reality of the model built on the patient on the MRI scan
acquired before the surgical intervention in the pre-operative phase, with the
reality of the patient inside the surgical room. we require this match because
every measurement that we will do during surgical procedure, typically the
position of the pointer that we see, in order to retroproject this position of
the pointer (better the tip of the pointer so the green cross he was showing)
onto the MRI scan in part of the pre-operative plan, will require to be matched
the two realities (pre and intra-operative realities) otherwise we can't open
the gate to transfer information from the surgical plan into the surgical room
and from the surgical room into the treatment, therapy and surgical plan. The
registration that will be one of the major focus of this course because it can
be done with very different ways and with different objects, according to
general schema that is common to the different ways in which we can perform
registration, is on the other hand a crucial procedure and it will provide the
necessary and required accuracy of matching the two phases and we influence a
lot the level of accuracy and reliability of the information that we can
display to the surgeon. If this registration procedure was wrongly made we
could understand that the green cross showing the position of the tip of the
pointer would have been in the wrong place with respect to being retroprojected
and graphically displayed on the MRI being part of the surgical plan. The
surgeon will believe to that information and will make a mess but the problem
is not of the surgeon but of the engineer that performed wrongly the method for
registering (matching the two realities). This is something we really need to
be precise
3. Navigation: intra-operative phase.

## Slide 9

Why the technology evolution has led to the implementation and application of
this kind of technologies in surgery?  A non exhaustive list of advantages and
rationales of the use of the technology for CAS in the currently practice
surgeries, especially the ones reported before:

1. *Minimality of invasiveness* of surgical procedure is higher if it's
   supported by CAS technologies and paradigms. The surgeon in the video was
   mentionining the dimension of the hole of the skull. If we know or can
   understand at the time of the surgical intervention where the tumor is (i.e.
   if i can visualize it properly on the MRI that was acquired on the patient)
   or, even more, if we have a guidance and I have decided in the pre-operative
   phase where is the point of insertion and how big the incision must be on
   the skull that the surgeon has to take, then the dimension of the hole will
   be as small as possible to perform the intervention and this is very
   different from a situation in which the surgeon knows more or less where the
   tumor is but he needs to open a wider hole on the skull of the patient in
   order to look and see what's inside. In the CAS case the hole is not
   important in terms of visibility of the structures inside the brain because
   they can be identified by putting the pointer inside the small hole that was
   practiced in the skull of the patient and looking on the MRI scan where the
   tip of the pointer is, where the structure to be removed and the ones to be
   spared are with respect to the tip of the pointer (which will be also the
   tip of the ultrasound bisturi that is used to emulsionate and to remove the
   tissue to dig the needed path in order to reach the lesion).
2. *Minimal hospitalization*: This depends on the previous point, mainly, if we
   have the minimal invasiveness to the procedure the patient needs to be less
   in the hospital, he will recover sooner and suffer less. There are lot of
   ethical and economical implication. If the patient can go home earlier, the
   hospital spares money because it receives back from the National Health
   System (or private insurance companies) the same amount of money but the bed
   will be occupied for less days. This is useful if we want to make hospital
   survive. 
3. *More predictable outcome* if we have the chance to implement and define a
   surgical plan in the pre-operative phase then we can predict with higher
   accuracy what is going to happen during the surgical intervention. We can go
   to the patient and tell him about the risks of the surgical intervention and
   discuss with him about the final decision. We are more conscious of what can
   be the risks and the outcome of the surgical intervention and the surgeon
   can ask himself if it's an overtherapy or not. This because we know in
   advance what is going to happen when the plan that was studied in silico
   will be transferred in the reality of the surgical intervention.
4. If we are using tracking technology as in neuronavigation, there's the
   possibility of *documenting what is happening*, record all the surgical
   intervention and this has a lot of implication: the use of this technology
   can provide quantitative datasets on how the things went during the surgical
   intervention to mitigate liability and litigation (i.e. the patient sues
   surgeons or hospitals because they had problems during the surgical
   procedure). This kind of document can be useful in order to understand and
   demonstrate how the things went during the surgical intervention.
5. *Minimized operating room (OR) time*: A guided surgical intervention
   normally lasts less time than a not guided one. This has economical but also
   ethical implication. The patient will need to be under anhestesia for less
   time.
6. In some cases, there can be a sort of *marketing for institutions*: so if
   institution advertises the use of very highly technological systems for
   surgery than more patient will go to that institution with respect to
   another.

## Slide 10

Let's start talking about agreement among us. The evolution of this scenario is
so fast that sometimes there's a little bit of confusion about what we can call
CAS paradigm or workflow, what is image guidance, whether image guidance is
part of CAS, whether intra-operative localization by means of optoelectronic
technologies can be referred in the CAS paradigm.

**First agreement**: We will be able and allowed to talk about a CAS workflow
(aka paradigm) *if and only if* we recognize in the workflow these three main
phases:

1. *Acquisition of 3D images on the patient, modelling of the patient*:
   building a model of the anatomical structures of interest of the patient and
   allow the surgeon to make the most of the patient-specific models that can
   be built within the software applications for surgical planning to define
   quantitative parameters of the surgical procedure. So we will have imaging
   dataset, patient-specific models with capabilities of the software, that I
   use for doing that, to visualize properly the anatomical structures, the
   patient-specific model. 
2. there will be for sure a *planning* phase in which the surgeon will use
   these models and this visualization potentials made available by the
   software in order to establish quantitative parameters (i.e. trajectories,
   point of insertions and whatever we want). So for instance, as we see in
   here, the trajectory in blue that the needle needs to follow in order to
   reach the very nicely represented lesion deep sited inside the brain.
3. *Realization*: recognizing the presence of specific systems (technologies)
   installed inside the surgical room that need be used to acquire information
   describing, generally speaking, the reality of the surgical intervention:
   position of the patient, position of the surgical tools, poistion of the tip
   of the pointer and things like that.

There's a lot of example in which we have only a subset of these three phases
so we need to be careful.  In between the second and third phase there's
something that is not reported but there's something that is so important that
if we have a plan and if we have navigation inside the surgical room then we
will always have here in between the **Registration**: the application of these
methods that allows us to match the reality of the two phases, that allows us
to open the door between pre and intra-operative phase.

## Slide 11

This slide is reporting a sort of list of elements that characterize the two phases:

- Pre-operative planning phase (pre-operative phase)
	- *The use and application of x-ray* (not only but also MRI) imaging
	  technologies that are always volumetric and provide three dimensional
	  information on the anatomical configuration of the patient. It can
	  also be 4D-CT. T1-, T2-weighted MRI for a correct and very precise
	  description of the anatomical configuration of the patient in the
	  field of interest where the surgical procedure will take place.
	- *Integration with functional imaging*: PET is very used in the case
	  of oncological diseases but not only, also fMRI. These are elements
	  to acquire the necessary information to build the patient-specific
	  model on which the plan will be defined.
	- *Segment the structure of interest*: this is not trivial because
	  errors in defining the contours of the structures of interests (both
	  lesions and sensitive structures) will lead to inaccuracies in the
	  whole procedure. We need to understand all the possible sources of
	  uncertainties and error in CAS paradigm then we need to be very
	  conscious when we mention and think of the use of any of these
	  element being described whether this element can be one of the
	  sources of potential errors or inaccuracies. This error is
	  incrementing the margin of safety that in the end will be
	  superimposed to the final information that we will provide to the
	  surgeon. One example of all is MRI that is affected by distortion. CT
	  can be affected by artifacts if we have metal implants somewhere in
	  the body. This distortion affecting MRI or artifact effecting the CT
	  are not normally a problem if we are using this kind of technology to
	  diagnose a disease but if we pretend to use the same technology to
	  take measures, to establish geometrical parameters of a surgical plan
	  to build a patient-specific model on which we need to take
	  measurements and these measurement will be part a surgical plan that
	  we need to transfer in the reality of the surgery which will be used
	  as a guidance for the surgeon well now distortion in MRI and
	  artifacts in CT become a problem. This is one example.  Another one
	  is the calibration of the localization system based on optical
	  technique we saw in the neuronavigation is an issue: if the accuracy
	  with which we are able to calibrate an optical tracking system inside
	  the surgical room is an issue in terms of the overall accuracy that
	  we can grant when using this technologies to localize the pointer
	  that the surgeon will insert in the brain of the patient and to take
	  this information and retroproject it through the registration
	  parameters onto the surgical plan. We will always have errors because
	  we know that every time we measure something we make an error. The
	  size of our error is our topic of interest and the issue we need to
	  study. How the error is in taking a specific measure.  We can't
	  ignore this fact because we need to know the size of the inaccuracies
	  that are intrinsic in the methods that we are applying because we are
	  applying these methods in a very crucial situation.
	- Definition of treatment physical (i.e. when we talk about the
	  radiation oncology we will need to establish the physics of the
	  radiation beam that we'll be using) and geometry parameters.
	- Simulations: we can simulate in silicon the way in which the surgical
	  procedure will go or would go according to the specific parameter
	  that we have established at that iteration of surgical planning. this
	  applies at different levels whether we are talking about neurosurgery
	  or orthopedic surgery or radiation oncology. In radiation oncology
	  for example there's an extensive phase of simulation. On a CT being
	  acquired on the patient an according 2D parameters of the radiation
	  beams (i.e. direction from which the beam will penetrate the patient,
	  how many beams i will be using, what's the orientation of the patient
	  wrt the beam, the dimension of the field of irradiation). According
	  to these parameters there's an extensive phase of simulation in which
	  the medical physicists together with radiation oncologists will look
	  at the final result. What will happen according to these specific
	  parameters on the physics and on the geometry of the beam, if we
	  would irradiate the patient according to these parameters. the final
	  result of the simulation is displayed in terms of radiation dose
	  distribution inside the patient. That's why we have written in here
	  "dose distribution simulation, optimization and evaluation". If we
	  change the dimension of the field of irradiation, the number of
	  fields, the directions of penetration and so on i will change also
	  the way in which the radiation dose will be distributed inside the
	  patient and due to the fact that the final clinical result for the
	  planning stage in radiation oncology is to focus the dose inside the
	  oncological target and spare as much as possible the healthy
	  structure surrounding the pathological target, the extensive
	  simulation phase will look for the best tradeoff among the different
	  parameters which can be changed in order to obtain the maximal dose
	  deposition inside the target and the maximal sparing of the
	  sorrounding healthy structures because irradiating these last ones
	  will create potentially side effects of the irradiation. The planning
	  phase is very though and takes long time to the operation to reach a
	  final tradeoff which always be a tradeoff between the expected
	  therapeutic effect of the irradiation and the associated side
	  effects. The ratio between these two requirements (controlling and
	  sterilizing the tumor by means of irradiation and keeping good
	  quality of life with no important side effects of the irradiation for
	  the patient) needs to be established on a patient-specific based to
	  an extensive phase of treatment planning and treatment simulation
	  in-silico. 
- Delivery and treatment phase (intra-operative phase):
	- *Position of the patient*: the position of the patient inside the
	  surgical room or the therapy bank will be normally different with
	  respect to the one the patient had at the time of the images
	  acquisition. the problem will be dealt by matching the two reality by
	  the fiducials or through image-based registration. So registration is
	  important.
	- *Verification* that the things correspond during surgical procedure
	  to what we expect and have described in a dataset related to the
	  surgical plan.
	- *Measure and compensation* for patient's deviations and modifications
	  from the time of acquisition of the images for surgical planning and
	  the time of surgical procedure execution or treatment delivery.
	- This is referred to irradiation oncology: there might be modification
	  between the planning and realization phase and we need to measure
	  them but there might be also variation of the patient also during the
	  surgical procedure and during the radiation (*intra-fractional
	  deviation*). Another example respiration in the orthopedic spinal
	  surgery: structures move due to respiration and this causes
	  geometrical modification irresolvable. we need to deal with
	  incertainties, know that they are there and cope with them. Those are
	  one of the major source of inaccuracy in navigation.

## Slide 12

Graphical representation of the element of Integrated CAS systems.

There are different elements concurring together to establish the pre-operative
dataset.  Not only imaging and result of simulation and planning but there are:

- Amnestic patient data.
- Laboratory data (histological results).
- Structural or geometrical information of the implants that need to be
  inserted into the patient.

In the intra-operative phase we see normally a workstation that controls and
integrates the information coming from different technologies that we normally
find in a surgical room equipped by technologies for CAS:

- Optical tracking localizers
- intra-operative imaging devices:
	- intra-operative MRI scanners.
	- intra-operative CT units.
	- Ultrasound imaging devices for acquiring image related informations
	  to be compared with the corresponding images coming from the
	  treatment plan.
- Robotic surgical devices (i.e. Da Vinci)
- Completely autonomous robots (like the one of radiation oncology to deliver
  the treatment).

## Slide 13

The next slides summarize what we have to know about the imaging technologies
that have a role in the initial phase of all our journey from the very
beginning to the production of the last information for guiding the surgeon
during the surgical procedure.

For sure we'll talk about x-ray-based imaging and CT as the reference
technology for x-ray imaging for obtaining information that we need to build
the patient-specific model. We'll talk about the way in which these
technologies can be used, concentrating just on the applications of interest,
so the best way to use this technology with the reference to the possibility of
minimizing the possible sources of errors in all our journey. Every station of
the journey will be a source of error and what will be our interest is
understanding how is the best way to use this technology in order to limit as
much as possible all the errors that may come up by using this technologies and
what is the best practice in order to minimize them or to be conscious of them
and measure them.  In every station we put in our back-pack one little error
that we'll bring together and we need to keep the weight of the back-pack as
small as possible in order to arrive at the end of the journey with the
lightest bag possible. We need to explore this every time. we need at every
station to understand what's the weight I need to accept to carry by exiting
the station and going on in the journey because even if i do things right i
know that i can't cancel out completely all the possible sources of errors in
the stations that we will need.

## Slide 14

We'll talk about time resolved imaging, 4D-CT in particular. we were mentioning
respiration before as one of the major source of errors in our paradigm. 4D-CT
is 3D in time and it is a way of imaging patient in order to represent also the
motion and particularly respiratory motion affecting the structures that might
be of interests for our surgical procedure. 

In the slide we are talking about radiation oncology: this is a lung cancer
moving a lot during respiration. In order to make an appropriate treatment plan
we need to understand, visualize and measure the motion patterns of the lesion
because we need to take into account the motion of the lesion if we want to hit
it with radiation beam or even hit and reach this lesion with a biopsy needle.
This applies also for sub-diaphragmatic structures such as the liver. We will
understand where all the artifacts come from and study countermeasures for them.

## Slide 15

We will mention MRI. This functional and anatomical MRI is a reference
technology for what we need. Distortions included. We consider it because
especially in one of the surgeries we mentioned as a reference it is a
reference technology for describing and differenciating nicely soft tissues
especially in CNS.

## Slide 16

We'll just mention PET as a complementary source of information for describing
metabolic functions inside the patient. This is particularly used in oncology
in order to detect the size, the presence and the extension of an oncological
lesion in the patient. Here is shown a lung cancer for this patient. The use of
PET is very important when we want to segment properly this lesion in order to
include in the contour that we are defining when segmenting the lesion also the
microextension of the lesion that we need to take into account when we want to
remove or to irradiate this lesion. It's not what we see on a CT that needs to
be included in the target but also the microscopic infiltration of the lesion
that are not visible in CT but can be detected nicely, even if with a lower
spacial resolution, in the PET. So the fusion between CT and PET provides the
radiation oncologist with the real information of the extension of the
oncological lesion that needs to be removed. That enters as a reference
technology concerning metabolic functions and evaluation of the lesion in the
treatment or surgical plan definition.

## Slide 17

Ultrasounds: some specific technology to be used inside the surgical room. MRI,
CT and PET are used in pre-operative phase. There are other imaging
technologies that are used only in intra-operative phase (ultrasound is an
example). We know all the problems this technique has in terms of the quality
of image but on the other hand it is very handy and it can be used during the
surgical procedure to acquire the information and to compare this information
with corresponding images coming from the treatment plan.

## Slide 18

We will talk for sure about registration in the largest term that we need. It
needs to be considered as a superimposition of information. Registration is
comparing and matching information. This comparison can be used in order to
superimpose properly or to detect deviation, differences between the two
datasets that we want to register in order to undertake actions according to
the results to that registration procedure.

## Slide 19

A slide on planning: it's a very wide application and will be always present in
our paradigm. it will be very different according to the specific surgery that
we are dealing with. Planning a neurosurgical intervention will be really
different from planning a radiaiton oncology intervention or an orthopedic
intervention for hip prosthesis implantations but for sure every time the most
of the parameters that will constitute together a treatment plan or a surgical
procedure will be mostly geometric (Position of points, position of features,
trajectories of intervention, important anatomical references, angles and stuff
like that). Everything will be done in 3D (stereotactical approach.
Stereotaxis means a three dimensional attitude). Nowadays no CAS paradigm is
developed in 2D but always in 3D from the technologies of imaging we use,
ending up with the final information for guidance provided to the surgeon. In
the slide, there's a mention to one of our difficulties: the hypothesis that
the surgical plan after all the efforts that are produced during the
pre-operative phase is representative of the intra-operative reality and this
is not true. We can't pretend that this is the case. The patient will always be
different from what we see in the model that was built on the image we had
acquired with respect to the real situation of the patient after some days
maybe inside the surgical room or therapy bank.  We need to be conscious of the
fact that all the workflow will be built on this hypothesis and there will be
already difficulties in order to be as accurate as possible even if we could
consider this hypothesis as real. The hypothesis we always know from the very
beginning is that it's not real and we have to do our best, even if considering
valid the hypothesis of representativeness of the surgical plan of the reality
of the patient inside the surgical room but then we'll have to
be conscious of the fact that we'll have another problem on top of it:
measuring the size of deviations that occurred in the patient and may occur in
the patient during the surgical procedure and have for sure occurred in the
patient between the time of images acquisition for surgical planning and the
reality of the patient during surgical procedure.

## Slide 20

A mention to registration: there are various methods to do it:

1. Corresponding point-based.
2. Non corresponding point-based (surface matching).
3. Image based.

If we have a plan and we have navigation we'll always have to have a
registration procedure. That is the element that, according to the agreement,
needs to be always present in order to talk about the CAS paradigm.

## Slide 21

We'll talk for sure about intra-operative localization based on optical
tracking systems, that are nowadays the reference technologies for
intra-operative localization of tools, of the pointer and the patient as we saw
in the video. We'll discover how we can calibrate them, what we have to pay
attention to in order to allow the systems to acquire the most accurate as
possible measurements that they need to take during their application during
the surgical procedure. Every localization made by this kind of system of one
marker will be affected by an error because it's a measurement and we need to
know the size of the error. We know the intrinsic accuracy of this system and
their accuracy at the time of use. This will be another little "rock" to put in
the back-pack that will increase the level of inaccuracies, potentially very
much but we need to know how much.

## Slide 22

Another slide on registration. Just to mention: the results of the registration
are really related to the possibility to perform navigation.

## Slide 23

Image based registration: nowadays they use a lot of technologies based on
images: C-arm x-ray systems, intra-operative CTs, non conventional imaging
methods using industrial robots, non conventional systems using oblique
projections and implementing the so-called 2D-3D registration which we'll talk
about or volumetric registration of images when we want to compare 3D
volumetric imaging dataset with other volumetric dataset that can be acquired
during the surgical procedure by intrinsecal 3D methods of imaging.

## Slide 24

We'll talk about organ motion. When we arrived at the end of the journey
considering the hp of rigidity of the patient or of stability of the
geometrical configuration of the patient between the pre-operative and
intra-operative phases we need to worry about the fact that there will always
be disparities between pre-operative and intra-operative phase or even within
intra-operative phase and these disparities falsify the hypothesis that the
pre-operative plan is representative of the anatomo-pathological reality of the
patient during the intervention. Again here we can't do in many cases anything
to eliminate the disparities. We need to measure them, verify and adapt.
According to the information that we may be able to acquire during the surgical
procedure in order to understand the size of this disparities and to adapt the
plan to the reality that we discovered during the surgical procedure or at the
beginning of the surgical procedure.  The acquisition of the information at the
beginning of the surgical procedure or during the surgical procedure will allow
us to understand the size of the disparity and to take decisions: whether to
interrupt the procedure or take into account the size of the disparity and
adapt the surgical action according to the size of this disparity. This is the
current challenge of people working in CAS: trying to put in practise and to
propose to surgical community the methods able to quantify accurately the size
of the disparity and to propose adaptation strategies of the surgical plan
according to the size of the disparities which have been measured. Because the
way that the surgical plan can be adapted, according to the measurement. It is
easier to measurement of the disparity, by imaging or whatever. it's not that
easy and straight forward to convert the measurement of the disparities being
occurred and being measured into adaptive strategies and how we can adapt
treatment or surgical plan according to the disparity that i have just measured
on the patient already under anhestesia, already positioned under the linear
accelerator for radiation oncology in order to take into account of the
disparities. This is an adaptation that i need to do online and immediately and
with expectation of the result of this adaptation in terms of restoring the
geometrical quality of the surgical plan or of the radiotherapy plan. This is a
hot topic in clinically applied research in the field of CAS.

## Slide 25

List of definition and terminology to which we agree. <Read> 

*Stereotaxis*: often we find this terms in papers. it means based on 3D
information. Like stereo information. This is always the case.

There are other information: robot, registration and so on.

## Slide 26

- *Segmentation*: a critical topic and a very big potential source of
  inaccuracy when performing the treatment or surgical plan.
- *Rendering*: if we want to provide the surgeon with the proper tools to
  perform the proper surgical planningg we need to let them play with nice
  representation and this is our business. We need to allow them to be familiar
  and navigate in silico inside the anatomical structure of the patient, to
  take decisions, to establish trajectors, to optimize as much as possible the
  surgical plan definition.
- *IGS*: Image guided surgery
- *IGRT*: Image guided radiotherapy

## Slide 27

Starting from the discovery of x-ray that has started the technological
evolution towards our days in which we make extensive use x-ray 3D technology,
passing through the invention of CT and then going on with the 1980 in which
the first application of surgical robotics has appeared as pioneer experiments
inside the surgical rooms around the world. Technologies we mentioned and
methodologies such as neuronavigation are there since 30 years since their
first appeareance but technological evolution is still to come though with
particular reference to the organ motion phenomena that we mentioned some
slides before.

## Slide 28

Another omnicomprehensive pic of technical element of a system.

## Slide 29

The reference surgeries just to recall what are the reference surgeries in
terms of extensive use of CAS technologies. Neurosurgery. Orthopedic surgery or
spinal surgery and Radiotherapy. There are other surgeries that benefit of
technology that we can refer to CAS paradigm.

## Slide 30

Here there are some topics that might be referred to CAS paradigm but we don't
have time to go through.

## References

Slides: 1

[Youtube video](https://www.youtube.com/watch?v=_BFTK6LWH5g)
