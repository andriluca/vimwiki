# Lecture 2 --- Classification of imaging devices

## Slide 1

We are starting our journey into the process that starts from the pre-operative
phase. We need to understand and assess technical specification of the
different methodologies and technologies that are used to acquire
patient-specific data that will be used to build a patient-specific model on
which the surgical plan will be defined. When we evaluate the different
technologies what we will be intrested into in understanding is the specific
characteristics of these technologies related to the use that we need to do
with this technology and the use of it is the creation of a geometrical
patient-specific model on which we will have to make measurements, to define the
location and configuration of structures that are to avoid or respect during
the surgical procedure. 

Our main worry will be to understand if this or that technology used in this or
that way can be a source of inaccuracies, because, due to the fact that we need
to take measurements, we know that we will make errors in taking this
measurement and we need to assess and understand the size of these inaccuracies.
i.e. there could be distortions that MRI can bring along and if these distortions
can be considered not important during the diagnostic phase (because the
radiologist doesn't care much about the presence of artifacts in the images
when he has to define the presence of the lesion or whatever pathology by
examining the imaging dataset) now, on the other hand, when this imaging
dataset needs to be used to create a surgical plan then distortion must be
considered as a source of inaccuracies because the artifacts that will be
always present in whatever imaging technology we will use will affect, in the
end, the level of accuracy with which we are able to define the measures, the
characteristics, the features of the surgical plan that we need to define in
this phase. 

So we will focus on this phase on which the imaging characteristics and the
imaging dataset we will be using play the most important role in terms of data
on which we will be working on. In pre-operative phase we don't only have only
the imaging dataset on which we work but potentially other data, particularly
there will be a whole set of activities concerning planning, simulation and
personalization that we should read in this slide as the phase of the surgical
planning, in which the surgeon or the operator sits in front of a computer
screen and works on the software designed to allow to define the surgical plan.

## Slide 2

We have to keep in mind, when we examine the different technologies for imaging
we have available in pre-operative data phase, that we must have our personal
assessment criteria: we are not interested in what specific technology does but
in the listed characteristed:

1. If the technology used mainly in pre-operative phase or if it can be used
   also during the surgical procedure (intra-operative phase) in order to
   acquire the information that we will be in any case required to acquire
   during the surgical procedure to compare what was planned with what is going
   on during the surgical procedure. We will see that even the most complex
   imaging technology mainly devoted to be used in pre-operative phase, can
   find also applications in intra-operative phase (i.e. MRI that normally is
   used for acqiuring imaging dataset on which we will define the surgical plan
   in the preoperative phase but there are examples of intra-operative MRI
   scanners designed to be installed within the surgical room and used to
   acquire MRI images on the patient during the surgical procedure). The more
   complex the technology the less used it is in the intra-operative phase.
   Indeed, MRI scan for intra-operative acquisition are very rare because of
   the cost and the fact that it forces to design all the surgical room in
   function of the presence of this technology within the room (paramagnetic
   material only and so on). CT is a little bit better to use in the
   intra-operative phase, though all the radioprotection issues need to be
   taken into consideration. There are other technologies which are mainly
   devoted for intra-operative phase: ultrasound imaging above all. One of the
   first characteristic we need to define when we are looking at a specific
   technology for selection or evaluation or whatever is the use of this or
   that technology within the framework of our interest, CAS in our case,
   articulated in the two phases. So it's important to make a classification in
   terms of application of the imaging technique.
2. *Dimensionality of the information* that the technology is providing: we
   need immediately to understand whether the nature of the information in
   terms of dimensions that this or that technology is providing is
   two-dimensional, 2D+time, 3D or 3D+time (aka 4D). Some examples: x-ray
   projection, conventionally we have to imagine a digital version of a
   conventional x-ray radiography, is providing a 2D information: it's the
   result of the attenuation of the x-ray beams within the human body that will
   excite the detector that is placed at the end of the pathway of the
   radiation beam and this excitation will, in turn, give rise to a gray level
   map representing the attenuation pixel by pixel that was encountered by the
   radiation beam by crossing the human body and it will then finally represent
   a map of attenuation. This map of attenuation is bidimensional, so it is
   intrinsically providing two dimensional information. When we use the same
   x-ray projection continuously in time (aka Fluoroscopy): we leave on the
   x-ray tube, we let x-ray beam cross the patient throughout a certain amount
   of time and what we will get is 2D information concerning the geometry (it
   will always be a map of attenuation) but it would be a sequence of maps of
   attenuation in time, with a certain temporal resolution up to 10-15 Hz. In
   this case we will talk about a dimensionality of information that is 2D+time
   (i.e. Fluoroscopy is used for following the motion of whatever structure we
   are interested into, understanding the size ofthe motion, the incertainties
   that this motion can provide during the surgical plan definition. Then
   there's a whole set of technologies for imaging that provides intrinsically
   a 3D information (the technologies that are mainly of our interest): to this
   category belong all the technologies that we all know and represent a
   reference for acquiring the imaging dataset on which the surgical plan will
   be defined. If we are talking about x-ray it's the CT, providing a gray
   level volume representing ultimately the physical density of the tissue
   contained in each volumetric element (aka voxel). MRI also belongs to this
   category: anatomical (t1, t2) or proton density MRI.  For all these
   technologies there exist also a version that is 4D, that will provide not
   only one single complete volume of the region of interest of the patient
   that we want to acquire and to model but a sequence of volumes bringing
   along the dynamics and kinematics of any each motion that might influence
   the configuration of the volume of interest we want to image and this will
   be very important as for fluoroscopy, even more in this case because it will
   be a sequence of three dimensional information distributed in time with a
   specific temporal resolution that will be able to display to the operator
   the size in 3D of the motion of whatever structure that is involved somehow
   in the surgical plan definition, in this case we will talk about 4D-CT and
   4D-MRI technologies that are typically applied to measure the size of the
   motion of structures due to respiratory motion. We will talk diffusely about
   4D techniques because it's one of our major interest as well as one of the
   major sources of inaccuracies from a geometrical point of view when we want
   to deal with deformable and moving structures due to specific physiological
   motion, as respiration is, that is not something we can suppress very
   easily.
3. set of criteria:
	- *coding of intensity level*: how many gray levels we have available
	  to code the information that is attributed to each elementary element
	  of the imaging (pixel if we are talking about 2D images or voxel if
	  we are talking about 3D technologies). In other terms: how many bits
	  we have available to code the gray level. the higher the number of
	  bits the higher the resolution with which we can represent the
	  information associated to that specific map that is the image.
	- *Spacial resolution*: influences a lot the accuracy of the
	  measurement that we want to make. It gives us the sampling power of
	  the imaging technology on the volume or on the area of interest of
	  our patient.  If we have a big bore, a big gantry of a CT with lots
	  of pixels with which we can define the spacial resolution of the
	  slices that i can drag out from the initial gray level volume, the
	  higher will be the accuracy of the information I have sampled on my
	  patient. If we have a voxel (elementary element of a 3D imaging
	  technology) that is big ($`1cm^3`$) then there will be a lot of
	  material inside this element, while if we reduce the size of the
	  elementary element ($`1mm^3`$) the information will be highly
	  resoluted with respect to the former case.
	- *SNR*: the higher the quality of the images (with less presence of
	  artifacts), the more accurate will be the measurements. If the
	  picture is noisy when i will have to take measurement for surgical
	  planning (identifying specific locations of structures) i will be
	  less accurate than dealing with images with a higher SNR.
	- *Distortions*: another source of errors in taking the measurement.
	- ... other items that affects the patient's geometrical representation
	  that are not reported. Every time the approach should be that we need
	  to add items that in some way influence the geometrical accuracy of
	  the patient-specific model.  The higher the accuracy the safer I am
	  when defining the numerical parameters of a surgical plan. The less
	  accurate the information the higher the errors and again if we think
	  of the backpack on the back we will have to add rocks representing
	  inaccuracy that i bring along since the very beginning (since we have
	  just started to acquire information on the patient on which anything
	  is based on).

## Slide 3

In this excursus on the imaging technologies we will be looking at the
technologies seen represented here:

- x-ray imaging. This is still, conventionally and also traditionally,
  representing the main category of imaging technology for surgical planning.
  This because everything starts from the radiologists. They were used to start
  studying imaging using x-ray technologies. There's still a lot of
  conservatory attitudes by radiologist to use preferably x-ray-based
  technologies with respect to others. The reason of this has also other
  implications: economically a CT costs less than MRI and PET. It's still the
  most economical way to acquire 3D information without spending too much
  money. Things are changing though and MRI is becoming more and more
  competitive both economically but also in terms of capability and geometrical
  accuracies and soft tissues differenciation. So there's a competition between
  x-ray-based and MRI-based imaging technologies.
- MRI: it has a lot of development, especially related to unconventional or
  advance imaging techniques like diffusion-weighted, perfusion-weighted and so
  on because MRI is able with one unique unit to provide not only anatomical
  information but also, through advanced sequence of images other information
  that may in turn be very important also for the patient-specific
  characterization and support the surgical plan.
- PET: an important imaging technology associated to nuclear medicine
  especially in oncology field.
- Ultrasonography as the kind of technology for imaging which finds particular
  application during the intra-operative phase because it can be easily used in
  the surgical theater much more than other technologies technologies which are
  more complex to be installed within a surgical room.

## Slide 4

Let's start from x-ray imaging. We don't have to forget that x-rays were
discovered by Röntgen at the end of the 19th century (1895) by chance. He was
just making electrical experiments on ionizing currents and he discovered by
chance the presence of these rays that called x-rays because he didn't know
what they were, a sort of "unknown" manifestation of energy which he did not
know. The very interesting thing is that Röntgen and collaboration very soon
found out the medical application of this new energy that they have discovered
by chance and, in fact, after very few years from the very pioneering
experiments of Röntgen looking to understand what was this strange phenomenon
that was lighting up iron structures in his laboratory he found very quickly
the possibility to apply this new discovery to look inside the human body. This
photograph reported on the right is showing the hand of his wife with the ring
that is the first official document of the usage of x-ray for medical
application. We can't see x-ray because located in the spectrum of the
electromagnetic radiations with a high frequency (so small $`\lambda`$) and can
penetrate easily the human body being on the other hand attenuated in different
ways by the different natures of tissues that are present inside the human (or
the high level developed living organisms') body. These categories that can be
visualized on conventional x-ray projections are mainly the bones (behaving
like rocks inside our bodies), soft tissues (assimilated to water) and air.

## Slide 5

The machine that produces the x-ray. We recall all the elements of the x-ray
tube, aka Coolidge tube or Rontgen tube. The way in which the x-ray tube is
designed is still very important. We need to recall the fact that in order to
produce x-ray we need to have a flux of high energy electrons colliding against
a target with a very high atomic number and typically this material is Tungsten
(aka wolfram). In order to produce this collision that in the end produces the
x-ray exiting from the tube we need to excite, by applying a convenient
potential difference between a cathode and anode. The cathode is a filament in
which we produce a cloud of electrons by termoionic excitation and the anode
will be our high atomic number element. Between the two elements (anode and
cathode) we apply a potential difference of many kV. We talk about tens of kV
in order to obtain the x-rays with the proper features designed to penetrate
conveniently the human body. The anode is typically put in rotation because the
collision between electrons and atoms of the anode produces a lot of heat, so
by putting the anode in rotation we distribute the heat on the whole target
delaying the consumption of of the material of the anode which, at a certain
point needs to be replaced and, according to what we need to have, every
element needs to be put inside the tube in which we have the vacuum. We don't
want any molecule of air or foreign molecule inside the vacuum tube in which
this physical process takes place. 

An important thing to remember is that the efficiency of the x-ray tube is very
low, so we lose a lot of the energy that we apply in the form of potential
difference in many different processes, namely heating processes. The
efficiency is proportional to the potential difference applied and the atomic
number of the anode but we always have in modern machine to cool down the
elements that are present in the vacuum tube. We can't let the thing going on
without worring of cooling down all the apparatus, otherwise everything will
burn out.

Another important thing is the relationship between the potential difference
applied to this simple design and the energy of the x-ray beam that we obtain.
When we are talking about the potential difference applied to the tube we talk
about, as we see, 50-70-80 kV. There's a relationship, that is very intrinsic
of this representation, between voltage and x-ray beam energy, once filtered
cutting out the tails of the spectrum of energy that we obtain. The energy of
x-ray beams is expressed in terms of eV. Conventionally, referring to this
specific simple design of the x-ray tube we say that if we apply 100kV as
potential difference to cathode and anode of the simple design, what we will
obtain in the end is an x-ray beam with average energy of the spectrum of 100
keV. This relationship even if it's not perfect from the theoretical point of
view it is still valid from an engineering point of view. We stay on the
existance of this relationship between the potential difference applied to the
simple design of the x-ray tube and the energy that we obtain of the x-ray
beam. We all know that the x-ray beams produced will not possess all that
specific energy and we will have a wider spectrum of energy of the produced x-ray
beam but if we go to the average energy obtained by conveniently filtering
through Aluminium filters that produce x-ray beams what we will obtain is an average
energy of this x-ray beam that is related to the potential difference that we
applied to the simple design of x-ray tube.

## Slide 6

Here we can see a picture of the tube in which we can recognize the different
elements. We can see the anode motor that puts in rotation the anode, the
Tungsten rotating anode, the cathode filament. In red it is reported the
distance between anode and cathode on which it is applied the potential
difference. Here happens the big collision of electrons which are all densly
produced around the cathode filament against the Tungsten rotating anode. What
we will obtain is a x-ray beam that comes out from the exit window of the x-ray
tube and ready to be filtered both physically (cutting out the low energy and
high energy photons) and also geometrically (to shape the radiation beam in a
convenient way before it enters into the patient and to the detectors). 

In the smaller picture we can see the consumption of the anode that are
typically seen on a rotating anode so it is very consumed. The collisions
between electrons and atoms of the anode consumes the anode and this is a part
of the tube that needs to be replaced in order to keep a proper functioning of
the tube.

## Slide 7

We assume that we have this strange apparatus with inside some elements that we
need to know. Tens of kV of difference of potential are transformed in tens of
eV of x-ray beam energy. These are typically the energies that are required to
penetrate the human body and the structures. We want to have some of the
radiation that exits the patient in order to impact with and excite the
detector in order to form the attenuation map which is the first result of
x-ray imaging. 

we are more interested in the phenomena that underlie the penetration of x-ray
beam inside the biological matter because these are the phenomena that are
ultimately responsible of the different attenuation of the xray beam when
encountering the different natures (bones, water and air) that are contained
inside the human body. These phenomena are three and are dependent, in terms of
frequency of the events, as a function of the energy of the x-ray beam. Some of
them will be dominant when the x-ray beam has low energy and some other will be
dominant when x-ray beam has higher energy. 

1. The first phenomenon we are considering is the *photo-electric effect*. It
   happens mostly when there's an interaction between low-energy x-ray photon
   with the atoms of the biological matter. The impinging photons doesn't have
   a lot of energy and the interaction produces an effect of expulsion of one
   of one of the external electrons in the orbital of the atom.  This expelled
   electron is a source of noise, something that we would like to avoid and
   limit as much as possible because this expelled electron is going somewhere
   and interact will the other atoms nearby. A lot of these phenomena are
   taking place at the same time and lots of electrons will be scattered around
   without a predefined direction ad will potentially reach our detector with a
   very strange direction, putting energy to the detectors which is not useful
   to form the attenuation map. Everytime that we see that the interaction
   between the impinging photon with the biological matter will cause scattered
   radiation (aka secondary radiation), this phenomena for us (who are very
   worried about accuracy of the maps we are building in 2, 3, 4D) will be
   considered as potentially decreasing the SNR of the attenuation map we want
   to form. The electron or scattered radiation can detrive the quality of the
   information contained in the image.

## Slide 8

2. Compton effect. The energy of the impinging photon is higher and the
   interaction of the impinging photon with the biological matter will cause a
   double scatter: this regards another photon and an electron. On top of the
   expelled electron there will be also another photon being produced with
   lower energy, potentially producing other interactions (maybe also
   photo-electric effect because the energy of the expelled photon is less, due
   to the first interaction). Again a lot of secondary scattered radiation is
   produced: an electron and a photon going around again and producing other
   interactions.  The higher is the energy the higher will be the amount of
   secondary radiation that we produce, the higher the detrimental effect of
   the secondary radiation for the definition of the information I'm looking
   for.

## Slide 9

3. Pair production. Very high energy photons. Typically it's not used for
   diagnostic x-ray imaging but a dominant effect when we have x-ray beams with
   energy in the order of MeV. Pair production means that the interaction gives
   rise to the expulsion of one positron and one electron from the atoms of the
   biological matter. These two elements (one the anti-material of the other)
   will interact immediately producing a gamma photon. The combination causes
   the production of a high energy photon in the order of 512 keV. This photon
   will cross a lot of other biological material and produce a lot of other
   interactions and a lot of other secondary radiation and a lot of noise on
   our images. Luckily enough this phenomenon is not dominant for the x-ray
   energies that are used for diagnostic images but still, due to the fact that
   we want to filter out this high energy photons we will have some of these
   interactions that are very detrimental for imaging because they cause a lot
   of secondary radiation.  
   
Again we look everything, not only the technology but also the phenomena which
are responsible for producing the information that we want with an attitude of
understanding the source of inaccuracies.

## Slide 10

We can see in this graph the dominance of the three phenomena that we just
analyzed as a function of the energy of the impinging photons, here expressed
in MeV. For the pair production the percentage of interaction of phenomena is
starting when the impinging photons have an energy higher than 1 MeV, much
higher than the 50-60-80 keV used for diagnostic x-ray imaging. The compton
effect which, in turns, in any case produces a lot of secondary radiation so
the compton photon and compton electron is really dominant in the level of
energies we are using for diagnostic imaging along with photo-electric effect.
Our source of noise in conventional x-ray imaging will be the secondary
electrons and photon produced by the two dominant effects in this range of
energies that are photo-electric and compton effects. We need to take counter
measures to avoid that the secondary radiation will reach our detector because
the secondary radiation is not bringing any information concerning the
attenuation of the radiation beam because the direction with which this
secondary radiation will eventually reach the detector is unknown and
unpredictable so it just causes noise to our image. We need to find out a way
to limit as much as possible the influence of the secondary radiation on the
excitement of our detector put at the end of the measurement chain. Being
conscious in any case that this is the price we have to pay from an ethical
point of view also for the patient because the secondary radiation will go
around in the body of the patient causing interactions, ionization (every time
an electron is expelled from an atom, the atom becomes positive --> ionization)
and every ionization phenomenon affecting the atoms of the biological matter is
potentially dangerous because it may affect the DNA of cells and potentially
bring along aberration phenomena of DNA of cells which might go into new nature
(cancerous cells or whatever). All undesired ionization phenomena in the human
body that makes x-rays dangerous for the human living system are depending
explicitly and uniquely by the secondary radiation produced by the interaction
of the photons with biological matter. The secondary radiation is not only
detrimental for what we want to do (making a good attenuation map with high
quality in terms of SNR) but also dangerous because it's the dangerous thing of
the x-ray beams crossing the human body because they cause ionization
phenomena, a lot of them, it's like an exponential multiplication of these
ionization phenomena that may in turn be dangerous for potential modification
that they may cause inside DNA of the cells.

## Slide 11

The attenuation of x-ray crossing the human body happens according to the
Lambert-Beer Law: the intensity (energy through the unit of time and surface)
of x-ray beam at a certain depth inside a material, i.e. water that is the
reference for soft tissue for human body, will be given by the initial
intensity of the single photon beam impinging on that material multiplied by
this negative exponential expression in which we find as exponent the depth at
which we are, the deeper the more important will be the attenuation that the
x-ray beam has received multiplied by a couple of coefficients that might be
grouped together in a unique coefficient that express the physical density of
the tissue (the higher is the density the higher the attenuation of the
photons) multiplied by the coefficient of attenuation (that depends upon the
specific material).

Sometimes these two element are grouped together in a unique absorption
coefficient	but inside this negative exponent in Lambert-Beer Law we find
physical characteristics of the tissue of the material which is crossed by the
radiation beam that is the physical density and the properties related to the
physical density of absorption. This tells us that when an x-ray beam crosses
water, it will be attenuated in a very specific way due to the physical density
of an absorption capability of water, if it crosses bones it will be attenuated
much higher, if it crosses air it will be attenuated less. 

The absorption coefficient and the nature of the material that are present in
the human body are steering factors in order to define the best energy that we
need to apply to our x-ray beams when we want to perform x-ray imaging, because
ideally we want a beam that possesses enough energy to cross the whole section
of the patient through which it is shooted, capable of crossing the entire
human body and exiting the body with a certain amount of remaining intensity,
despite the attenuation, and reach the detector in order to excite it. We need
to do that even without knowing exactly from the very beginning what will be
encountered and met through the crossing when crossing the human body so we
need to find out the right tradeoff in terms of energy to assign to x-ray beams
in order to have them crossing the whole patient and reach with certain
remaining energy differently attenuated as a function of the specific sections
of the beam and the specific material that each section of the beam has
encountered during its pathway through the patient in order to give rise to a
proper attenuation map that is exactly what we want to obtain, possibly
reducing as much as possible the presence of noise.

### Note

Steering: in it. che guidano.

## Slide 12

*This graph* represents the idea that steers the fact that the diagnostic
imaging energy in terms of radiation beams is exactly around 50-70 keV that is
applying on our x-ray tube 50-70 kV of potential difference. It's true that the
absorption coefficient for each specific material depends upon the energy of
the xray beam. The physical density remains always the same in the Lambert-Beer
Law but the other coefficient (absorption coefficient) depends upon the energy
of the x-ray beams and varies as a function of x-ray beam energy. What we want
to obtain is to apply and use an energy of x-ray beams so that the absorpion
coefficients, which vary as a function of x-ray beams energy, are as much as
possible far apart from each other to obtain the best absoption differences as
a function of a specific material that it is encountered. If we would apply a
very high energy the differenciation between soft tissues and bone will be the
same: it is so energetic that does not feel any difference between the case in
which it crosses bones or it crosses water and that's not what we want if the
objective is the information concerning what is present inside the human body
crossed by the x-ray beam. We want a differential absorption between the
different structures so no way that we will use 10 MeV to perform diagnostic
imaging. On the other hand if we go a little bit less in terms of energy we
have a point in which the two absorption coefficients dependency upon the x-ray
beam energy are very much apart from each other (considering the two curves)
and that's exactly the energy point that we want to use in order to obtain the
best differenciation in terms of absorption of the x-ray photons crossing
different materials. That's why we go to use typically an energy that is in the
range mentioned before.

## Slide 13

A very schematic representation of the measurement chain for conventional x-ray
imaging. 

- *X-ray tube*
- A first filter which is a physical filter to cut out the low energy photons
  that enters the human body and stop inside: they don't have enough energy to
  cross the whole human body and arrive to the detector with a specific
  attenuation. This low energy photon are unuseful, detrimental and source of
  secondary radiation (source of inaccuracy) because they will stop after a lot
  of interactions with biological matter causing secondary radiation. We are
  eliminating them with the *Aluminium filter*.
- *Collimator*: A geometrical filter. It is constituted by two blades of heavy
  and high atomic number material (leads). It is able to stop completely x-ray
  photons beams. Only the photons passing through the hole are kept. We are
  using this to conveniently shape (typically in a conic or prismatic
  morphology) the radiation beam. Now the beam will enter the patient
  possessing only the photons having enough energy to cross the entire human
  body and shaped in a proper way to invest the patient in corrispondence of
  the ROI we want to image.
- *Primary radiation*: it's the useful source of information for us. The photon
  entering the patient are undergoing attentuation as a function of the
  material that they have encountered and capable of possessing still enough
  residual energy to exit the human body and reach the detector. They will have
  a characteristic of exiting the patient bringing along useful information:
  the lower the residual energy the lower the level of excitement they will
  give to the exciting elements of the detector, telling us that the photon has
  encountered a lot of bones with respect to another photon in another section
  of the beam that might have encountered just air or water.
- Secondary radiation: source of noise. In order to avoid it we exploit the
  fact that the direction of exiting of this secondary radiation will not be
  the main direction of the xray radiation beam so immediately before the
  detector we will always find a grid with channels conveniently oriented in
  order to represent the main directions of the different beams in which we can
  decompose the entire x-ray radiation beams so that the only or mainly primary
  radiation will be able to get in and cross this grid, stopping as much as
  possible the secondary radiation which will exit the patient with strange
  directions. It's a final filtering of the useful information with respect to
  the noise source brought along by the secondary radiation in order to limit
  the possibility that the secondary radiation hits the detector.
- Detector: film in the past.
- Screens: fluoroscopic screens intensify the possibility of excite the
  crystals that are on the film in order to incresase the intensity and quality
  of the image. This is not the case for the digital radiography.

So again the whole chain of measurement must be considered recalling the
elements that are useful to increase SNR. Enemy is secondary radiation:

1. For the patient: it is dangerous
2. For the SNR of the attenuation map we want to form

## Slide 14

X-ray imaging with dimensional characteristics of 2D and 2D + time are very
much used, still nowadays, in the intra-operative phase. In every surgical
theater of the surgeries that are a reference for this course (neurosurgery,
radiation oncology, orthopedic surgery) we will always find x-ray system that
is able to acquire x-ray projection with a 2D (or 2D+time) level of
information. These are the x-ray system that are used to acquire information on
the patient during the surgical procedure. Think about any intervention of
ortopedic surgery where we have to deal with bones a lot: x-ray imaging intra
operatively is a standard. This will be our main source of information
collected intraoperatively, registered somehow with coresponding data coming
from the surgical plan in order to compare what is going on during surgical
procedure with respect to what was planned and represented in the surgical
plan. This kind of apparatus is called "C arm" system because they always
features a sort of C designed of this arm mounted on a support which might
feature certain degrees of freedom of motion (typically a rotation around the
central axis of the C), at the extremities of the C we have the tube and the
detector (respectively to the bottom and to the top of the image reported in
the slide). There used to be analog detectors (aka image intensifiers) but now
they are completeley replaced by digital detectors (digital flat panel) that
are faster, they last more and provide higher quality image with respect to
analog detectors. They are handy because they have wheels and they can be put
at the sides of the bed of the patient and oriented conveniently by exploiting
the rotational motion of the C arm around the patient in order to select the
proper direction of imaging as function of specific interest of the surgeons
and of the typology of the information that we want to acquire and they are put
in operation with all the tricks which are brought along by the fact that we
are doing x-ray imaging inside the surgical room for the protection of the
operators. In fact the usage of x-ray imaging in surgical rooms brings along
some problems that need to be considered to ensure a proper protection of the
operator against the secondary radiation during the x-ray imaging

## Slide 15

When we talk about flat panels we see the dominant technology of Digital flat
panels implemented in commercial systems: the impinging x-ray will excite a
matrix of scintillating crystals producing and converting x-ray beams impinging
on the detector into visible light with an intensity which is dependent by the
energy (so the intencity) of the impinging x-ray. This visible light will
excite a matrix of amorphus silicon photodiodes (elements capable of converting
visible light energy into electric energy). This energy will be then
represented through a conventional matrix for video images creation (TFT
array) and we will obtaining the digital image, the digital array, with number
representing for each pixel of the image the gray level of that specific pixel
that can be conveniently represented on displays. There are other technologies
which might be implemented into the flat panel digital detector for x-ray
imaging on modern C arms: 

- direct conversion: not used because of the price
- indirect conversion: using a CCD (optical camera) that is not used or
  abandoned because it contains a lower accuracy of the conversion.

The dominant conversion technology is the one we see here in red: indirect
conversion using not optical component but matrix of amorphus silicon photodiodes.

## Slide 16

Here is a representation of a commercial flat panel detector. There's a cover.
The amorphus silicon matrix and the crystals are embedded in various different
layers below the cover. 4030 means that the dimension of the detector is 40cm
per 30cm (the dimension of FOV). In the slide is also reported an example of
the circuit responsible for the conversion.

## Slide 17

Concerning flat panel detectors. Let's see some characteristics of them.  It's
very important to understand:
- their dimensions: 397mm wide 298 hight.
- Number of pixels which gives the spacial resolution of the image that we are
  able to collect from this kind of detectors: 2048x1900 pixels (a very nice
  spatial resolution). A reduced physical dimension of the pixel that can be
  obtained by dividing each of the dimension by the number of pixel in that
  dimension: $`\frac{397}{2048}`$ and $`\frac{298}{1536}`$. 
  
These are very performing system for building up x-ray projection. They are
very fast and accurate in terms of spacial resolution and very expensive (4030
digital amorphus silicon detector is 70k euros). They represent the high level
standard for x-ray imaging on portable devices which are very useful for
obtaining images during intra operative-phase

## Slide 18

This an example of the image quality that we can obtain with the 4030 amorphus
silicon detector. It has a nice spacial resolution but if we look at small
portion of the image of the phantom we see something like in the reported slide.
We don't really like it: there's not a high gradient in terms of gray level
going from a pixel belonging to the phantom and the one belonging to air. We
have a whole slow slope of changing of gray level according to the different
sections and this is of course noise because if we want to make a segmentation
of structure on this image we run in higher trouble because of this slow
gradient of gray level passing from one structure to the other. 

So high level technology but still posing problems in accuracies,
uncertainties: when segmenting we are having inaccuracies in placing the
contour of the structures that may vary of 1-2 pixels (for example 1mm/pixel
error). We have to be conscious of that. Couple of mm is the level of
inaccuracy we bring along the journey, the little rock we have to put in our
backpack as a burden to bring along our journey.

## Slide 19

Here's an example of fluoroscopy. We use the same flat panel for digital
imaging applied in a fluoroscopy method. Fluoroscopy is a technique that leaves
the tube switched on all the time (continuous emission of x-ray beam) and a
continuous readout of the detector with a proper temporal resolution that can
be up to 25 Hz. The result is a 2D+time information that is used to track the
motion of whatever structure inside the human body that we might be interested
into. in this case We see tracked properly and automatically radiopaque clips.
Little repere that are inserted surgically in proximity of the lesion or the
target we want to reach with bisturi, biopsy needle or radiation beam.  We
can't see directly the lesion because it's soft tissue on soft tissue and we
don't have enough differenciation power in conventional radiography to
discriminate the target from the surrounding healthy tissue so we need to
implant clips surgically around the lesion and assess the size of the motion in
2D of this lesion by applying a fluoroscopic methods of detection. The clips
are used as surrogates of the position of the lesion so by detecting them we
can in turn detect the position of the lesion. Fluoroscopic guidance is
something that we put in practice in order to be ready to get to the final
target we want to reach.

Problems of fluoroscopy: x-ray dose with which we expose patient and operators.
A continuous exposure increases the secondary radiation production that is
harmful for patient and operators. Application of this kind of methods for
tracking targets considering the motion of the patients needs to be applied
with attention, as a result of the tradeoff between what is the advantage to
have this kind of methodology applied with the potential harm we can cause to
the patient.

## Slide 20

3D x-ray imaging: the reference technology here is CT. Computer axial
tomography due to the axial design that features the CT scanner. 

The basic architecture: patient at the middle of the gantry (structure of CT
scanner in which the x-ray tube, that we can assume to be the one of some
slides ago, and a matrix of detectors rotate around the patient). They rotate
around the patient with the x-ray tube always switched on with a very fast
speed so that they collect a map of attenuation of x-ray beam produced
conveniently collimated in the shape of a blade of a fan. At every rotation the
attenuation information is collected in corrispondence of thin region of
interest inside the patient and the data are so redundant that this information
can be converted in a 3D matrix of elemetary elements in which we can consider
condensed the information of attenuation (related to physical density) of ROI
that is scanned. After one rotation the patient needs to be moved in order to
expose to the following rotation another section of the patient with a
convenient thickness and the process starts again till we have examined, moving
the patient along his longitudinal axis, the whole ROI on which we are
interested.

## Slide 21

- *Traditionally* the acquisition was made step and shoot: the couch on which
  the patient is lying was moved in a predefined position, one rotation was
  performed, the information was collected and then the rotation was stopped,
  the couch was moved and another rotation was made. This technique has been
  overcome due to a lot of factors affecting the quality of the imaging
  (inertia of the tube and detector, inertia of the couch motion, length of the
  examination...). 

- *Nowadays* the CT scanners are all elical scanners: the couch is continuously
  moving inside the scanner and the rotation of the xray tube and matrix of
  detector is kept on all the time. This made the examination much faster and
  reduced to a minimum the inertial effect of starting and stopping the
  rotation of the tube and detector. The modern units are typically multislice:
  for every rotation i don't acquire information related to only one single
  slice of the patient (little portion of the patient) but i'm acquiring
  information in order to reconstruct multiple slices (6-8-12, up to 256).

## Slide 22

We can also decide in the modern units how many slices we want to reconstruct
for every rotation as a function of the spacial resolution (in axial direction)
we will obtain at the end of the examination. The higher the number of
contemporary slices we can acquire with a single rotation, the lower the
spacial resolution (the higher the size of the voxel in the axial direction).
If we decide to obtain a lot of slices we'll have 5mm thickness (related to the
slice, dimension of the voxel along the axial direction). If we decide to
reduce the number of slices we want to obtain with a single rotation we will
count on less slices reconstructed contemporarily but we will be able to count
on a better spacial resolution (reduced dimension of the voxel along the axial
direction). The number of slices and related slice thickness is one parameter
on which the operator can define at the time of acquiring the examination of
the patient. If we want to acquire on a patient a CT on which clinicians want
to perform a diagnosis (i.e. presence of the lesion, where the anatomical
variation causing the pathology is) we will not care about the spacial
resolution on the axial direction. The tradeoff in this case will be just to be
able to recognize the pathology and perform the diagnosis, on the other hand,
we have th dose to put on the patient to obtain that specific examination: the
higher the dimension of the voxel the lower the spacial resolution, the lower
the sampling on the patient and in the end the lower the radiation that patient
has received to create the final gray level volume. But if we're interested to
make measurements on the gray level volume, to locate where the lesion is and
what is the geometry of the lesion, to treat it, to reach it or remove it
surgically, to locate precisely where sensitive structures are placed with
respect to the lesion, then i can't accept 5mm spacial resolution on the axial
direction but we will have to go down to 3, 2, 1mm in order to sample as much
as possible my patient paying the price to expose him/her to higher dose but
gaining in terms of geometrical accuracy with which we will be able to describe
him/her more accurately. Spacial resolution we're using is again an uncertainty
that we'll bring along in the process. If we decide to use a 1mm spacial
resolution in axial direction this is the lowest margin of error we can count
on. We would want to have even higher resolution but we have to decide the
tradeoff between the way in which we sample the patient and the dose of
radiation that we will expose the patient to. This is something that needs to
be defined as a ratio between what we can gain and what we have to pay in terms
of geometrical accuracy with respect to the dose imposed to the patient.
Typically for surgical planning in delicate areas of the body (i.e. in
radiation oncology and in neurosurgery...) when CT is applied, the slice
thickness that is typically defined is around 1mm. Lower than this I cannot go.

## Slide 23

This concept is valid for CT and can be extended to other intrinsically 3D
technologies for imaging. 

When we look at a slice of  a CT on the computer screen, it is a 2D picture
characterized by different gray level of the pixels of the screen of the PC
that have been lighted up according to the map that was read by the software
that is producing the image on the PC screen. We can think of this observation
of CT slice as we were looking to a 3D information: every time we look at the
slice we are considering something which is intrinsically 3D even though
displayed in 2D because every pixel that we look at in our CT slice brings
along the information which is converted and communicated to us as a function
of the color with which the pixel is lighted up. This information is the
physical density of the tissue included in the voxel which is corrisponding to
that specific pixel: 

if we take a picture of the slice I'm looking I will see a certain gray level.
That gray level is displayed as a function of 3D information. This 3D
information is the physical density (or attenuation property) of the biological
matter which is contained in a 3D element (aka voxel) corresponding to that
pixel that we are looking at. The amount of material included in the voxel is
considered centered and condensed in the center of the object and the amount of
material condensed in the center of the voxel will be more, the bigger the
voxel dimension is. 

The bigger the voxel, the lower the spatial resolution, the higher the amount
of material contained in the voxel producing a unique information concerning
the physical density of that material sampled in a bigger element. So from now
on when we look at CT every pixel will be lighted up with a certain gray level
as a function of the physical density of all the material that was captured by
the CT examination and considered condensed in the center of the voxel. The
amount of material will be higher, the higher the dimension of the voxel. That
is why the CT and MRI are intrinsically 3D imaging (measurement) technologies.
What we will see by looking at a slice of a CT is a measurement of physical
density of the tissue inside my patient (expressed in 3D). 

We will never have the possibility to ignore the size of the voxel related to
each specific CT examination that we are using for performing surgical
planning. Size of the voxel, spacial resolution featured by that specific CT
examination is an information that is so relevant and important that we will
never have the possibility to neglect and always have to know it because it
establishes the lower limit (lower threshold) of our accuracy: better than that
dimension we cannot go. Typically the dimension of the voxel for surgical
planning purposes are the ones we see in this slide: 

- Slice thickness (from 1 to 3mm) a parameter to play around with. We can
  establish at the time of examination this parameter that is the depth along
  the axial direction of the voxel. 
- The other two dimensions (.98mm x .98mm) are called the physical dimension of
  the pixel. If i look at the CT slice i don't see the depth of the voxel, i
  can see the planar extension of the pixel on the plain of the slice that i'm
  looking at, that is a fixed dimension that depends upon the field of view of
  the CT and the spacial resolution with which the system is able to
  reconstruct the images. It is a physical feature with CT scan that we can't
  play with. Due to the fact that their value is lower than 1mm then they're
  not so critical because they are really small. 

The overall spacial resolution of our 3D map of the patient concerning the
physical dimension of the tissue are made by three dimension: thickness (i can
play with it), the other two are fixed properties of the design of the scanner
that I have to accept and to recall: even these dimensions I can't neglect
because they give the dimension of the voxel that is our sampling power in
space of the patient that i'm able to obtain. This dimension will fix the
lowest threshold of the accuracy with which I can go.  Every time I will
contour, segment structure in 3D passing through centers of voxels that
position of the contour will be passing through the center of the voxel but it
has an indetermination and a level of uncertainties that is the physical
dimension of the voxel and this again is another rock to put in the back (the
lowest limit under which i can not go). Many methods we will see in future (in
terms of accuracy of segmentation, accuracy of representation of this or that
property of the patient) will have as a reference to be compared to the
physical dimension of the voxel because we are all conscious of the fact that
if we have sampled our patient with that spacial resolution, better than the
spacial resolution of that imaging technique i have applied to the patient i
can not perform.

## Slide 24

Some terminology that will be encountered when talking about imaging and CT
imaging.  The primary information brought along by CT is the physical density
of the tessues in the voxel.  Due to historical reasons this information is
converted and represented in terms of the so-called *CT numbers* or Houndsfield
Units. Houndsfield was the inventor of CT.  The CT number, for questions of
normalization, are obtained by the coefficient of absorption (the physical
density of the material contained in the voxel) by the formula we can see in
the slide.  We will not talk about physical density of the materials but about
CT numbers.  When we have to semgent structures in CT and we need to establish
a threshold of voxels belonging to a structure (lungs) and voxel not belonging
to that structure (outside the lung) i will define these thresholds talking
about HU because of normalization.  In this representation we can see some
numbers attributed to specific tissues we can find in the human body. We have
to remember that in a CT we can have the potential to differentiate among
specific soft tissue structures.

- Air: -1000
- lungs: -950 (with air) to -550 (end of exalation)
- water: around 0
- bones: we go up in the scale
- fat or soft tissue which is very similar to water

We can notice that considering the scale of the first 80 HU, we are able to
differenciate many soft tissue structures, even though there are some
superimposition.

## Slide 25

Some example of visualization of CT slices. Let's focus the attention on a
pixel of one of this image on the screen. That pixel gray level is representing
a physical property, through the houndsfield number, of a determined physical
density of the material contained in the voxel which is corrispondent to that
pixel on which we are pointing the mouse.

The different representations we see on ths slide depend on the freedom that the
software designer has in terms of representation in window level and width of
window level: we can play around with the transfer function between the HU and
the range of HU represented in that specific slice in order to obtain more or
less contrasted images representation.

So by playing with these two values we can obtain:

- a better description of the structures inside the lung that will though lose
  any differentiation in terms of soft tissues in the remaining structures.  
- We can restrict the width of the window and increase the range of the level
  we want to represent: we lose everything that is inside the lung but we gain
  a lot in terms of compact bone and spongeous bone representation and soft
  tissue representation of structure being in the mediastinum.

## Slide 26

List of technical specification of CT:

- We use only helical CT right now.
- Acquisition time is .5-1s. this is important when we'll talk about 4D-CT.
- Multislice CT: varying from 4 to 256 slices obtained contemporary. The number
  of slices I can obtain contemporary depends upen the slice thickness that I
  want to define for the specific examination.
- Minutes are required for a complete examination so we can't implement about
  real time CT.
- Cost of the machine: around 500k euros
- Intra-operative unit: just very few. They exist but not very popular because
  they bring along lot of radioprotection issues: the production of secondary
  radiation, although the section of the beam is very thin, is notable and
  cause a lot of problems to operators. There will never be somebody in an
  operating room when an intra-operative CT is used on the patient (with all
  the consequences we can think of). There exist intra-bunker CT units in
  radiation oncology.
- Typical features: read the slide


## Slide 27


## Slide 28

Intra-operative CT that we can find in the most modern radiation oncology
bankers. 

This is one of the bankers for proton therapy at a center in Switzerland (PSI:
Paul Scherrer Institute) just to see another usage that CT can have
intra-operatively.  The idea is to have available a CT unit inside the therapy
bunker where the radiation beam of protons is directed on the patient in order
to acquire a CT on the patient immediately before the irradiation. For
radiation oncology a treatment plan is performed some days before the beginning
of the therapy using a CT acquisition. Using CT in radiation oncology is
mandatory because all the simulation of those depositions that would be carried
out during the treatment planning needed to have as an information physical
density map distributed conveniently in the patient otherwise we can't simulate
the way in which radiation therapy beam distributes the dose inside the
patient. CT in radiation oncology is the reference imaging technology for
treatment planning but every radiation oncologist is conscious on the fact that
from the time at which the CT for treatment planning is acquired to the time at
which the first irradiation of the therapy is delivered to the patient there
are a lot of uncertainties that can play around in the patient: there might be
sensible variations of the anatomical configuration of the structures which are
involved in the therapy, both the target of the radiation and the sensitive
structures around the target that I don't need to hit a lot with my radiation
beam. The idea in this case is to have available in the therapy room a CT unit
that i will be using immediately before the irradiation to acquire a complete
CT on the patient in order to have the reference of the same nature of the
information that i have for treatment planning to be compared one against the
other. The idea here is that i will be using the couch mounted on this robotic
arm where the patient is lying to place the patient with a robotic arm rotating
to place the patient in a specific imagin position and let the gantry of the
scanner slide on the rails in order to obtain the volumetric acquisition. In
this case it's not the couch that is sliding inside the fixed gantry but it's
the gantry that is sliding along the patient in a fixed imaging position (This
technique is called "CT on-rail"). The approach of image guidance is that in
this case i'm able to acquire a complete CT on the patient immediately before
the irradiation that is exaclty the same information on which I've planned the
therapy, so it's easier to compare in a monomodal fashion the same kind of
information on which I defined the therapy plan and that I have collected
immediately before the irradiation. The comparison of these two CTs has a name:
the detection of deviation in the configuration of the anatomy and of the
pthology of the patient that needs to be assessed by the radiation oncology in
time of irradiation in order to take a decision: is the therapy plan still
valid? Yes: let's procede with the radiation. Is the size of the deviation that
i can see through this comparison too high to ensure the therapeutic ratio
between the effect of the treatment and the side effects on the quality of life
of the patient? Let's not do the treatment or let's try to implement adaptation
strategies of the treatment plan in order to cope with the deviations we are
looking at when we are comparing the planning CT with the in-room CT just
acquired on the patient. The approach is clear and the way it can be
implemented quantitatively in terms of what do I look at when i compare the two
CTs, what are the uncertainties measures that i need to define quantitatively,
what is the threshold of decision (let's do the treatment vs.  let's stop and
start again vs applying adaptation strategies on therapy plan to take into
account the uncertainties in the geometry of the patient that I'm looking at
right now).  The implementation on a quantitative basis of these decisions and
this adaptation strategy is not trivial at all. This is a matter of clinical
applied research in radiation oncology nowadays.

CT on-rails are quite spread out and are used in some of the most advanced
therapy centers.


## Slide 29

Introducing the cone beam CT (aka CBCT).

This method is replacing the use of a CT inside the surgical room or inside the
therapy bunker with an easier way of acquiring a volumetric information,
similar to a CT, but in a much more simple and applicable fashion. 

*Professor Baroni here is showing an external program*. This is an application
for the comparison of images coming from a CT that we can see here on a patient
(in the example, pelvic region from the side, in the axial view and from
above).  This is the planning CT, the one
acquired on the patient in order to perform the treatment planning. The
radiation oncologist will see this CT together with the medical physicist and contour
the structures of interest and they will define the parameters of the geometry and the
physics of the radiation beam in order to obtain the proper dose distribuiton
inside the patient. The, then, patient goes onto the couch for treatment, as the
photograph we saw before of the PSI.  We need to obtain the information
concerning the configuration of the anatomy of the patient: how much it is
similar to the CT on which the therapy was defined or to access that whether
there are anatomical deviation. If i have a CT in the bunker, like in PSI, i
can acquire the CT. If i don't have a real CT i can use a technique which is
quite similar to the acquisition of the CT but which uses a conventional design
of the tube and the flat panel detector. This technique is called Cone Beam CT:
a CT that is acquired with a beam with a conic geometry. Let's see the results
before going into the results compared with CT.  By putting on the cone beam CT
on the complete CT: we can still recognize some of the anatomical structures and
these are nicely superimposing to the normal CT. A radiation oncologist
performing this kind of comparison is fairly happy because he can see in a nice
way the level of superimposition of the anatomical structures before giving his
ok to the irradiation. Of course this is not enough but, as engineers, we need
to go deeper than that and want a numerical assessment of this superimposition.
We want to display to the radiation oncologist in a quantitative fashion the way
in which this superimposition is there or not and this will be done with
technique of image registration: quantitative images comparison. Even more than
that we will be able to suggest how to move the patient on the treatment couch
in order to maximize this superimposition avoiding to redo anything from
scratch but trying to correct the geometry of the patient through the motion
that we have available of the couch in order to maximize the superimposition.

There are limits in terms of image quality and of field of view of the cone
beam CT image with respect to the complete CT image in the pelvic region. The
cone beam CT in this case features a much more reduced field of view which cuts
out lot of information in terms of superimposition between even with possibly
relevant anatomical structure and the quality of the image is not really very
high. we can recognize things nicely especially if we play around with the gray
scales, let some other structure come out nicely like soft tissues, so that
radiation oncologist can do their job. We have some artifacts of the image and
all come down from the way in which the cone beam CT is used.

Why is the quality of the image, which is 3D (we could have repeated the
process of qualitative comparison also on the coronal and the sagittal
representation) that bad? Because with respect to the complete CT, the cone
beam CT is acquired with a conic geometry of the radiation beam while in the
conventional CT the x-ray that crosses the patient has a predefined thickness
but it's like a blade crossing the patient during the rotation, so the
radiation beam is collimating such to give rise to a blade of a predefined
thickness which will give rise to the slice thickness (parameter that we can
play around), instead in the cone beam CT the beam geometry is conventionally
conic: like a conic beam coming out from x-ray tube and investing a much larger
portion of the patient during the rotation of the xray and detector, causing a
creation of much more secondary radiation, much more scattered radiation with
respect to a thin blade crossing a little section of the patient. The noise
that will go and reach the detector in the cone beam CT will be much higher
than the noise that will reach the matrix of detector inside the scanner of a
conventional CT.  That's why the quality of the representation of the cone beam
CT is much degraded and has been a topic of research over the last 15 years to
try to increase the quality of the cone beam CT by proper calibration of the
flat panel of the x-ray tube, by applying filtering and so on.

Since the very beginning of cone beam CT, due to the fact that is much more
handy to attach a tube and a flat panel for digital projection acquisition on
a C arm system or to the sides of a linear accelerator for performing radiation
oncology, and due to the fact that even being so easy it could provide 3D information
acquired immediately before the radiation, in case of radiation oncology, or
during the surgical procedure, if we are talking about the cone beam CT acquired
during surgical procedure or even orthodontic procedure, this made the
technique very interesting. I don't have to install a complete CT in my bunker
or in surgical room but i can use a much easier c arm system that is rotating
around the patient continuously or simply an x-ray tube and a flat panel
detector attached to a linear accelerator capable of rotating around the
patient. In any case it's providing, even paying the price of a lower quality
of the images, a 3D information of the same level and nature of the information
that i have available for therapy planning that is CT. 

## Slide 30

The differences between the conventional CT single slice with this thin blade
crossing the patient during rotation, and the multislice CT with some more
blades but a limited amounts of blades passing thorugh the patient in a
conventional multislice helical CT, and the cone beam CT where a large beam
crosses an entire section of the patient causing a lot of secondary radiation
and in turns this means higher noise on the resulting images and
reconstruction. Then we have the problem of the field of view: in conventional
CT i will have a relative motion of the patient with respect to the coupled
x-ray tube and detector either made up by sliding the couch inside the gantry,
or as we saw with the on-rail CT, sliding the gantry along the patient. Here we
have a fixed geometry along the axial direction: i could move the patient but
typically the size of the field of view in the three dimensional representation
that i will obtain with the same kind of tecnique is exactly the cylinder
corresponding to the entry points at the surface of the patient and going down
till the end of the patient. I can't be larger than that. I can make multiple
acquisition in cone beam CT if i want to enlarge the volume but then the
stitching (in it.  cucitura) will be complex, so I have limitation in terms of
the extent of the field of view along the axial dimension and even more...

## Slide 31

...I have limitations of the extent of the field of view along the axial
dimension and even more I have limitation in the field of view extensions on
the axial plane. because on the axial plane i can count only on the dimension
of the conic xray beam if i want to make calculation on the extension of the
field of view of the axial plane. Typically these are the dimensions: 25-26cm
so not capable of covering some of the districts of interests (typically pelvic
districts). They are ok in terms of fov extension for head and neck districts
but not for extracranial districts.

Fortunately there's a trick to overcome this problem, to increase the extension
of the FOV of CBCT: it is called geometry of half fan that is compared to the
geometry of full fan that allows only a reduced field of view in the axial
plane. Full fan geometry: collimating the beam in a way that the beam will exit
the patient and hit the detector covering the entire useful area (40x30cm of
the detector). In this case we can't go larger than the physical dimension of
the radiation beam when crossing the patient. The trick of the half fan
geometry is to displace the detector physically in such a way that if i
collimate the half of the beam in this direction i'm able to hit the entire
40x30cm detector with half of the beam because i displaced the detector and
collimate the beam in order to increase the field of view so the portion of the
patient that is crossed by the radiation beam towards one direction. Then i
have a 360 degrees resolution of this couple around the patient. This means
that for half of the rotation the portion of the patient which is sampled by
the radiation beam will be the one on the right and for the remaining 180
degrees it will be the other half of the patient to be sampled by the radiation
beam with displaced detector. In this way due to the fact that with only 180
degrees i'm able to reconstruct in 3D losing some of the information but it's
the only way to go. By exploiting the combination of the two 180 degrees
rotation with displaced detector and half collimated radiation beam i will be
able to more or less double the size of the field of view of the axial plane
that i will be able to obtain. This is what allows to sample the entire pelvic
or toracic district with cone beam CT.

## Slide 32

These are typical dimensions of the CBCT mounted on a linear accelerators in
radiation oncology. This technique is used in other reference surgeries and
especially in orthodontics, which gives a 3D panoramics of the teeth and
craniofacial configuration. We see here what we can obtain with the half fan
geometry exploiting the displacement of the detector and the half collimation
of the radiation beam with a 45 cm diameter which is enough to visualize
properly entire districts of interests such as toracic or pelvic region.

## Slide 33

Together with the previous one.

## Slide 34

Some example of CBCT with different levels of post processing of images. In
some there are artifacts. It is a sort of progression of image processing of
CBCT along the last 15 years that brought the quality of cone beam CT nowadays
applied in radiation oncology to a level of quality of image which is very
closed to the conventional CT image.

## Slide 35

Head and neck districts in which we see the progress of the evolution of image
quality that will in turn make easier the comparison between the volumetric
dataset obtained with CBCT with the planning CT because that's what we want to
do. The higher the quality of the CBCT, the easier and more reliable will be
the comparison between the cone beam CT with the CT to assess and quantify the
anatomical deviation, the uncertainties that are affecting the patient at the
time of treatment. The effort has been towards the increase of the quality of
CBCT not only the increase field of view but the quality of the representation
to make easier this comparison. the more the noise superimposed on CBCT the
less reliable the comparison.

## References

Slides: "Lezione_MIBCA2_ENG"
