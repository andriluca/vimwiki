# Introductory notes

There's a strong link between the two modules:

- Professor Baselli's concerns imaging for diagnostic purposes.
- Professor Baroni's concerns imaging for therapeutic purposes (i.e. computer
	aided surgery and radiotherapy).
	
We are going beyond the basic imaging course (Professor Signorini's) in the
sense that we are not only approaching 2D filtered backprojection but also
extension to 3D and numerical methods. In doing that, we will recall the basic
principles of FT (aka Fourier Transform), CST (Central Slice Theorem) and the
FBP (Filtered BackProjection).

Beyond the improvement on image reconstruction, this course will be dedicated,
for its major part, to MRI (aka Magnetic Resonance Imaging). This because it's
a method that requires much time to get the basic principles and to be able to
understand special methods (e.g. Chemical Shift, Diffusion Imaging,
Angiography, fMRI that are mentioned during the course). With the basic
principles the student will be able to understand many other methods like
Magnetization Transfer, Arterial Spin-Labelling and so on.

We are going to listen to the Fourier story and CST in a 3D-oriented paradigm,
so that student are more keen on understanding a more complex reality, such as
a tridimensional one. The reason why three dimensions are important is because
human beings are intrinsically tridimensional. So, if we try to perform image
registration of two slices from two different systems, they will never be on
the same plain and we can't recover those errors. Conversely, by starting from
two different volumes, we can really fit them. Beside that, navigation strongly
needs 3D in order to work properly.

## Some general point on biomedical imaging

### Current evolutions in hardware

- X-ray has evolved to 3D very potently because now we have flat panel
	detectors that can revolve around body. This is far simpler than what could
	be done with the image intensifiers.
- CT (aka Computerized Tomography) has evolved from a single-slice to
	spiral/multi-slice scanner. This is a 3D technology. The multi-slice scanner
	does make even 4 rotation per second with so many arches (i.e. sensors are
	not lying on plains but on rounded surfaces) of detectors in the order of
	256\. So at each revolution some centimiters of the body can be captured by
	the machine: during a breath hold (technique that is necessary to avoid
	artifacts due to respiration), it can get an entire abdomen and torax.
- US (aka ultrasound) has evolved towards 3D because they can direct the beam
	insonating 3D rather than just 2D as in the ultrasound that were studied in
	the basic course. One major breaktrough in doing that is that the old echo
	scanner, they were doing the beam-forming (i.e. focusing the US beam) just in
	transmission and they got all the echoes back to the receiver. This choice
	created lots of artifacts also known as speckle (in italian: "macchiolina")
	noise. Speckles are reproducible and are the intersection of many scattered
	echoes. This has been overcome by beam-forming not only in transmission but
	also in reception and by phasing the arrays of sensors (in order to detect
	the signal in the right time).
- MRI has exploded with 2 types of contrasts. 
- Optical spectrometry.
- The instrumentation for labs for small animals (micro CT) have evolved.

### Current evolution in modalities

- 3D scans.
- 3D rendering.
- The richness of contrasts give us more functional information overlapped to
	the "simple" anatomical information.
- This goes even to microstructural and biomolecular characteristics which are
	extracted.
- Multimodal for navigation (fusing the information).
- Dynamics which involves 3D volumes evolving in time.
- Genomic mapping.
- Data banks and atlases.

## Books

As we saw in the previous section, this is a very rich world that we would like
to look into. There are many books and summer schools that revise some of the
most advanced bioimaging methods.

[*Advanced bioimaging book link* (**NOT REQUIRED FOR THIS COURSE** and in italian).][immagini_biomediche]

## The Exam

Professors will present the two different exams and then they perform rounded
up average of the marks. There's the **possibility to have the two parts in
different sessions**. However, at the end of the winter session, Professors
reset all (meaning that the marks are valid **for one academic year**).

- Written test: 10 open questions.
- Oral discussion. The appointment has small group of students. In the same
	session there's the correction of the written test having the student in
	front of Professor Baselli and then oral discussion starts and give the mark
	about his part. When Professor Baroni sends to Professor Baselli the matching
	part, he performs the average and register the final mark.
	
When the exam will be approaching, we are meeting at the indicated hour and
room for the written test and, after that, students are given some days to
insert their names in a Google Drive worksheet to compose small groups. These
groups can't be made before the written test because Professor doesn't know the
actual number of participants.

If someone has some serious problem, the Professor is substituting the written
and oral exam into a big oral exam.

# Lecture 1 --- Recall of monodimensional FT to allow extension to multi-dimensional (2D...ND) FT

What is the magic? why the FT can be so easily extended to more then one dimension.
All is just in two concepts:

1. How to know the phase of the oscillation I'm considering in more than one
	 dimension.
2. Different oscillations are orthogonal in the sense that multiplied and
	 integrated they must be zero. This means that it is an orthonormal basis
	 vector space which has this very important property. This is also the way we
	 do the FT and the IFT (aka inverse FT).

Fourier in last part of 1700 understood that periodical mechanisms could be
separated into harmonics. The first harmonic has the same period of the
periodic wave and the details of the shapes of each period can be summarized by
the further harmonics (2nd, 3rd, ...). This gave unexpected developments in all
the fields that implied signal or image processing. For instance much of the
importance of Fourier transform is that in linear systems we can separate the
various component, treat them separately and then recompose them. This because
we have the linear summations of the inputs and outputs which can be divided
first, filtered into a linear system and next recomposed. There is a lot more
and we will see that also non-linear processes like demodulation of
radiofrequency for transmission is more easily described in Fourier Transform
space. This will help us when dealing with MRI.

- FT decomposes a 1D signal into sum (aka expansion) of sinusoidal functions
	(aka harmonics).
- It is hugely applied in the analysis of linear systems and periodical
	phenomena.
- *Reasons of FT importance*:

	- The FT and CST (Central slice theorem) permit the resolution of the problem
		of *reconstruction of the image from projections* (aka Inverse Radon
		transform). In particular, we can demonstrate the relationship between the
		Inverse Radon Transform (which deals with the projections), the FT and the
		image space. This will give us the basic tools to solve the problem
		discussed at the beginning of this bullet point.
	- FT is the way of describing raw data collected in MRI scan (aka the
		*k-space*), does provide the key for MRI image reconstruction, does get
		Fourier components of the image. Naturally with a trick of phase encoding
		which is translated in k-space (that is just a Fourier Space with another
		name) it's natural to work with spacial frequency. The part we will find
		strange is how we can use temporal frequency on modulating a radiofrequency
		to get spacial frequency. This is very strange but once we get this we can
		do anything with MRI. We have to remember that *Harmonics in time are
		converted into harmonics in space*.

- The most important concept to keep in mind (when dealing with images) is the
	one related to *Current Phase* of an harmonic, equivalent to the one of
	Amplitude related to 1D signal.

## 1D Fourier Transform (1D-FT)

The definition of 1D-FT is the following:

$$F(\omega) = \int\limits_{-\infty}^{+\infty} {f(x)\cdot e^{-j\omega x} dx} = F_{1D} \{f(x)\}(\omega)$$

In this first formula $x$ is the distance in our case, the exponential is
representing the harmonic. 

A second notation is the following: $F(\omega) = F_{\text{1D}}\{f(x)\}(\omega)$

In this notation, the braces are used to spot what is the argument of FT. The
second notation reported is showing that we have no more the dependence on x of
the FT but just the dependence on $\omega$, the radian frequency.

Radian frequency $\omega = [\text{rad}/\text{cm}]$ is used instead of natural
frequency (f = [\text{cycles}/\text{cm}]), so beware of the reference because
we'll say frequency but we are referring to radian frequency instead.

It's important to keep in mind [**Euler equation**](#euler-equation) which
permits us to use imaginary exponentials in place of sine and cosine. They
are actually there but it's easier to treat these kind of integration if we
consider these complex sinusoids instead of just linear combination of sine
and cosine.

### The scalar product and the orthogonal components

Let's consider the previous FT definition from now on. We can divide the term
inside the integral into two factors. The first one, $f(x)$ is the signal and
the second one is something that can be scanner-dependent, a basis function or
something else. This second term is also known as *integration kernel*. The
integration kernel has the purpose of weighting the original function with
something which has the desired properties. 

The FT notation is nothing different, in the continuous domain, from a scalar
product (aka inner product) of two vectors. Let's suppose we have samples of
$f(x)$ and samples of $e^{-j\omega x}$ for 100 different values of x (i.e.
discretizing the two terms). Now we have two vectors. The scalar product
operation consists to multiply the first element of the first vector with the
first of the second vector, then the second with the second and so on until the
100ths are reached. After multiplying element by element we sum up the products
and accumulate everything in a variable. Doing that with $dx$ with virtually
null dimension is not so different from doing that considering two vectors with
finite dimension.

By understanding the former concept of the scalar product we can easily
understand orthogonality. 

$f(x)$ can be considered as the sum of different harmonics. The inner product
with $e^{-j\omega x}$ will *kill all the harmonics that are different from*
$e^{j\omega x}$ and return a complex scalar value $F(\omega)$ that represents
the **amplitude** and **initial phase** of that specific harmonic at frequency
$\omega$. This is a particular point of the FT.

The key concept is that everything that results orthogonal is discarded by the
inner product inside the 1D-FT. Only the non-othogonal components are kept.

If two frequencies are different the multiplication of positive slopes and
negative slopes lead to different signs per regions that are repeating every
period independently from the initial phases. By integrating the product of the
two harmonics with different frequencies, the result will be zero,
demonstrating the orthogonality of the two. **Different harmonics are
orthongonal and they constitute a system of orthonormal basis functions**

To sum up:

- Two sinusoids of different frequencies are orthogonal (meaning that the
	scalar product of the two vectors associated to them is zero).
- A sine and a cosine that share the same frequency are orthogonal.

A positive value of omega with the negative value of omega will be something
orthogonal because they are rotating in the opposite way in the complex plain
(?).

Sometimes we are having some problems because in some notation is preferred, as
a measurement unit, $\frac{[\text{cycles}]}{[\text{cm}]}$ instead of radians.
*Normally in this course we are using radians and radian frequency*.

### Euler equation

![Graphical representation of Euler's equation in Re-Im space.][euler]

$e^{-j\phi} = \cos(\phi) + j\sin(\phi)$

Physically, we never have complex signals but real ones related to
measurements. Complex notation is so synthetic that we associate the in-phase
component with the real part and the in-quadrature component with the imaginary
part. In this way, with just a number we don't have to bother with sine and
cosine.

It's now easy to understand the 1D-FT. $e^{-j\omega x}$ is the integration
kernel, an orthonormal basis function in which its purpose is to "kill" every
component of $f(x)$ that is orthogonal to it. In this way we are left with just
the component $e^{j\omega x}$ (that's because of mathematical tricks).

### 1D-Inverse FT (1D-IFT)

The same interpretation fits with the formula of the IFT. If we want to
reconstruct the original signal, we get all the components at each frequency
$\omega$ and we give each of them the FT value, which is their amplitude, and
then sum up. 

$$f(x) = \dfrac{1}{2\pi} \int\limits_{-\infty}^{+\infty}{F(\omega)\cdot e^{j\omega x} d\omega}$$

Actually, if we do both the FT and IFT using natural frequency in place of
radian frequency, we don't need the coefficient $\dfrac{1}{2\pi}$, also known
as Jacobian.

### 2D-FT

Any image can be decomposed into spatial frequencies by means of the 2D-FT. The
2D-FT is useful as it allows to:

1. Filter an image.
2. Detect objects and edges.
3. Process raw magnetic resonance images by the convolution with linear
	 filters.

Let's revise some concepts that may be useful:

- *PSF*: Point Spread Function: a system's impulse response. It represents the
	blurring of the image that is intrinsic to the system.
- *OTF*: it's the optical transfer function. The 2D-FT of PSF.
- *MTF*: it's the modulation transfer function. modulus, absolute value of OTF.

Now that we have revisited the 1D we can pass to 2...ND.

We work on images with the 2D-FT. In this paradigm we have concepts as the
optical transfer function, that is the FT of an image in phase and amplitude
(like for signals in 1D). The amplitude of OTF is called modulation transfer
function (MTF). Filters are normally described by MTF because most often in
space they are symmetrical, so the phase is null. 

### Generalization of frequency of oscillation in 2D

![Representation of harmonic space. Let's notice that in the direction orthongonal to axis t the signal is not changing.\label{fig:harmonic_space}][harmonic_space]

![Interpretation of harmonic space.\label{fig:harmonic_space_ext}][harmonic_space_ext]

We need to extend the concept of oscillations (sine and cosine) to the 2D (and
next 3D and so on) harmonic space. This is done in a very simple way by
supposing that:

- There's one direction (called main direction t) orthogonal to the crests and
	valleys of oscillations which defines the frequency of the oscillations. It's
	the direction in which phase $\phi = \omega\cdot t$ has fastest increase.
	This means that in other directions there are lower 1D "perceived"
	frequencies.

- There's the direction that is orthogonal to the main one implies that the
	function is constant. 

Figure \ref{fig:harmonic_space} is representing an harmonic in a 2D space. It
does have some interesting geometrical property which permits us to represent
it very easily in a 2D space of frequencies depending from these directions. 

With a couple of trigonometric passages, it's possible to say that the period
on the x-axis is longer than the period across the main direction just defined.
The frequency of the oscillations on the x-axis is lower with respect to the
main direction. The same happens for the y-axis. The factor depends from the
cosine on the x-axis and on the sine on the y-axis.

$T_x = \dfrac{T_t}{\cos{\theta}} \rightarrow \omega_x = \omega\cdot \cos{\theta}$

$T_y = \dfrac{T_t}{\sin{\theta}} \rightarrow \omega_y = \omega\cdot \sin{\theta}$

In the end, the logic of this passage is that a frequency can be represented in
the Fourier space of two dimensions given the apparent frequency on the x-axis
and the apparent frequency on the y-axis as coordinates. With these coordinates
we can construct a vector so that:

$\vec{\omega}=\begin{bmatrix} \omega_{x} \\ \omega_{y} \end{bmatrix} = \begin{bmatrix} \omega\cdot\cos{\theta} \\ \omega\cdot \sin{\theta} \end{bmatrix}$.

**Attention** The representation of $\omega$ in the frequency space does not
imply its dependency from the frequencies that describe the direction of
oscillations. It's just a way to represent the direction in which the
oscillations take place.

### The concept of local phase (which different from phase shift)

The other very important concept is phase. Do we know in which phase of the
cycle we are? Here we should recall the original meaning of phase. In
engineering, particularly in BSP, we use the term phase in the sense of initial
phase (when position/time is zero) or the phase shift between harmonics (e.g.
output vs. input in Bode diagrams). It's seldom useful to refer to the actual
phase in a point of a signal, of an image or something else. The term phase had
that original meaning: if we think about the moon phase it's the same. It's
also a concept related to the phase of the year. This kind of phase is known as
local or instantaneous phase.

With this kind of sinusoids still in each point we know in which local phase we
are. Local phase depends from:

1. *Direction* of oscillations.
2. *Initial phase* of the oscillations.
3. *Frequency* along the main axis t.

### Frequency vector and local phase

![Representation of the 2D-frequency $\omega$ in the 2D-FT domain is given by a
vector $\vec{\omega}$ in the reference system $\omega_x\times\omega_y$ oriented
with the same angle $\theta$ we considered in the harmonics domain. The
components of the vector $\omega$ are not 2D-frequencies but just coordinates
in the 2D-FT space. Performing the operation of putting the vector omega on the
2D space for every direction is equivalent to obtain the FT of the whole
image.][frequency_space]

Once this concept is fixed and we have the notation to represent the
frequencies as a vector, we come to the point in which we can say that the
local phase is just the scalar product of the frequency $\vec{\omega}$ and the
position $\vec{r}$. $\vec{r}$ is the used notation for position.

$$\varphi = \vec{\omega}\cdot \vec{r} = \omega_x x + \omega_y y = \omega \cdot r$$

This in 2D but it can easily be extended to N dimensions by just keeping on
multiplying and summing all the components.

### Orthogonality of harmonics in 2D space

![Representation of two harmonics, both having the same absolute frequency but
different directions.\label{fig:tartane}][tartane]

Figure \ref{fig:tartane} tells us that two frequencies with different
inclination are orthogonal because we have a kind of tartane with oscillation
of one frequency in on direction and the other frequency in the other direction
and they combine one to each other with positive peaks and valleys
symmetrically. This means that integrated will give zero as a result. What
happens if the direction is the same? It's easy to understand just considering
the properties of 1D-FT. 

What we still miss is what happens when we work with omega vectors which do
have frequencies and a directions of oscillation.
By changing direction, even if the two components were the same in absolute
value of frequency, they will kill one another because of the orthogonality
between them.

We know the phase, the orthogonality is preserved and we can perform the same
computation in more than 1D exactly as if we were in 1D.

So, to sum up, orthogonality is confirmed if:

- Two harmonics share the same direction but have a different absolute
	frequency.
- Two harmonics share the same absolute frequency but have a different
	direction.

[*Orthogonality link*, in other words.][orthogonality_link]: we have to
consider the inner product to prove orthogonality as
$\langle\phi_1,\phi_2\rangle = \int_0^{2\pi} \phi_1(x)\phi_2(x)\ dx$

Property of orthogonality is really important because it allows to compute 1D
integral along each axis to obtain the multidimensional FT. This results in the
application of FFT.

## ND-FT

The extension to N dimensions of FT is very simple, with the latest concepts in
mind.

The position vector $\vec{r}$ has N components. This means that it's necessary
to perform N integrations: one per each dimension. The integration kernel is
the same as the one used in 1D and 2D with the only difference in the
expression of the local phase.

### Generalization of local phase to ND

In 1D-FT it had this form: $\phi = \omega t$. It's possible to generalize the
phase by expressing the position vector. The reasoning is done in 2D.

$\vec{r}=\begin{bmatrix} x \\ y \end{bmatrix}$

$\phi = \vec{\omega} \cdot \vec{r} = \omega_x\cdot x + \omega_y\cdot y = \omega\cos{\theta}x + \omega\sin{\theta}y = \omega(\cos{\theta}x + \sin{\theta}y) = \omega \cdot t$

So the extensions to N dimension of FT and IFT are the following:

$F(\vec{\omega}) = \int\limits_{-\infty}^{+\infty} \cdots \int\limits_{-\infty}^{+\infty} {F(\vec{r}) e^{-j\vec{\omega} \vec{r}} d\vec{r}}$

$f(\vec{r}) = \dfrac{1}{(2\pi)^N} \int\limits_{-\infty}^{+\infty} \cdots \int\limits_{-\infty}^{+\infty} {F(\vec{r}) e^{+j\vec{\omega} \vec{r}} d\vec{\omega}}$

The extension of the formulation of FT is so trivial which seems kind of
strange. This is driven by the fact that the integration kernel can be
determined in its phase at each point in integration.

## Properties of FT

- *Separability of orthogonal directions* permits to decompose a 2D-FT in the
	horizontal direction first, creating an hybrid result. Then, this hybrid
	result is transformed again in the y direction to give the actual 2D-FT. This
	is a lot more practical: in numerical computation we have the FFT that can be
	exploited. We don't need to invent a 2D-FFT algorithm but we can apply FFT by
	rows and then by column and then we are done.
- *Linearity*: FT of linear combinations are linear combination of single FTs.
- *Complex conjugate symmetry*: if we deal with real images, the FT must have
	symmetrical values and respect this symmetry. In MRI it's really exploited.
	Generally: $F\{f^{*}(x, y)\} = F^{*}(-\omega_x, -\omega_y)$. For real signals
	(and images): $F(-\omega_x, -\omega_y) = F^{*}(\omega_x, \omega_y)$.
- *Change of scale*: the general rule is that if we shrink the first domain we
	are delaying the other domain. Short period means high frequency. $F\{f(ax,
	by)\} = \dfrac{1}{ab}F\left(\dfrac{\omega_x}{a}, \dfrac{\omega_y}{b}\right)$.
- *Shift*: a shift in 2D space does not alter the amplitude but adds a phase
	shift linearly increasing with the frequency. $F\{f(x - a, y - b)\} =
	F(\omega_x, \omega_y)\cdot e^{j(a\omega_x + b\omega_y)}$.

[immagini_biomediche]: 	https://www.patroneditore.com/volumi/9788855534277/immagini-biomediche-nuove-tendenze-in-tencologia-metodi-e-applicazioni
[orthogonality_link]:	https://math.stackexchange.com/questions/474398/waves-of-differing-frequency-are-orthogonal-help-me-understand

[euler]:				Pictures/euler.png		{ width=400px }
[harmonic_space]:			Pictures/harmonic_space.png	{ width=300px }
[harmonic_space_ext]:			Pictures/harmonic_space_ext.png	{ width=400px }
[frequency_space]:			Pictures/frequency_space.png	{ width=400px }
[tartane]:				Pictures/tartane.png		{ width=300px }
