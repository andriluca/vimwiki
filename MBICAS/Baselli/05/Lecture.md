# Lecture 5

## Slide 1

blades → lame in italiano

## Slide 2

*near the graph*:
s → source of xray. it requires hundreds of kev to produce the xray. We need slip rings.

gantry → utilizzato da qualunque cosa utilizzi un buco per far passare il lettino (il supporto esterno). it comes from the support from beer's barrel, to keep the beer for drinking.

there are 2 kind of solution: the one related to an arch and the one related to flat panel detectors (FPD). The second one is the one we are interested in.

c arms → 2 branches: one from the source, the other for the flat panel and nothing inside

*lower part of the page*: beta → position of the source. for the central
a → orthogonal to the isocenter of the detector (similar to theta).

by translating the point of the detectors on the virtual line we can make the solution more readable.

## Slide 3

Referred to the $\cos{\gamma}$:
$\gamma$ → position of the ray inside the fan

we have to consider that the travelled path of rays with inclination gamma need to be compensate

## Slide 8

*The full radon transform*: the true radon transform in space.

