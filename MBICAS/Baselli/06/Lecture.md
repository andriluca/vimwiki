# Lecture 6

## Page 1

*hardware of mri scanners*. it's important to understand the way by which the
image is collected by a scan and next converted into a visible image. The main
feature of MRI scanner, which is shown in the picture, is a very big and
powerful magnet in order of Tesla (1T = 20000 times the magnetic field of the
Earth and 10000 Gauss). The great success of MRI was immediately recognized in
the unique capability of producing very good contrasts in soft tissues,
something that CT by x-ray transmission will never give us (this last one is
much more precise in absolute positioning by exploiting its geometrical
properties but it does not give us a good contrast in soft tissues because
their density of x-ray absorption is close to the one of water which is put to
zero Hounsfield unit for convention. conversely the CT gives us lot of other
informations: good contrast in bones compared to soft tissues and also air
compared to soft tissues). MRI conversely it's exploiting intimate properties
of NMR mechanism and *its coupling with biomolecular environment* that is
changing the relaxation times $T_1$ and $T_2$. NMR is produced by Hydrogen (H)
in clinical scanners , other nuclei could be used by 99% is H. H means mainly
water but not only water but any organic substance contain a lot of hydrogen.
the basic MRI contrast is given by the density of this H (*proton density*: how
many protons we have in each type of tissues). For this reason the bone, which
does not contain H, is black to the MRI but, as we can see in the head, we have
a really good delineation with good contrast of all the soft tissues
structures. MRI gives good contrast between liquids (i.e. in the gall bladder
compared to the parenchima outside.). By means of proper contrasts or even
signal nulling, we can have fantastic ortopedic images (look at the knee).
Look at the axial section of abdomen with internal organs very well defined.
Actually it should be understood that the huge flexibility in modulating the
contrast according to what we want to see has come very well ahead the
invention by Lauterbur of MRI because there was a long experience on chemical
analysis by means of NMR. Since the 1940s the NMR came into chemical physical
analysis of samples of molecules (or tissues etc) and the analysis of the
signals was already developed. What really lacked was the capability to
position the sources of signals inside the body. The great breakthrough
introduced by Lauterbur was the *introduction of gradients* exploiting the fact
is that *the radiofrequency signals at which the body does resonnate is
depending from the intensity of the magnetic field locally* --> if we have a
small sample we do consider just the main magnetic field but if we have an
entire body we must introduce some further magnets which are called gradients
because they generate a gradient magnetic field which is varying linearly
through the body and so the phenomenon of the magnetic resonance is differently
modulated according to the position and this is a kind of encoding of the
position we can work on to reconstruct the image which really permitted the
first MRI images and, since it was known the potential of contrast, this was a
huge explosion but since the position is just encoded and we must frequency
encoding or phase encoding of the radiofrequencies which are emitted and since
this is subject to small biases the absolute positions are not very well
calibrated. The MRI will be calibrated for the gradients and the homogeneity of
the zero with a proper function almost on a daily basis but anyway there are
some computation in between the positioning. Our eye does not see whether
there's some small distortion over image because it looks locally: we make
foveation in seing something so for visual diagnostic this is not important but
we, especially in this course, should always keep in mind that if I did
conversely some absolute measure the absolute position is not enough good for
navigation. Also for this huge magnetic field it's almost impossible to have
the MRI in the operating room or in the radiotherapy room because really it's a
nightmare for the influences of the magnetic field and also the interaction
with all the objects which are around: if we have something ferromagnetic this
will get inside the bore with the speed of rocket attracted by the big magnetic
field. Furthemore the signals of radiofrequency are very sensitive to all the
metal objects which can be around the scanner because they are distorting the
magnetic field even from the outside. So really the state of the art is that of
having excellent MRIs but normally as pre-intervention images. So we can
register an MRI onto a CT which will give the exact position and look at the
informaiton given by the MRI while navigating inside a surgical intervention or
a radiotherapy but we must be aware that it's very difficult to have a MRI in
the OR (Operating Room).

## Page 2

The phenomenon of NMR was discovered by Purcel and Bloch in 1940s and they won
nobel price in 52. Starting from the 1940s immediately the chemists did
understand the capability in analyzing expecially organic molecules with that
mechanism. For these the particular machines were of NMR smectrometry were
developed. In a small sample we can't distinguish positions but we can
distinguish the signal and how it's influenced by the chemical bounds. Here we
can see the typical spectrum of H signals of these molecule (Ethanol) and we
really see how the different chemical bounds are giving slightly different
signals. This scale is normalized by the frequency of the main magnetic field
which is put to 0 conventionally and there are some very small modification of
the resonnance due to the shielding effects since this molecule is surrounded
by a cloud of electrons andso the magnetic field which is experienced by the
single H nucleous is different according to the bounds. The H in OH group is
givin the middle signal and so on... The signal is also split into sub spikes
of spectral lines (actually this is not seen in in-vivo MRI), the reason is
related to what is the interaction between one single H with the nearby ones.
It's important this number of peaks we have per region because these patterns
represent the fingerprint of a molecular bound and structure. so we can
understand the huge power of looking of how this molecularstructure are done.
They can also use the signal coming from C-13 (Carbonium-13), which is also
resonating, and construct molecules with only one C-13 nucleous, while the
other one is the common non resonating C-12, and follow structures of molecules
and also methabolites of that molecules, for instance in this way they can
follow each single step of the really complex methabolic pathway of the
digestion, the burning of glucose in the mytocondria which is producing energy
in the form of ATP. We will be back on this in a specific lesson. This signal
is really important in this course. We did not know a lot about the signal and
even on very fine details. Even the fact that there was the relaxation of the
signal which was lost very soon and that by relaxation time T2 (or transfer
relaxation) and that you have to wait some times in order to have some times
for getting new signal due to the longitudinal regulation (???) back to
thermodynamic equilibrium was known very well as well as techniques exploiting
echoes of the signals. The field was already covered but nobody at that time
understood that you could get someting more from squeezing the this signal
information. It was Lauterbur that invented that. 

## Page 3

Lauterbur invented that. He was  a chemist in Chicago and the interview with L
in 2003 is very nice because ther was, when he proposed the idea, a lot of
skepticism in his collegues.  they didn't understand how important could have
been these kind of analysis. Exploiting a signal which does come mainly from
water, something that is vital and ubiquitus in the organic structures.

Getting the image was not just the proton density (which is the intensity of
the signal) but they could also modulate the 2 relaxation times T1 and T2. A
capability of not only having contrast but also modulating the contrasts
according to the specific clinical application that we want to obtain. Another
advantage of the MRI is that in principle it is non invasive --> it doesn't use
ionizing radiation like CT. If we have someone that is not cooperating or in a
severe condition it's much more easy to do a CT. MRI, even if not invasive at
all creates a lot more problems: it's terribly sensitive to movements, scans
are much longer than CT, patient is in a very long bore closed inside so
claustrophobia can be a problem, there's also a lot of noise inside the bore
--> switching on and off of the gradients creates the switching on and off of
magnetic fields and this means to impose forces inside the structures which are
as much rigid as possible but in this case the forces are translated into these
nasty clicks that are reproduced with various cadencies depending on the scan
procedure. The very important constitutive element of the NMR spectrometer is
an antenna for the radiofrequencies which should transmit to excite the NMR and
receive back the radiofrequency (the resonant signal) produced by the nuclei.
Of course anything must be tuned around the nominal frequency $f_0$ (Larmur
frequency) that is proportional to the main magnetic field $B_0$. The scales of
the NMR spectrometry (the graph) is in parts per milion (ppm). really tiny
differences produced by chemical shift effects due to the structures but,
introducing the gradients, we introduce the differences of frequency in order
of tens or hundreds of parts per million. One thing which is hard to understand
is that in some cases we can tune our scan in order to pinpoint these small
differences of metabolites and in other type of scans we can overlook, neglect
this small changes and consider the big changes introduced by the gradients in
the different positions. We can either consider this richness of signal or
overlook it. These single lines of signal shown below will be called isocromats
(it. isocrome). In the standard MRI we do consider just isocromats of water
together with other molecules' like the one of fat and we do neglect these.
Sometimes this neglected feature will produce artifacts that need to be
canceled by some tricks. Each scan is given by a specific sequence of
radiofrequency pulses switching on and off gradients with various intensities
and acquisition times. This is repeated many times with changes in order to
collect raw data that will be processed to reconstruct the images. A common way
to name the scan procedure in MRI is sequence. A specific sequence is giving us
information that will result in different type of images.

## Page 4

The main magnet in blue. B0 is along the axial direction. We take this
direction as z. xy are in the transaxial plane. we will see that is not
necessailry horizontal but depends on how it'made the shape of the main magnet.
Inside this we will need the gradient coils: we need 3 of them to have a
gradient along the 3 axis which can be linearly composed as vectors to have a
gradient in all the possible directions over the sphere.  Closer to the patient
we have the radiofrequency coils: only one could be needed but any scanner does
have quite large number of coils specific to the part of the body which must be
scanned.  All this information is going to a computer for storing raw data and
next process them to give an image visualized and recorded. Computer is also
driving the scanner in sequences: giving pulses to switch on and off the
gradients. it's giving the pulses which shall next be modulated at
radiofrequency to excite the radiofrequency coils and it's also receiving the
radiofrequency signal, demodulating it from radiofrequency band to audio band
in order of KHz from the order of tens of MHz of radiofrequency and sampling
will give the raw data to be stored in the main computer. The circuits here are
connected to the radiofrequency coils are radiofrequency circuits: a different
kind of circuits with respec to the ones for the audio band signals. Conversely
the cicuits are partly digital for synthetizing the shapes of the waves pulses
which are in the audio band (with duration of ms or fraction of ms -->
bandwidth of KHz) and the interfaces driving the amplifiers and the coils are
in this field of bandwidths, the same towards the shaping of the radiofrequency
we are in audioband environment because also the radiofrequency pulses have
duration of ms or fraction of ms, while the passage to radiofrequencies and
back from the radiofrequency is done by modulation and demodulaiton
respectively, much like the radioset we use to listen at some music program.


Some conventions: B0 is taken in the z direction and the unit vectors are
called conventionally i j k. i in x direction and so on. The main magnetic
field can be represented as scalar B0 that give intensity in tesla times the
unit vector. As we said Larmor frequency is f0 and proportional to intensity,
the proportionality factor is $\gamma$ in the formula: it is gyromagnetic
factor specific to the nucleus and it's expressed in
$\frac{\text{MHz}}{\text{T}}$, due to this proportionality. f0 here is a
frequency, not a radian frequency, so that's why there's a $2\pi$ factor.  The
three gradiant coils have a component to be zero which is varying linearly. if
we switch on the gradient Gx we will have a variation of the intensity
superimposed to the constant B0 which is proporitonal to the position x.
gradient is measured in intensity of magnetic field per unit of space and
clearly since we can linearly combine we can have any direction of the
resulting gradient. we can write gradient vector of the 3 directions with its
modulus and its proper direction in the space defined arbitrarily. We are in a
volumetric environment then because the whole volume is excited with the
antenna and giving back a signal. Note that for the gradient we are only
considering the z component. indeed it's impossible to have gradient coils
giving us only z component but for us the gradient is us the z part given by
those coils and this is precise with our practically known approximation since
there are 3 orders of magnitude between the intensities of these components, Gx
Gy and Gz, compared to the main magnet B0 so if the gradient coil is generating
with Gx in the k direction, also some Gx in the i unit vector or j unit vector
who cares! those are spurious components that are not changing the direction of
the final field which I get. what we really perceived is the linear gradient of
the component along the z direction (Formula reported in pt2).

Q: what's the z axis?
A: the one of B0.

## Page 5
## Page 6

modulation circuits. we will see why the modulation of a signal in
radiofrequency is requiring both a phase and quadrature components --> we make
an amplitude modulation of the sinus and also an in quadrature modulation of
cosinus. we independently decide the amplitude modulation envelop of the 2
components. This permits us to have both amplitude and frequency modulation.
It's important both in transmission --> we want the radiofrequency to deviate a
little bit and so we need also phase modulaiton which can bring to a frequency
modulation and also In the demodulation we need to understand what is the
amplitude of the signal we receive back and also the phase of the signal -->
needed to get the phase encoding which is also varying information of the
frequency encoding of position. we will have to understand that.

Digital circuits: in audioband synthetizers since they must generate shapes of
gradients on and off, shapes of radiofrequency pulses. Of course this would be
made digitally and next converted to analog in the audio band which is just the
concept of synthetizing signals from digital computation. This is needed
because in sequences we use various shapes of both gradient pulses and
radiofrequency pulses. Next we need sampling of what was demodulated to be
stored as digital raw data to be converted and we will come to the point where
we will understand well that the image reconstructoin is more or less just an
inverse Fourier Transform because of the oscillating nature of the signals
which are converted by very dirty tricks into oscillations in space (not in
time) and this will give us a FT of the images in space.

## Page 8

Now that we know the components of the MRI let's see what are the technical
features:

The main magnet can be of different dimension, shapes and technologies
according to the appplication. Bore MRI with 1.5T MRI that is rapidly growing
to scanners with 3.0T that has been permitted for clinical application. 8T is
just for research and has a lot of problem in its application. The only way to
get this strong magnets is by using superconducting magnets (magnets at
temperature of liquid He (@4K) --> no resistance: we have to load the sc coil
by a huge amount of electrical current and the passage of closing the coil on
itself is very delicate but after that we just need to keep this in the bath of
liquid He because it will run forever because of superconductivity condition).
the current will circulate forever if the temperature is kept at that
condition.

Something simpler can e done with permanent magnets by reaching 0.3T or with
resistive magnet that are consuming a lot of energy and introducing a lot of
heat (because we are using just copper) --> 0.15 T. These solutions are good
for small scanners (i.e. limb scanners).


B0 direction is next driving the configuration of the gradient coils and also
the positioning of the radiofrequency antennas because of what we are needing
to do.

## Page 9

On the main magnetic field th technical specification we must define are
limited to Homogeneity and intensity.
- Intensity: a scanner is build for that intensity (i.e. 1.5T) which means that
  Larmor frequency is fixed (63MHz). The scanner will never change that. it's a
  constitutive element.
- Homogeneity: it must be kept in levels of 1ppm. The reletive changes in
  space, moving in a field of view must be really precise. The reason why is
  that all our ways to define the position are based on frequency changes. so
  the changes in the main field due to inhomogeneities for construction reasons
  will be equivocated and aliased as change in position and that's not good.
  This is more severe in chemical analysis where we play on ppms.  Typically we
  use phantoms which are spheres filled with waters and other elements to
  simulate the contrast we want (but homogeneous anyway) with diameter of 50cm
  which means the field of view. It's clear and obvious that homogeneity on a
  small span is much more easier than homogeneity in the whole fov span. If in
  volume we can insert something that is 50cm thick we must be sure about
  homogeneity over a 50cm sphere and this is evaluated by looking at the
  distortion which is given in the reconstruction of that sphere.

homogeneity can be quite good also with open magnets. See the parts below, it's
shown the helmoltz pair (--> 2 coils with current flowing in the same
direction), the simplest way to obtain a constant magnetic field. We can
compute the formula of how the magnetic field does change along the axis. If we
look at the graph we can see that there's a correspondance between the diameter
and the distance between the coils. if the distance is equal to the radious a
we have really small variation of the flatness but flatness is optimized. The
dimension of the magnet can't be reduced and we need a really long bore. if the
distance on which it's develop the magnet is longer than the dimension of the
field of view we will be in the middle of the cuve and very flat. if we want to
have a smaller MRI we will have a lot of inhomogeneity.

## Page 10

New generation without the liquid nitrogen phase. The container of the liquid
He is much like a thermos "flask", like a thermos for coffee, reflecting with
vacuum and reflecting surfaces to keep the energy outside and the helium
inside. we always have some thermal energy entering, so there's a continuous
evaporation day and night. The evaporated helium must go to a rigenerated
process and this is why when we are near MRI the noise of rigeneration of
helium going on day and night. this system which is kept in this way for the
entire life of MRI is strongly unstable because if we lose the
superconductivity in a small poriton of the coil we have a part of the material
which is no longer superconductive and the Joule effect start and producing
further heat so long and so forth and this brings all the evaporaton of He -->
quenching of the magnet. This is dramatic because we need to restart the
operation of the magnet which is very expensive also for the cost of the helium
which was lost. when it's eveporating He is freezing anything that it's
encountrering. *voluntary quenching* --> if something metal went inside the
bore and we can't get it out due to the forces --> we push the red button to
quench the MRI for safety reason.

## Page 11

Gradient coils: a part from the z gradient the shapes are quite strange for
these coils. The trick for the z gradient is very simple and given by the
Maxwell pair (first ones on the left): two parallel rings and make the current
flow on one directon and in the opposite direcion in the other one --> in the
middle the contribution of these 2 dipoles will be exactly 0 and the field in
the z axis will increase in that direction.  x (and y) axis --> golay pair:
pushing up and down a fairly linear region of gradient.

## Page 12

Here we have a formula for the magnetic field of maxwell pair along the middle
axis. We really care about the component in the z direction and computed on the
axis we obtain this shape which has the same formula we had for the helmholtz
but with a '-' sign between the two members and we have a really good linearity
for a small region inside --> important for the calibration of the region
inside.

## Page 13

Antennas ans RF coils: a huge topic because we might have different types of
coils: all the coils of a given scanner must be tuned to Larmor frequency with
a certain bandwidth which can change on applications. f0 is proportional to B0
that can't be changed. So the passive inductance and capacitance of capacitor
and inductor must be tuned with the classical formula of resonance of LC
circuit below. Parasyte capacitance and inductance because we don't need to
have specific capacitors and inductors because, working at RF, any loop creates
strong inductance and any conductor which is close to another is creating a
parasyte capacitance. So just the very well design of the coil should fix
these. however we have small capacitors for tuning --> because value of L
changes depending on what it's inside the coil so it can change and we must
tune. this means that, since we are transmitting with coaxial cable a
radiofrequency, we want impedence matching, otherwise we will have energy
reflections. Tuning and matching capacitance.

## Page 14

Do we want perfect resonance of this LC circuit? absolutely not!  we don't want
a too high quality factor because we need to have a bandwidth on which we can
modulate the signal in the antenna. So in the whole span of the bandwidth we
don't want a cut off of of some frequency. What is the bandwidth? KHz of
bandwidth over the 63 MHz of Larmor in 1.5T magnet.


We need many shapes of coils because there's a tradeoff between homogeneity and
sensitivity.  in the image we are seing the maximal homogeneity that we can
have by having several resonant coils which are arranged as a bird cage with
the axis of the cage coincident to the z axis. This is very much used for the
head becaues we need an homogeneous reconstruction of the volume.

If we want to maximize the sensitivity we really need simpler coils which are
flat and which are adapted to the part we have to image. for instance for a
backbone we want to visualize a small portion: we don't care if the image does
fade to black moving to the belly in front of that. We only need to illuminate
very well the interested part. 

the property of a surface coil is that its sensitivity to what is far is fading
very rapidly. in this way we lose field of view but it's not a problem because
we don't want to visualize portion that is far from the one of interest. we
would use a different coil if we wanted to see the abdominal organs. In
magnetic resonance we are fighting for SNR against the noise which is produced
by electrons that are moving at body temperature inside the body. I am sensing
only the noise which is coming from the part of the image i really need and the
noise volume will be much less than if i had a volumetric antenna which is
homogeneous. So this is why since the very beginning we had volumetric and
surface antennas according to the application. 

Nowadays since we are starting using arrays of surface coil, for instance for
the long backbone, we need an array of surface antennas which is built in the
bed typically. The technology of array of antennas grew more and more and now
we can build up caskettes of surface antennas for the head with the order of
30-40 small coils which are giving us both the advantages of surfaces coil and
the composition of all of them is also giving us a good volumetric homogeneity.
So now the trend is thus toward arrays and this means that each antenna must
have its own modulating and demodulating channels (2 for the phase and
quadrature). The throughput of data is growing and also the complexity to drive
these arrays of these antennas. This is the trend.

## Page 16

Some remarks about safety. Biologically the intensity of a static magnetic
field is not an issue: they have grown thousands of chickens in eggs which were
born inside magnetic fields and staic field is not perceived and is not
producing any effect. So in principle we can grow as much as we want the
magnetic field but the limits are much more constructive. These are the
guidance in 2003. 

When we move somebody inside a static homogeneous field we have induction
effects as if we were swtiching on/off magnetic field so for instance to insert
a patient in an 8T scanner for research they need something like 20min moving
the heads very slow not to induce currents which will be understood better for
the gradients. dB/dt is inducing currents. The problem of inducing current is
that it can activate nerves and muscles which is giving us pain, contractions
of muscles --> they must be kept below that activation thresholds. More
specific is the problem of Specific Absorption Rate: with RF we are delivering
W/Kg to the body and we don't want to create a microwave oven cooking the
patient. this is not a problem for the old good very slow sequences giving us
good images but with a lot of time. This can be a problem for the fast
sequences which are bombarding the body with very fast sequences of RF pulses.
This is of course limit.

Attenction to the heat we are providing the tissues.
