# Lecture 7

## Slide 1

nuclei, the center of the atoms, are spinning --> they have an angular momentum because since they have mass and they are spinning; and they have a positive charge and it's going around with the mass, inducing a magnetic dipole --> spin generates both magnetic momentum of dipole (mu) and an angular momentum vector in the same direction. 


So the way, starting from this mu, the magnetic dipole, it's quite long and we will go through it step by step:

1. what are the features at atomic scales --> quantum physics concepts
2. how the individual dipoles can result in a material magnetization. M(t) a vector that can vary in time. Particularly we will be interested in the transferce component of this magnetic dipole, which is Mxy. with this transvert component we are able to sense through our sensing antennas. the bar in the notation is representing complex numbers because having just 2 dimensions of vectors we are interested in is very practical to deal with with complex numbers also because we will see that component in x and y are oscillating in a quadrature fashion (90° of phase). 
3. after that we will pass to voltage through the antenna. Attention: We are considering the complex number Mxy in the RF field but after this, in the antenna, we don't have complex signal (a complex voltage makes no sense) anymore so V(t) is a REAL signal but when we pass with downsized band by demodulation, we are then back to the complex number (because we are demodulating in phase and quadrature) --> 2 sampled signals: one along the cosinus and the other along the sinus sampled over time and here we are in the audio band. So from mu to V(t) we are in MHz and from S on we are in the kHz (audioband). 
4. Dirty trick: in these samples we will be able to recognize a FT which will be represented in the k space called k for historical reason but it's much like an omega apart for the 2pi. 
5. Inverse fourier transform to get our image in the volume r with xyz positions.

 This line is summarizing all we will have to do with MRI a part from the variations on top of this which will be later seen. [07:25]


## Slide 2

without any external magnetic field all the dipoles are randomly directed --> the total magnetization of the material is 0 (M=0).

The spinning nucleus have both angular and magnetic momentum.

When we are imposing B0 which is defining the z axis of our frame reference. all the dipoles are trying to orient to that direction, however there's a consrevation of momentum that does not permit the torque to bring the dipoles in the correct direction but will the dipoles preceed around the z axis with a fixed angle. dipoles can't give away the energy they have for conservation of energy and angular momentum (tipo trottole). they can be in 2 condiitons:
- spinning up: spin up spins
-opposite direction: spin down spins

there's an unbalance towards the more stable condition. there's a magnetization.
This magnetization is static so it does not induce electromagnetic forces. This longitudinal magnetization is the basis on which we will be able to obtain a transvert magnetization so a radiofrequency signal in our antenna (???).
Since we have fixed mass and charge in a given nucleus the raito between the magnetic dipole and the angular momentum is a constant that depends upon the nature of that particular nucleus. mu is a fixed constant of any isotope. even using the rules of the classical mechanics we can compute gamma for the protons knowing the dimension radius and data reported doen


## Slide 3

we should use the rule of quantum physics and this is of striking importance when we move to more complex nuclei. Actually we will be interested with nuclei with spin quantum I equal to 1/2 for practical application. As we can see the hidrogen is 1/2, as well as C-13.

gamma cut is gamma/2pi. for H --> 42.58
the gyromagnetic factor is less for C-13 --> more difficult to have a SNR.


Rule to define the quantum number --> we should see the simmetries in the nucleus. the numver of mass (apex preceding the simbol). the number of charge is defining the type of atom.

if number of mass (proton + nucleus) is odd we have asymmetry --> I=1/2
if even mass and charge number --> we will never perceiv enothing from that element. Helium is in this case (2 protons and 2 neutrons). When I is wqual to 0 we don't have magnetic resonnance
if mass is even but charge is odd --> particular case


## Slide 4

we can have several level of enregy. the number of quantum levels is given by the rule 2I+1. the quantum number mi is going to be +1/2 or -1/2 depending on spin up or spin down condition. with -1/2 we are in the least energy case (spin up condition). the spin down condition is that with the hightst level of energy with mi=+1/2.


*Referred to $\mu_{z}$*: the precession process is stable at a very specific angle which is here indicated. an angle that is about 50 degrees. the - is depending on the level of mu.


the reason why the precession is stable is in the quantum physics. in the macroscopic magnetization we would not have limits to the possible values of angles that are called in that case alpha. we could be close or far from b0.

In the scale of atoms we only have these 2 values of theta. why these 2? in the formulation we have above we have h, the constant of plank. these movement are subject to the laws of plank and quantum physics. the h/2pi (h tagliata or hatched). What is interested is considering the difference of energy bethween the spin up condition and the spin down condition. We will see that the deltaE does have a very precise meaning about the radiofrequency that can interact with that spin.

## Slide 5

the frequency of larmor is given by the gyromagnetic factor times b0. so we should consider the zeeman effect of separation of energy which is splitting the energu comparing the case of spin up and spin down which happens when we have spin up and down. this effect --> the quantum level from the outside is dividing into 2 levels because of magnetic field. we should remember that gamma divided by 2pi times b0 is the frequency of larmor. the relationship of this frequency is detemined by the relationship by ... (???) 

Instead of f0 we can use the nu --> by substitution in the formula we have obtained the energy of the photon. the radiofrequency that is impinging on the material and tuned in radiofrequency is going to excite the material becaues of resonance mechanism but also in quantum phisics. it's exciting the material because photons have energy that are sepering spin up from down condition. 
- continum resonance
- quantum physics --> one photons which have exaclty the energy to push from spin up to down (???)

E_spin down is the culo dell'ago della bussola, quello con più energia. lo stato + stabile è quello verso cui normalmente tende la bussola.

## Slide 6

*First equation*: the unbalance is very small and we can linearize this term and obtain the equaiton below.


*Second equation*: the 2 in the denominator is given by the fact we are both linearizing and we ar enot far from a perfect balance. the amount of magnetization is not far from the number of spins we have in the material. N_s is the proton density practically. The inbalance is proportional to B0, that's why we want higher B0 because it's providing higher signal in this way. the higher B0 the higher the magnetization.
the amount of magnetization is also proportional to gamma. higher larmour frequency give higher sensitivity. h has 4 times the one of carbon 13.


spect and pet ar high sensitivity methods


only 3 ppm of protons are helping us --> very low sensitivity. we have good sensibility vecause we are made out of water

## Slide 7

spin precession: as we have seen the basic spthings are te angular momentum and the magnetic momentum. in h we have just a proton spinning. 99% of cases we are dealing with protons. so proton density = h density inside our body. The precession which is explicated undrer a  -- is like a precession of gyroscope hich is spinning and is not faling doen but it preceeds because of the conservation of angular momentum. only thanks to friction this spinnin gis stopping but if we put it in a levitating environment we can keep his precessing gor a very long time. we must go back to rationlal mechanics and the solid mechanics to undertande this mechanism of precession. we should consider the torque which is the same acriss the spin. w ecan rapresent it with the vector orthogonal to the rotation plane, obtaining ith the rule of rotation. it should be directed to x axis. the torque is giving the derivative of the angular momentum.
j and mu are directed in the same way and coincident to the gyromagnetic ratio constant. if j is preceeding also mu is.

## Slide 8

let's see very small delta of time. deltaj is bringing the old j to the new j at t0+delta t. deltaj is oriented like q.
the next deltat is considering a deltaj still orthogonal to j and we have a small movement in the circle and at the end we are reaching the starting point.


we can use differential equation.


omega0 --> larmor frequency (radians/time)


z component is equal to 0! This is the reason we are considering just the transversal plane

## Slide 9

let's obtain the second derivative. this is exactly the harmonic equaiton of oscillation with frequency omega0. this is happening similarly for y direction.

both are oscillating at frequency omega 0 and to understand their relationship let's consider the expansion of the equation seen before and we can see that one is the derivative of the other and we can understand that the phase of both is such that seing omega0 as a vector this should be directed in the negative direction in order to have the formulas below.


we can exploit euler formulation to group everythong together. x component is in phase (cos) and y component is in quadrature (sin).


**Typo**: We need the imaginary unit j to multiply the exponent.
