# Lecture 11

## MRI Signal charcteristics

three different cathegoris for the signal:
- Free induction decais --> after a sngle RF pulse
- echoes: more than one RF pulse:
	- spin echoes:
	- stimulated: 3 or more pulses
- gradient echoes: dfferent because the signal is provided with 1 rf pulse and the nwe use a gradient wich is activated and its sign is inverted over time many multiple times. we give this gradient after the first pulse that induce FID and then ...

Assumptions to simplify the equation:
- excitation of RF pulse is istantaneous (@t=0)
- ignoring the effect of phase shifts and non uniformities. we use omega instead of deltaomega. all the constant terms are calibrated.

inhomogeneity are due to the emission.

spin spectral density function $`\rho(\omega)`$.

The exponential used is keeping into account the time decaying nature of the magnetization.

Signal expression (S segnato)

## Slide 2

## Free induction decay

stimulation is one RF pulse --> flipping th magntization.

alpha is the flip angle.


## Slide 3

the image is showing the FID of some isocromats.

The spectrum of the signal is dependent on SDF.

- In case of FID we have the first graph. The spectrum rho hat is not a single line though bwcause the signal is decaying but it's bell shaped (real part) and the imaginary part is on the right of the image. The spectral bell is giving us really important parameter. the width of the bellshape (è scritto). information on the T2 are enbedded in the real part of the signal.

Two isocromats or more. (graph at page 5) each of the peaks are referring to a specific isocromat and each FWHM are studied . the area under the curve is equal to the proton density of each isocromat of interest. FWHM is giving us iformation about T2.

Mixed isochromats: the FID signal has a much faster decrease. imagine to consider 2 different isocromats. ???


# Slide 7 -- Echoes

different from FID. Fid was asymmetric signal. i we have echoes -- simmetric.
Two pulse echo


# Part 2 -- MRI contrast

Ratio between the different of object a and b divided by a reference that is chosen arbitrarily.

There are 3 type od contrast:
spin density contrast
t1-t2 contrast -- the spin density constrast is always present and t1 and t2 are two weights. T1 is related to saturation recovery. sta leggendo sia t1 e t2. GE (gradient echo sequence). T1 and T2 are not mutually exclusive. in practise we divide the images of T1 and T2. (chiamate  T1 weighted and T2 weighted).

T1 --> providing really good contrast.

Contrast is modulated by diffrent parameters (proton densities, t1 and t2 but also something else that are sometimes overloooked because considered as artifact). for nstance T2* -- oxigenation of tissues, delta omegac -- chemical shift (metabolical information about the tissus), the velocity, the diffusion coefficien D (tissue microstructure).

## Slide 3

Everytime we are exciting the tissue with a 90 degrees pulse we obtain ..
we discard the pulse with the yellow (called first pulse).

Tr (time of the repetition) must be higher than T2 (the time of decaying in the graph).

If we want to avoid the presence of T1 weight we can make the parenthesis be equal to 1


## Inversion recovery

providing the 180 deg pulse before the 90 deg. we are flipping the spins. we have then a recovey and then at a certain apmpitude we excite.
why inverting the pulses? this enhances the T1 weighting of the image. this is made for 2 different consideration:

- increasing the relaxation rate
- ...



Signal nulling effect. we can basically modify the signal Mz(Ti) ???


## Short time inversion recovery

It has a very short t1. the effeect is much shorter to the one of ...
we can nullify the contribution of that in the final signal which result in enhancmeent of the contrast. we are able to suppress the chemical shift by using this tecnique.

how is it possible? we have an image here that is showing the longitudinal magnetization that initially is very low and then there's a rephasing. if we select a time of inversion that is inversion that is so short and coincide with the fact that the fat is crossing mz = 0. the consequence will be that i have zero at a certain time. no available magnetization to be able to generate contrast. The amount of magnetization shown by the different ucrves are the ones that are going to be flipped.

usign a very long time of inversion we can exclude the one of the fluid as shown in the picture. Suppression of fluid is very much used to suppress them in the image and this allows to see lesions in the structures that were before hidden by the different contrasts.

In the first image we have a very high contrast that is hiding the signal. by suppressing the fluid we are able to see the tissues better.


# Slide 8

General rule of thumb

- PD image --> no t1 nor t2 weight in the formula at page 8.
- T1 image --> suppress the T2
- T2 image --> suppress the T1


# Slide 9

## Inversion recovery in spin echoes
we can exploit spin echoes properties to have T1 or also proton density images. it's what we did before but with addition of sources of contrasts.

There are 4 different images with exclusion of particular components (fat, white matter and so on).


## Gradient Echo

We use one RF pulse to excite the spin system and then no more because the gradients can make dephasing and rephasing possible without further stimulations.
