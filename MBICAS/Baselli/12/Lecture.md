# Lecture 12 --- MRI signal localization

We are going on with the problem of localization of MRI. We have seen the first
method which is slice selection used to deal with 2D. Frequency and phase
encoding is used in the method.

## Frequency encoding

Frequency encoding is the encoding using gradient during the sampling
operation. The signal is demodulated in real and imaginary encoding.
We can really pick any axis we want.

We are considering larmor frequency which is not constant but has a linear
change in x axis proportional to the gradinet $G_x$. If we put that in the
signal of the antenna we can understand something. We can revert the order of
integraiton and we will realize that there's..

Integration along x direction is driven with this complex exponential here
emphasizing.

This should remind something: the full radon transform. We are working in a
volume in a given instant and we are differenciating through plains tat are
orthogonal to the gradient direction. 

We can see this representation in which we have in the middle the $\omega_0$
and in the others slower or higher frequency of resonance.

Proceeding by backprojection because this is the transform of the projection
along the single line. We can explore all the direction. We will see a scheme
that is giving us the reconstruction of image according to this way.

It's intuitive how the different position is distinguished. The distintion is
one single coordinate. All that is at the same rate is undistinguish from a
single encoding. Here we generalize, speaking of frequnecy encoding direction.
It can be represented as the modulus and the direction unit vector. This is the
generalization of what we have previously seen. We distinguish entire plains
and we are doing what it's needed to fill un the full radon transform. There's
a phase factor. Over time we have many different forms of 1D-FT since it is
exactly the kernel and the complex harmonic kernel which we found in the FT.

Normally a single direction is addressed by FE but ...

## Phase encoding

This is less intuitive. It's applied after the radiofrequnecy pulse for
activation and ahead of the readout. This is the one of the main reasons why we
prefer to work not on the field directly but with the spin echo that gives us
the time to do the encoding. We consider what does it happen when we apply a
gradient to the system. The different speed of precession is developing through
time a phase difference so when we switch off the gradient this will return to
nominal larmor frequency and they will keep this phase difference untill
readout happens. The delta phase must be linear. This because there's a linear
change in B.

We can generalize with any phase encoding direction we can distinguish modulus
and direction and we can obtain the phase. We find in the signal the same
formula but now we have a constant $T_{PE}$. We deal with the elements of FT
but ... We will see that we can change the accumulated phase encoding time from
readout to readout.

There's no problem in combining more than one phase encoding direction. By
switching on another gradent orthogonal to the previous one we ... this permits
us to obtain all the basis functions that are identified by any frequency in
any direction of the space.

The possibility to have two phase encoding directions permits us to do 3D
acquisition without slice selection but for instance 2 PE and 1 FE covering xy
space. Let's look at the k-space interpretation.

## K-space interpertation

k(t) = 1/(2\pi) \cdot \int_{0}^{t}{G(\xi)d\xi}. 

We can find confined both the PE and the FE during the sampling time. This is a
special frequency. To start convincing us we can see this example.

Let's suppose that we have a time of PE that is maintained with a given
strength maintained in y direction. This creates cycles along y direction and,
very important to understand is that the PE causes variations of many cycles
along the phase encoding direction. If we want to explore along the field of
view we need the harmonics, so many cycles. If we want to have higher frequency
in y direction we can switch on the gradient with the same direction. By
switching on a gradient of FE in the x direction this gives rise to maintain,
up to thi cycle. This is developing two periods. If we go on sampling the
period in this direction will increase untill the frequency we want to resolve
is reached.

Now by this scheme we can start intuitively how we can sample the k-space. We
have spin echo system with 90° selection, time. We can start with the
preparation of of k_x in order to have symmetric sampling around zero
frequency. Starting at -4,-4, sample to sample we obtain the harmonics that we
are interested along the x direction. Clearly in the next repetition we would
do the same, exploring from -4 to + 4 but we will give a phase encoding a
different value to have another line. We can now have the all 2D-FT. Next we
inverse 2D-FT and get the image.

Inside the material, with the applicatio nof gradient we are able to physically
impose the shape of archimede spiral. ...

We have information both of phase and quadrature of harmonics. We need to
position the ripple at the correct absolute position.

We can see here the cosine and the sine. 

### Examples of k-space trajectories and sampling (from Lauterbur)

We have seen the elements for FE and PE and how both are implied in the
application of gradient. Normally in 2 orthogonal directions. The next step is
to see how we can design something ... in k-space which is the representation
of FT of the 2D image reconstructed.

Let's start with simple example in 1D.

1. Let's suppose to acquire an FID and to explore the density of a bar. We
	 start sampling right after the frequency pulse. As we can see in this way we
	 move having just one single sign of the FE gradient G_x we are obliged to
	 move towards the positive position. We can impose that the IFT is a real
	 image in 1D but actually not practical.
2. W introduce a preparation gradien to take us the most extreme portion of k
	 axis and next we start sampling from the most negative value, reach 0 and
	 move symmetrically to point B. At this point, if we had some phase shift
	 which is confounding real with imaginary part of FT.
3. Something more realistic that is something happening in real life. Frequency
	 encoding is applied during readout but there's also a preparation phase
	 after the... that brings us to A- and then brings us to the symmetrical
	 value in the fourier space. This permits sampling by going ahead giving us
	 all the harmonics ... The preparation now is positive and, ahead of
	 inversion is equivalent to negative gradient.

Let's now see how we can derive an image by FT. The signal is going to be the density image $\rho (x, y, z)$ and the exponential. Our image will be just the image of x and the change of density along the ???

Let's read by substituting this image, which is actually projections by plain
and this is exactly a 1D-FT. To get this monodimensional image we just have to 1D-IFT.

If we are not along object but we are using a volume we can apply slice
selection, first in one direction.

### Two dimensional imaging

Let's suppose to have an image I(x, y). We have confined activation in the
interval $\[z_0 -\Delta z/2 ... +\]$. The basic imaging equaiton is the 2D-FT
in which we have k_x and k_y. This can be sampled by frequency encoding and by
phase encoding. We will find the selective pulse which is representing a sinc
and, next, the preparation gradient ahead of the inversion and the readout
gradients in x and y directions. Please note that when we are applying slice
selecting gradient we are not focusing the globe because rephasing is ????

### 3D imaging

It is terribly slow. We get alltogether the image in x,y,z coordinates and we
want the samples for the 3D-FT. This can be done again exploring all the
possible directions which is not practical at all, which will need to combine
both azimuth and elevation angle. The values which are cos(\theta),
sin(\theta)sin(\phi).. clearly the number of repetition that in 2D was N now
they are N^2. We need to explore the k_x in frequency encoding direction and,
while we need to frequency encoding direction with different ... 


## Part 2

### Visual demo of phase encoding

we can look at the belt as if it's the spin system on one axis. the rotating at
the larmor frequency. in the rotating frame the belt is like it's not moving.
what happens if i switch on the gradient on the z axis? i am subtracting one
part on the left hand and adding on the other hand. left hand spins are slowing
down while right hand are sped up. so we are creating a phase differences that
are proportional to time. that's why the position in the ???. if i switch off i
will keep the phase shifted like freezed. this phase encoding is related to
position even if it was worked in the time domain.

1. switch on at a given time and get the first armonic
2. double the time and get the second harmonic and so on

with this trick i can match the basis function with the harmonics i want. i can
go on with the resolution as much as i want but there's a physical limit. on
small sample we could go on to have finer details. the physical limit --> the
thermal diffusion is working and i will have a mixing of phases caused by
brownian motions of the particles.


in the varying length pendulum: small length --> high larmor frequency.  big
length --> low larmor frequency.


the clinical limit of resolution: there's a tradeoff between resolution and
SNR. we are  limiting the number of harmonics to the resolution that is really
needed to keep a sufficient SNR.
