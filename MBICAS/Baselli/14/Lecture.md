# Lecture 14 --- MRI: Artifacts and anomalies in FE and PE

In the previous lecture we ended up with the concept of k-space. We have seen
the basic ways to explore this k-space (line by line). In echo planar images we
can fill an entire plain in a single repetition but we would be using a much
greater gradient. There are even more complex patterns to fill the k-space, for
instance the spiral one that avoids abrupt changes of gradient. Another is
sparse sampling of k-space to make it more quick. K-space is a general concept
that is used to design a sequence.

What is really happening when we are sampling the k-space? In imaging an
ordinary feature is the limitation of resolution. This is given by the point
spread function (aka PSF). A very contrasted and small spot can be modeled as a
dirac pulse but, after acquisition this spot does not correspond to a point. We
are so linked to the digital representation of things but the limitation is
ahead of sampling. In RX the area of the focal spot cause the fact that RX
projection is not geometrically conical but it creates shadows. If we are in a
clinical RX we require very small focal spot. If we want to make RX microscopy
the focal spot should be even smaller. PSF is defined by the k-space patter,
that limitates the maximal value that we want to measure. This is related to
the dimension of the voxel we want and, in this case, it is somewhat digitally
fixed. Can I go on higher frequecny so to have smaller size? yes but with 2
limitations:

- The smaller the voxel volume, the lower the SNR. We have to get a compromize
	between the 2. Time does influence SNR as well. Actually, theoretically,
	there's a limitation due to molecular diffusion movements: spin system during
	each repetition we hypothesized that it's not changing position but this is
	not really true in reality. MRI microscopy can be done with high gradient.
	When we stop sampling at a given k what we obtain is a sort of squared
	windowing. The effect of cutting is the IFT, the convolution with a sinc
	function. This lead to the PSF. We can overlook the effects of the side lobes
	and cocentrate on the main lobe. This is similar to a bell shape. To be
	mathematically precise this is not a sinc but a Dirichlet function, the
	descrete counterpart of sinc. As everything in the discrete space, it is
	descibed periodical. The sin is calibrated over the period that is exactly
	the field of view. Sampling in one domain means periodicity in the other
	domain: when we deal with sampling in both domains, both domains are
	periodical.
	
Again, the wideness of the lobe is represented in FWHM. Rule of sampling gives
us Delta x_f that can be done for all 3 dimensions. normally, in the digital
representation this is maintained. 1/Delta x_f means 2k_x,max is the Shannon
theorem. Side lobes, actually, by cutting abruptly the rectangunar window, the
side lobes are giving us these effects that we have already mentioned. We can
see the real resolution we have. Resolution is limited when the sum does result
in a single bar shape.

Clearly we can apply windowing, going back to the theory of FIR filters, but
here everything is upside down. We have an entire selections of windows we
might recall from theory and the rule is that smoother windows like hamming and
hann, compared to rectangular windows, have smaller lobes but a broader main
lobe so a higher loss of resolution.

## Signal to Noise Ratio

A single equation can describe it. The study can be done in a very much
sophisticated way.

SNR is proportional to B_0 * \dfrac{voxel volume}{noise volume} \sqrt{T_{acq}}.

In RMS we can show the ratio of power or effective amplitudes. We must say what
it's represented. It would be nicer to represent it in dB. It solves this
ambiguity and gives us the same number in amplitude and in square amplitude
(variances). By convention when we deal with power (square amplitude).
Convention is that for dB applied to power is not 20 log but it's 10 log, so
that the representation is not ambiguous. Here we compare amplitudes.

The higher the main field B_0 the higher the signal. The signal is also
proportional to voxel volume (we can see the compromise between volume and
SNR). Next we have to divide the noise volume, the integral of the sensitivity
function of antennas extended to the volume. Sensitivity does fade so the
volume that contributes to noise is reduced by going away from the antenna. Why
the noise is this that gives us the RMS ... because it's sensible to charges
that are the sources of noises that are volumetrically summed up. Last term,
t_acq, does require some explaination and some examples. This is nothing
different than the usual improvement of repeated measures compared to accuracy
??? This is not the time in which the patient is inside the scanner but a much
smaller time. The sum of all the readout times which undergo and that are used
to reconstruct the image. We must consider only the readout used for just that
image. If we make a 2D-FT with 10 repetitions, that would be the sum over N
repetition of that slice. With PE we have to sum ??? so total acquisition time
is increased sensively. That's why 3D acquisition has a better SNR acquisition
than 2D acquisition done by slice selection. When we make a repetition for 3D
acquisition we activate all the volume and obtain different ??? but it's the
whole body giving the signal. In 2D we are activating just a slice (smaller
signal) but all the body is still contributing to noise.

Let's make some examples.

### Example 1

We repeat the scan many times and in the end we average the data ahead.
Acquisition is improved by square root of number of acquisition used for
average. Same happens for T_acq.

### Example 2

Enhancing the readout duration. Suppose that we make the sequence in which we
don't consider carefully the spin echo carefully but we use the ??? we double
the readout time and, since the number of samples does not change, what we get
is that we are doubling the sampling time. The acquisition time is twice the
previous acquisition time. New SNR is square root ot 2 the old SNR. There is
hidden the usual rule of averages. Not too trivial is to consider also the
bandwidth: we have double the sampling time but have reduced by a factor 2 the
bandwidth, so we can have a better filtering. with half a bandwidth we are
improving SNR.

### Example 3

We reduce resolution a-posterior after the scan. This is also called smoothing.
We want to smoothe the image in spite of resolution. To do this in MRI it means
it's a last resort. A smoothing MRI is really done if we have some old MRI and
we can't repeat them but if we must set parameter s it's better to do that
a-priori. ???

### Example 4

This is the good way to do that. A priori averaging is reducing the signal
window we have and permits us to really have a doubling of SNR ratio. What we
have changed is directly the voxel volume.

## "Zebra" artifacts

Zebra artifacts. These are clearly artifacts. In one image are in these way, in
another image in another and they come up sinusoidal. Clearly this is sampling
that is causing that one of the spots of k-space is completely wrong and it
introduces a sinusoidal noise. There's some sparking in the scanner or in MRI
room. A spike does create a very short RF disturbance which is translated in a
spot in k-space which back-transformed gives us a sinusoid. We can't do
anything against it but just call the technician and see where the problem is.
Particularly if it's due to the antenna it can be dangerous for the patient.
Some years ago Professor gave an idea of a sensor to??? every piece of
equipment inside the shielded room must be tested and control.

## Position aliasing artifacts

When they had not all the support in fixing parameters given by up-to-date
workstations. If the FOV was not sufficiently large. Sampling of k-space is not
done properly. Sampling means periodicity and we can see basically the
superimposition of aliased version of the image. 

## Motion artifacts

Riverberation given by periodical ghosts. These artifacts are in the phase
encoding direction. These are opposite of chemical shift, which happens in FE
direction. Periodicity come up with movement that are periodical themselves.
There's the link between time and space in MRI. If a scan lasts several minutes
we can't ask the patient to make a breath hold and we have to deal with
periodical movements. The heart pulse is a periodical movement itself and can
create artifacts.

Let's understand how it works. This kind of artifact happened in first
generation 2D-FT spin echo. At each repetition we hac one line. Each repetition
is orderly presented over time and, during time we have modulation given by the
breath. Here we represent an amplitude modulation. When we come to
reconstruction we have the real image and, since there's a multiplication in
time we have a convolution. So we have the main lobe which is what we wanted
but the periodicity are creating harmonics. If we convolve the main lobe is
giving us the image but then the other lobes are giving us replicas. The simple
streak is to repreogram the sequency and to fit the k-space jumping up and down
to fill all the rows (kind of random shuffling, generating a random noise). The
result in the image is a sort of "fog".
