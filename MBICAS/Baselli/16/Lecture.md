# Lecture 16

## Slide 1

In the past if clinicians wanted to check the vessel codition after a subject
has reported a trauma, they had to perform planar RX (CT) with contrast.
Planar RX with contrast: looking at the vessels with contrast and this can
product convulsion to the patient that received it

MRI was became really popular later because it could really help in the scalp.
It is not as invasive as the RX technique.

in MRI we use contrast (i.e. contrast enhanced MRI) and in that case we are using
something toxic. the characteristic of MRI to permit 2 types of angiography that
are based on mechanisms that are exploiting internal contrasts. There's also
another system that permits to detect moving object and enhance them without
invasive contrast injection: ultrasound echo doppler. Echo doppler is using internal
physical contrasts based on velocity. it is very much used in clinics. Conversely
ultrasound does not have the three-dimensional properties and are limited in the
visual field. MRI is permitting to look at a complete anatomical contest.
Sometimes the integration is the best thing to do.  From error and artifacts of
MRI, its extreme sensitivity to the movement which is going to create motion
artifacts with structure moving and replicated all aorund the movement and
signal voids (i.e. aorta inside the abdomen). They understood that this
sensitivity can be exploited in some way. it was used to develop 2 kind of
contrasts: 

1. *time of flight (TOF)*: they have worked a lot on MRI. This name is used
   typically by physicians related to particles flying in the space. In our
   context it's the movement of the blood inside the body. what is exploites
   here is the fact that the activated spins are moved with the blood. we can
   design the sequence in order to enhance only the moving spins. it's just the
   same principle of signal voids seen due to the fact that the activated spins
   in aorta were washed out (???). if we do the opposite we can have very
   bright moving.  blood is producing the signal against black background.
2. *Contrast enhanced MRI*: in connection to TOF. Some limitation of TOF can be
   overwhelmed of an injection of a contrast mean.
3. *Phase contrast MRI*. this is really sensitive not to the transport of
   material but to the velocity. it give us less of precision in the anatomy of
   the vessels but is giving us quantitative information about the velocity
   profile and integrating it we can obtain the flow of the vessel. The italian
   for flow is "portata". flux = flow/area.

## Slide 2

**TOF MRA**: with a short repetition time we can differenciate the fast
recovering tissues with a short $T_1$ which should be bright compared to the
long recovering tissue which should be darker.  The not moving tissue will
result black while fresh blood is ready to accept the excitation. Blood inflow
is bringing spins that has not experiencing the repetition so this blood has a
longitudinal magnetization that is the equilibrium one. Being rapid with
repetition and considering that fresh blood is entering in the vessels ???

## Slide 3

*The graph*: we can select an image of just the vein or the artery. What we do to
reconstruct the angiography is to get a slice after another. we are proceeding
orderly in counter current, against the selected flow and we acquire the slide
with a slab of saturation. This is eliminating artifacts and we are eliminating
the signal of veins that are flowing in the opposite direction. slice by slice
we are constructing the volume of each artery passing from that slide. if we
are moving top down we will obtain the vein (by moving in the opposite verse with respect to the flow of the vessel we are able to highlight its signal).

The limitation of this is that since we are moving in 2D imaging we are not
allowed to acquire while waiting for repetition to be ended as we do in 2D
imaging but we have to consider 1 slice at a time. Now the repetition time is
very short (1/10 wrt anatomical images). The slice thickness can't be very
small. we want good definition of the borders but our volumetric reconstruction
is quite bad. We have non isotropic voxel. if we want to have good study of
anomalies in the angiography we need good three-dimensional reconstruction. so
that we can evaluate the risk of breakout of aneurysm. the staircase artifact
is not nice to accept. we have to reconstruct the slices orthogonally to the
vessel (compelled to the axial sections). Both limits are partially overcome by
3D TOF. (???)

## Slide 4

We are performing it over thick slabs to basically have all the structure of
the volume. we have 2 phase encoding. phase encoding is quite long in its
acquisition so limiting one of the axis to the sharply defining of vield of
view ??? limit the number of repetition. anyway the rest of the story is
exactly the same the repetitin time is short. there's a limitiation in the
trick: blood that enters the blod from the bottom and start receiving
repetition to get saturated itself. the blood that needs to be blight is goin
to be dopo un po' black. we can get slabs of 5cm ??? 

in 3d we get an entire piece of vessel and we can have isotropic voxels

## Slide 5

To obtain this image we are not considering the projection, like in ct with the
average value, but we are obtaining the maximal intensity projection (MIP).

## Slide 6

**CE MRI**: Gadolinium can enhance tissues. aggiungi una D al nome: DCE ...

where we have lesions we have a broken barrier. the contrast medium is washed
out in the order of minutes when we can look at the diffusion of the contranst
enhaced inside the tissues. 

## Slide 7
## Slide 8

A second method to use in terms of contrast but not based on trasport of fresh
spins not saturated but on sensing the velocity by properly velocity encoding
factor.

- *First image*: here is shown the bipolar gradient pulse.
- *Second image*: the inversion pulse is performing the inversion of the
  gradient so we dont' have to invert it, the pulse does the job.

$\phi(t = -\gamma x \int\limits_{0}^{t} {G_x(\xi) d\xi})$

In this formula: x of spin is not necessarily a constant. speeds inside blod
spins have a component.

## Slide 9
## Slide 10

We can divide the phase encoding in 2 parts. one related to the initial
position and the other one on the velocity. the effect of a bipolar gradient can
be appreciated on the bottom graph.

the third graph --> quadratic function of time.

depending on the velocity we are accessing we dimensioniamo gli altri
coefficienti.

bipolar pulse permits us to have zero result ???

facciamo lo stesso ragionamento però switchando il gradiente rispetto all'asse
t, calcoliamo la fase e poi facciamo la differenza + - -.

## Slide 11
## Slide 12
## Slide 13

Per ottenere l'immagine finale devo fare l'IFT dei raw data e poi il modulo
perchè non avrò mai un valore totalmente reale.

## Slide 14
