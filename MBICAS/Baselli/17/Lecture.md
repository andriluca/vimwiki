# Lecture 17

## Lecture 17a

### Slide 1

The f in fMRI stands for functional, of course. A great breaktrhough to
neurosciences since they permit us to localize the activity of the brain by
non-invasive or moderately invasive in-vivo methods. Ahead of functional
imaging the only way to understand what was going on in our brain and where was
just leasional. People with great wounds because of war or work has very
selective loss in cognitive function were very important case study to
understand the specialization of areas in the brain. 

After that recordings directly from the cortex (animals because of invasivity).
Sometimes during surgical intervention on the brain with open skull since the
brain does not need anhestesia because it has not sensing of pain, after the
first opening phases the surgeon is able to wake up the patient and interrogate
him/her and this, in many surgical procedure is really important to monitor
directly the possible cognitive impairments in which it's creating the surgery.
Really didn't have non invasive methods for looking the functioning brain from
the outside in-vivo without arming the subject. As we know there's a lot of
evoked potential so electrical recording from the scalp and EEG itself and also
magnetic electroenchephalography (MEC) which can better localize but the
electrical activity is poorly localized anyway. Functional imaging permits us a
greatly localization, mainly in the cortex but after the pioneering times we
could observe also other gray matter structures which are the basal nuclei
(island of cells inside the white matter within the Central Nervous System).

We can see even the spine which is a tiny structure, so very difficult to be
resolved in images. Nowadays most of our increments in the knowledge about
working brain comes from fMRI. Functional imaging started with nuclear
medicine following the perfusion of the cortex by means of radiotracers. Where
the part of the cortex is increasing its activity there is an indirect
hemodynamic response which is claiming more oxygenated blood. So the regional
blood volume and flow are increasing in that area with dynamics that will be
analyzed soon but which is within the time scale of seconds. The time
resolution is quite horrible compared to the spike of a neuron that is 1 ms
long so seconds are **3 order of magnitude longer** that is a completely time scale
that we can observe invasively by microneurographies with very small needles
inserted among the neurons. But advantages of non invasivity and the
possibility to ask to the subject to make really complex cognitive tasks and
observe what was activated by those tasks selectively really gave us a big
insight in the neural mechanisms. Next it came fMRI.

fMRI: they realized that hemodynamic response can be sensed as anomaly of the
nuclear magnetic resonance signal. So in a few years we can say that fMRI
completely overwhelmed functional nuclear medicine, mainly by PET. So fMRI is
now the leading technique (the working horse) when it comes to cognitive research.
By now, we have encompassed the time in which we wanted to look at the
specialization of the single areas and we are trying to go over to a much
complicated problem: looking at the whole brain at work, the keyword is
connectivity nowadays. We here we are dealing with the functional connectivity:
how the activity of distant brain areas is synchronized and correlated. We will
deal in the next lesson with structural connectivity (reconstructing the fibers
which are connecting those areas from the outside and with the use of images).

### Slide 2

*Statistical analysis* because the SNR we do have in observing the hemodynamic
response is not sufficient to observe the single event but we must repeat many
times and in the end we map the statistical significance of the activation of
specific areas compared to the background noise which is present in other
areas. 

### Slide 3

Local brain hemodynamic response: it reflects the neural activity.
Oxyhemoglobin and desoxyhemoglobin are used as natural contrast agents. in this
case it's mainly the oxyHb that is sensed and diversely from a functional
near-infrared spectroscopy (fNIRS) that does combine both the evaluation of oxy
and deoxyHb. We are working in parallel also in fNIRS which has some advantages
and many limits when compared to functional MRI.

Remember:

- diamagnetic, like oxyHb: These kind of materials are characteized by the fact
  that their magnetization has opposite sign with respect to the external
  magnetic field. So these materials are weakly repelled.
- paramagnetic, like deoxyHb: creates field inhomogeneities (less than MR
  signal, about 4-5%). Paramagnetism is a form of magnetism in certain material
  shouwing just in presence of magnetic field. It's producing a magnetization
  having same direction and verse of the one of the externalmagnetic field.

### Slide 4

The basic principles: Brain activity rises up as the oxygen consumption (neurons
are consuming a lot of energy to produce AP, let's think about the Na-K pump
that has to restore the equilibrium of the ions after the depolarization of
membrane). However There's a strict interconnection between the neural
structure and the glia with the local regulation of vessels which renders the
regulation of cerebral circulation challenging and absolutely important. The
claim of cerebral blood flow is even greater than the real consumption. 

Why there's this sort of waste of oxygenated blood? we need to have a lot of
oxygen more in order to bring the needed increase of oxygen through the
blood-brain barrier, inside the cells, to the mythocondria that are producing
the energy, so a gradient is needed and we should be really fast in doing this.
So oxyHb goes up in this balance and pushes away the deoxygenated blood (aka
venus blood with deoxyHb). this is intuitive: if there's a great inflow of
oxygenated blood this means that a lot of deoxygenated blood is moving away
from the tissue. The effect on the RF which are measured in MRI is that the
known homogeneity of the region drops because $`\Delta B_0`$ which we introduced
to explain the effect that in the vivo we have the $`T_{2}^{*}`$, that is much
faster than $`T_2`$ relaxation time, is depending from the microscopic structures
inside each voxel and among those the microvessels are the main ones.  oxyHb is
diamagnetic: it does not change the suscettibility, conversely deoxyHb is
paramagnetic so creates a lot of $`\Delta B_0`$ in the microscopic environment, so
a structure which is more oxygenated has a $`T_{2}^{*}`$ decay which is longer.
If we get by a gradient echo a contrast which is $`T_{2}^{*}`$ weighted (this
should happen with the gradient echo) the solution is found which means that
with an activation after all the considerations done in the slide is more MRI
signal. BOLD (Blood Oxigen Level Dependent Contrast) is the acronym we use for
this kind of response. its localization is very good and we can have resolution
in the order of few mm.

We can't neglect that the change in MRI signal is in the order of 4-5% wrt
normal contrast that can be observed by gradient echo images in the cortex, a
very small change. The change occurs when the subject is doing the task and we
can repeat the task many time and so get it statistically valid.

### Slide 5

the hemodynamic function gets a very long time. if we have a concentrated
stimulus in time (shown in brown), we would see the bold signal reaching a peak
after something as long as 5 seconds. We should compare this with the duration
of a single AP in a neuron which is a little bit less than 1ms. 5000 times more
and after the peak we have also a slow recovery and all the story is getting
about 12 seconds. it's important to say that this is not necessarily deriving
by the fMRI but there was a lot of research which was on animals because
invasive, in which they used optical images of the brain surface observing the
mechanisms of hemodynamic response in the working brain in animals. they not
only used microneurography (behaves well locally but not so well in mapping)
but conversely they used a lot also optical images in the near-infrared
spectrum and they really did know what was the dynamics of the hemodynamic
response. clearly with the birth of functional imaging (nuclear first and MRI
next) they needed a standard which is supposed to be valid in a standard
hemodynamic response function which is normally used to interpter the data
coming from the outside.

### Slide 6

*Experimental design*: we need to decide the experimental sequence in which
the subject is asked to do something in a systemtatic way and at times we can
record. Sometimes the activation of the brain comes from the outside (i.e.
hearing, visual, somatosensory stimuli). The experimenter which delivers the
stimuli and can know exactly at what time the stimulus is given and will be
repeated. As a general paradigm we have normally intervals of time in which
there's stimulation and the ones of rest in order to compare two statistically
different conditions and so get a statistical parametric mapping of the
significance. Other times the stimuli can be related to something which is
doing the subject, so for instance motion and clearly we must solve the problem
of having the timing of what's doing the subject and we can have 2 different
ways:

1. Give a signal to the subject (i.e. sound, light) telling them when they
   should be acting and when they should stop
2. we can ask to push buttons and so on and that will be recorded. 

This can happen even if there's no movement! there are many neuropsycho tasks
in which the subject is asked to think something (i.e. verbal fluency without
moving the lips, for 30 seconds think about animals, stop, think about tools,
stop) Are the areas that were involved to encode animals and the tools the
same? are they independent? We can also work on speech production.  Tasks are
so different and we can look at the significance of experimental paradigms
which contain many combination of the stimuli to see the interference and
synergies of stimuli between them.

The specialization of these experiment grew very much at the point that we
can't put more inside one experiment but the capability of implementing very
complex paradigm is huge and what we can do with the measures in the meanwhile
are:

1. Brain activation: does that area be activated by the task or not?
2. Brain activation in response to different stimuli and conditions: the tools
   which are given to us permit to have appropriate statistical questions. In
   inference statistic it' absolutely important to put the correct question to
   have a statistical answer. "Is the activation in this area different from
   0?" "is there a difference between the activation of this area with a
   stimulus and with a different one?" Also "contrast between different areas
   (which area is more activated in the brain?)"
3. we can ask question about functional connectivity. "The activity of one area
   is synchronous to the activity of another area?" We have understood that we
   have networks in our brains which are different from one another but occupy
   the whole brain so that the parts of these networks are quite distant one
   from the other,
4. effective connectivity is more complex. the question now is: "Is the
   syncronization between A and B modulated in some way by the activation of
   C?".  An example: A is talking with B, C is disturbing the communication.
   the communication that was correctly happening before between A and B is now
   disturbed. There are also cases of facilitation of this communication.

### Slide 7

*Experimental design*: needs to decide the way in which we want the timing.  it
does not include only the types of stimuli we want to combine but the way we
want to have the timing to compare the activity with non-activity. We need to
have a background activity in order to verify a statistically significant
deviation from the baseline condition. There are 2 big choices to be done.

- *Block design*: we can decide to have periods of stimulation (or activity or
  whatever) which comes in blocks. So one is a dummy signal which is simply a
  flag telling us when this activity is on. When this dummy signal is on it
  means we have an activity, otherwise we have non-activity. 
- *Event related design*: we can also let the time free or random. 

this can be our choice or a request imposed by the experimental design.  All
the block designs can be converted in event related design but this is so
simple and practical that most of the time we don't do the use of the event
related design but there are some cases in which event related design is needed
(i.e. in my mind is happening something that we don't expect at a certain point
and we have to push a button which this happens. for example recognition of x
letters when reading letters one after the other. Sometimes it's the
recognition of the same letter repeated twice with just one interval of
presentation [aka one back experiment]; the letter is repeated but not in the
previous screen in the second screen ahead [aka two back experiment]). we are
trying to understand what is the working memory (memory of very short events,
those which are permitting us to read a telephone number and dial it or follow
a conversation with someone else so it's very important). There might be
experiments in which something happens randomly and the subject pushes a button
when it happens randomly.

An example of something that happens randomly in our head is necker cube: it
can be perceived in 2 ways because there is no shadow, no depth perspective and
we will start to perceive it in one of the possible way. after few seconds we
are switching the perception immidiately and start perceiving it in the
opposite way. we can try as much as we want to perceive it in the way we want
but the necker cube changes when it really wants. it's a switching of
perception that happens in our head independently from our will.

Let's now concentrate on the expected BOLD signal. This is important because if
we give this paradigm of activation (block or pulse concentrated) we should not
expect a bold signal which is going from 0 to 1 and from 1 to 0 in a squared
wave fashion but a signal which is the hemodynamic response function convoluted
with this signal.  If i have an event and something happens in the brain but
that happened with a very rapid neural mechanism, however the hrf should start
from that point so this should be those 5 seconds to peak and a tail afterwards
up to 10-12 second. Clearly if this is a pulse the convolution repeats the
impulse response at each pulse. with other signals we have to fully compute the
convolution and as we can see, of course we don't have a jump step from 0 to 1
but a rise to peak which is exactly that of the hemodynamic function and
similarly the drop down to zero with the tail afterwards. we should search for
the BOLD signal and we make a regression trying to find this signal.

### Slide 8

What is the sequence we need? it's echo planar imaging (EPI). it is a very fast
acquisition which is based on gradient echo. Since we have to scan as fast as
possible and since we have a $`T_{2}^{*}`$ contrast this is the correct
solution.


The strategy implies to get one slide per each echo planar imaging sequence and
so we read out the brain activity once per slice and we come back to the same
slide after a repetition time which is very long. typical figures is say one
slice with echo planar image requires hunderds ms. the slice covering all the
brain from cerebellum up to the apex are about 30 so it's common to have a
repetition time back to the same slice of 3 seconds. We deliver an EPI every
hundred of ms but the activation RF pulse which is needed is limiting its
activity only on 1 of the 30 slides. So the repetition time relative to $`T_1`$
weighting is really the time we need to come back to that very slice =100ms*30
= 3s --> a lot of time which does mean that we have no $`T_1`$ weighing. $T_1$
weighing is not a problem, $`T_{2}^{*}`$ is what is providing the signal.

### Slide 9

In this slice we can see the typical sequence. 90 degrees, obviously selective
(we can see that by the wave on *G_z*. This wave has also the refocusing lobe
with opposite sign wrt the previous one). Then we have 180 degrees to get the
spin echo (it is also selective, as we can see from *G_z*, to be more sure to
get the slice) and after that, we move during the spin echo (so the readout)
with the gradient echo strategy. we switch on a very strong frequency encoding
gradient. this is going to destroy the signal due to $`T_{2}^{**}`$ mechanism
that is the even faster decay induced by the dephasing made by the gradient, so
after a while we have no more signal but we can recover this systematic
dephasing of the spins by inverting it, so we invert and go on the other side
and we get another gradient echo and so on an so forth. we don't want to go
back to the same line of the k-space because this was already sampled. what we
do is to give very shortly a "blep" of phase encoding (the gradient *G_y*) so
that we move on parallel lines (like a farmer with a tractor that is plugging
the field very fast). At that point we have all the row data useful for that
slice. This takes hundred of ms and we will be back at the same slice after a
repetition time which is 3s. Nowadays the efforts to go faster and to cover
more volume of the brain to overcome the limitations in fMRI were of course
very big in the last decade and nowadays we have $`T_r`$ of about 2s which is
better with the possibility to cover all the brain structures. obviously with
3T magnets we can have more SNR.

### Slide 10

Here we can appreciate the fact that each slide (actually each volume) are
sample peroidically at one repetition time with $`T_{2}^{*}`$ weighting. We have a
fair anatomical delineation which must be used and also on top of this we have
small changes of the contrast due to the BOLD differences which are in the
order of 5%. Clearly we have to do a lot of preprocessing in order to start
analysing this. 

The first preprocessing is an image re-allignment or generally registration:
simply the head of the subject during the 5 minutes in which hundred of samples
are taken in fMRI sequence, can move a little bit and this is terrible for fMRI
because partial volume effects changing with movement can be much more than
that 5% of BOLD contrast very easily. Suppose that between the white matter and
gray matter there's a change of contrast of 10% we can reach the 5% by of
partial volume effect just with the movement of half a voxel. Suppose that the
change of contrast (between grey and white matter) is 50%, we need a movement
of 10% of pixel size. with pixels of 2mm we can understand that very small
movements produce really big effect. Allignment is mandatory and it is even not
sufficient and we can do more than that. A rigid registration does need a
rotation matrix in 3D which has 6 degrees of freedom (3 for translations and 3
for rotations).

### Slide 12

We need some preprocessing and these are the classical tutorial appearing with
statistical parametric mapping.

1. Reallignment.
2. Smoothing: very used before now not so much. For statistical significance th
   epresence of noise is really a lot harming and smoothing out all the edges
   renders the distribution of grey levels more close to the gaussian
   distribution which is condidered in the next statistic
3. normalization is quite common. this happens with a template that is an atlas
   obtained by many subjects. nowadays the most popular one is the MNI
   (Montreal neurological institute atlas) made by 150 normal brains which were
   fused together, registered one after the other and then averaged. normally
   we put the single subject brain in the space after a warping of the image
   (we must know which kind of deformatoin we have given to go back to the
   original). We have lot of advantages. it is compulsory if we want to compare
   2 groups. Two groups must be compared single area by single area in a common
   space to everybody (both the patients and the controls). For a single
   subject the transformation of the image in the normalization can give to a
   labeling of the areas to the image. So when we are back in the original
   space we get a labeled brain compared to the normal areas.

After preprocessing we can proceed with statistical analysis which is done by
GLM (general linear model). It is nothing different from a multivariat
regression. posing statistical question as: "is the crosscorrelation with
expected bold signal significant in that specific voxel of the fMRI?" and
importantly the question is posed voxel-wise so the resolution of that is in
principle that of the voxel. however the resolution is larger (2mm^3) and it's
not the real resolution because we have to smooth ahead of that. In the end of
the day we look at the significant response to the stimulation paradigm and we
map the statistical significance of that. For instance in the slide we have
most likely a visual stimulus which is activating visual areas in the back,
occipital region of the head. this is going to give us a lot of information
both general with groups of patients or even on single subject. The big
difference between the old times of functional nuclear medicine and fmri is
that in the first case we would not be able to have individual significance but
they had to but together 10-20 subject and consider a group significance. with
fMRI we can have individual significance and this is really useful for surgical
planning. let's suppose to ask the subject to have a verbal fluency task (think
about words, then stop, then words and so on) this is going to activate the
Brocas area. if a surgeon needs to make an intervention that will necessairly
get away part of the brain the possibilities of recovery of the brain are
suprising and there are great areas that can be eliminated with negligible
changes in quality of life but there are also very specialized portions
(brocas, the one of dominant hands) that shall not be touched.

### Slide 15

Let's talk about the mathematics behind the scene:

$Y=X\cdot \beta + \varepsilon$

- Y: vector of samples.
- $`\beta`$: linear regressor.
- $`\varepsilon`$: error.
- $`X`$: design matrix.

For each voxel we know that we are measuring a vector of samples Y in the MRI
signal containing some BOLD contrast we want to detect. we have linear
regressor beta and an error epsilon. The design matrix should be very well
designed. The significance activation must be tested on the significance of
value of these parameters beta. We must understand which parameters are
significantly different from 0. They are picking up with their significance the
significance of one of the columns of this design matrix. This is an estimate
with residuals and the common strategy is the least square significance. The
inference hp is that the errors are gaussian with is 0 deviation (non dovrebbe
essere 0 mean?) and $`\sigma^2`$. c'è un typo nella varianza dell'errore.

**Errata corrige**: $`\varepsilon \sim N(0, \sigma^2)`$

Statistical parametric methods which permits to see what are the areas of the
brain which are significantly activated by our task. The model is that of a
linear regression given by the generalized linear model. Generalized in the
sense that we insert in the design matrix not only the interested regressors
but the confounding ones in order not to have equivocation of something which
can be correlated but is something else. Typically in fMRI it's the motion so
we have seen that we put in the matrix parameters of translation and rotation
which were computed in order to align all the volumes we have acquired.
Importantly the output of this generalized linear model is the signal inside
the single voxel: this is a voxel-wise analysis done in parallel for all the
voxels inside the brain volume. So actually we can mask and exclude all the
parts that we are not interesting to us and keep just the gray matter. So it is
voxel-wise and this has important consequences:

1. The inversion of matrix, which is the heavy part of computation, is the same
   for all the voxel (the only thing that changes is the output) but conversely
   we have to correct the statistics for repeated measures. It's not the same
   to ask if there are significant differences between two groups if we get one
   single measure or a lot of measures. The reason is very simple: suppose that
   we have a group of 20 subject getting a drug (or with some disease) and 20
   subjects which belong to control group. Suppose that we are looking to just
   a single feature the number of subject is reasonable to test a significant
   difference of 5% (1/20 which is the most common threshold for significance).
   Conversely if we have got many measures it's clear that just randomly
   something different should come out from the sample analysis because of the
   limitation of the samples. so the correction of the probability threshold
   must be undertaken and there are several methods. Anyway we have many
   statistical analysis made in parallel.


### Slide 16

The design matrix: We need as regressors x, y, the expected bold signal which
is those blocks we saw before or the pulses properly convoluted by the
hemodynamic response function. Here we have a paradigm of activity in white and
rest in black. the question is "is parameter beta_1 signifiacnt?" in this case
we can say that specific voxel was activated by that particular stimulus. Very
often there is some redundancy in the design matrices. we can do anything
without repeating byt they prefer to repeat. Having not a full rank matrix (if
i have that is linearly dependent from the second one i wouldn't be able to
compute the inversion of this algebraic problem because we have a redundancy
and non full ranking). We have the good solution is the one of Moore-Penrose
solution. What is really important is to understand that aside from the columns
which are representing the stimuli and there can be many combined in various
fashions, we need also to have other regressors which are presenting
confounding effects:

1. the mean value: it must be subtracted and it's easy to demonstrate that by
   doing a multiple regression in which you may regress also a dummy input
   which is a constant (1) is exactly the same as to subtract all the constant
   ahead of doing the regression. so center the axis onto the baricentrum of
   the cloud of data. So we need to insert a constant value, otherwise we
   should subtract the mean from each signal which is considered both y and
   both the inputs. This is useless in such a context with a lot of data
   excetera. it's much easier to put a regression towards a constant.

Other confounding effects can be many. here are shown the main one in MRI
considering a single subject and these are the rototranslation parameters (at
the center of the image) which we use to allign the various volumes and which
we put again inside the regression matrix. we aligned the various instants so
visually the motion is eliminated but there can be some residual which is
depending just from the motion, so by putting these confounding effects
expicitly in our regression we can cancel them. In the old funcional nuclear
imaging they used to put much more attention to slow drifts so they had not
only the constant but also inserted small drifts that may happen in nuclear
imaging. in MRI this is not a problem. if we consider group (passing from the
first level analysis with just single subject to groups of subjects): each one
should have its own average and so we will have one column of constant marking
each single subject and we may decide to add for instance the age. Suppose that
we were not able to balance the groups comparing patients to controls so age
can be a confounding effect and for this reason we put it explicitly inside the
regression of the design matrix and so we separate out those effects which are
trivially related to age etc etc.

We need to represent at least once but no problem if we repeat the expected
bold response if there is an activation with the stimulation paradigm and
confounding effects.


Here we have the usual visual representation of the design matrix. Clearly the
first columns are related to the most simple instance: one activation and one
period of rest but we could have many if we had protocols in which we are
combining several activity and stimuli. Conversely we have also the confounds
which in fMRI, as said, are normally limited to the roto-translational
parameters. Of course we have also the mean value estimate which is given by
the presence of one column which is a constant (typically one): this is the
trick to have exactly the same result as subtracting the average values from
the data ahead of the regression and the result is exactly the same. This
method is giving us a lot more flexibility and generality avoiding to make a
separate analysis about the averages and the correlation.

### Slide 17

We can even decide that we don't trust the canonical HRF and in this case the
story becomes more complex because we have to estimate both the responses and
the local shape of the hemodynamic response function. In this case each of
these imput lines should be replicated three times and we should insert:

1. convolution with HRF
2. convolution with derivative HRF
3. convolution with second derivative HRF

This means a lot more statistical questions we can ask but a lot more
statistical power and this is done only when it is really needed.  We expect
that the HRF is like the one in red in this slide but it can shift over time
(some small delay). the first derivative is, with the function itself,
describing in all the possible combination (also time shifts). Second
derivative means that the HRF can be modified by some more or some less
dispersion and this is given by considering also the combination of the second
derivative.


Normally the standard hemodynamics response function is used but in some
special case we can convolve not only by the HRF but also by its derivative and
the second derivative. This means that we have tripled the number of main
regressors, which of course requires much more statistical power to give
significance: we are looking at some details which is normally neglected
assuming that in each part of the brain, in each individual, the hemodynamic
response function is more or less the same one.

### Slide 18

Let's come to the statistical parameter estimate.

Y regressed over X (here we are in imaging and we represent matrices as
images), the vector of parameters and the vectors of residual.


So we are now facing the problem of estimating for each single voxels these
parameters of regression $`\beta`$. The objective function is, as usual, the
least squares of the errors, that is minimal variance of the residuals of this
equation.

### Slide 19

We want to look at the statistics of the problem. this is a classical prooblem
of inferential statistics: *ask a question and try to inference from the data
whether the question is correct or not*. One very important point is that we
should never put the question in a positive way: "is there a difference?" it's
not good in statistical inference. the question we should pose in inference
statistics is: "Is it likely that with my data there is no difference?". if we
change our point of view in this way we will immediately understand the
statistical inference. So the error of course is defined as a residual that is
the difference between what we predict given $`\hat{\beta}\times X`$ and what we
really have measured in that voxel at each time sample (*error formula*). So we
want to minimize the residual variance or power. This is a classical problem of
multiple regression for which we have immediately the result beta_hat without
the need of any iteration but by a close formula in which we are using the
pseudoinverse of the design matrix X. here the pseudoinvese is represented in
its most easy way which is: 
1. take the inner product of the design matrix: this is giving us a much
   smaller squared matrix of dimension $`[p \times p]`$ where $p$ is the number
   of parameters so the number of columns in matrix X. conversely the design X
   is very big with p columns and N number of samples (hundreds of rows). this
   lead us with a quite small matrix which should be inverted and next we have
   to multiply again by X transposed and by the output vector Y.

*Exercise*: compute the dimension of parameters vector.  $`\hat{\mathbf{\beta}}
= (\mathbf{X}^T \mathbf{X})^{-1} \mathbf{X}^T \mathbf{Y} \rightarrow
([p,N]\cdot [N,p])^{-1} [p,N]\cdot [N,1] \rightarrow [p,p][p,1] \rightarrow
[p,1]`$. We have as a result a column vector of dimension p.

The dimension of the vectors should be congruent throughout this and give us a
small vector of optimal parameters. Optimal in the sense of least squares and
since it's an estimate we are callng it $`\hat{\beta}`$. 

In many multiple regression problem (quite always with the way we compose the
design matrix) this X matrix could be non minimal rank --> this means that the
inner product is not invertible and the reason why it happens is that for
designing the next inferential analysis it is quite simpler to repeat for
instance some column (not exactly but say "activation = 1, rest = 0" and also
the opposite as we have seen previously). Also the constant value may create
problems about that and so how can we solve this? the standard solution which
is implemented in all the statistical parametric mapping packages (MATLAB) is
that to revert to the Moore-Penrose pseudoinverse: it is the usual trick we
have seen in the beginning of the course when we had to solve a completely
different problem which was the Discrete to Continuous theoretical solution of
reconstruction from projection. also in that case but in a different context we
used Moore-Penrose pseudoinverse which means that since we have an infinite
number of equivalent solutions we had a further regularization constrain and
the regularization is that the parameters have miniimum variance --> the
parameters vector is constrained to be as much regular as possible. we will
understand immediately that this is not a problem because we are not
interested, in this context, to the absolute value of these parameters but we
are interested in their statistical significance when deviating from the null
HP that is parameter value equal to 0 (beta=0?). Suppose that we have parameter
$`\beta_1`$ and $`\beta_2`$ that are providing the same regression component. let's
suppose that a solution could be $`\beta_1=2, \beta_2=0`$. also the opposite
could be true $`\beta_1=0, \beta_2=2`$ . so with the M-P logic we will end up
putting them equal one to each other, so minimal variance -->
$`\beta_1=\beta_2=1`$. We could say the same for $`\beta_1 =  -1 , \beta_2=3`$,
$`\beta_1 =  -1000, \beta_2=1002`$. solution can be infinite but with this
constrain of M-P of maximum regularization (so minimal variance of vector of
parameter) we are out of this problem.

### Slide 20

now we are at the very last point of the statistical parametric mapping that is
the possibility of having an anatomical map which is marked by false colours
indicating the statistical relevance of the activation.

### Slide 21

we need the possibility to make different questions.  It would be not so
practical to look at the entire $`\hat{\beta}`$. 

we would like to have the statistical inference done on a *scalar value* and so
we introduced the concept of the contrast vector. We will eventually, in the
end, do **the statistical inference on the inner product of the contrast vector
and the parameter vector**. This gives rise to a scalar statistical variable.
we shall never forget that both the vector $`\hat{\mathbf{\beta}}`$ and the
scalar value $`c^{T}\cdot \hat{\mathbf{\beta}}`$ are indeed statistical variables
because if i had the possibility to make the same measures on the same subject
in the same condition for random changes i would always obtain slightly
different values of the regression parameters because i have got different
measures in each repetition. That's why if $`\hat{{\beta_{1}}}=0.1`$ is that
different from 0? If we put it in this way the question does not make sense. we
**must compare the deviation of beta_hat_1 from 0 with an estimate of its
statistical variance**. we are not able to get the variance by repeating and
repeating, of course so we must use the statistical inference both to estimate
the most probable deviation of the mean value and also of its variance. 

This trick of getting a scalar by the mean of a contrast vector permits us
several statistical question on the same experiment! For instance:

- *first example* is the second parameter different from 0 significantly?
  clearly i would put all zeros in the contrast vector except for 1 in position
  2 which is picking up the second parameter in doing the scalar product
  (*first example*).
- *second example,  the logic or*: supposing that we want to know wether either
  the condition which is related to parameter1 or the condition or task which
  is related to parameter2 (that could be different) are activating something
  independently. the contrast would be one for the first two parameters and
  zero in the rest of the contrast vector. in this way we are testing trivially
  the sum of the two parameters which are addressed.
- *third example*: suppose that we are providing a visual stimulus: sometimes
  the pattern is red and some other times the pattern is in green. Of course
  there will be some of the pathway in the visual recognition which is common
  in both so it may be interesting to ask ourself what's in common to the two
  stimuli. Conversely we could also ask ourself whether one kind of stimulus is
  different from the other one or, better to be said, which area is
  discriminating between the two. it might be that some part of the pathway is
  common but in the end if i can psychophysically recognize the two stimuli and
  discriminate them, there must be some part of the brain that permits me to do
  that. So i will use the contrast -1, 1 --> making the difference
  $`\beta_2-\beta_1`$ 


### Slide 22

let's see the statistical test. Our statistical inference is considering the
null HP of $`H_0: c^{T} \beta = 0`$. if the theoretical value which is hidden in
the phenomenon was 0 i would not...


Statistical inference: we recognized that even if the theoretical statistical
value we can't measure had an average of zero, anyway it would have a
dispersion around zero. So we have to fix a threshold of small probability for
the tails which are going away from the value 0 just randomly. So we accept a
probability of saying "yes it is different from zero" even if it is not. We may
never have zero probability. The probability however must be very small:
normally it is accepted the probability of 0.05 to have statistical
significance. 0.01 is even better. 0.001 is very significant. This value is the
probability of having a type 1 error.

We must understand how the statistical distribution of the error is. Let's
consider first that the inner product of the contrast vector times the vector
of parameters comes with a normal distribution because it's the linear
combination of Gaussian variables. Are those variables gaussian? Indeed yes
because we did as much as possible to make them gaussian and one important part
is that we had a smoothing which did some linear combination among nearby
voxels and so this was done also to increase the normality (so the gaussian
distribution) of this all signal inside the image. Actually we must take into
account that the estimated residual variance has a probability distribution. We
did minimize the $`\sigma`$ but this is just an estimate of the theoretical
residual and so we must admit that our variance estimated $`\hat{\sigma}^2`$ does
have a $`\chi_{N-p}^{2}`$ distribution with N-p degrees of freedom. N is the
number of measures and p the number of paramterers included that they are
relevant to the average and so this is known by statistical inference to be the
statistical distribution.

### Slide 23

if we consider this statistical variable which is the deviation normalized by
the sigma (square root of sigma squared). We recognize that at the numerator we
have a normal distribution and at the denominator we have a $`\chi^2`$
distribution and this means that the final distribution is the Student's t
distribution. This distribution is not exactly Gaussian but pretty similar: it
is bell-shaped, it has tails on both sides, it's symmetrical, it has no skews.
However it's more correct to use the Student t-test. These packages are
automatically doing the t-test for us. One may wonder by looking at the
formulas how the hell we can consider the theoretical $`\beta`$ which we do not
have and which we are just estimating with the $`\hat{\beta}`$? We are
considering the null HP so the situation in which the theoretical $`\beta = 0`$.
This means that also the scalar product with the contrast vector is zero. The
real question is: "How probable is that even if $`\beta`$ is zero i can go away
from that?". we know the distribution and the area under the curve of each tail
at each level of distance from zero. So we will put a threshold where we have
one tail with 0.25 of probability on the two symmetrical parts --> two tails
t-test is giving the probability of only 0.5. We are well aware that if we get
a statistical difference 1/20 we are saying that it's significant but this
significance was just that given by random errors. There is no better way
actually. It's very difficult to have some statistical difference in our data:
many times we don't reach the threshold of the statistical significance and we
must publish and explain that there was not the statistical difference we
expected. This is the scientific method.

### Slide 24

here is explained a little bit better as we were saying in the previous slide:
the question about the tails, the thresholds and so on.

### Slide 25

In the problem we are considering the issue of multiple measures is really
important because we are working on whole brains with thousands of voxels (we
are saying thousands because we are able to mask many, otherwise they would be
tens or hundreds of thousands). So we need to have a statistical correction. 

- The most severe statistical correction is the Bonferroni one: we have to
  divide the significant probability by the number of measures. For instance if
  we are testing 1000 voxel instead of 0.5 we should test as significant 0.0005
  (0.5/1000). it's too much severe since we have done a lot of preprocessing,
  smoothing and so on by which we did not make the single measures independent.
  The activation of areas is not per se independent but we have full clusters
  of voxels which should be activated. If we say that we did no correction the
  paper would be immediately rejected: it's bullshit without correction. This
  came clear some year ago. There was a debate about corrected and non
  corrected significances and some mad guys present a paper in which they
  presented significant results without correction but those result came out of
  a dead salmon put inside MRI. That was a provocation to say that without
  statistical correction you will always get some "apparently significant"
  statistical result.
- False Discovery Rate (just to mention, not really important but more popular)
- Small volume correction: something which is not an alternative but which may
  permit us to get more statistical power. when we have solid reasons to limit
  the number of examined voxels clearly, we can understand that for the
  Bonferroni correction, the coefficient by which we must divide for having the
  threshold of probability is obviously reduced.
- Cluster size correction: This is a correction that rely on a really
  complicated statistical theory (random fields). This is done very
  empirically. If we have a single voxel which is activated but isolated we can
  cancel it out. if conversely we have a cluster of 10 voxels all together
  resulting significant we can keep that cluster. It's a kind of machiage of
  statistical parametric mapping in order to concentrate not only to the
  intensity of the deviation but also to the size of the deviation.

### Slide 26

After that, we have these nice representation as output. A typical feature of
these packages is that to give us many possibility of representation. For
instance this (*first image*) is called the "glass brain" because we can see it
in transparency just evidencing the activated areas or various type of volume
rendering.

Normally the color code is that of having the anatomy in gray while the scale
of significance in a heat map colours. These packages are also including
atlases. As said, Montreal Neural Institute (MNI) atlas is nowadays the most
popular one and this is also related to the brodmann's areas. we can have
reports of areas according to brodmann or to other some kind of parcellation
and catalogation of cortex which are permitting us to tabulate the outcomes.

When we are thinking about Brodmann areas it's interesting to find that they
were defined in an architectural way but later scientists they have also a
functional meaning. This is why, when we do fMRI, we refer to Brodmann area.
What is strange is that they weren't defined on functional features by any
means but they were defined by the *cortex architecture*, so on structural
features under the microscope, post mortem.

Architectural --> the number of the layers in the cortex are always the same
but depending on what an area is doing the dimension and the percentage of
cells that can be represented is changing even a lot. For instance the visual
cortex is called "striate cortex" because under the microscope we can see a big
white stria which is given by the fourth layer in which a lot of mylinated
fibers are entering because in that area the input is the most important part
of the job for the primary visual cortex. Anyway this concept is really
important: *strict correlation between structure and function*. Even if
Brodmann was not up to look in an experimental way at what a specific area was
doing, still he was able to catch areas which are different on the structure
but which are also different and very specific to the function.

## Lecture 17b

### Slide 3

we can invent everything in the experimental design. For instance this is a
typical scheme for a cognitive design and of course the MRI acquisition should
be equipped with proper means which can be permit us to both deliver stimuli or
order to do something (a beep, a flash to tell to perform a movement) also to
record events. there are specific equipment for example glasses projecting
images, headsets delivering sounds and so on. clearly they are ad hoc hardware,
built to work under the condition of really strong magnetic fields of MRI
scanners and that do not create interferences with RF.

### Slide 4

it should be equipped with proper means to deliver stimuli and recording the
signals. this is something that must be compatible with really high magnetic
fields of MRI and don't create interferences.

### Slide 5

SPM vs FSL.  We have wonderful packages that are growing and growing. This is
an example of the screens of the first one that was built (Statistical
parametric mapping SPM) produced in London but the competitor of them which is
overwhelming the old SPM is FSL which was developed in Oxford.It's much like
soccer teams (there are fans of both the methods) but both are continuously
growing with lots of tools not just for the basic fMRI analysis.

### Slide 6

we can look at the signal, we can have the reallignment translation and
rotation parameters which of course is good because  suppose that at some point
something goes terribly out of range we can just throw those data away.

### Slide 7

Prerpocessing and normalization: It's important to compare different planes.
Look at the lack of data in the top part of the above representations: our
problem is really that of catching enough of the brain. We have to go so fast
that the number of slices are trying to cover all the brain's structure of
interest we have to address but the tradeoff between time and volume and SNR is
really a limitation and so a lot of work is done in order to have more precise
signal, bettere SNR and also faster sampling covering more volume, little by
little always improving.

### Slide 9

in here we are putting again the roto-translation parameters even if we have
corrected for them but there could be something odd which is correlated exactly
to the movement and we want to separate in our statistical inference.

### Slide 11

here the contrast vector is investigating the difference between the first two
vectors.

### Slide 12

we get the results: we have an activation map by Brodmann areas nicely labeled.

### Slide 13

Activation in frontal lobe and in parietal lobe: memory vs attention. The
discussion about the results are never trivial.

### Slide 16

There are some example of possible tests: visual motor task, language task,
working memory (random sequence of letter and we have to recognize when one is
happening two frames ahead). show something emotional and look at the limbic
emotional structure.

### Slide 17

This is surgical planning: something which has a lot more to do with our
course. In surgical planning in the brain looking at the position of the most
important areas, by fMRI, is nowadays in some type of surgery, a standard. For
instance, Broca's area is not exactly in the same position in all the brains
but crests and sulci is highly differenciated from one human and another. A
growing tumoral mass may displace some areas which is still functioning but
moved to position that is different from the normal one. we have then to
investigate what is continuing to work around the tumoral mass we have to
remove.

### Slide 18

Epileptic focuses are one of the investigation which could be addressed. here
the reason why is that surgical planning must be done in order to remove the
epilectic focus without harming the important areas of the brain. this is the
very last resort for very severe epilepsy after years of trying with pharma and
without the possibility to getting rid of that with less invasive methods.

### Slide 19

Rehabilitation effects: After stroke or in neuroreinnervation we are perfecly
knowing that the brain has plasticity and can really well readapt even after
traumatic events and this can be monitored.

### Slide 20

Unfortunately not every pathology can be solved. we can monitor the state of
progression of neurodegeneration in Alzheimer's disease. Look at how the
ventricles (*in the middle*) and the aracnoidal spaces (*at the base*) are
increased. here there's clearly a very evident atrophic condition. but in this
atrophic brain what is it working and what not? clearly the anatomy must be
integrated with the functionality.

### Slide 25

we can move to something more complex which is functional connectivity. Here
the problem here is how the activity of the two different areas is correlated.
Again linear regression and cross-correlation is used but at this point not
with external tasks but inside the brain between two different areas.

Actually this is done most often in a resting state, so there is a whole
chapter in which we look at the activity of the brain without tasks (the
so-called resting state condition) because actually our brain never rests, it's
always active, even if we are trying not to think of anything, which is what is
typically asked to the subject (also trying not to fall asleep). Strangely
enough there are structures with quite far connections inside the brain which
do organize working together. so we may extract, by blind source separation
Independent Component Analysis the resting state network, which are quite
specialized with areas which are involved in activities, apart one which is the
default mode network, which is activated only when we are doing nothing. It is
the stream of consciousness in our brain. with this kind of method we have to
revert to ICA because here we are without any anchorage to external events, we
don't have stimuli but we completely blindely analyse the data and look in the
dynamics of the data what of systematic is present.  This is a set of tools
which in FSL package are a lot more developed because this kind of analysis was
born in oxford and also the analysis in the ICA of resting-state networks are a
big part of the human connectome project.
