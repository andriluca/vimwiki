# Computer Aided Surgery

| Lecture                     | Topic                             | Revised | Difficulty | Comprehensible Handouts | References | Days  |
| :---                        | :---                              | :---    | :---       | :---                    | :---       | :---  |
| [01](Baroni/01/Lecture.md)  | Introduction                      | Yes     | Normal     |                         | 01         | 08.04 |
| [02](Baroni/02/Lecture.md)  | Classification of imaging devices |         |            |                         |            | 12.04 |
| [03](Baroni/03/Lecture.md)  | Imaging and DICOM (1)             | No      |            |                         |            | 15.04 |
|                             | Imaging and DICOM (2)             |         |            |                         |            | 19.04 |
| [04](Belotti/04/Lecture.md) | Seminar 1                         |         |            |                         |            | 20.04 |


| Day   | Link                                                                                                                     |
| :---  | :---                                                                                                                     |
| 08.04 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=1dcb09b6333c423a8791fe80201937a4                      |
| 12.04 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=36957ed0ef3841258fb5df37c85cdb31                      |
| 15.04 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=bd2ef742d368432ea3c1efd672187087                      |
| 19.04 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=93d868e5c81745af951050d3480596a6                      |
| 20.04 | https://politecnicomilano.webex.com/webappng/sites/politecnicomilano/recording/d769707c26a642fe8cf03d4e9e39a3e9/playback |
| 26.04 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=c75d6a9dfd88468f845742799ad8b79d                      |
| 29.04 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=ee6d47026825423780f1063d1df9565d                      |
| 03.05 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=647fa2fac16b47e6a975cee2635dbd52                      |
| 04.05 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=9021081d262b916089f3a78accd1a572                      |
| 06.05 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=da5587611cae4f2793fd6bb11bfd3727                      |
| 10.05 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=a7c73606e7674c8792931f71f5f5afa1                      |
| 11.05 | https://politecnicomilano.webex.com/webappng/sites/politecnicomilano/recording/b524fa30948210399fde00505681472d          |
| 13.05 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=6901a5ea7ce14d6b9f9cdb2f646b23f1                      |
| 17.05 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=ae857e38a51a43dabeb2e16cab1db4da                      |
| 18.05 | https://politecnicomilano.webex.com/webappng/sites/politecnicomilano/recording/bb2807f69a021039bdd900505681b7cf          |
| 20.05 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=86650213623c438886285f4882358ccb                      |
| 24.05 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=540f6d8621d8475892978d2eb3bef726                      |
| 27.05 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=fd627a30ab024dbc82318fb03a8a6e68                      |
| 01.06 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=93f0011b59b91c18836f4aa9ef10bafd                      |
| 03.06 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=552791ebac6917ceecbc93c2fe1212f1                      |

## Notes

Baroni3pt2 is not recorded from the beginning.

# Esamination procedure

Oral exam. See Baroni1.

# Methods for Biomedical Imaging

| Lecture                       | Topic                                 | Revised   | Difficulty | Comprehensible Handouts | References     | Days                      |
| :---                          | :---                                  | :---      | :---       | :---                    | :---           | :---                      |
| [01](Baselli/01/Lecture.md)   | Multi-Dimensional FT                  | Yes       | Normal     | ?                       | 0, 1           | 22.02                     |
| [02](Baselli/02/Lecture.md)   | Filtered Backprojection               | Yes       | Hard       | Yes                     | 2              | 23.02                     |
| [03](Baselli/03/Lecture.md)   | Reconstruction from projections       | No        |            | ?                       | 3              | 25.02                     |
| [04](Baselli/04/Lecture.md)   | Numerical methods (ART and OSEM)      | No        |            | ?                       | 4              | 01.03                     |
| [05](Baselli/05/Lecture.md)   | Analytical reconstruction methods     | No        |            | ?                       | 5              | 02.03                     |
| [06](Baselli/06/Lecture.md)   | MRI Scanners                          | Sbobinata | Hard       | ?                       | 6, 2 Video MRI | 04.03                     |
| [07](Baselli/07/Lecture.md)   | MRI Physics                           | No        |            | ?                       | 7              | 08.03                     |
| [08](Baselli/08/Lecture.md)   | MRI Bloch equation for NMR            | No        | Hard       | ?                       | 8              | 08.03 (22:11 - end)       |
| [09](Baselli/09/Lecture.md)   | MRI Bloch equation for RF excitation  | No        |            | ?                       | 9              | 09.03                     |
| [10](Baselli/10/Lecture.md)   | MRI signal detection and demodulation | No        |            | ?                       | 10             | 09.03                     |
| [11](Baselli/11/Lecture.md)   | MRI characteristics                   | No        | Very Hard  | ?                       | 11             | 15.03                     |
| [13](Baselli/13/Lecture.md)   | MRI contrast                          | No        | Very Hard  | ?                       | 13             | 15.03                     |
| [12](Baselli/12/Lecture.md)   | MRI signal localization (1)           | No        | Very Hard  | ?                       | 12             | 16.03                     |
|                               | MRI signal localization (2)           | No        | Very Hard  | ?                       | 12             | 18.03 (start - 40:26)     |
| [14](Baselli/14/Lecture.md)   | MRI Resolution, SNR and artifacts     | No        | Hard       | ?                       | 14             | 18.03 (40:26 - end)       |
| [15](Baselli/15/Lecture.md)   | MRS and Chemical shift imaging        | No        |            |                         | 15             | 22.03                     |
| [16](Baselli/16/Lecture.md)   | MRA: Angiography                      | No        | Hard       | ?                       | 16             | 23.03                     |
| [17](Baselli/17/Lecture.md)   | fMRI: functional MRI (1)              | Yes       | Hard       | No                      | 17             | 25.03                     |
|                               | fMRI: functional MRI (2)              | Yes       |            | No                      | 17             |                           |
|                               | fMRI: functional MRI (3)              | Yes       | Very Hard  | No                      | 17 (15-end)    | 29.03                     |
| [18](Baselli/18/Lecture.md)   | DWI and DTI (1)                       | No        | Very Hard  |                         |                | 29.03                     |
|                               | DWI and DTI (2)                       | No        | Very Hard  |                         |                | 30.03 (missing recording) |
| [19](Baselli/19/Lecture.md)   | Image registration                    | No        | Very Hard  |                         |                | 30.03                     |


| Day   | Link                                                                                                |
| :---  | :---                                                                                                |
| 22.02 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=208a17dfd66b477c81992176cf3ef446 |
|       | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=24f15f38e6f14fea85a17f3ce83821fe |
| 23.02 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=e1f8d5ea94684ea8ba7becbd6ae7b1e1 |
|       | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=f8b25759df74447299802ab5408af09b |
| 25.02 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=68515bb04c9d4b01aa0f3887a011f17c |
| 01.03 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=c564c6a9270842998464dbbe50bbfe56 |
|       | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=69665ef43ab4445b83bbda83350ecb7d |
| 02.03 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=9b7c0400912944a5bfdd74cbc1f975a9 |
|       | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=a21a96a843fc4039adc7cabf6f056e3b |
| 04.03 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=2ed844651de043a1b8119626e8b04236 |
| 08.03 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=1ba28e9369fc48fda2ef4c353a049749 |
|       | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=6a44708f4f4542c6b86a6f4459c2286b |
|       | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=21d5facf285c427b9e98be09c128dfd5 |
| 09.03 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=72873da84aaa49dba15fc9cd70d4fe4e |
|       | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=76c23221323641ffab3ee208cb5c3c3e |
| 15.03 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=16c22c8d688848069c36d96c839aa5eb |
|       | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=bb66c63d51c84d3a918569d8556d2090 |
| 16.03 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=4e07792ade4d41139d0f3956074158c1 |
|       | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=aba1d8727bc94b2f9e2cc8f06fb9447a |
| 18.03 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=80148f5ea76545ada269cf2173752ce7 |
|       | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=ada15b3818464ac0bacfa7be8b76e317 |
| 22.03 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=186adb474b8c460f9366688ef2d89512 |
|       | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=9b100bd2bbd24ea0807803780a4baa2b |
|       | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=4e5f03f24bbc40a09cd1f6613911b7b6 |
|       | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=8f8f871069e34e4787fcccbe50352987 |
| 23.03 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=474924895dfc48739b01deeef3258aa6 |
|       | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=823b35b563ae4681bc6f979ba248517b |
| 25.03 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=dbd1ebadbc5a4e1bb3eb6d1777c5796c |
| 29.03 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=a975059e55a8471dbe649132e1f9e530 |
|       | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=698ef466e1d644a8949417168f385817 |
| 30.03 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=3eee9159b3de4d9e93cd91c0f688f78b |
|       | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=2bb41602332945ac9cc28b169570c972 |
| 01.04 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=8c1db02455734a1f86be0ca5942e6b0c |


## Examination procedure

Written test and oral exam.
