# Lezione 1 --- Dinamica e sinergie

## Slide 2

## La macchina automotrice

Il corpo umano può essere considerato come una macchina automotrice.
Considerando la complessità delle funzioni dei sistemi si può intravedere la
presenza di una miriade di componenti.

## Slide 3

Possiamo considerare qualcuno di questi componenti:

1. Una struttura di sostegno: lo scheletro. Questo ha funzione di supporto, ma
	 anche di protezione per gli organi interni.
2. Il sistema motore: i muscoli. A differenza delle macchine, che possiedono un
	 motore centrale la cui potenza può essere ripartita, noi possediamo un
	 sistema motore distribuito. I muscoli sono motori distribuiti si trovano
	 nella zona in cui è necessario vi siano delle azioni meccaniche.
3. Sistema di alimentazione: polmoni, sangue e ossigeno. Questo trasporta
	 l'ossigeno, il principale comburente, nei punti in cui serve produrre
	 energia (i.e. muscoli), oltre alle strutture biologiche.
4. Un serbatoio di combustibile (e.g. zuccheri, amidi, grassi). Queste sostanze
	 sono messe nei tessuti adiposi e nelle varie strutture del corpo.
5. Strumento di regolazione automatica: sistema endocrino. Questo sistema è in
	 grado di produrre ormoni che regolano poi le funzioni vegetative (equilibri
	 acido/base)
6. Un sistema di comando: sistema nervoso. Questo è suddiviso in diverse parti:

	-	Encefalo
	- Prolungamenti nel midollo spinale
	- Collegamento con la periferia mediante nervi

7. Trasmissione meccanica: I muscoli agiscono come motori ma devono trasmettere
	 la loro potenza verso l'esterno. Gli elimenti che impongono una resistenza
	 sono i tendini e i sisemi di leve.
8. Sospensioni e propulsori. La cosiddetta "massa trasportata", ossia quella
	 costituita dal busto, la testa, gli arti superiori, è messa in moto dagli
	 arti inferiori.
9. Sistema di controllo dell'assetto. Perché è possibile mantenersi verticali,
	 in equilibrio? per la presenza di un sistema di controllo fatto da sistemi
	 complessi che si riducono al sistema vestibolare (i.e. a livello
	 vestibolare, nell'orecchio), il sistema somatosensoriale che riguarda
	 l'insieme dei recettori che sono distribuiti nel corpo (e.g. sulla pelle, a
	 livello articolare), sistema propriocettivo che riguarda i recettori interni
	 al corpo che hanno a che fare con muscoli e tendini (essi forniscono
	 informazioni riguardanti la forza applicata e le quantità cinematiche), in
	 sistema visivo che fornisce informazioni riguardo la posizione del nostro
	 corpo rispetto all'ambiente esterno.
10. Sistema di retroazioni: tutte le afferenze sensoriali. Sono tutti quei
		segnali che la nostra macchina riesce a percepire dall'esterno del sistema
		nervoso (i.e. stimoli che provengono sia dall'esterno del corpo che
		dall'interno). La macchina riceve quindi le informazioni attraverso una
		serie di sensori che permettono delle retroazioni, azioni in risposta a
		segnali percepiti.
11. Sistema di protezione da eventi lesivi. Importanti per la sopravvivenza del
		soggetto. Un sistema dolorifico è importante per la sopravvivenza.
12. Sistema di autoriparazione: è estremamente importante pensando che i
		tessuti sono ottimizzati per resistere ad alcune sollecitazioni ma
		utilizzati anche in modo tale da non avere sovradimensionamenti. Le lesioni
		vengono automaticamente riparate.
13. Un sistema d'esplorazione dell'ambiente (tattile, visivo e acustico).
14. Sistema di manipolazione (arti superiori).

## Slide 6

Lo studio dell'apparato locomotore è svolto nell'ambito fisiologico ma, essendo noi degli ingegneri, abbiamo bisogno di descrivere tale comportamento anche in termini ingegneristici. Trasferiamo alcuni concetti tipici del ramo della fisiologia all'ambito ingegneristico

## Slide 7

Si descrivono alcuni esempi:

| Termini clinici      | Equivalente ingegneristico                                                                  |
| :---                 | :---                                                                                        |
| Postura              | Posizione                                                                                   |
| Gesto o atto motorio | Cambiamento di posizione in funzione del tempo, traiettoria. Lo si studia con la cinematica |
| Funzione motoria     | Capacità di azionare gli attuatori in modo coordinato                                       |

## Slide 8

Tra i vari atti motori che possiamo andare a studiare esistono due diverse categorie: 

- *Periodici*: comportano una ripetizione ciclica del cambiamento di assetto.
	e.g. corsa, cammino.
- *Non periodici*: atti motori transitori. e.g. alzarsi dalla sedia.
- *Vegetativi*: questo tipo di atto motorio non dò luogo ad un movimento
	esterno del corpo.
- *di carattere protettivo*: riflessi da evitamento. Cercano di proteggere il
	corpo da eventi lesivi.
- *di comunicazione verbale*: fonazione
- *di comunicazione non verbale*: mimica e gesticolazione.

## Slide 9

Le parti del corpo interessate durante gli atti motori sono, principalmente,
gli arti superiori ed inferiori. Questi sono quelli di interesse per il nostro
corso. Non sono gli unici però e qui ne sono riportati altri.

## Slide 10

## Definizione di biomeccanica

- Scienza che studia le strutture e le funzioni dei sistemi biologici
	utilizzando la conoscenza e i metodi della meccanica. Per cui si applica la
	meccanica classica a sistemi biologici. 
- Scienza che esamina le forze agenti sulle strutture biologiche e all'interno
	di esse e analizza gli effetti prodotti da queste forze. Questi effetti
	possono essere di diverso carattere.

## Slide 11

Postura: l'assetto che il corpo assume in una particolare situazione.

## Slide 12

Il movimento umano. Come qualunque sistema di masse distribuite e collegate tra
loro, questo sistema rimane in un equilibrio statico dalla presenza di forze
(in campo gravitazionale al cer). 

La modificazione del sistema di forze agenti sul sistema di masse genera il
movimento. Questo non è un concetto nuovo ma deriva dal primo principio della
dinamica: un corpo mantiene il proprio stato di quiete o di moto rettilineo
uniforme finché una forza non agisce su di esso. Questa forza quindi altererà
lo stato di quiete/MRU.

Le forze che possono modificare lo stato di equilibrio del corpo sono:

- Forze esterne: non solo forze nuove applicate all'esterno del corpo ma anche
	una riduzione di quelle già esistenti.
- Forze interne: forze muscolari. Queste non sono in grado di spostare il
	centro di massa dell'intero sistema. Questo è molto importante perché avrà
	delle implicazioni. Il cammino è possibile solo perché esistono delle forze
	esterne applicate al nostro corpo: attrito col terreno e la forza di gravità.
	Le sole forze interne non sarebbero sufficienti a spostare il centro di massa
	e quindi a far muovere il sistema (e.g. un uomo nello spazio). 

## Slide 13

Le forze esterne possono dipendere da varie condizioni: possono provenire da
un'altra persona, dall'interazione con oggetti e ciò andrebbe a modificare lo
stato di equilibrio. Se vogliamo modificare lo stato di equilibrio con forze
interne è necessario lavorare sui muscoli. Essi sono in grado di modificare in
tempi brevi le forze interne al nostro sistema. Questi sono controllati dal
SNC. Il SNP porta poi i vari segnali verso i muscoli. Questi muscoli agiscono
sotto dei comandi motori del SNC e con il contorllo delle afferenze sensoriali
(provenienti dalla periferia e convogliate al SNC). Questi comandi quindi non
sono lanciati in anello aperto ma continuamente adattati e modificati a seconda
dell'informazione che viene ricevuta come risultato della contrazione
muscolare. Le afferenze informano il SNC di quello che è avvenuto in periferia
(se la forza è diminuita o aumentata, se è avvenuto il contatto con altre
strutture, informazioni sulla cinematica). In base a questo il SNC modifica i
comandi motori. Questo è un sofisticato sistema di retroazione.

## Slide 14

Riprendiamo alcuni principi fondamentali della dinamica. In questo video viene
mostrata l'evoluzione temporale di quattro curve: la forza lungo y,
l'accelerazione, la velocità e lo spostamento del cubetto rappresentato (tutto
in relazione a y). La forza produce un'accelerazione: una massa sottoposta ad
una forza avrà un'accelerazione inversamente proporzinale alla massa e
direttamente proporzionale alla forza applicata. A forza costante corrisponde
un'accelerazione costante. Un'accelerazione costante significa una velocità che
cresce proporzionalmente (i.e. linearmente) nel tempo. Lo spostamento, essendo
l'integrale della velocità, è caratterizzato da un'andamento quadratico: una
parabola che, in questo caso, ha concavità verso l'alto.

## Slide 15

Si studia in questa slide l'esempio per cui la forza viene applicata per un
brevissimo istante temporale (quel minuscolo delta iniziale). Durante questo
breve intervallo avremo un'accelerazione, una velocità che cresce linearmente,
Lo spostamento parabolicamente. Quando viene tolta la forza abbiamo il
mantenimento della velocità finale (si mantiene il MRU). Lo spostamento, in
quanto integrale della velocità, avrà un andamento lineare. Nell'esempio
notiamo che il blocchetto non solo ha una velocità costante ma che ruota pure
su se stesso. Questo perchè la forza non è applicata esattamente in
corrispondenza al baricentro del sistema ma un po' disassata: la forza ha un
braccio rispetto al baricentro e questo produce un momento di rotazione attorno
ad esso. Quando termina la forza è curioso che si mantenga costante sia la
velocità di traslazione che quella di rotazione del blocchetto.

Questo ci porta a riflettere su ciò che accade nel movimento umano che porta un
arto ad assumere una diversa posizione da quella iniziale. sarà necessario
accelerare l'arto verso l'oggetto d'interesse e poi decelerare per fermare la
mano in corrispondenza dell'oggetto che desideriamo raggiungere. Questo perché
se applicassimo un'accelerazione e poi riportassimo a zero il corpo
continuerebbe a muoversi.

## Slide 16

Diamo uno sguardo alla seguente simulazione che serve a dare un'ulteriore
riprova di ciò che è appena stato detto.

*Grafici sulla prima colonna*: La forza viene applicata in modo costante
produce una velocità lineare e uno spostamento parabolico.

*Grafici al centro*: La forza è applicata per un certo periodo di tempo e poi
torna ad essere zero. La velocità durante l'applicazione della forza cresce
linearmente, diventando costante nel momento in cui la forza (quindi anche
l'accelerazione) diventa nulla. Lo spostamento, durante il tratto di velocità
lineare, cresce linearmente mentre nel tratto costante della velocità questo
cresce linearmente.

*Grafici a sinistra*: Se volessimo che l'oggetto si fermi dobbiamo generare una
forza contraria a quella applicata precedentemente. Soltanto se noi applichiamo
una certa forza in una certa direzione (a favore) otteniamo una velocità che
cresce linearmente, se applichiamo una forza in senso contrario (i.e.
accelerazione con verso negativo) avremo una velocità che decresce. In questo
modo avremo che nella fase con velocità positiva lo spostamento cresce in modo
quadratico (curvatura positiva), nella fase di velocità negativa questo avrà
curvatura verso il basso e si assetta ad un valore finale di spostamento. La
velocità finale sarà zero. Quando la velocità finale è realmente nulla, il
sistema dev'essere in grado di cancellare la forza negativa applicata con un
preciso tempismo. L'importante è che l'integrale dell'accelerazione nella fase
positiva sia uguale a quello della fase negativa. Non è necessario che le
accelerazioni abbiano stesso modulo ma verso opposto.

## Slide 17

È possibile applicare questo ragionamenti per sistemi anche molto complessi. Si
può capire che per spostare l'arto verso un oggetto dovremo prima applicare una
forza nel verso dell'accelerazione (per aumentare la velocità) e poi applicarne
una negativa finché la velocità è nulla e torna a zero la forza, di modo che la
condizione finale raggiunta e mantenuta. Siccome le forze sono prodotte dai
muscoli ciò ci fa capire che sono necessari due tipi di muscoli per produrre
un'azione semplice come questa. Questo accade perché per natura i muscoli
possono applicare una forza solamente in un determinato verso (i.e. verso
concentrico, ossia tirare verso il centro) e non nell'altro (i.e. eccentrico,
spinta). Servono due muscoli che tirano da due parti opposte. Allora serve una
coppia di muscoli agonisti e antagonisti. Questi termini sono stati scelti in
relazione al movimento che si vuole compiere. L'agonista produce il movimento
desiderato mentre l'antagonista si oppone a questo. Agonisti ed antagonisti
devono essere attivati in modo coordinato tra di loro.

Facciamo un esempio: l'estensione del gomito. L'agonista è l'estensore,
l'antagonista il flessore. Nel momento iniziale l'agonista viene attivato,
producendo una forza che fornisce velocità positiva. In un secondo momento
l'estensore si rilassa e il flessore attivato, di modo che il movimento venga
frenato.

Questo sistema è molto sofisticato in termini di controllo della forza. infatti
è necessario produrre precisi valori di forza con un determinato tempismo. Nel
nostro corpo, tutto ciò che appare statico in realtà è dinamico.

## Slide 18

## Principi dell'equilibrio applicati al corpo umano

1. Il corpo umano è costituito da segmenti anatomici dotati di massa e connessi
	 da articolazioni.
2. Se il corpo umano è immerso nel campo gravitazionale terrestre, ciascuna sua
	 molecola è sottoposta ad una forza diretta verso il centro della Terra.
3. Una forza applicata ad un corpo, in assenza di una seconda che la contrasti,
	 produce un'accelerazione del baricentro del corpo stesso di valore
	 inversamente proporzionale alla sua massa (Secondo Principio della
	 Dinamica).  
4. Quando la forza applicata si annulla la massa conserva la velocità
	 raggiunta. per annullare tale velocità occorre applicare una forza
	 deceleratrce per un tempo sufficiente.
5. Forze interne non sono in grado di spostare il baricentro del sistema.
6. Nel sistema articolato che rappresenta il corpo umano le articolazioni
	 possono essere considerate come cerniere ideali (senza attrito) che
	 permettono la rotazione relativa tra segmenti adiacenti.

## Slide 19

Cosa succede se consideriamo due segmenti adiacenti?

Trascurando la forza di gravità. Muscolo è situato tra A e B. La contrazione
avvicina A a B.

## Slide 20

Spaccando il sistema otterremmo questo.

Esiste una massa distribuita in tutto il volume e una forza Fm applicata in A.
Questa forza produrrà un'accelerazione del baricentro del segmento. Tale
accelerazione è facilmente calcolabile facendo il rapporto tra Fm e la massa m1
del braccio. Quest'accelerazione la possiamo immaginare applicata nel
baricentro. Fm non è applicata nel baricentro però. 

Se avessimo una sola forza applicata a questo corpo non avremmo soddisfatto il
principio di azione e reazione. Affinchè sia così bisogna ipotizzare che vi sia
una forza fittizia che si oppone alla forza applicata al corpo (i.e. forza
d'inerzia). Tale forza non è altro se non il prodotto tra l'accelerazione del
baricentro per la massa di quel segmento. Il segno, ovviamente, sarà opposto a
quella della forza applicata. 

La forza applicata in A possiede un braccio rispetto a baricentro (b1). Si
genera un momento di forza. Siccome il corpo ha una massa distribuita, questo
momento produce un'accelerazione rotazionale (alpha1). è possibile calcolare il
valore di alpha1 dividendo il momento che la forza Fm genera per il momento
d'inerzia di massa del braccio (J1). Deve esistere, per il principio di azione
e reazione, un momento di reazione, il momento d'inerzia alla rotazione (Mi1).
Mi1 è il prodotto tra accelerazione angolare e il momento d'inerzia di massa
cambiato di segno.

La stessa cosa che è stata fatta per il primo segmento è stata fatta per il
secondo. La distanza tra il punto d'applicazione della forza e il baricentro G2
è maggiore. L'accelerazione linerare è inversamente proporzionale alla massa,
che è inferiore rispetto a m1. L'accelerazione angolare (alpha2) probabilmente
sarà maggiore, ipotizzando che il momento d'inerzia di massa sia inferiore a
quello del primo segmento, dato che il secondo segmento ha una massa minore. Il
momento d'inerzia alla rotazione sarà minore perchè la massa e il momento
d'inerzia di questo segmento sono inferiori rispetto al primo. 

Nonostante la forza applicata ai due segmenti anatomici sia la stessa, quello
avente massa e momento d'inerzia di massa maggiore subirà, a parità di forza
applicata delle accelerazioni sia lineari che di rotazione minori rispetto al
corpo avente massa e momento d'inerzia di massa minore.

## Slide 21

Una simulazione molto semplice che rappresenta braccio, avambraccio. Una molla
rappresenta il muscolo flessore del gomito. si ipotizza che la molla si accorci
bruscamente (contrazione muscolare). 

Cosa succederebbe se non ci fosse forza di gravità e non ci fosse il vincolo
articolare del gomito? Il modello mostra che i segmenti compenetrerebbero
(errato se considerassimo comunque la fisica dei corpi rigidi). I segmenti si
muoverebbero caoticamente.

## Slide 22

Una seconda simulazione, sempre in assenza di gravità ma con vincolo cinematico
a livello del gomito. Possiamo notare che il movimento realizzato è più simile
a quello riscontrato fisiologicamente negli arti. Notiamo che il segmento
grosso si muove meno mentre quello piccolo si muove di più. Inoltre si può
notare che il baricentro complessivo di questo sistema rimane fisso, esistono
solamente i movimenti relativi di un corpo rispetto all'altro. 

Come si fa a calcolare la cinematica di questo sistema? Diventa complesso
perchè non abbiamo solo una forza applicata ad un segmento che produce
un'accelerazione lineare ed un momento di rotazione ma anche altre forze (ossia
le forze di reazione interna alle articolazioni). In questo caso, tra i due
segmenti che abbiamo separato, dobbiamo mettere in evidenza che ci sarà una
forza uguale ed opposta dovuta alla presenza del vincolo cinematico della
cerniera del gomito. Inoltre dobbiamo ipotizzare che il sistema non sia
completamente "galleggiante" nel vuoto ma che sia collegato ad altri segmenti
anatomici.Dovremo quindi andare a considerare anche le reazioni interne.
Dovremo risolvere un sistema di equazioni differenziali in cui dovremo imporre
dei vincoli (e.g. vincoli di movimento: congruenza geometrica tra punti
appartenenti ad una determinata articolazione).

## Slide 23

La forza d'interazione (i.e. forza interna) tra segmenti anatomici varia in
accordo con il principio d'azione e reazione. Risulta quindi che la variazione
di forza muscolare di un solo muscolo, rispetto a quella necessaria per
mantenere l'equilibrio statico, deve comportare necessariamente, in assenza di
interventi di compensazione:

- l'accelerazione (lineare e angolare) di tutti i segmenti corporei. Questo
	perchè si trasferiscono l'uno con l'altro le reazioni vincolari. Se cambia
	una forza all'estremità di un segmento, questa modifica la forza di reazione
	vincolare di un'articolazione possimale, che modifica l'equilibrio del
	segmento prossimale, il quale trasmette le forze di reazione a segmenti più a
	monte.
- la conseguente variazione di configurazione geometrica del corpo.

## Slide 24

Anche le forze di equilibrio alla forza di gravità cambiano e non sono più in
equilibrio.

Nel soggetto che sta in piedi sottoposto a forza di gravità:

- La conseguente variazione dei momenti dovuti alle forze gravitazionali
	rispetto alle articolazioni.
- La conseguente variazione delle forze di reazione al terreno.
- Il collasso del corpo a terra.

## Slide 26

L'azione di un musolo che produce movimento, nel nostro sistema fisiologico, è
accompagnata da variazioni di forze muscolari nei muscoli che agiscono su
articolazioni prossimali rispetto al segmento in movimento (muscoli
*fissatori*). I muscoli fissatori sono quindi quelli più prossimali. Distale
(lontano dal centro del corpo), prossimale (vicino al centro del corpo).
Muovende un braccio, i muscoli prossimali si contraggono per mantenere una
postura e adattarsi alla diversa configurazione. In assenza di questo ci
sarebbe la perdita d'equilibrio. La compensazione da parte dei muscoli viene
mediata differentemente a seconda che le masse coinvolte nel movimento siano
piccole (e.g. movimento delle dita) oppure grandi (e.g. durante un inchino).

I cambiamenti posturali di assetto posturale conseguenti al movimento e
comportanti rischio di collasso al terreno vengono compensati da variazioni di
forze nei muscoli che impodiscono tale cedimento (muscoli *antigravitari*).
Questi muscoli antigravitari sono, per gli arti inferiori, i muscoli estensori
(quelli che spingono verso il basso e tendono ad allungare l'arto). Negli arti
superiori gli antigravitari sono i flessori, quelli che tengono a ridurre la
lunghezza dell'arto (e.g. sollevamento di una valigia).

## Slide 27

Cosa succede se considerassimo in questo modello semplicissimo anche
l'intervento della gravità e delle interazioni con le articolazioni prossimali?

## Slide 28

Bisogna tener presente che l'interazione tra due segmenti è legata, fra le
varie equazioni, anche dalla legge della conservazione della quantità di moto.
In assenza di forze esterne al sistema (in questa slide quello costituito da
due palline collegate da una molla). L'unica forza agente tra queste palline
(in assenza di gravità) è quella mediata dalla molla che attrae una pallina
verso l'altra.

$Q=m_1 v_1 = m_2 v_2$ in assenza di forz esterne.

Guardando i grafici della posizione delle due palline possiamo notare che
queste si avvicinano ed allontanano. Lo spostamento con velocità maggiore è
quello della pallina rossa perchè ha massa minore. La pallina più pesante avrà
una velocità minore. Terminato il ciclo di accorciamento/allungamento,
considerando la molla come un'insieme di molla ideale e smorzatore, raggiungerà
una condizione di equilibrio. La pallina rossa ha subito, rispetto alla
posizione iniziale, uno spostamento maggiore rispetto a quello esistente tra la
posizione iniziale e finale di quella blu. Ciò dimostra come, per effetto della
conservazione della quantità di moto, le masse maggiori si spostano meno
rispetto alle masse piccole.

Il movimento finale dipende da come sono distribuite le masse del nostro
sistema.

## Slide 30

Altra condizione che determina il movimento risultante è la caratteristica dei
vincoli. Un sistema vincolato (cingolo scapolare fissato ad una barra cui si
impedisce la rotazione rispetto a z) avrà un movimento dell'arto ma non del
busto.

Diversa è la condizione per cui si ha un cingolo scapolare libero di ruotare
attorno all'asse verticale del corpo: la contrazione muscolare determina anche
la rotazione della barra su cui giace tale vincolo.

## Slide 32

Possiamo considerare il corpo umano come un insieme di molti segmenti anatomici
sottoposti alla forza di gravità. Ciascun segmento avrà una forza peso
proporzionale alla propria massa. Essendo l'accelerazione che subisce
inversamente proporionale alla massa, tutti subiscono la stessa accelerazione
verso il basso. 

- In assenza di forze muscolari e contatti con il terreno tutti i segmenti
	corporei accelererebbero verso il centro della terra mantenendo la
	configurazione geometrica iniziale. La loro posizione relativa rimane la
	stessa. 
- In presenza del terreno si svilupperebbero delle forze di contatto in grado
	di impedire il movimento verso il basso dei segmenti anatomici a contatto con
	esso (i.e. i piedi). I segmenti più in alto ruoterebbero attorno alle
	articolazioni dei segmenti inferiori ed il corpo collasserebbe a terra.
- In presenza, oltre che del terreno, anche di forze muscolari in grado di
	produrre opportuni momenti alle articolazioni che equilibrino i momenti
	generati dalle forze gravitzionali e dalle forze di contatto con il terreno
	il corpo rimarrebbe nella configurazione iniziale. Se ci sono forze in grado
	di sostenere i momenti dovuti al contatto col terreno il corpo può
	irrigidirsi in quella posizione e mantenere in una postura statica.

## Slide 33

Il monopode, un modello molto semplice che contiene elementi importanti: massa
trasportata, coscia, gamba e piede. 

Quali sono le forze che i muscoli devono applicare per evitare che avvenga il
cedimento. Si consideri un corpo rigido dotato di massa incernierato in un
certo punto, sottoposto alla forza di gravità applicata nel baricentro e con
una forza muscolare (che tende a bilanciare la forza di gravità) rappresentata
dal vettore rosso. Essendo un sistema in rotazione intorno ad una cerniera noi
riduciamo l'azione della forza all'azione di momenti. Dobbiamo equilibrare i
momenti quindi.  Avremo un momento dovuto alla forza di gravità (mgd) e una
forza d'equilibrio (Fb). Potrebbe sembrare intuitivo che in ogni istante noi
conosciamo la posizione del segmento anatomico e noi facciamo il calcolo della
forza e il segmento dovrebbe stare in equilibrio. Questo solo in teoria però.

## Slide 34

Nel mondo reale accade ciò: noi possiamo calcolare la forza d'equilibrio come
fatto precedentemente. Essendo il valore quasi giusto (inferiore) il sistema
esce dal suo stato di equilibrio e l'arto cade.

## Slide 35

Proviamo quindi ad aumentare il valore di forza per capire cosa accadrebbe. Il
valore sarebbe maggiore del necessario e quindi sarebbe ancora instabile.

## Slide 36

Il sistema di controllo quindi non decide di inviare un comando motorio per
produrre una forza predefinita e mantenerla costante ma utilizza un sistema che
si adatta facilmente alle variazioni del carico esterno. Il motivo per cui il
corpo accelera verso l'alto o il basso è che se ho un piccolo spostamento verso
l'alto (o verso il basso ma consideriamo il caso verso l'alto) avremo che il
braccio b tende a diminuire, così come quello di d. Se la diminuzione di d è
più rapida di b il momento associato a F diminuisce più rapidamente di quello
associato alla forza peso. Man mano che ci si sposta dalla situazione di
equilibrio aumenta la differenza dei due momenti e per cui l'accelerazione
tende ad aumentare. serve qualcosa che bilanci queste piccole variazioni
d'azione. Bisogna usare il muscolo *come se fosse una molla*. Anzichè dare alla
forza muscolare un valore predefinito ma una funzione definita con il modello
della molla (costante elastica per variazione di lunghezza) noteremmo che man
mano che si abbassa il braccio diminuendo b e d ma, visto che aumenta la
lunghezza, aumenterebbe la forza muscolare. L'aumento di forza dovuto
all'aumento di lunghezza può compensare la diminuzione del braccio e, a questo
punto, si può trovare una situazione d'equilibrio in cui è raggiunta una certa
lunghezza maggiore di prima ma è in grado di produrre comunque un momento
sufficiente a sostenere il peso del corpo. Si mette quindi in relazione la
forza interna con la forza esterna.

## Slide 37

La forza esterna è quella della forza peso che dev'essere tradotta nello stesso
sistema di riferimento della forza interna. La traduciamo quindi in forza in
direzione del muscolo: $F_{est} mg\dfrac{b}{d}$ (effetto del momento della
forza gravitazionale nella direzione della forza muscolare). La forza interna
invece è $F_{int}=k\Delta l$. Ora siamo nello stesso sdr. La forza interna ha
un andamento lineare proporzionale alla lunghezza (all'aumento della lunghezza
la forza cresce linearmente). Gli effetti della forza esterna è di questo tipo:
quando la lunghezza della molla diminuisce (sollevamento del carico) l'effetto
della forza gravitazionale aumenta. Il punto d'equilibrio è l'intersezione tra
la retta, che corrisponde alla forza interna, e la curva rossa, corrispondente
alla forza esterna letta rispetto all'asse x. Si volesse variare il carico
applicato esternamente, la curva rossa shifterebbe verso l'alto diventando
quella gialla (come riferimento il grafico di destra). Il punto d'equilibrio si
sposterebbe più in basso (la molla sarebbe più allungata) ma la forza
sviluppata sarebbe maggiore.

Se volessimo controllare la posizione del corpo si può cambiare alcuni
parametri, tra cui la lunghezza di riposo della molla e la rigidezza.
Quest'ultimo è il parametro più importante, la pendenza della curva. Aumentando
la rigidezza della molla spostiamo il punto di equilibrio verso sinistra.
Questo concetto qui è solo accennato ma viene ripreso nel controllo motorio, in
particolare quando si parla della teoria dell'equilibrio lambda. Il sistema di
controllo sfrutta molto bene questa proprietà dei muscoli di comportarsi in
modo simile alle molle in modo da adattarsi e compensare piccole perturbazioni.

## Slide 38

Si studi lo stesso caso di prima ma ora modellizzato in modo differente. In
questo caso la forza non rimane costante ma aumenta con la lunghezza della
molla. La forza peso tende a spostare il corpo verso il basso ma, allungandosi
la molla, è aumentata la forza muscolare che trascina il corpo verso l'alto,
facendolo oscillare attorno alla posizione d'equilibrio a cui poi tende se
esiste uno smorzamento.

## Slide 39

Questa situazione ci dimostra che possiamo variare anche notevolmente il valore
della forza iniziale e che ci saranno delle variazioni di posizione ma il
sistema troverà un equilibrio in una posizione che corrisponderà
all'uguaglianza all'equilibrio del momento della forza interna e quello della
forza esterna. Questo è valido anche per variazioni di forza notevole (5-10N).

## Slide 40

Quindi questo è il meccanismo con cui lavorano i nostri muscoli. Immaginiamo di
avere quindi un sistema caratterizzato da diverse molle opportunamente tarate
(lunghezza di riposo e rigidezza adeguate) possiamo trovare l'equilibrio.

## Slide 41

## Slide 42

Modificando la configurazione geometrica del corpo o cambiando il valore di
rigidezza di alcune molle non si garantisce più l'equilibrio e il soggetto
potrebbe cadere. Questo perchè se il baricentro uscisse dalla base d'appoggio
del piede questo sistema cade mantenendo una configurazione più o meno simile a
quella iniziale negli altri segmenti. Per la stabilità del corpo in postura
eretta servono delle azioni interne tali da mantenere la configurazione del
corpo e compensare piccole perturbazioni interne ma serve anche un controllo
del baricentro che deve stare al di sopra della base d'appoggio. 

## Slide 43

Le perturbazioni possono essere sia interne che esterne. Un esempio di
perturbazione interna la si genera sollevando bruscamente gli arti superiori.
Questo sollevamento comporta una forza d'inerzia che spinge il corpo
all'indietro, facendo oscillare il corpo. A livello di anca e ginocchio vengono
compensate bene. A livello della caviglia i flessori dorsali (tibiale
anteriore) e flessori plantari (muscoli del polpaccio, antagonisti dei flessori
dorsali. e.g. soleo), queste molle subiscono delle variazioni di lunghezza, in
corrispondenza delle quali accade ciò che è riportato alla prossima slide.


## Slide 44

In corrispondenza del momento in cui gli arti vengono spostati verso l'alto,
abbiamo un'allungamento del muscolo tibiale anteriore (flessore dorsale) che
quindi svilupperà una forza maggiore. La molla che si trova posteriormente al
piede (flessore plantare, soleo) si accorcia e quindi la forza applicata da
questo muscolo diminuisce per poi ritornare su ancora. 

Esistono quindi due azioni di queste molle:

1. aumento forza flessori dorsali
2. diminuzione forza flessori plantari

Queste azioni prodotte dalle molle in modo passivo sono necessarie anche negli
elementi attivi (muscoli veri). Nel sistema di controllo però si utilizza anche
artifici migliori che rende più efficace la compensazione della perturbazione.

## Note

- Perturbazione: movimento delle braccia.
- Compensazione: agire con i muscoli per ridurre il più possibile lo
	spostamento del corpo ed evitare cadute rovinose.


## Slide 45

La condizione sperimentale che è stata utilizzata per effettuare questi studi è
la seguente.

Il soggetto è in piedi su una piattaforma dinamometrica (piastra nel pavimento
con sensori in grado di misurare le varie componenti della forza applicata
sulla superficie. Una specie di bilancia che non misura solo la componente
verticale ma anche quelle orizzontali antero-posteriori e medio laterali).
Queste componenti derivano dal fatto che le masse che costituiscono il nostro
corpo possono accelerare in varie direzioni, generando forze d'inerzia che
vengono trasmesse al terreno e che vengono misurate dalla piattaforma
dinamometrica.

Oltre a questo sistema di misura è stato usato un sistema di cattura del
movimento (una telecamera che inquadra il soggetto di lato che inquadra dei
marker sul soggetto). Questi marker riflettono la luce nella direzione in cui
l'hanno ricevutae che vanno ad individuare dei punti del corpo d'interesse per
la nostra analisi. Questi marker vengono visti dalla telecamera e tracciati nel
tempo. Tracciando i marker nel tempo si può tracciare effettivamente anche il
movimento del soggetto.

A questi due sistemi è aggiunto anche un sistema elettromiografico. Un
apparecchio che registra l'attività elettrica dei muscoli. Ci sono elettrodi
posti superficialmente, in corrispondenza dei vari muscoli d'interesse. Nel
nostro caso quelli d'interesse sono i flessori dorsali e i flessori plantari.
Così si analizza l'attività dei muscoli in conseguenza del movimento che il
soggetto esegue.

## Slide 46

Il movimento che si chiedeva di compiere al paziente consisteva nell'inizio di
un passo. Il soggetto faceva un passo. I vari tracciati riportati hanno diverse
informazioni.

Guardando lo spostamento dei marcatori (terzo grafico, quello coi numeri)
questo è identificato dalle curve numerate:

- 1, 2, 3 sulla testa e spalla
- 4, 5, 6 bacino e ginocchio
- 7 sulla caviglia

Possiamo notare che dall'istante in cui viene dato il comando di movimento
trascorre un po' di tempo prima di vedere il primo movimento del marcatore che
sta sull testa. Progressivamente si aggiungono anche gli altri e, per ultimo
quello alla caviglia. Questo mostra una sequenza di movimenti in avanti che
parte dalla testa fino alla caviglia. Questo ci fa pensare in un certo senso ad
un pendolo inverso. La cosa interessante è che trascorrono circa 300 ms prima
di osservare un primo movimento nella parte alta del corpo.

Osserviamo cosa accade nel frattempo ai tracciati elettromiografici del tibiale
anteriore (i.e. TA) e del soleo (Soleus), primi grafici. Nonostante il
movimento avvenga 300 ms dopo il comando, i muscoli si attivano molto prima. In
postura eretta esiste una piccola attivazione del soleo. La pedana
dinamometrica rileva la presenza di una forza di reazione diretta verticalmente
più o meno nel baricentro di quest'area e diretta nel baricentro del corpo.
Essendo un'azione che sta producendo una reazione esterna applicata a metà
della pianta del piede questa produce un momento flessorio dorsale rispetto al
centro della caviglia, per cui è necessaria l'azione dei muscoli antigravitari
(flessori plantari) per mantenere l'equilibrio con la forza esterna ossia la
reazione d'appoggio. Qui vediamo quindi l'azione del soleo con una contrazione
muscolare più o meno costante nella postura eretta. Il tibiale anteriore non ha
necessità di essere attivato. Al momento del comando motorio, l'azione del
soleo viene ridotta bruscamente (inibizione) e poco dopo avviene un'attività
del TA che si prolunga nel tempo. Prendendo questi segnali e calcoliamo
l'inviluppo (un'integrazione rispetto al tempo) otteniamo le curve riportate
sotto (quelle con scritto TA e Soleus, quarto e quinto grafico). Il tibiale
anteriore dopo un po' di tempo quindi si attiva e dopo ulteriori 150 ms si ha
il movimento.

Le curve riportate poi sotto sono quelle del contatto del piede con terreno. HO
(Heel Off) TO (Toe Off). Quindi possiamo proprio identificare le varie fasi del
cammino, da quando il tacco lascia il suolo alla fase di volo quando anche la
punta si stacca. 

Questa sequenza d'inibizione dell'attività del soleo e d'attivazione del TA
prende il nome di sinergia posturale. Si dice sinergia l'azione coordinata di
vari muscoli che possono essere dello stesso tipo (i.e. due agonisti) o di tipo
diverso (agonista e antagonista). In questo caso abbiamo due muscoli
antagonisti che lavorano in modo sinergico: uno diminuisce la sua attività e
l'altro, trascorso del tempo, si attiva.

## Slide 47

Paragonando i risultati ottenuti tramite il modello nel caso di perturbazione
interna (e.g. sollevamento arti superiori) e quelli dell'esperimento appena
discusso, se si considerano le forze stimate possiamo ritrovare alcune analogie
nella loro rispettiva morfologia. È curioso il fatto che nell'esperimento umano
le forze in gioco vengono sviluppate in fase anticipatoria mentre nel modello
queste forze, essendo passive, vengono sviluppate in modo sincrono tra loro e
sincrono anche alla perturbazione. Si parla quindi, nel caso dell'esperimento
umano di *aggiustamento posturale anticipatorio (aka APA)*.

Nel sistema fisiologico analizzato l'inibizione e la contrazione avvengono in
modo dilazionato nel tempo e sono in anticipo al movimento che conduce alla
perturbazione. Questo anticipo da la denominazione al fenomeno di APA. Questa è
un'azione muscolare sinergica che anticipa il movimento. Lo scopo di APA è
quella di minimizzare la perturbazione. Quindi, nel caso del modello passivo,
l'azione dei muscoli è una conseguenza della perturbazione e quindi avviene con
un certo ritardo: il sistema riesce a spostarsi prima di riottenere
l'equilibrio. Nel sistema fisiologico invece vengono anticipate queste attività
di modo che il corpo è in una posizione tale per cui nel momento in cui la
perturbazione avviene il corpo è già pronto a sostenerla e riduce l'effetto
della perturbazione.

## Slide 48

Le sinergie di questo tipo avvengono in tutta una serie di movimenti. Questo
genere di sinergie vengono dette, generalmente, *forward oriented*, orientate
al movimento in avanti. Queste sinergie presentan lo stesso schema di
coordinamento motorio riprodotto in vari gesti motori con varianti che
riguardano i tempi di latenza (i.e. intervallo di tempo tra il fenomeno
d'inibizione del soleo e d'attivazione del TA), e di ampiezza. Lo scopo è
sempre quello di minimizzare gli effetti della perturbazione del movimento in
avanti.

## Slide 49

Come si spiega questa inibizione del soleo e attivazione del TA?

La forza di reazione del terreno passa più o meno a metà del piede, in avanti
rispetto al centro della caviglia. Questa situazione è mantenuta se esiste un
equilibrio tra i momenti prodotti dalla forza di reazione (dorsiflessori) e
momenti muscolari del Soleo.

Per effettuare uno spostamento in avanti bisogna produrre l'accelerazione del
baricentro. Per far ciò bisogna diminuire il momento generato dalla forza di
reazione al terreno, spostando la forza più indietro, verso il centro della
caviglia. Così facendo creiamo uno sbilanciamento tra la forza peso e la forza
di reazione, creando una coppia di forze che produce un momento in senso
orario, che produrrà una rotazione intorno al suo baricentro. Per poter
avvicinare al centro della caviglia il punto d'applicazione della forza di
reazione al terreno è necessario ridurre il momento angolare dovuto alla forza
muscolare esercitata dal soleo. Il disassamento delle forze elimina la
condizione di equilibrio statico, che dovrà essere ristabilito introducendo un
momento d'inerzia pari a P-R opposto alla coppia. Questo si realizza applicando
una forza d'inerzia nel baricentro del corpo (-ma) e una seconda forza a
livello del terreno (ma).

Un effetto di maggior entità lo si può raggiungere attivando il tibiale
anterioreche produce un movimento dorsiflessorio. La retta d'azione della forza
R è arretrata e l'accelerazione del baricentro aumenta.

Il movimento non sarebbe possibile se non esistessero le forze d'attrito.

## Slide 50

Sinergia: azione coordinata di muscoli che concorrono all'esecuzione corretta
dell'atto motorio. Sono come dei sistemi di pre-ordinati di coordinamento che
si adattano alle condizioni in cui ci si trova. Quindi queste sinergie possono
ritrovarsi con lo stesso schema ma a seconda del gesto proteso in avanti questi
muscoli antagonisti possono modificarsi in latenza (delay fra fenomeni) e
ampiezza di attivazione.

Quindi l'azione sinergica ha uno sviluppo temporale, che riguarda la sequenza
d'azioni muscolari e uno sviluppo parziale, che riguarda i muscoli che sono
coinvolti.

## Slide 51

Le sinergie possono avere vari scopi:

- Posturali: mantenimento della postura anche a seguito a perturbazioni interne
	(contrazioni muscolari involontarie o volontarie) o esterne (applicazioni di
	forze esterne al corpo).
- Motorie: hanno lo scopo d permettere l'esecuzione corretta di atti motori.
	L'inizio del cammino richiede un'attivazione di sinergia motoria. Nel cammino
	il coordinamento degli arti viene fatto grazie a queste sinergie motorie.
	
Queste si adattano alle diverse condizioni di esecuzione dell'atto motorio
modulando i propri parametri d'intensità e latenza.

Il riflesso: risposta involontaria ad uno stimolo (interno e esterno).
Generalmente si pensa sia una risposta stereotipata. In fisiologia di solito si
distingue la risposta dal riflesso in quanto la prima può essere anche a
latenza lunga. Il riflesso è una risposta a bassa latenza (nell'ordine di
decine o centinaia di ms). Questo è dovuto al fatto che il circuito nervoso che
li controlla è costituito da pochi neuroni, poche stazioni di elaborazione. Il
riflesso è la tipica risposta ad uno stimolo, che corrisponde allo schema del
feedback che gli ingegneri hanno bene in mente: un segnale arriva, viene
elaborato da una serie di neurone e produce una risposta.

## Slide 52

Programma motorio: la realizzazione di un fenomeno (i.e. il movimento) che
utilizza tutte queste risorse (i.e. sinergie, risposte a lunga latenza,
riflessi) modulandole opportunamente attraverso un programma. Per l'esecuzione
del cammino esiste un programma locomotorio che si sviluppa nella fase di
ontogenesi. Questi programmi quindi in parte si imparano (i.e. "modellati"
durante lo sviluppo) e in parte sono predefiniti. In tutto questo sono
coinvolte molte strutture tra cui i muscoli, circuiti nervosi (che presentano
molte connessioni ridondanti. queste sono utili nel caso di inabilità nel
compiere una determinata funzione), sistemi sensoriali (ogni azione che
facciamo non è solo attivata in anello aperto ma strettamente comandato dal
feedback sensoriale, addirittura a livello intrinseco del muscolo con recettori
specifici della variazione di lunghezza, a livello articolare che informano
sullo stato cinematico dell'articolazione, sensori periferici tra cui quelli
cutanei, dolorifici etc.), sistemi di controllo.

All'esecuzion del programma motorio concorrono in generale sistemi di
retroazione e sinergie. Esistono movimenti particolari che vengono eseguiti
anche senza l'uso di retroazioni dirette. Sono chiamati gesti pre-programmati
di tipo balistico (e.g. respingere una pallina da tennis). Questi gesti
estremamante rapidi non sono controllati dal sistema di retroazione diretto e
quindi sono in anello aperto. Questo è vero fino ad un certo punto perchè le
informazioni vengono comunque raccolte dal sistema visivo in anticipazione,
quindi il sistema di controllo agisce in modo anticipatorio e si programma il
movimento della racchetta sulla base della direzione e della velocità della
pallina (feedforward, una retroazione anticipata).

## Slide 53

Per far contrarre o rilasciare i muscoli per  ottenere l'atto motorio considerato:

1. Bisogna inviare comandi accuratamente temporizzati (distribuzione temporale)
	 a diversi muscoli (distribuzione spaziale).
2. Il Sistema di controllo deve creare le condizioni posturali per cui il
	 movimento si possa svolgere correttamente. Queste hanno a che fare con il
	 sistema di feedforward però si svolgono anche per gesti non balistici.
	 Nell'esempio visto prima dell'inizio del cammino, prima di attivare i
	 muscoli che servono per sollevare il piede e fare il passo deve mettere il
	 piede in condizione di produrre questo movimento verso l'avanti.
3. Il sistema di controllo deve tenere in considerazione le caratteristiche
	 funzionali dei vari componenti della struttura. Noi dobbiamo avere, in ogni
	 istante temporale durante il gesto, avere la giusta forza muscolare prodotta
	 da un certo numero di muscoli e temporizzata etc. La forza generata dal
	 muscolo però non possiede una forza "predefinita" ma questa dipende, a
	 parità di comando motorio, da quanto il muscolo è allungato, da quanto il
	 muscolo si sta allungando/accorciando, dalle caratteristiche strutturali
	 delle dimensioni (numero di fibre che contiene, tipologie di fibre), dalla
	 fatica muscolare. Arrivare a produrre la forza giusta al momento giusto in
	 un sistema aventi un così gran numero di variabili non è per niente
	 scontato.

## Slide 54

Il cervello, la parte che contiene l'elaborazione ad alto livello, non si può
tenere occupato per controllare l'attivazione dei singoli muscoli o di porzioni
di esso, quindi deve esistere un sistema gerarchico di controllo motorio. La
fisiologia dimostra che ci sono delle attività a livello corticale che elabora
i segnali di alto livello e che decide che tipo di movimento fare (e.g. fuggire
o raggiungere) e tiene conto dei vari ostacoli tramite i sistemi di feedback,
stabilendo anche la modalità di svolgimento del movimento. Esistono poi
elaborazioni a livello più basso (i.e. spinali) che regolano i sistemi in modo
più dettagliato, ossia la forza muscolare tenendo conto della cinematica, delle
condizioni di affaticamento modulando l'attività dei motoneuroni, i
responsabili del controllo dei muscoli.

## Slide 55

Vediamo ora il sistema di feedback. Il sistema di controllo quindi non funziona
ad anello aperto. Si possono distinguere due tipi di sistema di controllo che
sono schematizzati in forma ingegneristica.

Il primo sistema è quello di feedback. è il classico sistema di retroazione.
- L'uscita desiderata (traiettoria desiderata) è quello che vogliamo che il
	sistema faccia. 
- Il sistema controllato (i.e. sistema muscoloscheletrico). Quel sistema che,
	ricevuto il comando motorio, produce una traiettoria in uscita. Questo
	sistema è soggetto a disturbi esterni tra cui elementi costanti quali la
	forza di gravità, ma anche altri contributi non costanti dovuti al
	cambiamento di postura. Questo sistema non è lineare perchè il momento
	prodotto da un muscolo dipende dall'angolo articolare. In teoria l'uscita di
	questo sistema dovrebbe corrispondere all'uscita desiderata, alla traiettoria
	che volevamo fin dal principio.
- Il sistema di controllo però non si fida dell'uscita del sistema controllato
	e verifica l'uscita a questo sistema. Ci sono quindi delle variabili
	controllate che vengono rilevate da sensori. Queste variabili sono molteplici
	(e.g. lunghezze muscolari, velocità di variazione di lunghezza, forze
	d'interazione tra le ossa, movimenti articolari) e sono tutte conseguenti al
	movimento realizzato e danno una descrizione in termini neurali di quello che
	è stato il gesto ottenuto. Il sistema quindi va a confrontare le informazioni
	raccolte riguardanti il movimento eseguito con il movimento desiderato. Dalla
	differenza tra traiettoria desiderata e realizzata è evidente che bisogna
	effettuare correzioni che dev'essere effettuata.
- Il controllore di feedback mappa l'errore sull'uscita con una correzione di
	comando motorio.

È evidente che questo sistema funziona correttamente solo se il movimento è
lento e la trasmissione del segnale è veloce. Il nostro sistema nervoso però,
per quanto concerne la velocità di trasmissione del segnale è piuttosto scarso
(~10m/s, le più veloci dell'ordine di 100m/s).

## Slide 56

Quando ci sono perturbazioni molto rapide deve entrare in gioco un sistema di
tipo balistico.  Si deve fare una previsione della perturbazione (i.e. dello
stimolo in arrivo) e preparare in anticipo la risposta, di modo che arrivi nel
momento in cui c'è la perturbazione.

Il controllore feedforward si aggiunge a quello di feedback, di cui abbiamo già
verificato l'esistenza. Integra le informazioni anticipatorie relative al
disturbo in modo da fornire un comando motorio più velocemente rispetto al
controllore feedback. Il controllore feedforward è, inoltre, in grado di
modulare il guadagno del controllore di feedback.

### Riferimenti bibliografici

Slides: "1-DinamicaSinergiePostMot_Frigo"
