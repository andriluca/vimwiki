# Lezione 2 --- Controllo motorio e struttura scheletrica

## Slide 1

Le componenti di questo sistema complesso. La prima componente è la struttura
di sostegno. La macchina automotrice ha delle strutture rigide che servono a
sostenere il corpo e dare punti di giuntura ai muscoli e protezione agli organi
interni. Queste funzioni sono mediate dallo scheletro, ovviamente.

## Slide 2

Lo scheletro ha una struttura molto complessa. Si parla di sistema assiale
(colonna e bacino) e sistema appendicolare (arti superiori e inferiore).

Possiamo notare che gli arti superiori e quelli inferiori hanno delle analogie
tra loro: presentano un osso lungo prossimale (omero e femore) e poi due ossa articolate tra di loro (radio e ulna; tibia e perone). 

Le stutture è simile anche se funzionalmente sono molto diverse: nel caso di
perone e tibia abbiamo un collegamento tramite sinartrosi (articolazione
cartilaginea senza movimento relativo. probabilmente in precedenza vi erano
articolazioni tra tibia e perone a livello di filogenetica). In certi casi il
perone può essere tolto e utilizzato per fare innesti ossei da altre parti. Il
perone quindi non ha una grande utilità, se non quella di mantenere
l'attaccatura dei muscoli vicini. Radio e ulna sono estremamente importanti
perchè sono le due ossa che, articolate in modo opportuno permettono il
meccanismo di pronazione e supinazione del braccio. Ci si fa caso pensando che
siamo in grado di pronare e supinare l'avambraccio qualunque sia l'angolo
assunto dal gomito. 

La mano è estremamente complessa e la maggiorparte degli ossicini vengono
attivati da muscoli che sono presenti a livello dell'avambraccio.

Oltre alla funzione di struttura portante, principalmente svolta dalla colonna,
il gran pilastro poggiato sulla base del bacino e che sostiene la parte
superiore, vi è la funzione di tipo protettivo (si pensi alla gabbia toracica,
il bacinoche protegge le viscere e la scatola cranica). Un'ulteriore funzione è
quella di riserva di minerali (Ca, P) che possono essere messi in circolo per
alcune funzioni biologiche. Le ossa lunghe presentano un midollo che ha una
funzione ematopoietica: producono globuli rossi. Le ossa si presentano
apparentemente come lunghe strutture passive, ma in realtà sono vive. Queste
hanno un meccanismo di rigenerazione continua, per cui attraverso gli
osteoplasti vengono sciolte parti minerali e attraverso gli osteoblasti vengono
ricostruiti nei punti distrutti dagli osteoplasti. L'osso continua a
rigenerarsie nel giro di pochi anni viene rigenerato completamente tutto il
nostro scheletro. In questo processo rigenerativo lo scheletro può subire
deformazioni, in seguito a danni e infiammazioni. La struttura quindi non è
fissa ma è biologica e viva.

## Slide 3

Caratteristiche funzionali dello scheletro:

1. Possiede la capacità di crescita, adattamento e riparazione
2. Svolge le seguenti funzioni:
	1. Supporto (sostegno del peso dei vari segmenti, attaccatura di muscoli).
	2. Movimento (permesso dalle articolazioni).
	3. Protezione di organi interni vitali (cervello, midollo spinale, organi
		 toracici, viscere).
	4. Riserva di minerali (calcio, fosforo, sodio, potassio, magnesio).
	5. Formazione di cellule del sangue (i.e. globuli rossi) nel midollo osseo.

## Slide 4

Dal punto di vista meccanico, questo trasmette le forze dalla parte alta del
corpo verso i piedi. Questo avviene attraverso la colonna che distribuisce il
carico alle due articolazioni delle anche le quali, attraverso le teste e i
colli femorali, trasmettono le forze al femore, alla tibia e al piede.

Queste forze cambiano nel tempo e in base alle condizioni in cui ci si trova.

## Slide 5

Le forze sono di natura anche molto diversa a seconda di come il nostro corpo
(e quindi anche il nostro scheletro) si atteggia. Nella slides sono riportati i
valori in percentuale del peso del corpo delle varie parti. 

Schematizzando lo scheletro con le varie strutture importanti abbiamo il
segmento superiore che riassume testa, braccia e torace (HAT), il bacino che fa
da base d'appoggio, le due cerniere alla base del bacino (le anche) e i due
piloni che rappresentano gli arti inferiori. questo sistema è instabile dal
punto di vista meccanico: le masse dei segmenti stanno al di sopra delle basi
d'appoggio o comunque al di sopra del fulcro attorno al quale possono ruotare.
Una massa che sta sopra un fulcro è in un equilibrio instabile e tende ad
oscillare. Se oscillasse verso destra, come mostrato nella slide, vi sarebbe la
produzione di una forza d'inerzia (-ma): l'accelerazione verso destra produce
forza d'inerzia con verso a sinistra che si trasmette poi alla base della
cerniera. Avremo la forza che sbilancia e spinge il bacino verso sinistra.
Andando verso sinistra si inclinano i due piloni che sostengono il bacino e si
sviluppano delle forze orizzontali. Le cerniere ideali trasmettono solo forze e
non momenti: avendo due cerniere agli estremi di un arto le uniche azioni che
possono trasmettersi dalla parte superiore a quella inferiore è una forza
diretta come l'asse che congiunge le due cerniere. Se il corpo si è già mosso
un po' la forza che deve svilupparsi sull'asse delle due cerniere comprende sia
una componente orizzontale che una verticale. Questa componente orizzontale
produce un'accelerazione e, per inerzia, si genera la forza Fi nel baricentro
del bacino. Così abbiamo l'equilibrio delle forze in condizioni dinamiche
(forza d'inerzia = forza accelerante).

## Slide 6

Esempio di una piccola perturbazione e dell'effetto che questo provoca sul
corpo.

## Slide 7

Equilibrio interno del sistema. Per evitare che si accartocci il corpo, servono
delle azioni interne, dei muscoli, attorno alle cerniere e ai vincoli
cinematici.

Rimane il problema dell'equilibrio esterno: anche se la struttura dovesse
essere resistente alle perturbazioni, se la forza peso uscisse dalla base
d'appoggio il corpo cadrebbe in modo quasi rigido. Per evitare questo ci sono i
muscoli che agiscono intorno alle articolazioni.

## Slide 8

Capiamo quali sono i muscoli necessari ad evitare questi tipi di cadute (quando
il tronco della persona si inclina verso sinistra).

Se la testa, rispetto al tronco, tende a mantenersi per inerzia nella sua
posizione e il tronco andasse verso destra, dovrebbero attivarsi dei muscoli
che stanno sulla parte controlaterali (flessori laterali del collo di
sinistra). Allo stesso modo coniderando il tronco: dei muscoli devono
sorreggere il busto, che cade verso destra, lavorando a sinistra (erettori
spinali laterali di destra). Il bacino si sposta verso sinistra, quindi
l'angolo  tra bacino e coscia sta aumentando. Per impedire un aumento anormale
si lavora con gli abdutori dell'anca destra. Stessa cosa può essere ottenuta da
muscoli antagonisti ma sinergici, gli adduttori dell'anca di sinistra.
Consideriamo il ginocchio fisso. Tra gamba e piede abbiamo bisogno di muscoli
che impediscono l'eversione del piede (eversione: la pianta va verso l'esterno, inversione: pianta verso l'interno).

## Slide 9

Nel piano sagittale. Se il soggetto per cadere sta compiendo questo tipo di
movimento, il tronco si sta spostando verso l'avanti. La testa tenderebbe a
rimanere ferma ma viene trascinata in avanti dal tronco per cui significa che
viene trascinata da muscoli che sono flessori del collo. Per impedire che il
tronco cada bruscamente in avanti servono i muscoli erettori spinali che fanno
da tirante sulla colonna. Per evitare che l'anca si fletta cadendo giù servono
gli estensori dell'anca. per impedire che il ginocchio si fletta servono gli
estensori del ginocchio. Servono i flessori plantare. Questi muscoli sono
quelli che impediscono la caduta del soggetto e sono perciò detti
antigravitari.

## Slide 10

L'arto inferiore.

## Slide 11

è una struttura piuttosto complessa anatomicamente. Il femore, che si articola
nell'articolazione dell'anca, ha una testa che entra nell'acetabolo (coppa che
accetta la testa femorale). L'anca è, cinematicamente, una cerniera sferica
(3GDL): abduzione/adduzione, flesso/estensione (antero-posteriore, nel piano
sagittale), rotazione interna/esterna (attorno all'asse verticale). Il femore
ha questa forma particolare, una "mensola" che sporge, con questo collo
femorale che sporge medialmente rispetto all'asse della diafisi (i.e.
componente lunga) femorale. Quindi nella parte interna abbiamo una mensola a
sbalzo. Se applicassimo un carico direttamente sulla testa del femore avremmo
un momento flettente su tutto il collo femorale. Il momento flettente aumenta
linearmente verso il punto centrale della mensola: dal collo prossimale (dove
abbiamo la testa femorale) a dove ho l'incastro sulla diafisi abbiamo un
aumento della sezione (massiccio trocanterico) per sopportare grandi carichi il
cui momento cresce con la distanza dal punto d'applicazione della forza. La
diafisi femorale che non è perfettamente rettilinea. Alla base del femore c'è
il massiccio dei condili, che sono le due estremità che poggiano sul piatto
tibiale. Nel piano frontale sono due superfici convesse, che poggiano su due
superfici concave, non perfettamente congruenti, chiamate piatti tibiali. Nel
piano sagittale le strutture sono più complesse. Al di sotto di queste
strutture c'è la tibia con superfici leggermente concave che supportano il
carico del femore. La tibia si prolunga verso il basso (verso i malleoli). Il
vero malleolo tibiale è quello mediale, quello laterale invece è la
protuberanza del perone, che si trova lateralmente. Il perone è attaccato alla
tibia tramite cartilagine fibrosa e nella parte superiore, c'è la testa del
perone o capitello fibulare. I due malleoli si articolano su un osso che è
detto astragalo e fa parte del piede e una serie di ossicini che formano una struttura complessa del piede. 

In postura eretta, considerando che i piedi sono avvicinati rispetto al bacino,
ci ritroviamo in una condizione mostrata dalla slide. Si definisce l'asse
meccanico quello che collega il centro articolare dell'anca, quello del
ginocchio (il centroide, più propriamente) e il centro della caviglia (aka
articolazione tibio-astragalica). Quest'asse meccanico è detto così perchè
collegando queste tre articolazioni permetterebbe di trasmettere un carico
dall'alto verso il basso senza produrre alcun momento articolare. Permette
quindi il sostegno di un carico esterno senza richiesta di momento e quindi
senza azione muscolare. Nella postura eretta quest'asse è inclinato rispetto
all'asse verticale di circa 3-4 gradi. Quest'angolo può cambiare se allargo i
piedi. L'altro asse importante è quello della diafisi femorale. Siccome la
diafisi è inclinata rispetto all'asse meccanico per via della sporgenza del
collo femorale, ci sono 4-5 gradi d'inclinazione tra l'asse della diafisi
femorale e l'asse meccanico. Quindi questo da luogo ad un momento flettente sulla parte alta del collo del femore e sul resto della diafisi. L'asse della tibia si trova esattamente allineato come l'asse meccanico e questo coincide con l'asse meccanico in condizione fisiologiche. Ciò significa che una forza trasmessa sull'asse meccanico non produce momenti sulla tibia ma solo compressione.

Immaginiamo ora che la persona sia in piedi. La forza che esercita sulla
struttura è verticale e la forza non si sviluppa lungo l'asse meccanico ma
lungo l'asse verticale che passerebbe per il centro della caviglia (linea
grossa nera). Ciò significa che una forza di questo tipo passerebbe
prossimalmente rispetto al centro dell'anca, producendo un momento che tende ad
addurre l'arto sull'anca. La situazione richiede quindi un'azione muscolare per
equilibrare quest'adduzione. Si richiede quindi l'azione dei muscoli abduttori
dell'anca, che sono quasi tutti attaccati sulla protuberanza ossea chiamata
grande trocantere (lato femore) e sull'ala iliaca (lato bacino) (vedi la
figura). Questi muscoli agiscono in questo senso per sostenere il momento
adduttorio di una forza esterna. Al ginocchio notiamo che il carico verticale,
che passa per il centro della caviglia, avrà un certo momento adduttorio
rispetto al centro articolare del ginocchio. è necessario l'intervento
abduttorio del ginocchio: questo è mediato dai legamenti articolari, cosiddetti
collaterali laterali. L'azione del momento è sviluppata dall'azione dei
legamenti sul lato laterale e dalla contrapposizione della forza di contatto
tra le ossa medialmente. Come geomtria possiamo notare che, essendo l'asse
della diafisi femorale inclinato di circa 4-5 gradi rispetto l'asse meccanico,
il quale è inclinato di 3-4 gradi rispetto alla verticale, l'asse della diafisi
femorale è inclinato di circa 7-8 gradi rispetto all'orizzontale. Rispetto
all'orizzontale, quindi, l'asse della diafisi del femore spazia un angolo di
circa 81 gradi. L'asse della tibia, rispetto all'asse orizzontale (ossia l'asse
del ginocchio) è circa 93-94 gradi (la tibia è inclinata verso il basso).
Quest'asse orizzontale è, idealmente, l'asse di flesso-estensione delle
ginocchia e affinchè il funzionamento sia fisiologico bisognerebbe che l'asse
sia in comune tra le due ginocchia, altrimenti vi sarebbero discrepanze nel
movimento.

### Note

Un esempio di superfici congruenti è quella di testa del femore e acetabolo.
Il perone è anche noto col nome di fibula.

## Slide 12

Kapandij fa riferimento alla struttura dell'arto inferiore cercando di giustificare la morfologia di queste ossa. 

In particolare vediamo il piano sagittale, quello che divide il corpo in due
metà (destra e sinistra). In questo piano sagittale abbiamo una morfologia
particolare: i condili femorali che poggiano sul piatto tibiale. Qual è la
giustificazione della forma? Supponiamo di avere due aste con estremità
appiattite che sono a contatto. Se vogliamo che avvenga la rotazione relativa
delle due aste è necessario che vi sia una superficie concava su un'asta ed una
convessa sull'altra. Con questa morfologia c'è un piccolo movimento possibile
ma, naturalmente, il becco posteriore dell'asta di sotto va ad interferire con
l'asta di sopra, quindi il movimento sarebbe limitato. Per poter avere un
movimento più ampio bisognerebbe scavare un incavo dentro cui si insinua il
becco. Così facendo però la struttura si indebolirebbe (figura d). Per renderla
ancora sufficientemente robusta basta spostare l'intera asta superiore in
avanti rispetto alla convessità. Questo spiega la configurazione del femore. I
condili sono dunque spostati indietro rispetto alla diafisi femorale. Questo è
giustificato dalla possibilità di poter aumentare l'angolo di flessione di
questo segmento rispetto all'altro. è possibile migliorare ulteriormente la
struttura se la diafisi tibiale venisse spostata indietro. Si crea dello spazio
all'interno del quale possono essere alloggiati i muscoli quando si ha una
flessione completa dell'arto.

Un'altra cosa che vale la pena indagare è la curvatura della diafisi femorale.
Si assimila il femore ad una trave caricata in punta. Secondo la teoria di
Eulero, a seconda dei vincoli all'estremità dell'asta caricata di punta,
subisce delle deformazioni che sono ben descritte sotto certe ipotesi di
densità e resistenza dei tessuti. Si ipotizza che nella parte inferiore del
femore vi sia un incastro e nell'estremità superiore vi sia una cerniera (piano
frontale), dando luogo ad una curvtura con un punto di flesso ad un certo
punto. Nel piano sagittale invece l'osso non ha un vincolo d'incastro ma una
cerniera libera di ruotare.Ci sono quindi due cerniere da considerare e la
teoria di Eulero stabilisce che vi sia un'onda di questo tipo senza flessi.

Lo stesso ragionamento esposto per il femore vale per la tibia. Nel piano
sagittale ha due cerniere e si incurva senza flessi. Nel piano frontale invece
ha una forma più particolare.

Un'altro ragionamento interessante proposto da Kapandij è quello che riguarda
la "sintassi del movimento". Queste ossa hanno anche delle particolari
rotazioni lungo lo sviluppo longitudinale. Questo discorso lo si può capire
guardando la vista 3D dall'alto, come mostrata sulla destra. Si prenda come riferimento l'asse di flesso-estensione del ginocchio (quello che congiunge i due epicondili del femore). I due epicondili collgati tra di loro danno luogo all'asse i flesso-estensione del ginocchio (perpendicolare al piano sagittale). Ci accorgiamo che quest'asse, se considerato come riferimento, nella parte superiore (a livello del collo femorale) questo non si trova nello stesso piano dei condili femorali: il grande trocantere si trova più indietro della testa del femore. Questo fenomeno si chiama anteversione del collo femorale: rispetto al grande trocantere la testa del femore è rivolta più in avanti. Questo significa che, spostandoci più in basso c'è un'intratorsione del femore: rispetto all'asse del collo femorale, spostandoci verso il basso, quando ci si trova a livello dei condili femorali, si è subita un'intratorsione.

Al di sotto del ginocchio abbiamo generalmente che la maggiorparte delle
persone in postura ortostatica hanno i piedi leggermente divaricati. Rispetto
all'asse delle ginocchia l'asse dell'articolazione tibio-astragalica (caviglia)
risulta ruotata con la parte interna in avanti. Questo significa che vi è
un'extratorsione della tibia rispetto all'asse dei condili femorali. Quindi,
recapitolando e partendo questa volta dall'alto abbiamo un'intratorsione del
femore e un'extratorsione della tibia.

In più c'è qualche grado di rotazione a livello del collegamento tra tibia e
femore a livello del ginocchio. In sostanza si ha una compensazione tra
l'intratorsione del femore e l'extratorsione della tibia. 

Alla fine ci troviamo con le punte dei piedi leggermente orientate lateralmente
rispetto al piano di progressione. Kapandij giustifica questo fatto
(rappresentato all'ultima riga) con il fatto che, quando si cammina, il bacino
si orienta in avanti sul lato che progredisce. Ruotando in avanti, la persona
mette il piede nella direzione di progressione. In questo modo il corpo sfrutta
al meglio la lunghezza del piede per produrre la propulsione.

### Note

Condili: parti del femore simili a ruote che poggiano sul piatto tibiale.
Epicondili: protuberanze laterali o mediali dei condili.

## Slide 13

Nel piano sagittale il femore ha un'onda unica senza flessi. L'asse meccanico
di solito p leggermente inclinato con il piede leggermente indietro dispetto
alla testa femorale di circa 2-3 gradi. Questo comporta che se abbiamo una
forzadi reazione del terreno che agisce nella direzione verticale passante per
il centro dell'anca (i.e. annullare il momento all'anca), in questo caso
richiede un momento al ginocchio, perchè la forza di reazione al terreno
passerà davanti al centro del ginocchio producendo un momento estensorio. Per
compensare questo momento estensorio entrano in gioco i muscoli flessori del
ginocchio. In realtà quano si raggiunge l'estremità del movimento, come in
questo caso (il ginocchio è già esteso) entrano in gioco anche le azioni di
strutture passive quali i legamenti, la capsula articolare, che impediscono
l'iperestensione del ginocchio. Se siamo in condizioni limite avremo una
leggera flessione del ginocchio dovuta ai muscoli flessori. Quali sono i
flessori del ginocchio? I più efficaci sono quelli rappresentati qui, dei
flessori plantari che sono indicati perchè servono sia un'azione dei muscoli di
tipo flessorio al ginocchio per sostenere il momento esterno estensorio, sia
l'azione di muscoli flessori plantari per compensare l'azione della forza che
produce un momento flessorio dorsale. Il soleo (monoarticolare, flessore
plantare alla caviglia) e il gastrocnemio (due ventri muscolari, i gemelli.
Agisce non solo alla caviglia ma sopra il ginocchio con un avvolgimento dei
tendini prossimali sopra i condili femorali), che compongono il tricipite della
sura (o surale). Questi muscoli sono particolarmente indicati perché il soleo
compensa e contrasta l'azione dorsiflessoria della forza esterna, il
gastrocnemio aiuta a sostenere l'azione dorsiflessoria della forza esterna ma
in più agisce come flessore del ginocchio, aiutando a sostenere il momento
estensorio applicato al ginocchio.

### Note

Surale: riferito alla tibia.

## Slide 14

Le cose diventano più complesse andando a vedere i movimenti consentiti da
queste articolazioni. Studiamo il caso di un arto sinistro che si accuccia.
Qual è il problema? A causa dell'introversione del femore e dell'extratorsione
della tibia ci troviamo ad avere l'asse di flessione del ginocchio (il nostro
riferimento di prima) che inizialmente è diretto ortogonalmente al piano
sagittale ma, al di sotto della tibia, l'astragalo permette una rotazione
attorno ad un asse che è inclinato rispetto al piano che contiene l'asse del
ginocchio. I piedi sono ruotati in fuori e si flettono intorno ad assi che non
sono paralleli alle ginocchia ma inclinati sul piano orizzontale come mostrato.

Se volessimo quindi far scendere la testa del femore verso il basso,
permettendol'accucciamento dell'arto, troviamo che tra tibia e femore la
rotazione avverra attorno all'asse medio-laterale del ginocchio, al di sotto
agirà intorno all'asse della caviglia, che è inclinato, per cui il movimento
risultante non giacerà sul piano sagittale ma sarà un movimento che si sposta
con una traiettoria rossa riportata. Notiamo che la testa del femore tende a
scendere lateralmente, per effetto della diversa inclinazione degli assi della
caviglia e del ginocchio.

Una delle condizioni per cui si verifica ciò è il fatto che il piede sia fisso
al terreno e supponendo che non vi sia un'altra rotazione possibile se non
quella attorno all'asse di flesso-estensione della caviglia. Se volessimo avere
un accucciamento dell'arto senza lo spostamento laterale dell'anca dobbiamo
dare la possibilità alla tibia di ruotare non solo attorno all'asse di
flesso-estensione della caviglia ma anche attorno ad un asse ortogonale a
questo che permetta la compensazione di questo movimento. Quindi quello che in
realtà accade è che bisogna concedere un ulteriore grado di libertà a livello
dell'articolazione col piede: una rotazione di pronazione-supinazione rispetto
ad un asse anteroposteriore del piede.

## Slide 15

Questo è quello che succede in realtà perchè effettivamente, al di sotto
dell'astragalo, l'osso su cui poggia la tibia che permette solo la
flesso-estensione, esiste un'altra articolazione tra astragalo e calcagno
(articolazione sotto-astragalica) che ha un'asse di rotazione non proprio
antero-posteriore, un po' inclinato ma, ad ogni modo, ortogonale all'asse della
tibio-astragalica. Questo da una possibilità alla tibia di ruotare non solo in
flesso-estensione ma anche in prono-supinazione. Grazie a ciò l'anca può
scendere verticalmente: flessione del ginocchio e una flessione composta
attorno un asse medio-laterale ed uno antero-posteriore. Per rotazioni molto
elevate la traiettoria devia comunque da quella che starebbe sul piano
sagittale. 

Quello che succede in realtà non è riportato nella figura: il piede dovrebbe
sollevarsi sulle punte quando l'arto è molto accucciato e la rotazione non
avviene più solo attorno alla caviglia ma anche attorno all'asse che contiene
le teste metatarsali. A quel punto si può scendere mantenendo le ginocchia sul
piano sagittale.

## Slide 16

Questo modello serve a chiarificare ulteriormente il concetto. Anatomicamente è complesso.

Cosa ricordare? L'*articolazione della caviglia è detta tibio-tarsica*. Questo perchè il tarso comprende tutte le ossa che stanno davanti la caviglia, compreso il calcagno. Si può modellizzare quanto segue.

1. Flesso-estensione: garantita dal movimento tra tibia e astragalo tramite un
	 asse medio-laterale.
2. Asse della sotto-astragalica: antero-posteriore (che in realtà è un
	 inclinato verso la parte mediana del piede e verso l'alto). Non è proprio
	 perpendicolare alla tibio-astragalica. Tra astragalo a calcagno esiste
	 quest'articolazione complessa sia come forma che come superficie. L'asse di
	 quest'articolazione è visibile dall'immagine ed è inclinato verso l'alto nel
	 piano sagittale e medialmente nel piano orizzontale.
3. Al di sotto vi sono articolazioni complesse, semplificabili con una cerniera
	 sferica (giunto di Lisfranc).
4. Le falangi delle dita sono disposte in un certo modo e permettono la
	 rotazione dell'avampiede rispetto ad un piano che contiene le falangi
	 attraverso un asse di rotazione metatarso-falangeo (riportato in verde nella
	 figura).

### Note

In arancione nelle immagini anatomiche è riportato l'astragalo. In verde
pisello invece il calcagno.

## Slide 17
## Slide 18
## Slide 19
## Slide 20

L'arto superiore è costituito da due ossa principali. un osso prossimale
(omero) e due ossa appaiate che costituiscono l'avambraccio (radio e ulna).

Questa struttura anatomica permette dei movimenti molto ampi dell'arto
superiori. Più ampie rispetto di quelli che sono consentiti all'arto inferiore.
Le articolazioni hanno caratteristiche diverse rispetto a quelle dell'arto
inferiore.

Vediamo in termini generali l'articolazione tra omero e tronco. L'intero
braccio compie una serie di movimenti con molti gradi di libertà. è in grado di
sostenere carichi in direzioni anche molto diverse: spingere verso il basso, appendersi ecc. Ha una gran mobilità che è garantita dalle articolazioni.

Per quanto riguarda la spalla si parla di complesso articolare della spalla.
Questo perchè, quello che vediamo come movimento del braccio rispetto al tronco
è garantito in realtà da ben 5 articolazioni. L'omero non è fissato
direttamente al tronco ma fissato con un'articolazione di rotazione (3GDL) alla
scapola. La scapola è un osso piatto che si trova posteriormente al tronco ed
ha una forma triangolare con due prominenze (o bracci): l'acromion e il
coracoide. 

L'articolazione vera e propria tra omero e scapola è detta gleno-omerale.
Questa è un'articolazione di accoppiamento sferico. La superficie dell'omero,
la quale è convessa e abbastanza approssimabile ad una sfera, si appoggia nella
cavità glenoidea. L'accoppiamento tra le superfici non è congruente perchè la
superficie glenoidale in realtà è molto appiattita. L'omero quondi non è ben
fissato all'interno di quest'articolazione e può sfuggire in avanti o indietro
a quest'articolazione. Questa è l'articolazione in senso fisiologico perchè è
un accoppiamento di due superfici seppur non ben congruenti contenute da una
certa capsula articolare con strutture fibrotiche intorno. La scapola a sua
volta non è ben fissata al tronco. Questo è un osso che si appoggia dietro la
gabbia toracica con possibilità di ampi movimenti di scivolamenti
medio-laterali. Dei muscoli tengono quest'osso in posizione. Quest'osso piatto,
attraverso l'acromion compie un articolazione con un osso posto nella parte
anteriore del tronco, la clavicola. La clavicola è l'unico osso ad essere
attaccato al tronco. Questo ha un'articolzione fisiologica vera e propria con
la parte superiore dello sterno e parzialmente con la prima costola
(articolazione sterno-costo-clavicolare). Questa è l'unica articolazione
fissata, tutto il resto di quello che abbiamo detto potrebbe cedere.

Il sistema è reso stabile dalla presenza di un gran numero di legamenti e da
muscoli. Questi ultimi vincolano l'ampiezza dei movimenti e costringono queste
ossa a rimanere in sede sopportando i carichi applicati dall'esterno. Questi muscoli costituiscono delle sorta di articolazioni "virtuali".

Le articolazioni virtuali che si realizzano sono:

1. Sotto-deltoidea. Non è proprio un accoppiamento tra due ossa ma,
	 semplicemente, al di sotto dell'acromion e della clavicola, si forma nella
	 parte anteriore una cupola sostenuta dal muscolo deltoide. Questo muscolo si
	 trova lateralmente sopra la spalla. Al di sotto di questo muscolo si trova
	 una struttura fibrosa con una borsa sierosa che forma da tetto a
	 quest'articolazione virtuale. L'omero che potrebbe scivolare in alto per il
	 fatto che l'articolazione con la scapola non è congruente, viene impedito di
	 risalire da questa cupola costituita dai muscoli che stanno sopra la spalla. 
2. Scapolo-toracica: quest'osso piatto che si appoggia nella parete posteriore
	 del torace può scivolare in avanti o in dietro ed è tenuta in posizione da
	 muscoli sottoscapolari che la controllano e permettono di tenerla aderente
	 al tronco ed eventualmente di sopportare carichi che tendono a farla ruotare
	 in un senso o nell'altro. Quest'accoppiamento, che è un'appoggio di un osso
	 piatto su una superficie convessa del tronco, è considerata un'articolazione
	 virtuale. 

Nel complesso articolare della spalla abbiamo fondamentalmente 5 articolazioni:
3 di tipo fisiologico (sterno-costo-clavicolare, gleno-omerale,
acromio-clavicolare) e le due "virtuali" (sotto-deltoidea e scapolo-toracica).

Nell'arto superiore poi troviamo l'articolazione del gomito, tra omero, radio e
ulna. Più distalmente vi è una serie di articolazioni tra radio, ulna e una
serie di ossicini che non vengono analizzati. Questi ossicini formano il carpo,
la parte della mano relativa al palmo, poi vi sono i metacarpi e le falangi che
costituiscono poi le dita. Il numero di gradi di libertà della mano è molto
elevato e i movimenti permessi sono molteplici. I muscoli che compiono questi
movimenti complessi si trovano per lo più nell'avambraccio. Esistono anche dei
muscoli interossei nella mano di cui però non trattiamo. 

I movimenti concessi dalla rotazione del gomito sono di flesso-estensione e
pronazione-supinazione. Come avviene questo meccanismo? Vediamo attentamente
come è fatta l'articolazione del gomito.

## Slide 21

Si immagini di avere l'omero che si eleva verso l'alto nel movimento di
abduzione, sostenendo il proprio peso. per poter sollevare l'arto bisogna che
la scapola sia fissata al tronco. I muscoli fissatori sono quelli che producono
l'abduzione della scapola e che producono forze in senso antiorario (qui è
riportato un arto destro). Il trapezio è un muscolo molto ampio che ha diverse
funzioni perchè attaccato in varie parti del corpo. Nella parte superiore il
trapezio è attaccato al collo, il trapezio medio è attaccato alle vertebre a
livello dorsale e il trapezio inferiore è attaccato più in basso. L'insieme
forma un unico muscolo piatto. Queste tre porzioni producono un'abduzione della
scapola e un sostenimento del peso del braccio quando questo va in abduzione.
Insieme a questo complesso vi è anche il gran dentato, un muscolo attaccato al
di sotto della scapola, che tra l'altro serve anche a tenere la scapola
aderente al tronco. Quest'insieme di muscoli quindi permettono all'omero il
sollevamento laterale (abduzione).

Ci sono dei muscoli che possiedono un effetto opposto, ossia quando l'omero
spinge verso il basso è necessario che fissino la scapola impedendole di
ruotare in senso orario. Questi muscoli sono il romboide e l'angolare, che
danno una spinta verso il all'omero. Questi sono i principali. 

Ci sono anche il succlavio e il piccolo pettorale che stanno anteriormente al
tronco. Questi esercitano un'azione di trazione nella parte anteriore del
torace.

## Slide 22

Tra le varie cose, è importante ricordare alcuni elementi di questo complesso
scapolo-omerale. In particolare bisogna ricordare il ritmo scapolo-omerale.
Questo è una particolare sinergia di muscoli che permette un movimento
combinato dell'omero, della scapola e della clavicola. L'intero complesso viene coordinato nel movimento da un'insieme di attivazioni muscolari che avviene in modo automatico. Questo ritmo scapolo-omerale cos'è? 

Immaginiamo di far abdurre un arto ad un soggetto e di doverne studiare la fluoroscopia (situazione riportata effettivamente nella slide). Ricordando che la fluoroscopia è una tecnica d'immagine che permette di vedere il movimento relativo delle ossa. 

L'omero va in abduzione e, data la sua conformazione, molto rapidamente
comincia a contattare l'acromion con una sua protuberanza. Quindi il movimento
sarebbe teoricamente fermato da questo limite cinematico. Il sistema di
controllo, che prima teneva la scapola fissa al tronco, produce un momento in
senso orario (ricordando che è un arto sinistro quello in esame) di questa,
favorendo l'ampiezza del movimento dell'omero. A questo poi si aggiunge anche
il movimento della clavicola che prima era appiattita e poi comincia ad
inclinarsi verso l'alto ed insieme alla scapola favorisce il movimento di
abduzione dell'omero. Questo movimento è importante perchè senza di esso
potremmo abdurre fino a circa 90 gradi. È un movimento automatico perchè
composto da molti muscoli fissatori di spalla e clavicola che intervengono in
modo sinergico per permettere quest'ampiezza di movimento.

## Slide 23

Modellino del movimento permesso dall'articolazione gleno-omerale che può
raggiungere circa un centinaio di gradi. In assenza del movimento della
clavicola e della scapola, il movimento è molto limitato.

## Slide 24

Se consideriamo il movimento d'elevazione della scapola e della clavicola
possiamo notare quanto il movimento diventi ampio.

## Slide 25

Un close-up mostra le variazioni di lunghezza di molti muscoli. In verde è
mostrato il deltoide.

## Slide 26

L'altra articolazione è quella del gomito. I movimenti di flesso-estensione e pronazione-supinazione dell'avambraccio sono permessi da una particolare combinazioni di superfici all'interno dell'articolazione del gomito. Consideriamo la parte superiore (omero), la cui parte distale s'allarga a formare la cosiddetta paletta omerale. All'interno di questa struttura vi sono delle superfici che possono essere schematizzate come un rocchetto ed una sfera. Il rocchetto è una cerniera monoassiale, con una sagomatura diversa da quella cilindrica, su cui si appoggia la parte prossimale dell'ulna, che ha delle superfici che vanno coniugate a quelle del rocchetto (o troclea omerale). L'ulna si accoppia in modo tale da far combaciare al solco dell'omero una cresta sporgente. Questa cerniera permette il movimento di flesso-estensione dell'ulna rispetto all'omero. Questo movimento ha una certa ampiezza notevole (da un'estensione di 180 gradi fino ad una flessione completa a 180 gradi). Questo movimento è garantito dal fatto che la superficie dell'ulna prossimale si inserisce nella paletta omerale sia posteriormente che anteriormente di modo da avere una maggior ampiezza di movimento. 

L'immagine lo mostra bene. L'ulna ha una superficie di contatto perpendicolare
all'asse dell'osso stesso e una posteriore. La superficie posteriore è
costituita da una protuberanza ossea, il dente dell'olecrano. Questa
protuberanza è un punto d'attaccatura del tricipite brachiale. L'altro dente,
perpendicolare all'asse di sviluppo dell'ulna, è il dente coronoidale.
L'ampiezza è garantita dal fatto che l'omero, in vicinanza dell'articolazione,
si assottiglia nella zona centrale, permettendo con una fossetta posteriore,
l'accettazione del dente dell'olecrano e, con una fossetta anteriore,
l'accettazione del dnte del coronoide. In alcuni soggetti vi è addirittura una
compenetrazione del dente nel rocchetto per via di un buco che però non crea
problemi e consente movimenti più ampi. Il problema non sussiste perchè la
paletta omerali ha due "pilastri" in direzione mediale e laterale, che
sostengono l'articolazione. Quindi l'articolazione omero-ulnare consente solo
un grado di libertà, quello di flesso-estensione.

La pronazione-supinazione è consentita dal radio che, nella parte prossimale,
si articola con una coppetta sferica con la sfera dell'epicondilo omerale. La
sfera consente 3GDL rispetto all'omero, per cui il radio può ruotare lungo
l'asse longitudinale, lungo un asse medio-laterale (asse del rocchetto)
consentendo la flesso-estensione del radio sullo stesso asse dell'ulna. Il
radio potrebbe addirittura compiere adduzione-abduzione. Si parla quindi di
articolazione radio-omerale. 

Il radio è anche articolato con l'ulna e viene tenuto a contatto da un
legamento anulare, una fascia che contiene il capitello radiale e che lo
mantiene adiacente all'ulna. Nel contatto tra il capitello radiale e la sommità
dell'ulna vi è un'articolazione, la radio-ulnare prossimale.


## Slide 27

Il radio è collegato con l'ulna dall'articolazione radio-ulnare distale. Questa
permette l'accoppiamento tra una superficie concava cilindrica (radio) ed una
convessa cilindrica (ulna). L'asse di rotazione longitudinale coincide
prossimalmente con l'asse del capitello radiale e, distalmente con l'asse della
superficie cilindrica concava posta sul bordo laterale dell'ulna. Quest'asse
permette quindi una rotazione del radio attorno all'ulna, mantenendosi sempre a
contatto.

La pronazione-supinazione avviene con la rotazione del radio attorno all'asse
comune delle articolazioni radio omerale e radio ulnare distale. Tale rotazione
sposta il radio anteriormente o posteriormente, rispetto all'ulna.

Cambiano gli angoli relativi tra capitello radiale e la sua sede sull'ulna, per
questo motivo l'anello fibroso lo racchiude. Questo permette al radio di
assumere angoli diversi deformandosi di conseguenza.

Quando il palmo è diretto indietro le due ossa dell'avambraccio, trovandosi in
questo modo si trovano mediamente allineate con l'omero. L'avambraccio e il
braccio sono perciò allineati.

Quando siamo in posizione anatomica, con il palmo diretto in avanti, siccome il
radio si trova lateralmente rispetto all'ulna, si trova un certo angolo tra
l'asse dell'omero e quello dell'avambraccio. Questo è detto l'angolo cubitale.
Da l'impressione di essere abdotto rispetto al braccio ma è dovuto a questo
cinematismo.

La mano s'interfaccerà poi con il radio distale che ha una superficie notevole.
La base del radio ha una forma quasi ovoidale, per cui si possono individuare
due assi principali (medio-laterale: flesso-estensione, antero-posteriore:
abduzione e adduzione).

## Slide 28

La maggior parte dei muscoli che si trovano hanno un'azione flessoria del gomito: il bicipite brachiale (muscolo biarticolare che ha un'azione di flessione del gomito e di flessore della spalla) il brachiale anteriore etc.

Gli estensori del gomito sono costituiti dal tricipite brachiale.

## Slide 30

Colonna o rachide. Collega la base d'appoggio, il bacino con organi sostenuti
da questa colonna, il cingolo scapolare e il cranio.

Dal punto di vista archittonico abbiamo un elemento rettilineo nel piano
frontale. Nel piano sagittale, di lato presenta tre grosse curvature:

1. Lordosi cervicale.
2. Cifosi dorsale, una sorta di gobba.
3. Lordosi lombare

Al di sotto di queste vi è una struttura che è quasi un blocco unico costituito
da vertebre sacrali e coccigee. 

La colonna non è rigida ma costituita dalla sovrapposizione di strutture vicine
tra di loro dette vertebre. La loro sovrapposizione costituisce un pilastro di
sostegno.

Le costole sono articolate sulla colonna con faccette articolari che permettono
il movimento necessario per la respirazione (ampliare il volume della gabbia
toracica durante l'inspirazione e poi ridurlo durante l'espirazione). Questo
avviene grazie al fatto che le costole si articolano sulla colonna e poi anche
anteriormente sullo sterno. Queste articolazioni permettono il movimento di
elevazione delle costole. L'aumento di volume della gabbia toracica avviene
perchè la rotazione verso l'alto la loro distanza diventa la distanza dalla
colonna (movimento del manico del secchiello: un secchiello con manico
appoggiato esternamente. Quando solleviamo il manico facendolo diventare
orizzontale, questo si allarga con l'aumento della distanza). L'inspirazione è
dunque un movimento attivo (i muscoli sollevano le costole), l'espirazione è
passivo.

Vertebre:

- 7 cervicali
- 12 dorsali o toraciche
- 5 lombari
- 5 sacrali
- 2 coccigee

### Note

- Cifosi: concavità verso l'avanti.
- Lordosi: concavità verso l'indietro.

## Slide 31

La colonna poggia sul bacino grazie alle articolazioni sacro-iliache. Le
vertebre del sacro si appoggiano con queste articolazioni al bacino che sono
poco mobili e diventano poi rigide nell'adulto.

Nella zona superiore le vertebre hanno una forma particolare. Costituiscono un
accoppiamento con le ossa del cranio che permette un gran movimento della testa
rispetto alle vertebre che stanno sotto. Le due vertebre particolari sono
l'atlante e l'epistrofeo. Sono un accoppiamento di ossicini. L'atlante presenta
delle faccette articolari sulle quali si appoggiano le ossa del cranio e che
permettono le rotazioni di rotazione assiale. Le faccette articolari
sull'epistrofeo permettono il movimento del cranio nel senso antero-posteriore.
Tra le due vertebre atlante ed epistrofeo c'è la possibilità di rotazione in
senso assiale, cioè rispetto ad un asse verticale.

## Slide 32

Le vertebre hanno una forma diversa a seconda che ci si trovi in alto o in
basso.

A livello cervicale le vertebre sono costituite da un massello piccolo. La
tipica vertebra. 

La tipica vertebra considerata è quella dorsale. Ha un massello osseo molto
importante. Tra un corpo vertebrale e l'altro c'è un disco vertebrale con
struttura complessa cartilaginea con un nucleo gelatinoso (nucleo polposo) che
permette una certa mobilità. Il carico assiale è supportato dalla
sovrapposizione di questi corpi vertebrali. posteriormente al corpo vertebrale
si trova l'arco vertebrale che è costituito dalla lamina che avvolge
posteriormente la cavità che si forma all'interno, chiamato forame vertebrale.
All'interno del forame vertebrale si forma un canale ossia il canale
vertebrale. All'interno di questo canale vertebrale si trova il midollo spinale
che si prolunga verso il basso con i vari nervi. Queste vertebre hanno processi
(protuberanze) spinosi. Il processo spinoso si trova sul piano mediale del
corpo, posteriormente. I processi trasversi si prolungano lateralmente alle
vertebre. Questa vertebra presenta un certo numero di articolazioni costituite
da diverse faccette articolari. Queste faccette articolari sono articolazioni
poco congruenti, leggermente concave. Le principali faccette articolari sono
quelle superiori ed inferiori. Queste permettono di collegare le vertebre più
in alto e più in basso permettendo certi movimenti (flesso-estensione,
rotazione).

Esistono faccette articolari con le costole: le faccette costali superiori e
faccette costali inferiori. Una costola si può muovere lungo un asse che
congiunge le faccette costali. L'allargamento della costola è dovuta alla
rotazione attorno all'asse tra faccetta costale superiore e faccetta costale
trasversa.

Andando dall'alto verso il basso il massello osseo aumenta la sezione.Il forame
vertebrale dall'alto verso il bsso si riduce in sezione.

Tra i processi trasversi e quelli spinosi si forma una concavità, un canale
laterale per ogni lato. Qui si adagiano i muscoli che danno sostegno alla
colonna.

## Slide 33

Esistono muscoli su diversi piani. Quelli più vicini alla colonna realizzano
movimenti più localizzati. Troviamo muscoli interspinali e intervertebrali.

Esistono anche muscoli lunghi (lunghissimo), il multifido. muscoli che fanno da
tiranti che devono sostenere l'albero della colonna.

Nella parte superficiale invece abbiamo le masse muscolari appiattite: il
trapezio.
