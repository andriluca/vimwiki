# Lezione 3 --- Struttura scheletrica

## Slide 1

Parliamo dei muscoli.

## Slide 2

Il sistema ha una struttura generale di questo tipo. Il nostro corpo è
interamente rivestito da muscoli. Le componenti intrinseche del muscolo sono
capaci solo di produrre forze di trazione, accorciando il muscolo. A seconda di
come i muscoli sono attaccati al corpo, dall'orientazione e altri fattori, i
muscoli hanno una diversa interazione.

## Slide 3

Questo esempio riguarda l'articolazione dell'anca. Questa è una semplice
articolazione, dal punto di vista cinematico ma è presente un gran numero di
muscoli. Possiamo fare delle considerazioni sulla base della disposizione
anatomica. Come possiamo notare, questa è una sezione su piano orizzontale a
livello della testa del femore. Si sta considerando un arto sinistro.
Superiormente possiamo visualizzare la parte anteriore del bacino. Possiamo
vedere l'azione di questi muscoli che producono l'attuazione sul segmento
inferiore, il femore. 

Dobbiamo considerare quali sono gli assi di interesse funzionale. In questo
caso l'asse di flesso-estensione (medio-laterale, rappresentato
orizzontalmente) e l'asse di adduzione-abduzione (antero-posteriore,
rappresentato verticalmente). Andandoli a rapresentare nella sezione diventa
più chiara la funzione dei vari muscoli.  Abbiamo le diverse sezioni dei
muscoli: (RF: retto femorale, IL-PS: ileopsoas, TFL: tensore della fascia
lata). Per capire che azione compiono tali muscoli si deve prima comprendere
dove si trova il centroide (i.e. baricentro) di tali muscoli (immaginando che
la forza di quel muscolo sia poi applicata al suo baricentro). Dopo si guarda
qual è la posizione dei vari centroidi rispettivamente al sistema di
riferimento che avevamo stabilito prima.

- RF: ha una leggera azione Abduttoria e un'azione flessoria.
- TFL: ha un'azione identica sia in abduzione che in flessione.
- GL min e med: i due glutei hanno evidentemente più un'azione di abduzione e
	una minima azione flessoria nel caso min ed extensoria nel caso med.
- IL-PS: azione prevalentemente flessoria ma anche in parte adduttoria.

Questa situazione non è statica ma, a seconda di come si trova il femore in
relazione al bacino, cambia la disposizione dei centroidi dei vari muscoli,
andando a modificare anche l'azione.

## Slide 4

Andiamo ad analizzare alcuni modelli di muscoli scheletrici.

A livello dell'arto inferiore troviamo il gastrocnemio e il soleo (che
compongono il tricipite surale).

- Soleo: produce flessione plantare. Ha un'origine su un'ampia superficie a
	livello di tibia e perone. è rappresentabile con un unico tirante dal
	centroide dell'attaccatura del soleo fino all'attaccatura al tendine
	d'Achille. è un attuatore monoarticolare. Cambiando l'angolo del piede
	rispetto alla tibia, cambia il braccio di leva, andando quindi a creare
	situazioni più o meno vantaggiose.
- Gastrocnemio: sono due i gemelli, rappresentabili con un unico attuatore
	perchè la funzione molto simile. Si attacca ad un inserzione del tendine
	d'achille e sopra i condili femorali in posizione posteriore. Oltre a
	produrre una flessione plantare produce quindi anche una flessione del
	ginocchio.

Il tibiale anteriore si trova invece anteriormente ed è un flessore dorsale. è
attaccato anteriormente sulla tibia e, con un tendine che si avvolge sul
retinacolo della caviglia, si prolunga sul primo metatarso ed ha un'azione
flessoria dorsale, oltre a quella supinatoria.

## Slide 5

Con il modello del muscolo sceletrico si può rappresentare l'andamento di tutti
i vari muscoli dell'arto inferiore che sono rappresentabili con delle linee
d'azione. Animando il modelli si può andare a vedere come varia la lunghezza
muscolare durante il pattern motorio.

## Slide 6

Il concetto che il muscolo agisca come un'attuatore e generatore di forza
concentrica può essere rappresentato con il modello di una molla.

Il nostro sistema di controllo invia dei comandi al muscolo per farlo contrarre
e produrre una forza d'accorciamento ma non regola esattamente il valore della
forza. Se così accadesse, le piccole perturbazioni lo porterebbero fuori dalla
situazione d'equilibrio. Il muscolo deve quindi adattarsi alle perturbazioni
prodotte da piccoli spostamenti o dalla presenza di carichi esterni. Il comando
motorio non è quindi un comando preciso di forza ma un comando che dice
"mantieni questa posizione" oppure "esegui un certo movimento". Un sistema
gerarchico di livello più basso rende realizzato questo comando. Un modo
semplice di realizzare questo concetto è quello della molla. Questa molla
intrinsecamente si adatta al carico esterno. La molla supporta a trazione e
quando il carico aumenta la molla si allunga ma poi mantiene l'equilibrio col
carico esterno. Si pensa che il sistema di controllo regola la posizione
dell'arto non in modo così preciso ma vengono variate le proprietà del muscolo
di modo che il risultato sia quello desiderato in termini di posizioni e di
carico. Si pensa che il sistema di controllo agisca sulle proprietà di questa
molla. Elementi elastici dentro il muscolo sono stati dimostrati e ci sono. C'è
un adattamento al carico esterno e ci sono dei riflessi che intervengono per
modificare automaticamente lo stato di contrazione, per cui si pensa che il
sistema di controllo sia in grado di modificare due parametri fondamentali in
una molla: la lunghezza di riposo e la rigidezza. Le rette nel grafico
rappresentano l'andamento della forza muscolare in funzione della lunghezza. In
figura sono riportate diverse rette parallele, il cui unico parametro che
cambia è la lunghezze iniziale. Queste molle sono mono direzionali: non
producono forze quando vengono accorciate al di sotto di l0. Si pensa che il
sistema possa regolare il valore di l0.

Il comando motorio varia la lunghezza l0 in l'0. Facendo ciò la forza passa dal
punto d'equilibrio A in cui si trovava il sistema inizialmente al punto B e poi
si stabilizza al nuovo punto d'equilibrio C.

## Slide 7

Si possono simulare le azioni singole dei muscoli isolati.

Considerato che le molle siano alla loro lunghezza di riposo tranne il muscolo iliaco.

- Iliaco: flessore dell'anca. La trazione produce la flessione dell'anca.
	L'azione vera però è più complessa perchè quando si compie un certo movimento
	dobbiamo tener conto dell'accoppiamento dinamico. Questo fa si che un
	segmento si muova in una certa direzione, trasmetta forze attraverso le
	articolazioni al segmento prossimale, il quale si muove e poi trasmette.
	Teoricamente quindi il movimento di un segmento corporeo comporta il
	movimento di tutti gli altri. Questo è un fenomeno che non è visibile
	normalmente in soggetti umani.
	
Coi modelli dinamici possiamo simulare queste situazioni e comprendere cose
interessanti.

In questa slide è rappresentato l'accoppiamento dinamico. La flessione del
ginocchio avviene perchè lo spostamento in avanti della coscia fa si che, per
inerzia, la gamba rimanga nella sua posizione.

Se analizzassimo la situazione dal punto di vista di un muscolo che non è
monoarticolare come l'iliaco ma biarticolare, il retto femorale. è un flessore
dell'anca ed estensore del ginocchio. In questo caso la sua azione non solo
flette l'anca ma estende il ginocchio. Coscia e gamba rimangono in linea tra di
loro. In questo caso l'anteversione del bacino e del dorso è maggiore rispetto
al caso della sola contrazione dell'iliaco. è necessario precisare che,
affinchè si possa realizzare il confronto tra queste due situazioni, è
necessario creare delle condizioni comuni: l'energia utilizzata dai due muscoli
per compiere lo spostamento dev'essere la medesima. A parità del lavoro
prodotto dai due muscoli abbiamo quindi queste situazioni. Questo lo si può
spiegare perchè il movimento della flessione dell'anca comporta una rotazione
congiunta di coscia, gamba e piede nel caso della contrazione del retto
femorale. Nell'altro caso invece la gamba poteva rimanere in basso. L'inerzia
(e il momento d'inerzia di massa), nel caso della contrazione dell'iliaco, è
minore rispetto all'altra situazione.

## Slide 8
## Slide 9

Quantificando analiticamente i momenti d'inerzia.

### Nel caso del ginocchio fisso

Consideriamo i 3 contributi. Il momento
	d'inerzia di massa è dato da 3 termini:
	- Jh: momento d'inerzia di massa totale attorno all'articolazione dell'anca.
	- Jtg: momento d'inerzia di massa della coscia rispetto al proprio
		baricentro.
	- $m_{T}d_{TH}^2$: momento d'inerzia della massa della coscia come se fosse
		concentrata nel baricentro della coscia per la distanza al quadrato del
		baricentro della coscia rispetto al punto di rotazione (anca).
	- Jsg: momento d'inerzia di massa della gamba (i.e. in eng. shank) rispetto
		al proprio baricentro.
	- $m_{S}d_{SH}^2$: momento d'inerzia della massa della gamba rispetto al
		centro dell'anca.
	- Jfg: momento d'inerzia di massa baricentrale del piede
	- $m_{F}d_{FH}^2$: momento d'inerzia della massa del piede rispetto al centro
		dell'anca.

Il momento d'inerzia di massa totale è la somma di questi tre contributi. 

Il momento d'inerzia alla rotazione è dato dal prodotto del momento d'inerzia
di massa totale e l'accelerazione angolare alla quale è sottoposta l'anca.

### Nel caso del ginocchio libero

Al ginocchio il momento è nullo, quindi non dobbiamo applicare più il momento
per far ruotare anche la gamba insieme alla coscia. Ci sarà dunque una forza
d'inerzia applicata al centro del ginocchio dovuta alla massa che sta sotto il
ginocchio e sarà dovuta alla somma della massa della gamba e della massa del
piede per l'accelerazione lineare del centro del ginocchio o, meglio, il
baricentro del sistema gamba piede, che passerà sicuramente per il centro del
ginocchio, dato che il momento al ginocchio è nullo.

Per cui con i muscoli dell'anca dovremo produrre un momento che sarà pari al
prodotto tra somma del momento d'inerzia di massa baricentrale della coscia e
del momento di trasporto (aka momento d'inerzia alla rotazione della sola
coscia) per l'accelerazione angolare della coscia, sommato al prodotto del
modulo della forza d'inerzia per la distanza tra centro del ginocchio e centro
dell'anca.

In conclusione è possibile verificare che il momento sviluppato nel caso di
ginocchio libero è nettamente inferiore rispetto a quello sviluppato dal retto
femorale.

Questo dimostra il fatto che un momento applicato alla coscia minore come nel
caso dell'iliaco darà luogo ad un momento di forza a livello del bacino (per
principio di azione e reazione) che sarà minore di quella del retto femorale.

## Slide 10

Effetti del muscolo semimembranoso. Questo muscolo sta dietro la coscia e si
attacca alla tuberosità ischiatica andando poi fino a sopra il ginocchio,
attaccandosi medialmente sulla tibia. La sua azione perciò estende l'anca e
flette il ginocchio. fa parte del gruppo degli ischio-crurali (crura: secondo
nome della tibia).

Quello che accade nella situazione di piede libero è che la coscia invece di
estendersi si flette al contrarsi di questo muscolo. è un comportamento
particolare in quanto il semimembranoso dovrbbe causare l'estensione della
coscia. Questo lo si spiega in termini di inerzia che si genera. La flessione
del ginocchio produce un avanzamento della gamba, che spinge davanti la coscia
producendone una flessione.

## Slide 11

Si mostrano le due condizione di piede libero e in condizione di piede
vincolato al terreno. In quest'ultimo caso è evidente l'estensione dell'anca e
una flessione del ginocchio.

## Slide 12

Se il muscolo produce una flessione del ginocchio, la gamba tende a salire e,
per effetto d'inerzia produce una forza diretta in avanti sul ginocchio stesso.
L'azione estensoria del muscolo è minore di questo effetto inerziale in quanto
il braccio di leva del muscolo a livello dell'anca è basso rispetto al braccio
di leva che ha la forza a livello del ginocchio.

Un altro effetto è la risalita del bacino nel lato in cui si muove l'arto. Si
ha un elevazione del bacino perchè il semimembranoso produce anche un'azione
adduttoria. L'adduzione riduce l'angolo tra bacino e coscia. il bacino può
scendere dal lato opposto finche non trova una reazione vincolare al terreno
sull'altro arto che impedisce l'ulteriore discesa.

## Slide 13

Possiamo notare anche i movimenti dovuti al soleo e al gastrocnemio.

## Slide 15

Come si può stimare l'effetto dell'azione di un singolo muscolo.

Consideriamo l'arto superiore e l'azione mediata da un singolo muscolo, il
brachiale anteriore. Qual è la forza che questo muscolo può trasmettere
all'estremità della mano. Dipende molto dalle condizioni al contorno.

Il muscolo sviluppa una forza in trazione, che si traduce in un momento
angolare considerando la struttura di sostegno. Il momento sarà uguale e
contrario al momento di una forza esterna che si oppone a questo movimento. Se
misurassimo la forza che viene prodotta contro una certa resistenza nel palmo
della mano, potremmo dire che la forza che produce il muscolo sarà data da un
equilibrio dei momenti: la forza muscolare sarà data quindi dal momento
sviluppato diviso la distanza tra il centro di rotazione e il punto
d'applicazione della forza. Con un certo momento muscolare sono però possibili
un'infinità di direzioni di forza applicata in quel punto (la componente
tangenziale è sempre la medesima) Tutte queste forze che hanno la stessa
componente tangenziale producono lo stesso momento. Quale forza considerare
dipende da un altro fattore. Cambia la componente radiale che va a trasmettersi
al centro articolare del ginoccio. Se c'è un appoggio del gomito ad un piano,
questa forza radiale va ad appoggiarsi a questo piano. A questo piano possono
realizzarsi reazioni di qualunque entità. Se la reazione è molto grande
significa che posso realizzare una forza molto inclinata, altrimenti
perpendicolare. L'inclinazione della forza f dipende, in situazioni
fisiologiche, dall'attività muscolare attorno all'articolazione gleno-omerale.
I flessori della spalla, uniti al brachiale anteriore, daranno luogo ad una
forza simile ad F1.

## Slide 16
## Slide 17

Analizziamo l'effetto delle singole contrazioni muscolari degli arti inferiori
in certe posizioni. Si studiano in certe condizioni. 

Nel caso in cui non si consideri la gravità. Il gruppo muscolare dei vasti (VA)
ha un azione estensoria dl ginocchio. la forza VA passa esattamente nel centro
articolare dell'anca. Questo ci fa capire che i VA hanno un'azione di
estensione sul ginocchio e non creano momento sull'anca. Esistono anche
muscoli, come il gluteo massimo, che invece hanno un'azione di estensione
dell'anca e non del ginocchio, quindi il vettore ha una direzione passante per
il centro del ginocchio.

Questa situazione non è quella che si riscontra in realtà perchè non si sta
considerando la gravità. Se infatti non si considerassero i contributi degli
altri muscoli, l'arto sollevato sarebbe soggetto alla forza riportata in rosso.
Tutte le direzioni dei vari contributi di forza dei muscoli vengono cambiati in
relazione a questo vettore rosso (somma vettoriale tra ogni vettore della
condizione senza gravità e il vettore rosso).

## Slide 19

Andiamo a studiare i muscoli dal punto di vista anatomico, studiandone la
struttura.

Il muscolo è formato da fibre muscolari, fibre e fibrille. Un muscolo presenta
una membrana esterna, detta aponeurosi che contiene un interstizio costituito
da tessuto grasso e fibroso che separa i vari fascicoli (perimisio). Questi
fascicoli all'intero presenta le varie fibre muscolari separate tra di loro
dall'endomisio. Ciascuna fibra è rivestita da una membrana chiamata sarcolemma
e contiene fibrille. Nei muscoli vi sono delle parti in cui scorrono vasi
sanguigni e all'interno dei fascicoli vi sono solitamente dei capillari.

Le fibrille sono strutture allungate simili tra di loro e ripetitive che sono
elementi fondamentali della contrazione disposte in senso parallelo. Queste
producono forze d'accorciamento. La sezione totale del muscolo contiene
parecchie migliaia di fibrille, perciò la forza sviluppata è prodotta dalla
somma di queste fibrille in parallelo. Queste fibrille hanno una struttura
ripetitiva ciclica disposta in serie. Si individuano piccoli tratti chiamati
sarcomeri. Vi sono striature trasversali della fibrilla che delimitano le
strutture in serie. La striatura della fibra da il nome a questi muscoli:
muscoli striati. Muscoli scheletrici volontari striati.

## Slide 20

Nel muscolo abbiamo un insieme di elementi in serie, i sarcomeri, che
costituiscono fibre. Le fibrille non sono ben separate tra loro mentre le fibre
sono separate dal sarcolemma e si possono ben distinguere.

è possibile ragionare in due modi considerando il muscolo: si considerano
elementi in serie ed in parallelo.

- Considerando le fibre in parallelo, risulta evidente che la forza risultante
	del muscolo è la somma delle forze di tutte le fibre.
- Considerando gli elementi in serie, avremo che ogni sarcomero sarà in grado
	di produrre la stessa forza. La variabile è la lunghezza della fibra, che è
	pari alla somma delle lunghezze.

## Slide 21

Il sarcomero è limitato dalle due strisce nere (dette strisce zeta, ma anche
dischi zeta) visibili in figura. All'interno si trovano diverse strutture, una
fascia centrale con delle fibre longitudinali costituiti da filamenti di
miosina. Questa è una proteina con forma allungata. Tanti filamenti di miosina
formano questa zona centrale. La lunghezza della zona centrale è all'incirca di
1.65µm. Alle due estremità, rispetto alla zona centrale, si trovano altre due
zone, che contengono filamenti più sottili e organizzati longitudinalmente.
Sono costituiti da actina della lunghezza di circa 1µm. Dall'osservazione sono
presenti più bande, una isotropa chiamata banda H e delle bande anisotrope
laterali (AI). Questi filamenti di actina sono vincolati ai dischi zeta, si
prolungano verso il centro del sarcomero, terminando al limite della regione
anisotropa. I filamenti di actina si sovrappongono con quelli di miosina. Nella
zona anisotropa si possono reare dei legami tra miosina e actina. Mentre le
lunghezze dei filamenti rimangono costanti, la porzione relativa alla parte
anisotropa può variare nella lunghezza a seconda che la fibra si stia
accorciando oppure allungando.

## Slide 22

Il filamento di miosina è modellizzabile come da figura C. ha una simmetria
centrale. Dalla porzione cilindrica fuoriescono delle propaggini dette teste di
miosina. Le teste sono direzionate rispetto al centro della fibra. Il filamento
di miosina è quindi formato da un fascio di queste molecole aventi la struttura
riportata in D. Una coda, meromiosina leggera, e una testa, meromiosina
pesante. Queste sono collegate da un collo e fulcro. La testa della miosina è
bilobata (i.e. doppia), articolata nella zona del collo. Le teste sono disposte
con una geometria ordinata.

La disposizione delle fibre è la seguente: ogni filamento di miosina ha sei
filamenti di miosina accanto. Ogni filamento di actina è condiviso da tre
molecole di miosina.

Le teste di miosina può agganciarsi con l'actina, creando il ponte
actina-miosina.

## Slide 23

I filamenti con le teste sporgenti legano in un punto particolare della
molecola di actina, detto sito attivo dell'actina. Quando si aggancia la testa
rimane in quel sito per un po' di tempo ed è in grado di generare delle forze.
L'aggancio avviene quando lo ione calcio subentra nella membrana del muscolo,
andando a modulare la presenza della troponina I che migra dal sito in cui la
molecola di miosina legherà. Creato il legame, interviene un meccanismo attivo:
l'idrolisi di ATP e il legame a livello del collo della testa di miosina induce
la deformazione del collo che tende a far ruotare all'indietro la testa
rispetto al suo asse principale, spostando all'indietro l'actina. questo è il
meccanismo secondo cui avviene la contrazione concentrica.

## Slide 24

Quando il filamento di actina non riesce a muoversi nella direzione in cui
viene trascinato dalla testa di miosina, man mano che il filamento si sposta,
con l'aumento di velocità di spostamento diminuisce la forza. L'accorciamento
del muscolo avrà un effetto di riduzione della forza prodotta. Quando il
filamento si accorcia alla velocità in cui la testa si ripiega, la forza
sviluppata è nulla.

Se il filamento di actina viene mantenuto in posizione, la forza si può
sviluppare e raggiunge un certo valore che è tipico del muscolo.

Quando il filamento di actina viene trascinato in avanti dalla forza esterna il
collo della molecola di miosina è messo in trazione e deve allungarsi perchè
deve rimanere agganciato per un certo periodo di tempo in cui però avviene una
deformazione in trazione con aumento di forza all'interazione tra actina e
miosina. Lo spostamento in allungamento comporta un aumento di forza per
trazione elastica.

Questo grafico quindi mostra la forza di contrazione in relazione alla velocità
di accorciamento.

## Slide 25

La capacità di generare forza dipende anche dalla lunghezza del muscolo. Il
numero di ponti actina-miosina dipendono da quanta è ampia la zona di
sovrapposizione tra actina e miosina. Il sarcomero. Nel momento in cui tutte le
teste si trovano in zona anisotropa, il muscolo può sprigionare la massima
forza, quando il muscolo si allunga i due dischi z si allontanano e i filamenti
di actina scivolano all'esterno rispetto ai filamenti di miosina. Le zone di
sovrapposizione si riducono notevolmente, fino a quando, ad una certa
lunghezza, non esiste la zona di contatto, quindi non possono essere generate
forze. 

Quando il muscolo si accorcia molto, se i due dischi z sono molto vicini
potrebbe esserci interferenza tra i due filamenti di actina, oltre al fatto che
le molecole di miosina non possono essere compresse. Queste due resistenze
interne vanno a diminuire la forza sviluppata dal sarcomero.

Possiamo andare a notare dal grafico che il sarcomero non ha una lunghezza
predefinita ma varia tra 1.27 e 3.65µm. Se noi accorciamo il muscolo a partire
dall'estremo massimo, cresce progressivamente e proporzionalmente la forza che
può sviluppare il sarcomero (cresce proporzionalmente il numero di ponti
actina-miosina che si generano). Quando arriviamo a 2.25µm siamo alla
condizione in cui tutti i ponti possono essere creati e anche piccole
variazioni di lunghezza non modificano la forza prodotta. Al di sotto dei 2µm
si sentono le interferenze dei filamenti di actina, diminuzione lineare della
forza. Quando anche i filamenti di miosina si oppone, la forza decresce
bruscamente.

La cosa importante di questo grafico è che si può scalare l'asse delle ascisse
sulla base di quanti sarcomeri stanno componendo la fibra muscolare. Per
esempio se ho 1000 sarcomeri, basti moltiplicare per 1000 i valori sull'asse
delle ascisse. Questo considerando i sarcomeri in serie

Considerando le fibre in parallelo, sarà il valore della forza che sarà
moltiplicata per il numero di questi elementi in parallelo. Il valore della
forza prodotta sarà quindi moltiplicata per il numero di fibre in parallelo.

La morfologia della curva F-L, nel caso del muscolo reale, in realtà si
discosta dalla curva qui riportata, perchè vi sono elementi viscoelastici che
"smussano" la curva ai confini dei vari tratti.

## Slide 26

La fibra muscolare viene attivata da un comando motorio che riceve. Questo
comando motorio non comporta uno sviluppo di forza massima ogni volta ma, a
seconda del numero di fibre che lo ricevono, si sviluppa una forza più o meno
intensa. è necessario altresì ricordare che, oltre al comando motorio, lo
sviluppo della forza dipende da altre condizioni, tra cui le relazioni F-L e
F-V, la fatica etc.

Alla base del comando motorio si trova il potenziale d'azione (AP) che qui è
riportato. Questo è l'elemento che produce la forza nella fibra muscolare. è il
segnale che viene utilizzato nella comunicazione del sistema nervoso e
muscolare. Costituisce l'unità d'informazione che viene trasmessa tra neuroni.
La forma degli AP è simile tra loro e deriva dall'attività elettrochimica delle
cellule eccitabili.

Descrizione del potenziale d'azione.

Deriva da una variazione del potenziale elettrico all'interno della cellula
eccitabile. Per effetto di equilibrio elettrochimico, inizialmente, si trova
tale cellula ad un potenziale di circa -90mV (ricordando che il potenziale di
membrana è interno - esterno, quindi è negativo rispetto all'esterno). Questo
deriva dal fatto che, all'interno della cellula vi sono una gran quantità di
anioni di grosse dimensioni che vengono generate nella cellula per fenomeni
metabilici oppure passano all'interno della membrana e non possono più uscirvi.
Quando giungono dei potenziali d'azione alla cellula eccitabile che stiamo
considerando attraverso le sinapsi che li conducono a essa, a seconda del tipo
di sinapsi (eccitatorio o inibitorio) varierà il risultato dell'integrazione
spaziale. Nel caso un gran numero di contributi eccitatori venissero
considerati si verificherà una variazione positiva. Quando il potenziale
transmembrana supera un valore di soglia di circa -60mV, i canali
voltaggio-dipendenti del sodio si aprono bruscamente facendo entrare quantità
massicce di tale ione, portando il potenziale di membrana a valori tendenti a
+30mV. Al contempo si aprono anche dei canali di conduzione della membrana agli
ioni potassio che si trovano in una quantità maggiore all'interno della
membrana. Fuoriuscendo, gli ioni potassio abbassano il potenziale interno,
tendendo a riportarlo verso il valore di riposo. Di fatto la fuoriuscita dura
per un periodo più lungo di quello che ci si aspetta, per cui si ha
un'iperpolarizzazione e il potenziale di membrana tende ad assumere valori di
equilibrio per lo ione potassio.

## Slide 27

Il segnale arriva alla membrana della fibra muscolare con delle sinapsi. Avremo
quindi i motoneuroni nel midollo spinale che mandano i loro assoni alle fibre
muscolari. Ciascuna delle ramificazioni del neurone forma una sinapsi con una
fibra muscolare. Ogni fibra muscolare ha una ed una sola sinapsi. Questa zona
dove si trovano le sinapsi tra nervo e muscolo si chiama punto motore. In
questa zona del muscolo si trovano concentrate tutte le sinapsi eccitatorie. Quando arriva il potenziale d'azione (la cui durata è di circa 2-3 ms), si scatenano due fenomeni all'interno della fibra muscolare:

1. di natura elettrica: l'AP si propaga nelle due direzioni della fibra
	 muscolare (prima e dopo la giunzione neuromuscolare).
2. di natura meccanica: il passaggio degli ioni corrispondente al potenziale
	 d'azione fa sì che vi siano degli ioni di calcio che stanno all'interno del
	 reticolo sarcoplasmatico che penetra all'interno della fibra e produce una
	 deformazione dell'elica che si trova sopra il filamento di actina e che la
	 troponina vada dentro la molecola liberando un sito di questa molecola che
	 ora è in grado di accettare il collegamento con la testa di miosina. Se
	 questo fenomeno si ripete su tanti ponti si genera una forza che ha un
	 andamento crescente (contrazione o fase di generazione della forza),
	 raggiunge un massimo e poi avviene un rilasciamento. In seguito ad un
	 impulso la fibra genera la forza che cresce e decresce spontaneamente. Se
	 andiamo a vedere cosa accade dentro la fibra vedremo che l'ATP usata per
	 generare la contrazione (tramite l'idrolisi) dopo un po' viene seguita dalla
	 reazione di un'altra molecola di ATP che fa sganciare i filamenti di
	 actina-miosina. In assenza di quest'uso della molecola il muscolo rimarrebbe
	 in contrazione (situazione di rigor mortis).
	 
L'andamento crescente e decrescente della forza è quello che viene chiamato
anche risposta all'impulso nervoso. Assomiglia ad un impulso ed è di natura
meccanica. La curva prende il nome di twitch di forza. Il twitch è diverso a
seconda del tipo di fibre che si sta considerando. Quindi si possono descrivere
delle fibre lente (twitch di lunga durata) e delle fibre veloci (twitch di
durata molto minore).

Possiamo notare che non si genera forza continua. Vi è un ritardo meccanico tra
l'arrivo del potenziale d'azione e l'inizio della forza generata. Questa
dipende dai vari fenomeni fisici che dipendono dal tempo.

## Slide 28

Le tipologie di fibre muscolari e le proprietà meccaniche. Le fibre lente e
quelle veloci, distinte precedentemente sulla base della durata del twitch di
forza, possono essere classificate ulteriormente sulla base del meccanismo
metabolico da loro utilizzato. è importante studiare anche la capacità di
riprodurre il twitch in successione per tempi più o meno lunghi. Quando il
twitch viene generato avviene un consumo energetico: l'ATP idrolizza sia nella
fase di contrazione che in quella di rilasciamento e, man mano che si consuma
dev'essere ripristinata con determinati meccanismi. 

1. *Tipo I, SO (Slow Oxidative)*: Esistono fibre che hanno dei meccanismi di
	 ricostituzione dell'ATP sulla base di meccanismi aerobici, sfruttando
	 l'ossigeno attraverso l'apporto effettuato dall'emoglobina del sangue per
	 generare ATP. Queste sono dette fibre ossidative. Attraverso l'ossidazione
	 di sostanze interne producono energia. La condizione necessaria a queste
	 fibre è quella di essere particolarmente irrorate. Prendono il nome di fibre
	 rosse perchè contengono molta emoglobina e mitocondri. Queste fibre sono
	 lente e generano twitch di lunga durata. Sono in grado di riprodurre il
	 twitch per tempi lunghissimi perchè avendo un buon apporto di sangue possono
	 facilmente rigenerare le molecole di ATP. Hanno una gran resistenza
	 all'affaticamento.
2. *Tipo IIb, FG (Fast Glycholitic)**: Hanno un twitch breve e utilizzano un
	 meccanismo glicolitico. per rigenerare l'ATP utilizzano un meccanismo, la
	 glicolisi, che consuma glicogeno che si trova nelle fibre stesse. Queste
	 fibre non sono irrorate da vasi sanguigni, per cui generano energia in tempi
	 molto brevi ma esaurendo le risorse presenti in queste fibre (il glicogeno
	 viene apportato con tempi lunghi). Sono di tipo bianco perchè non sono
	 irrorate dal sangue e non possiedono un gran numero di mitocondri. Per tempi
	 lunghi la fibra veloce non è più in grado di produrre forza. Questo fenomeno
	 di incapacità di produrre forza è detto affaticamento.
3. *Tipo IIa, FOG (Fast Oxidative-Glycholitic)*: fibre intermedie che sfruttano
	 le migliori caratteristiche dei due precedenti tipi. Hanno un meccanismo
	 misto di rigenerazione dell'ATP: sia ossidativo che glicolitico. Hanno una
	 buona irrorazione, per cui le fibre che hanno questo tipo di meccanismo
	 possono mantenere la contrazione per tempi lunghi rispetto a quelle bianche.
	 Per tempi lunghi osserviamo che esiste un decadimento della forza ma avviene
	 più lentamente delle fibre affaticabili di tipo IIb.

I muscoli possiedono in diverse percentuali le tipologie di fibre riportate
qui. I muscoli posturali i quali dovranno mantenersi contratti per tempi molto
lunghi, hanno bisogno di avere una gran percentuali delle fibre di tipo I, in
quanto non affaticabili.

### Note

Come si genera una forza continua? ad una sequenza di AP si generano molti
twitch. Se un AP riesce a generare un twitch prima che il precedente abbia
completamente rilasciato il muscolo i due  sommano le loro forze temporalmente
(sommazione temporale). La forza, per effetto della stimolazione ad una certa
frequenza, tende a giungere ad un valore più o meno costante.

## Slide 29

La forza sviluppata dipende da molti fattori (F-l, F-v, tipo di fibra, etc).
Nella singola fibra possiamo sommare i twitch di forza (sommazione temporale)
fino ad arrivare ad una valore di forza sviluppata più o meno costante. Il
muscolo può modulare la forza prodotta utilizzando quel fenomeno della
sommazione temporale (variando la frequenza di invio dell'AP) sia variando il
numero di fibre muscolari reclutate. Aumentando il numero di fibre reclutate
aumenta la forza sviluppata (ricordando che queste fibre sono tutte in
parallelo). Questo secondo fattore è detto sommazione spaziale.

Si utilizza dei meccanismi sulla base dell'azione delle unità elementari di
contrazione (i.e. le unità motorie). Un'unità motoria corrisponde al minimo
insieme di fibre muscolari che possono essere attivate. Il motoneurone ha un
assone mielinizzato (rivestita da mielina, un isolante elettrico). I vari
manicotti sono separati tra di loro da fessure che costituiscono i nodi di
Ranvier. La propagazione del potenziale d'azione, dal punto in cui viene
generato (al collo dell'assone) si propaga nella fibra attraverso il fluido che
sta all'esterno dell'assone. Le zone elettricamente isolate non consentono il
passaggio di ioni. In altre parole: quando si genera un potenziale d'azione a
livello di un nodo di Ranvier, si genera anche un campo elettrico nell'intorno
di questo nodo che va a stimolare anche il nodo successivo. La propagazione del
segnale da un nodo ad un altro è detta saltatoria per questo motivo. Questo
permette una velocità di trasferimento dati maggiore (10m/s fino a 100m/s). Le
ramificazioni della fibra nervosa vanno ad incidere su diverse fibre muscolari
mediante sinapsi. Il potenziale d'azione che giunge a tutte le fibre muscolari
innervate dallo stesso neurone le farà contrarre più o meno contemporaneamente,
trascurando i piccoli ritardi dovuti alle differenti lunghezze delle
ramificazioni. Il twitch di forza risultante dalla somma spaziale darà luogo al
twitch dell'unità motoria. L'unità motoria è l'elemento basilare della
contrazione. Non è possibile, fisiologicamente, contrarre un numero di fibre
inferiori al numero di fibre innervate da un motoneurone. Il motoneurone, l'assone che lo collega alle fibre muscolari e tutte queste costituiscono, nel loro insieme, l'unità motoria.

A seconda del muscolo considerato si possono avere diversi numeri di fibre
muscolari collegati allo stesso assone. Se l'unità motoria ha un rapporto
d'innervazione grande (il numero di fibre muscolari per motoneurone è grande)
non possono graduare finemente la forza. Ciò invece è possibile se
considerassimo delle unità motorie con rapporto d'innervazione piccolo.

Tipicamente, i muscoli della mano hanno un rapporto d'innervazione basso. i
muscoli posturali e i fissatori hanno un numero di fibre muscolari di alcune
migliaia per ciascun motoneurone.

Ricordiamo che i meccanismi di modulazione della forza in un'unità motoria sono
tipicamente due: la sommazione temporale (lavorando sulla frequenza cui gli AP
giungono) e la sommazione spaziale (in dipendenza dal rapporto d'innervazione).

## Slide 30

La forza muscolare dipende anche dall'architettura muscolare, ossia la
disposizione delle fibre muscolari. Generalmente un muscolo scheletrico ha una
struttura costituita da un ventre muscolare collegato a due estremità ossee. La
parte più prossimale è detta origine e quella più distale è detta inserzione.

- *Fusiforme*: il muscolo emblematico. Ha un ventre muscolare allungato e due
	estremità tendinee.
- *Bicipite*: due origini. Il muscolo è diviso in due ventri muscolari con due
	differenti origini. Dall'altra parte hanno un tendine in comune.
- *Tricipiti*: tre capi tendinei da una parte.

I tendini, che si trovano alle estremità del ventre muscolare, sono degli elementi passivi e stanno in serie alle fibre muscolari. La forza dovrà essere trasmessa a questi elementi passivi prima di raggiungere le estremità ossee.

Esistono anche delle membrane di rivestimento, l'aponeurosi. All'interno dei
muscoli ci sono i fascicoli in cui vi sono ulteriori elementi passivi,
l'endomisio, il perimisio, etc. Questi elementi sono invece disposti in
parallelo rispetto alle fibre muscolari. Queste membrane, sia di rivestimento
interno in parallelo, sia le estremità in serie, hanno un effetto sulla forza
che tali muscoli riescono a trasmettere alle loro estremità.

## Slide 31

Iniziamo considerando le componenti in parallelo alle fibre.

Andando a visualizzare le forze che il muscolo produce alle diverse lunghezze a
cui si trova, in condizioni isometriche. Considerando il tratto di curva a
sinistra è possibile notare che, all'aumentare della lunghezza del muscolo,
aumenta la forza sviluppata (non vi sono né interferenze tra le porzioni di
miosina né tra quelle di actina. I ponti actina miosina che si riescono a
sviluppare raggiungono il loro massimo). Immaginando di stimolare tutti i
sarcomeri presenti in tutte le fibre muscolari appartenenti al muscolo che
stiamo considerando, il ragionamento appena compiuto si dimostra valido. Quindi
ci aspetteremmo di ritrovare la curva della forza prodotta dal sarcomero anche
nel caso dell'intero muscolo. Questo è vero solo in parte perchè, nel secondo
tratto della curva reale, vi è un aumento della forza. Questo è dovuto agli
elementi passivi che si trovano in parallelo rispetto alle fibre muscolari. Se
prendessimo il muscolo e, invece di considerarne la forza sviluppata dalle
fibre muscolari, lo considerassimo in situazione di allungamento passivo (senza
attivazione nervosa), osserveremmo che il muscolo si lascerebbe allungare e
dopo Ls genererebbe una forza di resistenza all'allungamento di natura passiva.
Questa cresce rapidamente dopo Ls. Se facessimo la differenza tra la curva
totale (contributo contrattile + passivo) e quella trovata nelle condizioni di
allungamento passivo otterremmo una curva simile a quella del diagramma
forza-lunghezza di un sarcomero. Questa è una curva a campana.

## Slide 32

Si può spiegare questo comportamento con il modello reologico di Hill. La prima
considerazione è quella che la curva reale sia da considerarsi la somma di due
contributi, quello contrattile e quello passivo. Questi si trovano in parallelo
(i.e. il parallelo realizza la somma delle forze). CE: contracting element.
In condizioni isometriche (i.e. statiche), questo modello funziona bene.

In condizioni dinamiche il modello sopra descritto non funziona. Basti
considerare la risposta al quick release per rendersene conto. Quick release:
si pone il muscolo ad una certa lunghezza, con un certo tipo di contrazione
prodotta artificialmente. Se si permetteva al muscolo di accorciarsi
bruscamente (quick release), si notava che la forza non diminuiva al valore
inferiore, come ci si sarebbe aspettati guardando la lunghezza della molla in
parallelo ma si abbassava molto di più. La forza diminuiva di molto per poi
risalire in un secondo momento verso un valore che corrispondeva alla seconda
lunghezza raggiunta. Questo fenomeno ha fatto pensare all'effetto di elementi
viscosi all'interno del muscolo. In effetti si è riscontrata la presenza di
fluidi sia intracellulari che interstiziali che hanno un effetto sulla forza
risultante in condizioni dinamiche. La forza viscosa dipende dalla velocità:
maggiore la variazione di lunghezza, maggiore la forza che si opporrà al
movimento. Le forze viscose poi si riducono man mano che la velocità
diminuisce. Si giunge quindi al secondo modello di Hill, più utilizzato. In
questo vediamo la presenza di un elemento visco(i.e. Ba)-elastico(i.e. Ks) che
costituisce l'elemento che tiene conto delle variazioni dinamiche (variazioni
di lunghezza del muscolo, quindi non in condizioni isometriche). L'elemento
contrattile non è connesso direttamente all'estremo di destra di Ks ma allo
smorzatore, così da modellizzare i tempi di latenza della trasmissione della
forza. Quindi qui non si sta solo considerando la componente parallela del
muscolo (kp) ma anche la componente in serie (ks) rispetto alla componente
contrattile.

## Slide 33

Lo studio di questo modello si basa su un equilibrio di forze.

## Slide 34

Il modello esprime che, quando ho un allungamento brusco di X1, avrò una forza
che aumenta drasticamente al tempo t0, per poi stabilizzarsi al valore pari
alla costante elastica parallela per l'allungamento. Il punto x2 non può
variare istantaneamente per via dello smorzatore, che farà crescere
esponenzialmente il valore di X2.

Nel caso in cui si avesse un accorciamento su X1, la forza avrebbe un calo
drastico al tempo iniziale per poi avere una salita esponenziale per
raggiungere l'asintoto orizzontale. L'andamento di X2 è sempre esponenziale.

## Slide 35
## Slide 36

Come venivano realizzati questi esperimenti negli anni trenta. La strumentazione era molto rudimentale ma ha dato luogo a risultati notevoli.

Sulla sinistra: strumento che serve ad osservare cosa succedeva andando a
variare la forza applicata al muscolo. Il muscolo veniva appeso ad un elemento
fisso e all'estremità opposta veniva aggiunto un peso collegato ad una leva
(quello indicato con q). Il muscolo veniva messo in contrazione tramite
stimolazione elettrica e, una volta raggiunto il valore di forza, si lasciava
il muscolo libero di accorciarsi togliendo il nottolino (i.e. blocco) sulla
destra (quello indicato con n). Si vedeva che la velocità con cui si accorciava
il muscolo dipendeva dalla forza resistente, ossia da dove si trovava il peso
q. Spostando il braccio di leva del peso q si poteva aumentare o diminuire
l'effetto di quel peso sul muscolo (più si allontana il peso, spostandolo verso
il fulcro della leva sulla sinistra, e minore è il contributo della forza peso
di q sul muscolo). Quindi più si spostava verso destra e più il muscolo si
contraeva velocemente. Quanto fu scoperto con questo esperimento è in accordo
con la teoria perchè, applicando una forza resistente molto bassa, consentiamo
al sarcomero di raggiungere una velocità di contrazione vicina al massimo,
ossia pari alla velocità di ripiegamento dei ponti actina-miosina. La velocità
del muscolo veniva calcolata dallo spostamento di un ago sulla carta disposta
sulla sinistra.

Un raffinamento ulteriore è stato fatto per controllare la velocità di
accorciamento del muscolo. Il muscolo è posto in trazione con un elemento in
grado di regolare la velocità d'accorciamento. In base alla velocità che veniva
consentita, la forza che era regolata da una molla rotazionale elastica
all'estremità, faceva variare la posizione di una leva di più o di meno a
seconda della forza generata. Con velocità elevate di accorciamento la leva
rimaneva ferma. Se la velocità era minore, la forza sviluppata cresceva e si
vedeva una deflessione di quest'asse.

Alla fine di questi anni, Hill definì una relazione tra F e v di accorciamento
del muscolo che era un'iperbole.

L'iperbole da lui definita è questa:

$(P+a)(V+b)=(P_{0}+a)b$

in cui P è la forza e V è la velocità d'accorciamento.
$P_{0}$ è la forza isometrica.

L'espressione, che esprime che il prodotto tra forza e velocità è costante, è
modificata dai coefficienti a e b. Quest'iperbole ha degli asintoti che stanno
nei quadri negativi del piano. L'intersezione con l'asse delle ascisse fornisce
il punto avente velocità massima. L'altro valore sull'asse y è la forza
isometrica, ossia a velocità nulla. Questo grafico rappresenta bene l'andamento
F-v del muscolo e può essere analizzata dando luogo a diverse considerazioni.

## Slide 37

La normalizzazione dei valori rende possibile avere una curva risultante avente
asintoti equidistanziati dall'origine.  Riferiamo i valori di forza e velocità
ai valori indicativi di questa curva, i valori $P_0$ e $V_0$. Dividendo per
quei valori otteniamo una relazione del tipo riportato. Facendo alcuni passaggi
algebrici ci troviamo che l'ipebole passa per i punti indicati con 1 nel
grafico. Dai calcoli effettuati risulta che i due asintoti sono discostati
dall'origine con una distanza uguale ($\frac{a}{P_0}$). Ci si riferisce in
particolare all'espressione della velocità massima $V_{0}$ in termini di forza,
da cui si ricava che $\frac{b}{V_0}=\frac{a}{P_0}$.

## Slide 38

La formulazione di Abbott e Wilkie della forza sviluppata in funzione di forza
e velocità considera, come equazione di partenza, quella di Hill. Questa viene
modificata in modo che $P_{0}$ diventi la forza generabile dal muscolo ad una
certa lunghezza. Così facendo è possibile ottenere la forza sviluppata come
funzione in due variabili, la velocità di contrazione e la lunghezza del
muscolo. Il risultato di Abbott e Wilkie, però, non considera che la velocità
di contrazione si mantiene costante per una lunghezza muscolare nel range
compreso tra 1.65 e 2.7µm.

Per tener conto di questo nella descrizione del comportamento muscolare è
necessario moltiplicare il risultato precedente per un fattore $f(L)$ che varia
da 0 a 1 e corrisponde alla forza normalizzata in accordo con l'andamento F-l.

Possiamo quindi identificare tre differenti piani:

1. F-l: è possibile identificare la tipica forma a campana del sarcomero e,
	 pertanto, della fibra muscolare.
2. F-v: è possibile identificare le tipiche iperboli, come ipotizzato da Hill.
3. v-l: l'andamento è rettilineo se compreso nel range sopra identificato.

## Slide 39
## Slide 40
## Slide 41
## Slide 42
## Slide 43
## Slide 44
