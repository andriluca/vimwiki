# Bioingegneria del Sistema Motorio

| Lezione                   | Argomento                                                   | Giorno | Link                                                                                                                             |
| :---                      | :---                                                        | :---   | :---                                                                                                                             |
| [01](Frigo/01/Lezione.md) | Dinamica e sinergie                                         | 15.09  | https://politecnicomilano.webex.com/recordingservice/sites/politecnicomilano/recording/playback/e6869083d1894a23a63da391fa9c8f80 |
| [02](Frigo/02/Lezione.md) | Controllo motorio e struttura scheletrica                   | 18.09  | https://politecnicomilano.webex.com/webappng/sites/politecnicomilano/recording/playback/286f358d6f8f4b77b49d10f1a75b7bf2         |
| [03](Frigo/03/Lezione.md) | Struttura scheletrica                                       | 22.09  | https://politecnicomilano.webex.com/recordingservice/sites/politecnicomilano/recording/playback/5d920d3a771b406e833fb0c6cef45de0 |
| [04]()                    | Il sistema motore                                           | 22.09  | https://politecnicomilano.webex.com/recordingservice/sites/politecnicomilano/recording/playback/8ff3c4cf87504517b00e38a920029e72 |
| [05]()                    | Il sistema motore                                           | 25.09  | https://politecnicomilano.webex.com/webappng/sites/politecnicomilano/recording/playback/f0a864b7266049a1ba422b616ce6574a         |
| [06]()                    | Energetica                                                  | 29.09  | https://politecnicomilano.webex.com/webappng/sites/politecnicomilano/recording/playback/12a9c0e6ebba419a9d676745a57957df         |
| [07]()                    | Biomeccanica del muscolo scheletrico                        | 06.10  | https://politecnicomilano.webex.com/recordingservice/sites/politecnicomilano/recording/playback/0d761ff7c31f42769f499131f7ece48a |
| [08]()                    | Energetica sistema di controllo                             | 09.10  | https://politecnicomilano.webex.com/webappng/sites/politecnicomilano/recording/playback/5b06a2f13fc74d4da7ed5904d83af11e         |
| [09]()                    | Sistema di controllo e modello lambda                       | 13.10  | https://politecnicomilano.webex.com/webappng/sites/politecnicomilano/recording/playback/d9fd7d0c8fbe420cb0c2bb58d06176c4         |
| [10]()                    | Equilibri statici e dinamici                                | 16.10  | https://politecnicomilano.webex.com/recordingservice/sites/politecnicomilano/recording/playback/8fe226d3673f45debb9dde4f96ae8e68 |
| [11]()                    | Biomeccanica del movimento                                  | 20.10  | https://politecnicomilano.webex.com/webappng/sites/politecnicomilano/recording/playback/74a5c1fd671340a1a91e8664c6f8879f         |
| [12]()                    | Problema dinamico inverso                                   | 27.10  | https://politecnicomilano.webex.com/webappng/sites/politecnicomilano/recording/playback/bcab032629dc4f4cace25b041f7e5519         |
| [13]()                    | Laboratori analisi del movimento, piattaforma dinamometrica | 30.10  | https://politecnicomilano.webex.com/recordingservice/sites/politecnicomilano/recording/playback/547443946dce46688f2b74857d46e6de |
| [14]()                    | Seminario controllo motorio                                 | 03.11  | https://politecnicomilano.webex.com/recordingservice/sites/politecnicomilano/recording/play/533c060e36704d00ab8ef6336cb82346     |
| [15]()                    | Cinematica del corpo rigido                                 | 10.11  | https://politecnicomilano.webex.com/webappng/sites/politecnicomilano/recording/playback/d68a5405c3684c7a9ddd94f596840e53         |
| [16]()                    | ???                                                         | 20.11  | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=c23ec8dc3fef4f6a85217ba70036a11f                              |
| [17]()                    | Postura e modello del pendolo inverso                       | 17.11  | https://politecnicomilano.webex.com/webappng/sites/politecnicomilano/recording/playback/69d284e95c784c3ea84ebe0bac722936         |
| [18]()                    | Sistemi di riferimento anatomici                            | 20.11  | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=189edee8a70e4d8a9716fee8a3fc9093                              |
| [19]()                    | Seminario controllo posturale                               | 24.11  | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=604cac137a7846dda030664d024290f0                              |
| [20]()                    | Fenomenologia della locomozione                             | 01.12  | https://politecnicomilano.webex.com/webappng/sites/politecnicomilano/recording/playback/e5946f53783b4aa99e41ca97f894396a         |
| [21]()                    | Dinamica inversa                                            | 04.12  | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=b59f97fe9d5242e19721809296e962a2                              |
| [22]()                    | ???                                                         | 11.12  | https://politecnicomilano.webex.com/webappng/sites/politecnicomilano/recording/playback/5b565b61655e4a1f877a15893f2bcea1         |
| [23]()                    | Ultima lezione                                              | 18.12  | https://politecnicomilano.webex.com/webappng/sites/politecnicomilano/recording/playback/7a92ffb8b84b44059dad18327c9bac59         |


## Modalità d'esame

Prova scritta a risposta multipla ed esercizi quantitativi (l'esame sarà
online, nel caso). Chi non è soddisfatto dalla valutazione potrà effettuare un
esame orale.
