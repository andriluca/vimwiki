# Review of control strategies for robotic movement training after neurologic injuries. Marchal-Crespo (2009)

## Introduction

There's an increasing interest in using robotic devices to help provide
rehabilitation therapy following neurologic injuries such as stroke or SCI
(Spinal Cord Injury).

There are two kind of paradigms when it comes to robotic devices:

1. The general one: *physical interaction* between robot and patient's limbs in
	 order to perform the training session.
2. *Coaching the participant*: there isn't a physical interaction with the
	 patient but the robot is motivating the participant to perform the motor
	 task.

New works concentrate their effort into developing more sophisticated solutions
(e.g. with more degrees of freedom) to support more complex movements (walking,
etc). Another ambition is device portability so to make its usage possible in
everyday life.

This paper is mainly about the different robotic therapy control algorithms
that can be implemented in the interaction between robot and human.

The goal of control algorithms is to control robotic devices for rehabilitation
exercises, so that the selected exercises to be performed by the participant
provoke motor plasticity and, therefore, improve motor recovery. Unfortunately
there aren't scientific evidences on how to achieve this goal, yet.

These algorithms are designed ad hoc, basing on the motor learning,
rehabilitation, neuroscience literature.

The following control algorithms classification, based on the strategy
(Human-Robot Interaction, HRI) they use to provoke motor plasticity, is
organizing the review of this paper:

1. **Assistive**: the most developed paradigm. It *helps* the participant to
	 move their weakened limbs. It uses a strategy similar to *active assist*
	 exercises performed by physiotherapists.
2. **Challenge-based**: refers to controller doing the opposite of assistive
	 ones. They make movement tasks more difficult or challenging
3. **Haptic simulation**: it refers to the practice of activities of daily
	 living (aka ADL) movements performed in virtual environment. They are very
	 flexible and they constitute a safe environment for patients.
4. **Coaching**: helping to direct the therapy program, to motivate the
	 participant and promote motor learning.

Clearly, motor training is personalized, so the strategies are not independent
from one another, thus they can even be used in combination. Assistance and
challenge can be interpreted as one the opposite to the other.

The goal of this paper is to review "high level" control algorithms: the
aspects of the control algorithm that are explicitly provoking motor
plasticity. For many robots, these high-level algorithms are supported by low
level controllers that achieve force, position, impedence or admittance control
necessary to implement high-level algorithm.

## Assistive controllers

Active assist exercise is the *primary* control paradigm that has been explored
in robotic therapy development, so there's plenty of reviews devoted to this
topic. Physiotherapists manually implement this technique in clinical
rehabiliation on regular basis.

Active assist robots can be classified in this way:

1. Impedance-based assistance controller.
2. Counterbalance-based assistance controller.
3. EMG-based assistance controller.
4. Performance-based adaptive assistance controller.

### Rationales for assistive controllers (pros)

There are many rationales for active assist exercises but none is extensively
verified in scientific studies.

1. Alternation between muscular effort produced by the patient (in order to
	 provoke motor plasticity) and stretching of muscles and connective tissues
	 (in order to help preventing stiffness of soft tissues).
2. It provides novel somatosensory stimulation that helps inducing brain
	 plasticity.
3. Physically demonstrating the desired pattern to the subject may help the
	 patient learn to achieve the pattern.
4. Creating a normative (standard) pattern in sensory input will help
	 reestablishing a normative pattern of motor output.
5. Movement assistance allows participant to perform more movements in a
	 shorter amount of time, increasing the intensity of the practice.
6. It's possible to modulate motor task intensity, allowing the participant to
	 practice in safe conditions.
7. Active assistance may have a psychological benefit for participants that can
	 witness their success. Therefore, this will help motivating repetitive and
	 intensive practice by reconnecting intention to action.

The points going from two to four are put together: controller, repeating trial
after trial, helps having a re-activation of sensori-motor loop by giving at
least the sensory feedback to the brain.

### Cons for assistive controllers

There are mainly two cons:

1. Physically guiding a movement may decrease motor learning for some tasks.
	 This is also known as "**guidance hypothesis**". This happens because *a
	 physically-assisted movement changes the dynamics of the task* so that the
	 *learnt task is different from the target task*. Moreover, movement
	 assistance reduces the burden on the learner's motor system to discover the
	 principles necessary to perform the task successfully.
2. In some cases, movement guidance causes subjects to decrease physical effort
	 during the traning. This is also known as "**slacking hypothesis**": *a
	 robotic device could potentially reduce recovery if it encourages slacking*
	 (i.e. decrease in motor output, effort, energy consumption and/or attention
	 during training).

#### Solution: assistance-as-needed

Because providing too much assistance may have negative consequences for
learning, a commonly stated goal in active assist exercise is to provide
"*assistance-as-needed*". This means assisting the participant only as much as
is needed to accomplish the task. 

An example of strategy: encouraging the patients effort and self-initiated
movements include allowing [in the control strategy] some error variability
around the desired movement using a *deadband* (e.g. area around the desired
trajectory in which the assistance is not provided), triggering assistance only
when the paricipant achieves a threshold (in terms of limb force or velocity).

### Impedance-based assistance

The first assistive robot therapy controllers proposed were proportional
feedback controllers. More recent controllers use more sophisticated forms of
mechanical impedance (i.e. viscous force fields) with respect to stiffness.

Generally, assistive control strategies are based on a common idea: when the
participant moves along the desired trajectory, the robot should not intervene
but when he/she deviates from the desired trajectory, the robot should create a
restoring force (generated using an appropriately designed mechanical
impedance). Assistive strategy controllers provide a form of
assistance-as-needed.

Let's make an example: a proportional plus derivative feedback position
controller. Whenever the participant moves away from the desired trajectory,
the robot force output increases proportionally, acting like a damped spring. A
deadband is introduced into impedance-based control scheme to allow normal
variability without causing the robot to increase its assistance force.

Impedance-based assistance algorithms have been implemented:

- in space: through virtual channels that guides limb movement, or a region of
	acceptable pelvic motions during walking.
- in time and space: through virtual channels with a moving wall (something
	that follows and assist the evolution of the trajectory in time).

#### Triggered assistance

This is a variant of impedance-based assistance. This control strategy allows
the participant to attempt a movement without robot guidance, but initiates
some form of impedance-based assistance after some performance variable (i.e.
elapsed time, generated force, spacial tracking error, limb velocity or
muscular activity) reaches a threshold.

A danger of using the trigger-assisted controllers is that a participant
produces the trigger conditions and then "rides" the robot, remaining
ostensible passive for the rest of the movement.

### Counterbalance controllers

Another strategy that has been developed is providing a weight counterbalance
to a limb. Example of weight counterbalacing may be found in motor training
literature both for upper and lower limbs (i.e. overhead slings, harnesses for
supporting body weight).

It's possible to exploit this strategy with two different approaches:

1. **Passive**: an example of this is Therapy-WREX, based on the mobile arm
	 support WREX, uses four-bar linkages and elastic bands to passively
	 counterbalance the weight of the arm, promoting performance of reaching and
	 drawing movementrs through a wide workspace. The applied assistance,
	 measured as a percentage with respect to the limb weight counterbalanced, is
	 selected by the clinician by adding or removing elastic bands.
2. **Active**: robot controllers are setup, via software, in order to support
	 the limb weight to meet participant's needs. In fact this kind of devices
	 can also take into account for different forces than weight that could
	 restrain the motor abilities of the participant. Some of therecent devices
	 provide some of the counterbalance mechanically because of two important
	 reasons: a power shutoff is not ending in a robot free fall; actuators range
	 of force is extended.

Both active and passive approaches can be tuned during training in order to

optimize the patient's performance.

### EMG-based controllers

Surface ElectroMyoGraphy signals (sEMG) can be used to drive the assistance in
a triggered (binary) or proportional way.

- *Triggered* controller: EMG is collected from target muscles, properly
	pre-processed and used in order to trigger the assistance whenever it reaches
	a fixed threshold.
- *Proportional myoelectric* controller: participants control their own
	movements, which is firstly decided by him/herself, while robots compensate
	for weakness generating a force which is proportional to the EMG signal
	needed to drive that movement.

There are limitations to this control strategy:

- Parameters need to be *calibrated* for each participant and at each session.
	These parameters consider *skin properties*.
- *EMG is sensitive* to: interference from neighboring muscles signals,
	electrode placement, overall neurologic condition of the participant.
- This strategy works in an *open loop*: if the participant creates an
	abnormal, uncoordinated muscle activation pattern, the robot could move in an
	undersired way.

### Peformance-based controllers

The control algorithms that were review till now were *static* in the sense
that they don't adapt controller parameters based on online measurement of the
participant's performance. 

Adapting control parameters have the potential advantage that the assistance
can be *tuned* to the participant's individual changing needs, both throughout
the movement and over the course of rehabilitation.

Several adapting strategies have the form:

`$P_{i+1}=fP_{i}-ge_i$`

Where:

- P: control parameter that is adapted.
- i: the i-th movement.
- e: performance error or measure of the participant ability to initiate
	movement or to reach the target.
- f: forgetting factor.
- g: gain factor.

Such algorithm is altered to adjust impedance as follows:

`$G_{i+1}=fG_{i}-ge_i$`

Where:

- G: robot impedance.

When applied at many samples of the step trajectory during walking, it was
found to cause these impedances to converge to unique, low values that assisted
the participants with SCI in stepping effectively.

#### Forgetting factor

Let's discuss about *forgetting factor* f. It is introduced in order to address
the possible problem of participant slacking in response to assistance. When
excluding this factor (`$f=1$`), if the performance error is zero, the
algorithm holds the control parameter constant and the participant is not
further challenged. If `$0<f<1$` and the performance error is low, then the
algorithm reduces the control parameter, with the effect of always challenging
the participant.

It's interesting to note that the human motor system apparently incorporates
the forgetting factor into an error-based system learning law as it adapts to
novel dynamic environments in order to minimize its own effort.

In a patient-cooperative framework the robot takes into account for the
participant's intention rather than imposing an inflexible control strategy.
When dealing in this framework, adaptive controllers are built so that
mechanical impedance (or stiffness) is increasing when participant is not
putting too much effort (because of slacking perhaps), while it's decreasing
when he/she's putting effort into it.

The need of adaptive controller becomes important when the goal is to provide
mechanically **compliant assistance for movement**. *Compliant robots*,
differently from stiff robot, must calculate an apropriate amount of force that
needs to be applied to cancel the effects of increased tone, weakness or lack
of coordinated control by the participant (all of which are varying widely
between participants, suggesting use of adaptive or learning-based principles).

## Determining the desired trajectory

Implementing many of the control strategies, particularly assistance and some
of the challenge based, often requires to *specify a desired trajectory*. There
are different ways in order to determine the desired trajectory:

1. Modeling the trajectory *based on normative movements* (mathematical models
	 of normative trajectories, such as minimum-jerk trajectory, pre-recorded
	 trajectories from unimpaired volunteers, pre-recorded trajectories during
	 therapist-guided assistance.) This is the most common approach.
2. When dealing with *bilateral tasks*, determining the desired trajectory is
	 to *base it on the movement of the unimpaired limb*, in a mirror-like
	 fashion.  These strategies might have also a **neurologic benefit**, related
	 to the neurology of bilateral control in both upper extremities and lower
	 extremities.
3. *Adaptive approaches*:
	- based on the contact forces between robot and participant.
	- re-planning the minimum-jerk desired trajectory at every time sample based
		on the actual performance of the participant.
	- adapting replay-timing at every time sample based on the difference between
		the measured state of the participant and the desired state.

There's an equivalence between determining the desired trajectory for a robotic
therapy and predicting the human behavior for a given task: identifying a model
of human motor behavior. Depending on the task complexity, identifying a model
of motor behavior can be more or less complicated. Basically specific tasks are
associated to specific normative motor behavior.

Some robotic therapy controllers *do not require* desired trajectories.
Resistive strategies and EMG-proportional controllers do not require desired
trajectories. 

The use of BCI (Brain-Computer Interfaces) to specify a desired trajectory or
even robot forces, may allow greater participant control over the movement to
be performed.

## Challenge-based controllers

They are also known as resistive controllers, as we will analyze. Their
approach is complementary with respect to the assistive controllers: the two
types belong to the same continuum but in opposition. This kind of controllers
tend to make the task more difficult than it is normally.

### Resistive strategies

They provide resistance to the participant's hemiparetic limb movements during
exercise. There is a reasonable amount of evience from multiple non-robotic
studies that resistive type exercise that requires higher effort from the
impaired limb can indeed help post-stroke patients improve motor function.

### Constraint-induced strategies

Constraint-induced therapy consists in constraining the unimpaired limb and
forcing the use of the impaired limb to perform the motor tasks.

### Error-amplification strategies

Research on motor adaptation showed that kinematic errors generated during
movement are a fundamental neural signal that dirves motor adaptation. Thus
researcher have developed robotic therapy algorithms that amplify movement
errors rather than decreasing them.

## Haptic simulation controllers

Robotic evices can be used as haptic interfaces in order o interact with
virtual reality simulation of ADL (activity of daily living).

There are potential advantages:

- haptic simulator can create many different environment in no time, without
	having to wait for setup.
- it's possible to automatically grade the difficulty of the training
	environment by adding/removing features.
- Virtual environments can be more "attractive" than an hospital hallway.
- The simulation is easy to be reset if something went wrong.
	
## Non-contacting coaching controllers

There are also control strategies for mobile robot whose purpose is to
encourage the patient performing the motor task and the therapy activities.
There's evidence that people respond differently to embodied intelligence.

The emergence of this field serves to highlight the importance of motivation in
rehabilitation therapy.

An related field that is emerging in motor lerning research and could be used
to help design robot "coaches" for rehabilitation is that of using
computational models of learning to determine the best sequence of movements
for maximizing adaptation to novel dynamic environments.

## Conclusions

Rehabilitation robotics is evolving yet and a very open field. The paper
suggests three different direction for future research:

1. Focusing on RCTs on rigorous comparison between the different algorithms
	 (and with respect to non-robotic therapies).
2. Establishing with more precision what control algorithms are most
	 appropriate for which rehabilitation task, for what types of neurologic
	 injury and at which stage of recovery.
3. Develop better computational models of motor learning and recovery in order
	 toinform robot therapy control design. This way such models can help
	 developing control algorithms using an optimization framework, once the
	 variables that drive adaptation are more clear.
