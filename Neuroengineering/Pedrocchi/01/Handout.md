# Lecture 1 --- Computational neuroscience

*Movement* is not an easy task and it's very difficult to be learned.

## Source of complexity in Motor Learning {#ch:complexity}

These are the main factors affecting the complexity of the movement:

1. **Delays**: both from carrying the information from the brain to the muscle
   and from the sensory inputs back to the brain. This loop takes time but we
   know that we can compute movements that can be extremely fast. This loop can
   take about 100 ms and we have movements which are precisely synchronized and
   coordinated and very fast and they occur fully in a very short time window.
   This means that for fast movement we cannot rely only on feedback control
   but we need also prediction. Prediction is based on the capability of our
   brain to keep information in the memory and to exploit what is memorized in
   order to plan movement which are not new and already known.
2. **Neural (signal-dependent) noise**: level of uncertainty which is on top of
   our neural signals. The noise is both on the neural pathway from brain to
   muscle, and back from sensory input to the brain. In this loop, we have such
   noise and this is higher the higher is the signal (*signal-dependent
   noise*): the standard deviation of the noise is higher when the activity of
   the neuron is higher.
3. **Non-stationarity** of the movement is given by two factors:

	- **Long-term scale**: time variance due to an ontogenetic factor (it
	  takes place during the whole life). Body is changing with age.
	- **Short-term scale**: the biomechanic is changing with the tools we
	  are using. We change tools and the overall biomechanical model needs
	  to be adapted.

4. **Non linearity**. We are not affected by the overlapping
   effect: the sum of two sequences of motor commands is not as the sum of two
   predefined movements. We have two evident examples of non-linearities of our
   motor control:

	1. *External non-linearity*. Presence of gravity force. If I am
	   activating the same sequence of motor commands to the muscle while i
	   am lying or while I'm standing the resulting movemnt is completley
	   different because of gravity. A very different activation of motor
	   command is required when I'm fighting against gravity to make the
	   desired movement happen and when I'm not.
	2. *Internal non-linearity*. joint range of motion (ROM). The same
	   exact motor sequence result to a very different solution. Let's
	   consider for example the elbow join extension: I have to deal with
	   physical constraints of human body so the same movement repeated
	   under different conditions will result different, according to those
	   limits. Saturation is known as one of the major non-linear property
	   of the system. Every limitation in the range of motion of the joins
	   (i.e. elbow, knee, fingers) is a non-linear element in the motor
	   control.

5.  **High dimensionality (aka Multidimensionality, Redundancy)**: We have
    approximately 600 muscles in our body.  Let's suppose to simplify the state
    of our muscle as ON/OFF (binary state). In this case we have `$2^{600}$`
    possible activation patterns which has an order of magnitude of
    `$10^{180}$`. This is only the muscular part but we need to consider also
    all the contributions of the different sensory inputs when we are dealing
    with movement control. So the number of data is very high but if we look at
    the execution of tasks performed by different subjects this will result in
    exactly the same trajectory during the execution. So despite the huge
    available possibility for solving this problem we can see experimentally
    that the solution is very stereotyped. There's a capability in our brain to
    optimize the choice for doing the task by recruiting synergies and motor
    programs.

### "Implemented" solutions

Prediction is needed, in order to solve the delay problem. In the same way,
planning is needed to solve neural noise. These two features, together, are
necessary in order to assure fine, reliable and accurate task executions
despite the presence of the mentioned element of complexity.

## Motor Learning

*Motor learning*: Motor control, especially considering kids, is one of the
elements where learning ability is the most exploited.  **Definition of motor
learning**:  process that allows us to adequate the behavior (e.g. the way
actions are executed) according to optimize the interaction with the
environment. The goals is to **improve performances**. The way learning occurs
is based on optimization of energy consumption and the possibility of handling
objects.

An example: bipedal walking respond to very high level performance cost
function which deals with energy consumption, flexibility and capability to do
multitasking.

Motor learning is not just a matter of thinking but a coadaptation (involving
together) both the anatomy, the physical characteristic of the body and the
neural control (functioning of the brain underpinning the task execution). We
have always to consider motor control as a single process which puts together
brain, muscles and sensory system and the *interplay between these parts is
changing as we learn*.


### Innate vs. learned abilities

1. **Innate abilities (Reflexes for humans)** are available from the very start
   of life. In fact, motor learning can't start from scratch. They are hard
   wired, fixed, automatic and robust to noise. Reflexes are not learned. There
   are series of clinical tests in order to evaluate the state of the nervous
   system of newborn babies. Most of these reflexes are forgotten in about six
   months of birth. If you meet a newborn kid and touch his palm he would grasp
   your finger. Reflexes are *the base* of the innate behavior. reflex: simple
   transformation which is automatic and occurs in the same way (hard-wired),
   not modulated or tuneable. **Input causes a Direct output**. The form of the
   output is predefined (tendon jerk reflex).  Some of the reflexes are still
   remaining in human after few month of birth.  These are the ones that are
   tested if we go to neurology.
2. **Learned abilities** are adaptable to changes, slow (as they require time
   in order to occur), and flexible. The *opposite* to the former capabilities.

If we compare across animal species, we can notice that the simple ones have a
huge amount of innate capabilities (i.e. a spider can walk on the net only
after few seconds from birth) while complex ones present a decreased amount of
hard-wired capability (i.e. newborn humans can walk after one year of life).
The necessity of motor learning allows human to interact with a very different
and flexible environment. We can change the characteristic of the body, using
tools and have multiple objectives in our motions.

This allows human to interact with flexibility. This flexibility is the major
capability that is related to motor learning. Our interest is focused on the
second kind of capabilities because they are the only one that can be learned.

The amount of learning is very different across species: humans have a very
higher capability of learning. This is the base for our comopolitism: only
humans live everywhere on the Earth, this happens because we are not
specialized. This hallmarks of *removability* (using a tool and stopping using
it), *multifunctionality* (responding to multiple objectives) and
*cosmopolitism* (surviving in Norway or Equator at the same way by putting on
proper clothes) together make the so-called exodarwinism (capability to adapt
in a very short time to a multiple set of objectives).  This is completely
different from the process that have allowed to animal and species to evolute
(endodarwinism) because the time scale we are referring to in the two
darwinisms are very different from one another.

## Brain and species

There are some studies that are investigating how the human brain is different
from animals'. It's not just a matter of sizes and rate between the brain
weight with respect to body weight which anyway is completely different in
human with respect to the other animals but it's also a matter of
functionality.

### High Bandwidth Synaptic Communication and Frequency Tracking in Human Neocortex

There's a very interesting [**study published on PLOS biology**](https://journals.plos.org/plosbiology/article?id=10.1371/journal.pbio.1002007)
in 2014 about the capability of two cortical neurons of the brain of two
subjects, which were taken from either epileptic or tumor subjects during the
surgery. These are adult, human cells extracted by surgery in a very accurate
way: as soon as the surgeon extracts them, the researchers do the experiments.
The reason for making the surgery is not to make experiments but because of
epilepsy or brain tumors, though these data were based on neurons that were
healthy so not directly connected to these pathologies. Here we can see two
pyramidal neurons and they could test the capability of one neuron to be
stimulated and transfer the information to the other one. Thus, transferred
information between a couple of neurons is the main topic of this paper. There
are huge datasets of this kind of studies in animals and, actually, most of our
knowledge is based on the information that we have from animals. This is a very
important information and it's available in labs but we need to have a glimpse
about the differences between human neurons with respect to animals'.

Let's look at the reported image: INSERT In this experiment we provide a
sequence of pulses to a neuron and then we measure the answer of the other
neuron to this sequence. The sequence has a burst of pulses, then a pause and
then an external pulse again. We want to check wether the burst (event that is
evoking a reduction of capability to react to stimuli). We can see that this
depression is located after circa .3s both for young and adult mice. After 500
ms the new stimuli is reacting with a lower amplitude (the recovery for
depression is not fully completed in the first two tracks). This doesn't happen
to humans. The first graph, the one of the synaptic depression, is providing
information about the rate between the height of the last spike of the burst
with respect to the first one and we can see that some reduction is present in
all the three recordings but on the nineth input (the one after the pause from
the burst) we can see that in young and old mice we have still a depressed
signal (.7 with respect to the initial pulse of the burst). In humans we can
distinguish the subject that had brain tumors and the epileptic one by
respectively red and green color. We can notice that both the kind of patients
belong exactly to the same cloud. These patients don't have any depression in
reacting to the stimuli. This means that they recover faster and more ready to
transfer new information and to code for faster information.

This higher rapidity in recovery from depression is different from adult human
new cortex neurons with respect to animals'.

This results in a huge difference in the information in bits that can be
transferred and in the information rate. So let's keep in mind that a lot of
work we know about the brain is based on information and experiments on animal
models but this gives us very precious highlights about how the brain works but
it's not fully transferable to human brain as it has higher capability.

This higher capability is both in the anatomical characteristic (there's a
linear scaling at the level of the proportion of the brain weight with respect
to the body mass) and in functions.

Anatomical characteristics:

- Expansion of the frontal area that is devoted to high cognitive thinking.
- Human beings have much more neurons in brain and cortex.

This higher dimension is also coupled, as already mentioned, with different
features in the functions:

We have a faster neuronal and synaptic properties. This is due to:

- Larger and more complex dendrite trees to convey information.
- Much more synapses per neurons.
- Faster information transfer within the synapses.
- Faster action potentials.

In conclusion, animal models are a very important source of information but we
have experimental evidences that the human brain has some superior properties
both in the structure and in the function. The availability of data of human
neurons is much more critical with respect to data of animals. That's the
reason behind the massive use of the information coming from animals.

### Sensory system: Top down approach in perception

If we think about the image processing on a computer, it goes pixel by pixel
and it gets the information content of each element that composes it (according
to the used color convention). Scanning goes line by line, column by column.

Our sensory system is not working in a *bottom-up* approach: fed by the
information coming from the outside in a passive way, as it was just described.
The brain is managing the sensory process. There's a *top-down* approach (also
known as active perception).  Perception is not a passive process which just
receives information from the outside but it's active in the sense that our
brain works with our sensory system in order to extract the information. How
can we have a clear evidence of this process? Let's look at the picture INSERT.
A person could say that it's representing an upside-down version of Mona Lisa
by Leonardo Da Vinci. We are guessing the information dispite the fact that's
upside down. But, if we rotate it back by 180 degrees, the image actually looks
very different from the one it was expected.  So there's something in human
brain that is completely neglecting the wrong information when the image was
the one on the left while we have immediately recognized the error in the 180
degrees rotated ("correct verse") version. This capability of recognizing means
that we are not scanning the information like a computer (line by line),
otherwise it would have been easy to spot the error on the first one but our
experience and our available knowledge of Mona Lisa makes our sensory
information processing capable to take the information as the whole and not
just by scanning element by element.

Thus, there are two elements that guide the processing of the incoming data:

1. *Experience*: the exposure to information. Let's look at this image INSERT:
   it does not have a clearer information that guides the perception so we have
   to look at it as an unmeaninful image of white and black pixels but, as soon
   as we are aware that there's a dalmata somewhere, we immedialtely start
   recognizing everything else in the picture (the tree and its shadow, etc.).
   So dispite having the possibility to have the full information from our
   sensory system, the real content that we extract from the information that
   we receive is guided by the brain. Perception is an active process where
   brain, basing on the experience, process the full dataset that is coming
   from the sensors.
2. *Attention*. All of us have some neuron which at the moment is providing
   information about foot touching the floor. Nobody is putting any attention
   to that information, at least before it was mentioned. Human brain
   prioritizes the information of the sensory input depending on the task we
   are performing.

## Sensori-motor integration

*Why "engineerizing" motor control?* From an engineering point of view
(robotics) all the features we have just discussed in [**this chapter**](#ch:complexity)
make the control of the robot really difficult to design. This is the reason
why it's very interesting from an engineering point of view to study the motor
control.  Motor control deals with the loop that goes from brain to muscle
(motor command) and then from the sensory to the brain (information).

Sensory signals go into the brain and brain produces motor command. Brain
also guides the sensory inputs in order to optimize the motor command. This
sensory motor integration needs to go to a sequence of transformations. So if
we want a robot to do a task (drinking for example) we need to go through a
sequence of transformations and this same sequence has to be done by the
"cells" used in our in-silico model.

Considering the reaching task as an example INSERT:

1. *Localization of the object with respect to our body* (egocentric
   coordinates).
2. *Localization of the object with respect to the end effector* we are
   supposed to do the task with (hand-centric coordinates).
3. Determined *how the joints of our arms need to be moved* in order for the
   hand to go from the initial position to the target position. There's a
   transformation from end effector coordinates to joints trajectories.
4. In order to accomplish the joints' movement we need to define the muscular
   activations, the required forces to be exerted by the muscles in order to
   assure the hand to get to the object.

There are three redundancies, considering the phases just described:

1. **Infinite end effector pathways**: there are infinite trajectories in order
   to go from one point to the other.
2. **Infinite joint trajectory solutions**: the same trajectory can be
   accomplished by different joints configurations.
3. **Infinite muscle activation solution**: once trajectory and configuration
   are selected (e.g. fixed) one can pick very different speed in order to
   accomplish the same task. We can have different muscular contractions
   playing with activation of agonist and antagonist.

## Reflexes vs Voluntary movements

The problem of selecting is part of voluntary movements. We have seen that we
have reflexes and voluntary movements.

- *Reflexes*: very simple transformation which are automatic and hard-wired.
  They always occur in the same way and couldn't be modulated. A specific input
  causes a specific output. The form of the output is predefined and
  stereotyped. Let's think about the **tendon jerk reflex**.
- Voluntary movements: goal directed. They respond to an intention. They are
  independent on the effector. The response of time depends on the amount of
  information to be processed. The execution speed is inversely correlated to
  the accuracy. They are learned by experience.

From now on, the focus will be on voluntary movements and we will not go deeper
into reflex mechanism.

## Voluntary movements are planned

Let's look at this experiment. INSERT. In this experiment a subject was asked
to perform different reaching tasks on an horizontal plane (with different
trajectories). We can record as many humans we want, scaling down the distance
between the different targets depending on the antropometric characteristics of
the subject. If we record all the end point trajectories we can see that these
INSERT are the areas where all the data are recorded and we can see that they
are kind of straight trajectories. Actually there are some stereotyped features
in this tasks. This is extremely interesting because we can notice that we have
infinite solution but, experimentally, the problem is solved in the same way by
many different subjects. This informs us that the motor control (the selection
of the solution) is occurring in a stereotyped way between different subjects
and this is what we want to understand. Why is the brain selecting this
solution? In order to get to this knowledge we start from describing what that
solution is and what the solution's common characteristics are.

### Characteristics of the solution

1. **Having straight end effector pathway**: it responds to very different
   joint trajectories depending on where the target is.  Endo point Trajectory
   is still very straight going from B1 to B4 but also from B1 to B5 despite
   the position in space of the targets. These straight trajectory is performed
   by using very different angular (joints) trajectories.
2. The velocity of the joints are as well modulated depending on the target's
   position but if we look at **the end point velocity it is scaled on the
   distance of the target**: our brain is coding these different tasks keeping
   stable the straightness of end point trajectory and the bell-shaped end
   point velocity. If we go deeper in studying the data we can see that by
   changing the distance of the target in the same direction (from 2.5cm to
   30cm), both hand acceleration and velocity are changing over time depending
   on where the target is. From movement onset, the planning of the task is
   different if we want to get to the far, the intermediate, or the close
   target. There is a sort of *scalar scaling* and the motor program
   specifies the spatial features of the movement and the angles and these
   movements' kinematic depends on the target position. So we are not starting
   the movement in the same way and then change abruptly the acceleration but
   we are changing the way we start the movement depending on where the target
   is.

## Laws of voluntary movement

### Law I: Voluntary movement show invariant features

The most evident observation about this law is about motor equivalence that was
proved by Hebb in 1950. It was experienced by everyone trying to write/draw
using a foot on the sand. Somehow the motor planning we have been learning
about writing, which was fully focused on the dominant hand features, can be
translated, executed and moved to another end effector of choice. We can notice
that we are able to write with both dominant and non-dominant hand.

The performance depend on the familiarity that we have with the end effector
and we won't be producing the same accuracy with a different hand effector but
our brain is capable to move from one end effector to another. Changing end
effector is changing completely the joint trajectory. The *independence from
end effector is one of this invariant features of the voluntary movement*.

### Law II: Reaction time increases with the information to be processed

If we got asked to choose between two colors we take some time to decide. When
more choices (e.g. alternatives or pieces of information to be processed) are
provided, the reaction time non-linearly increases. This is typical of
voluntary movement.

### Law III: Speed-accuracy tradeoff (aka Fitt's Law)

*The higher the speed the lower the accuracy*. If we got asked to target a
point in space very slowly we are able to repeat this movement in a very stable
way with a very high accuracy, while if we are asked to do that at the maximal
speed the points that we will touch will be much more spread across the target.
The accuracy can be measured by the end point standard deviation on target (how
far are the different repetitions, how large is the cloud of the different
repetitions). The higher the velocity the large is the cloud, the large is the
variability of the target reaching, the lower is the accuracy.

This happens because the shorter the time the lower the feedback correction we
can put in place. There's a tradeoff between the speed and the number of
recluted muscolar fibers. Higher the speed, higher the number of recruited
motor units. The higher signal-dependent noise we have, the lower the accuracy.

### Law IV: Movement efficacy grows with experience

The more we repeat the task the better we perform it. There's a process of
learning. Learning deals with voluntary movement and does not apply to
reflexes.

## Feed-forward and feedback motor control

All these rules comes at the results of an interplay between a feed-forward and
a feedback control.

- *Feed-forward control*: Based on previous experience, on a planning, on a
  prediction.
- *Feedback control*: Enters on an actual information, on the current sensory
  information. It's based on the comparison between the executed task and the
  desired task and it's characterized by a gain.

The existence of a feedforward planning can be for example shown by this figure
INSERT where we can see it's an interceptive task (grasp a ball falling from
the ceiling). We can see that before the impact and before the real motion the
effect that the subject is seing the ball falling down makes a preparatory
activation of the muscles. By looking at the activity of the biceps, triceps
and wrist muscles (flexors and extensors) we can easily recognize a muscolar
activator which is preparatory to the impact and this preparation can be only
available because we have a feed-forward planning and it doesn't use any
feedback information.

## Internal models

So in order to study these processes and this transformation of sensory-motor
loop, neuroscientists have introduced models. These are used to approximate the
functions of a real system in order to try and grab the main rules that
underpin the execution.

Internal models: representation of sensory-motor and motor-sensory
transformation that occur *internally* in our brain. They are used by our brain
to mimic the function of the system that is controlled.

### Inverse model

It estimates the motor commands required to achieve the desired sensory
feedback. Thus it works in an anti-causal direction: I have a desired task
(e.g.  a desired trajectory) and something in my brain is capable to transform
it into a motor command. This model is doing the opposite of what the body
usually does: Human body receives motor command and produces a trajectory. So
this model which is converting the desired trajectory to a feed-forward motor
command is an inverse model of my body. It's the base of the feed-forward
control because it doesn't use the actual trajectory and feedback information
but it starts from desired trajectory and information about the body status and
then produces the feed-forward motor command. It's fast (no delay) as all the
feed-forward controller but it is not able to provide any correction if
something unexpected occurs.

### Forward model

The inverse model coverts the desired behavior (e.g. desired trajectory) into a
motor command which, then, goes to the muscles.

There's also another model, known as *forward model*. This model takes as input
a copy of the output motor command (called *efference copy* going from brain to
muscles) and converts it into a predicted behavior. We need this for two main
reasons:

1. A *predictive behavior* is, in this way, *immediately available* without
   requiring all the time for the sensory feedback to come back to the brain.
2. It's an estimate of the sensory feedback that we are waiting for. It's a
   picture of what we expect to occur because of our motor command.  The
   predicted behavior *allows us to distinguish between what is a consequence
   of our motor command and what isn't*. So we can distinguish our actions from
   external consequences (e.g. external events).

*In the graph*: copy of the efference signal coming from the body and puts it in
the forward model.  We have a predicted behavior → a picture that shows what
we expect to occur because of our motor command. We can distinguish what is the
consequence of our motor command.

#### Proving forward model existence: Helmholtz experiment

One of the best empirical confirmation of the existence of the forward model
was first proposed by Helmholts INSERT. He was just guessing what is the
mechanism that allows us to distinguish whether the image we are looking at is
moving in our retina because of the rotation of our eye.  Let's suppose that I
stare a specific point: by rotating the eyes or the head despite keeping the
eyes fixed on the target the image of my retina is moving because of the motion
of the head and the motion of the eyes.  This information is not processed in
my brain as "the computer screen is moving" but I'm fully aware that the target
is fixed and I'm fully capable to distinguish if the motion of the target is
due to my head or eye motion with respect to the motion of the screen. This is
because the sensory information is processed knowing the motor command I'm
giving either to neck or eye muscle.  By keeping into account the motion I'm
sending to the muscle which will produce a sensory change of the image on my
retina I can compensate for the sensory change because I know that is not the
target moving but it's my eyes rotating. On the other hand, if the same image
is moving in my retina but not because of an eye movement caused by my muscle
but because I'm gently pushing the eyeball I would have the sensation that the
gaze is not fixed.  This experience can be modeled by the existence of this
forward model.  If I want to move my eye, my brain sends a signal (motor
command) to my eye muscle. It uses a copy of that signal (efference copy) to
predict the eye's behavior. The prediction of the consequence of that signal is
that i will have a change in the image on my retina.  This change is due to my
own movement and not because of my "screen" moving.

#### Proving forward model existence: ketchup bottle pushing

Second experiment: the ketchup bottle pushing. If you want to get the ketchup
on your dish, you strike the bottle. If you do it with your left hand you exert
a certain grip on the bottle to keep the bottle and not to let it slip.  The
load coming from the other hand would be exactly in line with the grip
intensity of the right hand (increasing gripping force as we know that we are
about to strike the bottle). If someone else is doing the external loading we
will see that, at first, we have a grip baseline that is higher in order to be
sure not to have the slipping of the bottle and then we have a delay between
the grip and the load pulses.

#### Proving forward model existence: two eyes for an eye

[**This paper**](https://science.sciencemag.org/content/301/5630/187.full) was read in class.

Escalation is a by-product of neural processing.

Self-generated forces are perceived as weaker when compared to the externally
generated ones. The sensory consequences of a movement is anticipated and used
to reduce the perception of that sensation → this is the possible cause of
the escalation in neural processing.  The perceivable component of sensory
input is removed to self-generated stimuli, so this is why we are perceiving
external sensations as enhanced. (nel paper è scritto bene).

When we provide the task we have a predicted sensory feedback coming from that
task and so the perception of the task is attenuated because if its prediction.
We are more prone to give more information to data coming and that are not
consequences of our action.

#### Proving forward model existence: why you can't tickle yourself

Fourth experiment: Why you can't tickle yourself

We don't perceive the same action in the same way if we do it on ourself or if
someone else is doing it on us.

There is a nice experiment in which we are moving the tickling farer from my
action:

1. external tickling by the robot
2. robot is moved by left hand (kind of synchronized direct self tickling)
3. robot is moved by left hand but with an increasing delay between the motion
   of the hand and the action of the robot (→ extending the link between the my
   action and the input that I receive)
4. tickling with the rotation with respect of the movement of the hand.

*Page 23, Slide 1*: This is  the ranking of the tickling sensation that is a
certain level in the case of the self produced and it goes higher toward a
similar perception to an externally produced tickling the more is the delay and
the more is the rotation.

#### Proving forward model existence: existence of associated pathologies

We can also use pathologies which affect the sensations and motor control in
order to have confirmation about the assumption we are taking.

- Schizophrenia is one.
- Auditory hallucinations can make a person erroneously interpret the internal
  voice as external.
- passivity of experiences leads to the incapability to distinguish between
  action caused by the subject himself or someone else. In this case there's a
  problem related to the predictor of the model that is not working correctly.

*In the graph at page 24, slide 1*: It's shown the mean perceptual rating
difference between self-produced and externally-produced tactile stimulation
for 3 subject groups:

1. Patients with auditory hallucination and passivity: Perceptions are pretty
   much the same for both self-produced and externally-produced, so the mean
   perceptual rating is around zero.
1. Patients without these symptoms: self-produced tactile stimulation is less
   tickly than externally produced
1. Normal control subjects: self-produced tactile stimulation is less tickly
   than externally produced

## How to train an inverse model

We have a desired state (e.g. desired trajectory) and we have the intention,
the prediction, the inverse model which produces motor commands from the
desired state to the muscles, a copy of the motor commands goes to the forward
model (dynamic predictor) and produces the predicted current state, sensory
prediction is compared with sensory feedback and the difference is used to
correct the actions. This correction depend on how much I trust (it can be
weighted differently basing on how much I can be sure about my prediction or my
sensory feedback).  Example: If I'm playing tennis in the fog, I will consider
the sensory feedback very low and I will make the action correction based only
on the prediction while in a very sunny day I will consider the sensory
feedback.

INSERT graphical representation.

## Multiple paired forward-inverse model

High cognitive functions: splitting the complex tasks into modules or
primitives. If i want to grab a bottle from someone of you i have to stand up,
walk till the line and so on to reach it. The inverse-forward model is not
imagined by scientists as a monolitic system where i'm planning the
inverse-kinematic of the overall actions to get to the bottle but it's cut into
small primitives:

1. Grab the microphone
2. stand up
3. walking
4. reaching
5. grasping

Small modules which are used together either in sequence but also
with overlapping. The way this composition has been explicitly explained.
They suppose that there are different modules and each task is a composition of a
weighted sum of different modules. How these modules are weighted? It depends
on the high level function which provides a responsibility estimator (which
gives a role to the different "sheets").

For instance the red line coming from below is the weight of module 1. the
weight goes to the motor command produced by the inverse model (the output to
the muscle), then also to the motor error (because if that module has
contributed at 10% to the task actually the motor error used to train the
inverse model needs to be weighted at 10% as well). the same weight to compose
the motor command is mirrored to the adaptation of the inverse model but also
to the forward model → we can have a training that is confined to specific
modules. if you analyze this from a neural network point of view it is much
better to back propagate and attribute error to specific parts. in this way
training is way faster than changing every weight. The network is said to be
agile. By looking at the picture on the exercise book we can see that every
single network is always trained on the error of its output. We can further add
an high cognitive function which is the **responsibility estimation of
modules**

Where are all these high level functions that we have observed in the action
and then modeled in the scheme? is there a link between the anatomy and the
physiology of the brain (anatomy → structure, physiology → function).

The current knowledge of the brain is not fully capable of making these links.
There have been a lot of studies in order to understand what are the roles of
specific areas in motor control. The puzzle is not complete. The field is very
open and we know so little of it. The effect that the understanding is
jeopardized is a fact.

## References

Slides: "computational neuroscience 1", "computational neuroscience 2"
