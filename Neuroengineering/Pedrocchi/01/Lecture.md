# Lecture 1 --- Computational neuroscience

## Page 1

### Slide 1

*Movement* is not an easy task and it's very difficult to be learned.

## Page 3

### Slide 1

#### Complexity of movements

The complexity of movement is influenced by different factors:

1. *Delays*: both from carrying the information from the brain to the muscle
   and from the sensory inputs back to the brain. This loop takes time but
   we know that we can compute movements that can be extremely fast. This loop
   can take about 100 ms and we have movements which are precisely synchronized
   and coordinated and very fast and they occur fully in a very short time
   window. This means that for fast movement we cannot rely only on feedback
   control but we need also prediction. Prediction is based on the capability
   of our brain to keep information in the memory and to exploit what is
   memorized in order to plan movement which are not new and already known.
2. *Noise*: neural noise is on this level of uncertainty of noise which is on
   top of our neuronal singals. this means that we have some uncertainties in
   the signal which are carried by our nervous system. The noise is both on the
   neural pathway (from brain to muscle) and back (from sensory input to the
   brain). In this loop we have noise and this is the higher the higher is the
   signal: the standard deviation of the noise is higher when the activity of
   the neuron is higher. This means that we need prediction and planning. These
   are necessary in order to assure fine and reliable and accurate task
   execution with this two element of complexity (delay and noise).

### Slide 2

3. *Non-stationarity of the system*. The time-variance of the system is across
   years: our body changes from kids to adults (onthologenic growth). There's
   also a short time scale: everytime we change the tool and we are using tools
   in our hands the overal biomechanical model which is doing the task needs to
   be adapted.

## Page 4

### Slide 1

4. *Non linearity of the system*. We are not affected by the overlapping
   effect: the sum of two sequences of motor commands is not as the sum of two
   predefined movements. We have two evident examples of non-linearities of our
   motor control:

	1. *External non-linearity*. Presence of gravity force. If I am
	   activating the same sequence of motor commands to the muscle while i
	   am lying or while I'm standing the resulting movemnt is completley
	   different because of gravity. A very different activation of motor
	   command is required when I'm fighting against gravity to make the
	   desired movement happen and when I'm not.
	2. *Internal non-linearity*. joint range of motion (ROM). The same
	   exact motor sequence result to a very different solution. Let's
	   consider for example the elbow join extension: I have to deal with
	   physical constraints of human body so the same movement repeated
	   under different conditions will result different, according to those
	   limits. Saturation is known as one of the major non-linear property
	   of the system. Every limitation in the range of motion of the joins
	   (i.e. elbow, knee, fingers) is a non-linear element in the motor
	   control.

### Slide 2

5. *High dimensionality of the system*. We have approximately 600 muscles in
   our body.  Let's suppose to simplify the state of our muscle as ON/OFF
   (binary state). In this case we have `$2^{600}$` possible activation
   patterns which has an order of magnitude of `$10^{180}$`. This is only the
   muscular part but we need to consider also all the contributions of the
   different sensory inputs when we are dealing with movement control. So the
   number of data is very high but if we look at the execution of tasks
   performed by different subjects this will result in exactly the same
   trajectory during the execution. So despite the huge available possibility
   for solving this problem we can see experimentally that the solution is very
   stereotyped. There's a capability in our brain to optimize the choice for
   doing the task by recruiting synergies and motor programs.

## Page 5

### Slide 1

By considering all the features we have just discussed, from a robotic point of
view, the design of the control system of a machine that is similar to humans
is very difficult to be achieved. The motor control deals with the
sensori-motor loop: From the brain to the muscle and then from the sensory to
the brain.

### Slide 2

#### Motor learning

If we think about a kid, we see that motor control is one of the element where
our learning capabilities are the most exploited. Motor learning is the process
that allows us to adequate the behavior (i.e. the way the action is executed)
according to optimizing the interaction with the environment. The overall
objective of motor learning is the **improvement of performance**. The kids
learn to walk and then to run. The way this learning occurs is based on an
optimization of the energy consumption and the possibility of handling objects.
Bipedal walking respond to very high level performance cost function which
deals with energy consumption, flexibility and capability to do multitasking.

## Page 6

### Slide 1

#### Innate vs. Learned abilities

Motor learning can't start from scratch, we need to have some innate capability
and innate motor behaviors which are available from the very start of our life.

Then this *innate abilities* are:

- hard-wired
- automatic and fixed
- robust to noise.

*Learned abilities*: what we learn has opposite characteristic (if compared to
innate abilities):

- It's adaptable to each changes over time
- It's slow: requires time in order to occur
- It's flexible

### Slide 2

If we compare across animal species we can see that the simple ones have a huge
amount of innate capabilities while the complex ones (i.e. humans) the amount
of hard-wired capability is decreased. We can see that spider can walk on the
net after few seconds of life while in human this takes one year of life. The
necessity of motor learning allow human to interact with a very different and
flexible environment. We can change the characteristic of the body, using tools
and have multiple objectives in our motions.

This allows human to interact with flexibility. This flexibility is the major
capability that is related to motor learning.

## Page 7

### Slide 1

Also humans don't really start from scratch but have some innate behavior:
reflexes. These are not learned and we have some reflexes that are available at
birth and some of the reflexes in the newborn are abandoned in a very few month
of life, opening room to the possibility to learn. The reflex of sucking a
finger helps because as soon as the baby is born he can be easily fed by the
mother's breast.

To every newborn baby there's a set of tests done in the birth room to check if
the nervous system is working properly and this is done through testing
reflexes.

Most of these reflexes are forgotten in about 6 months from the birth. Some
example of reflexes:

- Grasping reflex
- ...

### Slide 2

Some of the reflexes are still remaining in human after few month of birth.
These are the ones that are tested if we go to neurology.

Motor learning is not just a problem of thinking but a coadaptation (involving
together) both the anatomy, the physical characterisic of the body and the
neural control (functioning of the brain underpinning the task execution). We
have always to consider motor control as a single process which put together
brain, muscles and sensory system and the interplay between these parts is
changing as we learn.

## Page 8

### Slide 1

The amount of learning is very different across species: humans have a very
higher capability of learning. This is the base for our comopolitism: only
humans live everywhere on the Earth, this happens because we are not
specialized. This hallmarks of *removability* (using a tool and stopping using
it), *multifunctionality* (responding to multiple objectives) and
*cosmopolitism* (living in Norway or Equator) together make the so-called
exodarwinism (capability to adapt in a very short time to a multiple set of
objectives).  This is completely different from the process that have allowed
to animal and species to evolute (endodarwinism) because the time scale we are
referring to in the two darwinisms are very different from one another.

##### Notes

Hallmark: a typical characteristic of a person or feature of a thing.

### Slide 2

#### Brain and species

There are some studies that are investigating how the human brain is different
from animals'. It's not just a matter of sizes and rate between the brain
weight with respect to body weight which anyway is completely different in
human with respect to the other animals but it's also a matter of
functionality.

## Page 9

### Slide 1

##### High Bandwidth Synaptic Communication and Frequency Tracking in Human Neocortex

There's a very interesting [**study published on PLOS biology**](https://journals.plos.org/plosbiology/article?id=10.1371/journal.pbio.1002007)
in 2014 about the capability of two cortical neurons of the brain of two
subjects, which were taken from either epileptic or tumor subjects during the
surgery. These are adult, human cells extracted by surgery in a very accurate
way: as soon as the surgeon extracts them, the researchers do the experiments.
The reason for making the surgery is not to make experiments but because of
epilepsy or brain tumors, though these data were based on neurons that were
healthy so not directly connected to these pathologies. Here we can see two
pyramidal neurons and they could test the capability of one neuron to be
stimulated and transfer the information to the other one. Thus, transferred
information between a couple of neurons is the main topic of this paper. There
are huge datasets of this kind of studies in animals and, actually, most of our
knowledge is based on the information that we have from animals. This is a very
important information and it's available in labs but we need to have a glimpse
about the differences between human neurons with respect to animals'.

Let's look at the reported image: INSERT In this experiment we provide a
sequence of pulses to a neuron and then we measure the answer of the other
neuron to this sequence. The sequence has a burst of pulses, then a pause and
then an external pulse again. We want to check wether the burst (event that is
evoking a reduction of capability to react to stimuli). We can see that this
depression is located after circa .3s both for young and adult mice. After 500
ms the new stimuli is reacting with a lower amplitude (the recovery for
depression is not fully completed in the first two tracks). This doesn't happen
to humans. The first graph, the one of the synaptic depression, is providing
information about the rate between the height of the last spike of the burst
with respect to the first one and we can see that some reduction is present in
all the three recordings but on the nineth input (the one after the pause from
the burst) we can see that in young and old mice we have still a depressed
signal (.7 with respect to the initial pulse of the burst). In humans we can
distinguish the subject that had brain tumors and the epileptic one by
respectively red and green color. We can notice that both the kind of patients
belong exactly to the same cloud. These patients don't have any depression in
reacting to the stimuli. This means that they recover faster and more ready to
transfer new information and to code for faster information.

This higher rapidity in recovery from depression is different from adult human
new cortex neurons with respect to animals.

### Slide 2

This results in a huge difference in the information in bits that can be
transfer and in the information rate. So let's keep in mind that a lot of work
we know about the brain is based on information and experiments on animal
models but this gives us very precious highlights about how the brain works but
it's not fully transferable to human brain as it has higher capability.

## Page 10

### Slide 1

This higher capability is both in the anatomical characteristic (there's a
linear scaling at the level of the proportion of the brain weight with respect
to the body mass) and in functions.

Anatomical characteristics:

- Expansion of the frontal area that is devoted to high cognitive thinking.
- Human beings have much more neurons in brain and cortex.

This higher dimension is also coupled, as already mentioned, with different
features in the functions:

We have a faster neuronal and synaptic properties. This is due to:

- Larger and more complex dendrite trees to convey information.
- Much more synapses per neurons.
- Faster information transfer within the synapses.
- Faster action potentials.

In conclusion, animal models are a very important source of information but we
have experimental evidences that the human brain has some superior properties
both in the structure and in the function. The availability of data of human
neurons is much more critical with respect to data of animals. That's the
reason behind the massive use of the information coming from animals.

### Slide 2

##### Sensory system: Top down in perception

If we think about the image processing on a computer it goes pixel by pixel or
column by column and it gets the information content of each element that composes
it (according to the used color convention). Scanning goes line by line, column
by column.

Our sensory system is not working in a *bottom-up* approach: fed by the
information coming from the outside in a passive way, as it was just described.
The brain is managing the sensory process. There's a *top-down* approach (also
known as active perception).  Perception is not a passive process which just
receives information from the outside but it's active in the sense that our
brain works with our sensory system in order to extract the information. How
can we have a clear evidence of this process? Let's look at the picture INSERT.
A person could say that it's representing an upside-down version of Mona Lisa
by Leonardo Da Vinci. We are guessing the information dispite the fact that's
upside down. But, if we rotate it back by 180 degrees, the image actually looks
very different from the one it was expected.  So there's something in human
brain that is completely neglecting the wrong information when the image was
the one on the left while we have immediately recognized the error in the 180
degrees rotated ("correct verse") version. This capability of recognizing means
that we are not scanning the information line by line, otherwise it would be
easy to spot the error on the first one but our experience and our available
knowledge of Mona Lisa makes our sensory information processing capable to take
the information as the whole and not just by scanning element by element.

## Page 11

### Slide 1

There are two elements that guide the processing of the incoming data:

1. *Experience*: the exposure to information. Let's look at this image INSERT:
   it does not have a clearer information that guides the perception so we have
   to look at it as an unmeaninful image of white and black pixels but as soon
   as we are aware that there's a dalmata somewhere we immedialtely start
   recognizing everything else in the picture (the tree, its shadow, etc.). So
   dispite having the possibility to have the full information from our sensory
   system, the real content that we extract from the information that we
   receive is guided by the brain. Perception is an active process where brain,
   basing on the experience, process the full dataset that is coming from the
   sensors.
2. *Attention*. All of us have some neuron which at the moment is providing
   information about foot touching the floor. Nobody is putting any attention
   to that information, at least before it was mentioned. Human brain
   prioritizes the information of the sensory input depending on the task we
   are performing.

## Page 12

### Slide 1

#### Sensory-motor integration

Sensory signals go into the brain and brain produces motor command. Brain
also guides the sensory inputs in order to optimize the motor command. This
sensory motor integration needs to go to a sequence of transformations. So if
we want a robot to do a task (drinking for example) we need to go through a
sequence of transformations and this same sequence has to be done by the
"cells" used in our in-silico model.


### Slide 2

## Page 13

### Slide 1

##### Movement phases and sequence of transformations

Considering the reaching task as an example INSERT:

1. *Localization of the object with respect to our body* (egocentric
   coordinates).
2. *Localization of the object with respect to the end effector* we are
   supposed to do the task with (hand-centric coordinates).
3. Determined *how the joints of our arms need to be moved* in order for the
   hand to go from the initial position to the target position. There's a
   transformation from end effector coordinates to joints trajectories.
4. In order to accomplish the joints' movement we need to define the muscular
   activations, the required forces to be exerted by the muscles in order to
   assure the hand to get to the object.

### Slide 2

#### Reflexes vs Voluntary movements

The problem of selecting is part of voluntary movements. We have seen that we
have reflexes and voluntary movements.

- *Reflexes*: very simple transformation which are automatic and hard-wired.
  They always occur in the same way and couldn't be modulated. A specific input
  causes a specific output. The form of the output is predefined and
  stereotyped. Let's think about the **tendon jerk reflex**.
- Voluntary movements: goal directed. They respond to an intention. They are
  independent on the effector. The response of time depends on the amount of
  information to be processed. The execution speed is inversely correlated to
  the accuracy. They are learned by experience.

From now on, the focus will be on voluntary movements and we will not go deeper
into reflex mechanism.

## Page 14

### Slide 1

#### Voluntary movements are planned

Let's look at this experiment. INSERT. In this experiment a subject was asked
to perform different reaching tasks on an horizontal plane (with different
trajectories). If we record as many humans we want, scaling down the distance
between the different targets depending on the antropometric characteristics of
the subject. If we record all the end point trajectories we can see that these
are the areas where all the data are recorded and we can see that they are kind
of straight trajectories. Actually there are some stereotyped features in this
tasks. This is extremely interesting because we can notice that we have so many
infinite solution but experimentally the solutions are solved in the same way
by many different subjects. This informs us that the motor control (the
selection of the solution) is occurring in a stereotyped way between different
subjects and this is what we want to understand. Why is the brain selecting
this solution? in order to get to this knowledge we start from describing what
is that solution and what the solution's common characteristics are.

Let's analyze the common chacteristics: *Straight end-effector pathway*. This
pathway responds to very different joint trajectory depending on where the
target is. So trajectory is very straight if we go from B1 to B4 but also from
B1 to B5. Straightness of trajectory is common despite the location in space of
the target. This same straight trajectory is performed by using very different
angular trajectories. These trajectories are reported on the right. *Velocity
of the joints* are as well modulated depending on the targets but if we look at
the end point velocity we can notice that it's just *scalar scaled* on the
distance of the target! This is telling us that our brain is coding this
different tasks and selecting the solution keeping stable the straightness of
the end point trajectory and the bell-shaped end point velocity. If we go
deeper in watching this data we can see that if I change, on the same
direction, the distance of the target from very near (2.5cm) to far (30cm) we
can notice that hand acceleration over time and hand velocity over time is
scaled depending on where the target is.  From the beginning of the task
(movement onset) the planning of the task is different if we want to get to the
far target or if we want to get to the close target or the intermediate one.
There's a kind of scalar scaling and the motor program specifies the spacial
feature of the movement and the angles and this movement kinematics depends on
the target position. We are not starting the movement in the same way and then
breaking the abrupt deceleration but we are changing the way we start the
movement depending on where the target is.

##### Notes

Abrupt: improvviso.

### Slide 2

## Page 15

### Slide 1

#### Laws of voluntary movements

##### Law I: Voluntary movement show invariant features

The most evident observation about this law is about motor equivalence that was
proved by Hebb in 1950. It was experienced by everyone trying to write/draw
using a foot on the sand. Somehow the motor planning we have been learning
about writing, which was fully focused on the dominant hand features, can be
translated, executed and moved to another end effector of choice. We can notice
that we are able to write with both dominant and non-dominant hand.

The performance depend on the familiarity that we have with the end effector
and we won't be producing the same accuracy with a different hand effector but
our brain is capable to move from one end effector to another. Changing end
effector is changing completely the joint trajectory. The *independence from
end effector is one of this invariant features of the voluntary movement*.

### Slide 2

##### Law II: Reaction time increases with the information to be processed

If we got asked to choose between two colors we take some time to decide. When
more choices (e.g. alternatives or pieces of information to be processed) are
provided, the reaction time non-linearly increases. This is typical of
voluntary movement.

## Page 16

### Slide 1

##### Law III: Speed-accuracy tradeoff (aka Fitt's Law)

*The higher the speed the lower the accuracy*. If we got asked to target a
point in space very slowly we are able to repeat this movement in a very stable
way with a very high accuracy, while if we are asked to do that at the maximal
speed the points that we will touch will be much more spread across the target.
The accuracy can be measured by the end point standard deviation on target (how
far are the different repetitions, how large is the cloud of the different
repetitions). The higher the velocity the large is the cloud, the large is the
variability of the target reaching, the lower is the accuracy.

This happens because the shorter the time the lower the feedback correction we
can put in place. There's a tradeoff between the speed and the number of
recluted muscolar fibers. Higher the speed, higher the number of recruited
motor units. The higher signal-dependent noise we have, the lower the accuracy.

### Slide 2

##### Law IV: Movement efficacy grows with experience

The more we repeat the task the better we perform it. There's a process of
learning. Learning deals with voluntary movement and does not apply to
reflexes.

## Page 17

### Slide 1

#### Feed-forward and feedback motor control

All these rules comes at the results of an interplay between a feed-forward and
a feedback control.

- *Feed-forward control*: Based on previous experience, on a planning, on a
  prediction.
- *Feedback control*: Enters on an actual information, on the current sensory
  information. It's based on the comparison between the executed task and the
  desired task and it's characterized by a gain.

The existence of a feedforward planning can be for example shown by this figure
INSERT where we can see it's an interceptive task (grasp a ball falling from
the ceiling). We can see that before the impact and before the real motion the
effect that the subject is seing the ball falling down makes a preparatory
activation of the muscles. By looking at the activity of the biceps, triceps
and wrist muscles (flexors and extensors) we can easily recognize a muscolar
activator which is preparatory to the impact and this preparation can be only
available because we have a feed-forward planning and it doesn't use any
feedback information.

### Slide 2

#### Internal models

So in order to study these processes and this transformation of sensory-motor
loop, neuroscientists have introduced models. These are used to approximate the
functions of a real system in order to try and grab the main rules that
underpin the execution.

Internal models: representation of sensory-motor and motor-sensory
transformation that occur *internally* in our brain. They are used by our brain
to mimic the function of the system that is controlled.

## Page 18

### Slide 1

##### Inverse model

It estimates the motor commands required to achieve the desired sensory
feedback. Thus it works in an anti-causal direction: I have a desired task
(e.g.  a desired trajectory) and something in my brain is capable to transform
it into a motor command. This model is doing the opposite of what the body
usually does: Human body receives motor command and produces a trajectory. So
this model which is converting the desired trajectory to a feed-forward motor
command is an inverse model of my body. It's the base of the feed-forward
control because it doesn't use the actual trajectory and feedback information
but it starts from desired trajectory and information about the body status and
then produces the feed-forward motor command. It's fast (no delay) as all the
feed-forward controller but it is not able to provide any correction if
something unexpected occurs.

### Slide 2

## Page 19

### Slide 1

##### Forward model

The inverse model coverts the desired behavior (e.g. desired trajectory) into a
motor command which, then, goes to the muscles.

There's also another model, known as *forward model*. This model takes as input
a copy of the output motor command (called efference copy going from brain to
muscles) and converts it into a predicted behavior. We need this for two main
reasons:

1. A *predictive behavior* is, in this way, *immediately available* without
   requiring all the time for the sensory feedback to come back. It's an
   estimate of the sensory feedback that we are waiting for. It's a picture of
   what we expect to occur because of our motor command.
2. The predicted behavior *allows us to distinguish between what is a
   consequence of our motor command and what isn't*. So we can distinguish our
   actions from external consequences (e.g. external events).


### Slide 2

###### Proving forward model existence: Helmholtz experiment

One of the best empirical confirmation of the existence of the forward model
was first proposed by Helmholts INSERT. He was just guessing what is the
mechanism that allows us to distinguish whether the image we are looking at is
moving in our retina because of the rotation of our eye.  Let's suppose that I
stare a specific point: by rotating the eyes or the head despite keeping the
eyes fixed on the target the image of my retina is moving because of the motion
of the head and the motion of the eyes.  This information is not processed in
my brain as "the computer screen is moving" but I'm fully aware that the target
is fixed and I'm fully capable to distinguish if the motion of the target is
due to my head or eye motion with respect to the motion of the screen. This is
because the sensory information is processed knowing the motor command I'm
giving either to neck or eye muscle.  By keeping into account the motion I'm
sending to the muscle which will produce a sensory change of the image on my
retina I can compensate for the sensory change because I know that is not the
target moving but it's my eyes rotating. On the other hand, if the same image
is moving in my retina but not because of an eye movement caused by my muscle
but because I'm gently pushing the eyeball I would have the sensation that the
gaze is not fixed.  This experience can be modeled by the existence of this
forward model.  If I want to move my eye, my brain sends a signal (motor
command) to my eye muscle. It uses a copy of that signal (efference copy) to
predict the eye's behavior. The prediction of the consequence of that signal is
that i will have a change in the image on my retina.  This change is due to my
own movement and not because of my "screen" moving.

## Page 20

### Slide 1

###### Proving forward model existence: ketchup bottle pushing

Second experiment: the ketchup bottle pushing. If you want to get the ketchup
on your dish, you strike the bottle. If you do it with your left hand you exert
a certain grip on the bottle to keep the bottle and not to let it slip.  The
load coming from the other hand would be exactly in line with the grip
intensity of the right hand (increasing gripping force as we know that we are
about to strike the bottle). If someone else is doing the external loading we
will see that, at first, we have a grip baseline that is higher in order to be
sure not to have the slipping of the bottle and then we have a delay between
the grip and the load pulses.

### Slide 2

###### Two eyes for an eye

[**This paper**](https://science.sciencemag.org/content/301/5630/187.full) was read in class.

Escalation is a by-product of neural processing.

Self-generated forces are perceived as weaker when compared to the externally
generated ones. The sensory consequences of a movement is anticipated and used
to reduce the perception of that sensation → this is the possible cause of
the escalation in neural processing.  The perceivable component of sensory
input is removed to self-generated stimuli, so this is why we are perceiving
external sensations as enhanced. (nel paper è scritto bene).

When we provide the task we have a predicted sensory feedback coming from that
task and so the perception of the task is attenuated because if its prediction.
We are more prone to give more information to data coming and that are not
consequences of our action.

## Page 22

We don't perceive the same action in the same way if we do it on ourself or if
someone else is doing it on us.

There is a nice experiment in which we are moving the tickling farer from my
action:

1. external tickling by the robot
2. robot is moved by left hand (kind of synchronized direct self tickling)
3. robot is moved by left hand but with an increasing delay between the motion
   of the hand and the action of the robot (→ extending the link between the my
   action and the input that I receive)
4. tickling with the rotation with respect of the movement of the hand.

## Page 23

### Slide 1

This is  the ranking of the tickling sensation that is a certain level in the
case of the self produced and it goes higher toward a similar perception to an
externally produced tickling the more is the delay and the more is the
rotation.

### Slide 2

We can also use pathologies which affect the sensations and motor control in
order to have confirmation about the assumption we are taking. Schizophrenia is
one of them. Auditory hallucinations can make a person erroneously interpret
the internal voice as external, and passivity of experiences leads to the
incapability to distinguish between action caused by the subject himself or
someone else. In this case there's a problem related to the predictor of the
model that is not working correctly.

## Page 24

*In the graph*: It's shown the mean perceptual rating difference between
self-produced and externally-produced tactile stimulation for 3 subject groups:

1. Patients with auditory hallucination and passivity: Perceptions are pretty
   much the same for both self-produced and externally-produced, so the mean
   perceptual rating is around zero.
1. Patients without these syntomps: self-produced tactile stimulation is less
   tickly than externally produced
1. Normal control subjects: self-produced tactile stimulation is less tickly
   than externally produced

### Slide 2

We have a desired state and we have the intention, the prediction, the inverse
model which produces the motor commands from the desired states to the muscles,
a copy of the motor commands goes to the forward model and produces the
predicted current state, sensory prediction is compared with sensory feedback
and the difference is used to correct the actions. This correction depend on
how much I trust (it can be weighted different based on how much I can be sure
about my prediction or my sensory feedback).
Example: playing tennis in fog → I will consider the sensory feedback very
low and I will make the action correction based only on the prediction while in
a very sunny day I will consider the sensory feedback.

## Schema of the models

The more motor command correction is used for training, modifying the inverse
model. The error in the kinematics is used to train the forward model. Inverse
and forward kinematics work adapting themeselves in order to optimize the
performance. This mechanism is the learning. 

Another important high cognitive function is the evaluation of the reliability
of the feedback vs models: depending on the conditions the high level of the
brain can either give more importance to the feedback or to the models.

This picture was born not by anatomical investigation of the brain but by
psychofisical studies. In order to explain the way we are able to perform tasks
neuroscientists have built this hypothesis of existance of the internal models.
These internal models are arranged in this way in order to explain the motor
control functioning.

In other words: the body receives motor commands and produces movements which
are then encoded as kinematics. The inverse model goes the other way around (wrt
the body): it receives desired kinematics and produces the motor command. The
forward command does the same of the body: it receives motor commands and
produces the expected sensory feedback.

## Page 25

### Slide 2

High cognitive functions: splitting the complex tasks into modules or
primitives. If i want to grab a bottle from someone of you i have to stand up,
walk till the line and so on to reach it. The the interplay between
inverse-forward models is not imagined by scientists as a monolitic system
where i'm planning the inverse-forward kinematics of the overall actions to get
to the bottle but it's "cut" into *small primitives*:

1. Grab the microphone
2. Stand up
3. Walking
4. Reaching
5. Grasping

Small modules which are composed together either in sequence but also with
overlapping. The way this composition has been explicitly explained by Kawato
and Wolpert is that they suppose that there are different modules and each task
(what we want to do) is a composition of a weighted sum of different modules.
How are these modules weighted? It depends on the high level function which
provides a responsibility estimator (which gives a "role" to the different
"sheets").

Let's consider the red line coming from below. It is the weight of module 1
(generally the n-th module). This weight goes respectively to:

1. *Motor command* produced by the inverse model (aka the output to the
	 muscles).
2. *Motor error*: this is important because if that module has contributed to
	 10% of the task then the motor error, which is used to train the inverse
	 model needs to be weighted at 10% (refer to the reported scheme). Let's
	 suppose to execute a movement composed by standing up, reaching and
	 grasping. How much the error is due to the grasping? just a small part.
	 That's the reason why the same weight that is used to compose the motor
	 command is mirrored to the adaptation of the inverse model.
3. *Forward model*

This allows to have a training which is confined to specific modules

By analyzing this from a neural network point of view, it's very important when
we build a network to have an *agile learning*. We don't want the whole network
to move and at every single time we have an error. If we can we have to
separate into block the overall system and then backpropagate (or whatever
algorithm) attribute the error to a specific part of the network. The overall
training and learning is much more faster. The same happens in here: we use
modules because training goes back to those specific modules that the subject
effectively used in the task. There's a parallelism that we can reason about:
every single network is always trained on the error of its output.

On the schema we just saw we can add the responsibility estimator of modules.

## Page 26

### Slide 1

Do we have a clear understanding on where these function that we have
observed in the action and modeled are located in the CNS?  Where are all
these high level functions that we have observed in the action and then modeled
in the scheme? is there a link between the anatomy and the physiology of the
brain?

The current knowledge of the brain is not fully capable of making these links.
There have been a lot of studies in order to understand what are the roles of
specific brain areas in the motor control. It's not a complete puzzle. Brain
studies is an area of study where the field is very open because we know so
little of it. There's the fact that understanding is kind of jeopardized in
this field. It needs to be humbly taken as something that has some
understanding evidences but not all the pieces are perfectly combined in a
single figure and it's a very liquid understanding. There are lots of studies
that are very active in order to improve this field. It's not like designing an
FIR filter but it's a kind of moving field. What we learn today is the base for
understanding what is being discovered in the future that may go against our
assumption. Knowledge is not freezed but these are the current understanding
and tools in order to be able to catch up with something new that may happen.

## Page 27

### Slide 1

If we talk about motor control the main areas that we should consider inside
the CNS which works toward the motor control are: motor cortex, the brain stem
and spinal cord, the basal ganglia (subcortical structure of nuckei), the
cerebellum and thalamus.

The story about learning and sensory motor loop occurs with the the interaction
of different *loops* inside the brain:

1. motor cortex → spinal cord → thalamus → motor cortex.
2. *Dopamine Loop*: basal ganglia → thalamus → motor cortex. this loop is
	 damaged in the parkinsonian deseases (tremor, gait freezing, slowliness).
3. *Cerebro-Cerebellum Loop*: Motor Cortex → Cerebellum.
4. *Spinal-Cerebellum Loop*. Spinal cord → Cerebellum → Brain Stem → Muscles.
5. *Arc-Reflex* → occurring directly at Spine Level.

Many structures are used in order to receive information about sensory, the
actual movement, and provide better performance for motor commands.  What
exactly does each single part is not fully known.

### Slide 2

What particularly does the primary motor cortex in this world of motor control?
Penfield's homulculus associates a specific group of neurons in motor or
sensory cortex to a specific district of the body with somatotopic
organization: closer parts in the body correspond to adiacent group of neurons
in the cortex. This association has been done by Penfield and it is based on
the fact that, for example, the neurons corresponding to the lip in the motor
cortex is the *first neuron* that produce some activation in my muscles at the
level of the mouth (e.g. the neuron with the *lowest threshold*).  By looking
at my mouth and see the neurons that are activated when i use my mouth, these
are many more than this association.  This association is done with the first
neuron able to activate that part, it's not about the only ones activating that
part. Low intensity stimuli can activate the single muscle but one muscle can
be activated by the stimulation of many more cortical sites.  The homunculus
exist but it's not the only pathway in order to activate that part of the body
and we need to keep in mind something really important: the homunculus is
*dynamic*. We all think that the homunculus is in everybody's brain. This is
not fully true. All of us have a map in motor and sensory cortex and this
depend on the training and it's a plastic map modified by the experience that
we have.  The motor area is larger with the greater ability to do something.
The game between training and neural changes (last time we talked about
neuro-motor coadaptation) is called brain plasticity and it is the basis for
any rehabilitation work: helping the patient by training properly to find new
neural structures to perform efficiently the task. Rehabilitation deals with
brain mapping. This is why we have a lot of redundancy in the system. This same
redundancy is what we see in these many loops going around in the brain. this
redundancy can be exploited when a damage is present so if we have a brain
damage of a certain area of the brain then the other neurons can be adapted
(aka dynamically weight changes) in order to compensate for the damage. This
compensation can be either full or partial. It works in helping the subject to
adapt this mechanism and obtain a functional recovery.  GOTO Pag 30.

## Page 30

### Slide 1

From an histological point of view cerebral cortex has a very repetitive
structure which is based on columns. These columns are perpendicular to the
surface of the cortex and they are described by six layers where we can find
different types of neurons. Different neurons populate the different layers. we
have now a clear understanding of the anatomy and the histology of these
columns. These columns have a repetitive structures.  We know how the different
columns are connected either within the same layer or to other layers or to
other structures outside the cortex (i.e. thalamus etc).  How are these neurons
activated in order to produce the function? This is something that is still not
understood. We know all the anatomy and who is contacting who, the number of
synapses and so on. We have the picture very clear but how it functions to
produce the task is far from being known. Some specific parameters of the task
are coded inside the neuron of the cortex. What is the cortex encoding about
the task execution? GOTO Pag 28.

## Page 28

### Slide 1

Many experiments on primates on the role (the parameters of the task) which can
be recognized in the functioning on neurons in the cortex.  Example of the
slide: a primate which is making the flexoextension of the wrist (Evarts,
1968).  The amplitude of the wrist is set to be the same so we are not
investigating on the kinematic of the movement.   The dashed line is reporting
the starting of the task (`$t_0$`).  The study considers three different
conditions. He was recording the muscular activation, the first two signals in
each condition. Let's biomechanically analyze each of them:

1. *Free movement* without any role of the pulleys.  We can see that there's a
	 coordinated recruitment of the muscle of the wrist so as soon as the task
	 start the extensors are silenced and the flexors starts the activation. This
	 produces the task without any load against or in favor.
2. *The pulley (support of the system) against the direction of the movement*
	 (more force is required) The extensor activation is no more necessary while
	 a stronger muscular activation of the flexor muscles is required.
3. *The pulley favouring the movement* (less force required).  Higher
	 activation of the extensor to keep the pulley at the beginning and then we
	 need to stop this action to grant the rotaton of the pulley without any
	 flexors' contribution.

The previous description was kind of trivial. Evarts concentrated on PTN
activity (third track for each condition).  PTN → pyramidal tract neuron.  This
is a neuron in the cortex which was selected because connected at that muscle.

Considering the first PTN track as the reference we can notice that in the
second condition we have higher firing rate (higher activation of the PTN of
case II) and a silent PTN in the third case. The conclusion of Evarts was that
the firing of the corticospiral neuron (aka the one under study by Evarts) is
related to the force exerted during the movement and not to the displacement of
the wrist. We can see that we are linking the activation of a cortical neuron
with a parameter of the task which is not in kinematics-related (because
kinemactics are exactly the same for the three conditions) but it's related to
the force required to do the task. The higher (lower) the force, the higher
(lower) the firing rate. The firing rate of the neuron directly modulates the
force.

### Slide 2

Experiment \# 2: Cortical neuron which is recorded (on the left) and EMG muscle
(on the right).

Two considered tasks:

	1. With a precision grip (a specific and precise action of all the fingers).
		 Two different sub-conditions were acquired:
		1. Little force
		2. Heavy one
	2. With a power grip (grasping a large handle).

Let's start from the muscle. From a muscular point of view, these two actions
require the activation of the same muscle and we can record the activation of
that muscle which have different profiles but kind of similar.  But if we look
at the cortical motor neuron we can notice that this modulates evidently the
activation in the case of light and heavy precision grip also modulating the
activity in a different way in the two conditions while it's silent in the case
of the power grip.

Even if a given motorneuron is monosynaptically connected to a given CM cell
their firing patterns do not have to parallel each other because of the
multiplicity of connections to motorneurons... 

**Conclusions**: Cortical neurons are modulating differently depending on the
*task that we need to do*. The kind of motor task *tunes* the activation of the
different neuron even if the task is involving the same muscles.

We can notice that we are getting further from the Penfield homunculus. It's
much more complex.

## Page 29

### Slide 1

## Georgopoulus's experiment

Primates with array of electrodes. Not a patch of a single neuron but a
recording of multiple neurons. The task is a reaching task in different
directions in the horizontal plane.

On the left we have  *raster plots*. It provides information about one neuron
activity over time. It takes the boolean information on *when the neuron is
spiking*. If the neuron has a spike a little line appears. Each track is
representing a neuron. The information is organized into different lines
representing the different neurons being monitored.

if we look at the neuron 5 (the last one in each group) we can see that it
tends to spike at a higher frequency on the left side targets. Despite looking
at the monkey was very good in getting to the target with a high resolution
capability, if we look at a single neuron we can see that it *modulates* the
frequency with the direction but not in a high resolution way, it's quite the
same on a broad areas of direction.  The idea of this study is that it's not
the single neuron that is coding the direction.  Somewhere the direction needs
to be coded in a very high resolution way because the effect is a very precise
task (like the one performed by the primate) but if it's not the single neuron
it can be a *population of neurons* that works together in order to accomplish
this high accuracy task. He modeled each neuron with vectors (from neuron 1 to
neuron 5).  Putting together all these vector giving the following rules:
maximal neural activation gives versus and direction of the vector we want to
build for a specific neuron.  Let's suppose, for example, that neuron 5 has
maximal activation on left direction. This means that its maximal activation
gives the direction (horizontal) and versus (to the left) of neuron 5. The
amplitude of this vector is *scaled depending on the frequency of the spiking*

By representing all the neurons of the raster plots (considered individually)
in the vectorial form, according to this rule, we can obtain a vectorial
representation similar to the one on the right. The sum of the vectors is
providing information about the vector of the population of neurons. It's
impressive how this summation alligns with the direction of the task. The
entirety of the activity of those neurons encode with very high accuracy the
direction of the task. Neurons by themselves had encoded the information about
the direction but they couldn't account for the high accuracy of the task. This
information integrated together explain and is very well accurate with respect
to the direction of the task. GOTO Page 30.

### Slide 2

leggi la slide.

From compendium: The spike frequency of a neuron encoding for one specific
direction is increasing when load is applied against it. It's also true that
the frequency of spike is reduced if the load is applied in the neuron's
preferred direction. [To be continued]

## Page 30


### Slide 2

## Primary motor cortex

We have really low level connections between motor cotex and muscles and then
we have demonstration that inside the motor cortex some very high level content
parameter are encoded. Force to execute the task, the goal of the movement, the
direction of the movement etc... They are high level parameters so they are not
bringing binary information but modulating the activity. GOTO Next.

## Page 31

### Slide 1

## Spinal cord

The spinal cord has a somatotopic construction even though part of the somato
depends on the level of the spine and not a transversal map of the spine. What
is present in the transversal map is that we have in the lateral pathway.
lateral: most distant part with respect to the medial line of the body. Lateral
part controls the distal muscles (limbs) the ventro-medial pathway controls
axial muscles (trunk, muscular tone, equilibrium control, breathing, etc).
Spinal cord has a very important role in *cyclic movement* (i.e. gait,
chewing). In cyclic movement we usually have a symmetrical or antisimmetrical
use of the two sides in a coordinated way.

### Slide 2

These cyclic motor tasks are a combination between some voluntary component and
some reflexes. In this way the role of the spine (remembering that reflexes
occurs in there) is really important in the execution of cycling tasks because
it's the providing the involuntary components. There are lots of studies that
investigates on the contribution of spinal side in the control of gait vs the
contribution of high brain structures. Some role of the brain is still there
because people with traumas can't move on foot so of course there's a voluntary
component. But we also know that a decerebrated cat can walk on a threadmill
and this work about the brain and the spine collaboration into cyclic task is a
very hot topic in the study of gait control. There are some areas in the brain
stem which work as Central Pattern Generators (aka CPG. they are oscillatory
pacing that gives rhythm to the specific cyclic task, a sort of "internal clock
signal for a specific motor task").  Activation of CPG is at the kind of
triggering of the mechanism at the spine level.

## Page 32

### Slide 1

## Supplementary motor areas

Supplementary Motor Areas is a cortical part anterior with respect to M1.  It
has a lot of demonstrated rules in complex tasks planning.  Neuroimages like
fMRI are used as a different investigation approach. A simple finger flexion is
not activating the supplementary motor area but a sequential finger tapping is
a typical task when it comes in performing fMRI because it's very complex and
it investigates coordination and it is a small movement and quite far away from
the head of the subject that needs to be fixed. In this case we can see that M1
and S1 are active and there is a huge activation of SMA and pre motor cortex.
What happens is that by asking the healthy subject to repeat mentally the
finger tapping only SMA is active. SMA is related to the thinking of the task.
If the subject is not executing the task, M1 and S1 are not active.

### Slide 2

If we try to put in a scheme the overall picture we can see the following.
Let's start from the bottom: we are considering the muscoloscheletal system
(the simplest because related to biomechanics).  The activation of the
motorneurons (the link between the spinal chord and the muscles).  The motion
of the limb goes to the different sensory systems (proprioception, visual and
so on) which are the afferent signals going inside the spinal cord and running
upward.  If we stay with the colors of the scheme the one in red is information
related to the kinematics.  The kinematics information is the one going to the
thalamus. Thalamus is the structure in the brain where all the sensory inputs
arrive and are then sent to the different parts of the brain. they can go to
the parietal cortex or somato-sensory cortex and some of the information goes
in the premotor area. The reaching of the motor cortex pass through the
parietal cortex and then part of the sensory feedback goes to the pre-motor
areas. These pre-motor areas talk to the motor cortex as well and then the
motor cortex also receives the feedback from the somatosensory cortex and
produces the output to the spinal cord (cortico-spinal tract). The motor cortex
goes through the pontine and communicate with the cerebellum, who has an output
which goes back to the motor cortex as well. There's the other loop of the
cerebellum which instead goes to the spinal cord through the brain stem.

The multiple areas have some specific parts in the picture and are in many way
connected in loops. It's visible the reflex arc at the bottom right. The only
thing that's not reported is the connections involving basal ganglia.

## Page 33

### Slide 2

## The cerebellum role in motor control

The cerebellum role in motor control. It's a structure in the occipital part of
the brain with an anatomy that is very different. it receives forty and much
more inputs with respect to output. The information is condensed and integrated
there. It receives information both about the motor action (from the cortex)
and the sensory feedback (from the spinal cord). It projects upword to the
cerebro and through the brain stem to the spine and the motor neurons. We know
a lot about the cerebellum and its most important feature is that it has a very
*high plasticity mechanism*. We know a lot about the capability of synapses,
between the cells of the cerebellum, to change across time.

## Page 34

### Slide 1

From an anatomical point of view it's a scaling down of the cerebro: it has
both the emispheres and an internal subcortical part. It has also a cortex as
well. It's very close by the brainstem and it's underneath the occipital part
of the cortex. It is organized in two emispheres (left and right emispheres)
and in lobes (Anterior and posterior lobes). Then it has a very internal
structure which is the floccolus. If we consider it from the medial line (look
at the right): we have the spinocerebellum (medial part of the emispheres which
mainly goes to the descending systems, so the spinal chord). The interpositum,
the fashons goes to the lateral part of the spine. The dentate (lateral part of
the emispheres) goes to the premotor cotex and the cortex. The dentate nucleus
(lateral part of the emispheres) goes to the pre-motor cortex and the cortex,
constituting the so-called *Cerebro-cerebellum Loop*.

### Slide 2

The cerebellum is very strange because we can find a person that gets to adult
age who went to the doctor for headache and he was discovered without any
cerebellum. He arrives at 30 years old without having any reason for doing a CT
scan. On the other side there's a case in which the cerebro was almost
completely black (black = damaged) and the only one working was this little
guy. So what's the point in this? The cerebro-cerebellum collaboration allows a
surrogation between the two. What is nice of this picture is that in the first
case we are considering a patient with a cerebellum removal with surgery
(typically for tumors). As we can see the subject was doing a series of "swing
and stance" (gait) so the subject was capable to walk. All the graphs are
referred to that patient.

- reticulo-spinal pathway governs the *limbs*.
- vestibulo-spinal pathway governs the *equilibrium control*.
- rubro-spinal pathway governs the *limbs and trunk*.

The spine is conveying all the information which is running to the muscles.
All the different parts of the spine have specific spiking activity related to
the phases of the gait (swing/stance). The role of the different pathways is
different in the parts of the stem. What is impressive is that without the
cerebellum all these activities is very changed (red tracks). The subjects can
still walk, of course but somehow we can see that the cerebellum was a
supermodulator of those signals which are still assuring the walking but in a
different way.

Another experiment of cerebellar patients is that if we ask a patient to play
darts and wear prismatic glasses that deviate the information of the patient's
view of an angle. If the subject plays darts while wearing the glasses we would
notice a shift from the target but if we let him check for the position error
he will be able, after a while, to compensate this shift. Then when we remove
the glasses from the guy and ask him to perform the same he will perform the
shift in the opposite direction because he has precedently learnt to compensate
for the shift. This compensation is something that happens in the cerebellum
and it's unconscious and automatically done. In a healthy subject the first
compensation happens slowly and the after-effect compensation happens faster
but if we consider a patient without cerebellum this capability of automatic
learning is fully damaged and they keep going to the shifted target. Cerebellum
has a great importance in **Automatic learning**.

The other great advantage of the cerebellum is that it's based on a very few
types of cells. Its structure is repeatable slice by slice and it's based on
the interaction between few cells. We have a kind of a very good understanding
of those cells.

## Page 35

### Slide 1

- The first input to the cerebellum is given by **mossy fibers** (mf). They
	synapse (positively) to granule cells through glomeruli (a kind of synapsis
	buttons).
- **Granule cells** are the most numerous cells in the brain. they are lots and
	lots and they are tiny. They have a predominant axon which goes and forms the
	so-called parallel fibers.
- **Parallel fibers** have a specific direction and they are axons of the
	granule cells.
- The mossy fiber also activates **Golgi Cells** (GoC): they are big cells in
	the same granular layer of the cerebellum and they few Golgi cells with
	respect to granule cells. Golgi cells are inhibiting cells, they produce
	negative connections to the granule cells to avoid an extra activation of the
	granule cells. *Golgi cells are interneurons*.
- Parallel fibers connect to the arboric dendrites of the **Purkinje cells**
	(PC), These dendrite trees are very open and connect parallel fibers
	transversally.  Close parallel fibers are all connected to the same Purkinje
	Cell.
- In the same molecular layer where the PC dendrite is we have some other cells
	which are the **Basket cells** and the **Stellate cells**: they are the
	molecular interneuron (MLI). These cells are inhibiting neurons in the
	molecular layer. Inhibiting fiber between the parallel fibers and the
	purkinje cells.  (comment: So to sum up we have positive molecular
	contribution from the purkinje cells and negative contribution given by the
	basket and stellate cells. check if it's true)
- The Purkinje cell project its inhibitory output **Deep Cerebellar Nuclei**
	(DCN).  **DCN is the only output of the cerebellum**.  In the end there's a
	loop between the mossy fiber and the DCN.
- There's another input, which is the **inferior olive cells** (IO-C): which
	project to the cerebellum through the so-called climbing fibers.
- The **climbing fibers** have a very specific activity in the cerebellum: they
	are connected to purkinje cells and they are climbing around the dendritic
	branches of the PC and have a lot of synapses with it. When they are
	activating it's like shouting on the PC. it's taking the whole dendrites and
	giving the same signal.

### Slide 2

Here we can the same scheme in a model-like representation.  MF goes to granule
cells and to Golgi cells (which are inhibitory interneurons).  Granule cells
activate PC, then we have climbing fibers going directly to the PC and then PC
inhibits the neurons of the deep cerebellar nuclei. We can see that the
structure is quite simple. Connections are known and who goes to where.

## Page 37

### Slide 1

Let's see what happens to the signal. At the level of the Purkinje fiber over
time.  We can see that there are two main patterns.

1. *Simple spikes*: produced by the parallel fiber excitation (e.g. GrC)
2. *Complex spike*: it is activated by climbing fibers (e.g. Inferior Olive
   input).  
   
There's a plastic connection between simple spike and complex spike. Let's go
deep in this connection.

### Slide 2

## Hebbian plasticity: an experiment. Spike time-dependent synaptic plasticity

In-vitro experiment: one cell is patch-clamped with its post synaptic cells.
Experiment in which we have two patched-clamped neurons. We can inject current
to activate them and see also the link between them. The first proposing this
kind of experiment was Hebb. Hebb's synapsis have this characteristic behavior.
`$w$` is the weight between pre and post neuron.

- There's a level of connection before the experiment so that some activation
  on pre-synaptic neuron is producing a change in post-synaptic voltage across
  the membrane.


- The y-axis is the post voltage.
- The x-axis is time. On x axis the small arrow is indicating the time of
  pre-synaptic spike.

- The experiment is based on a phase called pairing phase: from the outside
	(from the 2 patch clamps) we force the 2 neurons either 
		- Post-synaptic to spike just after the pre-synaptic (mimicking a
			causal relation). It's following the direction pre-post. *graphs on the
			left*.
		- Post-synaptic spiking just before the pre-synaptic. This is the
			anti-causal direction. *graphs on the right*.

After stopping the pairing (comment: which is a kind of learning I suppose) we
can check the level of activation that is produced on the post-synaptic after
the spike of the pre-synaptic neuron. What we can notice is that, on the left,
the pairing has produced a higher post-synaptic voltage, while on the right we
have a reduction of the post-synaptic voltage.

- Causal: strenghten of the connection.
- Anti-causal: weakened of the connection.

Hebbian Rule: The more the two consecutive neurons fire together, the higher
the strength of their connection.

This mechanism is the base of one of the most important of synaptic plasticity.

## Page 38

### Slide 1

The mechanism described in the previous slide is the base of one of the most
important rules of synaptic plasticity, which is the *time dependent synaptic
plasticity (TDSP)*.  It is usually pictured with this kind of plot. On the
x-axis we have the time interval between pre and post firing and the 0 is the
pre-synaptic spike time.  The y-axis is the slope of excitatory post-synaptic
potential (aka post voltage).  If post is happening after the pre (positive
time interval) then we have an increase of the voltage: a change of the weight
with a strengthen of the connection (higher weight).  If the post is happening
before the pre (negative time interval) there's a weakening of the connection.
This overall effect is active within a certain time window. If the two neurons
are not spiking together their connection is not modulated. This mechanism is
exactly what manages the synaptic plasticity between the climbing fibers and
the parallel fibers.

### Slide 2

Both parallel and climbing fibers go to the same Purkinje Cells.  There's a
weight between the parallel fiber and the Purkinje cell (so in this slide we
are studying the plasticity between parallel fibers and Purkinje cells).
Horizontal axis of the graph on the right is time and we have the parallel
fibers spiking a few of the simple spikes while the climbing fibers is a huge
spike in a specific timing which produces the complex spike. What happens to
the plasticity?  Those parallel fibers that were spiking right before the
climbing fibers are strengthen a lot (huge change of the weight) while if a
parallel fiber (not reported on the graph) were spiking way before the complex
spike, the connection between the parallel fiber and the Purkinje fiber is not
strengthen. The shouting of the climbing fiber is used to tune the connections
between the parallel fibers and the Purkinje cells. These synaptic changes is
what produces the change of the output because if the parallel fiber is giving
more signal to the Purkinje cell then the Purkinje cell is providing more
signal to the DCN and so it's inhibiting the output. The overall functioning of
the cerebellum can be imagined as a mechanism to modulate the DCN output.

## Page 39

### Slide 1

Somehow there are physical and chemical structures of the neurons which work as
information storage in the brain. we have also signal and synapses which work
as information transmission (parallelism between machine and human brain
function).

- Storing information: it's inside the structure of the neurons and the
	synapses in the brain.
- Information transmission: all between synapses (chemical, electrical etc).
- Primary computing element: the spiking events that occurs in the neurons.
- Computational basis: we don't know.

### Slide 2

So what we have analyzed is:

- What is motor control?
- What is motor learning?
- What are the areas involved in the motor control?
- Focus on the cerebellum because of its learning capabilities. Huge importance
	in motor control and motor learning.
- How the physiology of the cerebellum accounts for these learning capabilities
	(plasticity, changes between cells).

## Page 1

### Slide 1

Today we are working on the modeling of the neurons which is the basic element
to develop computational neuroscience simulation.  We have seen the cerebellar
circuit and we are tuning the lesson on the modeling of the cerebellar system
as a paradigmatic example but most of the tecniques here proposed are used for
other networks and other neurons.  The information coding between neurons runs
on action potentials (AP) or spikes which are characterized on 2 conditions
(ON/OFF) and three different states of the neurons (Resting state, Fast
depolarization, return to the resting state condition which is slower). Spike
can be considered the basic unit of neural coding. All the elements that
contain the information are about how the cell gets to the spiking.

### Slide 2

The \#2 is the basic coding.

## Page 2

### Slide 1

### Slide 2

## Neural electroresponsive properties

There are different electroresponsive properties of the neuron that needs to be
understood and considered and allows us to split apart the different neurons of
the brain:

- **Autorhythm**: *graphs on the left*. Autorhythm spontaneous firing pattern.
	It means that without any external input the neuron has spontaneous pacing
	spiking activity. if i=0 (null input current), let's look at the graphs to
	the left. The spiking activity is still present (first row). This activity is
	due to internal mechanism in the neurons which allows a continuous change of
	the under-threshold voltage of the neuron. It's an *intrinsic* property
	because it doesn't depend on the extrinsic input provided to the neuron.
- **SubThreshold Oscillation (STO)**: another *intrinsic* property, it does not
	depend on extrinsic stimulation, as the first property. It means that there's
	an oscillation underneath the voltage of the neuron. If the frequency of the
	stimulation is in line with this oscillation (e.g. wrt the internal
	resonance) the system becomes resonant.
- **Depolarization induced bursting**: There is an increasing firing rate of
	the neuron following the starting of the depolarization current. A positive
	current causes a burst (i.e. very fast and close-by spikes). This is valid
	for an excitatory neuron. 
- **Linear current-frequency relationship**: The higher the intensity of the
	input, the higher the frequency of the produced spikes of the neurons. Let's
	consider the extrinsic currents graph below (*graph to the left, in red*).
	The lowest-intensity one is going to have an output similar to the first
	graph on top-right, the middle-intensity the second and the high-intensity
	the third. Let's also look at the top graph on the very right. It's showing
	the relationship between firing rate with respect to the intensity of the
	current.
- **Spike-Frequency Adaptation (SFA)**: by looking at the top graphs in the
	middle (the ones with the spikes) we can notice that the very first spikes
	are very closed to each others with respect to the steady state condition.
	The input is stable but there is an adaptation phoenomenon. The steady-state
	frequency is slightly lower than the one reached at first. In the top graph
	at the very right we can see the 2 curves: the top one is indicating the
	initial firing rates while the bottom one represent the steady-state
	condition.

## Page 3

### Slide 1

- **Phase reset**: *graph on right-top*. As soon as there is a huge input to
	many neurons, this input makes a phase reset. This means resetting the
	neurons' autorhythm to a common point.
- **Post-inhibitory rebound burst**: *graphs on right-middle and right-bottom*.
	Let's consider an excitatory neuron that was inhibited by a negative current.
	When we remove the inhibition (by removing the negative current) from the
	neuron the autorhythm starts with a rebound burst (a fast sequence of the
	very first spikes).
- **Resonance**: *graphs on the left*. If we are giving a sinusoidal
	stimulation to the neuron of a certain amplitude (in this case 700 pico amps)
	and this sinusoid has a frequency that may change from .3 to 10 Hz, we can
	see that the production of the spikes is faster (occurs immediately after the
	starting of the stimulus) or slower (it happens after time). The output
	occurs immedately after the stimulus or after a while. this response time
	(basically the latency) is related to the resonance. It's not a problem of
	amplitude because it was fixed.  There's a certain frequency where this
	response is higher. This frequency is the resonance frequency of the neuron.
	Spiking frequency increases with the amplitude of the stimulus but the
	response time to a sinusoidal input depends on the oscillatory frequency of
	the neurons.

These properties are not belonging of all neurons. They differentiate from each
other because they have them or not. They could also be different. There's the
need of in-vitro study in order to understand these abilities. The only ability
they have in common is the fact that they're coding information by spiking
activity.

### Slide 2

This table is summing up the presence of the properties just mentioned in
Cerebellar cells.  MLI (Molecular Layer Interneurons) are gathering both
Stellate and Basket Cells.

| Cerebellar Cells                                   | Autorhythm   | Sub-Threshold Oscillations | Depolarization-induced Burst | SFA (Spike Frequency Adaptation) | Phase reset | Post-inhibitory rebound burst | Resonance  |
| :---                                               | :---         | :---                       | :---                         | :---                             | :---        | :---                          | :---       |
| Golgi Cell                                         | Y (5-15 Hz)  | Y                          | Y                            | Y                                | Y           | Y                             | Y (0 band) |
| Granule Cell                                       | N            | Y                          | N                            | N                                | N           | N                             | Y (0 band) |
| Purkinje Cell                                      | Y (40-80 Hz) | N                          | Y                            |                                  | N           | N                             | N          |
| Molecular Layer interneurons (Stellate and Basket) | Y (10-20 Hz) | N                          | N                            | N                                | N           | N                             | N          |
| Deep Cerebellar Nuclei                             | Y(10-30 Hz)  | N                          | Y                            | Y                                | N           | Y                             | N          |
| Inferior Olive Cells                               | N            | Y(1-4Hz)                   | n.a.                         | n.a.                             | Y           | N                             | N          |

# Page 4

## Slide 1

## Some recall of H-H model (Hodgking and Huxley model)

[H-H model](https://en.wikipedia.org/wiki/Hodgkin%E2%80%93Huxley_model):
refresh it from the compendium.  This H-H model is modelling the activities of
the neurons by studying the conductances of the membrane of the neurons and the
variation of the conductances by opening and closing the channels of the
membrane for the specific ions. We are introduced to HH model by considering Na
and K as they are the most relevant ions underpinning the occurence of spikes
and the neuronal activity. This is a very rough simplification. Indeed if we
wanted to really use HH models in a high detailed modelization of neurons we
should use *compartimental models*.

## Slide 2

- This is the basic idea of **HH model** (*graphs on the left*): we have
	variation of conductances across the membrane which explain the occurrence of
	spikes and also the occurrence of specific pattern of spikes.
- On the other hand (*graphs on the right*) the world of neuronal models goes
	to **single point neurons**, which means that the whole neuron is kind of
	condensed into a generator of spikes, so the information which is really kept
	about the activity of the neuron is not the whole process of modification of
	the membrane voltage but the occurrence of spikes.  *a second, more complete,
	definition of single point neurons*: Models that condensed the neurological
	knowledge in one single point, not compartments, and these models are built
	in order to be able to mimick the electroresponsive properties know for that
	neuron. This kind of models are used when there's the need to model
	microcircuits

# Page 5

## Slide 1

## Modeling the neuron

### HH compartimental models

Just to have an idea on how HH models are used nowadays, the direction that has
been taken by neuroscientist to capture the mechanism that describes the
mechanism of the neuron is by using a compartimental models. The base is an HH
equation but the difference is that we have to compute it not for the cell or
generally for the membrane of the neurons but for compartments of neurons. Why?
Because from elettro-physiological studies we know that the different segments
have different resistances and conductances properties. So to put everything
into a single model is far from biological knowledge of the neuron.


## Slide 2

This is a purkinje cell. As we know, it has an open dendritic tree which goes
transversal to the parallel fibers. This is an example of a study in which we
can see that the model has been split into multiple compartment: very small
dendrites, medium size dendrites, large size dendrites and so on (the different
rows in the table on the right). Inside the overall dendrite tree different
compartment have been associated to dendrites of different diameters. The other
compartments are: soma, axon initial segment (AIS) and so on as reported on the
table on the left... We can see that one single cell is made by very different
compartments because the electrophysiology knows that the specific ion channel
is present only in certain compartment and not in others: Biological and
electrophysiological studies have a deeper understanding of the channels which
are present or not in the different compartment. This means that collapsing
anything in a single model is far from the current elettrophysiological
scientific knowledge. For each compartment there are physical and geometrical c
properties and sections and then all the existance of conductances. so putting
together all this knowledge from the literature means that we need to consider
all the potassium channel that are present in a specific compartment and that
was measured with specific values of conductances and resting potentials and so
it's like that each compartment and each ions determine a part of the whole
equation of representing the neurons. Then of course there are open parameters
which need to be tuned in order to assure that there's a continuity in between
the different compartments: boundary condition between one compartment to the
other must assure that continuity is respected.  GOTO Page 8.

# Page 6

## Slide 1
## Slide 2

# Page 7

## Slide 1
## Slide 2

# Page 8

## Slide 1
## Slide 2

This is to give a clear understanding that using HH models, usually neurons or
python neurons (software-implemented) are capable of taking advantage of all
the information that biologist have on the cells but then we have for one cell
like 15 hours of simulation for one spikes production. This means that the
complexity and the computational loads which derives from this very detailed
description of the funcioning of the neurons allows us to capture a lot of
electroresponsive properties and get really close to describing what the neuron
is actually doing but the computational load is extremely high. So if we want
to simulate small circuit and not just a single cell this solution is not
viable.  Actually there are three directions the scientists are going in order
to solve this kind of limits:

- *simplify the neuronal model*: in this way we are having more capability to
	consider microcircuit.
- *improve the computational load*: CINECA supercomputers are now dedicating
	30\% of their resources to neuroscientists. There is a very large use of HPC
	for having realistic, high level simulations.
- on the other sides we have that new computational solutions based on the idea
	of neurons which goes up and down are new hardware to be designed. If we want
	to have a look: information on Human Brain Project website about Spinnaker
	thought for neuro-simulations.

Here on this slide we can see that if we use a very detailed compartimental
models we can explain a lot of the properties. i.e. the complex spike of
Purjunje cells reported on bottom left, the post inhibitory rebound on
top-central, the frequency current adaptation (bottom-central). This is the
frequency modulation with a ramp external input (top-right). All of these are
the results of the good models.

# Page 9

## Slide 1

## Modeling the single neuron

### The Leaky Integrate and Fire neuron

On the other side of the medal we can try to reduce the details of the model
and to try to just capture not the processes that underpins the activation of
the spikes but *the spikes activity* as we were saying before. This because the
spiking frequency is the encoding information in neurons. So the very basic
single point neuron models is the **Leaky Integrate and Fire**.

The equation discribing it is the following:

$\tau_{m}\cdot\frac{dV_{m}(t)}{dt} = -(V_{m}(t) - E_l + R_m\cdot I_{in}(t))$. 
Membrane potential dynamics equation.

If $V_m > V_{th}$, then $V_m = V_{\text{reset}}$. Spike condition

This model is described by one single membrane potential's dynamic potential
having one resting potential $E_l$ and then having a threshold $V_{th}$: if the
membrane voltage $V_m$ goes above $V_{th}$ the spikes occur. What describes the
membrane is its resistance $R_m$ and the time constant $\tau_m$. The time
constant is given by the product between the membrane resistance and the
capacitance $C_m$. It says *how fast the voltage changes below threshold*. Then
there's an input current $I_{in}(t)$ which accounts for external inputs.  In
the LIF there's also refractory period which account for the non capability of
the neuron to answer to any external input in a time window just after the
spike event. Also for LIF in the compendium there's a great description but
it's assumed as a previous knowledge. Action potentials are described only as
events so they don't have a shape and time profile but they only represent a
binary information: whenever the membrane voltage reaches the threshold the
membrane potential is reset and the spike occurs, it's registered. The shape of
the spike is lost in this model. Remember that the interplay between the
changes of the conductances of Na and K is exactly what allows us to describe
the shape of the spike. Now the shape of the spikes is condensed in just an
event recording (e.g. it has occurred or not).

## Slide 2

### Biological plausibility vs computational cost (i would make it the first subsection)

As a whole we can start considering that we need to consider two elements and
find the happy medium between these two parameters.

- computational cost
- biological plausibility

HH model do have a very high biological plausibility expecially when
considering compartimental HH but they have almost a prohibitive cost in terms
of computational loads.  LIF has a very efficient computational cost but the
biological plausibility is very far from the HH. There have been a lot of
studies and proposal which have tried to actually move in the vertical
direction (down verse) or in the hyperbolic down direction: paying some extra
computation load without going high values of load but increasing the
biological plausibility.  In this direction we will see a few models.

# Page 10

## Slide 1

### Izhikvich neuron

One of the mentioned models is the Izhikevic neurons (2003). It's based on a
quadratic equation of the variation of the membrane potatial with a second
variable (multidimentional equation) which is a membrane recovery variable,
linked to the potential by some specific parameters and again as in the LIF
model we have that when the membrane potential overcomes a threshold which
Izhiasdf fixed at 30mv there's a spike. After the spike membrane potential is
set to another parameter. After the spikes the membrane recovery variable is
set at another parameter.

Model parameters:

- a → which describes the timing for the u variable to decay.
- b
- c → reset voltage after the spike.
- d → how much the membrane recovery variable is increased after spike.

Depending on values of these four parameter the model can describe very
different electroresponsive properties. Dor instance, let's concentrate our
attention to the thalamo-cortical neurons (aka TC, bottom-bottom-left).  If you
look at the graphs on top the points with label TC we can basically see what
are the parameter values that best model the behavior (aka electroresponsive
properties) of those neurons.  The plausibility is increased, despite the
increase of the computational cost (quadratic formula, we have 4 parameter).

## Slide 2

### Adaptive Exponential LIF neuron

Another attempt in order to improve the biological plausibility (the
description) of single point neurons (→ not considering single conductances,
not considering compartments inside the neurons so the neuron is considered as
one element and it has one set of parameters).  Another example is here
reported: **the Adaptive Exponential LIF neuron**. In this case we can see that
an exponential component appears. The exponential depends on the distance
between the current voltage of the membrane and the threshold. The difference
with respect to Izhikevic is that the threshold is not set at 30mV a priori but
it's a free parameter for the neurons. In this way, a larger set of
electroresponsive properties can be described based on the parameter values.
The voltage threshold is more realistically set, differently for each neurons
and, also, subthreshold resonances and adaptations can be captured.

# Page 11

## Slide 1

### E-GLIF (Extended generalized LIF)

Another model of neuron is the Extended Generalized LIF which has been used to
model all the neurons in the Cerebellum. We are again simplifing the structure
of the cells into a single point model. We are only describing the neurons by
recording the spikes as boolean events (losing the description of the shapes of
the spikes). What we want to capture is the burst in the polarization, the
adaptation of the frequency from the initial burst into the steady state and
the subthreshold oscillation which occurs without any external inputs. The goal
is to have a *linear* model that can be solvable with a very low computational
power. The point is to try to understand whether the same model can be used to
describe different cells by tuning properly the different parameters based on
the current knowledge that we have from biology.

## Slide 2

In this case we have 3 equations but they are all linear 3 LODE (Linear
Ordinary Differential Equations).

1. Membrane potential
2. Adaptive current
3. Depolarizing current

Then we have a description of the refractory period which is more detailed. We
can see that first we have a random generation of the spiking (second block of
equation, second equation) as soon as the threshold is reached there's not a
deterministic activation of the spike as in all the other models so that,
whatever is the condition of the cells, everytime we have the overcoming of the
threshold we will have an AP generation while in this model the AP depends on
an equation which is called *Escape rate*. Of course this random generation
depends how faster the system has reached the threshold. As soon as a spike
occurs the different parameters are set to specific values similarly to the
Izhikevic model with parameters a, b, c, d. These equations have a lot of free
parameters. They (both the red and the blue ones) are the ones that need to be
tuned to make the model works but some of them (blue ones) are biological
quantities and they have a direct correspondance to a biological property and,
thus, should be found in specific biological experiments (they could or should
be measured). Red parameters are, instead, free parameters of the model and are
used in an optimization process in order to move the model and capture the
electroresponsive properties known for the specific cells.

# Page 12

## Slide 1

In this slide there's a description of the functioning of the model. *The top
graph* is the membrane potential. $V_r$ is the resting potential and on the top
we have the threshold $V_th$ (dashed red line). The model moves up and down
reaching the threshold.  Everytime it reaches the threshold, there's a
stochastic process which determines the spike event which are the final output
of the model (red dots).  To describe this underthreshold mechanism the other
two equations are needed (let's look at the graph below): the red curve is
referred to the $I_\text{adap}$. Everytime there's a spike the red has a deep
change, an offset, later it has a slow decrease and the next offset start from
new value (not from zero). So we can see that we can ramp up or go down.  on
the other side, the green curve is the depolarizing current $I_\text{dep}$ is,
instead, an increasing variable (→ equation block below, equation 3: there's
+A2) which at every spikes goes exactly to the same value A1.  $I_\text{dep}$
at spike is set at A1 and then it is described by an equation which is based on
a parameter k1. These two mechanisms are present in the equation of the
membrane voltage and they describe these different shapes of the underthreshold
trajectories. For example *looking at the top graph, third wave*, we consider
this window and, *looking at the corresponding window in the graph below*, we
can see that the effect that the depolarizing current is going down but the
adapting current is still quite high there's a kind of slowing down of the
changing of the membrane voltage which, when the adaptive current decreases,
allows the current to reach the threshold and then for the spike to occur.

## Slide 2

## Model developing, the methodology

What are the steps we need to go through to develop this model?
1. We *start from the equation of the model*.
2. We need to *simulate the equation in proper software* which have been
	 developed by putting the parameters which are already known from biological
	 knowledge. (i.e. derived from experiments, the blue parameters we saw
	 before).
3. *Model optimization*: we need to have a clear understanding of what the
	 model should capture (what are the features) and then design an optimization
	 algorithm which properly tunes the free parameters (red parameters we saw
	 before).  Optimization is based on a *target behavior* and *sets the free
	 parameter* while before the optimization we need to set the biological known
	 parameters.
4. *Model validation*: It's extremely important that input data in this phase
	 must be different from the ones of the optimization phase, otherwise it's
	 lik talking to ourselves and conferming our results.
	 
To summarize: In order for a model to be well designed, we need to take all the
biological model and fit it into the model's parameters, then we need to
optimize the free parameters of the model on the electroresponsive properties
(e.g. the final behavior we want the model to describe). In the end we have the
validation: the model is freezed, optimization is completed and now we need to
feed new behavioral data. We need to understand by comparing the results on
this validation set if the model is capable to capture to the different
properties.

The reported graphs are related to model of Golgi cells. the one on the left is
related to optimization phase while the one on the right is related to the
validation phase.

# Page 13

## E-GLIF for Golgi cells

## Slide 1

Here are reported the results for the Golgi cells.  Refer to this slide for the
Golgi cells model information because there's not on the compendium.

In the Golgi cells we can see autorhythm, spike frequency adaptation and the
depolarization burst and post-inhibitory rebound (negative current switch off
going back to zero and the initial burst before going back to autorhyhm). Then
there's the target. the frquency of the first spike and triangles are the
frequency of the stady state. The black values are the simulation while the
green ones are the experimental data. we can see that they both go quite well
together.

## Slide 2

Validation of Golgi cell's model. All blue data are the experimental data while
the black datas are the correspondent simulation. In red there are the input
data and they are the same both for the experiments and the simulations.
Feature by feature we have to check. GOTO Page 15.

We can see that with smallr input data we have lower frequency and the higher the input the higher the frequency with the spike adaptation properties. The very first burst is faster than the steady state.

# Page 14

## Slide 1
## Slide 2

# Page 15

## Slide 1

This is phase reset. This was not present in the optimization but was tested in
the validation and we can see that the overall plant of the model allows to
capture the experimental picture of phase reset in a quite similar way. it's
not a one by one correspondance but the population of the cells had the
expected behavior.

Experiments and models can run parallelly. Any model takes as much as we can
from experiments. On the other way around sometimes the model can hypothize or
show a specific behavior which is not yet tested, so specific experiment can be
designed in order to test whether the result of the model are really describing
something that it's present in the cells or not. The experiment informs the
model but, on the other hand, the model can open question for new experiment.
there's a loop in between: it's not a direct translation of experimental
knowledge to model. If we think that with model we can put cells together and
make hypothesis about the population while from an experimental point of view
we still have some limits in recording information about population of neurons.
That is because if we patch clamp a cell we can get information few cells,
while if we want to capture information on a population we will lose the
information about the membrane of the single cells.  Sometimes model can inform
experiments of something that expoeriment is not able to see. We want to
advance in both lines so that the mutual information is confirmed.

## Slide 2

## E-GLIF for the other cerebellar neurons

Same approach can be tailored for different cells. We can see the behavior of
the different models of cells. The same model, changing both the parameter
coming from experiments and the ones that can be optimized, can be tuned in
order to describe the functioning of different neurons type.

In this way we have also behavior of GR (Granule cells), PC, MLI and DCN.

In the end, by modeling all the cells in the cerebellum we have all the bricks
to put together a very small microcircuit having all the elements of the
cerebellar network.

# Page 16

## Slide 1

For granule cells the model is able to capture the oscillations while for the
MLI there's the autorhythm (at 0 current there's a spiking frequency) and so on
an so forth.  Purkinje cells have a very high frequency autorhythm.  It's very
important for the DCN the post-inhibitory rebound. purkinje cells inhibits the
DCN. DCN behavior is very often dense of information in this post inhibitory
rebound.

## Slide 2

*Graphs on the left*: here there's the demonstration that this kind of models
describes even in spike coding (that is different from shape coding) the
complex spike.  Complex spike that we have seen in the previous lesson was dued
to the planning fibers. it's described not by a specific shape but *by a very
high frequency bursting*.

*Graphs on the right*: data about the modeling of the inferior olive neurons.

# Page 17

## Slide 1

Resonance has been well captured for granule cells and phase reset was very
evident in the inferior olive (which is very important for climbing fiber).

## Slide 2

We are moving in the right direction, improving the biological plausibility
without exceeding in the computational cost. We have a little bit of lower
biological plausibility (E-GLIF fully describe anything that's over threshold
while Izh is ramping up until 30mV which is somehow the initial depolarizing of
the neuron) but E-GLIF is gaining something about efficiency in computational
load because we are using linear equations instead of quadratic equation.

# Page 18

## Slide 1

## Neurorobotics

Robot: a machine capable to move with actuators. It needs to have at least few
sensors in order to gather information from the environment. Such information
coming from the sensor(s) are used by a controller in order to regulate and
control the activation of the actuator.

Three basic elements are:

1. Sensors
2. Controller
3. Actuators

How many of these elements respond on how complex the robot is. It depends on
the number of degrees of freedom and the information to be integrated.

Other necessary element for a robot is:

4. The body

Whatever simulator can simulate sensors, controllers and actuators but if we
want this simulator to be a robot it needs to have a body. This body can be, as
well, as complex as we want with as much degrees of freedom we want. Depending
on the characteristic of the body, the sensors, the actuators and the
controller are changed accordingly.

Then usually a robot has a *set of tasks*. The flexibility and the capability
to interact with multiple tools and very different environments is one of the
hallmarks of human species with respect to the other animals. The same is valid
for robot: usually robot are very good at performing specific tasks (i.e.
mounting the doors of the cars, painting the doors, mounting the front part,
etc). There are set of tasks on which robot design is tailored. Even the
humanoid robots do have a set of tasks (walk, sit down, dance).

The *way the robot is interacting with the environment* is mediated by the
characteristics of the environment. Environments is linked to *tasks*.
Environment is giving information to sensors and receiving the movement from
the actuator. Environment is giving informations to sensors and receiving the
movement from the actuators. It can be seen similarly to what we have modeled
when we were talking about inverse and forward model. The interaction between
body and environment is movement and, on one side it's commanded by the
actuator set and on the other sensed by the sensorial components.

## Slide 2

If we want to talk about neurorobotics this is a very specific part of robotics
which deals with the aim of mimicking the neural mechanism underpinning the
behavior of humans.

- Mimicking the behavior. This is biomechanical information
- Neurorobot should mimick the structures of the brain which controls the task
	execution.

What is the issue? The fact that we are dealing with two completely different
scales: on one side we have the biomechanical information which, for example
are the trajectories of the joints or the activation of the muscles.  For
instance we could have motion captions or inertial sensors, EMG data (images on
the bottom). We could also have information about which areas are active inside
the neuroimages. In between these two information there's a gap. if we want to
go deeper we could also use patch clamp to have single cell information or
local field potentials to see the behavior of group of cells. So we have a lot
of information in this story but this information is not automatically bonded
together because each of them has a different scale, a different level of
detail and passing from one level to the other is something that is not
immediate and automatic.  If we want to use a controller which mimicks the
brain circuits this means that we want to take the information from the small
scale, so neural information (Patch clamp and local field potential), on the
other way around, if it should mimick the behavior we should consider the
information on the other side (motion caption ad stuff like that). So somehow a
neurorobot needs to deal with the multiscale information. This information is
not complete. So there will be approximations and assumption in order for the
story to stay together. The link between mimicking the brain circuit and the
human behavior is made making a lot of models and a lot of assumptions.

# Page 19

## Slide 1

## Approaches to neurorobotic design

From neurophysiology we can build computational models (Today's lesson until
now). These computational models can be used either for simulation or for tests
on real robots. This embodiment (virtual or real) can result into reproduced
behavior. This reproduced behavior needs to be compared with the other part of
the story (e.g. biomechanics, kinematics, the information about behavioral
movement).

- Bottom-up approach: From neurophysiology to kinematics.
- Top-down approach: From kinematics to neurophysiology. From a feature of the
	behavior that we want to capture, we develop a neural network that's able to
	simulate the feature that we want to describe, as complex as we want, and
	then we can try to understand how the structure of that network can be
	interpreted, connecting it to the structure of the brain which is known to
	control the specific behavior . i.e. vestibulo-ocular reflex with a neural
	network that can recognize afterwards the role of the thalamus, the
	cerebellum and so on providing hints to the structure of the brain which
	underpins that behavior.

This scheme can be run either clockwise or counterclockwise. Both the
approaches are very important as they can use design and validation data on
both sides (kinematics and neurophysiology).

The so-called **data driven spiking NN** are spiking NN that starts from
neurophysiology, builds a computational models, and then are used in-silico or
on real robots, then they are used to reproduce tasks and then task is compared
to human behavior. Data driven spiking NN deals with the *clockwise direction
of the loop*. This is the particular approach we are considering right now.

# Page 20

## Slide 1

### From single neuron to microcircuit

The first brick are the single neurons models (first part of the lecture), then
we need to put these models in a scaffold or microcircuit.

Microcircuits need to mimick the structure of the brain that we are modelling.

There are some features we need to take into account:

1. *The number of neurons* so the *rates between neuron density*. We are not
	 able to mimick the numbers of the cerebellum but we must keep the rates
	 between different kind of cells.
2. *Connections*: The directions of the connections, if they are excitatory or
	 inhibitory, but also the convergence or divergence of the connectivity.
	 Usually a mossy fibers connects 10 times granule cells and only 2 granule
	 cells. The amplitude of the connections need to be scaled on the
	 electrophysiological data. Also density of the connections.
3. Orientation: geometrical properties of the cells. Mimicking the physical
	 structure: cells are given a spacial position (e.g. a cube) and put the cell
	 depending on the number, the rate of connectivity and orienting them in the
	 space mimicking the structures that we are modeling.

The difference between microcircuit and scaffold:

- If we consider just the first 2 properties: microcircuit
- If we consider just the whole 3 properties: scaffold

How does this microcircuit learn? how do we implement the training?

The same that happens in ANN (weight training) is happening for Data Driven
spiking NN. We need to have models for the weights to be changed. This is model
of synapses/plasticity. This model need to be as much as possible similar to
what we know about the plasticity rule in the brain structure that we want to
mimick.

## Slide 2

We need a model of synapses/plasticity.

# Page 21

## Slide 1

## Model of PF-PC plasticity

We have previously mentioned the work between the IO and the Granule cells in
terms of mutual learning properties. We might remember that when the inferior
olive spikes there was a spike time dependent plasticity (all the granule cells
spiking right before IO spikes, those granule cells were strengthen in their
connection to the Purkinje cells. The weight of those connection was changed
depending on the occurrence of the inferior olive spiking (aka climbing fiber
spiking)). We need to translate this knowledge into equations to be put inside
the model, otherwise they can't be taken by the microcircuit model.

It's reported just an example of this equation of plasticity.

Time zero is IO-SP: Inferior olive spike (aka climbing fiber spiking). The
variation of the weight is the variation of the weight between the parallel
fiber and the purkinje cell.

Purkirje cell is receiving both parallel fibers and IO through the climbing
fiber. Depending on when the io is spiking, the weight that is changed is the
one related to PF and PC. This weight is changed depending on the *kernel
function* which is the curve reported *on the right graph* and it is integrated
in time and modulated through $h(t)_{PF_i}$. This term just mentioned is equal
to one when the i-th parallel fiber is active, so the more is changed the
weight, the more timing this parallel fiber is acting inside this kernel shape.
If it was active soon earlier than the inferior olive spike there is a very
high weight, while if it was active way before the spiking of IO then the
change of the weight is very low.  If the parallel fiber is not simultaneously
activated with the IO, its weight is increasing.

So the weight is depressed if the parallel fiber which is running a signal just
before the IO is silenced (reduced in its weight) while those who are
uncorrelated from the IO keep growing their importance. it's a silencing
mechanism.

The way plasticity occur in the structure of the cerebellum is that the
occurrence of the complex spike changes the importance of the connections
between PF and PC in a spike time dependent paradigm. Let's suppose to have
more PF (PF1 and PF2). PF1 spikes way before wrt IO spike and then it's more
silent. PF2 is spiking nearer to when the IO spikes (always before of IO, of
course). Let's make a meaningful understanding. Inferior Olive is informing the
Purkinje Cell of an the occurring error. the information is "the purkinje cell
has produced an error in this moment". The purkinje cell needs to *learn not to
repeat the error*. So, in order not to repeat the error, the parallel fiber
that was activating the PC right before the IO-SP, need to be *silenced*, we
don't care about the others. The kernel tells where the time interval where the
importance of the parallel fiber activation is wrt time zero. We are depressing
the parallel fiber which is *causing* the behavior that was resulting in the
error.  Causing means to see the relationship we have considered in the last
lesson.

## Slide 2

We have a *data driven biologically inspired computational model* which can be
used as the controller of a robot. A controller which is based on
neurophysiological data. The point is how to interface sensor and actuator and
when we can challenge that network in order to behave in a certain way. What we
want to do is to link this model to information about behavior. How can this
model express a behavior properties. Because we have seen that the definition
on neorurobotic deals with the capability of bridging a gap between information
on neurophysiology inside the model and information on behavioral features that
comes from the body expression of the model. So the robot has to reason based
on the biological-inspired computational models, needs to have sensors and
activate a movement by actuators. We need to have encoding and decoding
strategies in order for the model to be fed of the information coming from the
sensor and to give output to the actuators for the movement execution.  Then,
and this is an important point, the kind of task we want to interrogate the
robot and the controller for the execution of the task are to be defined: we
will not be able to test the robot in whatever environment and whatever task
but we need to design specific test protocol which are supposed to be capable
to interrogate specifically the controller we have developed.

# Page 22

## Slide 1

A cerebellar spiking neural network is the starting point.

Legend:

- GRs: granule cells
- MFs: mossy fibers
- PCs: purkinje cells
- IOs: inferior olive
- DCNs: deep cerebellar nuclei

The numbers of cells mimicks the rates of the population inside a real mice
cerebellum. The rules for connectivity are mimicking the density and the role
of connectivity as known by neurophysiologists. Then there's plasticity and it
has to be designed with specific mathematical rules that models the
neurophysiologial knowledge that we have.

## Slide 2

## A first example of protocol: EBCC (Eye-Blinking classical conditioning)

The next step: we have to define the protocol for testing the cerebellar
properties is based on the capability of interrogating specifically the
properties of the cerebellar spiking neural network. We need to link the
protocol to the cerebellar structures and in order to do this we need to base
our design on all what is known about the cerebellar functions and the role of
the cerebellum. As we mentioned, the cerebellum has an important role inside
motor control and, specifically, inside the motor learning and, in order to
test whether the in silico-cerebellum (computational model) is capable to
mimick the same behavior that we know the cerebellum is doing, we need to
define the protocols. Where can we derive the protocols from? The idea is to
design this protocol basing them on what is used in real clinical practise
(e.g. in applied neurology) in order to test patients who are suspected to have
problems in the cerebellum. We know that people without or with damaged
cerebellum are not capable to perform this protocol. These protocols are
*clinical protocols* (e.g. defined from a clinical perspective) and are
specifically used in clinics in order to test the cerebellar physiology (if
it's working or not). Thus, we want to translate these same tests for our robot
which is controlled by the in-silico cerebellum network.  The first protocol is
the **eye-blinking classical conditioning EBCC**.  In clinics (real context),
there are multiple clinical protocols that can be designed for EBCC. The most
used is the one using two stimuli: a tone and an air puff. The air puff is
directed in front of the eye of the subject. There's an inter-stimulus distance
which is fixed, so everytime we have a tone, after 200-300ms, the air puff is
produced. This air puff makes the patient blink. If we propose these two
stimuli associated for a certain time of repetition to any healthy control
without a conscious knowledge, the cerebellum of the healthy person learns to
associate the two stimuli and anticipate the eye-blinking because the air puff
is fastidious (e.g. the physiological patient learns to "defend" himself from
the fastidious stimuli). By being exposed to the repetition of these sequence
of two stimuli, the cerebellum learns to anticipate the air puff and to close
the eye just before the air puff. This property of predicting the stimuli is
developed by the cerebellum and it's not something that comes consciously.
People having cerebellar disfunctioning are not capable to do this association
and to produce the conditioned response which is the anticipated eye-blinking.
After the repetition of the two stimuli, the cerebellum learns to provide a
conditioned response (e.g. the anticipation of eye-blinking with respect to the
air puff). We know that the information about the auditory tone is carried to
the cerebellum through the mossy fiber (this is called conditioning stimulus),
on the other side the air puff information is conveyed through the inferior
olive to the climbing fiber (it's a kind of error so a signal we want to
avoid). After the learning process, inside the cerebellum, the cerebellum
learns to produce a conditioned response which is a prediction of the air puff
and the eye-blinking occurs just before the air puff. This is the most used
paradigm for the cerebellar pathology testing in clinics and we want to
translate this protocol inside our robot framework. Thus there's the need to
interrogate the microcircuit of the in-silico cerebellum in order to see where
this learning properties can emerge. To do this we need a robot GOTO Page 23,
slide 2.

# Page 23


## Slide 2

Let's consider the NAO. We have a protocol, a microcircuit made by 6480
neurons, plasticity rules inside the microcircuits and parameters for
optimizing the network. The represented network is the one implemented in the
robot. We have embedded also two other sides of plasticity. The cortical
plasticity is the one we have already described in detail while te two nuclear
plasticities are included because they are known from neurophysiologists (MF
--- DCN and PF --- DCN). 

Let's talk now about the implemented protocol. This protocol is considering two
stimulus, like the clinical one but they are translated in the robot domain. To
make a comparison between the clinical protocol and the in-silico protocol
let's look at the following table.

|                      | in-silico (robotic embodiment) | clinical     |
| :---                 | :---                           | :---         |
| Conditioned stim.    | Tone                           | Tone         |
| Unconditioned stim.  | Laser beam                     | air puff     |
| Conditioned response | Shield protection              | eye-blinking |

The robot is exposed to the association of the two stimuli (a tone and a laser
beam). This association has a specific inter-stimulus time interval and learns
to protect himself from the laser beam by moving the shield just before the
laser beam arrival. If the in-silico cerebellum plasticity is mimicking the
real cerebellum, we expect the robot to learn trial after trial to change the
connectivity inside the network (thanks to the model of the plasticity) and we
expect an emerging behavior which is the capability to predict the laser beam
by moving the protective shield. We are **not** giving the robot a rule of this
kind (aka we are not expressing the behavior in a coding): measure the time
interval between the tone and the laser beam occurrence and then move the
shield accordingly to protect from the laser beam. We are only creating the
spiking neural network and the behavior emerges from the plasticity rules
between the different cells of the in silico controller.

The learning phase is also known as acquisition phase of the protocol.

The robot is learning, changing the weights of the network according to
plasticity, in a way that is similar to the one of the patient with a
functioning cerebellum to protect itself from the unconditioned stimulus
through the conditioned response. this task needs time to be learned,
repetition after repetition.  

Let's analyze inputs and outputs:

1. *Laser beam binary information is conveyed (e.g. encoding the error
	 occurrence) through the in-silico climbing fibers as it's an unconditioned
	 stimulus*. The information conveyed by the climbing fiber is always
	 interpreted as an error and must be inhibited.
2. *Tone binary information is conveyed (e.g. encoding the timing information)
	 through the in-silico mossy fiber* as it's a conditioned stimulus.
3. The plasticity between mossy fiber and purkinje cell defines the output to
	 the DCN
4. Output of DCN is decoded in terms of conditioned response (which is
	 protecting). If DCN changes the robot is moving the shield, otherwise it
	 does nothing.
5. The plasticity between PF and PC is changed over time by the occurrence of
	 the inferior olive. This occurrence is the laser beam.
	 
The laser beam occurrence (e.g. the error) carried to the IO changes the output
of the PC by modulating the plasticity between the PF and PC. We are exactly
using the network we were describing in the past without giving the robot any
formal rule.

One of the major feature about motor learning is that we are also capable to
forget, dislearn, as long as the condition of the learning are no more present
→ *extintion phase*. The capability of acquiring a behavior is also studied by
the capability of canceling when the ability is no longer requested.  If the
tone is not anticipating the laser beam then the robot that has already learnt
the motor response at first is reacting by protecting itself but then realizes
that there's no need to do that and "forgets" the behavior. The ability of
acquiring and forgetting are all inside the testing of the physiological
functioning of the cerebellum and there are also timing that caracterize this
phases (acquisition is slow, extintion needs time but it's faster).
Acquisition and extintion both need time. 

If, after an extintion the already learnt behavior is shown again, the next
acquisition will be faster, as well as the next extintion.

## A second example of protocol: vestibulo ocular reflex

A second protocol is the one regarding regarding the vestibulo-ocular reflex.
This is another protocol for cerebellar pathology testing. It consists in the
ability to keep the gaze fixed when we turn our head: if we want to stare the
screen and turning the head, I have an immediate reflex between the information
of vestibulus (about head rotation) and the information about the eye position.
There's a compensatory counter rotation of the eyebulbs in order to keep the
gaze stable. This capability is damaged in people with cerebellar pathologies.
This is a clinical protocol for testing cerebellar pathologies. 

In this case we can state that:

- Mossy fibers is receiving the vestibular input about the head rotation.
- Climbing fibers is receiving the retinal error of the gaze stability.
- The (conditioned) learnt response is constituted by oculomotor compensatory
	command.

It's not here shown a protocol embedded into robots.

## A third protocol: reaching task inside a force field

A third protocol is based on a reaching task inside a force field. Let's
suppose we need to transport a toy boat from one side to the other of a little
river and there's the water current. If I don't know there's the current in the
river, we would be dragged by the current (error with respect to the straight
trajectory) but, after a while, without a real consciousness I am starting to
anticipate the known perturbation we will be expiriencing, and going straight
directly. Thus, by doing this protocol with a patient we would have a double
concurrent action.

1. Initial error compensated by feedback controller. Thus if the patient sees a
	 discrepancy between trajectory and target, the patient tries to compensate
	 it.
2. After repetitions, the patient will be able to automatically compensate for
	 the current right from the beginning without an initial error (e.g.
	 prediction on the information of the force field).

We can state that:

- Mossy fibers is carrying the target (straight) trajectory.
- Climbing fibers are informed about the angular error (e.g. effect of the
	force field).
- Learnt response is constituted by the prediction of a corrective force. 

Plasticity rules inside the in-silico cerebellum are accounting for the
learning features that emerges.

The fundamental concept is that learning features in these neurorobots, emerge
from plasticity change inside the spiking neural network (SNN).  

# Page 24

## Slide 2

## Third protocol robot embodiment

The robot is NAO. We are controlling 3 degrees of freedom. Protocol is more
complex and robot embodiment must be complex as well.  We have a multi-joints
force field.

There are 3 baselines (e.g. starting condition without perturbation). There are
30 trials for learning (stable perturbation, happening in the same way for
every trial).

There are 3 degrees of freedom: 

1. DOF1: Shoulder elevation.
1. DOF1: Shoulder horizontal rotation.
1. DOF1: Elbow flexion.

The reported graph in the slide is showing the target trajectory (solid lines).
The perturbation creates an error inside each of the trajectories of the
joints. Each color is relative to a specific joint.

# Page 25

## Slide 1

In this graph, let's analyze all the windows (from left to right):

1. It is reported the baseline acquisition of the trajectories without
	 perturbation. 
2. There's acquisition in which the force field is switched on. There is
	 perturbation and trials are used to reduce the errors. Time is required in
	 order to reduce the error. The correction is performed in the three DOF
	 because now the protocol is more complex.
3. Extintion: perturbation ends and then the robot is asked to go back to the
	 starting situation because the robot is no longer perturbed. In this phase,
	 initially, the robot will be compensating but it needs to forget to
	 compensate for force field which is no longer present.

## Slide 2

This slide shows a video in which NAO is reproducing the step just described at
the previous slide. First it's instructed with the trajectory without the force
field, then it's "acquiring" the compensation and in the end it's
"extinguishing".

1. Baseline: The robot is doing a circular trajectory. It's not performing that
	 perfectly because there are internal frictions that makes the task good but
	 not perfect.
2. Perturbation: simulating a force field of 10 kg applied to the end point of
	 the robot. We can see that there's an error which is time by time
	 compensated. We can see the error getting lower and lower, trying to get
	 closer to the desired trajectory. The end point trajectory after training is
	 really similar to the desired one.
3. Extintion: as soon as we take off the load (e.g. removing the force field),
	 there's an overcompensation. the robot is still using the learnt behavior
	 but after a few trial it will go back to the baseline situation.
	 
Nothing is perfect with the robot because since we do a "ideal" embodiment and
controlling just 3 DOF, because of friction. Now robot would be able to follow
the desired circular trajectory if we activated all the motors and use a fully
compensated way. Using only 3 DOF the target trajectory is not perfectly
accomplished but that's not the problem in this kind of tests. GOTO Page 19,
slide 2

# Page 19

## Slide 2

We have taken our network which is a computational model built on
neurophysiology. We have defined a protocol and embedded it into a real robot
and we have tested that the reproduced behavior is the expected one. We have
then shown the running of this loop in a clockwise direction.

We have linked information derived, on one side, from the neurophysiology for
the design of the neurons, plasticity rules, the connectivity of the
microcircuits, with the emerging behavior as expected actions derived on
specific testing protocols designed on clinical paradigm for cerebellar
patients.

# Page 26

## Slide 1

## The limits

What are the limits? Modeling neurons is a high computational task as we have
seen: from a computational load, if we want to mimick a microcircuit, it's
something that challenges the computational capability. When we embed a SNN
with neuronal models inside a robot, we are imposing another very strong
constrain to our test, which is about real time. The controller needs to
process the sensory input and produce the actuation within a real time
condition because otherwise the robot is not reacting properly in the task.
This condition of real time is limiting the capability to embed inside the
controller more and more properties so as soon as we had that we want to
increase the computational load of brain simulator, which means that we want to
capture more feature and have bigger microcircuit and define more plasticity
rules (e.g. when we want to improve our simulator), there's a strong limit in
the capability to interface this simulator with the real robots and respecting
real time which is a mandatory constraint for real robot tests.  

A solution in order to improve the capability to give a body to embody brain
simulation has been proposed by the Human Brain Project and is now very used:
Neurorobotics platforms: it's an infrastructure of HBP (that is providing open
infrastructure and available to the whole scientific community). This platform
is a software infrastructure where robots have been modeled (Virtual robots).
if we embody the brain simulator inside the virtual robot we can skip and avoid
the condition of real time because we can scale 1ms into 1s without issues and
letting the robot fall down in between. Virtual robots can embed more complex
brain simulator, can use the physics of the virtual robots in order to give a
body to the brain controllers and can be used to design experimental pipelines
(e.g. testing protocols). It's another direction where the goal is to have more
complex brain simulator and embodied into physics within an experiment pipeline
avoiding the constrain of the real time.

## Slide 2

## Why developing computational models of the brain and give them a body

What are the reasons for all these effort of developing computational models of
the brain and to give a body to computational models of the brain?

There are *multiple long term goals* of these studies that are both on 

1. Basic science impact, 
2. Applied neuroscience (comprehension of neuropathologies that leads to
	 improving the care for this kind of patients), 
3. Impact on robotics


1. *Impacts on neuroscience*: when we build a model we need to take some
	 assumption and see some behavior. This behavior of the brain simulator can
	 drive new questions to neuroscience which can help to design new experiment
	 and to deepen the understanding of the brain. One nice example of this
	 parallelism of neuroengineering approaches and experimental in basic
	 neurosciences is a theses that has been just discussed by our collegue
	 Francesco that has worked with Neuroscientists of University of Pavia
	 building a co-design of a protocol in-vivo (with mices) and in-silico inside
	 the neurorobotic platform. in this case, there's inscopix (which is a
	 microscope mounted inside the head of the mice that can record a certain
	 activity of the neural population), then there's a protocol for the mice:
	 they needs to wait on a bar and his whiskers are then touched either on the
	 left or right side. The information of the touch on the whiskers is learnt
	 from the mice so that he moves his paws to retrieve a water drop coming
	 either from the left or right side of two tubes. He needs to do this in a
	 very specific time window, otherwise the waterdrop is taken away. This
	 protocol is used to study the network activity inside the brain of the mice
	 and to study this learning of an association to blinking-protocol (we can
	 notice the similarity of this protocol with EBCC), it has directional
	 information and a double reaching possibility depending on the stimulus on
	 the whiskers. In order to design such complex protocol you need to build the
	 whole hardware structure: sensors on the bar (to check the waiting
	 position). this information needs to be perfectly synchronized with the
	 recording of the Inscopix. We also need to control whether the left or the
	 right bar is moved. We then need to provide with a proper pump the water
	 drop either on the left or the right. The water drop needs to be sucked as
	 soon as the time window has expired. Designing this protocol in a very well
	 and controlled situation needs a lot of electronics hardware design,
	 actuators, synchronized acquisition and interfaces for the experimenter
	 in order to control all the parameters of the protocol. On the other side,
	 the same protocol has been designed inside the neurorobotic plarform of HBP
	 with a very detailed model of icab.  This need to stand on the bar and when
	 it receives the input on its left or right shoulder about the direction, the
	 drop is provided by a tube coming down from the ceiling (one of the two).
	 The icab needs to reach this drop and receives a reward.  In the case of the
	 mice what happens is that during the training the mice is deprived of water
	 (as far as compatible with ethics), so to be very thirsty.  In this way the
	 mice are very concentrated in order to learn how to get the water drop. As
	 soon as it gets the water drop it can drink. The overall learning is
	 reinforced by the experience of a reward. The same is mimicked in the icab
	 neurorobotic platform with a reward information and sent to the structure of
	 the robot brain. There's a parallelism between the mice protocol and the
	 robotic protocol. We want to record the running neuronal activity inside the
	 mice brain and to develop a brain simulator inside the robot capable of
	 learning the same behavior.  We can see that this approach can be run in
	 parallel: neurorobotics can be run in parallel with experimental
	 neuroscience and then brain simulator can be counter-validated with respect
	 to recordings inside the experiments and models inside the neurorobotics
	 platforms. This parallelism is expected to allow a lot of deeper
	 understanding about the processing of the information inside the brain. This
	 is because on the one side we can record only very few neurons inside the
	 brain of the mice and we are not capable to have an healthy mouse with many
	 implants of arrays of electrodes in the brain so to fully monitor its brain,
	 on the other side we can fully know what is occurring inside the brain
	 simulator of the robot when the same protocol is learned. We can use this
	 parallelism to understand better the recording that we are doing inside the
	 real brain and the brain simulator. This impact is very important for making
	 a real step forward in the knowledge about the brain. This has been
	 recognized by the scientific community as one of the key directions for
	 deepening the understanding of the brain.

# Page 27

## Slide 1
## Slide 2

2. *Applied neuroscience*: the knowledge of the brain has the eventual goal in a
	 better carring of people having neurological problem, so better care to
	 perform. How can we use computational neuroscience (in-silico models) in
	 order to treat pathologies? In the paper on the left part of the slide: we
	 considr two populations: one healthy control and the other with cerebellar
	 cortical degeneration. Cortical degeneration of cerebellum: damage in the
	 purkinje cells (the cortical neurons). CR (conditioned response incedence,
	 e.g. learning) of eye-blinking activity, same protocol of EBCC. We can see
	 that healthy control produces a conditioned response up to 60% showing
	 learning ability (white dots).  on x-axis → blocks = trials. Black dots,
	 patient with desease, don't learn anything. Thus, they don't improve their
	 response trial after trial. There are two different quantified parameters
	 that distinguish patient with respect to healthy control:
	 
	 	1. Onset of CR.
		2. Peak time of CR.

	 They are all about the CR timing. The eye-blinking occurrence, when it
	 occurs, it's characterized by a very early onset in healthy control (150ms
	 before the air puff) and it has a very short peak time. On the other hand,
	 even if occurring very rarely (10% for patient), its occurrence is later in
	 time and more spread. So we have data	about where is pathology and what are
	 the characteristics of the behavior of the patient. Now we can take the
	 (in-silico) computational model and apply the cerebellar cortical
	 degeneration and we can check how the in-silico network can reproduce the
	 behavior. We can see, in the results (on the right), by changing the level
	 of PC lost (cortical cerebellar degeneration) with 16-20% of PC lost, the
	 model can mimick the percentage of the conditioned response of the patient.
	 On the other side, the healthy model is mimicking the data of the healthy
	 control in the experiments. The same can be observed in the latency
	 parameters (timing parameters of the CR). Now what happens is that we know
	 the damage and the behavior, we can reproduce with the in-silico cerebellum
	 both the damage and the final behavior of the patient. The new window that
	 we can open is that we can look inside the brain simulator we have and see
	 how the different processes between the population of the neurons have been
	 affected by the pathology and achieving the target behavior. we have the
	 possibility of a real insight about the brain structures producing that
	 behavior in the damaged situation. This insight is fully impossible in the
	 patient: we just know that there's damage with neuroimaging but we can't go
	 inside the cerebellum and see what each cell population is doing. We simply
	 don't have the capability of recording the activity of each single
	 population inside the brain during patients being tested. This is different
	 for models because they are not "alive" but they have a similar behavior.
	 There's a huge output of this approaches in comprehension of these
	 pathologies and as soon as we comprehend better the pathology we can try to
	 target better the cure and the treatments because for example if we
	 understand that besides the PC damage there's a compensatory mechanism which
	 is not capable to fully compensate but a tentative compensation between MF
	 and DCN plasticity rules we can target specific drugs in order to promote
	 the neurotransmitter which assures a better functioning of that connections.
	 We can tailor the treatment to what the brain simulation is informing us.
	 This is the expectation of all the neuroscience: to impact real patients
	 life. This is the final goal. 
3. *Impact on robotics*: The way the information is carried and processed inside
	 the brain is extremely interesting for advancement of design of robotics. We
	 are at a point where we clearly understand that animal brain is capable of
	 doing some task much better wrt any robotic system. If we ask a kid to
	 recognize his grandmother after she went to the hairdresser, you will never
	 find any kid not capable of performing this operation. You don't have any
	 image processing capable to recognize that people or we are putting a lot of
	 effort in developing image processing that are capable to catch up with this
	 kind of feature which is very efficient in the brain. This knowledge in the
	 brain is something that can inspire the design of new computation strategies
	 and also robotic controls expecially driven to those tasks where humans are
	 super efficient while the standard computer science digital programming and
	 regular robotics design usually fail. There are a number of tasks where
	 still the humans outperforms with respect to robots.
	 
## References

Slides: "computational neuroscience 1", "computational neuroscience 2"
