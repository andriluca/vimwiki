# Lecture 2 --- Neurorobotics

# Page 18

## Slide 1

## Neurorobotics

Robot: a machine capable to move with actuators. It needs to have at least few
sensors in order to gather information from the environment. Such information
coming from the sensor(s) are used by a controller in order to regulate and
control the activation of the actuator.

Three basic elements are:

1. Sensors
2. Controller
3. Actuators

How many of these elements respond on how complex the robot is. It depends on
the number of degrees of freedom and the information to be integrated.

Other necessary element for a robot is:

4. The body

Whatever simulator can simulate sensors, controllers and actuators but if we
want this simulator to be a robot it needs to have a body. This body can be, as
well, as complex as we want with as much degrees of freedom we want. Depending
on the characteristic of the body, the sensors, the actuators and the
controller are changed accordingly.

Then usually a robot has a *set of tasks*. The flexibility and the capability
to interact with multiple tools and very different environments is one of the
hallmarks of human species with respect to the other animals. The same is valid
for robot: usually robot are very good at performing specific tasks (i.e.
mounting the doors of the cars, painting the doors, mounting the front part,
etc). There are set of tasks on which robot design is tailored. Even the
humanoid robots do have a set of tasks (walk, sit down, dance).

The *way the robot is interacting with the environment* is mediated by the
characteristics of the environment. Environments is linked to *tasks*.
Environment is giving information to sensors and receiving the movement from
the actuator. Environment is giving informations to sensors and receiving the
movement from the actuators. It can be seen similarly to what we have modeled
when we were talking about inverse and forward model. The interaction between
body and environment is movement and, on one side it's commanded by the
actuator set and on the other sensed by the sensorial components.

## Slide 2

If we want to talk about neurorobotics this is a very specific part of robotics
which deals with the aim of mimicking the neural mechanism underpinning the
behavior of humans.

- Mimicking the behavior. This is biomechanical information
- Neurorobot should mimick the structures of the brain which controls the task
	execution.

What is the issue? The fact that we are dealing with two completely different
scales: on one side we have the biomechanical information which, for example
are the trajectories of the joints or the activation of the muscles.  For
instance we could have motion captions or inertial sensors, EMG data (images on
the bottom). We could also have information about which areas are active inside
the neuroimages. In between these two information there's a gap. if we want to
go deeper we could also use patch clamp to have single cell information or
local field potentials to see the behavior of group of cells. So we have a lot
of information in this story but this information is not automatically bonded
together because each of them has a different scale, a different level of
detail and passing from one level to the other is something that is not
immediate and automatic.  If we want to use a controller which mimicks the
brain circuits this means that we want to take the information from the small
scale, so neural information (Patch clamp and local field potential), on the
other way around, if it should mimick the behavior we should consider the
information on the other side (motion caption ad stuff like that). So somehow a
neurorobot needs to deal with the multiscale information. This information is
not complete. So there will be approximations and assumption in order for the
story to stay together. The link between mimicking the brain circuit and the
human behavior is made making a lot of models and a lot of assumptions.

# Page 19

## Slide 1

## Approaches to neurorobotic design

From neurophysiology we can build computational models (Today's lesson until
now). These computational models can be used either for simulation or for tests
on real robots. This embodiment (virtual or real) can result into reproduced
behavior. This reproduced behavior needs to be compared with the other part of
the story (e.g. biomechanics, kinematics, the information about behavioral
movement).

- Bottom-up approach: From neurophysiology to kinematics.
- Top-down approach: From kinematics to neurophysiology. From a feature of the
	behavior that we want to capture, we develop a neural network that's able to
	simulate the feature that we want to describe, as complex as we want, and
	then we can try to understand how the structure of that network can be
	interpreted, connecting it to the structure of the brain which is known to
	control the specific behavior . i.e. vestibulo-ocular reflex with a neural
	network that can recognize afterwards the role of the thalamus, the
	cerebellum and so on providing hints to the structure of the brain which
	underpins that behavior.

This scheme can be run either clockwise or counterclockwise. Both the
approaches are very important as they can use design and validation data on
both sides (kinematics and neurophysiology).

The so-called **data driven spiking NN** are spiking NN that starts from
neurophysiology, builds a computational models, and then are used in-silico or
on real robots, then they are used to reproduce tasks and then task is compared
to human behavior. Data driven spiking NN deals with the *clockwise direction
of the loop*. This is the particular approach we are considering right now.

# Page 20

## Slide 1

### From single neuron to microcircuit

The first brick are the single neurons models (first part of the lecture), then
we need to put these models in a scaffold or microcircuit.

Microcircuits need to mimick the structure of the brain that we are modelling.

There are some features we need to take into account:

1. *The number of neurons* so the *rates between neuron density*. We are not
	 able to mimick the numbers of the cerebellum but we must keep the rates
	 between different kind of cells.
2. *Connections*: The directions of the connections, if they are excitatory or
	 inhibitory, but also the convergence or divergence of the connectivity.
	 Usually a mossy fibers connects 10 times granule cells and only 2 granule
	 cells. The amplitude of the connections need to be scaled on the
	 electrophysiological data. Also density of the connections.
3. Orientation: geometrical properties of the cells. Mimicking the physical
	 structure: cells are given a spacial position (e.g. a cube) and put the cell
	 depending on the number, the rate of connectivity and orienting them in the
	 space mimicking the structures that we are modeling.

The difference between microcircuit and scaffold:

- If we consider just the first 2 properties: microcircuit
- If we consider just the whole 3 properties: scaffold

How does this microcircuit learn? how do we implement the training?

The same that happens in ANN (weight training) is happening for Data Driven
spiking NN. We need to have models for the weights to be changed. This is model
of synapses/plasticity. This model need to be as much as possible similar to
what we know about the plasticity rule in the brain structure that we want to
mimick.

## Slide 2

We need a model of synapses/plasticity.

# Page 21

## Slide 1

## Model of PF-PC plasticity

We have previously mentioned the work between the IO and the Granule cells in
terms of mutual learning properties. We might remember that when the inferior
olive spikes there was a spike time dependent plasticity (all the granule cells
spiking right before IO spikes, those granule cells were strengthen in their
connection to the Purkinje cells. The weight of those connection was changed
depending on the occurrence of the inferior olive spiking (aka climbing fiber
spiking)). We need to translate this knowledge into equations to be put inside
the model, otherwise they can't be taken by the microcircuit model.

It's reported just an example of this equation of plasticity.

Time zero is IO-SP: Inferior olive spike (aka climbing fiber spiking). The
variation of the weight is the variation of the weight between the parallel
fiber and the purkinje cell.

Purkirje cell is receiving both parallel fibers and IO through the climbing
fiber. Depending on when the io is spiking, the weight that is changed is the
one related to PF and PC. This weight is changed depending on the *kernel
function* which is the curve reported *on the right graph* and it is integrated
in time and modulated through $h(t)_{PF_i}$. This term just mentioned is equal
to one when the i-th parallel fiber is active, so the more is changed the
weight, the more timing this parallel fiber is acting inside this kernel shape.
If it was active soon earlier than the inferior olive spike there is a very
high weight, while if it was active way before the spiking of IO then the
change of the weight is very low.  If the parallel fiber is not simultaneously
activated with the IO, its weight is increasing.

So the weight is depressed if the parallel fiber which is running a signal just
before the IO is silenced (reduced in its weight) while those who are
uncorrelated from the IO keep growing their importance. it's a silencing
mechanism.

The way plasticity occur in the structure of the cerebellum is that the
occurrence of the complex spike changes the importance of the connections
between PF and PC in a spike time dependent paradigm. Let's suppose to have
more PF (PF1 and PF2). PF1 spikes way before wrt IO spike and then it's more
silent. PF2 is spiking nearer to when the IO spikes (always before of IO, of
course). Let's make a meaningful understanding. Inferior Olive is informing the
Purkinje Cell of an the occurring error. the information is "the purkinje cell
has produced an error in this moment". The purkinje cell needs to *learn not to
repeat the error*. So, in order not to repeat the error, the parallel fiber
that was activating the PC right before the IO-SP, need to be *silenced*, we
don't care about the others. The kernel tells where the time interval where the
importance of the parallel fiber activation is wrt time zero. We are depressing
the parallel fiber which is *causing* the behavior that was resulting in the
error.  Causing means to see the relationship we have considered in the last
lesson.

## Slide 2

We have a *data driven biologically inspired computational model* which can be
used as the controller of a robot. A controller which is based on
neurophysiological data. The point is how to interface sensor and actuator and
when we can challenge that network in order to behave in a certain way. What we
want to do is to link this model to information about behavior. How can this
model express a behavior properties. Because we have seen that the definition
on neorurobotic deals with the capability of bridging a gap between information
on neurophysiology inside the model and information on behavioral features that
comes from the body expression of the model. So the robot has to reason based
on the biological-inspired computational models, needs to have sensors and
activate a movement by actuators. We need to have encoding and decoding
strategies in order for the model to be fed of the information coming from the
sensor and to give output to the actuators for the movement execution.  Then,
and this is an important point, the kind of task we want to interrogate the
robot and the controller for the execution of the task are to be defined: we
will not be able to test the robot in whatever environment and whatever task
but we need to design specific test protocol which are supposed to be capable
to interrogate specifically the controller we have developed.

# Page 22

## Slide 1

A cerebellar spiking neural network is the starting point.

Legend:

- GRs: granule cells
- MFs: mossy fibers
- PCs: purkinje cells
- IOs: inferior olive
- DCNs: deep cerebellar nuclei

The numbers of cells mimicks the rates of the population inside a real mice
cerebellum. The rules for connectivity are mimicking the density and the role
of connectivity as known by neurophysiologists. Then there's plasticity and it
has to be designed with specific mathematical rules that models the
neurophysiologial knowledge that we have.

## Slide 2

## A first example of protocol: EBCC (Eye-Blinking classical conditioning)

The next step: we have to define the protocol for testing the cerebellar
properties is based on the capability of interrogating specifically the
properties of the cerebellar spiking neural network. We need to link the
protocol to the cerebellar structures and in order to do this we need to base
our design on all what is known about the cerebellar functions and the role of
the cerebellum. As we mentioned, the cerebellum has an important role inside
motor control and, specifically, inside the motor learning and, in order to
test whether the in silico-cerebellum (computational model) is capable to
mimick the same behavior that we know the cerebellum is doing, we need to
define the protocols. Where can we derive the protocols from? The idea is to
design this protocol basing them on what is used in real clinical practise
(e.g. in applied neurology) in order to test patients who are suspected to have
problems in the cerebellum. We know that people without or with damaged
cerebellum are not capable to perform this protocol. These protocols are
*clinical protocols* (e.g. defined from a clinical perspective) and are
specifically used in clinics in order to test the cerebellar physiology (if
it's working or not). Thus, we want to translate these same tests for our robot
which is controlled by the in-silico cerebellum network.  The first protocol is
the **eye-blinking classical conditioning EBCC**.  In clinics (real context),
there are multiple clinical protocols that can be designed for EBCC. The most
used is the one using two stimuli: a tone and an air puff. The air puff is
directed in front of the eye of the subject. There's an inter-stimulus distance
which is fixed, so everytime we have a tone, after 200-300ms, the air puff is
produced. This air puff makes the patient blink. If we propose these two
stimuli associated for a certain time of repetition to any healthy control
without a conscious knowledge, the cerebellum of the healthy person learns to
associate the two stimuli and anticipate the eye-blinking because the air puff
is fastidious (e.g. the physiological patient learns to "defend" himself from
the fastidious stimuli). By being exposed to the repetition of these sequence
of two stimuli, the cerebellum learns to anticipate the air puff and to close
the eye just before the air puff. This property of predicting the stimuli is
developed by the cerebellum and it's not something that comes consciously.
People having cerebellar disfunctioning are not capable to do this association
and to produce the conditioned response which is the anticipated eye-blinking.
After the repetition of the two stimuli, the cerebellum learns to provide a
conditioned response (e.g. the anticipation of eye-blinking with respect to the
air puff). We know that the information about the auditory tone is carried to
the cerebellum through the mossy fiber (this is called conditioning stimulus),
on the other side the air puff information is conveyed through the inferior
olive to the climbing fiber (it's a kind of error so a signal we want to
avoid). After the learning process, inside the cerebellum, the cerebellum
learns to produce a conditioned response which is a prediction of the air puff
and the eye-blinking occurs just before the air puff. This is the most used
paradigm for the cerebellar pathology testing in clinics and we want to
translate this protocol inside our robot framework. Thus there's the need to
interrogate the microcircuit of the in-silico cerebellum in order to see where
this learning properties can emerge. To do this we need a robot GOTO Page 23,
slide 2.

# Page 23


## Slide 2

Let's consider the NAO. We have a protocol, a microcircuit made by 6480
neurons, plasticity rules inside the microcircuits and parameters for
optimizing the network. The represented network is the one implemented in the
robot. We have embedded also two other sides of plasticity. The cortical
plasticity is the one we have already described in detail while te two nuclear
plasticities are included because they are known from neurophysiologists (MF
--- DCN and PF --- DCN). 

Let's talk now about the implemented protocol. This protocol is considering two
stimulus, like the clinical one but they are translated in the robot domain. To
make a comparison between the clinical protocol and the in-silico protocol
let's look at the following table.

|                      | in-silico (robotic embodiment) | clinical     |
| :---                 | :---                           | :---         |
| Conditioned stim.    | Tone                           | Tone         |
| Unconditioned stim.  | Laser beam                     | air puff     |
| Conditioned response | Shield protection              | eye-blinking |

The robot is exposed to the association of the two stimuli (a tone and a laser
beam). This association has a specific inter-stimulus time interval and learns
to protect himself from the laser beam by moving the shield just before the
laser beam arrival. If the in-silico cerebellum plasticity is mimicking the
real cerebellum, we expect the robot to learn trial after trial to change the
connectivity inside the network (thanks to the model of the plasticity) and we
expect an emerging behavior which is the capability to predict the laser beam
by moving the protective shield. We are **not** giving the robot a rule of this
kind (aka we are not expressing the behavior in a coding): measure the time
interval between the tone and the laser beam occurrence and then move the
shield accordingly to protect from the laser beam. We are only creating the
spiking neural network and the behavior emerges from the plasticity rules
between the different cells of the in silico controller.

The learning phase is also known as acquisition phase of the protocol.

The robot is learning, changing the weights of the network according to
plasticity, in a way that is similar to the one of the patient with a
functioning cerebellum to protect itself from the unconditioned stimulus
through the conditioned response. this task needs time to be learned,
repetition after repetition.  

Let's analyze inputs and outputs:

1. *Laser beam binary information is conveyed (e.g. encoding the error
	 occurrence) through the in-silico climbing fibers as it's an unconditioned
	 stimulus*. The information conveyed by the climbing fiber is always
	 interpreted as an error and must be inhibited.
2. *Tone binary information is conveyed (e.g. encoding the timing information)
	 through the in-silico mossy fiber* as it's a conditioned stimulus.
3. The plasticity between mossy fiber and purkinje cell defines the output to
	 the DCN
4. Output of DCN is decoded in terms of conditioned response (which is
	 protecting). If DCN changes the robot is moving the shield, otherwise it
	 does nothing.
5. The plasticity between PF and PC is changed over time by the occurrence of
	 the inferior olive. This occurrence is the laser beam.
	 
The laser beam occurrence (e.g. the error) carried to the IO changes the output
of the PC by modulating the plasticity between the PF and PC. We are exactly
using the network we were describing in the past without giving the robot any
formal rule.

One of the major feature about motor learning is that we are also capable to
forget, dislearn, as long as the condition of the learning are no more present
→ *extintion phase*. The capability of acquiring a behavior is also studied by
the capability of canceling when the ability is no longer requested.  If the
tone is not anticipating the laser beam then the robot that has already learnt
the motor response at first is reacting by protecting itself but then realizes
that there's no need to do that and "forgets" the behavior. The ability of
acquiring and forgetting are all inside the testing of the physiological
functioning of the cerebellum and there are also timing that caracterize this
phases (acquisition is slow, extintion needs time but it's faster).
Acquisition and extintion both need time. 

If, after an extintion the already learnt behavior is shown again, the next
acquisition will be faster, as well as the next extintion.

## A second example of protocol: vestibulo ocular reflex

A second protocol is the one regarding regarding the vestibulo-ocular reflex.
This is another protocol for cerebellar pathology testing. It consists in the
ability to keep the gaze fixed when we turn our head: if we want to stare the
screen and turning the head, I have an immediate reflex between the information
of vestibulus (about head rotation) and the information about the eye position.
There's a compensatory counter rotation of the eyebulbs in order to keep the
gaze stable. This capability is damaged in people with cerebellar pathologies.
This is a clinical protocol for testing cerebellar pathologies. 

In this case we can state that:

- Mossy fibers is receiving the vestibular input about the head rotation.
- Climbing fibers is receiving the retinal error of the gaze stability.
- The (conditioned) learnt response is constituted by oculomotor compensatory
	command.

It's not here shown a protocol embedded into robots.

## A third protocol: reaching task inside a force field

A third protocol is based on a reaching task inside a force field. Let's
suppose we need to transport a toy boat from one side to the other of a little
river and there's the water current. If I don't know there's the current in the
river, we would be dragged by the current (error with respect to the straight
trajectory) but, after a while, without a real consciousness I am starting to
anticipate the known perturbation we will be expiriencing, and going straight
directly. Thus, by doing this protocol with a patient we would have a double
concurrent action.

1. Initial error compensated by feedback controller. Thus if the patient sees a
	 discrepancy between trajectory and target, the patient tries to compensate
	 it.
2. After repetitions, the patient will be able to automatically compensate for
	 the current right from the beginning without an initial error (e.g.
	 prediction on the information of the force field).

We can state that:

- Mossy fibers is carrying the target (straight) trajectory.
- Climbing fibers are informed about the angular error (e.g. effect of the
	force field).
- Learnt response is constituted by the prediction of a corrective force. 

Plasticity rules inside the in-silico cerebellum are accounting for the
learning features that emerges.

The fundamental concept is that learning features in these neurorobots, emerge
from plasticity change inside the spiking neural network (SNN).  

GOTO Page 24 Slide 2.

# Page 24

## Slide 2

## Third protocol robot embodiment

The robot is NAO. We are controlling 3 degrees of freedom. Protocol is more
complex and robot embodiment must be complex as well.  We have a multi-joints
force field.

There are 3 baselines (e.g. starting condition without perturbation). There are
30 trials for learning (stable perturbation, happening in the same way for
every trial).

There are 3 degrees of freedom: 

1. DOF1: Shoulder elevation.
1. DOF1: Shoulder horizontal rotation.
1. DOF1: Elbow flexion.

The reported graph in the slide is showing the target trajectory (solid lines).
The perturbation creates an error inside each of the trajectories of the
joints. Each color is relative to a specific joint.

# Page 25

## Slide 1

In this graph, let's analyze all the windows (from left to right):

1. It is reported the baseline acquisition of the trajectories without
	 perturbation. 
2. There's acquisition in which the force field is switched on. There is
	 perturbation and trials are used to reduce the errors. Time is required in
	 order to reduce the error. The correction is performed in the three DOF
	 because now the protocol is more complex.
3. Extintion: perturbation ends and then the robot is asked to go back to the
	 starting situation because the robot is no longer perturbed. In this phase,
	 initially, the robot will be compensating but it needs to forget to
	 compensate for force field which is no longer present.

## Slide 2

This slide shows a video in which NAO is reproducing the step just described at
the previous slide. First it's instructed with the trajectory without the force
field, then it's "acquiring" the compensation and in the end it's
"extinguishing".

1. Baseline: The robot is doing a circular trajectory. It's not performing that
	 perfectly because there are internal frictions that makes the task good but
	 not perfect.
2. Perturbation: simulating a force field of 10 kg applied to the end point of
	 the robot. We can see that there's an error which is time by time
	 compensated. We can see the error getting lower and lower, trying to get
	 closer to the desired trajectory. The end point trajectory after training is
	 really similar to the desired one.
3. Extintion: as soon as we take off the load (e.g. removing the force field),
	 there's an overcompensation. the robot is still using the learnt behavior
	 but after a few trial it will go back to the baseline situation.
	 
Nothing is perfect with the robot because since we do a "ideal" embodiment and
controlling just 3 DOF, because of friction. Now robot would be able to follow
the desired circular trajectory if we activated all the motors and use a fully
compensated way. Using only 3 DOF the target trajectory is not perfectly
accomplished but that's not the problem in this kind of tests. GOTO Page 19,
slide 2

# Page 19

## Slide 2

We have taken our network which is a computational model built on
neurophysiology. We have defined a protocol and embedded it into a real robot
and we have tested that the reproduced behavior is the expected one. We have
then shown the running of this loop in a clockwise direction.

We have linked information derived, on one side, from the neurophysiology for
the design of the neurons, plasticity rules, the connectivity of the
microcircuits, with the emerging behavior as expected actions derived on
specific testing protocols designed on clinical paradigm for cerebellar
patients.

# Page 26

## Slide 1

## The limits

What are the limits? Modeling neurons is a high computational task as we have
seen: from a computational load, if we want to mimick a microcircuit, it's
something that challenges the computational capability. When we embed a SNN
with neuronal models inside a robot, we are imposing another very strong
constrain to our test, which is about real time. The controller needs to
process the sensory input and produce the actuation within a real time
condition because otherwise the robot is not reacting properly in the task.
This condition of real time is limiting the capability to embed inside the
controller more and more properties so as soon as we had that we want to
increase the computational load of brain simulator, which means that we want to
capture more feature and have bigger microcircuit and define more plasticity
rules (e.g. when we want to improve our simulator), there's a strong limit in
the capability to interface this simulator with the real robots and respecting
real time which is a mandatory constraint for real robot tests.  

A solution in order to improve the capability to give a body to embody brain
simulation has been proposed by the Human Brain Project and is now very used:
Neurorobotics platforms: it's an infrastructure of HBP (that is providing open
infrastructure and available to the whole scientific community). This platform
is a software infrastructure where robots have been modeled (Virtual robots).
if we embody the brain simulator inside the virtual robot we can skip and avoid
the condition of real time because we can scale 1ms into 1s without issues and
letting the robot fall down in between. Virtual robots can embed more complex
brain simulator, can use the physics of the virtual robots in order to give a
body to the brain controllers and can be used to design experimental pipelines
(e.g. testing protocols). It's another direction where the goal is to have more
complex brain simulator and embodied into physics within an experiment pipeline
avoiding the constrain of the real time.

## Slide 2

## Why developing computational models of the brain and give them a body

What are the reasons for all these effort of developing computational models of
the brain and to give a body to computational models of the brain?

There are *multiple long term goals* of these studies that are both on 

1. Basic science impact, 
2. Applied neuroscience (comprehension of neuropathologies that leads to
	 improving the care for this kind of patients), 
3. Impact on robotics


1. *Impacts on neuroscience*: when we build a model we need to take some
	 assumption and see some behavior. This behavior of the brain simulator can
	 drive new questions to neuroscience which can help to design new experiment
	 and to deepen the understanding of the brain. One nice example of this
	 parallelism of neuroengineering approaches and experimental in basic
	 neurosciences is a theses that has been just discussed by our collegue
	 Francesco that has worked with Neuroscientists of University of Pavia
	 building a co-design of a protocol in-vivo (with mices) and in-silico inside
	 the neurorobotic platform. in this case, there's inscopix (which is a
	 microscope mounted inside the head of the mice that can record a certain
	 activity of the neural population), then there's a protocol for the mice:
	 they needs to wait on a bar and his whiskers are then touched either on the
	 left or right side. The information of the touch on the whiskers is learnt
	 from the mice so that he moves his paws to retrieve a water drop coming
	 either from the left or right side of two tubes. He needs to do this in a
	 very specific time window, otherwise the waterdrop is taken away. This
	 protocol is used to study the network activity inside the brain of the mice
	 and to study this learning of an association to blinking-protocol (we can
	 notice the similarity of this protocol with EBCC), it has directional
	 information and a double reaching possibility depending on the stimulus on
	 the whiskers. In order to design such complex protocol you need to build the
	 whole hardware structure: sensors on the bar (to check the waiting
	 position). this information needs to be perfectly synchronized with the
	 recording of the Inscopix. We also need to control whether the left or the
	 right bar is moved. We then need to provide with a proper pump the water
	 drop either on the left or the right. The water drop needs to be sucked as
	 soon as the time window has expired. Designing this protocol in a very well
	 and controlled situation needs a lot of electronics hardware design,
	 actuators, synchronized acquisition and interfaces for the experimenter
	 in order to control all the parameters of the protocol. On the other side,
	 the same protocol has been designed inside the neurorobotic plarform of HBP
	 with a very detailed model of icab.  This need to stand on the bar and when
	 it receives the input on its left or right shoulder about the direction, the
	 drop is provided by a tube coming down from the ceiling (one of the two).
	 The icab needs to reach this drop and receives a reward.  In the case of the
	 mice what happens is that during the training the mice is deprived of water
	 (as far as compatible with ethics), so to be very thirsty.  In this way the
	 mice are very concentrated in order to learn how to get the water drop. As
	 soon as it gets the water drop it can drink. The overall learning is
	 reinforced by the experience of a reward. The same is mimicked in the icab
	 neurorobotic platform with a reward information and sent to the structure of
	 the robot brain. There's a parallelism between the mice protocol and the
	 robotic protocol. We want to record the running neuronal activity inside the
	 mice brain and to develop a brain simulator inside the robot capable of
	 learning the same behavior.  We can see that this approach can be run in
	 parallel: neurorobotics can be run in parallel with experimental
	 neuroscience and then brain simulator can be counter-validated with respect
	 to recordings inside the experiments and models inside the neurorobotics
	 platforms. This parallelism is expected to allow a lot of deeper
	 understanding about the processing of the information inside the brain. This
	 is because on the one side we can record only very few neurons inside the
	 brain of the mice and we are not capable to have an healthy mouse with many
	 implants of arrays of electrodes in the brain so to fully monitor its brain,
	 on the other side we can fully know what is occurring inside the brain
	 simulator of the robot when the same protocol is learned. We can use this
	 parallelism to understand better the recording that we are doing inside the
	 real brain and the brain simulator. This impact is very important for making
	 a real step forward in the knowledge about the brain. This has been
	 recognized by the scientific community as one of the key directions for
	 deepening the understanding of the brain.

# Page 27

## Slide 1
## Slide 2

2. *Applied neuroscience*: the knowledge of the brain has the eventual goal in a
	 better carring of people having neurological problem, so better care to
	 perform. How can we use computational neuroscience (in-silico models) in
	 order to treat pathologies? In the paper on the left part of the slide: we
	 considr two populations: one healthy control and the other with cerebellar
	 cortical degeneration. Cortical degeneration of cerebellum: damage in the
	 purkinje cells (the cortical neurons). CR (conditioned response incedence,
	 e.g. learning) of eye-blinking activity, same protocol of EBCC. We can see
	 that healthy control produces a conditioned response up to 60% showing
	 learning ability (white dots).  on x-axis → blocks = trials. Black dots,
	 patient with desease, don't learn anything. Thus, they don't improve their
	 response trial after trial. There are two different quantified parameters
	 that distinguish patient with respect to healthy control:
	 
	 	1. Onset of CR.
		2. Peak time of CR.

	 They are all about the CR timing. The eye-blinking occurrence, when it
	 occurs, it's characterized by a very early onset in healthy control (150ms
	 before the air puff) and it has a very short peak time. On the other hand,
	 even if occurring very rarely (10% for patient), its occurrence is later in
	 time and more spread. So we have data	about where is pathology and what are
	 the characteristics of the behavior of the patient. Now we can take the
	 (in-silico) computational model and apply the cerebellar cortical
	 degeneration and we can check how the in-silico network can reproduce the
	 behavior. We can see, in the results (on the right), by changing the level
	 of PC lost (cortical cerebellar degeneration) with 16-20% of PC lost, the
	 model can mimick the percentage of the conditioned response of the patient.
	 On the other side, the healthy model is mimicking the data of the healthy
	 control in the experiments. The same can be observed in the latency
	 parameters (timing parameters of the CR). Now what happens is that we know
	 the damage and the behavior, we can reproduce with the in-silico cerebellum
	 both the damage and the final behavior of the patient. The new window that
	 we can open is that we can look inside the brain simulator we have and see
	 how the different processes between the population of the neurons have been
	 affected by the pathology and achieving the target behavior. we have the
	 possibility of a real insight about the brain structures producing that
	 behavior in the damaged situation. This insight is fully impossible in the
	 patient: we just know that there's damage with neuroimaging but we can't go
	 inside the cerebellum and see what each cell population is doing. We simply
	 don't have the capability of recording the activity of each single
	 population inside the brain during patients being tested. This is different
	 for models because they are not "alive" but they have a similar behavior.
	 There's a huge output of this approaches in comprehension of these
	 pathologies and as soon as we comprehend better the pathology we can try to
	 target better the cure and the treatments because for example if we
	 understand that besides the PC damage there's a compensatory mechanism which
	 is not capable to fully compensate but a tentative compensation between MF
	 and DCN plasticity rules we can target specific drugs in order to promote
	 the neurotransmitter which assures a better functioning of that connections.
	 We can tailor the treatment to what the brain simulation is informing us.
	 This is the expectation of all the neuroscience: to impact real patients
	 life. This is the final goal. 
3. *Impact on robotics*: The way the information is carried and processed inside
	 the brain is extremely interesting for advancement of design of robotics. We
	 are at a point where we clearly understand that animal brain is capable of
	 doing some task much better wrt any robotic system. If we ask a kid to
	 recognize his grandmother after she went to the hairdresser, you will never
	 find any kid not capable of performing this operation. You don't have any
	 image processing capable to recognize that people or we are putting a lot of
	 effort in developing image processing that are capable to catch up with this
	 kind of feature which is very efficient in the brain. This knowledge in the
	 brain is something that can inspire the design of new computation strategies
	 and also robotic controls expecially driven to those tasks where humans are
	 super efficient while the standard computer science digital programming and
	 regular robotics design usually fail. There are a number of tasks where
	 still the humans outperforms with respect to robots.
	 
## References

Slides: "computational neuroscience 2"
