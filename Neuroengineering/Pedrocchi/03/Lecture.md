# Lecture 3 --- RCT and Rehabilitation Robotics

## Page

### Slide 1

## Rationale of Design of experimental studies in rehabilitation

The topics of this lecture are outside neuroengineering but much more
transversal to a biomedical engineer preparation.  A point about design of
experimental studies is a crucial point that a biomedical engineer needs to
have and that's why we are having this lecture.  Especially because of the new
regulation that we are starting to have the next year (Medical Device
Regulation of the European Commission), the point about the clinical evidence
is becoming of paramount importance. The directors of Confindustria Medical
devices have expressed the challenges that they see and the expertise that they
want from the future engineers: the issue about regulatory and, specifically,
the design of clinical validation is one of the topics they highlight as the
most important.

It's worthy to be able to design a clinical trial and to make systematic
reviews of scientific literature because it's an important expertise that a
biomedical enginer can achieve.

### Slide 2

The challenge of aging is known and the reason we need clinical evidence
analysis is because we need to have a clear understanding of the assessment of
the use of technology in the health process. Exactly in parallel with drug
validation, we need to have a well-designed approach in order to check whether
a technology is worthy to be used in clinics, for which patient and under which
condition. In order to give the framework for having a clear outcome analysis
we need to design clinical evidence studies. This outcome analysis are the
basis for the policy makers to make an economical analysis and to define
whether a tecnology is worthy to be bought and given as a service
(cost-effectiveness analysis).  Then, what follows is a legal, ethical and
political rebound of these studies.  The starting of the story is to have a
clinical evidence. This is a key point of the new medical device regulation law
in Europe and in FDA in the US because, actually, in the past what there use to
be for medical device was just a validation of the risks and not a validation
of efficacy. Now there's the need to prove the efficacy and to give clinical
evidence of the worthiness of using a device in a clinical framework.

## Page 2

### Slide 2

## Evidence Based Medicine (EBM)

Evidence based medicine (EBM): is the conscientious, explicit, judicious use of
current best evidence in making decision about the care of individual patient.
The clinician has the specific patient (Mr Rossi) and he needs to decide what
is the best solution for the care of this specific patient. To do this he needs
to be provided with EBM, so the best evidence for supporting a decision.

## Page 3

### Slide 1

The evidence can be structured at different levels. Let's start from the bottom
of the pyramid up:

1. Expert opinion: of course the more the expert the more the opinion. We want
	 to overcome the just expert opinion.
2. Case-series/reports: by exchanging experiences between experts using
	 case-series
3. Cohort studies: observational studies of population.
4. *Randomized Controlled Trials*: the very first relevant layer of this
	 pyramid.  This is the apex of unfiltered information (where you are focused
	 on a specific technology design).
5. This step is characterized by comparing multiple studies in literature.
6. The apex of the pyramid is the systematic reviews.

The higher we move in the pyramid, the higher the complexity of the gathered
information and the higher the reliability of the evidence we are providing.

### Slide 2

## From studies design representation to RCT definition

In a study design we can have:

- Experimental studies: usually focused on the testing of a specific
	technology, a specific effect, a specific intervention. We want to check
	whether this intervention is worthy or not and gather evidence about this
	intervention by:
	- uncontrolled trials: trials without control group
	- controlled trials: trials with control group. This kind can either be:
		- non-randomized
		- **randomized (RCT)**: randomized allocation of subjects into experimental
			(intervention) and control group.
- Observational studies: do not have any intervention.

From the representation just reported we can abstract the definition of RCTs:
they are experimental studies focusing on an intervention using both control
and experimental group and with a randomized allocation of subjects to the
intervention or the control group.

## Page 4

### Slide 1

RCT is a type of scientific experiment used to test the efficacy and efficiency
of a service (aka intervention) in healthcare, such as na new technology,
methodology, treatment or drug therapy, in a well-designed target population
following a rigorous methodology.

- *Randomized*: participants are allocated on one group or the other by
	randomization.
- *Controlled*: new treatment is delievered to one group, the experimental
	group. The other group (control group) is provided with the usual treatment.

### Slide 2

## RCT pipeline

1. Formulation of a clinical question.
2. Definition the eligible patients by establishing the eligibility criteria.
3. Recruitment of the patients and formal entry to the trial. Usually this
	 entering in the trial is made by two steps:
	1. Ethical approval and informed consent of the patient. The key point is
		 that the patient's will is required in order to agree to the study.
	2. Checking the eligibility criteria.

	 Some people might not consent and not enter the trial because of whatever
	 reason.
4. Randomization: there's the split of the overall patients that have entered
	 the trial into two groups. The definition of standard care and intervention
	 needs to be performed when formulating the clinical question.
5. Definition of the outcomes and comparison of the outcome measures between
	 the two groups

## Page 5

### Slide 1

## Rationale for randomization

First ingredient is randomization, which is referring to patients allocation. It's done because it assures that the only difference between the control and intervention groups is the different treatment that is provided to them.

Randomization allows us to say that any difference in the recovery (e.g. in the
outcome) is specifically linked to the either the treatment or the control. The
randomization algorithm needs to be unpredictable and hidden until the end of
the study.

### Slide 2

## Rationale for control group

We need to prove that the novel treatment, which is under investigation, is different (eventually better) to the best treatment currently available.

The best treatment currently available is the one given to to the control
group. New treatment is given to the experimental group.

If the best treatment currently available is not fully recognized (e.g. there
isn't a common consensus), the control group may receive:

1. *No treatment at all*.
2. Sham treatment (placebo). Placebo treatment is not easy to be designed in
	 rehabilitation because usually rehabilitation has a direct action on the
	 patients. It's not easy to give the patient a rehabilitation treatment that
	 is not doing anything on him.

## Page 6

### Slide 1

## The rationale for the statistical approach

We need to approach the overall understanding of the study by using proper statistical methodologies. This means that we need to define the outcomes (the measures). A treatment result in different outcomes on a patient-by-patient base: we need to acquire measures on each single patients and then, to assess the efficacy of the treatment we need to move to a population base. RCT analyzes the mean behavior of a target population.

In order to deal properly statistics we need to define:

1. A well-designed target population.
2. The outcome measured
3. Proper sample size: the number of patients that are recruited is an
	 essential element in order to have a robust statistic approach and draw a
	 solid conclusion.

### Slide 2

## Ethical issues

The other crucial element of whatever clinical experimental study is about ethical issues. Every time we involve human beings we need to accomplish ethical principles which need to be respected during all the administration of the intervention.

The Declaration of Helsinki (1964): the formal document that has been set
across the Internationa Community in order to define the steps and the rules
for ethical approval of a study involving patients. In any pubblication of clinical studies, the authors need to explicitly mention that they had an ethical approval and that it accomplishes the Declaration of Helsinki. The Declaration states:

1. An experiment needs to be performed *in compliance with the rispect of the
	 patient*.
2. A treatment that is known to be inferior shouldn't be given to any patient.
	 Every time we have a question we can't compare the new treatment with
	 something that has been already proved to be inferior because in that case
	 you are unethical to the control group.
3. The privacy of the patient has to be guaranteed. The issue about patient
	 data management needs to accomplish really strict rules about privacy
	 (GDPR).
4. Voluntary participation and the patient can withdraw with the study in any
	 moment without any pressure on him/her. Voluntary participation is a very
	 sensible point.
5. An informed written consent needs to bi signed. This is one key document
	 that the Ethical Committee analyzes because it needs to be written in a way
	 that is comprehensible to the patient. It shouldn't be too scientific in the
	 exposure but it must be easy to be understood. A layman (e.g.
	 comprehensible) language is used to write this document.
6. Research can be conducted only if the importance of th objective overtakes
	 the risks. In any ethical documentation, we need to state clearly the risks
	 of the studies and the reason why these risks deserve to be undertaken.
7. A clear research protocol should be established. Ethical documentation needs
	 to describe very carefully what the patient is going to be submitted to.
	 
Protocol: the step-by-step procedure that the patient has to submit.

An ethical committee needs to approve the studies and decide whether or not the
research should be conducted. No autonomous activation of research can be done
without a proper ethical committee approval, if the experiment is involving
human beings.

## Page 7

### Slide 1

## Systematic errors (aka biases)

Systematic errors are mistakes that do not allow to get to solid conclusions
and induce misleading conclusions.  The ethical committee will check that our
study is properly designed by checking all these possible biases:

- *Selection*: the person that enrolls (in it. iscrivere) the patients knows in
	advance the allocation to the treatment group. A possible mitigation would be
	to enroll the patients before randomizing and allocating them to a group.
- *Allocation*: There's not a bilance in the size and the two groups are not
	similar in the baseline. An example of this case: we recruit patients and
	then we randomize them into control and treatment group but we end up in
	having them unbalanced in terms of age. If age is an important paramenter, a
	possible mitigation of the bias could be to go on with the recruitment until
	we have an age matched.
- *Assessment*: assessors, the people that are taking the outcome measures and
	making the final evaluation of the patients can be influenced by knowing
	whether the patient has been in control or treatment group. In order to
	mitigate this, we need to "blind" the assessors so that they don't know
	whether the incoming patient has undergone the treatment or the new
	intervention or the standard therapy.
- *Publication*: there's the risk that very nice and positive results are more
	likely to be published with respect to RCT that aren't demonstrating an
	evidence in favor of the treatment. This needs to be mitigated by having an
	equal reporting of positive and negative results. Also negative results are
	an essential information for research. Another form of mitigation to that is
	that any RCT needs to be registered. We need to register our trial and to
	provid the final results when it is complete (i.e. publication or other
	forms).
- *Stopping*: the trial is stopped when the result are "considered
	satisfactory".  This expression is not a solid statement. In order to define
	when to complete the study we need to set the sample size at the very
	beginning of the design of the trial. It is an a priori decision and it's
	about the calculation of the sample size for statistic robustness. All this
	information are usually checked and need to be put inside the ethical
	documentation. 

## Page 8

### Slide 1

## CONSORT

It's a standard. It stands for CONsolidated Standards Of Reporting Trials.
CONSORT statement has been developed by the scientific community in order to
report Randomized Control Trials, it's a sort of framework for reporting the
results. We have this framework that is articulated as a sequence of 25 items
that we need to describe and accomplish in the report of RCT.

The consort is the methodology to report consolidated standard of reporting
trials: when doing a clinical trials there's the need to follow this
methodology in order to communicate and report the results. It's make sso to be
a sort of checklist so that anyone can go through every single item and
describe how we dealt with every single point. Whatever changes from this list
needs to be stated and then checked by the ethical commettee.

### Slide 2

Let's see some of the items.

- Trial design: description of the trials and any changes we are adopting in
	the methods after the trial started. Any of this change needs to go back to
	the ethical committee. The initial approval that we have does not cover
	possible changes that we need to put in place. If we need to change something
	we need to go back to the ethical committee and provide any reason for the
	changes. There are different kind of trial designs:

	1. Parallel design: when we randomize, we put each participant in an
		 intervention arm or in the control group. We can have more than one
		 intervention, that's why we call it "arm". In parallel design each
		 participant is taking only one type of intervention.  
	2. Crossover design: when each participant is exposed to each intervention in
		 a random sequence. An example of this is the comparison of the use of a
		 particular exoscheleton with respect to another. In this case I can either
		 allocate half of the patients on exoA and half on exoB or, we can have
		 each of the patients making test both with exoA and exoB in a sequence. In
		 this case each patient is exposed to both the solutions. The order of this
		 test is randomized.
	3. Factorial Design: More than two treatments can be given to each patient
		 (i.e. A, B, A+B. Evaluating them singularly and together).

## Page 9

### Slide 1

	4. Superiority trial: determine if there's a relevant difference between two
		 interventions.
	5. Equivalence trial: determine if an intervention is neither worse nor
		 better than another intervention.
	6. Non-inferiority trial: determine if an intervention is not inferior to
		 another intervention. An example of this: I want to prove a
		 non-inferiority of using a technology, from a clinical point of view,
		 because this technology might allow me to have a better organization of
		 the manpower inside the hospital or can be done at home by the subject or
		 at a lower costs. This is a reason why non-inferiority deserves a full
		 evidence of efficacy.
	
	From a statistical point of view, it's very important to determine the null
	hypothesis. If we are trying to prove superiority, equivalence or
	non-inferiority, we need to organize the statistical problem differently.
	
## Slide 2

- Participants: We need to give the clear definition of inclusion and exlcusion
	criteria and the settings and location. It's crucial to make the result of
	the trial capable of being generalized over a large population. When we have
	defined the inclusion and exclusion eligibility criteria, the way to execute
	the trial, we are ready to make the real recruitment

## Page 10

### Slide 1

- Interventions: the rules, the protocol for the definitions
- Outcomes: pre-specified primary and secondary outcome measures including how
	and when they are assessed. It's a very important point. at the very
	beginning we need to select the outcome measures. These are the clinical
	scores we need to use to assess the efficacy. We can't take whatever measure.
	We need to select at the very beginning, when we are preparing the
	documentation for the ethical commettee, to define the outcome measure.
	Basing on the choice that was made it's easier to make comparison. The
	primary outcome measure is the parameter on which we need to calculate the
	sample size. These measures need to be pre-specified (a priori specified) and
	not an exploration afterwards of what is significant between the groups (a
	posteriori).  This is a key element in term of research integrity. Then I
	will list secondary outcomes which will help in understanding better the
	effects. Others should be able to use the same outcomes (e.g. replicability
	of the whole calculations that we are including in the publication). Outcome
	are the final measures that we are going to acquire before the intervention
	(pre) to define the baseline, after the intervention (Post), and during
	follow up (FU). On this visits we have specific set of outcomes, measures we
	need to acquire on each patient at every time point of our trial.  The
	outcomes usually include clinical scales and also some other quantitative
	measures. Among these measures we need to define what are the primary outcome
	measures (e.g. the ones we are going to include for the calculation of the
	sample size). GOTO Page 11 slide 1

### Slide 2 

	One important outcome is QALY (Quality Adjusted Life Years). That is with
	health technology assessment because it pertains to cost-utility analysis
	which estimates the cost per quality adjusted life year. Is our intervention
	impacting on the quality and quantity of life that our patient is going to
	live? We compare the indexes with and without the intervention: this
	difference is the basis for policy makers in order to consider the
	intervention as worthy.
	
## Page 11

### Slide 1

- Sample size: its determination is a critical point. It needs to be determined
	on the primary outcome measures. It needs to be compared with the clinically
	important difference that we want to see in the outcome measures. There are
	formulas that need to be apply to define the sample size.  They depend on
	clinically importance difference. The gait velocity in order to be clinically
	relevant should overcome this threshold (e.g. clinically important
	difference). There are studies that are establishing, for a given task, the
	clinically important difference. Once we have the amplitude (numerosity of
	subjects) of the outcome measures that is the clinically important
	difference, we need to: 

	- Estimate the *clinical importance difference* between the intervention
		groups.
	- Define the significance level (`$\alpha$`). So for instance i want a
		p-value of .05
	- Define the statistical power (`$\beta$`): probability to detect as
		statistically significant a clinically important difference (which is
		defined from the primary outcome measures. If we don't have it, it's going
		to be the standard deviation of the outcome measure) of a given size, if
		such a difference exists. So for instance i want a statistical power of 80%
		(I want to be sure at least of 80% of what are the statistical conclusion).
	- Define, in case of continuous outcome, *the standard deviation* of the
		outcome, representing the natural variability of the outcome.
	- *Dropout rate*: when we get to the sample size of for instance 100 patient
		we need to take into account a dropout rate: based on clinical experience
		about 10% of the patient might dropout during the studies because of
		whatever reason. The dropout rate needs to be quantified. If the sample
		size is 100 i will need to take into account 110 patients, considering the
		dropout rate.

The size of the effect is inversely related to the sample size.
	
## Page 12

### Slide 2

- Randomization: There are multiple possibilities to allocate randomly the
	patients inside the intervention. We need to define clearly in the CONSORT
	(e.g. in reporting the trial) and tell your public what is the randomization
	methodology we are taking. There are different kind of randomization:
	
	1. By block: overall the group going to one or the other arm is balanced in
		 terms of age for example.

## Page 13

### Slide 1

	Allocation concealment: the person enrolling participants does not know in
	advance which treatment the next participant will get. When we do the
	pre-evaluation, here we are talking about the assessor's blinding: the
	assessor is not aware about the group belonging to the patients, avoiding
	selection biases. Biases are not intended as conscious incorrect behavior but
	kind of self-conscious bias.

### Slide 2

## Blinding

- Blinding: Blinding is another essential element:

	- *double blind (fully blind)*: the participant and the assessors are unaware
		of the group assignment. In drug studies it's quite easy to have a fully
		blinded study because we can have the fake drug exactly the same shape as
		the real drug: both the nurse and the patient are unaware of the nature of
		the administered pills. On the other hand, in rehabilitation, it's much
		more diffcult to have that the patient and the operator are blind.
	- *Single blind* (more diffused in literature): only participants or
		assessors are unaware of group assignment. What we usually can have in all
		the studies is the second one, so that the assessors are blinded. So the
		patients and the therapists know whether they are doing the intervention or
		they are in the control group but, at the end of the treatment, a doctor is
		called just to take the outcome measures on the patients before and at the
		end of the treatment.  This doctor, who's the assessor, was not informed in
		any way whether the patient has undergone the treatment or not.
	
	Blinding is very important not to have psychological biases in the patient,
	in the operator and in the assessor.

	Blinding is dealing with the psychological effects that the subject might
	have because of feeling taking care. if you are participating to an
	experimental study you tend to have a positive prospect of our outcome and
	this implies a positive attitude to the pathology and a tendency to recognize
	and show and being involved into the treatment and, in the end, all these
	aspect can contribute to the positive effect registered at the end of the
	treatment.

## Page 14

### Slide 2

## Statistical methods

- Statistical methods: they start from a comparison of the baseline values, to
	understand if the two groups at baseline are at the same level. Possible
	differences in the baseline have occurred by chance (because of
	randomization).

	Anyway we need to document in the CONSORT report trials all the possible
	differences in the baseline values.
	
- Comparison of baseline: baseline is the pre, at the moment of recruitment
	each patient is evaluated by the outcome measures at baseline. The
	researchers will have a set of measurements for both for intervention and the
	control group. We may apply a statistical test in order to compare the data
	at baseline between the two groups. From a conceptual point of view, since we
	have randomized the two groups, we won't expect any baseline differences but,
	by chance, we might observe unbalances. In this case we may want to report
	these differences inside our trial report.

## Page 15

### Slide 1

	This slide represents all the visits and all the time-points in which the outcome measures are acquired and evaluated.

	We then need to estimate the treatment effect:

	Longitudinal (accross time) method analysis, which is divided into different
	phases:
		1. Baseline (pre-evaluation)
		2. Intervention
		3. Post-treatment (post-evaluation)
		4. Follow-up: recording after few months of treatment in order to evaluate
			 the persistance of the eventual achievement of the treatment.
	
	Depending on the kind of outcome measures that we are acquiring (either
	continuous or categorical) we need to setup the proper statistical test
	between the two groups. At the end, what we will be doing is the comparison
	of the differences between post data and pre data or between follow-up data
	and post and pre between the two groups. In the case of categorical
	variables, we will use non-parametric tests. In the case of continuous,
	variables we will use parametric tests (i.e. ANOVA test with two independent
	variables: time and group. What will be the most important information about
	the efficacy of the intervention will be in the interaction test of time per
	group, which answers to the question "is group1 having a statistically
	significant difference in their post vs pre data, which is different from the
	control group post vs pre? In other words, is there a difference between the
	two groups in time?").

Now we have all the process of the study and we need to summarize the results.

### Slide 2

### Study flowchart

The first point that we always need to report is this diagram that starts from
the number of asessed eligibility patients, the number of randomized patients.
Each of the branches below represent the allocation of the patients, in the
follow-up and in the analysis. Why specifying the follow-up? because the
patient can wthdraw at any time. In the ethical document we always have to let
the subject withdraw at any time. One very important point here is that the
number of the patients that we allocate may be different from the one that
completed the study itself: we need to give clear reasons for the subject who
discontinued. We may have problems in a few subjects' data acquisition so the
analysis could use different numbers. All these numbers need to be reported.

## Page 13

### Slide 1

doing the pre-evaluation we are talking about the assessor blinding (the person who is acquiring the assessment of the patient is not aware whwther the patient is in the intervention or in the control group). bias -> subconscious incorrect behaviour: the person enrolling the participant is not knowing what treatment the next participant will get.


## Page 16

### Slide 1

## Results

- Recruitment
- Baseline Data
- Numbers analyzed

About the ending of the trial one point is that  we have received the sample
size. Sometimes if we find a larger than expected benefit or any harm on the
experimental intervention we could have an anticipated ending of the trial.
This happens because we are no more assuring that we are ethical in not
proposing the intervention to the control group or, the other way round, in
proposing the intervention to the experimental group.  In other words, if the
results of the study is very good and we are having a large effect on 80% of
the sample size and we statistically reach these data, the issue is that from
that point on it would be unethical for new subject to be recruited and
randomized in control group.

### Slide 2

## Discussion

- Limitations: In the reporting of trials (this applies to any scientific
	publication), at the end of the publication we should have a paragraph, which
	can be titled "limitation" for example, in which we discuss about them. We
	talk about the assumption we have taken and the limits of the study.

## Page 17

### Slide 1

## Systematic reviews

Another important methodology in order to report scientific studies is about systematic reviews.

Systematic reviews is used in order to try to put together information coming
from other small studies in order to achieve greater evidence. So we start from
a specific research question and we collect all the studies that have dealt
with that specific research question and make a meta-analysis (e.g. an analysis
at higher level) which tries to put together the results of individual studies
in order to have a higher generalizability and higher level of evidence.
Cochraine, an international organization which has the goal of scientists
aiming at developing well-informed decisions, maintaining and promoting
systematic reviews. 

RCT is about designing a specific experimental study and recruiting patients
for the study by following rigorously the rules. On the other way round, if we
have many RCTs or pilot studies answering a main common question we can use
statistics in order to interpret together the results of all of them, building
a higher level of evidence.

## Page 18

### Slide 1

The key characteristics of a systematic reviews are the following:

- Having a very well defined eligibility criteria
- Having an explicit and repoducible methodology
- Systematic search: a clear and well-documented section discussing how the
	documents were collected.
- Assessment of validity of findings of included studies: we need to evaluate
	the validity of the studies we are putting togther.
- Systematic synthesis: a summary of the characteristics and findings of the
	included studies.

There are software helping in this analysis.

### Slide 2

### Definition of the review question

This deals with the so-called PICO, a really useful acronym to formulate the
question appropriately.

| Letter | Meaning                     |
| :---   | :---                        |
| P      | Participants                |
| I      | Interventions               |
| C      | Comparisons                 |
| O      | Outcomes (including timing) |

PICO deals with exclusion and inclusion criteria. For instance we might want to
have the control group, so in the comparison section we impose the existance of
control group and we will include all the studies where two groups are
reported, one having the the experimental treatment and the other not having it
and exclude all the studies which have all the people having the treatment.

The outcomes are not usually part of the crieria for including the studies.

## Page 19

### Slide 1

### Searching for studies

We need to define the databases, (CENTRAL, MEDLINE, EMBASE) and the type of
studies that we want to include, the possible unpublished studies that come
from the trial register. We have already mentioned that any RCT when is
designed and approved by ethical committee should be registered on one of the
international database for clinical trials (such as clinicaltrials.com). It's
important that, when we look for a systematic reviews, to check if there are
studies that answers our question (e.g. eligible in terms of PICO design) but
that has not reached the final publication and they are only in trial register.
This because one of the bias when we consider a systematic review is about the
so-called "publication bias" so that good results (that are in line with the
assumption of the study) tents to be more easily published with respect to
negative or zero result. If it's not published, it's not easy to find that
results. This goes back to the concept of integrity of research: proving a
negative effect is anyway something that builds scientific knowledge.

### Slide 2

#### Assessment of eligibility criteria

It needs to be done by two independent researcher doing the systematic review.

It deals with:

1. Merging the result of different databases removing duplicates
2. Esamine titles and abastarcts. Of course the starting point is an automatic
	 research based on keywords but after a fast reviews of title and abstract,
	 one can easily remove reports that are not relevant to the question.
3. Retrieve full text of potentially relevant reports, reducing again the
	 number of relevant reports.
4. Put together the reports that deals with the same studies.
5. Examine the full-text report for compliance of studies with eligibility
	 criteria.
6. Build final set of studies included in the systematic reviews. From that
	 time on we have the data on which we are going to work with.

## Page 19

### Slide 1

### Collecting data

The data to be collected are:

1. *Methodology*
2. *Participants*
3. *Interventions*
4. *Outcomes*
5. *Results* are based on the number of participants which gives us a kind of "weigth" of our study. Then we should have a summary data for each intervention.

Once we have the data of each single study, we need to have a statistical
metodology in order to do the meta-analysis: the statistical analysis which is
on top of the detailed results of each single studies. We need to make a proper
weighting (dealing with the number of subjects) and normalization (in terms of
outcome measures). We might have that we are measuring the functions of a
district of the body and this can be measured by different scales. In order to
put togther these data we need to normalize them at first and scale them
depending on their mean and standard deviation. After this conversion, we can
treat those data as inside the same meta-analysis always carrying with us the
information about the normaized data and the number of subjects of the study.

### Slide 2

### Evaluation of bias

Risk of bias assessment is done in each studies that we need to include in the meta-analysis. It deals with the possibility of having an under or over estimation of the true effect. 

## Page 20

### Slide 1

This kind of approaches are very structured and we don't have to invent them
but there's a very clear definition on methodologies. These need to be strictly
folowed in order to respond robustly to our question.

The bias that we need to check for any single paper included in the meta-analysis are:

- Selection biases: if there's any systematic differences between baseline
	characteristics of the groups.
- Performance biases: systematic differences btw groups in the care that is
	provided.
- Detection biases: how the outcome are determined. for example if we have no
	blinding of the assessor, we might have a detection bias.
- Attrition biases: systematic differences between groups in withdrawals from a
	study. Exclusions: patient are omitted from the report. Attrition: outcome
	data are not available.
- Reporting biases: possibility of having systematic differences between
	reported and unreported findings.

### Slide 2

The next step is to use statistic to pool together the estimate of the effect of the treatment. 

The rationale of the meta-analyses is:

1. increase the statistical power
2. improve in accuracy
3. to have the ability to answer question that were not posed by the individual studies
4. opportunity to settle controversies aising from conflicting claims

## Page 21

### Slide 1

The last point of the meta-analysis is the quality of evidence, the level of
confidence that we have in the conclusion of the study.

Grade ranges between four levels: high, moderate, low, very low quality. It
deals with the evaluation of our results, so about the meta-analysis and not
about the single study.

- High quality: further research is really unlikely to change our confidence in
	the estimate of the effect.
- Moderate quality: further research is likely to have an impact on our
	confidence in the estimate of the effect and may change the estimate.
- Low quality: further research is very likely to have an impact on our
	confidence in the estimate of the effect and it is likely change the
	estimate.
- Very low quality: we are very uncertain about the estimate.

If we have low or very low quality of the meta-analysis: this means that
there's no strong evidence answering the question we have posed. This means
that there's the need for new papers in order to answer the question. Low
grades are really relevant as well because it's informing the scientific
community that there's the need to further research on that question because
there isn't a scientific common ground in order to answer to that question.

### Slide 2

We then end up the systematic reviews with a summary of findings. In here we put:

1. Quality of the evidence: the grade we saw before
2. Magnitude of effect of the intervention examined: SMD (Standardized Mean
	 Differences)
3. Sum of avaiable data on important outcome for a given comparison. The
	 overall number of studies and number of people.

## Page 22

### Slide 1

An example of Cochrane review of 2015. We can first recognize the PICO:

| Meaning      | Description                                                               |
| :---         | :---                                                                      |
| Population   | Post-stroke                                                               |
| Intervention | elettromechanical and robot-assisted arm training                         |
| Comparison   |                                                                           |
| Outcome      | Improving activities of daily living arm funciton and arm muscle strength |

### Slide 2

Summarizing the eligible studies in this sort of diagram.

## Page 23

### Slide 1

Risk of bias: The evaluation of the studies. They considered each specific
categories of their interest (about biases) and assessed them with a grade:

- red: information not available or done not correctly.
- yellow: information not clear.
- green: information correctly reported.

Publication bias will downgrade the GRADE level. From a very high level quality
we will need to downgrade to moderate quality at least.

To conclude this kind of diagram is helping evaluating the meta-analysis they
are performing.

### Slide 2

Once we have the bias analysis done, we need to put together the means and
standard deviation of all the outcome measures. For each studies we have the
means of the treatment and the control group an we put in the forest plot (the
one reported on the right) the standard mean difference (SMD).

SMD is performed by considering mean and standard deviation of both treatment
and control group.

Let's consider the first row. We calculate SMD and then we associate to the
effect size of that study the weight. This weight deals with the number of
subject included in the analysis. Once we put inside the forest plot all the
data of each single studies, these data are normalized and associated to a
specific weight and then make an average. This average is the final outcome of
the systematic reviews. 0 is when we don't have any evidence that control is
better than treatment, negative when evidence is in favor of control and
positive when evidence is in favor of treatment.

The conclusion is that we have an evidence in favor of using electromechanical
and robot arm training in post-stroke patients.

## Page 24

### Slide 1

Summary of findings at the end of systematic reviews says that we have 700
participants across 18 studies with SMD of .37 and with a grade evaluation that
is very low. The author was downgrading the evidence of the studies by 3 votes.
This because of high risk of publication biases, differences in effect sies and
unexplained heterogeneity (we can see it in the forest plot: `$I^2$` is very
high). This means that the overall results are not very strongly considered as
a single group but there is some heterogeneity between the studies. Another
issue is that upper or lower confidence limit crosses an effect size of 0.5 in
either direction (e.g. the standard deviation of the SMD). This means that the
position of evidence is not certain: it would be a problem if it SMD had
crossed the vertical axis!

This means that there's the strong need for new studies to further research
whether the effect of the treatment is improving the outcome or not.
