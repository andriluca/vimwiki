# Lecture 4 --- Rehabilitation robotics

## Page 1

### Slide 1

### Slide 2

Some contextual information. It's very important to have a general overview on
the point that we are studying. All the questions about working on
rehabilitation deals with these graphs. 

Let's consider the graph on the left: The curve is showing that the strength is
growing until a certain age has been reached, from there on the strength is
going down. For untrained men we have a profile of decline over age which is
completely different from the one of trained men. That applies to overall
performance of the body. Training moves this curve and by moving the curve this
means that the elderly age situation would be better than in case of
untraining. There's a strong link between healthy aging (having a decline due
to aging) and active living (having an active healthy life). Active training
across the whole life moves the whole curve at another position. So in the end
we will have a situation in which elderly people will have higher performance
with respect to untrained situation. This is very important because our society
is overall aging: we have very good acute care so lots of people that live
longer, we have less children and so the rate of elderly people is increasing a
lot. In order to make the overall society sustainable we need to have this
aging population living an healthy and active life. In order to achieve this,
education is the first step: people are invited to have an active life all over
their life span and this invitation is an empowerment about the recognition of
the importance of active life for the overall society. In the end the problem
of rehabilitation deals with reducing the slope of the decline, reducing the
decline that follows specific aging and occurrence of acquired motor
disabilities.

What are the reasons of disabilities? The leading cause is stroke (it: hictus):
it accounts for highest number of chronic disable people in the world. The risk
of stroke increases with age. In our society we have a higher risk of stroke
because of aging. Other reasons are about neurodegeneration pathologies such as
MS (Multiple Sclerosis) and also SCI (Spinal Cord Injuries): typically young
people. The impact on the chronicity is high as well in this case. Another
cause of movement disorders is Parkinson Disease, even if it leads to a lower
disability ranges with respect to stroke and it has a progressive process which
is different from stroke. Disability in children is very few (2.8 per
thousands) but children need to be followed across their whole life. So overall
the structure of our society is asking for a lot of effort on rehabilitation.
Rehab means that we try and pursue the reduction of the effect of the
disability to the life of the people.

## Page 2

### Slide 1

In the realm of rehabilitation, the use of robots has become an important
element. We can distinguish between two different contexts of using robots in
this framework. These contexts are formally different but they are quite
overlapping in terms of technology. It's worthy to highlight the difference but
it has to be considered that the difference is in kind of gray values and not
white and black.

- *Rehabilitation robotics*: aims at helping the clinicians in promoting
	rehabilitation of an individual so that he/she can interact with the
	environment unassisted. This rehabilitation robotics happens *inside* a
	hospital, in a rehabilitation treatment. The final goal is that the
	patient can interact (executing tasks) with the environment without
	assistance.
- *Orthotics*: aims at improving functions in people with weakness, due to
	neurological disorders, who can't control it when interacting with the
	environment. Here we are not working in building an autonomous,
	unassisted functioning, but we are assisting the subjects with
	disability with specific devices. Wheelchair is an example that may
	belong to orthotics: it doesn't help the clinician in promoting the
	rehabilitation of walking, but it is assisting the patient improving
	mobility in order to interact with the environment. Mobility, which is
	disabled by the pathology, is recovered as function by using the
	wheelchair. Orthosis are designed to work in cooperation with the intact
	body (and that is a key point: we don't want the orthotics to completely
	substitute the patient's ability but we would like orthotics to be able
	to give the needed supplement to achieve the function, but let the
	subject use the residual capability that he/she has. The point here is
	that if the patient is a person that can kind of walk it's better to
	mobilitate the patient because of the strength graph we saw before. In
	some occasion we can use wheelchair for safety reasons but we would like
	to empower the subject as much as possible with his residual
	capabilities without taking any risks) and control or assist movement.
	Robotic exoskeletons was intended as an orthotic device and started as
	an augmentation of humans: it was not intended for disabled people but
	for soldiers or people who had to carry a lot of weight because it would
	have provided them extra capabilities of moving, carrying loads and
	supporting long-term efforts.

### Slide 2

The exoscheleton (exo) here shown is very bulky and not really usable
but from this paradigm the possibility to use this kind of devices towards
the helping of disabled people has come straight forward as an effect of
the effort done by robotic companies in this direction.

## Page 3

### Slide 1

## Stroke

Why stroke needs to be very known by us? When we talk about disability the
most of the target population is stroke people (survivors from stroke).
Since we are talking about development of medical devices, which need to
be produced, certified and commercialized, if we don't have in mind the
targets of our market it could be a rough mistake. Let's then try to
understand what is our target population: it can be very broad with some
specific disabilities, common by considering the symptoms, but the
etiology (reason for the disability) can be very different. We can develop
technology tackling (in it. affrontare) the correction of the symptoms but
we can't completely disregard the etiology of the symptoms. This makes us
understanding and knowing what causes the disability. There are different
pathologies that can cause disability but, from a statistical point of
view, the main cause of disability is stroke. It is not just the main
cause of disability but its post-affected population is also the most
promising in terms of possibility of benefiting of rehabilitation. This
two elements were essential when it come to focus on stroke survivals.

What is stroke? An acquired cerebro-vascular accident (CVA: in the brain
there's a problem of vascularization which happens as an acquired damage).
A fast cerebral functionality loss due to an improvise blood flow
interruption or haemorragy. Immediately after stroke 80% of the patients
are affected by hemiparesis (lack of muscular control and tone in half of
the body). This happens because vascularization of the brain goes from a
central artery of the right side and on the left side of the brain
hemisphere. Depending on which artery or vascular damage the subject is
having we will have an effect on one or the other hemisphere. An effect on
one hemisphere means a functional disability on the other side of the body
because of the contra-lateral control (functional deficit is contra-lateral
with respect to the cerebral lesion). 

The motor functional recovery has an exponential trend (this is a great
hope of recovery): a post-stroke subject will have a great recovery. The
level of disability just after stroke is very high but then there's an
exponential trend and a very fast recovery. The initial part of this
exponential recovery is due to the resolution of local ischemia (e.g. the
re-absorption of oedema). There's a kind of bump (in it. "bernoccolo")
inside the brain and there's a natural re-absorption of blood gone around
the damage so that the initial part of the brain which is affected by the
stroke is much larger than the real focus of the damage. This initial
recover accounts for the exponential trend of initial functional recovery.

Afterwards functional recovery occurs at the same time with a dynamic process
of cortical and sub-cortical reorganization: there's a phase called "acute
phase" which lasts about 6 months after the accident where the whole brain
works and is adapted, the whole connectivity of the brain is reorganized in
order to get to the functional recovery. Thus there's a natural part and a
re-connectivity. 

Then there's a chronic phase which is after a long time after the accident
(usually the threshold is about 6-9 months after). We can still have some
recovery which is smaller both in size and in time. 

Rehabilitation is about neuroplasticity.

### Slide 2

Neuroplasticity is the capability of the brain to define modifications in the
organization of the neuronal components (e.g. connectivity) because of internal
and external stimuli. 

There are external stimuli coming to the brain, which is having internal
connections. The stimuli are driving changing the connection of the brain.
Learning deals with the plasticity so the capability of the brain to
change its proprieties and connectivity. This applies exactly the same
also after a damage/trauma: if we have "black holes" inside the brain
whatever connectivity that used to pass from that black hole is
interrupted and we need to drive new connections which are able to
circumvent the damaged part and still connect area1 and area2 and to have
the function recovered. 

The target is behavior (functional), the resource is brain plasticity, the
tool to drive this changes is training. Training enhances lesion induced
reorganization of the brain. That's the rationale for any rehabilitation.

Potential for reorganization persists even in chronic stroke patients.
Reorganization is something that's *always* available: if someone's alive
he/she has the ability to reorganize his/her brain and the way to do that
is active training.

Active training is better than passive movements. From the first lecture, we
have that motor control is linking that flow of connections inside the brain
which starts from planning a movement till executing and controlling it
efficiently. Now if we want to recover the efficient control, we need to work
on the whole flow (from the planning to the execution): passively flexing a
knee through an external input is not enough because in this case we are not
calling all the areas which are about motor planning and motor control but just
working on the peripheral parts (i.e. reflexes). Training is the tool to help
reorganization of the brain and should span across the whole process of motor
planning till motor execution.

An important point that should be mentioned is maladaptive plasticity.
Maladaptive plasticity is the possibility that compensatory movements are
adopted by the subject in order to achieve a function, which in the end
results in a silencing and a "reduction" of the possibility of brain
reorganization. An example: let's suppose I have a stroke and my right
side is impaired and I'm not able to move my right hand. I can write my
left hand in order to write but if I start using my left hand for every
single task that implies the use of a single hand and not try to exercise
my right hand, somehow the brain areas which should be reorganized in
order to control my right hand and would help in the recovery of the right
hand are silenced: the fact that I'm not using means weakening of the role
of those areas. Vicariation and compensatory movement induce maladaptive
plasticity, a plasticity which is not in a direction of a recovery. In
rehabilitation we need to balance between functional autonomy (if we are
not well controlling the right hand, we can use left hand for some tasks)
but on the other side we keep going in training the right hand to achieve
the best possible recovery of that hand and the impaired part. It's a
mixture of both the components that need to be balanced. In other words,
maladaptive plasticity happens every time the brain is changing its
behavior not in the direction of recovering the function. This happens if
we disregard the impaired limb (typical when dealing with upper
extremities). If we reverse and not use the impaired hand, what happens is
that brain is reorganizing in order to promote the use of the other hand
and this non-use of the impaired limb makes a change in the reorganization
that is not promoting the recovery of the impairment but just of the
function.

There are two ways of working on brain plasticity:

- Working on the periphery (e.g. training): physiotherapy-based
	rehabilitation. You ask the subject to attempt doing the task and this
	has a rebound inside the brain. Stimulation on the muscles acts at the
	level of periphery. 
- Working on the brain sensibility (e.g. brain stimulation, central
	action): this can be done by drugs or by some tools for stimulating the
	brain (i.e. non-invasive brain stimulation, transcranial magnetic
	stimulation, transcranial electrical stimulation).  In this case, we
	give some stimulation to the damaged areas and moving the activity to
	those neurons closer to threshold. The theoretical assumption is that
	this kind of "pre-activation" of the areas makes them more prone to
	change and be activated. It's a way to prepare specific areas in order
	to make them more capable to achieve the plastic behavior which anyway
	comes from the peripheral training. 

## Page 4

### Slide 1

What are the ingredients that make neuroplasticity effective?

1. Amount of practice: the more you exercise, the greater the success.
	 That's the basic rule. We need an amount of training dataset.
2. Repetitive training: we need to do a training which is repetitive.
	 Let's think of an artificial neural network, trained with
	 back-propagation. If we want it to learn a behavior, we need to show
	 the network our set of training examples many times in order to use
	 that example efficiently for the training. We need to show that
	 training dataset many times to the network.
3. The earlier the intervention, the better the outcome. We need to use
	 as much as possible the window of the sub-acute phase where both the
	 natural reabsorption of the oedema and the neuroplasticity are full
	 usable for brain reorganization.
4. Functional and goal-oriented training. We don't want the patient to do
	 the task (i.e. flex/extend) without a task. It's better to give a task
	 (i.e. take the pencil). Why? Let's think again about motor control. We
	 want to re-train the whole process of motor control, including the
	 task execution, but also the *planning* and the *task definition*.
	 That's why we want the train to be goal-oriented as much as possible.
5. Biofeedback: sensory problems are as much important as motor
	 problem.  When we have an impairment that can either be because we
	 have a poor motor control in muscle recruitment or a poor feedback
	 information about the task that we are doing. If we think about motor
	 rehabilitation as a process that trains the sensory-motor integration
	 we can understand that we have to work both on the sensory and the
	 motor portion. Biofeedback deals with the possibility to give a clear
	 information to the subject about what he's doing very often. It's
	 extra information about the consciousness of the task that the subject
	 is performing. This can be done by the physiotherapist is telling the
	 subject if how he's performing, if he/she needs to put more effort
	 into it, etc. So physiotherapists are providing information about how
	 the subject is performing and what is the optimal task. At the same
	 time, biofeedback is when the therapist is asking the subject to look
	 at the task execution: vision is often the less impaired sensory
	 channel. Vision can vicariate (compensate) for impairment in other
	 sensory channels but it limits the automatism of fast tasks as well as
	 it asks for extra cognitive efforts in doing tasks. Anyway it's a way
	 to give subject a feedback.
6. Augmented proprioception. When we ask a subject to make a movement
	 inside a swimming pool. There are lots of information coming from
	 cutaneous sensors about the movement because we have a resistance of
	 the fluid. This resistance gives an extra information of the subject's
	 proprioception. Another example is when the physiotherapist is
	 resisting to the task asking for an extra activation of the muscle
	 which augment in the proprioception (coming back to the brain of the
	 patient). EMG triggered vibration are used with kids with distonia.
	 Distonia ends up in a very poor motor control and spastic behavior and
	 this kinetic behavior is due to sensory impairment, more than the
	 motor impairment. The sensory impairment is that the subject doesn't
	 have a clear information about the activation of the distonic muscle
	 so they are over-activating the distonic muscle in order to have a
	 proprioceptive feedback and this ends up in a distonic movement. It's
	 like the threshold of their proprioceptive feedback is wrongly set.
	 Augmented proprioception was intended by reading the EMG of the target
	 muscles and give cutaneous vibration on that same muscle. The
	 cutaneous information have different pathway with respect to the one
	 of proprioception. If the proprioception pathway was damaged, while
	 the cutaneous pathway was not, the brain could learn to process the
	 cutaneous information as a compensatory information about
	 proprioception, thus reducing muscular activation because earlier
	 informed by cutaneous channel. FES is always considered as an
	 augmentation of proprioception.
7. Rewarding and engaging. Motivation plays a very important role in
	 rehabilitation. *If if fail the task once why shall I repeat it 100
	 times?* We need the subject to be fully involved in the task. It's
	 also true that it's very frustrating for the patient to try to reach
	 the cup if he's not reaching it. In order to learn to reach the cup we
	 ask him to try reaching it as many times as possible. This implies
	 that boredom and frustration could occur in the subject. It's boring
	 to repeat something, it's frustrating to be asked to repeat something
	 if you are failing it. There's a really important role in
	 rehabilitation designing which is about rewarding and engaging.
	 Rewarding: setting the target of the task at the point where the task
	 is achievable. You don't have to make the subject doing something that
	 is a fail but we need the subject to be challenged in doing something
	 that in the end will result in success. So for instance the first the
	 cup will be very close by, second day it will be 2cm further. Day
	 after day we are challenging the subject at the level where the task
	 is achievable but challenging because if we are setting the task at an
	 easy-going point, that will become completely boring to repeat. The
	 capability to find the point for having a task which is reachable but
	 challenging is a very important point in the design of the
	 rehabilitation. This has a direct impact in the motivation of the
	 subject in doing repetitive tasks. The capability of making different
	 rewarding and engaging tasks can deal a lot with the use of games and
	 we call it augmented reality (different from virtual reality): you are
	 moving your arm and you see on the screen that you are reaching a
	 target, taking a cup or doing a task. It's about giving subject a
	 video feedback about task achievement that can be tailored on
	 different environments which are to engage the subject as much as
	 possible. An example is Hocoma, one of the leading industries in
	 rehabilitation robotics in Switzerland: their best game in their
	 setting of rehabilitation robotics was about taking the cow and
	 putting the cow outside of the road, milking the cow and move the
	 bottle of the milk to the farm. The tailoring of the game needs to be
	 as much as possible moving from repetitive task to functional task
	 with a goal.
8. Volitional contribution: it deals with the whole engagement of the
	 motor planning till motor execution. Volitional execution can be
	 included in the design of the therapies in terms of using as much as
	 possible the residual capabilities of the subject
9. Personalization: the capability of the therapy to be tailored to the
	 requests of the single user and to be able to capture the changes
	 across time and to adapt the target and the methodology of the
	 rehabilitation exercises of the training to the current capabilities
	 of the user. It's not just an initial customization but a continuous
	 customization that needs to be put in place.

### Slide 2

All these elements can be achieved using different methodologies of
rehabilitations which include both technology-based and
physiotherapy-based (traditional rehabilitation) rehabilitation. Every
time we are talking about technology-mediated rehabilitation we are not
excluding the therapist. This human role needs to be kept inside the
loop. The point in technology-mediated is the fact that the therapy in
not completely guided by the therapist. This needs a change of mentality
of therapists but also a lot of attention of engineers which develop
therapies.

## Page 5

### Slide 1

Reasons for considering conventional therapies as limited practice in these days:

1. the whole appropriateness about physiotherapist-mediated therapy is
	 inside the expertise of the therapist. We can have very expert
	 therapist or not so expert. This can unbalance the efficacy of the
	 therapies that we are doing and that the patient is receiving. 
2. Also we might have that the limited availability of the therapies: If
	 our system is only based on therapist-assisted rehabilitation we will
	 have post-stroke people admitted in hospital for rehabilitation for 4
	 weeks and they can do the activities only half an hour in the morning
	 and half an hour in the afternoon. This means that, across 12 hours
	 day, they are really training for an hour and this is mainly due to
	 the manpower shortage. The bottleneck is the availability of
	 therapists. The relationship between therapist and patient is one to
	 one. If we try to use technologies in order to help the therapies to
	 deal with two patient at a time each single patient can do, instead of
	 half an hour in the morning, one full hour in the morning. This
	 doubles his training and since the first rule is "the more the
	 exercise, the better the outcome", if we increase the possibility of
	 training this makes a lot of difference.
3. Repetitions: of course the problem of repeating task is very boring
	 both for therapist and the patient. In the end what happens is that
	 the therapist is repeating mobilization of joint but he's talking of
	 whatever else in order to let the subject be doing the repetitions as
	 many times as possible and not keeping the training so boring. This
	 makes the subject not concentrated on the task, so we need to have a
	 lot of repetitions with the subject concentrated and focused to what
	 he's doing without having a boring session.
4. Monitoring the therapy. We want the therapy to be as much as possible
	 tailored on the subject and tailored on the changes that happen in the
	 subject across time. If we don't measure this progress, the capability
	 to cope with the progress and to adapt the feature of the therapy is
	 limited. If the therapist is the only person that has direct
	 interaction with the subject, the only way to capture the subject
	 progress is let to the capability of the therapist to understand
	 what's going on. 
5. Limited modulation of therapy. This is a critical point. If we have an
	 expert therapist we will have high achievement but if he/she's not
	 expert we will have poor results.

We have to think of using the technology in order to help the therapist
inside this whole story. Rehabilitation robotics *help the clinicians*.
Never think of rehab robotics as something that substitute the therapist
or the clinicians. Technology enters the scene to help clinicians and to
achieve the best results in the end.

### Slide 2

## The goals

The goal is to have a clinical translation of safe, simple to use, engaging and
functional devices for assuring maximal recovery. This recovery can be either
done during hospitalization or also in continuation after discharge. It's
another important point.

We have the accident, then there's a first phase (few days/weeks) in
which the patient is stabilized (the overall situation of the accident is
"freezed" and not worsening anymore), then acute phase of rehabilitation
(6-9 month). In this period we have a time window in which the patient is
staying in the hospital, the length of which depends on the health care
system (i.e. in Italy, it's up to two months), then what happens is that
the patient is discharged and there's a wide window where training could
be very effective. This window can be split into different phases:
out-patient (the patient is no more in the hospital but called every 3
days a week for having some therapies) and another phase in which the
patient is left to his own situation. This means that technology could be
used not just in hospital. The goal is to span the use of the technology
from the near the time time the accident occur to when it's needed (in
out-patient and eventually at home). When we move to "at-home" we are
making a gray area between rehabilitation technology (helping the patient
regaining the ability of doing tasks) and assistive technology (which
help the patient doing tasks at home). Let's look the recovery graph on
the exercise book. By dividing into the different phases, we have that in
the stabilization window curve is exponential, as we were saying before,
and the slope of the curve is very steep, meaning that recovery is
happening really fast in that period. In the acute phase there's still
the exponential trend and the curve's slope is decreasing. After the
acute phase the recovery has not really reached a real plateau but
recovery is not evolving really much.

## Page 6

### Slide 1

We need to understand why robotics can work inside this domain.

Let's first see what are the most relevant solutions in rehabilitation robotics
and then try to go back and see how these elements are coupled with the key
essential requirement that we have mentioned about neuroplasticity. 

### Slide 2

We use rehabilitation robots (applied to upper and/or lower limb) that
are classified into two:

1. end-effector robots: applies mechanical force at the end effector (to
	 the distal segment of the limb). You just control the end effector,
	 it's easier to setup and control. The disadvantage is that we don't
	 have a control of the proximal joints. If we consider the schema of
	 the first lecture, reported on the exercise book: end-effector robot
	 produces the end point pathway. An example of this is the MIT-Manus.
	 The robot doesn't have any supervision of what is happening at the
	 proximal joints. What happens at proximal point could be very risky
	 about the patient because we might have incorrect posture occurring
	 which might be not beneficial or could be painful for the patient. We
	 consider that the physiotherapist is always there so the problems can
	 be controlled and mitigated by the presence of the therapist but from
	 a technological point of view, this kind of robots are unsupervised.
	 This is the reason why the other kind of robots are available.
2. exoskeleton: the robot elements are aligned and attached to the
	 anatomical axes of the subject. The robot has a complete knowledge of
	 each joint trajectory and degree of freedom. This means that we have a
	 direct control of individual joints and can cancel/avoid any abnormal
	 posture and set specific ranges of postures for specific subject but
	 we pay all of these extra functions with more complexity and cost for
	 the device.
	 
There's another solution that now is very trendy which is about soft robotics.

## Page 7

### Slide 1

Soft robotics: there are not rigid components. Exos are typically based
of metal bars going in parallel with body segments while soft robots are
basically based on cables (not rigid or minimally rigid). Robot becomes
more compliant to possible contribution by the subject: the interaction
between subject and robot is less rigid. We will see that there will be
solutions for having also metallic exos to be more compliant and less
rigid.

Soft robotics deals with human-robot interface and actuation system and
in the end respond to high portability (less bulky), they assure safety
input (reducing the force we put on the subject), we can let the subject
contribute to the motion.

In this direction the possibility to have wearable robotics has opened a
lot also in industrial applications. We started from superman, we stepped
back to disable subjects and now to industrial applications (soft exos to
help workers in daily tasks). This loop is very positive because if we
are able to use devices for industrial application we have a huge market
and a lot of money and we will have a positive effect of the development
and research for rehabilitation.

### Slide 2

In this slide the top part is referred to upper limb, the lower part to
lower limb. There aren't rehabilitation robotics before the 90s. First
robotics were all about manipulandum (end effector solutions). There were
also end effector solution for the lower limb (Gait trainer reha-stim
Berlin): robot is moving the feet one foot after the other following a
pathway without controlling knee and ankle. The beginning of '00 we had
the first exoskeleton solution. In 2001 we had the first gait trainer
which is LOKOMAT by Hocoma. Now we have a lot of devices that are based
on exoskeleton which are both for upper and lower limb.

## Page 8

### Slide 1

These are the available product for upper limb. Hand is the most tricky
part because of the many degrees of freedom which should be controlled
autonomously with many degrees of freedom for each fingers, the available
space is small. The problem about the robotic actuation of single
movement of the hand is the most challenging.

### Slide 2


## Page 9

### Slide 2

How do we evaluate robots and whatever intervention wants to tackle the
point of disability?

Let's consider ICF (International Classification of Functioning,
disability and health), it is a World Health Organization.

Let's focus on the three definition of functioning and disabilities by the
World health organization

1. *Body function and structure*: selective capability to perform a
	 specific task and movement and having a possible limitation in the ROM
	 or in the strength or something like that. It's a very quantitative
	 measure.
2. *Activity*: the capability of performing movement is tested in relation
	 to the capability of doing a specific activity. For example we are not
	 only testing an arm reaching but we want to test the capability of
	 reaching a glass and taking the glass to the mouth. We always need
	 functions in order to go to activity but we may have function and not
	 activity (activity is at higher level with respect to functions). It's
	 a quantitative measure: we can check the capability to do a specific
	 task.
3. *Participation*: the highest level of all. The level of capabilities
	 that we have allow us to participate efficiently to our desired life
	 and it means that the participation deals a lot with the quality of
	 life and it's measured by questionnaires and self perceptions of the
	 patient. Participation is a much more subjective evaluation of the
	 subject. 

All these three levels deals with the health condition (presence or not
of a pathology and the severity of it) but deals also with the contextual
factor (environment and personal factors).

### Slide 1

## Critical view of upper limb rehabilitation robot

The rationale for rehabilitation robotics can be synthesized into 

1. High-intensity and high-dosage training.
2. Task-oriented training.
3. Personalized training.
4. Possibility to increase task difficulty.
5. High level of safety.
6. Reduced effort for the physiotherapists.
7. Possibility to assess motor performance while training.

So far, there's no clear evidences from clinical study of the efficacy of
rehabilitation robotics.

There's a huge study, the [**RATULS (Robot Assisted Training for Upper Limb after Stroke) trial**](https://www.thelancet.com/journals/lancet/article/PIIS0140-6736(19)31055-4/fulltext),
that was recently published on Lancet London Journal. It includes 700 patients.
It aim at evaluating MIT-manus (intervention 1), a 2D end-effector robot. It's
a very basic tool, a manipulandum. The comparison is Upper limb therapy
(intervention 2), which is manual physiotherapy aiming at the same upper limb
but focused on daily activities and functional tasks. The control is actually
constituted by the traditional therapy. The results are quite controversial. 

- By comparing the first intervention with the control group, we can notice an improvement in upper limb impairment (e.g. level 1, function domain) compared with usual care.
- By comparing the first intervention with the second one, there were not improvement in upper limb function or activity of daily life (level 2, activity domain).

Results about intervention 2 showed improvement compared with control both in
function and activity domain.

Each domain (function, activity and participation) has its own measurement
scale to measure improvement. In the design of the study, there were both
scales evaluating body structure and activity domain.

Robotic training based on MIT-manus with respect to control showed improvement
at functional level and not at the activity level, while EULT (intervention 2)
showed improvement at both levels.

In conclusion this kind of robotic training is having:

- High-intensity and high-dosage training.
- Possibility to increase task difficulty.
- High level of safety.
- Reduced effort for the physiotherapists.
- Possibility to assess motor performance while training.

It's lacking in:

- Capability of engaging the subject
- Task-oriented training: MIT-manus allows the reaching task all over the
	plane, like Georgopoulos experiment and this results in a good support to the
	recovery of function but not a real support in doing activities. At the end
	of the day it was not training activities but just motions.

## Page 10

### Slide 1

One hypothesis given by Krebs, one of the major professors working on
robot-assisted therapy in MIT and the father of MIT-manus. He published at the
end of 2019 an overall review where he was claiming that robotic therapy should
focus on impairment training, combined with therapist transition-to-task
training to translate the impairment gains into function.

Robot works at level 1, function. Therapist-mediated training is focused to
level 2, activity domain. Robotic is a kind of facilitator, it prepares the
body to have a higher function so that the therapist can focus only on the
activity domain. This point of view is in line with the outcomes of RATULS
study. Maybe it's in line with manipulandum on 2D plane like MIT-manus but
could be a reductive view if we think about complex robots with a lot of
degrees of freedom and more challenging interaction designed. GOTO page 13,
slide 2.

## Page 13

### Slide 2

This is MIT-Manus, te commercial name is "InMotion2 Shoulder-Elbow Robot" and
it's a 2D end-effector based.

## Page 14

### Slide 1

This is ReoGo, another commercial robot, D end-effector based. The working
volumes include also vertical tasks.

### Slide 2

Myomo is not CE approved and discontinued. It has sensor on the skin which
allows to detect EMG. It moves the arm and use muscle signal to control the
movement. 

## Page 11

### Slide 2

This is Armeo. There are different versions: it can be powered or passive. It's
an exo so we have a structure that runs in parallel to the body segments.
There's a 3D workspace, coupled with virtual reality video-based games for
engaging the patients.

## Pag 17

### Slide 1

Reha-Stim are end-effector based: they just move the feet of the subject which
are fixed on a structure which is then moved. There's an harness (in it.
imbracatura) system that allows to assure safety of the task and to release
partially the body weight, to make the task more simple for the patient.

### Slide 2

The same is for the LokoHelp. The main difference is that the ankle is fixed
and patients move on a treadmill system. There's harness system as well.

## Page 16

### Slide 2

Motorika is an exoskeleton system: knee and ankle joints are assisted
specifically along the axis of the body and it allows the patient to contribute
the movement and allows the patient to contribute to the movement and provide he
remaining force necessary to walk. Wight bearing can be tuned. The interface is
abut giving a biofeedback to the subject so information about the task
performance.

## Page 12

### Slide 1

Lokomat is the most wide spread gait assistance exoskeleton. It has a passive
joint at the ankle and motors both at the hip and at the knee. The subject is
weight supported and there is possibility of gaming for the subject to be
engaged.

## Page 19

### Slide 1

Walkbot is another version of exoskeleton for gait assistance.

## Page 12

### Slide 2

Another domain is free walking: over ground walking, not on a treadmill nor in
a fixed gymnasium. They were thought for augmentation of healthy subject or for
spinal cord injured patients. They are actually becoming relevant in
rehabilitation because there are studies that demonstrate that walking on a
treadmill has different characteristics with respect to walking over ground.
This happens because the pacing of the treadmill modulating differently the
synergies used by the subjects in the natural walking overground.

The issue about these devices are about portability (they must be light in
order for the patient to be able to move with them on board) considering also
battery and computer weights and volumes. There are usually sensorized crutches
(in it. stampelle) and through both information about trunk orientation and
crutches weight-bearing, the activation of the step is controlled.

## Page 13

### Slide 1

ReWalk is a similar over ground exoskeleton.

## Page 16

### Slide 1

KineAssist: This is just a weight-bearing system, it supports the weight and
let the subject do the task freely, without any assistance to any joint.

## Page 18

### Slide 2

The same is this case (ZeroG): subject is free and activation is just to
support the weight bearing.

## Page 15

### Slide 1

Amadeo is a robotic system coupled with visual games and it supports the
rehabilitation of the hand.

## Page 20

### Slide 1

This last part of these slides are the guidelines of American Heart Association
for post-stroke survivors. It says each single possibility of therapy is
evaluated in terms of quality of the evidence (the LEVELS) and in terms of
efficacy (the CLASSES).

|                                                 | *CLASS I*: Benefit $\ggg$ Risk | *CLASS IIa*: Benefit $\gg$ Risk  | *CLASS IIb*: Benefit $\ge$ Risk | *CLASS III*: No benefit or harmful |
| :---                                            | :---                           | :---                             | :---                            | :---                               |
| *LEVEL A*: Many RCTs or meta-analysis           | Should be performed            | It is reasonable to be performed | May be considered               | Not useful/harmful. Avoid          |
| *LEVEL B*: Single RCT or non randomized studies | Should be performed            | It is reasonable to be performed | May be considered               | It is reasonable to be performed   |
| *LEVEL C*: Very few population evaluated        | Should be performed            | It is reasonable to be performed | May be considered               | It is reasonable to be performed   |


## Page 21

In this slide a lot of therapies are mentioned and the highlighted ones are of
our interest. Let's have a look on them.

| **Recommendation for cognitive impairment**                                                    | Class | Level of evidence | Evaluation                          |
| :---                                                                                           | :---  | :---              | :---                                |
| Exercise may be considered as adjunctive therapy to improve cognition and memory after stroke. | IIb   | C                 | May be considered, very few studies |
| Virtual reality training may be considered for verbal, visual and spatial learning.            | IIb   | C                 | May be considered, very few studies |

| **Recommendation for spasticity**                                                                                                                                    | Class | Level of evidence | Evaluation                       |
| :---                                                                                                                                                                 | :---  | :---              | :---                             |
| Physical modalities such as NMES or vibration applied to spastic muscles may be reasonable to improve spasticity temporarily as an adjunct to rehabilitation therapy | IIb   | A                 | May be considered, high evidence |

| **Recommendation for balance and ataxia**                                                                                    | Class | Level of evidence | Evaluation                          |
| :---                                                                                                                         | :---  | :---              | :---                                |
| Individuals with stroke should be prescribed and fit with an assistive device or orthosis if appropriate to improve balance. | I     | A                 | Should be performed, high evidence  |
| Postural training and task-oriented therapy may be considered for rehabilitation of ataxia.                                  | IIb   | C                 | May be considered, very few studies |

| **Recommendation for mobility**                                                                                                                                | Class | Level of evidence | Evaluation                         |
| :---                                                                                                                                                           | :---  | :---              | :---                               |
| Intensive, repetitive, mobility task training is recommended for all individuals with gait limitations after stroke.                                           | I     | A                 | Should be performed, high evidence |
| AFO (ankle foot orthosis) after stroke.                                                                                                                        | I     | A                 | Should be performed, high evidence |
| NMES is reasonable to consider as an alternative to AFO for foot drop.                                                                                         | IIa   | A                 | It is reasonable, high evidence    |
| Robot-assisted movement training to improve motor function and mobility after stroke in combination with conventional therapy may be considered.               | IIb   | A                 | May be considered, high evidence   |
| Mechanically assisted walking with body weight support may be considered for patient who are non-ambulatory or have low ambulatory ability early after stroke. | IIb   | A                 | May be considered, high evidence   |

| **Recommendation for upper extremity activity**                                                                                                                      | Class | Level of evidence | Evaluation                           |
| :---                                                                                                                                                                 | :---  | :---              | :---                                 |
| Robotic therapy is reasonable to deliver more intensive practice for individuals with moderate to severe upper limb paresis.                                         | IIa   | A                 | It's reasonable, high evidence       |
| NMES is reasonable to consider for individuals with minimal volitional movement with the first few months after stroke or for individuals with shoulder subluxation. | IIa   | A                 | It's reasonable, high evidence       |
| Mental practice is reasonable to consider as an adjunct to upper extremity rehabilitation services.                                                                  | IIa   | A                 | It's reasonable, high evidence       |
| Virtual reality is reasonable to consider as a method for delivering upper extremity movement practice.                                                              | IIa   | B                 | It's reasonable, single RCT or other |

| **Recommendation for adaptive equipment and medical devices**                                                                                                                                         | Class | Level of evidence | Evaluation                           |
| :---                                                                                                                                                                                                  | :---  | :---              | :---                                 |
| Adaptive and assistive devices should be used for safety and function if other methods of performing the task/activity are not available or cannot be learned or if the patient's safety is a concern | I     | C                 | Should be performed, very few studies |

### Notes

- NMES: NeuroMuscular Electrical Stimulation
- Ataxia: a neurological sign consisting of lack of voluntary coordination of
	muscle movements that can include gait abnormality, speech changes, and
	abnormalities in eye movements.
