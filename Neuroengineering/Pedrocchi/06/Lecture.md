# Lecture 6 --- Neuroprostheses

## Page 1

### Slide 2

## FES, Functional Electrical stimulation

Neuroprosthesis is based on electrical stimulation: artificial stimulation is
used to activate the muscles or the sensors of the natural system. Experiments
go back to Galvani but has been used in humans for motor recovery from about 40
years ago (Liberson, 1961, Serbian school).

## Page 2

### Slide 1

FES is a stimulation able to *induce the contraction in a muscle* without its
neuronal control, in ordr to obtain a useful funtional movement. (Vodovnik,
1971, Serbian School).

We have three main ingredients:

1. Inducing contraction of **the muscle**.
2. muscle has no natural neural control.
3. goal: **to obtain a useful functional movement** (i.e. flexo/extension of
	 elbow)

Now the approach of using electrical stimulation and neuroprosthetics is more variegate and complex.

A more recent definition was established (2008) and it's still valid:

Functionl electrical stimulation is the technique of applying safe levels of
eletric current to activate the damaged or disabled neuromuscular system *in a
coordinated manner* in order to achieve the lost function.

The ingredients:

1. Activation of **neuromuscular system**.
2. coordinated activation of multiple muscles.
3. goal: **to achieve the lost function** (i.e. taking a cup and bring it to
	 the mouth). This means that FES is task-oriented in the more recent
	 definition.  new definition

Neuroprosthesis is a device that uses elctrical stimulation to activate
the nervous system. These activation initiate a physiological-like
stimulation in the intact peripheal nerves, providing a functional
restoration of various body organs

external artificial stimulus --> nerve --> (physiological-like) --> body parts (muscles)

There's an activation of a natural pathway which is triggered from the
external stimuli at the very beginning.

### Slide 2

We have to consider that FES is used in very broad applications in medicine:

1. Muscular strength.
2. Maintenance or increase of joint mobility (e.g. minimizing
	 stiffening).
3. Minimization of muscular fibrosis (FES keeps muscular tissues
	 healthy).
4. Reduction of spasticity and associated pain.
5. **Reorganization of voluntary movement**: we can go back to brain
	 plasticity. FES applied to re-learning is the **focus of this lecture**.
6. Pain reduction
7. Improvement of peripheral vascularization: as soon as we activate the
	 muscles, the mechanical effect, as well as metabolic effect of
	 activation of the muscles increase the call of inflow of arterial
	 blood. It also helps venous return because of the mechanical
	 functions. This activation of the peripheral vascularization is a very
	 important aspect because the problem of source of decubitus ulcer
	 deals with lack of vascularization (also with mechanical pressure). 
8. Physical training of the muscular masses: the muscular tone is
	 improved by functional electrical stimulation. Physical training of
	 the cardiovascular system: this is important for people that has long
	 term immobilization.
9. Maintenance of bone mineralization: as soon as we unload bones, the
	 internal bone organization are no longer stimulated, so they tend to
	 become fragile. If we keep loading them by activating muscles, we keep
	 the stimulus of mineralization of bones and they are not prone to
	 fractures.
10. Prevention of thrombosis: this deals with peripheral vascularization.
11. Deep brain stimulation: a completely different electrical
		stimulation. All the previous points in the list deal with activation
		of the muscle while the DBS is using ES by wires implanted inside the
		brain in order to activate/deactivate specific regions of the brain.
		This is used for Parkinson disease patients, emicranias some
		psychiatric problems and epileptic seizures. It's really interesting
		story because what was used to do by clinician was to have surgery in
		order to cut parts of the brain which were known to be pathologically
		activated during specific pathologies (brain tomy). At a certain
		point neurosurgeons realized that it was possible to put electrode
		and give very high frequencies stimulus and that was working
		similarly to surgeries but the level of invasivity for the in-between
		tissue was much less. This was done for the thalamus for Parkinsonian
		patients (globus pallidus internus and externus), resulting in quite
		good clinical outcomes.  They tried to put the same kind of wires in
		different brain areas for different pathologies where there were no
		available treatment. This is a very interesting approach to think
		about because it's not something that we still have a clear
		understanding of the mechanism underneath the efficacy but there's an
		efficacy that is proven by clinical outcomes. There are clinical
		protocols that explain what kind of stimulation to perform in
		specific cases but we still don't know what is the model behind, what
		the treatment is really doing to that part of the brain and why This
		action on that part of the brain result in a positive clinical
		outcome. All of these are still open questions. This is again
		clarifies the difference between the clinical and scientific
		approach: sometimes clinics goes faster than science, while sometimes
		science comes first. To have good rationales for proposing treatments
		in clinical environment we need to prove the efficacy while the
		complete understanding of the scientific mechanism is not really
		necessary. This is something that when we work with clinician spot
		differences between these two worlds. Clinicians want to be useful to
		that single patient so they don't have the need to understand
		anything before proposing the treatment. This is important to make a
		common acknowledge that it's different but it's both worthy.
		Clinician wants to understand but they don't need to when there's the
		need to put it in practice in order to save a life.
12. There are specific muscles activation, for example stimulation of the
		sacral roots for sphincter control.
13. Recovery of function during posture and movement.
14. Phrenic nerve stimulation for assisting respiratory functions.
15. Pacemakers for cardiac assistance.
16. Specific stimulations for supporting coughing in tetraplegic.

## Page 3

### Slide 2

### Sensory restoration (not part of exam)

There's another that we are not dealing with in this course which is
about the sensory restoration. Neuro-prosthetics to stimulate afference
to the brain in order to provide back the subject with a sensory feedback
(i.e. tactile feedback for hand prosthesis). Providing back sensory
feedback to the brain of the subject is one of the most important
directions of research about prosthesis aiming at higher embodiment of
the prosthesis. This because, if the subject receives back the
information from sensors in the prosthesis (i.e. in surrogates of
fingertips or soil of the foot), they start feeling again the prosthesis
as part of their real body.

## Basis of FES

It's based on stimulation. It usually activates nerves rather than
uscles because the activation threshold of nerves is lower with respect
to motor neuron axons.

Electrical stimulation --> | activation of axons of motor neurons | --> muscular fibers

In order for the FES to work, *the axons of the motor neuron must be
intact*, so as the neuromuscular junction (e.g. the interface between
neurons and muscular fibers).

There's a current that is delivered to a volume of tissue between two
external stimulation electrodes. This current is producing an electrical
field in the volume of tissue between the two stimulation electrodes.
This electrical field creates an ionic flux inside the tissue. The
electrodes translates an electrical current that runs through the wires
into an ionic flux to the tissues underneath the electrodes. We then have
an hyper-polarization (abundance of anions) underneath the anode and a
depolarization underneath the cathode (abundance of cations). The
transmembrane voltage is increased and there's the AP in that specific
point in space (under the cathode). As soon as the spike is generated, it
runs through the axon's in a natural way (orthodromic direction) and in
the opposite direction (antidromically). This happens because the only
echanism that imposes a direction to the spike is refractarity, which is
not happening because the spike starts from the middle of the axon, so
the cells are all in resting condition. 

## Page 4

### Slide 1

Look at the exercise book for having an idea about the body circuit and
the interaction that happens when stimulating. Consider that nerves have
both sensory and motor fibers connected to the muscles that are
stimulated both orthodromically and antidromically.

### The stimulation parameters

There are different parameters that need to be taken into account:

1. Current amplitude [A]
2. Pulse width (aka PW) [µs]
3. Tension [V]
4. Stimulus frequency [Hz]: inverse of the distance between two
	 subsequent stimuli.
5. Stimuli shape: what we have usually is that it's a dual phase (e.g.
	 biphasic pulse) stimulus to avoid an unbalance between positive and
	 negative pulse. The quantity of charge delivered by the positive
	 stimulus (which has the efficacy in triggering the spikes) is
	 balanced by an equal negative area in order to have a concentration
	 of ions that is balanced in the tissues underneath the two
	 electrodes. Avoiding the ion accumulation at the tissue level is a
	 guarantee for the tissue to be healthy.

By multiplying the first two parameters we obtain the quantity of
charge (area of the first graph) [C].

### Slide 2

## Electrode types

The other major differences, when we design electrical stimulation is the kind of electrodes that we are using:

- *Surface electrodes*: they are put on the skin of the subject just
	above the belly of the muscle, so that the activation goes to the
	motor axon nerves and it's close by to the muscle.
- *Percutaneous electrodes*: they provide an invasive stimulation. They
	are electrodes composed by wires, at the end of which there are
	needles going inside the muscles. We can have mono-polar or bipolar
	percutaneous needle.

The main difference between these two different approaches are about:

|                      | Surface | Percutaneous |
| :---                 | :---    | :---         |
| Invasiveness         | No      | Yes          |
| Selectivity          | Low     | High         |

- *Long duration* (related to surface electrodes): Stability of the
	system depend on the interface between skin and electrodes.  We don't
	have a persistence of the coupling between them and that's the reason
	why we need to re-define parameters of stimulation, from time to
	time, if we want to have long duration stimulation.
- *Volume of activation*: when talking about surface electrodes, it is
	wide (about muscular size), providing low sensitivity. There can be
	difficulties in activating deep muscles (i.e. hamstrings: knee
	flexors). When we are referring to percutaneous electrodes we have a
	more narrow volume of activation that can even go deeper.

## Page 5

### Slide 1

## Natural muscular contraction

Increasing the stimulus intensity (that can be measured as quantity of
charge) we have the consequent muscular contraction. There's an initial
deadband which deals with the threshold and then there's a saturation.
We can notice, from the second graph the sigmoidal (e.g. non-linear)
shape of the contraction force: there are strong non-linearities both
in the first and in the last part but the middle part behavior is
linear. Even if the stimulus intensity is linearly increased, the
effect is non linear, excluding the middle part.

### Slide 2

There are some elements that make the artificial stimulation (FES) very
different from the natural muscular activation:

1. *Asynchronous activation*: our brain is capable to activate fibers in
	 an asynchronous way. This means that we can have one motor unit
	 which is active now and another MU active at a different time stamp.
	 This assures that low frequency activation of multiple units assure
	 a stable force. And there's a turnover in fiber activation in the
	 natural activation. In case of FES we have one single frequency
	 activating all the fibers that goes beyond the threshold with the
	 quantity of charge that we are delivering. We are not capable of
	 interleaving the activation of the various parts of the fibers, but
	 we are synchronously activating all the fibers that we can reach.
2. *Progressive activation*: This difference deals with the type of
	 muscular fibers we are activating. In the natural contractions we
	 have that brains tend to activate first Type I fibers, then IIa and
	 IIb. This sequence is used by our brain in order to optimize the
	 capability to support long-term tasks and deals with fatigue. Indeed
	 the first fibers which are activated are those that assure small
	 contractions and short recovery (resistant to fatigue). When higher
	 forces are required our brain starts activating types IIa fibers:
	 medium resistance to muscle fatigue (depending on the different
	 metabolism) but higher forces. When we are requested of a very high
	 contraction our brain recruits type IIb fibers (anaerobic), assure
	 high forces and fast contraction but they are very prone to fatigue.
	 This means that our brain tends to optimize the recruitment of
	 fibers depending on the capability to resist to fatigue. We can
	 resist with small forces for a long time while if we are required of
	 a huge and abrupt force then our capability to resist will be
	 shorter. What happens is the fact that the fiber diameters are
	 inversely proportional to the activation thresholds (the higher the
	 diameter, the lower the threshold). The diameters of the fibers are
	 increasing going from type I to IIa to IIb. So artificially i won't
	 be able to distinguish the fibers but i will be able just to
	 distinguish the threshold. By tuning the threshold i will tend to
	 activate first the large diameter fibers (IIb) and then the other
	 fibers: the order of activation is not driven by an optimization of
	 the metabolic characteristics of the muscular fibers but just by
	 their thresholds and position in space. The artificial stimulation
	 recruits fibers in a non-selective, spatially-fixed and synchronous
	 way.

## Page 8

### Slide 1

On the x-axis we have the quantity of charge (current \* pulse width). On the top of the slide there are representations of nerve bundle cross-sections. In one of these representation we can notice that there are small and big axons. On the top of the nerve bundle there's an electrode.

The horizontal axis of both the top diagram and the bottom graph is
representing the increasing quantity of charge (in the top diagram we
are time-by-time increasing it).

1. Under-threshold for all the fibers
2. Beyond threshold of large fibers that are closer to the electrode.
	 So there's a dependence on the size and the location of the fibers.
3. Start recruiting more deeper large fibers and superficial small
	 fibers.
4. ...
5. Stimulus is capable to recruit all the fiber of the muscular nerve.
	 That's the saturation point.

From point 2 to 4 we have that linear behavior we have seen before,
while point 1 and 5 represent, respectively, the deadband and the
saturation condition. The linear behavior is arranged on the type of
fibers in a different way with respect to natural contraction. This
different way depends on two elements:

- Diameters of the fibers
- Spacial arrangement and distance from electrode.

### Slide 2

If we want to keep a stable contraction (aka stable force), what
happens is that in case of natural contraction we have a stimulation of
the units which is about 10 Hz, while in case of muscular artificial
contraction we need to increase up to 20-30 Hz.

## Page 9

### Slide 1

## Differences between natural and artificial (FES) contraction

|                         | Artificial  | Physiological |
| :---                    | :---        | :---          |
| Fiber activation        | Synchronous | Asynchronous  |
| Motor units turn-over   | No          | Yes           |
| Motor fiber recruitment | IIb-IIa-I   | I-IIa-IIb     |

### Synchronous activation

Synchronous fiber activation in artificial contraction means that fibers are
contracting all together depending on the frequency of the external pulses.
Frequencies of external pulses determine the frequency of activation in this
case.

### Artificial motor fiber recruitment

It depends on the location of the fibers with respect to the electrodes and the
diameter of the fibers. Large diameter fibers are recruited before small
diameter ones (against an optimization of fiber selection for fatigue
management).

## Limitations

There are two:

- Higher muscular fatigue. A much more difficult issue when doing artificial
	stimulation.
- It's difficult to perform fine modulation of the contractions.

### Slide 2

We have a reduction of the muscular fiber cross-section (atrophy).
Sometimes these atrophy cause a reduction of Type I fibers which are
converted to Type II glycolitic fibers. Overall we need muscular
training periods to use FES in order to increase the base muscular
force and volume and resistance to fatigue.

Fatigue is a real problem when it comes to deal with patients that
haven't been using their muscles for a very long time (i.e. SCI): in
this case few stimulations are provoking fatigue. This doesn't happen
so easily for hemiplegic patients or with post-stroke patients.

## Page 10

### Slide 2

## Neural basis of FES for brain plasticity

What happens when we stimulate muscles in terms of effects at brain level? We
have to keep in mind that our attention should be focused on *rehabilitation*,
so we would like to be able to change brain plasticity in order to recover the
functions. 

There has been a lot of studies trying to understand the effects at brain
plasticity levels of the stimulation at axonal levels (muscular activation).
These studies are based on TMS (Transcranial Magnetic Stimulation) and fMRI.
TMS allows to measure the excitability of a specific area. In this case the
scientists were stimulating healthy subject and considering the different
effect of NMES (NeuroMuscular Electrical Stimulation. NMES=FES but sometimes
NMES can be underneath the motor threshold). They were comparing three training
paradigms:

1. Stimulation and volitional control of the subject (NMES + VOL).
2. Stimulation by itself (NMES).
3. Volitional control by itself (VOL).

The result is that when electrical stimulation is combined with
volitional control we have an amplitude of excitability of the brain.
This is in line with study used where NMES+VOL improved the prediction
of sensory consequences of motor command. So NMES+VOL has ah higher
cerebellar activity compared to NMES alone and a reduced bilateral
activity in the secondary somatosensory areas compared with volitional
control alone. Only healthy subjects were considered here. So the first
step is: trying to understand what happens in the brain of an healthy
subject when we do peripheral electrical stimulation? Is electrical
stimulation alone as benefiting as the combination between electrical
stimulation and volitional control by the subject? Is NMES+VOL
different from VOL? The answer is always yes. NMES+VOL is different
both with respect to VOL and NMES alone. Actually the kind of take home
message of this study is that when you combine FES with volitional
control you are amplifying the effects at brain levels. You are driving
more coordinated information to your brain in order to activate
plasticity and this is very important. In robotics we wanted the
robotic action to be combined with volitional control by the subject:
this to avoid slacking but also when we talk about bio-triggering of
robotic assistance, it was in order to combine volitional control with
robotic assistance. We are finding again this kind of combination
(volitional control with external inputs) as major ingredients in order
to provoke brain plasticity.

## Page 11

### Slide 2

In this case we were studying 17 healthy subjects with fMRI, comparing
FES+VOL, FES alone. We can play with contrasts of fMRI and check which
pixels of the brain volumes correlates with a specific experimental
condition. In this case we are studying (FES+VOL)-(FES): the real
difference bwtween the use of FES in case of combination with
volitional control and the passive case (alone). We can notice that
there's a huge activation in M1 and S1 (specific activation of M1 and
S1 areas of the brain, which deals with the fact that FES is combined
with volitional control). On of the major requirement that we need to
put in place in order to elicit as much as possible the plasticity of
the brain, the remodeling of brain mapping for relearning in rehab is
to combine volitional intention and NMES.

## Page 12

### Slide 1

The best paper who tries to understand what is really happening for
this combination to occur is by Rushton, 2003 (a great paper).

Rushton proposed an hypothesis on why we have maximal relearning effect when we
have the combination between volitional control and FES.

Rushton proposed that at spinal level we have Hebbian synapses. Hebbian
synapsis says that the more two fibers are synchronous, the more
strengthened their connection will be. If they fire together they are
amplifying their weight. 

A brain damage produce a lot of signal flowing from the brain which are
partly inefficient with respect to a real control of the muscle and
only a small part is really efficient. 

At spine level we receive all the signals and the poor control that we
have in the patients is because these many signals which are only
partly efficient are not weighted in the right way. When we apply FES
to the motor nerve we have both orthodromic and antidromic components
on the motor axon. The antidromic flow of activation goes back to the
interneurons in the spine. These interneurons (anterior horn cells) in
the spine, receive on the motor neuron side an input which is
synchronous with the muscular activation which is synchronous with the
muscular activation, so this input is synchronous with the good input
which is coming from the brain, the input which, in the brain, would
have caused the same muscular activation. This means that weights
related to those connections are strengthened while all the other
inputs which are asynchronous firing and making confusion are weakened. 

So at spine plasticity the antidromic volley of muscular activation
makes a kind of selection of the good volleys coming from the brain,
strengthening these connections, weakening the connections that are not
synchronous to the activation of the muscles.

Any time we are using together volitional control and FES we are
strengthening the part of the residual connections which are still
synchronous with the muscular activation.

## Page 13

### Slide 1

We have highlighted both for robotic control as well as for neuroprosthesis
(e.g. FES) that one of the major point about an efficient work is that we want
to interface the artificial devices with subject intention. Subject intention
is carried by some residual control which is not fully efficient.

This works both if we want to achieve an assistive goal, so we want to have the
technology supporting the subject in doing the task which is no more possible
to be done autonomously and in rehabilitation, so when we want the subject to
relearn. The combination between intention of the subject and the action
produced by the technology support is crucial both in restoration setting in
assistive devices and for relearning so in rehabilitation devices.

### Slide 2

What are the key elements in the design of a device which allows us to
interface with subjects? Those are sensors: we need somehow to collect the
information about what the subject intends to do. This picture is taken from
the medical instrumentation (Webster). 

## Page 14

### Slide 1

Actuator can be both the robot or the FES. We want to achieve information about
intention of the subject and include this inside the control system of the
devices.

### Slide 2

This applies for neuroplasticity, which we want to reconnect intention to
action (Rushton hypothesis) but also all the studies about fMRI and TMS.

## Page 15

### Slide 1

In the case of assistive neuroprostheses we have a chronic damage which induce
disability which prevent the subject to do some tasks by himself, limiting the
independence of the subject. In order to promote autonomous living, the goal of
restoration is to provide subjects with natural control of an efficient
function, which is assisted by technologies. Here technologies is applied both
for robot and neuroprostheses. Here again the point is to reconnect intention
to actions. If the subject wants to grasp and he/she is not capable to do that,
the robot should be capable to be controlled by the intention of the subject
for grasping and assure an efficient assistance in daily life. 

Also when this does not have the goal of motor re-learning: i.e. a subject who
has muscular dystrophy. Muscular dystrophy has a progressive profile over time
and recovery is not happening. In this cases we want to use technology for
making the subject being able to perform independently a task (not
re-learning). The problem here is not brain plasticity but about muscular
pathology (no more functioning of this section). In this case it's important to
reconnect intention to action just for assistive devices, not in rehabilitation
framework.

### Slide 2

We have many places in the brain where the intention of the action can be collected and can be recorded:

- Brain areas that include both deep and cortical areas in the brain
- Nerves
- Muscles

Information about the intention of doing the task is volleying through all this pathway and we can try to understand where this information can be collected by proper sensors and used to control the devices (NP or robots). 

We can have:

- EEG-based control devices: basing the controlling by recording field
	potentials by scalp electrodes (head of the subject). The devices are ere
	called BCI (Brain Computer Interfaces): a computer is used to carry out the
	decoding of the intention and the conversion of this intention as a control
	signal for the device.
- Electrocorticograms (ECoG): these record field potential but underneath the
	skin of the subject, so an implant. This implant can be done at the level of
	the skull with epidural or subdural electrodes. In this case we have an
	invasive surgical procedure.
- MEA (Multi-Electrode Array): brain array where we have, depending on the size
	of the electrodes and the array, we can have single units (very rare) or
	neural population inside the brain. They can be implanted not just on the
	cortex but also more in the in-deeper structures. It is invasive.
- Electroneurogram: collecting information at the nerve level. We are not doing
	an implant inside the brain but at the level of the nerves or through the
	nerves. There are different nerves electrodes (cuffed electrodes that
	embraces the nerve with sites all around the nerve; TIME electrodes which are
	goes through the nerve with multiple electrodes. They go with a needle the
	nerve).
- Electromyogram (EMG): the last point in which we can collect the information
	is muscle. We still don't have a full understanding on how the brain is
	controlling an action. 
	
Somehow it's not that straight forward understand what and where to collect the
signal. We still don't know where the fine tuning of task is encoded inside the
brain, while at neural level this information is much more selectively
represented. There's a general understanding that the more peripheral we go,
the more selective the information we are recording. The more high level, the
more difficult is the decoding of the information.

A brain computer interface is constituted by:

Sensor --> Computer --> Device (NP or Robots)

## Page 16

### Slide 1

We can use, at brain level, surface electrodes (EEG) or electrocorticograms or
brain arrays.

At peripheral nerves level, we can use multiple solutions of implanted
electrodes. From less to most selective: Cuff, LIFE, TIME electrodes.

At muscular level we can get information from EMG or other sensors that can
help for kinematic reconstruction or implanted sensors inside the joints.

### Slide 2

Let's go through some examples of how EEG can be used to control NP (e.g.
BCI-controlled NP).

## EEG-controlled Neuroprosthesis (BCI)

EEG: we are using scalp electrodes to measure it. Now we have quite reliable
dry electrodes, which work quite well, especially for bald people and for
frontal signal. If participant has hair, it's quite difficult to have very
reliable dry electrodes working on M1 and S1 area, for example. 

### Settings

The use of wet electrodes is still the gold standard. Actually wet electrodes
are placed with gel, the same that is used to perform ECG but actually it's
sticky and making hair dirty and takes forever to be put and properly
calibrated in the beginning (we need to get the right impedance for each
skin-electrode coupling). It's a long procedure to start with. It takes 15
minutes if we want a full positioning of the standard international for EEG
recording (10-20). Then, there's a lot of signal preprocessing. 

### Data processing

What is really important is that if we want to grab and extract the intention
information we have some algorithms which have been proposed and validated:

- steady-state VEP (Visual Evoked Potential): it works on flickering
	information at different frequency shown at the participant and we can
	recognize the frequency that the participant is looking at. A participant can
	turn left or right depending on which UI element is looking at.
- P300 which are very reliable algorithms dependent on visual stimulus. We can
	recognize, in a certain occipital recording site, a waveform inside the EEG
	signal which is connected with the showing of the target. P300 is a wave that
	we see about 300 ms after a visual input and it occurs when the visual input
	correspond to the intention of the subject. The typical use for P300 is
	writing: we show a grid of letters and the participant selects one line and
	one column. As we show the participant the column or row where the target is,
	P300 is evoked.
- Motion imagery: the best solution. Natural brain rhythms that are modulated
	because subject wants to do task. This can be recognized, especially as
	synchronization or de-synchronization of signal of many electrodes. The
	reliability of this kind of algorithm is small. It can be high but for very
	few selections.
	
So the problems of these data processing methods deal with too few selection or
with screen-dependence.
	
### Problems/challenges

- *Time for setting up*: dry electrodes would be much better to use because
	they would be ready to use. The direction towards we want to go is dry
	electrodes.
- *Time for calibration*: especially when dealing with motion imagery,
	calibration is a major issue. It needs to be done for every single session.
	If we think about real clinical translation of these kind of devices, not
	just for research, we need to have a reduction of this timing.
- *Training* of the subject in order for him to be able to perform SSVEP or
	P300 algorithms can be quite short, while the issue about training becomes a
	problem for motion imagery. Actually we have this spatial-temporal filters
	that works on EEG signals but in the end we need the subject to be capable to
	control these filters in order to achieve good accuracy.
- *BCI illiteracy*: some subjects have been proved to be BCI illiterate. This
	is not fully understood but just observed.

## Page 17

### Slide 1

## MEA (Multiple Electrodes Array)

In this case we can use cortical arrays or specific implants inside M1 and we need signal pre-processing but all these aspects, except for having a surgery, are not very critical.

### Data processing

Quite complex. We need to model neural firing rate with respect of movement
features. We have seen that there are neurons which are connected to specific
movement features but we have also seen that it's not that straight forward
what that neurons are carrying in terms of movement features. There are neurons
controlling the fine grasping or power grasping, neurons that control
directions of the task, neurons that control the effort that we need to put.
But it's not that clear the relation between the firing of the neurons and the
movement features and in order to use efficiently a motor control based on this
signal we need to make some hypothesis and some training in order to link the
neurons that we are recording with movement features.


### Problems

- *Surgery*: brain is the worst place in which we can do surgeries. Blood brain
	barrier makes the brain fully weak in terms of capability to react to
	infections. The immuno-reaction at brain level is very poor because, usually,
	the blood brain barrier is doing most of the defence of the brain. If we go
	inside the brain with something that is directly in contact with the neurons
	and the patient gets some infection around that place, brain blood barrier
	was already overcome and the brain by itself is not defending much.
	Infections are very prone to cause death.
- *Calibration*
- *Required training of the subject* (very long). The subject needs to think
	properly in order to move the device.
- Is this the cheapest, safest and most usable solution? From a research point
	of view it's amazing.

## Page 18

### Slide 1

## ENG-controlled robot

Peripheral control.

### Settings

Peripheral nerves activity are measured through electrodes, such as cuff
electrodes or TIME electrodes.

### Data processing

The main applications where it has been used was triggering of drop foot
stimulation and the control of some hand NP.

### Problems

- Surgery.
- Required training of the subject.
- We don't know about illiteracy.

In the field of sensory neuroprostheses, the ones that give back tactile
information to the brain, nerves implants are the most probable used solution.
For motor applications they are not still used.

### Slide 2

## EMG-controlled robots

EMG is the place where we can collect, if it is present, without any
implantation, with surface electrode a quite selective information about the
task that the subject wants to do. We can collect that even if the subject is
very weak.  This can be used both for robot and FES control. Actually it's more
difficult in the case of FES control because FES is *producing a signal* that
is then collected by the EMG recording! That signal is kind of a shouting
signal, a huge artifact that makes the EMG incapable to collect the tiny
signals which are supposed to be there for volitional control. We need a
specific algorithm in order to be able to extract volitional control when we
have FES on the same muscle.

### Problems

The problems about using EMG-controlled devices, in case of surface electrodes
are the typical EMG problems

- Electrodes positioning
- Electrode-skin contact instability
- Calibration that needs to be done every single section
- Cross-talk from adjacent muscles
- Same muscle control only if a weak but functional activation is still present
	(the pathology has made the subject not capable to have a functional control
	but we still have a small signal of an efficient control).

## Page 19

### Slide 1

EMG-triggered FES has been proposed in many different commercial devices and
many different body districts. It's much easier than any myocontrol because it
means to have an efficient reading of EMG before the stimulation and you don't
deal with the FES artifact: to trigger the action you don't need to have a
removal of the stimulation artifact. If we want to control the action and check
for the volitional control during the action we need to remove efficiently the
stimulation artifact.

### Slide 2

The two different solution of myocontrolled neuroprostheses:

- EMG-triggered NMES: we need to record the residual volitional EMG to trigger
	the onset of some stimulation. This stimulation is applied in an open loop
	modality.
- EMG-controlled NMES: we want to use residual volitional EMG to *modulate* the
	stimulation intensity in a closed-loop modality. 

Our target was to have VOL+NMES to promote motor re-learning but now what we
want is, at least, to have EMG-triggered. At the starting point of the task
there's a combination of VOL residual signal with FES. Of course what we would
like to have is that during the whole task we have a combination between EMG
(residual volitional signal) and FES (to optimize re-learning).

## Page 20

### Slide 1

### Pros and cons

|      | EMG-triggered                                                                             | EMG-controlled                                                                                         |
| Pros | Simple to implement: signal is measured before the start of NMES                          | Assures the synchronization between NMES and voluntary effort                                          |
| Cons | No guarantees about the synchronization between NMES and voluntary effort during the task | More complex technological solutions are needed to be designed to assure the proper artifact's removal |

### Slide 2

### EMG signal during hybrid muscle activation

Hybrid muscular activation: muscle is activated both by the volitional residual control and electrically induced control.

What we will read on EMG is that we have in this sequence:

1. A big stimulation artifact.
2. *M-wave*: the read in the contraction of the muscle that which is induced by
	 stimulation.
3. Tiny fluctuations of signals after M-wave. Here is hidden the volitional
	 control of the subject. We have to understand that we need to work on this
	 signals in order to be able to read reliably these volitional EMG.

Volitional EMG: a stochastic signal with an amplitude of at least one order of
magnitude less than the M-wave.

## Page 21

### Slide 1

The standard amplification unit for EMG recording can't be used in presence of
NMES, because the stimulation artifact is the result of a potential difference
produced by the stimulation current between EMG electrodes. It can't be
rejected by the differential amplifier. The best way would be to have
stimulation artifact as a common mode noise, so that differential electrodes
are neglecting it but stimulation artifact is not a common mode noise. Its
amplitude is much greater than the EMG small signals. More than that it can
saturate and even damage the amplifier of a standard EMG device. Different
solutions have been proposed to face the problem of the suppression of the
stimulation artifact.

### Slide 2

The simplest solution is to make a blanking signal. Every time that we know we
are triggering the stimulation, we are disconnecting the reading of EMG. The
signal, during the stimulation, is disconnected and the blanking covers
stimulation artifact and the initial window in between stimulation pulses.

Blanking can be used like a first stage, additional device before any EMG.

## Page 22


### Slide 1

A second technological solution is to have a very low gain amplifier (we don't
want the artifact to put the EMG in a saturation situation. It takes time for
the high gain amplifier to go back from a saturation. At the end we are
saturating the amplifier and making it not being able to record the signal even
after the artifact. So if we take a very low gain amplifier we are able to run
through the whole range of the amplifier without saturating it also during the
artifacts) with high resolution ADC (because we should be able to convert also
very small signals, where the volitional control signal is) which allow to have
the stimulation artifact read but never going to saturate the circuit. This
alternative means that we are using a specific EMG device which has a low gain
amplifier and high resolution ADC.

When we want to improve the recording of the signal we usually have two directions of actions:

1. Building the best amplifier filtering and selection of the sensor
	 (*processing side*). We can either have a blanking or a low gain amplifier
	 with a high resoluted ADC converter.
2. *Operational side*: Experimental optimization of the recording: in the case
	 of FES+EMG what we want is that as far as possible we want to have that the
	 artifact is seen by EMG electrodes as a common mode signal. If it is a
	 common mode signal and we are using a differential amplifier, the CMRR of
	 the differential configuration of the amplifier assures the rejection of
	 most of the signal. So the trick is to choose the best configuration for the
	 electrode so that the stimulation artifact is a common mode noise as much as
	 possible.

### Slide 2


## Page 23

### Slide 1

Then, we need to work on the best way to position the electrodes:

- *Standard solution* is to separate recording and stimulation electrodes. From
	a chemical point of view, they are not different. We should be able to both
	read and stimulate on the same electrodes but this is not used because
	actually to use different positioning is a way to have parts of the
	stimulation artifact as a common mode component. By placing the recording
	electrodes horizontally and the stimulation recording vertically (as shown in
	the picture) we are able to eliminate partially the artifact, considering it
	similarly to a common mode noise.
- *Blocking window* (Blanking): the signal is zeroed for the first 20-25 ms of
	each inter-pulse period. This allows to have the removal of artifact and part
	of the M-wave. This means that the volitional EMG is then estimated from the
	remaining part of the inter-pulse period. M-wave is not completely removed,
	so we still have a big and a tiny signal on our recording
- *High-pass filter*: 20-30 ms after the stimulation pulse, only low frequency
	electrically-induced components superimpose the volitional EMG. This means
	that we take out the artifacts with blanking, then we put a HP filter, which
	takes out what remains of the M-wave, which is a slower signal with respect
	to volitional EMG and we can extract the volitional EMG. The stimulation
	artifact as well as M-wav is a very repeatable signal because it's
	artificial. What we can do is to use HP filters in order to cut the low
	frequency components which are due to electrically-induced stimulation on the
	EMG.

So to sum up we have three solutions on acquisition level (software or
hardware), so on processing side:

1. Blocking window (Blanking)
2. Low gain amplifier and high resoluton ADC
3. HP filter

and we have one solution in the operational side:

1. Electrode positioning

### Slide 2

## Linear predictive adaptive filter

Another solution that we can apply was firstly proposed by Sennels but now
implemented in the most recent solution. It's about having a linear prediction
adaptive filter. 

$EMG_{v}(k)=EMG_{r}(k)-\sum\limits_{j=1}^{M}{b_{j}EMG_{r}(k-jN)}$

The assumption is that the volitional EMG ($EMG_{v}(k)$) is a band-limited
Gaussian signal and the *M-wave is time-variant*. 

We want to be able to capture some variability of the M-wave. This M-wave is
variant because there's fatigue. Thus, the response to the same stimulation
might be different over time. What we can do is to keep some smoothing averaged
signal of in-between two pulses. In the inter-pulse period, we take a certain
number of previous inter-pulse periods (M) and we make a prediction of the
profile of the EMG signal between the two artifacts. Then we subtract this
prediction to the current EMG ($EMG_{r}(k)$). If M-wave is a low changing
signal, while the volitional signal is a random signal, we have that this
average ($\sum\limits_{j=1}^{M}b_{j}EMG_{r}(n-jN)$) of the previous
inter-pulses recording, is a prediction of the M-wave component taken by a
window which moves from M previous periods. Moreover, we weight this summation
by proper coefficients ($b_j$) which minimize the output energy of the current
inter-pulse period.

## Page 24

### Slide 1

In the end we can see that we try to put together the different solutions in order to take best of each of them:

1. *Electrode positioning* (Frigo)
2. *Blocking windows* (just for the very first 10ms). It's cutting the
	 artifacts but not cancelling the M-wave because we want to be able to
	 capture the volitional signal which is superimposed to M-wave and this
	 couldn't be possible if we blank for a larger period of the signal.
3. *Adaptive filter*: cancelling from the current signal the prediction of the
	 M-wave, which is based on the previous M inter-pulse periods.
4. Now that we have extracted the volitional signal, we want to understand
	 whether the subject is active with stimulation (if there's a volitional
	 component of the task done by the subject). In order to answer to this
	 question we can give as estimate of presence of the volitional signal the
	 integral of the module (e.g. sort of envelope, more of a LP filter) of the
	 signal extracted by subtracting the M-wave estimation. At the end we have an
	 estimation of the volitional EMG during the inter-pulse interval.

### Slide 2

### Proportional controller

What we can do now is this. Our goal is either to connect the intention to the
action or to check that intention is there with the action.  

We want to collect EMG, then detects the volitional EMG (i.e. with the
algorithm we just used) then, having LP filter (last point of the algorithm of
Ambrosini) and as soon as we have filtered volitional EMG, we put thresholds,
specific to the subject, and we can provide the stimulation as soon as the
signal is between those thresholds in a linear way. Thus, stimulation is
mimicking EMG of the subject, amplifying the contractions of the muscles,
following EMG of the subject. For example if we have a patient with a weakened
muscular condition, we can produce higher, more functional contractions
following his own volitional intentions.

## Page 25

### Slide 1

### ON/OFF controller

We can also have a sort of ON/OFF signal, not just the proportional solution we
saw before.

Stimulation is not following a specific increasing profile but it's just
triggered both in the ON signal (easy because there isn't any stimulation) but
also in the OFF signal, which is more difficult because we need to read the
signal during the stimulation. We activate and deactivate the stimulation
depending on EMG volitional control signal (the one on the bottom).

### Slide 2

There are few clinical applications of EMG-controlled neuroprosthesis:

1. EMG-proportional controllers. There are some interesting studies about hand
	 prosthesis controlled by EMG for assistive purposes in spinal cord injured
	 patients. Stimulation is close-loop controlled with EMG. This is applied for
	 daily tasks in tetraplegic subjects
2. EMG-proportional controllers that improve hand functions in post-stroke
	 patient. This is a rehabilitative purpose. One of the most tricky challenges
	 about simplifying the whole procedure is to be able to compact and use the
	 same electrodes for both stimulation and recording. This makes the whole
	 story much more difficult technologically but it's a direction that is one
	 of the next challenges to be really used in normal clinical practice.

## Page 26

### Slide 1

We can see an example of NP controlled by EMG and the tracks at the bottom are
showing the estimation of EMG and the PW (pulse width: the control signal of
the stimulation). We can see that the pulse width, in the case of stimulation
on, follow a thresholding on EMG signal. It's not proportional but it's flat
(dashed red line). It's activated and deactivated by proper thresholding on EMG
signal. 

We can see that the angle profile without stimulation (top left): subject was
doing kind of smaller angle profiles with respect to the desired angle profiles
that are the dashed black lines. The test subjects were three SCI patients. By
following their EMG we can amplify their muscular contraction artificially and
reduce the difference between the target angle profile and the measured angle
profile.

### Slide 2

Couple of project Professor have been working on.  Passive exoskeleton (anti-g,
supporting weight bearing of the arm), then we haveEMG electrodes. Blue
electrodes are used for EMG recording while gray electrodes are for
stimulation. Arrangement of electrodes follows the indication to have as much
as possible CMRR applied to FES artifacts. The idea was to have an anti-gravity
exo and, in order to make an efficient movement of the arm on the horizontal
plane, the EMG is recorded and used to extract volitional EMG. This volitional
EMG is used to drive the stimulation of ON/OFF EMG controller in order to
produce the PW stimulation, which then produces the muscular contraction by
using the external electrodes.

## Page 27

### Slide 1

This is a stimulator. We can either use a pulse amplitude (current as a control
signal) or the pulse duration. The quantity of charge is given by the area
under the reported graph. This works as a spacial summation.

The frequency, the distance between the two following pulse can be set higher
(closer pulses) or lower and this regulates the temporal summation.

### Slide 2

EMG, in this case we were using a EMG amplifier with very high CMRR and with
very high resoluted ADC and a low gain (in order not to saturate because of FES
artifact). In this case we are not using the blanking. We also used active
shielded cables to avoid movement artifacts: this is an experimental rule to
limit the quantity of noise that we are collecting on our signal.

The 3 DOF exoskeleton had its degrees of freedom at the level of shoulder
(abduction/adduction, elevation and humeral rotation).

## Page 28

### Slide 1

Also 3 DOF for the elbow. In that case we were aiming at helping people doing
daily activities and as soon as the activation was triggered, the stimulation
was amplifying the signal for patient who had efficient but poor control of the
arm. There were three solutions about how to collect the intention of the
subject:

1. EMG.
2. Eye tracking if EMG is poor. A very good control. For patient with ALS
	 (amyotrophic lateral sclerosis), it is one of the most residual function,
	 preserved all along the degeneration due to the course of the disease.
3. BCI for subject that didn't have a good control of the gaze. One of the most
	 significant result in that case was that it's quite difficult subjects which
	 have a poor control of the gaze and have a better control of BCI. With BCI
	 you have an accuracy that is quite good but not super good. 

### Slide 2

A continuation of that project but with another target application. MUNDUS
(MUltimodal Neuroprosthesis for Daily Upper limb Support) project was aiming in
daily life assistance, while RETRAINER was developed for rehabilitation
activities.

The reason for moving to rehabilitation was not that the assistance to daily
life was not efficient or wasn't deserving a further developing but, from a
market point of view, we ended up in a business plan which for the goal of
producing a more commercial device (because the final goal to reach the
patients). MUNDUS was not enough strong in a business plan point of view
because the target of the population is very narrow, while technology level is
very high (cost-achieved market ratio was not that good). The idea was: let's
move to rehabilitation where there's higher market and we can sell the products
not to the very final user but to the hospital, which can use the device over a
large number of different subjects one after the other during the day. What was
developed for rehabilitation is still keeping in mind the possibility of being used also for assistance in daily living.

To move to rehabilitation, we used the same EMG+stimulator and, again, an
exoskeleton with only passive components. Then there's a device for controlling
the whole state machine of the system and to drive the subject through a
sequence of training exercises, which were using a set of interactive objects.

Let's analyze every module.

## Page 29

### Slide 1

### Passive exoskeleton

Exoskeleton: it has 4 DOF (5 if we include trunk inclination):

	1. $\alpha$: trunk inclination.
	2. $\beta$: rotation about vertical axis of shoulder.
	3. $\gamma$: shoulder rotation in the elevation plan.
	4. $\delta$: shoulder elevation.
	5. $\varepsilon$: elbow flexion/extension.

### Slide 2

### EMG stimulator

Integrating channels both for stimulation and measurement: 4 stimulation
channels, two of which can be used with EMG-measurement function. The box was
including both EMG-reading and stimulation but actually the electrodes were not
integrated but separated electrodes.

## Page 30

### Slide 1

## NMES (aka FES) controller

NMES is triggered based on the residual EMG activity. 

Blue trace, on the last graph on the left, is the EMG volitional (posterior
deltoid). The red trace is EMG volitional of another muscle (anterior deltoid).
There are thresholds and, as soon as they are overcome, then FES are activated.
Activation is driven by the anterior deltoid profiles and as soon as the
anterior deltoid profile overcomes a threshold the stimulation is activated
with a ramp up phase (middle left graph) and then it reaches the target level.
When the EMG goes underneath a certain threshold. well actually it was
deactivated depending on the achievement of the exercise. Then, during the
maximal activation (green area, bottom left graph), the volitional signal was
extracted and, if that volitional signal has overcome a certain value, a smiley
face was shown to the user, showing that he's successfully collaborating to the
task execution (EMG volitional was active during stimulation). 

If the muscle doesn't reach a predefined threshold, NMES is started after a
time-out. This applies to robotic control and to FES control: if we are waiting
for the subject to trigger a task but he's not giving any triggering because
the triggering he's providing is too small or whatever, then we need to fix a
time-out parameter so that, after time-out has passed, the robot (or the
stimulation) starts the tasks by yourself. This is important for the subject
not to get frustrated by the unsuccess. 

A happy or sad emoji is visualized at the end of each task based on the subject
involvement. It's informing the subject about his contribution.

### Slide 2

We have seen restoration and rehabilitation. In both our targets we want to reconnect intention to action (neuroprosthetics). This NP or robots can be controlled by:

1. EEG
2. MEA
3. ENG
4. EMG

## Take home message

The main resource is always brain neuroplasticity. The brain of the subject
learns to control the external device. The optimal sensors and NP depends on
the target of the application and disability (in other words on the subject).

## Page 31

### Slide 1

### Slide 2

Managing fatigue. It's a nice story for a methodological point of view.

## Page 32

### Slide 1

The point is that we have seen that one of the issues about using FES was that
fatigue is occurring much faster. What we have is what we have to control
stimulation. When we want to control stimulation, this deals with system that
is non-linear behavior (sigmoid curve) with a linear range in between. We have
also talked about time-variability (the fatigue effect): the same system,
receiving the same input does not respond in the same way because of fatigue.
If we want to achieve a robust, smooth and accurate control we need to manage
the non linearity and time variability of the system to be controlled and we
can't neglect this aspect. 

How can we control FES? 

- By feed-forward: designing a profile of stimulation which is based on
	a-priori profiles with forms of stimulation depending on the task
- Feedback controller
- Model based controller that can also be adaptive
- Controller based on artificial neural network

### Slide 2

If we want to focus on the performance of the controllers, what should be done is to focus on a single movement and check if our control system works by using both stimulation and experimental trial.

We can stimulate also healthy subject to test our control system but we have to
keep in mind that for healthy subject it's not very easy to let the stimulation
working by itself. The completely canceling is quite difficult for healthy
subject while for pathological subjects we can assume that the only controller
of the muscle is the simulation itself.

The problem now is how to control the profiles of the stimulation in order to
have accurate task achievement, also across time (so against fatigue).

To test which controller is the best in this framework we need to start from a simple movement (knee flex/extension). We want to track accurately a sequence of movements by modulating the profiles of the stimulation. 

## Page 33

### Slide 1

To do this we need to start considering the whole system we are stimulating: both the muscle to be stimulated. In the example we have a plant (simulation of knee joint control). We have at least 5 muscular group working across the knee joint. These muscular groups have both active and passive properties:

- active: achieved by the stimulation.
- passive: elastic and stiffening properties of the tissues themselves.

Also passive properties need to be considered if we want to make a good
tracking FES system.

### Slide 2

In order to control the simulation we need to convert pulse width into angles.
If we talk about internal models of the brain we are converting motor command
into movement features. In this case, this is the equivalent of a forward
model. The model of the plant is a forward model, a model of the real system.
Pulse width is converted, by considering the recruitment levels, the calcium
dynamics and muscular fatigue, into an activation profile. This activation
profile is converted into a torque by a proper contraction rules which depends
on the force/length and force/velocity relationships. This activation torque is
included into the dynamics of the system which take into account the passive
properties of the joint (viscosity and elasticity) as well as the active input.
Taking into account all of the components we can estimate the executed
movement. When we do an open-loop control, what we do is that the patient (or
something else) gives the command, then a stimulation profile is delivered with
a profile that is a-priori defined and sent as a sequence of stimuli to the
muscles. Then the feedback is only given back by the sensory feedback of the
patient which might give the next command or not.

In this case we have a very fixed stimulation trains and the patient need to be
very concentrated inside the loop. There are some manual controllers but also
some automatic controller that can be used (heel switches under the heel of the
subject to control stimulation of peroneal nerve to control drop foot).

As we are dealing with feed-forward control we don't have any compensation of
unexpected disturbances.

We produce a fixed movement, so there's no modulation of the movement and
there's no control to fatigue.

Open loop controls are efficient, they depend on the subject in the loop and
they provide a-priori fixed stimulation of the muscle that does not take into
account unexpected external perturbation and fatigue.

## Page 34

### Slide 1

Feedback controllers can be used in order to adapt and control the stimulation
depending on an error signal. This error signal can be calculated by comparing
the desired target movements with the current actual movement. The simplest way
to use a feedback controller is to use linear controllers including all the
three components (Proportional, Integral and Derivative components. PID). In
this way we are taking into account the current value of error, the variation
of error across time and the overall error occurred in the previous time
windows with proper PID parameters that are set in order to best tune the
compensation for disturbances.

PID has the advantage to be capable to capture unexpected error, following it.

## Page 35

### Slide 1

In the upper panel we have the desired knee angles (in blue), actually measured
angles (dashed black).

In lower panel we have the parameters for the stimulation.

- PID is a linear controller which is commanding a non linear and time-variant
	system.
- It's difficult to identify the PID coefficients ($k_{p}$ $k_{i}$ $k_{d}$).
- There's a delay in the control which counts for an error an includes an
	increasing profile of the stimulation across the following target movements.

At a certain point we can see, in the top graph, an abrupt decrease of the
measured angle and a final non capability for the subject to perform the task,
even though the stimulation (bottom graph) is at its top level. The phenomenon
of this temporal region is *fatigue*. The controller is capable to try to
follow the target trajectory for a while but, as soon as fatigue occurs, the
controller is amplifying the stimulation in order to compensate for fatigue but
amplification has exactly the opposite effect by amplifying further the
fatigue.

### Slide 2

## Model-based control

One possibility to avoid the initial delay of PID is to have an inverse model
inserted. Model-based control is a feedforward control. A feedforward component
is corrected by the feedback component (the one of PID): the two components are
summed together to produce the final movement. The problem of having an inverse
model is that we need to estimate the pulse width which is expected to produce
the desired trajectory. This means that we are somehow inverting (doing an
inverse model) the neuro-musculo-skeletal system, which is a very non-linear,
subject-dependent system. If we are dealing with subject which have pathology
at neuromuscular level the generalizability of the model of the neuromuscular
system is a very strong assumption. We should be able to develop an inverse
model for each single subject. While we are doing a good combination between
feedforward and feedback components we are not including any non time-variance
of system. We need to customize on single subject.

## Page 36


### Slide 1

We need to have an inverse model which is adapted by an adaptive part algorithm
in order to follow the profiles of fatigue for the subject. Again we are
inverting neuromuscular system and we are inverting it at every single run and
adapting it to a new situation of the subject. This kind of paradigm is the
target. 

So in the end we will have a feedforward model which is adapted across time, a
feedback model which is implemented in order to compensate for unexpected error
which are not fatigue.

### Slide 2

If that is the general target framework there are some issues that still are
needed to be faced:

- personalization (*difficulties in parameter identification*).
- inversion of neuromuscular system, which is a non linear system (*non
	linearity*).
- estimation of the profiles of the change of parameters due to fatigue
	(*time-variability*).

In this framework we can claim that ANN can be used to solve these issues
because ANN have:

- Black box approach
- good Generalization and transfer learning
- Adaptability to time variability of the system
- Suited for controlling system which are time variant and non linear

All these positive advantages need to be supported with a good and large
training dataset. Whatever application we are working on we can use "artificial
neural network" efficiently only if we have a large dataset to train the
network, then we can exploit the possibilities for the network to capture
non-linearity, time variability and good generalization and adaptability.

## Page 37


### Slide 1

The first question is "can we have good examples in order to train a network to
behave as an inverse model?" If we can use a network as inverse model we are
solving one of the problem that we had which is about having a single subject
neuromuscular system and inverting it. Can we collect a training set? Yes,
because if we take the plant (the subject) and we provide the subject with a
sequence of stimulation of pulse width and we collect the resulting movement
parameters (angle, angular velocity). We can measure the parameters of the
occurring movement as sequence of set of stimulation pulse widths that we are
providing to the muscle and use these data in an inverse way to train an
artificial neural network that works as an inverse model! This inverse model is
subject specific and can also be collected on the same operational experimental
settings as the one we are targeting at the end for our training. We are not
asking the subject to do something very different but just doing a pre-training
session to the subject where we are giving pulse width and collecting movements
without any aim to target a desired movement but then we have all the data to
understand and capture the link between PW and movement on the same subject and
we can use the same data to train ANN to behave exactly like the
subject-specific inverse model.

### Slide 2

In this way we have a neural inverse model which can be combined to a PID
feedback model and we can compare it to a PID-only system. The PID-only will
have for sure delay because this implies having the poles of the transfer
function.

## Page 38

### Slide 2

What is interesting is shown in this slide. Black dashed line is representing
NeuroPID, while red dashed line represent PID-only. The desired trajectory is
the blue line. We can see that the PID has a delay (pink curve is always right
sided in the angle). While neuroPID is following the ramping up very steep with
the time of the desired angle. This has a huge impact on the pulse width
profile: neuroPID starts deeper in increasing the pulse width at the beginning
but what happens is that in between the angle profiles the pulse width goes
back to zero for it (this does not occur for PID only). This is so important
because not having any time for the stimulation to go back to zero means not
having time for the muscle to rest. This continuous stimulation amplifies the
fatigue effect.

## Page 39

### Slide 1

NeuroPID comprise a neural inverse model and a PID as a feedback.

- reduces delay effect typical of PID controller.
- Overstresses the system in order to follow the desired trajectory.
- Overcomes the problem of intermediate solicitation.

A huge improvement with respect to PID alone is not obtained and instead we
have to train the ANN inverse model.

### Slide 2

contenuto richiesto e spiegato in questa lezione

## Page 40

### Slide 1

A "tricky" way to build neural network. We have seen how to collect dataset for
training an inverse model. Can we collect a proper dataset and used an ANN to
tune the feedback component of the model, so to be able to capture the time
variability of the muscular properties of a single subject?

### Slide 2

That's the tricky answer. We can use no extra collection! We want the subject
to be as low as possible bothered by the preparation of the experiment. Time of
patient and of the operator is really precious and we need to use very few of
it. The idea is: we have inverse model (already trained) which is easily
collected by just making sequence of stimulations and collection of movement
parameters on specific subject. This inverse model captures the behavior of the
subject within a certain time window and we can suppose that fatigue is just
partially captured by that inverse model and it depends on how long we have
been collecting the data. We can also admit that fatigue is not captured by the
ANNIM (Artificial Neural Network Inverse Model). What is ANNIM doing? We have a
desired movement and ANNIM is providing the pulse width which is nominally
required by that subject in order to achieve that target movement, then we use
this PW on the subject and we collect an actual movement which might be
different from the desired one. $\delta q$ is the error in angle or movement
parameters. PID is converting this error into a PW compensation profile. The
same needs to be done by whatever feedback system that we use, we want to
compensate for the occurring error: we watch the error in the movement
framework and we want to map this error into an extra stimulation (e.g. extra
motor command) in order to compensate and reduce that error. How can we
estimate the level of extra stimulation that we need to compensate for the
error? We have the real and the desired movement performed by the subject, the
nominal PW that was supposed to produce that desired movement but the subject
is indeed producing another movement which is the actual one and having an
error. We can use the same inverse model and convert the actual movement that
the subject is performing into an actual pulse width: what would have been
enough in a nominal situation to produce that real movement? The difference
between the desired pulse width and the actual one can give an estimation of
the level of fatigue of the subject and can be used to train the output of the
feedback.

Let's do an example with numbers. we want to do 90° movement. The pulse width
to produce a 90° movement is 350µs. We give these to the subject which is
performing in the end an 80° movement. We want to compensate for this error, so
the error is 90-80° (desired-actual). Nominally I should have been able to
achieve 80° with a stimulation that was just 300µs, so the 50µs which is the
difference between what was expecting to produce 80° and what is actually
producing the 80° can be used to map the fatigue level of the system at that
exact moment, so it's used to train the feedback network.

This solution is controlling both in a feedback and a feedforward way the
stimulation training the whole system with just the preparatory acquisition on
the same task for the single subject. Operationally, we are not asking the
subject to do anything that burdens.

## Page 41

### Slide 1

We have a compensation of fatigue (time variability) and an inverse model which
is set on a single subject.

### Slide 2

EMC (Error Mapping controller) is the combination of ANNIM (Artificial Neural
Network Inverse Model) and NF (Neuro Feedback) solutions. We are not really
doing a compensation for the error but we are mapping the error.

We can see that EMC:

- never gets to saturation of the pulse width
- it has a decrease profile over time, which is due to fatigue that is though
	lower with respect to other controllers

## Page 42

### Slide 1

This can be measured.  The following graph is showing three slots of 30 seconds
each. 

We can see that in the three slots EMC is having a smaller error in the time
profile with respect to the desired trajectory (tracking RMSE).

### Slide 2

If we put a distributed noise (a random noise over the whole system that can
come from any sources, from the erroneous application of the electrodes or by
the electrode-skin coupling which is unstable or changing in muscular
condition...) we can see that EMC has an inevitable decrease of performance,
due to fatigue, with the cycle of the movements. EMC error is always less than
the other controllers so it's mapping for whatever error sources and it's not
mapping specifically for one trained error.

## Page 43

### Slide 1

We mimick the occurrence of spasm during a lot of conditions (raising, return,
extension...) and PID is tending to over compensate for the error and increases
the error time after time. PID has a huge error in the beginning that is due to
the delay which occurs in the very first cycle. All these bars is composed by a
second element: the component on the top is the one given by the spasm. We can
notice that this component is controlled in the same way by all the controller. EMC is not decreasing the capability to generalize on the spasm.

### Slide 2

The final goal is that we want to be able to reduce calibration times and to
exploit as much as possible the generalization of the network also changing
subjects or also changing day sessions.

## Page 44

### Slide 1

These results are in simulation. We are simulating different parameters of the
subjects. The plant is no more the subject but a simulated subject. On the
simulation we can play with the parameters of the subject we are simulating.

These parameters of the subject can be represented by a set of parameters like
the mass of the muscle, the time for recovery for the muscles, passive
properties of the muscles and time for fatigue.

In this graph we are varying (increasing or decreasing) each of these
parameters measuring the tracking error performed at the very first cycle
(empty circles are for first cycle, stars are for fifth cycle, with some
fatigue).

Changing the parameters (we can set a threshold on the acceptable error) and we
can see that EMC system is answering quite well to variation up to 50% upward
or downward of both the damping and the time for recovery. If fatigue is very
increased we have that fatigue error is increasing very much and we will have
to accept a certain threshold of error (i.e. 10°) and check that the subject's
changes of fatiguing properties is no more than 20%. If the subject is
improvement the fatigue effect is less visible. Same threshold approach can be
applied also in the case of variation of the muscular mass. We will have some
values that allows us to say we can accept a decrease of 35% till about a grow
of 15-20%. These tests are telling us that within certain range the network is
capable to generalize and to achieve good results even if the subject is
different. One session after the other we can check for the muscular mass just
by measuring  the circumference and check if this mass is not changing out of
the -35 to +20% we can keep the same controller, otherwise we collect new data
and update the inverse model to the new condition of the subject.

All this to say that there are conditions in which the model is able to
generalize and some ranges in which the model is not really good to generalize.
