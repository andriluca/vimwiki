# Lecture 7 --- In-vitro neuroengineering

## Page 1

### Slide 1

The final topic of this course is about using technologies in order to
interface neuronal network in-vitro.

### Slide 2

Why is this target so important?

We have a problem of multiple scale. From molecules which govern the
information between the different neurons: it works in tuning the plasticity
and the behavior of the neuron.

Passing from the single unit function to an information transfer is
possible to be captured only if we go from single neurons to
microcircuits and network of neurons. Network of neurons are the first
levels where we can start capturing some information transfer between
the brain structures. 

This information transfer is what is managing the behavior which is the
other side of the multi scale story. The problem of studying network of
neurons is very important in order to comprehend the information transfer
and information coding between neurons 

## Page 2

### Slide 1

Before going in the description of the technology, we should say that
single-neuron level as well as whole CNS-level are quite well known while
the dynamics of neuronal networks (of microcircuits) are far from being
elucidated. 

This is an important claim.  What we are targeting is to understand is the
coding of information of small microcircuits where the focus is neither on
the single-neuron nor the whole brain but on a small subset of neurons
working together.

We have seen in computational neuroscience that the same kind of step is
done when we move from compartmental models (which are single neurons) to
microcircuit level. What we do is to use point neurons (we have considered
a balance between the computational load and the biological plausibility,
wanting to strike the happy medium between the two). At the same time we
need, from a biological point of view, to capture this level of the
picture. This capability to record microcircuit levels opens some very
interesting technological challenging, which is the goal of this final
lesson.

### Slide 2

The story has this multi-level situation. We can use both in-vitro and
in-vivo studies. There's not a one-fits-all solution but the choice we have
to make is depending on the goal of the study that we want to do.

- In-vitro: we have two directions that are the reported ones. If we want
	to move from single-neuron level to microcircuits levels we can either
	work with:
	
	- Neuronal networks of dissociated cells: coltured networks derived from
		embryo neurons of rodents. Neural cells in the rodent embryo are still
		not connected because it's an early developing phase. We take the
		neurons and culture them in-vitro (with ideal condition like humidity,
		pH). The network is then formed in-vitro (in petri). In this way we
		have a model of a neuronal network formation. It has been proved that
		this in vitro formation of networks has a lot of similarity with
		respect to natural networks in the brain, with the advantage that we
		can see this process along the time profile from dissociated neurons
		till a full formation of the network. We see the first pruning of the
		dendrites, then first connection and maturation of the network. We can
		take it out of the incubator and make a recording one day and then put
		it back and go back and forth at different time stamp.
	- Brain slices: we are sacrificing the animal when it's already mature.
		Biologist cuts slices of the brain and put them in the petri with all
		the environmental conditions in order to keep it alive. The network has
		been formed in the animal and not in vitro. It's a real network inside
		the brain but we won't be able to capture the formation of the network
		but we have like a snapshot of the network formation. We have a
		one-time point recording. It's much more difficult for biologist to
		keep alive a brain slice: we can have just a couple of recordings from
		it and then it is not useful anymore.
	
	In both of these cases we have an in-vitro network which can be study at
	the level of microcircuit.

- In-vivo

## Page 3

### Slide 1

For in-vitro experiment:

- *Slices* are functional naturally grown tissues which are extracted from the
	brain and then analyzed.
- *Coltured neurons*: embryonic dissociated neuronal cells which are cultured
	in vitro and built the neuronal network directly in-vitro. The system is
	completely autonomous but it is only a model of natural functional networks.
	This because the natural formation of the network in the brain depend on a
	lot of structure interfacing one with each other.  Interaction between
	different types of neuron create simply different kind of networks. It's very
	difficult to have multiple neuronal population in vitro (the so-called organ
	on a chip). There are attempts to do that but for the brain it's a big
	challenge.
- (Possible) *Human patient specific IPS* (Induced Pluripotent Stem cells)
	which are differentiated to neuron-like. They take cells of an adult
	(epithelial cell for example) and they are able, by proper biotechnological
	processes, to induce to go back to a pluripotent stem cell, which means that
	it goes back in a form of "neonatal" cell which can be driven toward forming
	different possible expression. This IPS cells can be driven toward
	neuronal-like cells. In this way we are building a neuronal like cells
	in-vitro by starting from the specific single subject cell. This has a huge
	advantage of being patient-specific but on the other side the capability to
	differentiate to neuron-like is still a very on-going process, this is one of
	the future direction.

We have these three models and we need to measure the activity of the
coltured neuron in order to understand how the information is processed.

There isn't an absolute best solution but depending on the question we are
investigating, sometimes we will have better answers by studying slices,
sometimes by studying coltured neurons.

It would be nice to think about what kind of experiment would most benefit of
using one type of in-vitro solution with respect to the other.

### Slide 2

## Page 4

### Slide 1

Let's start from the target requirements of this recording. What we would like
to have.

1. *Record and stimulate*: We want to be able to both record the activity of
	 hundreds individual neurons as well as stimulate them. This is the base to
	 say that we are studying the information transfer in neuronal network.
2. *Long acquisition in a stable situation*. Long because we want to understand
	 how the information is processed. Plasticity is our goal and occurs over
	 time and if we want to capture this property which is the base for learning
	 and behaving, we need to have long acquisition.
3. *Monitor the transmembrane potentials* (the electrical information that
	 occurs inside the neurons) which are spikes. We know that single neurons
	 speak a language which is based on the variation of transmembrane
	 potentials, subthresholds and thenspiking activity.
4. *Catching subresholds transmembrane potential* much lower signals which
	 described the behavior of the cells which provoked spikes. Then spike
	 occurrence and spike oscillation. We know that the information is inside
	 spike trains, but also in the way spikes are formed. This is based on
	 subthreshold oscillation of membrane potential.
5. Depending on the cells that we are recording, we would like to *record AP
	 with different amplitudes*. There exist many types of excitable cells and
	 they have very different profiles for the AP. Neurons tends to have a
	 duration of few milliseconds and an amplitude which is about 100mV.
	 Cardiomyocites have longer and wider action potential.

### Slide 2

Let's look at this picture. It shows us, in the plane of time/spatial
resolution. Time resolution is capability to discriminate two following in time
stimuli. The spatial resolution is the capability to discriminate two inputs in
the space. Depending on the goal of the research we might want one higher than the other. Depending on these characteristics we can pick the best technology in order to study the brain.

- EMG and MEG have not a very good spatial resolution (1cm cube) but they have
	very high time resolution (ms).
- fMRI: high spatial resolution (1mm cube) but in order to take all the slices
	it needs a few seconds to scan the whole brain, so this mean low time
	resolution.
	
There's another aspect that makes the difference there: what part of the brain
can be recorded. fMRI as well as PET has the possibility to study the whole
brain (cortex, subcortical structure, cerebellum, brain stem and so on). EEG,
MEG, NIRS, ECoG have all information about the cortex, so the closer part of
the brain.

If we talk about microcircuit we are considering the bottom left part of the
graph (high time resolution and high spacial resolution). We want to have 1ms recording and very small voxel. For in-vivo recording we have two different solution:

- Local Field Potential (LFP): when we insert wires and electrods inside the
	brain and record the activity of a cluster of neurons around that electode.
- Multi Unit Arrays: wires or matrices put inside the brain and which can
	record by electrodes the activity of a very tiny part of the brain with high
	temporal resolution.

So we can say that the three elements to evaluate the technical solution in
order to study the brain are:

1. Time resolution.
2. Spacial resolution.
3. Volume of acquisition.
4. Hardware complexity and price.

## Page 5

### Slide 1

Let's stay in in-vitro condition and see what are the methodologies available.

1. *Patch clamp*: we use transmembrane patch inside the neuron and we measure
	 the voltages across the membrane of the single neuron. It allows the
	 recording of a specific cell's transmembrane potential and we can record
	 both subthreshold voltage and spikes. We have a full recording of what is
	 occurring across that specific membrane. We can also use different drugs in
	 order to block/activate different ions channels and see how the voltage is
	 modulated. Patch clamp is the gold standard to study all the mechanism
	 inside the single neuron about its electroresponsive properties.
	 **Advantages** We have a really clear description of cause-effect mechanism,
	 a full understanding of who is doing something and where. An accurate
	 readout of the dynamic of voltages without distortion. A very reliable
	 measurement with very high temporal resolution (kHz) and single cell
	 recording (also compartmental). We can reach subcell spatial resolution.
	 **Disadvantages**. We are indeed passing with electrodes through the
	 membrane in order to record transmembrane potential and that means that we
	 are making a hole inside the membrane. A hole means that we are perturbing
	 what we are going to measure. This applies especially to long registration:
	 by going on with time, the consequences of the hole are increasing and, in
	 the end the cell dies. The invasivity limits a lot the duration of a
	 reliable recording. This limited the possibility to have repeated
	 experiments because the length in time of experiment is very limited. The
	 second limit is about the number of cells that we can record. We have
	 mentioned that the neuron is the computational unit inside the brain. The
	 information content is about the connections of neurons. This means that if
	 we want to understand the signal processing between the neurons we need to
	 record more than a neuron and see how the changes in one neuron provoke
	 changes in another neuron and to see how the electrical activity of one
	 neuron is modulating the electrical activity which is passed to a chain of
	 cells but, since patch clamp has a physical encumbrance and we need to do it
	 under the microscope. Biologists need to precisely patch the cells that they
	 want to record, all of this under the microscope. They can use
	 micromanipulators in order to position the pipettes for the recording. On
	 one in-vitro colture we can have a few manipulators working together because
	 of the physical encumbrance of those stuff, at the end we reach up to few
	 neurons recorded. It's not possible to consider a solution with small number
	 of cells because neurons, in order to grow, need to work together with the
	 others. If we make very rare cells they are very unnatural, artificial and
	 far away from a real model of what is happening in the real brain. This need
	 to be considered as a limit. The consequences of these two aspects is that
	 we have an instability of the biophysical and mechanical properties, which
	 means that the system is not available for long-term electrophysiological
	 recordings
2. *Microelectrode Arrays (MEA)*: They are coverslip where we have inside the
	 layer of the slip a substrate of integrated matrices of electrode. These are
	 the external pads of the electrodes and the external pads are connected
	 through wires that are inside the glass. There's a sensible area which is
	 the electrode which is exposed to the medium of the culture. Underneath the
	 colture we have a matrix of electrodes which can record the extracellular
	 activity, so we are no more able to to measure the transmembrane potential
	 because the electrodes are placed differently. What we can record is the
	 electrical field that is conducted by the medium which is conductive since
	 it's based on water and measuring the extracellular potential. From the
	 picture at page 6, slide 2, we can see that all around the electrodes we can
	 find the different soma of the neurons. One electrode is recording the
	 extracellular activity of a cluster of neurons, not a single neuron. We are
	 losing the capability of recording a single neuron but the advantage is that
	 we can record many sites. We have increased the volume of the recording but
	 we are paying in terms of spatial resolution, recording cluster of neurons.
	 The output of the recording is shown at the Page 6 slide 2: each single
	 square is the plot of the time profile of the recording of one single
	 electrode. There's a kind of matching about the spatial visualization of the
	 traces across time of the different electrodes under the culture and the
	 physical disposition on the glass. The time resolution is super high because
	 it's electrical recording.

### Note

coverslip: vetrino portaoggetti

### Slide 2

## Page 6

### Slide 1

What is the content that is recorded by MEA? We have extracellular space, which
is conductive but its resistance is very low but not zero. Extracellular
current result in a small voltage that can be measured with extracellular
electrodes. Extracellular signal are much smaller than transmembrane potential,
there's an attenuation signal from the source, which is the transmembrane
potential to the recording place, which is out of the cells. This attenuation
depends on the distance between the source and the electrode. 

### Slide 2

If we are here comparing the recording of intracellular (patch clamp) and field
potential (extra cellular) we can notice that, in case of baseline spiking, aka
endogenous spiking (the first graph line) we are able to capture the spikes
(the tiny signals on the right). We can't measure any under-threshold behavior.
We have a kind of thresholding of sensibility of the measurement. Extracellular
recording can measure whether a neuron has fired or not and not about the
sub-threshold mechanism that has provoked the spike trains. This is very clear
in this picture in which the spike trains are all rising extracellular activity
but for very different causes. I am losing the ability to recognize the causes
that could be discriminated with a normal patch clamp.

1. Endogenous spikes, a baseline activity: a neuron itself that reaches the
	 threshold and then starts the spike.
2. Excitatory Post Synapses Potential (EPSP): input from a pre-synapses cell
	 (excitatory synapse) which cause this scale up of the sub-threshold
	 potential reaching the threshold of the neuron and then the spike
3. IPSP: stopping of an inhibitory pre-synaptic input.

From extracellular recording i am not capable to distinguish these three
condition.

## Page 7

### Slide 1

This is because the signal that we are recording outside has an attenuation
between the source (neuron) and the recording. This coupling between neurons
and electrode depends on the adherence of the neurons on the electrode sensible
areas. The adherence of junctional membrane of the electrode pads improves the
SNR. The more adherence, the higher SNR. Neuron is generator.

### Slide 2

There are lots of studies in order to improve SNR and the coupling between
electrode and the neuron. Some of these has been exploring shapes of electrode
which goes inside the neuron without breaking the membrane to enlarge th
portion of the membrane of the neuron that is in contact with the electrode. In
this way SNR improves and, eventually the signal recording can be more
accurate.

## Page 8

### Slide 1

If the goal is to improve SNR we need to reduce the electrode impedance, which
is the attenuation produced by the electrode on the signal that we are
recording. To reduce the electrode impedance we need to enlarge the size of the
junctional membrane, this mean to make larger electrode. Larger electrodes
cause, though a worsening of the spatial resolution. There's a trade-off we
need to select between spatial resolution and SNR. 

Another solution is to develop gold Mushroom-shaped microelectrode, so that
spacial resolution is not that limited because the shape of the electrode is
like mushroom (the top is enlarged) and this is embraced by the neuron. The
whole membrane around the mushroom is increasing SNR. The more we play with the
shapes of the electrodes, the higher is the cost of fabricating them.

One single coverslip can be used for a few experiment in a row.

### Slide 2

We have seen that the situation we have is that we have an electrode pad with a
certain number of cell bodies clustered around the electrode. The best
expectation that we have is that the electrical recording we have from the
electrode is the summation of the effects of multiple cells. If we take one
strong assumption that two neurons afferent to the same electrode will
generally have different covered area and, even if they covered the electrode
in the same way, their spiking waveform would be different because generally
they have different nature and ionic channel density, then the shape of the
spikes for each neuron is stationary. We assume that the relative positioning
of the neurons with respect to the electrode is quite stable across our
(short-term) recording. Since the position of the neuron wrt the electrode is
the parameter that determines the shape of the trace of the spike of that
neuron, how is it recorded by the electrode? with a specific shape, which is
different from one neuron to the other. This because, even if they do the same
spikes, their relative position wrt the electrode changes the shape of the
extracellular recording that the electrode shows. If we consider that position
is stable across the recording we can assume that every time Neuron1 is spiking
i will see on the trace something that has its shape and every time Neuron2
spikes i will see on the trace its shape. If the two sources can be associated
to different shapes of the signal we can approach this problem as a pattern
matching, PCA analysis or whatever of these algorithms which can be efficiently
applied to the separation of sources inside the signal.

## Page 9

### Slide 1

How can we do that?  We start from raw data, the electrodes recording. Then we
filter the data with HP filter, to remove short fluctuations. Then we set a
threshold and we recognize when spikes occur. Spike are events where the set
threshold has been overcome by the signal. For each spike we have a certain
signal around that spike. We cut the signal into small branches around each
time event (about 5ms) and we take these same length signals associated to the
different spikes that have been recorded by the same electrodes. The small
branches can be superimposed and then we can try to capture, by many different
algorithms proposed in the literature, and separate the sources. The goal is to
be able to distinguish the shapes of the spikes as coming from different
sources and then associate each cluster to one cell. It's a blind separation.

In this way we are exploiting post-processing analysis in order to increase the
spatial resolution of the system.

### Slide 2

If we want to increase the spatial resolution from the hardware point of view,
one solution is to use high density MEA. They use c-mos technology: it's like
pixel matrix. C-mos work like electrodes but they are fabricated with a high
spatial resolution. The worsening of SNR is one on the consequences. This
because a single electrode sees a very small region, so the signal is smaller.

## Page 10

### Slide 1

Another issue about MEA is stimulation. When we interface with neuronal culture
we want to have two different tasks:

- *Read the activity of the neurons* (what we have talked so far)
- *Stimulate the neurons* and produce an activation from the outside. If we use
	MEA for stimulation we have lot of difficulties because of low selectivity.
	Since the medium of the colture is conductive, the tendency of spreading of
	the simulation is high and we are not able to confine the stimulation on a
	specific electrode area. One solution is to use microfluidic and separate the
	cultures in multiple separated cultures, this way the stimulation can be
	confined into the sub-colture of neurons.

### Slide 2

Let's summarize the pros and cons of MEA system.

Advantages:

- High spatio-temporal recordings of network activity.
- Network level large acquisition. And we can see the modulation of local
	properties and impact at the network level.
- Long and repetitive time recordings because we can have good recording across
	multiple measurement: the coverslip can be put in the incubators and then
	taken again for recording everyday. Temperature must be stable. pH can be
	kept quite stable by using proper caps that avoid evaporation of the medium.

Disadvantages:

- Losing correspondence between morphology and function.
- High temporal resolution but low spatial resolution.
- Low selectivity in stimulation.
- Losing the capability to understand what are the sub-threshold's event that
	produce the AP. This is called dark neuron problem: the neuron is dark
	because we can only see it when it spikes.

## Page 11

### Slide 1
### Slide 2

Optogenetics is not ethically available on human but only on animals.

## Page 12

### Slide 2

We are moving from electronic tools recording and stimulating neurons to
optical solution to do the same. We are in cultured neurons or brain slices (so
in-vitro) and we want to interface neurons with the artificial world.

Let's start from stimulation. There are very nice solution to stimulate neurons
via light. These solution are based on cage compound: we are taking a compound
(Glutamate molecule) and attaching to it a caging group, which is blocking the
effect of glutamate to the neurons (it's not behaving like glutamate). This
compound is just in the medium as a neutral body. If light is delivered on that
molecule, the caging group has been designed in order to be sensible to light.
This means that the cage compound can be switched into the active form by using
short UV pulses, so it's light-dependent. UV light pulses cut the cage compound
and frees the Glutamate which is now an active molecule. This way we have a
natural and physiological stimulation because it's managed through the use of
neurotransmitter. On the other side we have that UV pulse can be highly focused
so we can have a very high selectivity on which of the culture where we want
the glutamate to become active.

## Page 13

### Slide 1

How does this stimulation work?

We need to have, on the one side the UV laser (source of light with a specific
UV wavelength). This is mounted on the microscope. The micro-manipulator is the
one that is used to move the fiber glass and to focus the laser on the sample.
We have a very selective activation. We can couple this system with a MEA for
recording activity. At the end what we need is to control the delivery of the
pulse by the same computer that captures the information about where is the
focus of the pulse and which receives the recording from the MEA, to have a
synchronized system. The illuminator is to assure that the epifluorescence
microscope can work and see something from the camera.

### Slide 2

In case of stimulation, what happens is this kind of situation: we have an area
which is the focus of the UV light (the one marked with white).
Red-backgrounded signal is taken from the electrode shown on the left (the
black one). The signal has 2 very high pulses, that are the two UV pulses. We
can see a bunch of other electrodes which have activity correlated with the
timing of the two UV pulses. This kind of information is giving us data about
how is the connection of the network (consider that the green-backgrounded
electrodes are far away from the red one). This stimulation in the other
electrodes is not due to spreading of the stimulation because UV pulse is
spatially confined but it's due to the connectivity of the network so now we
are able to see how the network is transporting the information across
different places inside the network.

## Page 14

### Slide 1
### Slide 2

## Page 15

### Slide 2

Last part is about optical tools for studying neuronal network.

## Page 16

### Slide 1

Every time we talk about optical recording of cells we deal with the physical
mechanism of fluorescence. Fluorescence deals with the possibility for a
compound to move from a ground state to an excited state because of an external
excitation (photon). The compound remain on that state for a certain time and
then returns back to the ground state. This return is associated with an
emission of light with a lower energy with respect to the absorption.

We can recognize these passages from ground to excitatory state by reading the
emission light. Excitatory and emission of light are at different wavelengths
because wavelength is inversely proportional with respect to the energy.

### Slide 2

In order to produce fluorescence we need to have a source of light from which
we select the excitation wavelength, which goes to the compound passing through
the objective of the microscope. The compound is excited and excitation is
followed by the emission. Emission is then passing through the objective of the
microscope and goes back to a detector, a camera and actually we need spatial
filters (dichroic mirrors) capable for some wavelength to work as mirrors and
from other as transparent. 

From a white beam of light the excitation filter selects the monochromatic
beam. This goes against the dichroic mirror that flex the beam on the objective
and the specimen. The compound absorbs the excitation and goes to excitatory
state. Going down to the ground state produces an emission and the emission
beam is monochromatic at a wavelength that is higher than the one that the
absorption. Monochromatic emitted beam is going through the dichroic mirror and
hitting the detector.

Depending on what i put as a specimen I can modulate the excitation and
emission filters and read different properties of the specimen.

Fluorescence is very used in experiments, depending in the fluorescence dye
that we put in the experiment we can let it bind with different elements in the
specimen and have different results.

## Page 17

### Slide 1

Among this big topic of fluorescence there are some specific compound that are
called *voltage sensitive dyes* (VSD). These dyes are changing their
fluorescence properties depending on the voltage. Since our goal is to record
the activity of the neuron, our goal is to record voltage changes. We can read
the changes in voltages by looking to the fluorescence output of those
cultures. 

In the coverslip we have neurons and we put VSD inside the medium. VSD binds to
the membrane of the neurons and become sensitive to the change of the voltages
across the membrane so the electrical activity of the neuron (exactly
transmembrane potential). VSD has a bell shape that describes their absorbed
light and they are sensible to a specific ranges of wavelength light to be
absorbed and they will emit a light which is at higher wavelength with respect
to absorption. The wavelength of absorption and the one of emission are no
exactly 1 and 1 but a range of wavelength. We can though select the filter with
specific bandwidth. We can use 530 nm wavelength for exciting the cell (red
curve) and then read the  emission arround 660 nm wavelength.

What are the properties we are interested in?  The point is that the solid
curves correspond to baseline conditions while the two dashed curves correspond
to spike condition. When we have a change in the voltages of the membrane (i.e.
spike) we will see that first we have a shift of the excitatory bell which
means that a less power of excitation is delivered to the compound, then this
is accompanied with a reduction in emission (because emission is proportional
to the excitation) but more of that there's a shift of the curve of the
emission and so, what's happening is that when spike is on we will read the
gray part under third blue curve as energy of emission at spiking. When spike
was off we were able to read an energy that was all the gray area under first
blue curve, so a greater energy. There's a variation in the emitted light which
informs about the occurrance of the spike. We are optically reading an
information about the electrical activity of the cells.

### Slide 2


