# Neuroengineering for Biology

## Page 1

### Slide 2

to comprehend the information coding of the neuron.

## Page 2

### Slide 1

we want to understand the behavior of a small subset of neurons working together

the same step is done when we are moving from compartimental model (single neuron) to microcircuit level micorcirc level (point neuron)

### Slide 2

cultured networks --> derived from embryo neurons of rodents. these are not connected yet. we culure them in an environment condition in order to let them form a network in-vitro. this way we have a lot of similarity with the normal networks. we can exploit it at different timestamps and it's different from the brain slices because after 2 times the brain slice should be thrown away.

alternative --> using brain slices by sacrificing the animal with brain circuit. the biologist cuts a slice of the brain and keeps it alive. network has been formed in the brain. it is a real network in the brain. we are capturing a specific time window of the network.

## Page 3

### Slide 1

the real ippocampus is talking to other structures in-vivo.

Induced pluripotent stem cells. they go back to a pluripotent state. by proper technology we can bring back the cell to a pluripotent state, that's amazing! we can make them neuron-like cell in vitro by starting from the specific subject cell.

### Slide 2

- both record + stimulate the activity of hunderds individual neurons (studying information transfer in NN).
- long acquisition --> information processed (plasticity).
- transmembrane potential --> neurons speak a language made of variation of transm. potential so spike activity.
