---
Title: Marcal Crespo paper
Author: Luca Andriotto
---

# Marcal Crespo paper

Ci si sofferma sugli algoritmi di controllo ad alto livello, ossia quelli che
hanno come obiettivo *provocare la plasticità motoria*.  Possiamo raggruppare
gli algoritmi di controllo in quattro categorie:

- assistive: 			Il robot assiste il paziente a muovere l'arto
  				compromesso lungo una traiettoria desiderata.
- challenge-based:		Opposto all'assistive. Il robot complica il
  				movimento, creando una sfida per il paziente.
- Haptic simulation:		Vengono simulati dei movimenti ADL in un
  				framework virtuale.
- non-contact coaching:		Non vi è contatto tra robot e paziente ma il
				primo guida il secondo nello svolgimento della
				terapia.

Il paradigma più sviluppato è quello assistivo. Le precedenti strategie possono
essere utilizzate anche in combinazione.  Gli algoritmi di controllo ad alto
livello sono supportati da controller di basso livello che modulano forza,
velocità, impedenza e ammittanza.

## Controller assistivi

Forniscono un'assistenza esterna fisica al movimento.  Un'implementazione
manuale della stesse funzioni è svolta dai fisioterapisti.  Si frappone tra
esercizio e stretching dei muscoli del paziente: l'esercizio è fondamentale per
la plasticità motoria.  Lo stretching previene invece l'irrigidimento dei
tessuti molli.  Muovendo l'arto compromesso in modi altrimenti non facilmente
eseguibili favorisce la plasticità nervosa mediante la stimolazione
somatosensoriale.  Eseguendo diversi movimenti è possibile orientare il
paziente a sessioni di terapia più intense.  Questo approccio è utilizzato in
terapia in quanto è crea delle condizioni di esercizio in sicurezza per
movimenti quali il cammino e la guida.

Esiste però un approccio complementare che considera l'esercizio assistito
potrebbe decrescere il motor learning (i.e. guidance hypothesis).  Questa è
forte del fatto che la dinamica del movimento cambia quando lo si effettua
assistiti.  Il vero problema è che il paziente si impigrisce. non è motivato
nel provare a compiere il movimento stesso perchè la macchina lo aiuta ad
ultimarlo.

La soluzione alla slacking hypothesis e quindi al relativo problema
dell'impigrirsi del paziente è utilizzare l'approccio *"assistance-as needed"*.
Questa tipologia di controller permette di assistere i pazienti tanto quanto è
richiesto per lo svolgimento dell'obiettivo.  La macchina incoraggia l'inizio
del movimento e lo sforzo del paziente concedendo un'intervallo di variabilità
in cui l'azione del paziente non è guidata, se le variabili misurate del
paziente dovessero ritrovarsi al di fuori di questa deadband l'assistenza
verrebbe azionata per riportare lo stato nel range.

è necessario effettuare un'ulteriore classificazione dei dispositivi assisted

- impedance-based
- counterbalance-based
- EMG-based
- performance-based

### Impedance-based

I primi controller per terapia robotica assistiva sono quelli a feedback di
posizione.  Il feedback sulla posizione guida l'attività di assistenza al
paziente.  I controller più avanzati non dispongono della mera stiffness
(rigidezza fornita dalla costante eleastica) ma anche di campi di forze
viscose.  Alla base il robot fornisce assistenza al paziene qualora deviasse
dalla traiettoria desiderata, applicando una restoring force.  La restoring
force viene generata considerando un'impedenza meccanica propriamente
dimensionata.  Questo tipo di controllers si basa sul principio di
"assistance-as-needed".  Un esempio di questa tipologia sono i controller con
feedback di posizione proporzionale e derivativo: più il paziente si allontana
dalla traiettoria desiderata e maggiore sarà la forza esercitata dal robot,
seguendo il modello molla + smorzatore.  è necessario tenere conto della
normale variabilità del movimento umano, quindi il robot disporrà di una
deadband in cui l'intervento non è abilitato.  L'implementazione degli
algoritmi impedance-based può essere:
- spaziale:		canale virtuale che guida il movimento del braccio.
- spazio-temporale:	canale virtuale con parete mobile.

Esiste una variante all'approccio usuale impedance-based in forma di
triggered-assistance.  Il paziente, in assenza completa di intervento robotico
inizia un tentativo di movimento e successivamente viene triggerata una forma
di assistenza impedance-based.  Le variabili critiche misurate allo scopo di
realizzare il feedback sono:
- tempo trascorso
- forza generata dal paziente
- errore di tracking spaziale
- velocità dell'arto
- attivitò muscolare (misurata mediante elettrodi EMG superficiali)

### Counterbalanced-based

Controbilanciare, equiparare il peso dell'arto compromesso, è una secondo tipo
di strategia utilizzata.  L'utilizzo è molto diffuso in riabilitazione, a
partire dai supporti per braccia, imbracature sospese etc.  Si sfrutta anche la
galleggiabilità del corpo per effettuare terapie riabilitative in piscina.

- Therapy-WREX: utilizza una strategia di controbilanciamento passivo mediata
  da elastici vincolati a due gruppi di quattro barre, capaci di
  controbilanciare l'arto, supportando il reaching task e il disegno in ampi
  spazi di lavoro. L'assistenza viene modulata mediante l'aggiunta/rimozione di
  elastici a seconda delle esigenze del paziente.
- La robotica introduce un'utilizzo di tecniche attive per il counterbalance.
  Si può stabilire via software la quantità di carico da supportare, andando a
  considerare anche un supporto per altri tipi di forze.

Quest'operazione viene effettuata anche nel caso di supporti attivi e può
avvenire anche durante la sessione di allenamento.

### EMG-based

Sono stati sviluppati sistemi che utilizzano l'EMG superficiale (sEMG) per
guidare l'assistenza.  I segnali registrati dai muscoli scelti vengono
utilizzati per modulare gli effetti del robot.  Ci sono alcune limitazioni a
questo tipo di approccio, ad esempio quella il posizionamento degli elettrodi,
l'interferenza dovuta a segnali di muscoli vicini.  I contro di questa tecnica
dipendono dalle condizioni neurologiche dell'individuo che ne beneficerà.  I
parametri dell'EMG devono essere calibrati al variare dell'utente e della
sessione.  Un ulteriore problema: se il paziente generasse un input anomalo ciò
darebbe in output un movimento indesiderato.

### Performance based adaptation of task parameters

Le strategie studiate prima di questa vengono definite *statiche* perchè non
adattano i parametri del controller basandosi su misurazioni online delle
performance del paziente.  Il vantaggio principale è quello di avere
un'assistenza su misura delle performance e delle necessità del paziente.  Ciò
si verifica su diverse scale: dal movimento effettuato all'intero processo
riabilitativo.  L'adattamento dei parametri del controller è fondamentale:
- per le strategie **patient-cooperative training** (sviluppate in primo luogo
  per il Lokomat) in quanto viene considerata l'intenzione del paziente e non
  viene imposta rigidamente una strategia di controllo. (commento: potrei dire
  che la strategia in questo caso è più flessibile).
- per le strategie **Performance-based, progressive robot-assisted therapy**
  (sviluppate per MIT-MANUS).

$P_{i+1} = f*P_i - g*e_i$

con P: paramentro del controller.  i: movimento.  e: errore di performance
oppure misura dell'abilità del paziente.  f: forgetting 	factor g: gain
factor NB: for MIT-MANUS the $f = 1$

per esempio un robot con controllo in "position-feedback" può permettere ai
pazienti dei movimenti con traiettoria più veloce di quella desiderata. ciò è
possibile modulando i parametri di durata del movimento e di rigidità del
controller, permettendo a chi presenta dei danni maggiori di eseguire un
reaching task meno impegnativo.

l'ARM Guide invece propone un approccio simile per l'update dei parametri
considerando ad esempio come variabile di performance misurata la velocità
massima dell'arto durante il movimento nel reaching task mentre la variabile
controllata è il coefficiente negativo di smorzamento (il segno meno è dovuto
al fatto che il robot non deve smorzare il movimento, bensì amplificarlo).
Questo algoritmo è stato adattato al concetto di impedenza in questa
formulazione:

$G_{i+1} = f*G_i - g*e_i$

con G: impedenza. (con impedenza si intende il rapporto tra forza in output,
relativa al robot, e movimento in input. Se controllassimo l'impedenza
controlleremmo la forza di resistenza ai movimenti esterni).

L'impedenza in robot che assistono il cammino risulta convergere a valori bassi
e unici per aiutare la deambulazione di pazienti affetti da SCI (spinal cord
impairment).  (commento: Al crescere dell'impedenza ho un maggiore intervento
da parte del robot al movimento in input)

### Ruolo generico del forgetting factor Il forgetting factor è importante per
evitare che il paziente si impigrisca (slacking).  Esempio:

- se $f = 1$ e non avessi errore nella variabile misurata, il parametro
  controllato sarebbe sempre costante e ciò eliminerebbe l'elemento di sfida.
- se invece si considerasse $0 < f < 1$ l'algoritmo riduce parametro
  controllato anche quando l'errore in misura è trascurabile, con l'effetto di
  una maggior sfida per il partecipante. Si ottiene una riduzione della forza
  del robot quando l'errore della performance è piccolo.
