# Lecture 7 --- Coaching robots (Laura Santos)

## Slide 2

## Coaching robot

An general outline. Laura is talking about coaching robots, particularly
referring to its usage in the case of rehabilitation of ASD (Autism Spectrum
Disorder) children. She's talking about therapies and how the robot are used in
the therapies. A specific therapy is the focus of this presentation: the
mirroring therapy for ASD. Main design challenges and solutions and in the end
the conclusions.

## Slide 3

Coaching robots: inside the field of social assistive robots that provides
assistance, enhances therapy and achieves measurable progress in convalescence,
rehabilitation, learning and well-being.

## Slide 4

Coaching robots specifically tries to train motor skills. They are presenting
exercises to be executed by people. They have been used basically on two focus
group: adults and children

## Slide 5

Why ASD? ASD is a neurodevelopment disorder. It's characterized by three features:

1. Social deficit
2. Communication deficit
3. Presence of repetitive behavior

## Slide 6

Why robot are so good for ASD children? Because they implement repetitive
actions and kind of a stable environment, we can customize behaviors we give to
robot so that they are understood better by the children. We also have the part
of motivation and engagement which is an essential part for any kind of
therapy.

## Slide 7

Why not avatar? Because robots represent a physical entity, in this way the
need for the coach embodiment is satisfied. Robotic systems tend to elicit more
spontaneous reactions and imitation behaviors from subjects than in the
situation where the robot was presented through video. Having the physical
entity is important to train the people. This is valid both for the children
and the adults group.

## Slide 8

In terms of strategies for ASD therapist focusing on robot for autism she will
present a brief review to have in mind, so to discuss later about the solutions
that were performed to the groups.

96 recoiled papers from 2016-2020.

## Slide 9

First conclusion was in relation to the robot. We have verified that actually
most of the paper used humanoid (they seem like human) robots (78%) vs
non-humanoid robots (22%). They have a very simplified expression which is
essential for children with autism. In this slide some humanoid robots are
reported: it's clear that there are lots of paper with NAO because it's a
commercial robot.

## Slide 10

In case of main actors that are present in the floor in different cases, we have:

- The child alone doing the protocol. That is the case of the diadic
	interaction.
- Triadic interaction having a key role because they involve the robot, the
	child and another entity (it can be another robot, or another human). The
	therapist usually wants a triadic interaction with a human involved because
	in this way it's easier to make the child interact with other humans. Having
	human entity during therapy is really important.

## Slide 11

How about the robot control? It can be either:

1. Controlled by the therapist. Therapist is in the room, has a computer and
	 controls the robot.
2. Controlled by a person outside the therapy room. In this way the control is
	 also know as the "Wizard of Oz". The robot seems automatic but in reality
	 it's not because it is controlled by someone.
3. Autonomous:
	- Preset autonomy: the therapist define some test and exercises previously.
	- Non adaptive autonomy: the robot is able to do some tasks in autonomy
		(following the subject) but then it executes pre-setted tasks.
	- Adaptive autonomy: the robot perceives the reactions of the children and
		adapts its behavior to them. This is the most used in the literature but
		creates several ethical problems because in this case it's the robot that
		has the responsibility of deciding what is the course of the therapy. What
		is done now is to have a sort of a robot enhanced therapy in which
		basically we have adaptive autonomy (the robot knows what is the best
		exercised to propose to the child) but it just suggests to the therapist
		what is the next step: a sort of therapy guided by what the robot suggests.

## Slide 12

Distribution of written papers: the most autonomy ones are much less written
because they are much more difficult to implement then the others but that's
the direction toward which we are tending.

## Slide 13

In terms of setting: the number of studies performed in research laboratories
are the highest in percentage, few arrives to home and to school. The real goal
in this case is to arrive home, the real daily life of the children.

## Slide 14

In terms of sensors: most of the papers record session with cameras that are
integrated on the robot or external to the robot. Then researcher evaluate the
behavior of children. Other sensors are used depending on the purpose of the
work:

- Kinect
- Gaze tracker
- EMG
- EEG

## Slide 15

In terms of feedback, the most given one is vocal feedback. It's very though to
give that kind of feedback but for children which are not high functioning
(there are different degrees of ASD), this means that they are not vocal so it
is necessary to provide other feedback, such as lights or movement.

## Slide 16

There are several kinds of applications:

- Social skill: the most trained one.
- Imitation training.
- Joint attention training: specific type of attention trained in children with
	ASD.
- Emotional recognition training.


## Slide 17


Let's focus on imitation training: in the research in mirror neurons we know
that there are several neurons activated both during the execution of a
goal-directed movements as well as during the observation of movements
performed by others, so these neurons can be used to train specific motor
skills.

By another study it was discovered that the motor and imitation skill of
children with ASD are connected with social skill. It's important to train
these skills to also enhance the social skill, which ASD children has a deficit
of.

## Slide 18

How is this mirroring therapy done? In a very simple way:

1. The child moves
2. Robot mirrors and gives feedback
3. Return to 1

In terms of setup we used NAO robot with a Kinect to see what the person is
doing. Everything is connected to a computer which will give directions to the
robot.

## Slide 19

About the design challenges that are present in this kind of therapies we have
the medical and the technical challenge. One depends on the other creating
challenges one to the other.

Medical challenges:

1. Interactive protocol.
2. Functional movement.
3. Maintaining engagement of the people doing this protocol.

Technical challenges:

4. How is the embodied mirroring executed?
5. What are the quantitative measures that we can take?
6. How to do a feedback system?
7. How to evaluate the intention of the children?

## Slide 20

Let's start from the first challenge: the interactive protocol.

They have decided to have a triadic setting in which there's the child, the
therapist and the robot (triadic human). 

The basic protocol is this:

- NAO does an exercise.
- NAO mirrors the subject.
- NAO gives a feedback.

## Slide 21



## Slide 22

In terms of functional movement (second challenge), the chosen ones in terms of
medical recommendation are gestures. This because there's a communication
impairment in terms of ASD children, not just in verbal communication but also
in eye contacts and gesture used. These gestures have a meaning.

## Slide 23

To maintain the engagement (third engagement) we have to create different
difficulty levels.

This was created for both high functioning and low functioning ASD children. 

1. Familiarization with the robot. 
2. Gesture mirror.
3. Training the gestures.
4. Training gesture in specific scenarios.
5. How the children generalize the gestures.

All this first four levels are supposed to be done by the robot or by the
therapist in which the robot suggest

## Slide 24

Focusing on the technical challenges. Fourth challenge is the mirroring. How is
it done? Basically:

1. extract the Kinect point.
2. using a median filter to reduce the noise of these points.
3. rotating from the kinect reference frame to NAO reference frame (by using
	 rotation matrix)
4. Angles calculation: we calculate the angles from the Kinect point. Applying
	 this rule to several part of the body. In the research of Laura Santos the
	 formula is applied just for the upper limbs. Every arm is constituted by
	 three different points. The two vectors are arm and forearm. The angle is
	 between the two vectors
5. In the end motor torques are computed to have those kind of angles on the
	 robot.

## Slide 25

Quantitative measurements. They are very important for every strategy. Although
they are not standardized and this makes comparing different therapies very
difficult.

- Evaluation of the subject performance.
- Evaluation of exercise complexity.
- Adapt the exercise to the subject.

The panels represent four different angles that we give to the robot: the first
two tracks are for the left arm and the other two for the right arm.  There are
LShoulderPitch (left shoulder pitch, related to left shoulder elevation) and
LShoulderRoll (left shoulder roll, related to left shoulder
abduction/adduction) and also the correspective right versions. The task
consists in moving one limb after the other up and down (picking the stars).
These four different graphs represent exactly the four different angles. Here
the angles are given in terms of degrees but we will see that some angle are
negative. That might seem strange but it's the specification of the reference
frame of NAO robot. The pink areas represent the target angles, with its
accepted variability, participant needs to achieve. When the measured angles
reach the target angles (both for shoulder pitch and roll of a specific arm),
the subject achieves the first positive feedback. The outcome measure is the
time to achieve this first feedback.  In other words, the time interval (which
is here highlighted with a blue double arrow), from the time zero (when the
robot asks the participant to perform the motor task) and time of the actual
reaching of the target angles, is an important measurement because it shows how
fast the child will achieve the first feedback (e.g. how fast he performs the
movement).

## Slide 26

This was also about the feedback system which is very rule based because we
just define the rules for each angles of the person. When a person receives
certain angles he receives feedback but this has several problems:

- If we have a complex movement we have to define targets for every angle of
	the patient and sometimes we want is an evaluation of the overall movement.

Thankfully there are other machine learning technique that have been created.
This is an example: we have different kinematics in time (here shown as the
kinect output) 

We have several kinematics in time and various joint coordinates in 3D. The
joints are represented by a point in the kinect output. Each joint is
represented by three coordinates (x,y,z) and basically we have these mapping of
3D coordinates of body joints. We transform (x,y,z) values into (R,G,B) values
for a range of 0-255. The result is an RGB sequence. The repeated sequence in
time can be stacked one after the other to form an RGB image. This image have
the spacial structure (all the joints) on the rows and the temporal dynamics on
the columns. The goal of machine learning is to recognize pattern in this RGB
image through CNN (Convolutional Neural Network). 

We are using biomechanical data (trajectories of joints) and representing them
by normalizing them within the typical ranges of RGB associated x to R, y to G
and z to B. We scale the maximum and the minimum of the working volume
respectively to 0 and 255, so that we can associate the movement to a specific
RGB composition. Each single pixel relates to a single joint and the color
informed on the composition of (x,y,z) coordinates of that join. The first
column is all the joins at time 1 and then column after column all the other
time instants. This allows to exploit all the advances of DNN which have been
trained mostly trained on images processing into this kind of different
dataset. A tricky way to summarize data. In this way we maintain all the
information related to the biomechanics, something that was not possible to do
in a rule based approach.

## Slide 28

It's more about gaze and about evaluating the attention that is a super hot
topic in ASD because to maintain the engagement of the child we need to
understand where the attention of the child is and what's the child looking and
the various stage of attention. There are various technique that have been
explored, the implemented one is reported here.

- Expressing the keypoint (aka the set of joints freezed in time) from Kinect
- Feeding RGB data to gaze 360 to understand where the patient's looking.
- Extracting the azimuth and elevation of the eyes.

Kinect is very important because we don't want to use too much intrusive
sensors when dealing with ASD participants. This because children are very
reactive and sensitive and putting this kind of instrumentation could be very
cumbersome.

## Slide 30

Conclusions.

- Coaching robots have been introduced with success in ASD field.
- Mirroring therapy is intrinsically connected with training of motor skills.
- Robot adaptability is key to maintain the children engagement.
- Medical and technical problems should be tackled together in order to bring
	these therapies to the real clinical practice.

