# Neuroengineeering

| Lecture                       | Topic                                | Revised | TODO | Days       | Link(s)                                                                                                                          |
| :---                          | :---                                 | :---    | :--- | :---       | :---                                                                                                                             |
| [01](Pedrocchi/01/Lecture.md) | Computational neuroscience (1)       | Yes     | Yes  | 2020/10/15 | https://politecnicomilano.webex.com/recordingservice/sites/politecnicomilano/recording/play/dd17396e2da542daa141d2aee36497ac     |
|                               | Computational neuroscience (2)       | Yes     | Yes  | 2020/10/16 | https://politecnicomilano.webex.com/recordingservice/sites/politecnicomilano/recording/play/7cc779d40d1a4aa6ae4628ab8e962fd1     |
|                               | Computational neuroscience (3)       | Yes     | Yes  | 2020/10/19 | https://politecnicomilano.webex.com/recordingservice/sites/politecnicomilano/recording/play/db0907f269344cd98281b5deff140453     |
| [02](Pedrocchi/02/Lecture.md) | Neurorobotics                        | Yes     | Yes  | 2020/10/20 | https://politecnicomilano.webex.com/recordingservice/sites/politecnicomilano/recording/play/b4a2179c76494d90b3e37c43b5258d45     |
| [03](Pedrocchi/03/Lecture.md) | RCT and systematic reviews           | Yes     | Yes  | 2020/10/20 | https://politecnicomilano.webex.com/recordingservice/sites/politecnicomilano/recording/play/ae02d6cba5404d6d987634626d393a15     |
| [S1](D_Angelo/S1/Lecture.md)  | Seminar Human Brain Project          | No      | No   | 2020/10/22 | https://politecnicomilano.webex.com/recordingservice/sites/politecnicomilano/recording/play/d10e77b4e9914d888d98e33d0174459a     |
| [04](Pedrocchi/04/Lecture.md) | Rehab robotics (1)                   | Yes     | Yes  | 2020/10/27 | https://politecnicomilano.webex.com/recordingservice/sites/politecnicomilano/recording/play/30146ec749134c8cb630512bf13026de     |
|                               | Rehab robotics (2)                   | Yes     | Yes  | 2020/10/29 | https://politecnicomilano.webex.com/recordingservice/sites/politecnicomilano/recording/play/89577a9ce9ff492a88fd4522fdc271c4     |
| [J2](Journal/05/Lecture.md)   | Journal club \#2: Rehab robotics (1) | Yes     | Yes  | 2020/11/02 | https://politecnicomilano.webex.com/recordingservice/sites/politecnicomilano/recording/play/fbf65aba9d3d47a6b61e37b09e237b00     |
|                               | Journal club \#2: Rehab robotics (2) | Yes     | Yes  | 2020/11/02 | https://politecnicomilano.webex.com/recordingservice/sites/politecnicomilano/recording/play/cd1744e2f97e44218d267b42783b5aae     |
| [06](Pedrocchi/06/Lecture.md) | Neuroprosthesics (1)                 | Yes     | Yes  | 2020/11/03 | https://politecnicomilano.webex.com/recordingservice/sites/politecnicomilano/recording/play/3bf2d092dd1d4936a5ab55820a700940     |
|                               | Neuroprosthesics (2)                 | Yes     | Yes  | 2020/11/05 | https://politecnicomilano.webex.com/recordingservice/sites/politecnicomilano/recording/play/6aabcff30a7140f281235c2bd09548b4     |
|                               | Neuroprosthesics (3)                 | Yes     | Yes  | 2020/11/10 | https://politecnicomilano.webex.com/recordingservice/sites/politecnicomilano/recording/play/0bd993358e7c4ca2bd94e37b8b8dc26e     |
| [S2](Santos/07/Lecture.md)    | Coaching robots                      | Yes     | Yes  | 2020/11/03 | https://politecnicomilano.webex.com/recordingservice/sites/politecnicomilano/recording/play/3bf2d092dd1d4936a5ab55820a700940     |
| [07](Pedrocchi/07/Lecture.md) | In-vitro neuroengineering (1)        | Yes     | Yes  | 2020/11/10 | https://politecnicomilano.webex.com/recordingservice/sites/politecnicomilano/recording/play/e1514ba280424d4f970cb0302d7cb225     |
|                               | In-vitro neuroengineering (2)        | Yes     | Yes  | 2020/11/13 | https://politecnicomilano.webex.com/recordingservice/sites/politecnicomilano/recording/962b05554db040b3841ef58e3af82c56/playback |
| [S2](Molteni/08/Lecture.md)   | Seminar Villa Beretta                | No      | No   | 2020/11/12 | https://politecnicomilano.webex.com/recordingservice/sites/politecnicomilano/recording/90bef693be19430fa50078089f73c0e1/playback |


## Notes

The first journal club is not here reported. The one that are not marked in the
TODO column are *not* to consider as part of the programme.

# Exam's modality

- [Modalità d'esame](Documents/Neuroengineering/Pedrocchi/Modalità.md)
- [Robotics Rehabs](Robotics\ Rehabs.md)
- [Marcal Crespo paper](Marcal\ Crespo\ paper.md)
- [Appunti](Documents/Neuroengineering/Pedrocchi/Theory/README.md)
