# BioMedical Wiki

[![CC BY-SA 4.0][cc-by-sa-shield]][cc-by-sa]

![](Pictures/polimi-logo.png)

This is a tentative to create a repository in which people can share their
notes in an extensible format (text based, avoiding proprietary bloat) that can
be rendered by GitLab to show formulas and pictures automatically. I have added
some courses of my interest but feel free to add as many as you want. I am
really open to pull requests but I am asking you to follow the rule and keep it
simple.  I hope it could be useful to people.  If you like this project share
and contribute to this Wiki so that we can all help each other.  *Sharing is
caring*.

You can access the wiki by clicking [here](index.md).

*PS*: Forgive if sometimes I use Italian in English notes. I am working on this
semester courses and a lot is still missing, I hope you will understand.

## Requirements

1. A good text editor.
2. Markdown to PDF compiler. (Optional. Just if you want to print out the
   PDF documents).

## Tools and recommended tips

[Writing markdown](https://guides.github.com/features/mastering-markdown/) is not that big of a deal. Just find a good IDE/text editor
that suits your needs. For example you can even use "Notepad" (not
recommended), "VSCode" or so on. If you are into GNU/Linux or UNIX this
is the editor I recommend.

- [NeoVim](https://neovim.io/) (with some plugins):
	- [VimWiki](https://github.com/vimwiki/vimwiki): This helps a lot when
	  it comes to linking the documents in various folders.

You may want to compile the documents in HTML or PDF in order to print them out
later. Some text editors have plugins to do that.  What I recommend:

- [Pandoc](https://pandoc.org/installing.html): this tool is available for \*nix and Windows and it permits to
  translate a document in one specific format to any other supported format,
  it's really amazing!

### Bonus

If you are a Windows user I recommend a fantastic dev-keyboard to be installed
so to have all the symbols in the right place (unfortunately I found that only
the Italian layout):

- [ITA-Dev
  Keyboard](https://onedrive.live.com/?authkey=%21AKVHOaDV%2DYXh78o&cid=47531499F25297BA&id=47531499F25297BA%2114274&parId=47531499F25297BA%213619&action=locate):
  This remaps useful symbols:
	- <kbd>AltGr</kbd>+<kbd>ì</kbd>	 '\~'
	- <kbd>AltGr</kbd>+<kbd>'</kbd>	 '\`'

## Documents' organization

In this section I present the convention I (am trying to) follow.

### Tree

1. Each folder in the root of the repo shall maintain the name of a specific
   class. From here on this folder is being referred simply by "Class".
2. Each Class shall gather folders named after Professor. From here on this
   folder is being referred simply by "Professor".
3. In each Professor, folders shall gather the Lectures. Each Lecture is
   numbered incrementally every time a new topic has been introduced. *Note*:
   if lessons are jumping from one Professor to another of the same Class,
   numbers of lectures are still incrementing, indicating which comes
   chronologically first.
4. Each Lesson can contain a folder named "Pictures" in which we shall gather
   all ".png" to be shown in the respective document. Other than pictures these
   folders shall contain text documents with ".md" extension.

### Indexes

1. Each level (apart from Lectures) shall contains an "index.md" to better
   navigate the structure of the Wiki (using internal links to documents).
2. The first table shall gather the document links, difficulty while learning
   the topic and the day of the lecture. If you add a lecture be sure that it
   is correctly linked so that anyone can have access to it.
3. The second table shall gather the lectures link ordered by date.

### Notes

I propose this template that could be used both for lectures with slides and
with handouts. If you have better idea for notes organization I'm open for
discussions.

1. Each document shall begin with a first level header: \# Lecture NN
2. Second level headers (\#\# Slide NN) are used to divide into slides.
3. At the end of the document you can put reference paper, useful links to
   youtube videos or whatever you think it is useful to better understand the
   topic.
4. Each document shall end with references (Title of slides, Book chapter,
   etc.). This information shall be in the section \# References (or \#
   Riferimenti bibliografici).
5. Files shall be named "Lectures.md" or "Lezione.md" depending on the language
   they were written in.

#### Conventions

When looking at notes, sometimes you'll encounter some abbreviation I use in
different occasions:

- "Read as is": when professor is clearly reading the slide without adding
  comments.
- "===": separator between two different days' notes regarding the same slide.
  (sometimes used in Cerutti).

They are just conventions I use to better interpret the text. If you have some
more please propose!

## Some rules

I am trying to keep the wiki as clean as possible, you can help the process by
following these steps:

1. This is a markdown based repository. Push simple text documents with ".md"
   and images in a format that can be easily embedded into GitLab's preview
   (".png"). Any other document will be rejected.
2. Tree organization shall remain the same to easily automate the process PDF
   compilation. (I'm working on a shell script that can be very useful).
3. Pictures and graphs shall be explicative and small enough to fit into a
   compiled PDF.  Pictures that are too big are being rejected.
4. Before pushing please be sure that you don't have problem in compiling into
   PDF.
5. Formulas shall be edited this way: ```$` your_formula_here `$```
6. Don't use the ```math``` environment because it breaks compilation with
   pandoc. (if you don't know what it is, don't worry. Just format your
   formulas as written in the previous point).
7. Be good: please don't link to any "sci-hub", "libgen", etc. Piracy is not
   allowed.

**If the rules are not followed I'm not granting to accept your pull request!**

Add yourself to [AUTHORS.md](AUTHORS.md), along with your website or GitHub
page, to be cited.

## Why not GitHub?

GitHub doesn't permit an automatic math formulas' rendering. There are some
tricks but they result in weird looking markdown documents, by making their
interpretation not so trivial. Moreover someone just wouldn't like to compile
PDF and keep everythink in the cloud. I thought that this was the optimal
solution to have GB of unuseful data.

## Why not LaTeX?

Markdown syntax is very easy and macros can be easily setup by many text
editors.  Probably LaTeX would have been the best choice to write fancy
scientific documents but I preferred to keep it as simple as possible, so to
include more people in the project.

## Contacts

Do you have suggestion about anything? Would you like to collaborate? Feel
free to contact me via e-mail to luca.andriotto@mail.polimi.it

## License

This work is licensed under a
[Creative Commons Attribution-ShareAlike 4.0 International License][cc-by-sa].

[![CC BY-SA 4.0][cc-by-sa-image]][cc-by-sa]

[cc-by-sa]: http://creativecommons.org/licenses/by-sa/4.0/
[cc-by-sa-image]: https://licensebuttons.net/l/by-sa/4.0/88x31.png
[cc-by-sa-shield]: https://img.shields.io/badge/License-CC%20BY--SA%204.0-lightgrey.svg
