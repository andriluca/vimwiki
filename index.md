# Courses

| Classes                                                             | CFU | Prof(f).              | Revise              | Follow |
| ---                                                                 | --- | ---                   | ---                 | ---    |
| [Advanced Signal Processing](Advanced/index.md)                     | 10  | Cerutti, Barbieri R.  | 2-3-4 (C)           |        |
| [MBICAS](MBICAS/index.md)                                           | 10  | Baselli, Baroni       |                     |        |
| [BSP+MI](BSPMI/index.md)                                            | 10  | Signorini             |                     |        |
| [Elettronica Biomedica](Elettronica\ Biomedica/index.md)            | 10  | Dellacà               |                     |        |
| [Neuroengineering (Pedrocchi)](Neuroengineering/index.md)           | 10  | Pedrocchi             |                     |        |
| [Brevetti](Brevetti/index.md)                                       | 5   | Franzoni, Barbieri M. |                     |        |
| [Motorio](Motorio/index.md)                                         | 5   | Frigo                 |                     |        |
